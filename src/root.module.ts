import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule, APP_INITIALIZER} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule, PreloadAllModules} from '@angular/router';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {AlertModule} from 'ngx-bootstrap';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {JwtInterceptor} from './app/shared/services/jwt.interceptor'
import {CommonModule} from '@angular/common';
import {ToasterModule} from 'angular2-toaster';
// Routes
import {routes} from './root.routes';
import {RootComponent} from './root.component';
import {CookieService} from 'ngx-cookie-service';
import {AppCustomPreloader} from './app-routing-loader';
import {AuthModule} from './auth/auth.module'
import {AppModule} from './app/app.module'
import {ToasterHelperService} from './app/shared/services/toasterHelper.service';
import {PermissionService} from './app/shared/services/permission.service';

@NgModule({
  declarations: [
    RootComponent
  ],
  imports: [
    RouterModule.forRoot(routes, {
      useHash: false,
      preloadingStrategy: PreloadAllModules
    }),
    AlertModule.forRoot(),
    BsDropdownModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    CommonModule,
    AuthModule,
    AppModule,
    ToasterModule.forRoot(),
  ],
  providers: [
    {provide: 'ORIGIN_URL', useValue: location.origin},
    CookieService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    ToasterHelperService,
    AppCustomPreloader,
    PermissionService
  ],
  bootstrap: [RootComponent]
})
export class RootModule {
}
