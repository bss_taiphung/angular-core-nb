export const environment = {
  production: true,
  bllApiBaseAddress: 'https://mss-bll-dev.drawform.com',
  apiBlobUrl: 'https://mss-bll-dev.drawform.com/azure',
  cookieDomain: 'localhost',
  projectsUrl: 'https://mss-projects-dev.drawform.com',
  azureStorageContainer: 'mss-dev',
  signalRServer: 'https://itw-mss-signalr-dev.azurewebsites.net/',
  reportServerUrl: 'https://mss-reportserver-dev.drawform.com',
  reportAuthenticationToken: 'BypassITW94!',
  documentsSiteCollection: 'https://itwconnect.sharepoint.com/sites/drawform/documentcontrol/*',
  documentsTypeAndColumnGroupName: 'Drawform',
};
