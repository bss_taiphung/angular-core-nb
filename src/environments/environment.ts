// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  bllApiBaseAddress: 'https://mss-bll-dev.drawform.com',
  apiBlobUrl: 'https://mss-bll-dev.drawform.com/azure',
  cookieDomain: 'localhost',
  projectsUrl: 'https://mss-projects-dev.drawform.com',
  azureStorageContainer: 'mss-dev',
  signalRServer: 'https://itw-mss-signalr-dev.azurewebsites.net/',
  reportServerUrl: 'https://mss-reportserver-dev.drawform.com',
  reportAuthenticationToken: 'BypassITW94!',
  documentsSiteCollection: 'https://itwconnect.sharepoint.com/sites/drawform/documentcontrol/*',
  documentsTypeAndColumnGroupName: 'Drawform',
};
