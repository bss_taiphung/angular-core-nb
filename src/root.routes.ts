import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from "./app/app.component";
import { AuthGuard } from "./app/shared/guards/auth.guard"
export const routes: Routes = [
  { path: '', redirectTo: 'auth', pathMatch: 'full' },
  //{ path: '**', redirectTo: 'auth', pathMatch: 'full' },
  {
    path: 'auth',
    loadChildren: './auth/auth.module#AuthModule',
    data: { preload: true },
   
  },
  {
    path: 'app', 
    loadChildren: './app/app.module#AppModule',
    data: { preload: false },
  }
];
