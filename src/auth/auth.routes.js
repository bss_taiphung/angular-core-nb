"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var login_component_1 = require("./login/login.component");
var auth_component_1 = require("./auth.component");
exports.AuthRoutes = [
    {
        path: '',
        component: auth_component_1.AuthComponent,
        children: [
            //{ path: '**', redirectTo: 'login', pathMatch: 'full' },
            { path: '', redirectTo: 'login', pathMatch: 'full' },
            { path: 'login', component: login_component_1.LoginComponent }
        ],
    },
];
//# sourceMappingURL=auth.routes.js.map