import {Component, OnInit} from '@angular/core';
import {Validators, FormGroup, FormControl, ReactiveFormsModule} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../../app/shared/services/auth.service';
import {EqmSettingsService} from '../../app/shared/services/eqmsettings.service';
import {TokenDto} from '../../app/shared/models/tokenDto'
import {CookieService} from 'ngx-cookie-service'
import {CommonService} from '../../app/shared/services/common.service';
import {UserRoleService} from '../../app/sys-admin/userrole/service/userrole.service'
import {DATE} from 'ngx-bootstrap/chronos/units/constants';
import {EqmSettings, User, UserRole} from '../../app/shared/models/eqmSettings'
import {ProjectSettingsService} from '../../app/project/shared/services/projectsettings.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  token: TokenDto = new TokenDto();
  model: any = {};
  loading = false;
  pageRedirecting = false;
  returnUrl: string;
  isLoginSuccess: boolean = false;
  isLoginFailed: boolean = false;
  isUserSettingFailed: boolean = false;
  resetPwd: boolean = false;
  userRole: UserRole = new UserRole();
  public resetForm: FormGroup = new FormGroup({
    'resetUserName': new FormControl('', Validators.required),
  });

  isResetSuceess: boolean = true;
  errormessage: string = '';

  constructor(private route: ActivatedRoute,
              private router: Router,
              private authenticationService: AuthenticationService,
              private eqmSettingsService: EqmSettingsService,
              private userRoleService: UserRoleService,
              private cookieService: CookieService,
              private commonService: CommonService,
              private projectSettingsService: ProjectSettingsService) {
    this.userRole = this.eqmSettingsService.getUserRoles();
    this.resetForm.reset();
  }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || 'app/eqmaintenance/orders';
    const token = this.commonService.getCookies('usertoken');
    if (token) {
      // USER IS ALLREADY LOGGED IN, RETRUN TO ORDERS
      this.verifyUserSettings();
      // this.router.navigate([this.returnUrl]);
    } else {
      this.logout();
    }
  }

  resetPasswordSubmit(e) {
    if (this.resetForm.value !== '') {
      this.errormessage = '';
      const data = this.resetForm.value;
      this.authenticationService.ressetpassword(data.resetUserName,
        response => {
          this.resetForm.reset();
          this.isResetSuceess = true;
        },
        error => {
          this.errormessage = 'enter correct User PIN / Employee ID';
        }
      );
    } else {
      this.errormessage = 'enter your User PIN / Employee ID';
    }
  }

  login() {
    this.isLoginSuccess = false;
    this.isLoginFailed = false;
    this.loading = true;
    this.authenticationService.login(this.model.username, this.model.password).subscribe(
      response => {
        this.token = response;
        if (this.token) {
          this.isLoginSuccess = true;
          this.pageRedirecting = true;

          // for now we statically set the expiry to 30 years from now.
          // var expiry = new Date(this.token.expires);
          // expi = (expiry - new Date());
          // var days = timeDiff / (1000 * 60 * 60 * 24);
          this.commonService.setCookies('usertoken', this.token.accessToken);
          this.verifyUserSettings();

        } else {
          this.isLoginFailed = true;
        }
      },
      error => {
        this.isLoginSuccess = false;
        this.isLoginFailed = true;
        this.loading = false;
        this.pageRedirecting = false;
      });
  }

  verifyUserSettings() {

    // check and add mss cookie
    const mss = this.commonService.getCookies('Mss');

    if (!mss) {
      this.commonService.setCookies('Mss', 'Mss');
    }

    // set global user in cookie
    this.eqmSettingsService.setUser();

    // set global user roles, so we can later use it in menu setting
    this.eqmSettingsService.userRoles().subscribe(
      response => {
        // set global project workspace roles
        this.projectSettingsService.workSpaceUserSettings().subscribe();
      },
      error => {
        this.commonService.deleteAllCookies();
      }
    );


    this.eqmSettingsService.userSettings().subscribe(
      response => {
        if (response != null) {
          this.isLoginSuccess = false;
          this.isLoginFailed = false;
          this.pageRedirecting = false;

          // if (this.userRole.canViewEquipmentMaintenance && response.canAccessEqm) {
          if (response && response.canAccessEqm) {
            this.commonService.setCookies('module', 'eqm');
            this.router.navigate([this.returnUrl]);
          }
          else {
            this.commonService.setCookies('module', 'dashboard');
            this.router.navigate(['app/general/dashboard']);
          }

        } else {
          this.isLoginFailed = true;
          this.isLoginSuccess = false;
        }
        this.loading = false;
      },
      error => {
        this.isLoginSuccess = false;
        this.isUserSettingFailed = true;
        this.loading = false;
        this.pageRedirecting = false;
      });
  }

  resetPassword(e) {
    e.preventDefault();
    this.isLoginSuccess = false;
    this.isResetSuceess = false;
    this.resetForm.reset();
    this.resetPwd = !this.resetPwd;
  }


  logout() {
    this.commonService.deleteAllCookies();
  }
}
