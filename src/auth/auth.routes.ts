import {LoginComponent} from './login/login.component';
import {AuthComponent} from './auth.component';
import {PasswordResetRequestComponent} from './password.reset/password-reset.component';

export const AuthRoutes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      // { path: '**', redirectTo: 'login', pathMatch: 'full' },
      {path: '', redirectTo: 'login', pathMatch: 'full'},
      {path: 'login', component: LoginComponent},
      {path: 'completepasswordreset', component: PasswordResetRequestComponent}
    ],
  },
];

