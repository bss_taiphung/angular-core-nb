import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {SharedModule} from '../app/shared/index'
import {AuthenticationService} from '../app/shared/services/auth.service'

import {AuthComponent} from './auth.component';
import {LoginComponent} from './login/login.component';
import {AuthRoutes as routes} from './auth.routes';
import {PasswordResetRequestComponent} from './password.reset/password-reset.component';

import {ProjectSettingsService} from '../app/project/shared/services/projectsettings.service';
import {PasswordResetRequestService} from './password.reset/service/password-reset.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AuthComponent, LoginComponent, PasswordResetRequestComponent],
  providers: [
    AuthenticationService,
    ProjectSettingsService,
    PasswordResetRequestService
  ]
})

export class AuthModule {
}
