import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';
import {ActivatedRoute, Router} from '@angular/router';
import {PasswordResetRequestService} from './service/password-reset.service';
import {AuthenticationService} from '../../app/shared/services/auth.service';


@Component({
  selector: 'password-reset-request',
  styleUrls: ['./css/password-reset.component.css'],
  templateUrl: './html/password-reset.component.html'
})
export class PasswordResetRequestComponent implements OnInit {
  pageRedirecting = false;
  resetPwd: boolean = false;

  editForm: FormGroup = this.formBuilder.group({
    newPassword: ['', Validators.required],
    confirmPassword: ['', Validators.required]
  }, {validator: this.checkIfMatchingPasswords('newPassword', 'confirmPassword')});

  guid = '';
  errormessage = '';

  constructor(private passwordResetRequestService: PasswordResetRequestService,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private router: Router,
              private authenticationService: AuthenticationService) {

  }

  ngOnInit() {
    this.guid = this.route.snapshot.queryParams['guid'];
    if (this.guid === undefined || this.guid === '') {
      this.router.navigate(['/auth/login']);
    }
  }

  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      const passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({notEquivalent: true});
      }
      else {
        return passwordConfirmationInput.setErrors(null);
      }
    }
  }

  public saveHandler(e) {
    const data: any = {'linkGUID': this.guid, 'newPassword': this.editForm.value.newPassword}
    if (this.editForm.value !== '') {
      this.errormessage = '';
      this.authenticationService.completePasswordResetRequest(data,
        response => {
          if (response._body === 'false') {
            this.errormessage = 'link expire or invalid';
          } else {
            this.errormessage = 'Password successfully updated!!';
            setTimeout(() => {
              this.router.navigate(['/auth/login']);
            }, 5000);
          }
          this.editForm.reset();
        },
        error => {
          this.errormessage = JSON.parse(error._body).exceptionMessage;
          this.editForm.reset();
        }
      );
    } else {
      this.errormessage = 'New Password is required';
      this.editForm.reset();
    }
  }
}

