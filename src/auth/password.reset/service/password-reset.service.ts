import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

import {CommonService} from 'app/shared/services/common.service';
import {environment} from '../../../environments/environment';

@Injectable()
export class PasswordResetRequestService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private http: HttpClient,
              private commonService: CommonService) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/general/api/User/';
  }


  update(data: any): any {
    const url = this.apiBaseUrl + 'CompletePasswordResetRequest';

    return this.http.post(url, data)
  }
}

