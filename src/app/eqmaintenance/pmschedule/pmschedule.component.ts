import {
  Component,
  HostListener,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {Observable} from 'rxjs/Observable';
import {
  SortDescriptor,
  State
} from '@progress/kendo-data-query';
import {
  DataStateChangeEvent,
  GridDataResult,
  PageChangeEvent
} from '@progress/kendo-angular-grid';
import {FormGroup} from '@angular/forms';
import {PMScheduleService} from './service/pmschedule.service'
import {
  CreatePMScheduleDto,
  IPMScheduleDto,
  PMScheduleDto
} from './service/pmschedule.model';
import {HubConnection} from '@aspnet/signalr-client';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {CommonService} from '../../shared/services/common.service';
import {OrderService} from '../order/service/order.service';
import {
  EqmSettings,
  User,
  UserRole
} from '../../shared/models/eqmSettings'
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {SelectListDto} from '../../shared/models/selectListDto'
import {UserSettingsDto} from '../../shared/usersettings/usersetting.model';
import {UserSettingsService} from '../../shared/usersettings/usersetting.service';

import {environment} from '../../../environments/environment';

@Component({
  selector: 'pmschedule',
  templateUrl: './html/pmschedule.component.html',
  styleUrls: ['./css/pmschedule.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class PMScheduleComponent implements OnInit {

  data: IPMScheduleDto[];
  public formGroup: FormGroup;
  public view: Observable<GridDataResult>;
  private editedRowIndex: number;
  public editDataItem: CreatePMScheduleDto;
  public isNew: boolean;
  public pageSize = 10;
  public skip = 0;
  public state: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };
  public gridView: GridDataResult;
  public deleteDataItem: any;
  private sort: SortDescriptor[] = [];
  public isactive: boolean = true;
  public searchtext: string = '';
  private hubConnection: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  eqmSettings: EqmSettings = new EqmSettings();
  user: User = new User();
  userRole: UserRole = new UserRole();
  public siteList: SelectListDto[];
  public mss: any;

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private pmscheduleService: PMScheduleService,
              private userSettingsService: UserSettingsService,
              private eqmSettingsService: EqmSettingsService,
              private orderService: OrderService) {
    this.mss = this.commonService.getCookies('Mss');
    this.eqmSettings = this.eqmSettingsService.getEqmSettings();
    this.user = this.eqmSettingsService.getUser();
    this.userRole = this.eqmSettingsService.getUserRoles();
    this.commonService.validateLogin();
  }

  ngOnInit() {
  }

  signalRConnection() {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');

        this.hubConnection
          .invoke('Subscribe', 'EquipmentMaintenance', 'PMSchedule', 'SiteId', this.eqmSettingsService.getEqmSettings().currentSiteId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
      const _responseText = messageJson;
      let result200: any = null;
      let data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      let resultData200 = this.commonService.toCamel(data);
      result200 = resultData200 ? PMScheduleDto.fromJS(resultData200) : new PMScheduleDto();

      // patch for specialcase Ids
      result200.pmScheduleId = resultData200.pMScheduleId;
      result200.pmInactiveReasonId = resultData200.pMInactiveReasonId;

      var dataExist = this.containsObject(resultData200, this.gridView.data);

      if (operation.toLowerCase() === 'insert' && !dataExist) {
        this.gridView.data.unshift(result200);
      }

      if (operation.toLowerCase() === 'update') {
        this.gridView.data.forEach((element, index) => {
          if (element.pmScheduleId === resultData200.pMScheduleId) {
            //
            if (this.isactive) {
              if (result200.active == this.isactive) {
                this.gridView.data[index] = result200;
              } else {
                operation = 'delete';
              }
            } else {
              this.gridView.data[index] = result200;
            }
          }
        });
      }

      if (operation.toLowerCase() === 'delete' && dataExist) {
        var index = null;
        this.gridView.data.forEach((element, i) => {
          if (element.pmScheduleId === resultData200.pMScheduleId) {
            index = i;
          }
        });
        if (index !== null) {
          this.gridView.data.splice(index, 1);
        }
      }
    });
  }

  containsObject(obj, list) {
    var x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].pmScheduleId === obj.pMScheduleId) {
        return true;
      }
    }

    return false;
  }

  // Dialog box
  opened: boolean = false;
  loading: boolean = true;
  gridLoading: boolean = true;

  public close(status) {
    var $this = this;
    if (status) {
      this.gridLoading = true;
      this.pmscheduleService.delete($this.deleteDataItem.pmScheduleId,
        response => {
          this.toasterService.success('', 'PM Schedule Removed Successfully');
          this.gridLoading = false;
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    $this.deleteDataItem = null;
    $this.opened = false;
  }

  public open() {
    this.opened = true;
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.loadGridData();
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
  }

  private loadGridData(): void {
    this.pmscheduleService.getAll(this.pageSize, this.skip, this.sort, this.isactive, this.searchtext).subscribe(eventResult => {
      this.gridView = {
        data: eventResult.results,
        total: eventResult.count
      };
      this.loading = false;
      this.gridLoading = false;
    });
  }

  public addHandler({sender}) {
    if (!this.eqmSettings.canEditSchedules) {
      this.toasterService.error('You do not have permission to add new PM Schedule.', '');
      return false;
    } else {
      this.closeEditor(sender);
      this.editDataItem = new CreatePMScheduleDto();
      this.isNew = true;
    }
  }

  public editHandler({dataItem}) {

    this.editDataItem = dataItem;
    this.editDataItem.startDate = new Date(this.editDataItem.startDate);
    this.isNew = false;

  }

  public saveHandler(data) {
    this.gridLoading = true;
    if (this.isNew) {
      data.pmScheduleId = 0;
      data.pmInactiveReasonId = null;
      data.active = (data.active == null) ? false : data.active;
      const inputData: CreatePMScheduleDto = data;
      this.pmscheduleService.create(inputData,
        response => {
          this.toasterService.success('', 'PM Schedule Saved Successfully');
          this.gridLoading = false;
        },
        error => {
          this.toasterService.errorMessage(error);
          this.gridLoading = false;
        }
      );
    }
    else {
      if (data.active) {
        data.pmInactiveReasonId = null;
      }
      const inputData: PMScheduleDto = data;
      this.pmscheduleService.update(inputData,
        response => {
          this.toasterService.success('', 'PM Schedule Updated Successfully');
          this.gridLoading = false;
        },
        error => {
          this.toasterService.errorMessage(error);
          this.gridLoading = false;
        }
      );
    }
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public cancelHandler() {
    this.editDataItem = undefined;
    this.formGroup = undefined;
  }

  public removeHandler({dataItem}) {
    this.deleteDataItem = dataItem;
    this.open();
  }

  public showActive() {
    this.isactive = !this.isactive;
    this.gridLoading = true;

    this.doSearch(this.state, this);
  }

  public onSearchKeyup() {
    this.skip = 0;
    this.pageSize = 10;
    this.gridLoading = true;
    this.loadGridData();
  }

  // Order Popup
  public ordersearchtext: string = '';
  public orderDialog: boolean = false;
  public ordergridView: GridDataResult;
  public pmScheduleId = 0;
  public orderpageSize = 10;
  public orderskip = 0;
  private ordersort: SortDescriptor[] = [];

  public openOrderDialog(pmschedule) {
    this.orderDialog = true;
    this.pmScheduleId = pmschedule.pmScheduleId;
    //this.equipmentName = equipment.name;
    this.loadOrderGridData();
  }

  public closeOrderDialog() {
    this.orderDialog = false;
  }

  public onorderSearchKeyup() {
    this.orderskip = 0;
    this.orderpageSize = 10;
    this.loadOrderGridData();
  }

  public ordersortChange(sort: SortDescriptor[]): void {
    this.ordersort = sort;
    this.loadOrderGridData();
  }

  protected orderpageChange({skip, take}: PageChangeEvent): void {
    this.orderskip = skip;
    this.orderpageSize = take;
    this.loadOrderGridData();
  }

  private loadOrderGridData(): void {
    this.orderService.getAll(this.orderpageSize, this.orderskip, this.ordersort, false, this.ordersearchtext, this.pmScheduleId).subscribe(eventResult => {
      if (eventResult.results.length <= 0) {
        eventResult.count = 0;
      }
      this.ordergridView = {
        data: eventResult.results,
        total: eventResult.count
      };

    });
  }

  public ActionData: Array<any> = [{
    text: 'Related Orders',
    icon: 'preview'
  }, {
    text: 'Delete',
    icon: 'trash'
  }];

  public onAction(e, dataItem) {

    if (!this.eqmSettings.canEditSchedules) {
      this.toasterService.error('You do not have permission to update or delete PM Schedule.', '');
      return false;
    } else {
      if (e === undefined) {
        this.editDataItem = dataItem;
        this.editDataItem.startDate = new Date(this.editDataItem.startDate);
        this.isNew = false;
      } else {
        if (e.text == 'Delete') {
          this.deleteDataItem = dataItem;
          this.open();
        } else if (e.text == 'Related Orders') {
          this.openOrderDialog(dataItem);
        }
      }
    }
  }

  timeout = null;

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridLoading = true;

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.doSearch(state, this);
    }, 1000);
  }

  doSearch(state: any, that) {
    if (state.filter) {
      var search = that.commonService.getFilter(state.filter.filters, that.isactive);

      that.pmscheduleService.getAllWtihFilter(state.take, state.skip, that.sort, search)
        .subscribe(eventResult => {
          that.gridView = {
            data: eventResult.results,
            total: eventResult.count
          };
          that.loading = false;
          that.gridLoading = false;
        });

      this.skip = state.skip;
      this.pageSize = state.take;
    }
    else {
      that.loadGridData();
    }
  }

  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {
      this.cancelHandler();
      this.close(false);
      this.closeOrderDialog();
    }
  }

  public siteSelectionChange(e): void {
    this.gridLoading = true;
    this.signalRConnection();
    this.loadGridData();
  }
}
