﻿import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';


// region PMScheduleDto

export interface IPMScheduleDto {
  equipmentId: number;
  equipmentName: string;
  orderTypeName: string;
  priorityDescription: string;
  assignToName: string;
  orderTypeId: number;
  scheduleTypeId: number;
  scheduleTypeName: string;
  priorityId: number;
  assignTo: number;
  pmInactiveReasonId: number;
  pmInactiveReason: string;
  pmScheduleId: number;
  startDate: string;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  dueDays: number;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
}

export class PMScheduleDto implements IPMScheduleDto {
  equipmentId: number;
  equipmentName: string;
  orderTypeName: string;
  priorityDescription: string;
  assignToName: string;
  orderTypeId: number;
  scheduleTypeId: number;
  scheduleTypeName: string;
  priorityId: number;
  assignTo: number;
  pmInactiveReasonId: number;
  pmInactiveReason: string;
  pmScheduleId: number;
  startDate: string;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  dueDays: number;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;

  static fromJS(data: any): PMScheduleDto {
    const result = new PMScheduleDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPMScheduleDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.equipmentId = data['equipmentId'];
      this.orderTypeId = data['orderTypeId'];
      this.priorityId = data['priorityId'];
      this.assignTo = data['assignTo'];
      this.equipmentName = data['equipmentName'];
      this.orderTypeName = data['orderTypeName'];
      this.priorityDescription = data['priorityDescription'];
      this.assignToName = data['assignToName'];
      this.pmInactiveReasonId = data['pmInactiveReasonId'];
      this.pmInactiveReason = data['pmInactiveReason'];
      this.pmScheduleId = data['pmScheduleId'];
      this.scheduleTypeId = data['scheduleTypeId'];
      this.scheduleTypeName = data['scheduleTypeName'];
      this.startDate = data['startDate'];
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.dueDays = data['dueDays'];
      this.siteId = data['siteId'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.active = data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['equipmentId'] = this.equipmentId;
    data['equipmentName'] = this.equipmentName;
    data['orderTypeName'] = this.orderTypeName;
    data['priorityDescription'] = this.priorityDescription;
    data['assignToName'] = this.assignToName;
    data['orderTypeId'] = this.orderTypeId;
    data['priorityId'] = this.priorityId;
    data['assignTo'] = this.assignTo;
    data['pmInactiveReasonId'] = this.pmInactiveReasonId;
    data['pmInactiveReason'] = this.pmInactiveReason;
    data['pmScheduleId'] = this.pmScheduleId;
    data['scheduleTypeId'] = this.scheduleTypeId;
    data['scheduleTypeName'] = this.scheduleTypeName;
    data['createdByName'] = this.createdByName;
    data['startDate'] = this.startDate;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['dueDays'] = this.dueDays;
    data['siteId'] = this.siteId;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;

    return data;
  }
}

// endregion

// region CreatePMScheduleDto

export interface ICreatePMScheduleDto {
  pmScheduleId: number;
  equipmentId: number;
  orderTypeId: number;
  scheduleTypeId: number;
  priorityId: number;
  assignTo: number;
  pmInactiveReasonId: number;
  dueDays: number;
  startDate: Date;
  active: boolean;
}

export class CreatePMScheduleDto implements ICreatePMScheduleDto {
  pmScheduleId: number;
  equipmentId: number;
  orderTypeId: number;
  scheduleTypeId: number;
  priorityId: number;
  assignTo: number;
  pmInactiveReasonId: number;
  startDate: Date;
  dueDays: number;
  active = true;

  static fromJS(data: any): PMScheduleDto {
    const result = new PMScheduleDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPMScheduleDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.pmScheduleId = 0;
      this.equipmentId = data['equipmentId'];
      this.orderTypeId = data['orderTypeId'];
      this.scheduleTypeId = data['scheduleTypeId'];
      this.priorityId = data['priorityId'];
      this.assignTo = data['assignTo'];
      this.pmInactiveReasonId = data['pmInactiveReasonId'];
      this.startDate = data['startDate'];
      this.dueDays = data['dueDays'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['pmScheduleId'] = 0;
    data['equipmentId'] = this.equipmentId;
    data['orderTypeId'] = this.orderTypeId;
    data['scheduleTypeId'] = this.scheduleTypeId;
    data['priorityId'] = this.priorityId;
    data['assignTo'] = this.assignTo;
    data['pmInactiveReasonId'] = this.pmInactiveReasonId;
    data['startDate'] = this.startDate;
    data['dueDays'] = this.dueDays;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

//endregion

// region PagedResultDtoOfPMScheduleDto

export interface IPagedResultDtoOfPMScheduleDto {
  results: PMScheduleDto[];
  count: number;
}

export class PagedResultDtoOfPMScheduleDto implements IPagedResultDtoOfPMScheduleDto {
  results: PMScheduleDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfPMScheduleDto {
    const result = new PagedResultDtoOfPMScheduleDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfPMScheduleDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(PMScheduleDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion
