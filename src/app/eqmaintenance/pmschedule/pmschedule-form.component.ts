import {
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';


import {CommonService} from '../../shared/services/common.service';
import {SelectListDto} from '../../shared/models/selectListDto'
import {PMScheduleDto} from './service/pmschedule.model';


@Component({
  selector: 'kendo-grid-pmschedule-form',
  styles: [
    'input[type=text] { width: 100%; }'
  ],
  templateUrl: './html/pmschedule-form.component.html'
})
export class PMScheduleFormComponent {
  public active = false;
  public editForm: FormGroup = new FormGroup({
    'pmScheduleId': new FormControl(),
    'equipmentId': new FormControl('', Validators.required),
    'orderTypeId': new FormControl('', Validators.required),
    'scheduleTypeId': new FormControl('', Validators.required),
    'priorityId': new FormControl('', Validators.required),
    'assignTo': new FormControl('', Validators.required),
    'startDate': new FormControl('', Validators.required),
    'dueDays': new FormControl('', Validators.required),
    'pmInactiveReasonId': new FormControl(''),
    'active': new FormControl(false),
  });

  public equipmentList: SelectListDto[];
  public filterEquipmentList: SelectListDto[];

  public scheduleList: SelectListDto[];
  public filterScheduleList: SelectListDto[];

  public orderTypeList: SelectListDto[];
  public filterOrderTypeList: SelectListDto[];

  public priorityList: SelectListDto[];
  public filterPriorityList: SelectListDto[];

  public assignedToList: SelectListDto[];
  public filterAssignedToList: SelectListDto[];

  public pmInactiveReasonList: SelectListDto[];
  public filterpmInactiveReasonList: SelectListDto[];

  public pmInResoneRequired: boolean = false;

  constructor(private commonService: CommonService) {

  }

  @Input() public isNew = false;

  @Input()
  public set model(pmschedule: PMScheduleDto) {
    this.editForm.reset(pmschedule);
    this.active = pmschedule !== undefined;

    if (this.active) {
      this.commonService.getAll('equipmentmaintenance', 'Equipment').subscribe(eventResult => {
        this.filterEquipmentList = this.equipmentList = eventResult.results;
      });
      this.commonService.getAll('equipmentmaintenance', 'ScheduleType').subscribe(eventResult => {
        this.filterScheduleList = this.scheduleList = eventResult.results;
      });
      this.commonService.getAll('equipmentmaintenance', 'OrderType').subscribe(eventResult => {
        this.filterOrderTypeList = this.orderTypeList = eventResult.results;
      });
      this.commonService.getAll('equipmentmaintenance', 'Priority').subscribe(eventResult => {
        this.filterPriorityList = this.priorityList = eventResult.results;
      });
      this.commonService.getAll('general', 'User').subscribe(eventResult => {
        this.filterAssignedToList = this.assignedToList = eventResult.results;
      });
      this.commonService.getAll('equipmentmaintenance', 'PmInactiveReason').subscribe(eventResult => {
        this.filterpmInactiveReasonList = this.pmInactiveReasonList = eventResult.results;
      });
    }
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<PMScheduleDto> = new EventEmitter<PMScheduleDto>();

  public pmInactiveReasonHandler(value) {
    if (this.editForm.controls.pmInactiveReasonId.value == 0) {
      this.pmInResoneRequired = true;
    } else {
      this.pmInResoneRequired = false;
    }
  }

  public onSave(e): void {
    e.preventDefault();
    if (!this.editForm.controls.active.value) {
      if (this.editForm.controls.pmInactiveReasonId.value == 0) {
        this.pmInResoneRequired = true;
        return;
      }
    }
    this.save.emit(this.editForm.value);
    this.active = false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.active = false;
    this.cancel.emit();
  }

  private EquipmentListHandleFilter(value) {
    this.filterEquipmentList = this.equipmentList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private ScheduleListHandleFilter(value) {
    this.filterScheduleList = this.scheduleList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private OrderTypeListHandleFilter(value) {
    this.filterOrderTypeList = this.orderTypeList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private PriorityListHandleFilter(value) {
    this.filterPriorityList = this.priorityList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private AssignedToListHandleFilter(value) {
    this.filterAssignedToList = this.assignedToList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private pmInactiveReasonListHandleFilter(value) {
    this.filterpmInactiveReasonList = this.pmInactiveReasonList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
}
