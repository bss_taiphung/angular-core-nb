import {
  Component,
  ViewEncapsulation,
  OnInit,
  HostListener
} from '@angular/core';
import {FormGroup} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';
import {Observable} from 'rxjs/Observable';

import {HubConnection} from '@aspnet/signalr-client';
import {State} from '@progress/kendo-data-query';
import {PageChangeEvent, GridDataResult, DataStateChangeEvent} from '@progress/kendo-angular-grid';
import {FailureTypeService} from './service/failuretype.service'
import {SortDescriptor} from '@progress/kendo-data-query';

import {IFailureTypeDto, CreateFailureTypeDto, FailureTypeDto} from './service/failuretype.model';

import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';

import {EqmSettings, User, UserRole} from '../../shared/models/eqmSettings'
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {SelectListDto} from '../../shared/models/selectListDto'
import {UserSettingsService} from '../../shared/usersettings/usersetting.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'failuretype',
  templateUrl: './html/failuretype.component.html',
  styleUrls: ['./css/failuretype.component.css'],
  encapsulation: ViewEncapsulation.None
})


export class FailureTypeComponent implements OnInit {

  data: IFailureTypeDto[];
  public formGroup: FormGroup;
  public view: Observable<GridDataResult>;
  private editedRowIndex: number;
  public editDataItem: CreateFailureTypeDto;
  public isNew: boolean;
  public pageSize = 10;
  public skip = 0;
  public state: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };
  public gridView: GridDataResult;
  public deleteDataItem: any;
  private sort: SortDescriptor[] = [];
  public isactive: boolean = true;
  public searchtext: string = '';
  private hubConnection: HubConnection;
  public logMessage: string = '';
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  eqmSettings: EqmSettings = new EqmSettings();
  user: User = new User();
  userRole: UserRole = new UserRole();
  public siteList: SelectListDto[];
  public mss: any;

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private userSettingsService: UserSettingsService,
              private failuretypeService: FailureTypeService) {
    this.mss = this.commonService.getCookies('Mss');
    this.eqmSettings = this.eqmSettingsService.getEqmSettings();
    this.user = this.eqmSettingsService.getUser();
    this.userRole = this.eqmSettingsService.getUserRoles();
  }

  ngOnInit() {
  }

  signalRConnection() {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');

        this.hubConnection
          .invoke('Subscribe', 'EquipmentMaintenance', 'FailureType', 'SiteId', this.eqmSettingsService.getEqmSettings().currentSiteId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
      const _responseText = messageJson;
      let result200: any = null;
      let data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      let resultData200 = this.commonService.toCamel(data);
      result200 = resultData200 ? FailureTypeDto.fromJS(resultData200) : new FailureTypeDto();

      var dataExist = this.containsObject(result200, this.gridView.data);

      if (operation.toLowerCase() === 'insert' && !dataExist) {
        this.gridView.data.unshift(result200);
      }

      if (operation.toLowerCase() === 'update') {
        this.gridView.data.forEach((element, index) => {
          if (element.failureTypeId === result200.failureTypeId) {
            if (this.isactive) {
              if (result200.active == this.isactive) {
                this.gridView.data[index] = result200;
              } else {
                operation = 'delete';
              }
            } else {
              this.gridView.data[index] = result200;
            }
          }
        });
      }

      if (operation.toLowerCase() === 'delete' && dataExist) {
        var index = null;
        this.gridView.data.forEach((element, i) => {
          if (element.failureTypeId === result200.failureTypeId) {
            index = i;
          }
        });
        if (index !== null) {
          this.gridView.data.splice(index, 1);
        }
      }
    });
  }

  containsObject(obj, list) {
    var x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].failureTypeId === obj.failureTypeId) {
        return true;
      }
    }

    return false;
  }

  // Dialog box
  opened: boolean = false;
  loading: boolean = true;
  gridLoading: boolean = true;

  public close(status) {
    var $this = this;
    if (status) {
      this.gridLoading = true;
      this.failuretypeService.delete($this.deleteDataItem.failureTypeId,
        response => {
          this.toasterService.success('', 'Failure Type Removed Successfully');
          this.gridLoading = false;
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    $this.deleteDataItem = null;
    $this.opened = false;
  }

  public open() {
    this.opened = true;
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.loadGridData();
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
  }

  private loadGridData(): void {
    this.failuretypeService.getAll(this.pageSize, this.skip, this.sort, this.isactive, this.searchtext).subscribe(eventResult => {
      this.gridView = {
        data: eventResult.results,
        total: eventResult.count
      };
      this.loading = false;
      this.gridLoading = false;
    });
  }

  public addHandler({sender}) {
    if (!this.eqmSettings.canEditMasterData) {
      this.toasterService.error('You do not have permission to add new failure type.', '');
      return false;
    } else {
      this.closeEditor(sender);
      this.editDataItem = new CreateFailureTypeDto();
      this.isNew = true;
    }
  }

  public editHandler({dataItem}) {
    this.editDataItem = dataItem;
    this.isNew = false;
  }

  public saveHandler(failuredata) {
    this.gridLoading = true;
    if (this.isNew) {
      failuredata.active = (failuredata.active == null) ? false : failuredata.active;
      const inputFailureData: CreateFailureTypeDto = failuredata;
      this.failuretypeService.create(inputFailureData,
        response => {
          this.toasterService.success('', 'Failure Type Saved Successfully');
          this.gridLoading = false;
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    else {
      const inputFailureData: FailureTypeDto = failuredata;
      this.failuretypeService.update(inputFailureData,
        response => {
          this.toasterService.success('', 'Failure Type Updated Successfully');
          this.gridLoading = false;
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public cancelHandler() {
    this.editDataItem = undefined;
    this.formGroup = undefined;
  }

  public removeHandler({dataItem}) {
    this.deleteDataItem = dataItem;
    this.open();
  }

  public onSearchKeyup() {
    this.skip = 0;
    this.pageSize = 10;
    this.gridLoading = true;
    this.loadGridData();
  }

  public ActionData: Array<any> = [{
    text: 'Delete',
    icon: 'trash'
  }];

  public onAction(e, dataItem) {
    if (e === undefined) {
      if (!this.eqmSettings.canEditMasterData) {
        this.toasterService.error('You do not have permission to update failure type.', '');
        return false;
      } else {
        this.editDataItem = dataItem;
        this.isNew = false;
      }
    } else {
      if (e.text == 'Delete') {
        if (!this.eqmSettings.canEditMasterData) {
          this.toasterService.error('You do not have permission to delete failure type.', '');
          return false;
        } else {
          this.deleteDataItem = dataItem;
          this.open();
        }
      }
    }
  }

  public showActive() {
    this.isactive = !this.isactive;
    this.gridLoading = true;

    this.doSearch(this.state, this);
  }

  timeout = null;

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridLoading = true;

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.doSearch(state, this);
    }, 1000);
  }

  doSearch(state: any, that) {
    if (state.filter) {
      var search = that.commonService.getFilter(state.filter.filters, that.isactive);

      that.failuretypeService.getAllWtihFilter(state.take, state.skip, that.sort, search)
        .subscribe(eventResult => {
          that.gridView = {
            data: eventResult.results,
            total: eventResult.count
          };
          that.loading = false;
          that.gridLoading = false;
        });

      this.skip = state.skip;
      this.pageSize = state.take;
    }
    else {
      that.loadGridData();
    }
  }

  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {
      this.cancelHandler();
      this.close(false);
    }
  }

  public siteSelectionChange(e): void {
    this.gridLoading = true;
    this.signalRConnection();
    this.loadGridData();
  }
}
