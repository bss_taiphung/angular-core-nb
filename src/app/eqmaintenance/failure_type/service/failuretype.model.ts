﻿import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';

// region FailureTypeDto

export interface IFailureTypeDto {
  failureTypeName: string;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  failureTypeId: number;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
}

export class FailureTypeDto implements IFailureTypeDto {
  failureTypeName: string;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  failureTypeId: number;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;

  static fromJS(data: any): FailureTypeDto {
    const result = new FailureTypeDto();
    result.init(data);
    return result;
  }

  constructor(data?: IFailureTypeDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.failureTypeName = data['failureTypeName'];
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.failureTypeId = data['failureTypeId'];
      this.siteId = data['siteId'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.active = data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['failureTypeName'] = this.failureTypeName;
    data['createdByName'] = this.createdByName;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['failureTypeId'] = this.failureTypeId;
    data['siteId'] = this.siteId;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;

    return data;
  }
}

// endregion

// region CreateFailureTypeDto

export interface ICreateFailureTypeDto {
  failureTypeName: string;
  active: boolean;
}

export class CreateFailureTypeDto implements ICreateFailureTypeDto {
  failureTypeId = 0;
  failureTypeName: string;
  active = true;

  static fromJS(data: any): FailureTypeDto {
    const result = new FailureTypeDto();
    result.init(data);
    return result;
  }

  constructor(data?: IFailureTypeDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.failureTypeId = 0;
      this.failureTypeName = data['failureTypeName'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['failureTypeId'] = 0;
    data['failureTypeName'] = this.failureTypeName;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

// endregion

// region PagedResultDtoOfFailureTypeDto

export interface IPagedResultDtoOfFailureTypeDto {
  results: FailureTypeDto[];
  count: number;
}

export class PagedResultDtoOfFailureTypeDto implements IPagedResultDtoOfFailureTypeDto {
  results: FailureTypeDto[];
  count: number;

  constructor(data?: IPagedResultDtoOfFailureTypeDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(FailureTypeDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  static fromJS(data: any): PagedResultDtoOfFailureTypeDto {
    let result = new PagedResultDtoOfFailureTypeDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion
