import {
  Component,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import {
  Validators,
  FormGroup, FormControl
} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';
import {FailureTypeDto} from './service/failuretype.model';

@Component({
  selector: 'kendo-grid-failuretype-form',
  styles: [
    'input[type=text] { width: 100%; }'
  ],
  templateUrl: './html/failuretype-form.component.html'
})
export class FailureTypeFormComponent {
  public active = false;
  public editForm: FormGroup = new FormGroup({
    'failureTypeId': new FormControl(),
    'failureTypeName': new FormControl('', Validators.required),
    'active': new FormControl(false),
  });

  @Input() public isNew = false;

  @Input()
  public set model(failureType: FailureTypeDto) {
    console.log(failureType);
    this.editForm.reset(failureType);
    this.active = failureType !== undefined;
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<FailureTypeDto> = new EventEmitter();

  public onSave(e): void {

    e.preventDefault();
    if (this.active == null) {
      this.active = false;
    }
    this.save.emit(this.editForm.value);
    this.active = false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.active = false;
    this.cancel.emit();
  }
}
