import {
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';
import {OrderTypeDto} from './service/ordertype.model';


@Component({
  selector: 'kendo-grid-ordertype-form',
  styles: [
    'input[type=text] { width: 100%; }'
  ],
  templateUrl: './html/ordertype-form.component.html'
})
export class OrderTypeFormComponent {
  public active = false;
  public editForm: FormGroup = new FormGroup({
    'orderTypeId': new FormControl(),
    'orderTypeName': new FormControl('', Validators.required),
    'active': new FormControl(false),
  });

  @Input() public isNew = false;

  @Input()
  public set model(ordertype: OrderTypeDto) {
    this.editForm.reset(ordertype);
    this.active = ordertype !== undefined;
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<OrderTypeDto> = new EventEmitter();

  public onSave(e): void {
    e.preventDefault();
    this.save.emit(this.editForm.value);
    this.active = false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.active = false;
    this.cancel.emit();
  }
}
