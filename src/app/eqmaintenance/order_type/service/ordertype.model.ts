﻿import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';

import 'rxjs/add/operator/catch';

// region OrderTypeDto
export interface IOrderTypeDto {
  orderTypeName: string;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  orderTypeId: number;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
}

export class OrderTypeDto implements IOrderTypeDto {
  orderTypeName: string;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  orderTypeId: number;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;

  static fromJS(data: any): OrderTypeDto {
    const result = new OrderTypeDto();
    result.init(data);
    return result;
  }

  constructor(data?: IOrderTypeDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.orderTypeName = data['orderTypeName'];
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.orderTypeId = data['orderTypeId'];
      this.siteId = data['siteId'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.active = data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['orderTypeName'] = this.orderTypeName;
    data['createdByName'] = this.createdByName;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['orderTypeId'] = this.orderTypeId;
    data['siteId'] = this.siteId;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;

    return data;
  }
}

// endregion

// region CreateOrderTypeDto

export interface ICreateOrderTypeDto {
  orderTypeName: string;
  active: boolean;
}

export class CreateOrderTypeDto implements ICreateOrderTypeDto {
  orderTypeId = 0;
  orderTypeName: string;
  active = true;

  static fromJS(data: any): OrderTypeDto {
    const result = new OrderTypeDto();
    result.init(data);
    return result;
  }

  constructor(data?: IOrderTypeDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.orderTypeId = 0;
      this.orderTypeName = data['orderTypeName'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['orderTypeId'] = 0;
    data['orderTypeName'] = this.orderTypeName;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

// endregion

// region PagedResultDtoOfOrderTypeDto

export interface IPagedResultDtoOfOrderTypeDto {
  results: OrderTypeDto[];
  count: number;
}

export class PagedResultDtoOfOrderTypeDto implements IPagedResultDtoOfOrderTypeDto {
  results: OrderTypeDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfOrderTypeDto {
    const result = new PagedResultDtoOfOrderTypeDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfOrderTypeDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(OrderTypeDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion
