import {Component} from '@angular/core';

@Component({
  selector: 'app-eqmaintenance',
  template: '<router-outlet></router-outlet>',

})
export class EqmaintenanceComponent {

}
