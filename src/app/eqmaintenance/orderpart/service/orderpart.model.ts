import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';

// region OrderPartDto

export interface IOrderPartDto {
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
  orderPartId: number;
  orderId: number;
  partId: number;
  consumedQuantity: number;
  partName: string;
  partUserAvatar: string;
}

export class OrderPartDto implements IOrderPartDto {
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
  orderPartId: number;
  orderId: number;
  partId: number;
  consumedQuantity: number;
  partName: string;
  partUserAvatar = '';

  static fromJS(data: any): OrderPartDto {
    const result = new OrderPartDto();
    result.init(data);
    return result;
  }

  constructor(data?: IOrderPartDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.active = data['active'];
      this.orderPartId = data['orderPartId'];
      this.orderId = data['orderId'];
      this.partId = data['partId'];
      this.consumedQuantity = data['consumedQuantity'];
      this.partName = data['partName'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['createdByName'] = this.createdByName;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;
    data['orderPartId'] = this.orderPartId;
    data['orderId'] = this.orderId;
    data['partId'] = this.partId;
    data['consumedQuantity'] = this.consumedQuantity;
    data['partName'] = this.partName;

    return data;
  }
}

// endregion

// region PagedResultDtoOfOrderPartDto
export interface IPagedResultDtoOfOrderPartDto {
  results: OrderPartDto[];
  count: number;
}

export class PagedResultDtoOfOrderPartDto implements IPagedResultDtoOfOrderPartDto {
  results: OrderPartDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfOrderPartDto {
    const result = new PagedResultDtoOfOrderPartDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfOrderPartDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(OrderPartDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion
