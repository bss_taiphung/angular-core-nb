import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';

import {
  OrderPartDto,
  PagedResultDtoOfOrderPartDto
} from './orderpart.model'
import {CommonService} from '../../../shared/services/common.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class OrderPartService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private http: HttpClient,
              private commonService: CommonService) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/equipmentmaintenance/api/OrderPart/';
  }

  get(input: OrderPartDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'GetById';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  create(input: OrderPartDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'InsertAsync';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  update(input: OrderPartDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'UpdateAsync';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  delete(id: any, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'DeleteAsync';

    const params = new HttpParams().set('id', id);

    const options = {
      params: params
    };

    this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
  }

  getAll(take: number,
         skip: number,
         sort: any,
         isactive: boolean,
         searchtext: string,
         orderId: number): Observable<PagedResultDtoOfOrderPartDto> {
    const url = this.apiBaseUrl + 'GetPagedQuery';
    // Order by Clause
    let orderBy = '';
    if (sort !== undefined && sort.length > 0) {
      for (const item of sort) {
        orderBy = this.commonService.firstUpper(item.field) + ' ' + (item.dir == undefined ? 'asc' : item.dir);
      }
    } else {
      orderBy = 'OrderPartId desc';
    }

    let filter = '';
    if (isactive) {
      filter = 'Active eq true';
    }
    if (searchtext.length > 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += `ConsumedQuantity eq  ${searchtext} `;
    }

    if (orderId) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += 'OrderId eq ' + orderId + '';
    }

    const params = new HttpParams()
      .set('$top', take.toString())
      .set('$skip', skip.toString())
      .set('$count', 'true')
      .set('$orderby', orderBy.toString())
      .set('$filter', filter.toString());

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      const result200 = PagedResultDtoOfOrderPartDto.fromJS(response);
      return Observable.of(result200);
    });
  }
}

