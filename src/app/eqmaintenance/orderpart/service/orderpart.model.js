"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("rxjs/add/operator/map");
require("rxjs/add/operator/mergeMap");
require("rxjs/add/observable/fromPromise");
require("rxjs/add/observable/of");
require("rxjs/add/observable/throw");
require("rxjs/add/operator/map");
require("rxjs/add/operator/toPromise");
require("rxjs/add/operator/mergeMap");
require("rxjs/add/operator/catch");
var OrderPartDto = /** @class */ (function () {
    function OrderPartDto(data) {
        this.partUserAvatar = "";
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    this[property] = data[property];
            }
        }
    }
    OrderPartDto.prototype.init = function (data) {
        if (data) {
            this.createdByName = data["createdByName"];
            this.createdDateTime = data["createdDateTime"];
            this.createdDate = data["createdDate"];
            this.modifiedByName = data["modifiedByName"];
            this.modifiedDateTime = data["modifiedDateTime"];
            this.createdBy = data["createdBy"];
            this.modifiedBy = data["modifiedBy"];
            this.active = data["active"];
            this.orderPartId = data["orderPartId"];
            this.orderId = data["orderId"];
            this.partId = data["partId"];
            this.consumedQuantity = data["consumedQuantity"];
            this.partName = data["partName"];
        }
    };
    OrderPartDto.fromJS = function (data) {
        var result = new OrderPartDto();
        result.init(data);
        return result;
    };
    OrderPartDto.prototype.toJSON = function (data) {
        data = typeof data === 'object' ? data : {};
        data["createdByName"] = this.createdByName;
        data["createdDateTime"] = this.createdDateTime;
        data["createdDate"] = this.createdDate;
        data["modifiedByName"] = this.modifiedByName;
        data["modifiedDateTime"] = this.modifiedDateTime;
        data["createdBy"] = this.createdBy;
        data["modifiedBy"] = this.modifiedBy;
        data["active"] = this.active;
        data["orderPartId"] = this.orderPartId;
        data["orderId"] = this.orderId;
        data["partId"] = this.partId;
        data["consumedQuantity"] = this.consumedQuantity;
        data["partName"] = this.partName;
        return data;
    };
    return OrderPartDto;
}());
exports.OrderPartDto = OrderPartDto;
var PagedResultDtoOfOrderPartDto = /** @class */ (function () {
    function PagedResultDtoOfOrderPartDto(data) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    this[property] = data[property];
            }
        }
    }
    PagedResultDtoOfOrderPartDto.prototype.init = function (data) {
        if (data && data != null) {
            if (data["results"] && data["results"].constructor === Array) {
                this.results = [];
                for (var _i = 0, _a = data["results"]; _i < _a.length; _i++) {
                    var item = _a[_i];
                    this.results.push(OrderPartDto.fromJS(item));
                }
            }
               this.count = data["count"];
        }
    };
    PagedResultDtoOfOrderPartDto.fromJS = function (data) {
        var result = new PagedResultDtoOfOrderPartDto();
        result.init(data);
        return result;
    };
    PagedResultDtoOfOrderPartDto.prototype.toJSON = function (data) {
        data = typeof data === 'object' ? data : {};
        if (this.results && this.results.constructor === Array) {
            data["results"] = [];
            for (var _i = 0, _a = this.results; _i < _a.length; _i++) {
                var item = _a[_i];
                data["results"].push(item.toJSON());
            }
        }
   this.count = data["count"];
        return data;
    };
    return PagedResultDtoOfOrderPartDto;
}());
exports.PagedResultDtoOfOrderPartDto = PagedResultDtoOfOrderPartDto;
//# sourceMappingURL=orderpart.model.js.map
