import {RouterModule, Routes} from '@angular/router';
import {EqmaintenanceComponent} from './eqmaintenance.component';
import {ManufacturerComponent} from './manufacturer/manufacturer.component';
import {FailureTypeComponent} from './failure_type/failuretype.component';
import {OrderTypeComponent} from './order_type/ordertype.component';
import {PriorityComponent} from './priority/priority.component';
import {PMInactiveReasonComponent} from './pminactivereason/pminactivereason.component';
import {StatusComponent} from './status/status.component';
import {PartComponent} from './part/part.component';
import {ScheduleTypeComponent} from './schedule_type/scheduletype.component';
import {AuthGuard} from '../shared/guards/auth.guard'
import {PMScheduleComponent} from './pmschedule/pmschedule.component';
import {OrderComponent} from './order/order.component'
import {OrderDetailComponent} from './order/order.detail.component'
import {EquipmentComponent} from './equipment/equipment.component'
import {RolesComponent} from './roles/roles.component'
import {EQMusersettingsComponent} from './eqmusersettings/eqmusersettings.component'
import {EquipmentTypeComponent} from './equipment-type/equipment-type.component';
/*

export const routes: Routes = [
  {
    path: '', component: EqmaintenanceComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'manufacturer', component: ManufacturerComponent,  canActivate: [AuthGuard]
      },
      {path: 'failure-type', component: FailureTypeComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'equipment-type', component: EquipmentTypeComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'ordertype', component: OrderTypeComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'priority', component: PriorityComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'pmreason', component: PMInactiveReasonComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'status', component: StatusComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'part', component: PartComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'scheduletype', component: ScheduleTypeComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'pmschedule', component: PMScheduleComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'orders', component: OrderComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'orderdetail/:id', component: OrderDetailComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'equipment', component: EquipmentComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'administration/roles', component: RolesComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'administration/eqmusersettings', component: EQMusersettingsComponent, data: {preload: false}, canActivate: [AuthGuard]}
    ]
  },
];
*/

export const routes: Routes = [
  {
    path: '', component: EqmaintenanceComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'manufacturer', component: ManufacturerComponent, data: {preload: false}, canActivate: [AuthGuard]
      },
      {path: 'failure-type', component: FailureTypeComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'equipment-type', component: EquipmentTypeComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'ordertype', component: OrderTypeComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'priority', component: PriorityComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'pmreason', component: PMInactiveReasonComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'status', component: StatusComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'part', component: PartComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'scheduletype', component: ScheduleTypeComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'pmschedule', component: PMScheduleComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'orders', component: OrderComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'orderdetail/:id', component: OrderDetailComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'equipment', component: EquipmentComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'administration/roles', component: RolesComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'administration/eqmusersettings', component: EQMusersettingsComponent, data: {preload: false}, canActivate: [AuthGuard]}
    ]
  },
];


export const routing = RouterModule.forChild(routes);
