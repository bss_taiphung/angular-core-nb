import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';
import {StatusDto} from './service/status.model';


@Component({
  selector: 'kendo-grid-status-form',
  styles: [
    'input[type=text] { width: 100%; }'
  ],
  templateUrl: './html/status-form.component.html'
})
export class StatusFormComponent {
  public active = false;
  public editForm: FormGroup = new FormGroup({
    'statusId': new FormControl(),
    'name': new FormControl('', Validators.required),
    'description': new FormControl(),
    'isInternal': new FormControl(false),
    'isExternal': new FormControl(false),
    'isPMS': new FormControl(false),
    'canClose': new FormControl(false),
    'isAssigned': new FormControl(false),
    'active': new FormControl(false),
  });

  @Input() public isNew = false;

  @Input()
  public set model(status: StatusDto) {
    this.editForm.reset(status);
    this.active = status !== undefined;
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<StatusDto> = new EventEmitter();

  public onSave(e): void {
    e.preventDefault();
    this.save.emit(this.editForm.value);
    this.active = false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.active = false;
    this.cancel.emit();
  }
}
