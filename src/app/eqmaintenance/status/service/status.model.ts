﻿import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';


// region StatusDto
export interface IStatusDto {
  name: string;
  description: string;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  statusId: number;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  isInternal: boolean;
  isExternal: boolean;
  isPMS: boolean;
  canClose: boolean;
  isAssigned: boolean;
  active: boolean;
}

export class StatusDto implements IStatusDto {
  name: string;
  description: string;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  statusId: number;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  isInternal: boolean;
  isExternal: boolean;
  isPMS: boolean;
  canClose: boolean;
  isAssigned: boolean;
  active: boolean;

  static fromJS(data: any): StatusDto {
    const result = new StatusDto();
    result.init(data);
    return result;
  }

  constructor(data?: IStatusDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.name = data['name'];
      this.description = data['description'];
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.statusId = data['statusId'];
      this.siteId = data['siteId'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.isInternal = data['isInternal'];
      this.isExternal = data['isExternal'];
      this.isPMS = data['isPMS'];
      this.canClose = data['canClose'];
      this.isAssigned = data['isAssigned'];
      this.active = data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['name'] = this.name;
    data['description'] = this.description;
    data['createdByName'] = this.createdByName;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['statusId'] = this.statusId;
    data['siteId'] = this.siteId;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['isInternal'] = this.isInternal;
    data['isExternal'] = this.isExternal;
    data['isPMS'] = this.isPMS;
    data['canClose'] = this.canClose;
    data['isAssigned'] = this.isAssigned;
    data['active'] = this.active;

    return data;
  }
}

// endregion

// region CreateStatusDto

export interface ICreateStatusDto {
  name: string;
  description: string;
  isInternal: boolean;
  isExternal: boolean;
  isPMS: boolean;
  canClose: boolean;
  isAssigned: boolean;
  active: boolean;
}

export class CreateStatusDto implements ICreateStatusDto {
  statusId = 0;
  name: string;
  description: string;
  isInternal: boolean;
  isExternal: boolean;
  isPMS: boolean;
  canClose: boolean;
  isAssigned: boolean;
  active = true;

  constructor(data?: IStatusDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.statusId = 0;
      this.name = data['name'];
      this.description = data['description'];
      this.isInternal = data['isInternal'];
      this.isExternal = data['isExternal'];
      this.isPMS = data['isPMS'];
      this.canClose = data['canClose'];
      this.isAssigned = data['isAssigned'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  static fromJS(data: any): StatusDto {
    const result = new StatusDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['statusId'] = 0;
    data['name'] = this.name;
    data['description'] = this.description;
    data['isInternal'] = this.isInternal;
    data['isExternal'] = this.isExternal;
    data['isPMS'] = this.isPMS;
    data['canClose'] = this.canClose;
    data['isAssigned'] = this.isAssigned;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

// endregion

// region PagedResultDtoOfStatusDto
export interface IPagedResultDtoOfStatusDto {
  results: StatusDto[];
  count: number;
}

export class PagedResultDtoOfStatusDto implements IPagedResultDtoOfStatusDto {
  results: StatusDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfStatusDto {
    const result = new PagedResultDtoOfStatusDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfStatusDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(StatusDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion
