"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var eqmaintenance_component_1 = require("./eqmaintenance.component");
var manufacturer_component_1 = require("./manufacturer/manufacturer.component");
var failuretype_component_1 = require("./failure_type/failuretype.component");
var ordertype_component_1 = require("./order_type/ordertype.component");
var priority_component_1 = require("./priority/priority.component");
var pminactivereason_component_1 = require("./pminactivereason/pminactivereason.component");
var status_component_1 = require("./status/status.component");
var part_component_1 = require("./part/part.component");
var scheduletype_component_1 = require("./schedule_type/scheduletype.component");
var auth_guard_1 = require("../shared/guards/auth.guard");
var pmschedule_component_1 = require("./pmschedule/pmschedule.component");
var order_component_1 = require("./order/order.component");
var order_detail_component_1 = require("./order/order.detail.component");
var equipment_component_1 = require("./equipment/equipment.component");
var roles_component_1 = require("./roles/roles.component");
var eqmusersettings_component_1 = require("./eqmusersettings/eqmusersettings.component");
var equipmentType_component_1 = require("./equipmentType/equipmentType.component");
exports.routes = [
    {
        path: '', component: eqmaintenance_component_1.EqmaintenanceComponent,
        canActivate: [auth_guard_1.AuthGuard],
        children: [
            { path: 'manufacturer', component: manufacturer_component_1.ManufacturerComponent, data: { preload: true }, canActivate: [auth_guard_1.AuthGuard] },
            { path: "failure-type", component: failuretype_component_1.FailureTypeComponent, data: { preload: true }, canActivate: [auth_guard_1.AuthGuard] },
            { path: "equipment-type", component: equipmentType_component_1.EquipmentTypeComponent, data: { preload: true }, canActivate: [auth_guard_1.AuthGuard] },
            { path: "ordertype", component: ordertype_component_1.OrderTypeComponent, data: { preload: true }, canActivate: [auth_guard_1.AuthGuard] },
            { path: "priority", component: priority_component_1.PriorityComponent, data: { preload: true }, canActivate: [auth_guard_1.AuthGuard] },
            { path: "pmreason", component: pminactivereason_component_1.PMInactiveReasonComponent, data: { preload: true }, canActivate: [auth_guard_1.AuthGuard] },
            { path: "status", component: status_component_1.StatusComponent, data: { preload: true }, canActivate: [auth_guard_1.AuthGuard] },
            { path: "part", component: part_component_1.PartComponent, data: { preload: true }, canActivate: [auth_guard_1.AuthGuard] },
            { path: "scheduletype", component: scheduletype_component_1.ScheduleTypeComponent, data: { preload: true }, canActivate: [auth_guard_1.AuthGuard] },
            { path: "pmschedule", component: pmschedule_component_1.PMScheduleComponent, data: { preload: true }, canActivate: [auth_guard_1.AuthGuard] },
            { path: "orders", component: order_component_1.OrderComponent, data: { preload: true }, canActivate: [auth_guard_1.AuthGuard] },
            { path: "orderdetail/:id", component: order_detail_component_1.OrderDetailComponent, data: { preload: true }, canActivate: [auth_guard_1.AuthGuard] },
            { path: "equipment", component: equipment_component_1.EquipmentComponent, data: { preload: true }, canActivate: [auth_guard_1.AuthGuard] },
            { path: "administration/roles", component: roles_component_1.RolesComponent, data: { preload: true }, canActivate: [auth_guard_1.AuthGuard] },
            { path: "administration/eqmusersettings", component: eqmusersettings_component_1.EQMusersettingsComponent, data: { preload: true }, canActivate: [auth_guard_1.AuthGuard] }
        ]
    },
];
exports.routing = router_1.RouterModule.forChild(exports.routes);
//# sourceMappingURL=eqmaintenance.routes.js.map