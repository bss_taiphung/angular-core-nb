import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, HttpModule } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';


export class ManufacturerDto implements IManufacturerDto {
  manufacturerName: string;
  createdByName: string;
  createdDate: string;
  createdDateTime: string;
  modifiedByName: string;
  modifiedDateTime: string;
  manufacturerId: number;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;

  constructor(data?: IManufacturerDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.manufacturerName = data["manufacturerName"];
      this.createdByName = data["createdByName"];
      this.createdDateTime = data["createdDateTime"];
      this.createdDate = data["createdDate"];
      this.modifiedByName = data["modifiedByName"];
      this.modifiedDateTime = data["modifiedDateTime"];
      this.manufacturerId = data["manufacturerId"];
      this.siteId = data["siteId"];
      this.createdBy = data["createdBy"];
      this.modifiedBy = data["modifiedBy"];
      this.active = data["active"];
    }
  }

  static fromJS(data: any): ManufacturerDto {
    let result = new ManufacturerDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data["manufacturerName"] = this.manufacturerName;
    data["createdByName"] = this.createdByName;
    data["createdDateTime"] = this.createdDateTime;
    data["createdDate"] = this.createdDate;
    data["modifiedByName"] = this.modifiedByName;
    data["modifiedDateTime"] = this.modifiedDateTime;
    data["manufacturerId"] = this.manufacturerId;
    data["siteId"] = this.siteId;
    data["createdBy"] = this.createdBy;
    data["modifiedBy"] = this.modifiedBy;
    data["active"] = this.active;

    return data;
  }
}

export interface IManufacturerDto {
  manufacturerName: string;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  manufacturerId: number;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
}

export class CreateManufacturerDto implements ICreateManufacturerDto {
  manufacturerId: number = 0;
  manufacturerName: string;
  active: boolean = true;

  constructor(data?: IManufacturerDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.manufacturerId = 0;
      this.manufacturerName = data["manufacturerName"];
      this.active = (data["active"] == null) ? false : data["active"];
    }
  }

  static fromJS(data: any): ManufacturerDto {
    let result = new ManufacturerDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["manufacturerId"] = 0;
    data["manufacturerName"] = this.manufacturerName;
    data["active"] = (this.active == null) ? false : this.active;
    return data;
  }
}

export interface ICreateManufacturerDto {
  manufacturerName: string;
  active: boolean;
}

export class PagedResultDtoOfManufacturerDto implements IPagedResultDtoOfManufacturerDto {
  results: ManufacturerDto[];
  count: number;

  constructor(data?: IPagedResultDtoOfManufacturerDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data["results"] && data["results"].constructor === Array) {
        this.results = [];
        for (let item of data["results"])
          this.results.push(ManufacturerDto.fromJS(item));
      }
      this.count = data["count"];
    }
  }

  static fromJS(data: any): PagedResultDtoOfManufacturerDto {
    let result = new PagedResultDtoOfManufacturerDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data["results"] = [];
      for (let item of this.results)
        data["results"].push(item.toJSON());
    }
    if (this.count) {
      data["count"] = this.count;
    }
    return data;
  }
}

export interface IPagedResultDtoOfManufacturerDto {
  results: ManufacturerDto[];
  count: number;
}
