import {
  Component,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import {
  Validators,
  FormGroup,
  FormControl
} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';
import {ManufacturerDto} from './service/manufacturer.model';

@Component({
  selector: 'kendo-grid-manufacturer-form',
  styles: [
    'input[type=text] { width: 100%; }'
  ],
  templateUrl: './html/manufacturer-form.component.html'
})
export class ManufacturerFormComponent {
  public active = false;
  public editForm: FormGroup = new FormGroup({
    'manufacturerId': new FormControl(),
    'manufacturerName': new FormControl('', Validators.required),
    'active': new FormControl(false),
  });

  @Input() public isNew = false;

  @Input()
  public set model(manufacturer: ManufacturerDto) {
    this.editForm.reset(manufacturer);
    this.active = manufacturer !== undefined;
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<ManufacturerDto> = new EventEmitter();

  public onSave(e): void {
    e.preventDefault();
    this.save.emit(this.editForm.value);
    this.active = false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();

  }

  private closeForm(): void {
    this.active = false;
    this.cancel.emit();
  }
}
