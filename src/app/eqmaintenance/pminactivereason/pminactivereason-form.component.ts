import {
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';
import {PMInactiveReasonDto} from './service/pminactivereason.model';


@Component({
  selector: 'kendo-grid-pminactivereason-form',
  styles: [
    'input[type=text] { width: 100%; }'
  ],
  templateUrl: './html/pminactivereason-form.component.html'
})
export class PMInactiveReasonFormComponent {
  public active = false;
  public editForm: FormGroup = new FormGroup({
    'pmInactiveReasonId': new FormControl(),
    'reason': new FormControl('', Validators.required),
    'active': new FormControl(false),
  });

  @Input() public isNew = false;

  @Input()
  public set model(pminactivereason: PMInactiveReasonDto) {
    this.editForm.reset(pminactivereason);
    this.active = pminactivereason !== undefined;
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<PMInactiveReasonDto> = new EventEmitter();

  public onSave(e): void {

    e.preventDefault();
    this.save.emit(this.editForm.value);
    this.active = false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.active = false;
    this.cancel.emit();
  }
}
