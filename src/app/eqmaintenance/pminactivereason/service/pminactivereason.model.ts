import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';


// region PMInactiveReasonDto

export interface IPMInactiveReasonDto {
  reason: string;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  pmInactiveReasonId: number;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
}

export class PMInactiveReasonDto implements IPMInactiveReasonDto {
  reason: string;
  createdByName: string;
  createdDate: string;
  createdDateTime: string;
  modifiedByName: string;
  modifiedDateTime: string;
  pmInactiveReasonId: number;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;

  static fromJS(data: any): PMInactiveReasonDto {
    const result = new PMInactiveReasonDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPMInactiveReasonDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.reason = data['reason'];
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      if (data['pMInactiveReasonId'] === undefined) {
        this.pmInactiveReasonId = data['pmInactiveReasonId'];
      }
      else {
        this.pmInactiveReasonId = data['pMInactiveReasonId'];
      }
      this.siteId = data['siteId'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.active = data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['reason'] = this.reason;
    data['createdByName'] = this.createdByName;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['pMInactiveReasonId'] = this.pmInactiveReasonId;
    data['siteId'] = this.siteId;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;

    return data;
  }
}

// endregion

// region CreatePMInactiveReasonDto

export interface ICreatePMInactiveReasonDto {
  reason: string;
  active: boolean;
}

export class CreatePMInactiveReasonDto implements ICreatePMInactiveReasonDto {
  pmInactiveReasonId = 0;
  reason: string;
  active = true;

  static fromJS(data: any): PMInactiveReasonDto {
    const result = new PMInactiveReasonDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPMInactiveReasonDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.pmInactiveReasonId = 0;
      this.reason = data['reason'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['pMInactiveReasonId'] = 0;
    data['reason'] = this.reason;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

// endregion

// region PagedResultDtoOfPMInactiveReasonDto

export interface IPagedResultDtoOfPMInactiveReasonDto {
  results: PMInactiveReasonDto[];
  count: number;
}

export class PagedResultDtoOfPMInactiveReasonDto implements IPagedResultDtoOfPMInactiveReasonDto {
  results: PMInactiveReasonDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfPMInactiveReasonDto {
    const result = new PagedResultDtoOfPMInactiveReasonDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfPMInactiveReasonDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(PMInactiveReasonDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion
