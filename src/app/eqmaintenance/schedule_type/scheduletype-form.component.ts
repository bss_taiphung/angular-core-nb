import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';
import {ScheduleTypeDto} from './service/scheduletype.model';

@Component({
  selector: 'kendo-grid-scheduletype-form',
  styles: [
    'input[type=text] { width: 100%; }'
  ],
  templateUrl: './html/scheduletype-form.component.html'
})
export class ScheduleTypeFormComponent {
  public SpecialScheduleList: any = [
    {text: '-Select-', value: -1},
    {text: 'Counter Based', value: 1}
  ];
  public filterSpecialScheduleList: any = this.SpecialScheduleList;
  public active = false;
  public isspeicalschedule = false;
  public editForm: FormGroup = new FormGroup({
    'scheduleTypeId': new FormControl(),
    'name': new FormControl('', Validators.required),
    'noOfMonths': new FormControl('', Validators.required),
    'specialSchedule': new FormControl('', Validators.required),
    'rdNoOfMonths': new FormControl(true),
    'rdspecialSchedule': new FormControl(false),
    'active': new FormControl(false),
  });

  @Input() public isNew = false;

  @Input()
  public set model(scheduletype: ScheduleTypeDto) {
    this.editForm.reset(scheduletype);
    this.active = scheduletype !== undefined;
    if (scheduletype != null && scheduletype.specialSchedule !== undefined && scheduletype.specialSchedule === 1) {
      this.isspeicalschedule = true;
      this.editForm.controls['specialSchedule'].enable();
      this.editForm.controls['noOfMonths'].disable();

    } else {
      this.isspeicalschedule = false;
      this.editForm.controls['specialSchedule'].disable();
      this.editForm.controls['noOfMonths'].enable();
    }
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<ScheduleTypeDto> = new EventEmitter();

  public onSave(e): void {
    e.preventDefault();
    this.save.emit(this.editForm.value);
    this.active = false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.active = false;
    this.cancel.emit();
  }

  public getScheduleType(check: boolean) {
    if (check) {
      this.editForm.controls['specialSchedule'].enable();
      this.editForm.controls['noOfMonths'].disable();
      this.editForm.controls['noOfMonths'].setValue(0);
    } else {
      this.editForm.controls['specialSchedule'].disable();
      this.editForm.controls['noOfMonths'].enable();
      this.editForm.controls['specialSchedule'].setValue(-1);
    }

  }

  private SpecialScheduleListHandleFilter(value) {
    this.filterSpecialScheduleList = this.SpecialScheduleList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
}
