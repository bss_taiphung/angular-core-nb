﻿import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';


// region ScheduleTypeDto
export interface IScheduleTypeDto {
  name: string;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  noOfMonths: number;
  specialSchedule: number;
  modifiedByName: string;
  modifiedDateTime: string;
  scheduleTypeId: number;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  rdNoOfMonths: boolean;
  isSpecial: boolean;
  active: boolean;
}

export class ScheduleTypeDto implements IScheduleTypeDto {
  name: string;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  noOfMonths: number;
  specialSchedule: number;
  scheduleTypeId: number;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  rdNoOfMonths: boolean;
  isSpecial: boolean;
  active: boolean;

  static fromJS(data: any): ScheduleTypeDto {
    const result = new ScheduleTypeDto();
    result.init(data);
    return result;
  }

  constructor(data?: IScheduleTypeDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.name = data['name'];
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.scheduleTypeId = data['scheduleTypeId'];
      this.specialSchedule = data['specialSchedule'];
      this.noOfMonths = data['noOfMonths'];
      this.siteId = data['siteId'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.rdNoOfMonths = data['rdNoOfMonths'];
      this.isSpecial = data['isSpecial'];
      this.active = data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['name'] = this.name;
    data['createdByName'] = this.createdByName;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['scheduleTypeId'] = this.scheduleTypeId;
    data['specialSchedule'] = this.specialSchedule;
    data['noOfMonths'] = this.noOfMonths;
    data['siteId'] = this.siteId;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;
    data['isSpecial'] = this.isSpecial;
    data['rdNoOfMonths'] = this.rdNoOfMonths;

    return data;
  }
}

// endregion

// region CreateScheduleTypeDto

export interface ICreateScheduleTypeDto {
  name: string;
  specialSchedule: number;
  noOfMonths: number;
  isSpecial: boolean;
  rdNoOfMonths: boolean;
  active: boolean;
}

export class CreateScheduleTypeDto implements ICreateScheduleTypeDto {
  scheduleTypeId = 0;
  name: string;
  noOfMonths: number;
  specialSchedule: number;
  rdNoOfMonths = true;
  isSpecial: boolean;
  active = true;

  static fromJS(data: any): ScheduleTypeDto {
    const result = new ScheduleTypeDto();
    result.init(data);
    return result;
  }

  constructor(data?: IScheduleTypeDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.scheduleTypeId = 0;
      this.name = data['name'];
      this.specialSchedule = data['specialSchedule'];
      this.noOfMonths = data['noOfMonths'];
      this.rdNoOfMonths = data['rdNoOfMonths'];
      this.isSpecial = data['isSpecial'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['scheduleTypeId'] = 0;
    data['name'] = this.name;
    data['specialSchedule'] = this.specialSchedule;
    data['noOfMonths'] = this.noOfMonths;
    data['isSpecial'] = this.isSpecial;
    data['rdNoOfMonths'] = this.rdNoOfMonths;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

// endregion

// region PagedResultDtoOfScheduleTypeDto
export interface IPagedResultDtoOfScheduleTypeDto {
  results: ScheduleTypeDto[];
  count: number;
}

export class PagedResultDtoOfScheduleTypeDto implements IPagedResultDtoOfScheduleTypeDto {
  results: ScheduleTypeDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfScheduleTypeDto {
    const result = new PagedResultDtoOfScheduleTypeDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfScheduleTypeDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(ScheduleTypeDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion
