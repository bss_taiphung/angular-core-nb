import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-eqmnavmenu',
  templateUrl: './eqmnavmenu.component.html',
  styleUrls: ['./eqmnavmenu.component.scss']
})
export class EqmnavmenuComponent {

  treeViewActive = false;
  private eqmMenu: any = [
    'manufacturer',
    'part',
    'scheduletype',
    'ordertype',
    'failure-type',
    'priority',
    'pmreason',
    'status'];

  constructor(private route: ActivatedRoute, private router: Router) {
    const path = location.href.split('/')[4];
    const isMenu = this.eqmMenu.find(res => res === path);
    if (isMenu) {
      this.treeViewActive = true;
    }
  }

  activeMenu(path) {
    const isMenu = this.eqmMenu.find(res => res === path);
    if (isMenu) {
      this.treeViewActive = true;
    }
  }
}
