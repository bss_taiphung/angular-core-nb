import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EqmnavmenuComponent } from './eqmnavmenu.component';

describe('EqmnavmenuComponent', () => {
  let component: EqmnavmenuComponent;
  let fixture: ComponentFixture<EqmnavmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EqmnavmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EqmnavmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
