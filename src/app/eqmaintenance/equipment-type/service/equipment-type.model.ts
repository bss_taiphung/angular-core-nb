﻿import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';

// region EquipmentTypeDto
export interface IEquipmentTypeDto {
  equipmentTypeId: number;
  equipmentTypeName: string;
  description: string;
  siteId: number;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
}

export class EquipmentTypeDto implements IEquipmentTypeDto {
  equipmentTypeId: number;
  equipmentTypeName: string;
  description: string;
  siteId: number;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  createdBy: number;
  modifiedBy: number;
  active: boolean;

  static fromJS(data: any): EquipmentTypeDto {
    let result = new EquipmentTypeDto();
    result.init(data);
    return result;
  }

  constructor(data?: IEquipmentTypeDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.equipmentTypeId = data['equipmentTypeId'];
      this.equipmentTypeName = data['equipmentTypeName'];
      this.description = data['description'];
      this.siteId = data['siteId'];
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.active = data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['equipmentTypeId'] = this.equipmentTypeId;
    data['equipmentTypeName'] = this.equipmentTypeName;
    data['description'] = this.description;
    data['createdByName'] = this.createdByName;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;

    return data;
  }
}

// endregion

// region PagedResultDtoOfEquipmentTypeDto
export interface IPagedResultDtoOfEquipmentTypeDto {
  results: EquipmentTypeDto[];
  count: number;
}

export class PagedResultDtoOfEquipmentTypeDto implements IPagedResultDtoOfEquipmentTypeDto {
  results: EquipmentTypeDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfEquipmentTypeDto {
    const result = new PagedResultDtoOfEquipmentTypeDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfEquipmentTypeDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(EquipmentTypeDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion
