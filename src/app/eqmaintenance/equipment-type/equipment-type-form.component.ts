import {
  Component,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import {
  Validators,
  FormGroup,
  FormControl
} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';

import {EquipmentTypeDto} from './service/equipment-type.model';

@Component({
  selector: 'kendo-grid-equipmenttype-form',
  styles: [
    'input[type=text] { width: 100%; }'
  ],
  templateUrl: './html/equipment-type-form.component.html'
})
export class EquipmentTypeFormComponent {
  public active = false;
  public editForm: FormGroup = new FormGroup({
    'equipmentTypeId': new FormControl(),
    'equipmentTypeName': new FormControl('', Validators.required),
    'description': new FormControl(),
    'active': new FormControl(false),
  });

  @Input() public isNew = false;

  @Input()
  public set model(equipment: EquipmentTypeDto) {
    this.editForm.reset(equipment);
    this.active = equipment !== undefined;
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<EquipmentTypeDto> = new EventEmitter();

  public onSave(e): void {

    e.preventDefault();
    if (this.active == null) {
      this.active = false;
    }
    this.save.emit(this.editForm.value);
    this.active = false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.active = false;
    this.cancel.emit();
  }
}
