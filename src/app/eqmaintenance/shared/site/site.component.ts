import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {FormGroup} from '@angular/forms';
import {CommonService} from '../../../shared/services/common.service';
import {ToasterHelperService} from '../../../shared/services/toasterHelper.service';
import {EqmSettings, User, UserRole} from '../../../shared/models/eqmSettings'
import {EqmSettingsService} from '../../../shared/services/eqmsettings.service';
import {SelectListDto} from '../../../shared/models/selectListDto'
import {UserSettingsDto} from '../../../shared/usersettings/usersetting.model';
import {UserSettingsService} from '../../../shared/usersettings/usersetting.service';

@Component({
  selector: 'grid-header',
  templateUrl: './html/site.component.html',
  encapsulation: ViewEncapsulation.None,
})

export class SiteComponent implements OnInit {

  public formGroup: FormGroup;

  eqmSettings: EqmSettings = new EqmSettings();
  user: User = new User();
  userRole: UserRole = new UserRole();

  public siteList: SelectListDto[];
  public siteListVm: SelectListDto[];

  public siteSaving = false;
  public mss: any;
  public helpLink: string;
  public page: string;

  @Input()
  public set link(data: any) {
    if (data) {
      this.helpLink = data;
    }
  }

  @Input()
  public set pageName(data: any) {
    if (data) {
      this.page = data;
    }
  }

  @Output() siteSelectionChange: EventEmitter<any> = new EventEmitter();


  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private userSettingsService: UserSettingsService) {
    this.mss = this.commonService.getCookies('Mss');
    this.eqmSettings = this.eqmSettingsService.getEqmSettings();
    this.user = this.eqmSettingsService.getUser();
    this.userRole = this.eqmSettingsService.getUserRoles();
  }

  ngOnInit() {
    this.commonService.getAll('General', 'Site').subscribe(eventResult => {
      this.siteListVm = eventResult.results;
      this.siteList = this.siteListVm = eventResult.results;
      this.siteSelectionChange.emit();
    });
  }

  public siteHandleFilter(filter: any): void {
    this.siteList = this.siteListVm.filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  public updateCurrentSiteId() {
    const data = new UserSettingsDto();

    data.active = true;
    data.roleId = this.eqmSettings.roleId;
    data.userId = this.user.userId;
    data.userSettingsId = this.eqmSettings.userSettingsId;
    data.currentSiteId = this.eqmSettings.currentSiteId;

    this.userSettingsService.update(data,
      response => {
        this.toasterService.success('', 'User Site Updated Successfully');

        // update cookies to take new changes
        this.eqmSettingsService.userSettings().subscribe(eventResult => {
          this.siteSelectionChange.emit();
        });

        this.siteSaving = false;
      },
      error => {
        this.toasterService.errorMessage(error);
        this.siteSaving = false;
      }
    );
  }

  public onSiteSelectionChange(changedSiteId: any): void {
    if (changedSiteId) {
      this.siteSaving = true;
      this.eqmSettings.currentSiteId = changedSiteId.value;
      this.updateCurrentSiteId();
    }
  }
}
