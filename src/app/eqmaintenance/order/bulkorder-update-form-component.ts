import {
  Component,
  ViewEncapsulation,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {CommonService} from '../../shared/services/common.service';
import {FormGroup, FormControl} from '@angular/forms';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {OrderService} from './service/order.service';
import {SelectListDto} from 'app/shared/models/selectListDto';


@Component({
  selector: 'kendo-grid-order-bulkedit-form',
  templateUrl: './html/bulkorder-update-form-component.html',
  styleUrls: ['./css/order.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BulkOrderUpdateComponent implements OnInit {

  public defaultDate: Date = new Date();
  public editForm: FormGroup = new FormGroup({
    'orderIds': new FormControl(),
    'changeAssignedUser': new FormControl(false),
    'assignedUserId': new FormControl(0),
    'changeStatus': new FormControl(false),
    'statusId': new FormControl(0),
    'changePriority': new FormControl(false),
    'priorityId': new FormControl(0),
    'changeOrderType': new FormControl(false),
    'orderTypeId': new FormControl(0),
    'changeIsPlanned': new FormControl(false),
    'isPlanned': new FormControl(false),
    'changeDueDate': new FormControl(false),
    'dueDate': new FormControl(this.defaultDate),
  });

  @Input() public openedBulkEdit = false;
  @Input() public orderSelection = [];
  @Output() closeBulkEdit: EventEmitter<any> = new EventEmitter();

  public isdatepickerdisabled = true;
  public isOrderDelete = false;

  public assignedUserList: SelectListDto[];
  public filterassignedUserIdList: SelectListDto[];

  public statusList: SelectListDto[];
  public filterstatusIdList: SelectListDto[];

  public orderTypeList: SelectListDto[];
  public filterorderTypeIdList: SelectListDto[];

  public priorityList: SelectListDto[];
  public filterpriorityIdList: SelectListDto[];

  constructor(private commonService: CommonService,
              private toasterService: ToasterHelperService,
              private orderService: OrderService) {

  }

  ngOnInit() {
    this.commonService.getAll('General', 'User').subscribe(eventResult => {
      this.filterassignedUserIdList = this.assignedUserList = eventResult.results;
    });

    this.commonService.getAll('EquipmentMaintenance', 'Status')
      .subscribe(eventResult => {
        this.filterstatusIdList = this.statusList = eventResult.results;
      });

    this.commonService.getAll('EquipmentMaintenance', 'OrderType')
      .subscribe(eventResult => {
        this.filterorderTypeIdList = this.orderTypeList = eventResult.results;
      });

    this.commonService.getAll('EquipmentMaintenance', 'Priority')
      .subscribe(eventResult => {
        this.filterpriorityIdList = this.priorityList = eventResult.results;
      });
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeBulkEdit.emit(false);
    this.closeForm();
  }

  private closeForm(): void {
    this.openedBulkEdit = false;
    this.resetBulkEditForm();
  }

  private assignedUserIdHandleFilter(value) {
    this.filterassignedUserIdList = this.assignedUserList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private statusIdListHandleFilter(value) {
    this.filterstatusIdList = this.statusList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private orderTypeIdListHandleFilter(value) {
    this.filterorderTypeIdList = this.orderTypeList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private priorityIdListHandleFilter(value) {
    this.filterpriorityIdList = this.priorityList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  public onToogleValue(checkboxName, controlName) {
    const checkbox = this.editForm.controls[checkboxName].value;
    if (checkbox) {
      if (controlName === 'dueDate') {
        this.isdatepickerdisabled = true;
      }
      this.editForm.controls[controlName].disable();
    } else {
      if (controlName === 'dueDate') {
        this.isdatepickerdisabled = false;
      }
      this.editForm.controls[controlName].enable();
    }
  }

  private resetBulkEditForm() {
    this.editForm.controls['changeAssignedUser'].setValue(false);
    this.editForm.controls['assignedUserId'].setValue(0);
    this.editForm.controls['changeStatus'].setValue(false);
    this.editForm.controls['statusId'].setValue(0);
    this.editForm.controls['changePriority'].setValue(false);
    this.editForm.controls['priorityId'].setValue(0);
    this.editForm.controls['changeOrderType'].setValue(false);
    this.editForm.controls['orderTypeId'].setValue(0);
    this.editForm.controls['changeIsPlanned'].setValue(false);
    this.editForm.controls['isPlanned'].setValue(false);
    this.editForm.controls['changeDueDate'].setValue(false);
    this.editForm.controls['dueDate'].disable();
    this.editForm.controls['dueDate'].setValue(this.defaultDate);
  }

  public bulkSave() {
    const inputData = this.editForm.value;
    inputData.orderIds = this.orderSelection;

    this.orderService.bulkUpdate(inputData,
      response => {
        this.toasterService.success('', 'Order(s) Saved Successfully');
        this.closeBulkEdit.emit(true);

        this.orderSelection = [];
        this.closeForm();
      },
      error => {
        this.toasterService.errorMessage(error);
        this.closeBulkEdit.emit(true);

        this.orderSelection = [];
        this.closeForm();
      }
    );
  }

  public bulkDelete() {
    this.isOrderDelete = true;
  }

  public removeOrder(e, status): void {
    e.preventDefault();

    if (status) {
      const inputData = this.editForm.value;
      inputData.orderIds = this.orderSelection;

      this.orderService.bulkDelete(inputData,
        response => {
          this.toasterService.success('', 'Order(s) Deleted Successfully');
          this.closeBulkEdit.emit(true);

          this.orderSelection = [];
          this.closeForm();
        },
        error => {
          this.toasterService.errorMessage(error);
          this.closeBulkEdit.emit(true);

          this.orderSelection = [];
          this.closeForm();
        }
      );
    }

    this.isOrderDelete = false;
  }
}
