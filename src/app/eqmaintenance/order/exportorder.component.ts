import {
  Component,
  ViewEncapsulation,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'kendo-report-viewer',
  templateUrl: './html/exportorder.component.html',
  styleUrls: ['./css/order.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ExportOrderComponent {
  viewerContainerStyle = {
    position: 'relative',
    width: '100%',
    height: '800px',
    ['font-family']: 'ms sans serif'
  };

  @Input() public openReportViewer = false;
  @Input() public orderSelection = [];
  @Output() closeReportViewer: EventEmitter<any> = new EventEmitter();

  public reportUrl = '';
  public reportAuthenticationToken = '';

  constructor() {
    this.reportUrl = environment.reportServerUrl + '/api/reports/';
    this.reportAuthenticationToken = environment.reportAuthenticationToken;
  }


  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.openReportViewer = false;
    this.closeReportViewer.emit();
  }
}
