import {
  Component,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import {Validators, FormGroup, FormControl} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';
import {CommonService} from '../../shared/services/common.service';
import {SelectListDto} from '../../shared/models/selectListDto'
import {OrderDto} from './service/order.model'

@Component({
  selector: 'kendo-grid-order-form',
  styles: [
    'input[type=text] { width: 100%; }'
  ],
  templateUrl: './html/order-form.component.html'
})
export class OrderFormComponent {
  public active = false;
  public value: Date;
  public description = '';
  public editForm: FormGroup = new FormGroup({
    'orderId': new FormControl(),
    'statusId': new FormControl(),
    'equipmentId': new FormControl('', Validators.required),
    'orderTypeId': new FormControl('', Validators.required),
    'priorityId': new FormControl('', Validators.required),
    'dueDate': new FormControl(),
    'description': new FormControl(''),
    'isWorkCompleted': new FormControl(false),
    'active': new FormControl(false),
  });

  @Input() public isNew = false;

  @Input()
  public set model(order: any) {

    if (order !== (null || undefined)) {
      if (order.dueDate !== (null || undefined)) {
        order.dueDate = new Date(order.dueDate);

      }
      else {
        order.dueDate = new Date();
      }
    }

    this.active = order !== undefined;
    this.description = '';
    this.editForm.reset(order);

    if (this.active) {
      this.commonService.getAll('EquipmentMaintenance', 'Status')
        .subscribe(eventResult => {
          this.filterstatusIdList = this.statusList = eventResult.results;
        });

      this.commonService.getAll('EquipmentMaintenance', 'Equipment')
        .subscribe(eventResult => {
          this.filterequipmentIdList = this.equipmentList = eventResult.results;
        });

      this.commonService.getAll('EquipmentMaintenance', 'OrderType')
        .subscribe(eventResult => {
          this.filterorderTypeIdList = this.orderTypeList = eventResult.results;
        });

      this.commonService.getAll('EquipmentMaintenance', 'Priority')
        .subscribe(eventResult => {
          this.filterpriorityIdList = this.priorityList = eventResult.results;
        });
    }

  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<OrderDto> = new EventEmitter();

  public statusList: SelectListDto[];
  public filterstatusIdList: SelectListDto[];

  public equipmentList: SelectListDto[];
  public filterequipmentIdList: SelectListDto[];

  public orderTypeList: SelectListDto[];
  public filterorderTypeIdList: SelectListDto[];

  public priorityList: SelectListDto[];
  public filterpriorityIdList: SelectListDto[];


  constructor(private commonService: CommonService) {
  }

  public onSave(e): void {
    e.preventDefault();
    this.save.emit(this.editForm.value);
    this.active = false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.active = false;
    this.cancel.emit();
  }

  private equipmentIdListHandleFilter(value) {
    this.filterequipmentIdList = this.equipmentList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private orderTypeIdListHandleFilter(value) {
    this.filterorderTypeIdList = this.orderTypeList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private priorityIdListHandleFilter(value) {
    this.filterpriorityIdList = this.priorityList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private statusIdListHandleFilter(value) {
    this.filterstatusIdList = this.statusList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
}
