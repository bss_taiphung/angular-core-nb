//import { Component, Input, Output, EventEmitter } from '@angular/core';
//import { Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
//import "rxjs/add/observable/fromEvent";

//import { process, GroupDescriptor, State, aggregateBy } from '@progress/kendo-data-query';
//import { PageChangeEvent, GridComponent, GridDataResult, DataStateChangeEvent } from '@progress/kendo-angular-grid';
//import { SortDescriptor, orderBy } from '@progress/kendo-data-query';

//import { CommonService, SelectListDto } from "../../service/common.service";
//import { WorkRecordService } from "../workrecord/service/workrecord.service"
//import { WorkRecordDto } from "../workrecord/service/workrecord.model"

//@Component({
//    selector: 'kendo-grid-workrecord-form',
//    styles: [
//        'input[type=text] { width: 100%; }'
//    ],
//    templateUrl: './html/workrecord-form.component.html'
//})
//export class WorkRecordFormComponent {
//    public active = false;

//    public workrecordForm: FormGroup = new FormGroup({
//        'workRecordId': new FormControl(),
//        'orderId': new FormControl(),
//        'hours': new FormControl('', Validators.required),
//        'technicalNotes': new FormControl(''),
//        'active': new FormControl()
//    });

//    @Input() public isNew = false;
//    @Input() public set model(workrecord: WorkRecordDto) {
//        if (workrecord != undefined) {
//            this.workrecordForm.reset(workrecord);
//            this.active = workrecord !== undefined;
//        }
//    }

//    @Output() cancel: EventEmitter<any> = new EventEmitter();
//    @Output() save: EventEmitter<WorkRecordDto> = new EventEmitter();

//    public orderList: SelectListDto[];
//    constructor(private commonService: CommonService) {
//        this.init();
//    }

//    init(): void {

//    }

//    ngOnInit() {
//        this.commonService.getAll("EquipmentMaintenance", "Order").subscribe(eventResult => {
//            this.orderList = eventResult.results;
//        });
//    }   

//    public onSave(e): void {
//        e.preventDefault();
//        this.save.emit(this.workrecordForm.value);
//        this.active = false;
//    }

//    public onCancel(e): void {
//        e.preventDefault();
//        this.closeForm();
//    }

//    private closeForm(): void {
//        this.active = false;
//        this.cancel.emit();
//    }
//    public orderHandleFilter(filter: any): void {
//        this.orderList = this.orderList.filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
//    }
//}
