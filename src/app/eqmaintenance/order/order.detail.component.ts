import {
  Component,
  ViewEncapsulation,
  OnInit, Input,
  Output,
  EventEmitter
} from '@angular/core';
import {
  Router,
  ActivatedRoute,
  Params
} from '@angular/router';
import {
  Validators,
  FormGroup,
  FormControl
} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';

import {State} from '@progress/kendo-data-query';
import {
  PageChangeEvent,
  GridDataResult,
  DataStateChangeEvent
} from '@progress/kendo-angular-grid';
import {SortDescriptor} from '@progress/kendo-data-query';

import {OrderService} from './service/order.service'
import {
  OrderGetDto,
  OrderDto,
  CreateRelatedOrderDto
} from './service/order.model'
import {CommonService} from '../../shared/services/common.service';
import {SelectListDto} from '../../shared/models/selectListDto'
import {EqmSettings, User} from '../../shared/models/eqmSettings'

import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';

import {OrderPartService} from '../orderpart/service/orderpart.service'
import {OrderPartDto} from '../orderpart/service/orderpart.model'

import {WorkRecordService} from '../workrecord/service/workrecord.service'
import {WorkRecordDto} from '../workrecord/service/workrecord.model'

import {RelatedOrderService} from './service/relatedorder.service'

@Component({
  selector: 'order.detail',
  templateUrl: './html/order.detail.component.html',
  styleUrls: ['./css/order.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class OrderDetailComponent implements OnInit {

  orderId: any;
  createdByName: string;
  eqmSettings: EqmSettings = new EqmSettings();
  user: User = new User();

  @Output() save: EventEmitter<OrderDto> = new EventEmitter();

  public isOrderDelete = false;

  partData: OrderPartDto[] = [];
  public isNewPart = false;
  public isPartDelete = false;
  public partDeleteItem: OrderPartDto;
  public partLoading = true;
  public pageSaving = false;

  wrData: WorkRecordDto[] = [];
  public isNewWr = false;
  public isWrDelete = false;
  public wrDeleteDataItem: WorkRecordDto;
  public wrLoading = true;

  public partEditDataItem: OrderPartDto;
  public partDeleteDataItem: any;
  public partEditedRowIndex: number;
  public partFormGroup: FormGroup;
  public partGridView: GridDataResult;

  public isNew: boolean;
  public partPageSize = 10;
  public partSkip = 0;
  private partSort: SortDescriptor[] = [];
  public isactive = false;
  public searchtext = '';
  opened = false;
  loading = true;
  gridLoading = true;
  public state: State = {
    skip: 0,
    take: 5
  };

  public reFormGroup: FormGroup;
  public reEditedRowIndex: number;
  public reEditDataItem: CreateRelatedOrderDto;
  public rePageSize = 10;
  public reSkip = 0;
  public reGridView: GridDataResult;
  public reDeleteDataItem: any;
  public reSort: SortDescriptor[] = [];
  public reIsactive = true;
  public reSearchtext = '';
  public reOpened = false;
  public reLoading = true;
  public reGridLoading = true;
  public reState: State = {
    skip: 0,
    take: 5
  };

  public allChecked = new Array();

  public statusList: SelectListDto[];
  public equipmentList: SelectListDto[];
  public orderTypeList: SelectListDto[];
  public priorityList: SelectListDto[];
  public assignedUserList: SelectListDto[];
  public partList: SelectListDto[];
  public failureTypeList: SelectListDto[];
  isfailureTypeEditable = true;
  isWorkCompleted = false;

  editForm: FormGroup = new FormGroup({
    'orderId': new FormControl(),
    'statusId': new FormControl(),
    'equipmentId': new FormControl('', Validators.required),
    'assignedUserId': new FormControl(),
    'orderTypeId': new FormControl('', Validators.required),
    'priorityId': new FormControl('', Validators.required),
    'failureTypeId': new FormControl(),
    'dueDate': new FormControl(''),
    'completedDate': new FormControl(''),
    'isWorkCompleted': new FormControl(),
    'isPlanned': new FormControl(),
    'active': new FormControl(),
    'description': new FormControl(),
    'requestType': new FormControl()
  });

  public filterassignedUserList: SelectListDto[];

  constructor(private activatedRoute: ActivatedRoute,
              private orderService: OrderService,
              private router: Router,
              private relatedOrderService: RelatedOrderService,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private toasterService: ToasterHelperService,
              private orderPartService: OrderPartService,
              private workRecordService: WorkRecordService) {
    this.commonService.validateLogin();
    this.eqmSettings = this.eqmSettingsService.getEqmSettings();
    this.user = this.eqmSettingsService.getUser();
    this.init();
  }

  init(): void {

    this.activatedRoute.params.subscribe((params: Params) => {
      this.orderId = params['id'];
    });

    const order = new OrderGetDto();
    order.orderId = this.orderId;

    // FEEL ALL THE FORM DATA
    this.orderService.get(order,
      response => {
        this.setFormData(response);
      },
      error => {
        this.toasterService.errorMessage(error);
      }
    );

    // LOAD PART VIEW
    this.loadParts();

    // LOAD WORK HISTORY VIEW
    this.loadWorkRecords();

    // LOAD RELATED ORDERS GRID
    this.loadRelatedOrderGridData();

  }


  ngOnInit() {
    this.commonService.getAll('General', 'User').subscribe(eventResult => {
      this.filterassignedUserList = this.assignedUserList = eventResult.results;
    });

    this.commonService.getAll('EquipmentMaintenance', 'Status')
      .subscribe(eventResult => {
        this.statusList = eventResult.results;
      });

    this.commonService.getAll('EquipmentMaintenance', 'Equipment')
      .subscribe(eventResult => {
        this.equipmentList = eventResult.results;
      });

    this.commonService.getAll('EquipmentMaintenance', 'OrderType')
      .subscribe(eventResult => {
        this.orderTypeList = eventResult.results;
      });

    this.commonService.getAll('EquipmentMaintenance', 'Priority')
      .subscribe(eventResult => {
        this.priorityList = eventResult.results;
      });

    this.commonService.getAll('EquipmentMaintenance', 'part')
      .subscribe(eventResult => {
        this.partList = eventResult.results;
      });

    this.commonService.getAll('EquipmentMaintenance', 'FailureType')
      .subscribe(eventResult => {
        this.failureTypeList = eventResult.results;
      });
  }

  setFormData(order) {
    if (order !== (null || undefined)) {
      if (order.dueDate !== (null || undefined)) {
        order.dueDate = new Date(order.dueDate);
      }
      else {
        order.dueDate = new Date();
      }

      if (order.completedDate !== (null || undefined)) {
        order.completedDate = new Date(order.completedDate);
      }
      else {
        order.completedDate = new Date();
      }
    }

    this.createdByName = order.createdByName;
    this.editForm.reset(order);

    this.enableDisableFailureTypeDropdown(this.user.userId,
      order.assignedUserId,
      this.eqmSettings.canWorkOnUnassignedOrders);
  }

  private loadParts(): void {
    this.orderPartService.getAll(
      this.partPageSize,
      this.partSkip,
      this.partSort,
      this.isactive,
      this.searchtext,
      this.orderId).subscribe(eventResult => {

      this.partData = eventResult.results;
      this.partLoading = false;
    });
  }

  addPart() {

    this.partData.unshift(new OrderPartDto());
    this.isNewPart = true;
  }

  public partDeleteConfirm(part: OrderPartDto) {
    var $this = this;
    $this.isPartDelete = true;
    $this.partDeleteItem = part;
  }

  removePart(part: OrderPartDto, status) {
    const index = this.partData.indexOf(part);

    // EMOVE WORK RECORD FROM ARRAY, BECAUSE IT'S REMOVED
    if (index !== -1) {
      this.partData.splice(index, 1);
    }

    if (status && part.orderPartId) {
      this.orderPartService.delete(part.orderPartId,
        response => {
          this.toasterService.success('', 'Removed Successfully');
          this.gridLoading = false;
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        });
    }

    const $this = this;
    $this.isPartDelete = false;
  }

  savePart(part: OrderPartDto) {
    this.partLoading = true;

    if (!part.partId) {
      this.partLoading = false;
      this.toasterService.error('', 'Part is mondatory.');
      return false;
    }

    if (part.orderPartId) {
      this.orderPartService.update(part,
        response => {
          this.loadParts();
          this.partLoading = false;
          this.toasterService.success('', 'Part Updated Successfully');
        },
        error => {
          this.partLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    } else {
      part.active = true;
      part.orderPartId = 0;
      part.orderId = this.orderId;

      this.orderPartService.create(part,
        response => {
          this.loadParts();
          this.partLoading = false;
          this.toasterService.success('', 'Part Added Successfully');
        },
        error => {
          this.partLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  private loadWorkRecords(): void {
    this.workRecordService.getAll(
      this.partPageSize,
      this.partSkip,
      this.partSort,
      this.isactive,
      this.searchtext,
      this.orderId)
      .subscribe(eventResult => {
        this.wrData = eventResult.results;

        this.wrLoading = false;
      });
  }

  error(message: string) {
    this.toasterService.error('Not authorized.', message);
  }

  addWorkRecord() {
    this.wrData.unshift(new WorkRecordDto());
    // this.isNewPart = true;
  }

  public wrDeleteConfirm(workRecord: WorkRecordDto) {
    const $this = this;
    $this.isWrDelete = true;
    $this.wrDeleteDataItem = workRecord;
  }

  removeWorkRecord(workRecord: WorkRecordDto, status) {
    const index = this.wrData.indexOf(workRecord);
    // REMOVE WORK RECORD FROM ARRAY, BECAUSE IT'S REMOVED
    if (index !== -1) {
      this.wrData.splice(index, 1);
    }

    if (status && workRecord.workRecordId) {
      this.workRecordService.delete(workRecord.workRecordId,
        response => {

          this.toasterService.success('', 'Work Record Removed Successfully');
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }

    const $this = this;
    $this.isWrDelete = false;
  }

  saveWorkRecord(workRecord: WorkRecordDto) {

    if (!workRecord.hours || !workRecord.technicalNotes) {
      this.toasterService.error('', 'Hours and Notes are mondatory.');
      return false;
    }

    this.wrLoading = true;

    if (workRecord.workRecordId) {
      this.workRecordService.update(workRecord,
        response => {
          this.loadWorkRecords();
          this.wrLoading = false;
          this.toasterService.success('', 'Work Record Updated Successfully');
        },
        error => {
          this.wrLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    } else {
      workRecord.active = true;
      workRecord.workRecordId = 0;
      workRecord.orderId = this.orderId;

      this.workRecordService.create(workRecord,
        response => {
          this.loadWorkRecords();
          this.wrLoading = false;
          this.toasterService.success('', 'Work Record Added Successfully');
        },
        error => {
          this.wrLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  public enableDisableFailureTypeDropdown(loggedInUserId: number,
                                          assignedUserId: number,
                                          workOnUnassignedOrders: boolean) {
    if ((loggedInUserId == assignedUserId) || workOnUnassignedOrders) {
      this.isfailureTypeEditable = false;
    }
  }

  public onSave(e): void {

    this.pageSaving = true;

    e.preventDefault();

    const inputData: OrderDto = this.editForm.value;
    this.orderService.update(inputData,
      response => {
        this.pageSaving = false;
        this.toasterService.success('', 'Order updated Successfully');
      },
      error => {
        this.pageSaving = false;
        this.toasterService.errorMessage(error);
      }
    );
  }

  public orderDeleteClose() {
    const $this = this;
    $this.isOrderDelete = true;
  }

  public removeOrder(e, status): void {

    this.pageSaving = true;

    e.preventDefault();

    if (status) {

      const inputData: OrderDto = this.editForm.value;

      this.orderService.delete(inputData.orderId,
        response => {
          this.toasterService.success('', 'Order Removed Successfully');
          this.pageSaving = false;
          this.router.navigate(['/eqmaintenance/orders']);
        },
        error => {
          this.pageSaving = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    const $this = this;
    $this.isOrderDelete = false;
  }

  public statusHandleFilter(filter: any): void {
    this.statusList = this.statusList.filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  public assignedUserHandleFilter(filter: any): void {
    this.filterassignedUserList = this.assignedUserList.filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  // AssignedUserId dropdown change event
  public selectionChange(value: any): void {
    this.enableDisableFailureTypeDropdown(this.user.userId,
      value,
      this.eqmSettings.canWorkOnUnassignedOrders);
  }

  public priorityHandleFilter(filter: any): void {
    this.priorityList = this.priorityList
      .filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  public equipmentHandleFilter(filter: any): void {
    this.equipmentList = this.equipmentList
      .filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  public orderTypeHandleFilter(filter: any): void {
    this.orderTypeList = this.orderTypeList.filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  public partHandleFilter(filter: any): void {
    this.partList = this.partList
      .filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  public failureTypeHandleFilter(filter: any): void {
    this.failureTypeList = this.failureTypeList
      .filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  public open() {
    this.opened = true;
  }

  private loadRelatedOrderGridData(): void {
    this.relatedOrderService.getAll(
      this.rePageSize,
      this.reSkip,
      this.reSort,
      this.reIsactive,
      this.reSearchtext,
      this.orderId)
      .subscribe(eventResult => {

        if (this.allChecked !== (null || undefined) && this.allChecked.length > 0) {
          this.allChecked.forEach((checkedEquipment => {
            eventResult.results.forEach((itm => {
              if (checkedEquipment === itm.equipmentId) {
                itm.orderReport = true;
              }
            }));
          }));
        }

        if (eventResult.results.length > 0) {
          this.reGridView = {
            data: eventResult.results,
            total: eventResult.count
          };
        }
        else {
          this.reGridView = {
            data: eventResult.results,
            total: 0
          };
        }

        this.reLoading = false;
        this.reGridLoading = false;
      });
  }

  public reSortChange(sort: SortDescriptor[]): void {
    this.reSort = sort;
    this.reGridLoading = true;
    this.loadRelatedOrderGridData();
  }

  protected rePageChange({skip, take}: PageChangeEvent): void {
    this.reSkip = skip;
    this.rePageSize = take;
    this.reGridLoading = true;
    this.loadRelatedOrderGridData();
  }

  private reCloseEditor(grid, rowIndex = this.reEditedRowIndex) {
    grid.closeRow(rowIndex);
    this.reEditedRowIndex = undefined;
    this.reFormGroup = undefined;
  }

  public reAddHandler({sender}) {
    if (!this.eqmSettings.canCreateRelatedOrders) {
      this.error('You do not have permission to add Related order.');
      return false;
    } else {
      this.reCloseEditor(sender);
      this.reEditDataItem = new CreateRelatedOrderDto();
      this.isNew = true;
    }
  }

  public reEditHandler({dataItem}) {
    this.reEditDataItem = dataItem;
    this.isNew = false;
  }

  public reSaveHandler(data) {
    this.reGridLoading = true;
    if (this.isNew) {
      const inputData: CreateRelatedOrderDto = data;

      inputData.isWorkCompleted = false;
      inputData.statusId = 1;
      inputData.active = true;
      inputData.parentOrderId = this.orderId;

      this.relatedOrderService.create(inputData,
        response => {
          this.toasterService.success('', 'Order Created Successfully');
          this.loadRelatedOrderGridData();
          this.reGridLoading = false;
        },
        error => {
          this.reGridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    else {
      const inputData: OrderDto = data;

      this.relatedOrderService.update(inputData,
        response => {
          this.toasterService.success('', 'Order Updated Successfully');
          this.loadRelatedOrderGridData();
          this.reGridLoading = false;
        },
        error => {
          this.reGridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  public reDeleteClose(status) {
    const $this = this;
    if (status) {
      this.reGridLoading = true;
      this.relatedOrderService.delete($this.reDeleteDataItem.orderId,
        response => {
          this.toasterService.success('', 'Order Removed Successfully');
          this.loadRelatedOrderGridData();
          this.reGridLoading = false;
        },
        error => {
          this.reGridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    $this.reDeleteDataItem = null;
    $this.reOpened = false;
  }

  public reCancelHandler(e) {
    this.reEditedRowIndex = undefined;
    this.reFormGroup = undefined;
    this.reEditDataItem = undefined;
  }

  public reRemoveHandler({dataItem}) {
    this.reDeleteDataItem = dataItem;
    this.reOpen();
  }

  public reOpen() {
    this.reOpened = true;
  }

  public reDataStateChange(state: DataStateChangeEvent): void {
    this.reState = state;
  }

  public reOnSearchKeyup() {
    this.reSkip = 0;
    this.rePageSize = 10;
    this.reGridLoading = true;
    this.loadRelatedOrderGridData();
  }

  public checkOrderReports(itm, equipmentId) {
    const index = this.allChecked.indexOf(equipmentId);    // <-- Not supported in <IE9

    // ADD CHECKED ID IN ARRAY
    if (itm === true && index === -1) {
      this.allChecked.push(equipmentId);
    }

    // REMOVE ID FROM ARRAY, BECAUSE IT'S UNCHECKED
    if (itm === false && index !== -1) {
      this.allChecked.splice(index, 1);
    }
  }

  public exportOrders() {
    alert(this.allChecked);
  }

  public workCompleteCheck() {
    if (this.editForm.controls.isWorkCompleted.value) {
      this.editForm.controls.completedDate.setValue(new Date());
      this.isWorkCompleted = true;
    } else {
      this.editForm.controls.completedDate.setValue(null);
      this.isWorkCompleted = false;
    }
  }

  public ActionData: Array<any> = [{
    text: 'Delete',
    icon: 'trash'
  }];

  public onAction(e, dataItem) {
    if (e === undefined) {
      this.reEditDataItem = dataItem;
      this.isNew = false;
    } else {
      if (e.text === 'Delete') {
        this.reDeleteDataItem = dataItem;
        this.reOpen();
      }
    }
  }
}
