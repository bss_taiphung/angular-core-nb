﻿// import { Component, Input, Output, EventEmitter } from '@angular/core';
// import { Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
// import "rxjs/add/observable/fromEvent";

// import { process, GroupDescriptor, State, aggregateBy } from '@progress/kendo-data-query';
// import { PageChangeEvent, GridComponent, GridDataResult, DataStateChangeEvent } from '@progress/kendo-angular-grid';
// import { SortDescriptor, orderBy } from '@progress/kendo-data-query';

// import { CommonService, SelectListDto } from "../../service/common.service";

// import { OrderPartService } from "../orderpart/service/orderpart.service"
// import { OrderPartDto } from "../orderpart/service/orderpart.model"

// @Component({
//     selector: 'kendo-grid-orderpart-form',
//     styles: [
//         'input[type=text] { width: 100%; }'
//     ],
//     templateUrl: './html/orderpart-form.component.html'
// })
// export class OrderPartFormComponent {
//     public active = false;

//     public orderpartForm: FormGroup = new FormGroup({
//         'orderPartId': new FormControl(),
//         'orderId': new FormControl(),
//         'partId': new FormControl('', Validators.required),
//         'consumedQuantity': new FormControl(''),
//         'active': new FormControl()
//     });

//     @Input() public isNew = false;
//     @Input() public set model(orderpart: OrderPartDto) {
//         if (orderpart != undefined) {
//             this.orderpartForm.reset(orderpart);
//             this.active = orderpart !== undefined;
//         }
//     }

//     @Output() cancel: EventEmitter<any> = new EventEmitter();
//     @Output() save: EventEmitter<OrderPartDto> = new EventEmitter();

//     public orderList: SelectListDto[];
//     public partList: SelectListDto[];

//     constructor(private commonService: CommonService) {
//         this.init();
//     }

//     init(): void {

//     }

//     ngOnInit() {
//         //this.commonService.getAll("EquipmentMaintenance", "Order").subscribe(eventResult => {
//         //    this.orderList = eventResult.results;
//         //});

//         this.commonService.getAll("EquipmentMaintenance", "part").subscribe(eventResult => {
//             this.partList = eventResult.results;
//         });
//     }   

//     public onSave(e): void {
//         e.preventDefault();
//         this.save.emit(this.orderpartForm.value);
//         this.active = false;
//     }

//     public onCancel(e): void {
//         e.preventDefault();
//         this.closeForm();
//     }

//     private closeForm(): void {
//         this.active = false;
//         this.cancel.emit();
//     }
//     public partHandleFilter(filter: any): void {
//         this.partList = this.partList.filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
//     }
// }