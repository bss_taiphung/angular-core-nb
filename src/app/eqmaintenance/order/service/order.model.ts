import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';

export interface IOrderDto {
  equipmentId: number;
  equipmentName: string;
  orderTypeName: string;
  priorityName: string;
  assignToName: string;
  orderTypeId: number;
  priorityId: number;
  assignedUserId: number;
  statusId: number;
  parentOrderId: number;
  pmScheduleId: number;
  isWorkCompleted: boolean;
  completedDate: string;
  isPlanned: boolean;
  dueDate: string;
  createdByName: string;
  description: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  orderId: number;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
  requestType: string;
  statusName: string;
  orderReport: boolean;
  priorityDescription: string;
}

export class OrderDto implements IOrderDto {
  equipmentId: number;
  equipmentName: string;
  orderTypeName: string;
  priorityName: string;
  assignToName: string;
  orderTypeId: number;
  priorityId: number;
  assignedUserId: number;
  statusId: number;
  parentOrderId: number;
  pmScheduleId: number;
  isWorkCompleted: boolean;
  completedDate: string;
  isPlanned: boolean;
  dueDate: string;
  createdByName: string;
  description: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  orderId: number;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
  requestType: string;
  statusName: string;
  orderReport: boolean;
  priorityDescription: string;

  constructor(data?: IOrderDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.equipmentId = data['equipmentId'];
      this.orderTypeId = data['orderTypeId'];
      this.priorityId = data['priorityId'];
      this.assignedUserId = data['assignedUserId'];

      this.equipmentName = data['equipmentName'];
      this.orderTypeName = data['orderTypeName'];
      this.priorityName = data['priorityName'];
      this.assignToName = data['assignToName'];

      this.statusId = data['statusId'];
      this.parentOrderId = data['parentOrderId'];
      this.pmScheduleId = data['pmScheduleId'];
      this.isWorkCompleted = data['isWorkCompleted'];
      this.completedDate = data['completedDate'];
      this.isPlanned = data['isPlanned'];

      this.dueDate = data['dueDate'];
      this.description = data['description'];
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.orderId = data['orderId'];
      this.siteId = data['siteId'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.active = data['active'];

      this.requestType = data['requestType'];
      this.statusName = data['statusName'];
      this.orderReport = data['orderReport'];
      this.priorityDescription = data['priorityDescription'];
    }
  }

  static fromJS(data: any): OrderDto {
    let result = new OrderDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['equipmentId'] = this.equipmentId;
    data['equipmentName'] = this.equipmentName;
    data['orderTypeName'] = this.orderTypeName;
    data['priorityName'] = this.priorityName;
    data['assignToName'] = this.assignToName;
    data['orderTypeId'] = this.orderTypeId;
    data['priorityId'] = this.priorityId;
    data['assignedUserId'] = this.assignedUserId;
    data['statusId'] = this.statusId;
    data['parentOrderId'] = this.parentOrderId;
    data['pmScheduleId'] = this.pmScheduleId;
    data['isWorkCompleted'] = this.isWorkCompleted;
    data['completedDate'] = this.completedDate;
    data['isPlanned'] = this.isPlanned;
    data['dueDate'] = this.dueDate;
    data['description'] = this.description;
    data['createdByName'] = this.createdByName;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['orderId'] = this.orderId;
    data['siteId'] = this.siteId;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;

    data['requestType'] = this.requestType;
    data['statusName'] = this.statusName;
    data['orderReport'] = this.orderReport;
    data['priorityDescription'] = this.priorityDescription;

    return data;
  }
}

// endregion

// region CreateOrderDto

export interface ICreateOrderDto {
  equipmentId: number;
  orderTypeId: number;
  priorityId: number;
  assignedUserId: number;
  description: string;
  dueDate: string;
  active: boolean;
  isWorkCompleted: boolean;
  statusId: number;
}

export class CreateOrderDto implements ICreateOrderDto {
  orderId = 0;
  equipmentId: number;
  orderTypeId: number;
  priorityId: number;
  assignedUserId: number;
  description: string;
  dueDate: string;
  active: boolean;
  isWorkCompleted: boolean;
  statusId: number;

  static fromJS(data: any): CreateOrderDto {
    const result = new CreateOrderDto();
    result.init(data);
    return result;
  }

  constructor(data?: ICreateOrderDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.orderId = 0;
      this.equipmentId = data['equipmentId'];
      this.orderTypeId = data['orderTypeId'];
      this.priorityId = data['priorityId'];
      this.assignedUserId = data['assignedUserId'];
      this.description = data['descrption'];
      this.dueDate = data['dueDate'];
      this.isWorkCompleted = data['isWorkCompleted'];
      this.statusId = data['statusId'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }


  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['orderId'] = 0;
    data['equipmentId'] = this.equipmentId;
    data['orderTypeId'] = this.orderTypeId;
    data['priorityId'] = this.priorityId;
    data['assignedUserId'] = this.assignedUserId;
    data['description'] = this.description;
    data['dueDate'] = this.dueDate;
    data['isWorkCompleted'] = this.isWorkCompleted;
    data['statusId'] = this.statusId;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

// endregion

// region PagedResultDtoOfOrderDto

export interface IPagedResultDtoOfOrderDto {
  results: OrderDto[];
  count: number;
}

export class PagedResultDtoOfOrderDto implements IPagedResultDtoOfOrderDto {
  results: OrderDto[];
  count: number;

  constructor(data?: IPagedResultDtoOfOrderDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (let item of data['results'])
          this.results.push(OrderDto.fromJS(item));
      }
      if (data['count']) {
        this.count = data['count'];
      }
    }
  }

  static fromJS(data: any): PagedResultDtoOfOrderDto {

    let result = new PagedResultDtoOfOrderDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (let item of this.results)
        data['results'].push(item.toJSON());
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion

// region EditOrderDto

export interface IEditOrderDto {
  equipmentId: number;
  orderTypeId: number;
  priorityId: number;
  assignedUserId: number;
  description: string;
  dueDate: string;
  active: boolean;
}

export class EditOrderDto implements IEditOrderDto {
  orderId = 0;
  equipmentId: number;
  orderTypeId: number;
  priorityId: number;
  assignedUserId: number;
  description: string;
  dueDate: string;
  active: boolean;

  static fromJS(data: any): EditOrderDto {
    const result = new EditOrderDto();
    result.init(data);
    return result;
  }

  constructor(data?: IEditOrderDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.orderId = 0;
      this.equipmentId = data['equipmentId'];
      this.orderTypeId = data['orderTypeId'];
      this.priorityId = data['priorityId'];
      this.assignedUserId = data['assignedUserId'];
      this.description = data['descrption'];
      this.dueDate = data['dueDate'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['orderId'] = 0;
    data['equipmentId'] = this.equipmentId;
    data['orderTypeId'] = this.orderTypeId;
    data['priorityId'] = this.priorityId;
    data['assignedUserId'] = this.assignedUserId;
    data['description'] = this.description;
    data['dueDate'] = this.dueDate;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

// endregion

// region OrderGetDto

export interface IOrderGetDto {
  orderId: number;
}

export class OrderGetDto implements IOrderGetDto {
  orderId = 0;

  static fromJS(data: any): OrderGetDto {
    const result = new OrderGetDto();
    result.init(data);
    return result;
  }

  constructor(data?: IOrderGetDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.orderId = data['orderId'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['orderId'] = this.orderId;
    return data;
  }
}

// endregion

// region ResultDtoOfOrderDto

export interface IResultDtoOfOrderDto {
  results: OrderDto[];
  count: number;
}

export class ResultDtoOfOrderDto implements IResultDtoOfOrderDto {
  results: OrderDto[];
  count: number;

  static fromJS(data: any): ResultDtoOfOrderDto {
    const result = new ResultDtoOfOrderDto();
    result.init(data);
    return result;
  }

  constructor(data?: IResultDtoOfOrderDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data && data.constructor === Array) {
        this.results = [];
        for (const item of data) {
          this.results.push(OrderDto.fromJS(item));
        }

      }

    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data = [];
      for (const item of this.results) {
        data.push(item.toJSON());
      }

    }

    return data;
  }
}

// endregion


// region CreateRelatedOrderDto

export interface ICreateRelatedOrderDto {
  equipmentId: number;
  orderTypeId: number;
  priorityId: number;
  assignedUserId: number;
  description: string;
  dueDate: string;
  active: boolean;
  isWorkCompleted: boolean;
  statusId: number;
  parentOrderId: number;
}

export class CreateRelatedOrderDto implements ICreateRelatedOrderDto {
  orderId: number = 0;
  equipmentId: number;
  orderTypeId: number;
  priorityId: number;
  assignedUserId: number;
  description: string;
  dueDate: string;
  active: boolean;
  isWorkCompleted: boolean;
  statusId: number;
  parentOrderId: number;


  static fromJS(data: any): CreateOrderDto {
    const result = new CreateOrderDto();
    result.init(data);
    return result;
  }

  constructor(data?: ICreateRelatedOrderDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.orderId = 0;
      this.equipmentId = data['equipmentId'];
      this.orderTypeId = data['orderTypeId'];
      this.priorityId = data['priorityId'];
      this.assignedUserId = data['assignedUserId'];
      this.description = data['descrption'];
      this.dueDate = data['dueDate'];
      this.isWorkCompleted = data['isWorkCompleted'];
      this.statusId = data['statusId'];
      this.parentOrderId = data['parentOrderId'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['orderId'] = 0;
    data['equipmentId'] = this.equipmentId;
    data['orderTypeId'] = this.orderTypeId;
    data['priorityId'] = this.priorityId;
    data['assignedUserId'] = this.assignedUserId;
    data['description'] = this.description;
    data['dueDate'] = this.dueDate;
    data['isWorkCompleted'] = this.isWorkCompleted;
    data['statusId'] = this.statusId;
    data['parentOrderId'] = this.parentOrderId;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

// endregion
