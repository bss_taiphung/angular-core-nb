﻿import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';


// region BulkOrderDto

export interface IBulkOrderDto {
  orderIds: number[];
  changeAssignedUser: boolean;
  assignedUserId: number;
  changeStatus: boolean;
  statusId: number;
  changePriority: boolean;
  priorityId: number;
  changeOrderType: boolean;
  orderTypeId: number;
  changeIsPlanned: boolean;
  isPlanned: boolean;
  changeDueDate: boolean;
  dueDate: string;
  changeIsWorkComplete: boolean;
  isWorkCompleted: boolean;
  changeCompletedDate: boolean;
  completedDate: string;
  modifiedBy: number;
}

export class BulkOrderDto implements IBulkOrderDto {
  orderIds: number[];
  changeAssignedUser: boolean;
  assignedUserId: number;
  changeStatus: boolean;
  statusId: number;
  changePriority: boolean;
  priorityId: number;
  changeOrderType: boolean;
  orderTypeId: number;
  changeIsPlanned: boolean;
  isPlanned: boolean;
  changeDueDate: boolean;
  dueDate: string;
  changeIsWorkComplete: boolean;
  isWorkCompleted: boolean;
  changeCompletedDate: boolean;
  completedDate: string;
  modifiedBy: number;

  static fromJS(data: any): BulkOrderDto {
    const result = new BulkOrderDto();
    result.init(data);
    return result;
  }

  constructor(data?: IBulkOrderDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.orderIds = data['orderIds'];
      this.changeAssignedUser = data['changeAssignedUser'];
      this.assignedUserId = data['assignedUserId'];
      this.changeStatus = data['changeStatus'];
      this.statusId = data['statusId'];
      this.changePriority = data['changePriority'];
      this.priorityId = data['priorityId'];
      this.changeOrderType = data['changeOrderType'];
      this.orderTypeId = data['orderTypeId'];
      this.changeIsPlanned = data['changeIsPlanned'];
      this.isPlanned = data['isPlanned'];
      this.changeDueDate = data['changeDueDate'];
      this.dueDate = data['dueDate'];
      this.changeIsWorkComplete = data['changeIsWorkComplete'];
      this.isWorkCompleted = data['isWorkCompleted'];
      this.changeCompletedDate = data['changeCompletedDate'];
      this.completedDate = data['completedDate'];
      this.modifiedBy = data['modifiedBy'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['orderIds'] = this.orderIds;
    data['changeAssignedUser'] = this.changeAssignedUser;
    data['assignedUserId'] = this.assignedUserId;
    data['changeStatus'] = this.changeStatus;
    data['statusId'] = this.statusId;
    data['changePriority'] = this.changePriority;
    data['priorityId'] = this.priorityId;
    data['changeOrderType'] = this.changeOrderType;
    data['orderTypeId'] = this.orderTypeId;
    data['changeIsPlanned'] = this.changeIsPlanned;
    data['isPlanned'] = this.isPlanned;
    data['changeDueDate'] = this.changeDueDate;
    data['dueDate'] = this.dueDate;
    data['changeIsWorkComplete'] = this.isWorkCompleted;
    data['isWorkCompleted'] = this.isWorkCompleted;
    data['changeCompletedDate'] = this.changeCompletedDate;
    data['completedDate'] = this.completedDate;
    data['modifiedBy'] = this.modifiedBy;
    return data;
  }
}

// endregion

// region EditBulkOrderDto
export interface IEditBulkOrderDto {
  orderIds: number[];
  changeAssignedUser: boolean;
  assignedUserId: number;
  changeStatus: boolean;
  statusId: number;
  changePriority: boolean;
  priorityId: number;
  changeOrderType: boolean;
  orderTypeId: number;
  changeIsPlanned: boolean;
  isPlanned: boolean;
  changeDueDate: boolean;
  dueDate: string;
  changeIsWorkComplete: boolean;
  isWorkCompleted: boolean;
  changeCompletedDate: boolean;
  completedDate: string;
  modifiedBy: number;
}

export class EditBulkOrderDto implements IEditBulkOrderDto {
  orderIds: number[];
  changeAssignedUser: boolean;
  assignedUserId: number;
  changeStatus: boolean;
  statusId: number;
  changePriority: boolean;
  priorityId: number;
  changeOrderType: boolean;
  orderTypeId: number;
  changeIsPlanned: boolean;
  isPlanned: boolean;
  changeDueDate: boolean;
  dueDate: string;
  changeIsWorkComplete: boolean;
  isWorkCompleted: boolean;
  changeCompletedDate: boolean;
  completedDate: string;
  modifiedBy: number;

  static fromJS(data: any): EditBulkOrderDto {
    const result = new EditBulkOrderDto();
    result.init(data);
    return result;
  }

  constructor(data?: IEditBulkOrderDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.orderIds = data['orderIds'];
      this.changeAssignedUser = data['changeAssignedUser'];
      this.assignedUserId = data['assignedUserId'];
      this.changeStatus = data['changeStatus'];
      this.statusId = data['statusId'];
      this.changePriority = data['changePriority'];
      this.priorityId = data['priorityId'];
      this.changeOrderType = data['changeOrderType'];
      this.orderTypeId = data['orderTypeId'];
      this.changeIsPlanned = data['changeIsPlanned'];
      this.isPlanned = data['isPlanned'];
      this.changeDueDate = data['changeDueDate'];
      this.dueDate = data['dueDate'];
      this.changeIsWorkComplete = data['changeIsWorkComplete'];
      this.isWorkCompleted = data['isWorkCompleted'];
      this.changeCompletedDate = data['changeCompletedDate'];
      this.completedDate = data['completedDate'];
      this.modifiedBy = data['modifiedBy'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['orderIds'] = this.orderIds;
    data['changeAssignedUser'] = this.changeAssignedUser;
    data['assignedUserId'] = this.assignedUserId;
    data['changeStatus'] = this.changeStatus;
    data['statusId'] = this.statusId;
    data['changePriority'] = this.changePriority;
    data['priorityId'] = this.priorityId;
    data['changeOrderType'] = this.changeOrderType;
    data['orderTypeId'] = this.orderTypeId;
    data['changeIsPlanned'] = this.changeIsPlanned;
    data['isPlanned'] = this.isPlanned;
    data['changeDueDate'] = this.changeDueDate;
    data['dueDate'] = this.dueDate;
    data['changeIsWorkComplete'] = this.isWorkCompleted;
    data['isWorkCompleted'] = this.isWorkCompleted;
    data['changeCompletedDate'] = this.changeCompletedDate;
    data['completedDate'] = this.completedDate;
    data['modifiedBy'] = this.modifiedBy;
    return data;
  }
}

// endregion
