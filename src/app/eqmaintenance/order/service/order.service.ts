import {Injectable} from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';

import {
  OrderGetDto,
  PagedResultDtoOfOrderDto,
  OrderDto, CreateOrderDto
} from './order.model'
import {CommonService} from '../../../shared/services/common.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class OrderService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private http: HttpClient,
              private commonService: CommonService) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/equipmentmaintenance/api/order/';
  }

  get(input: OrderGetDto, successCallback: any, errorCallback: any): any {

    const url = this.apiBaseUrl + 'GetById';

    const params = new HttpParams().set('id', input.orderId.toString());

    const options = {
      params: params
    };

    this.commonService.httpRequest('get', url, options, successCallback, errorCallback);
  }

  create(input: CreateOrderDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'InsertAsync';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  update(input: OrderDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'UpdateAsync';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  delete(id: any, successCallback: any, errorCallback: any): any {

    const url = this.apiBaseUrl + 'DeleteAsync';
    const params = new HttpParams().set('id', id);
    const options = {
      params: params
    };

    this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
  }

  getAll(take: number,
         skip: number,
         sort: any,
         isactive: boolean,
         searchtext: string,
         showAssignedUserId: number,
         filter: string = ''): Observable<PagedResultDtoOfOrderDto> {
    const url = this.apiBaseUrl + 'GetPagedQuery';
    // Order by Clause
    let orderBy = '';
    if (sort !== undefined && sort.length > 0) {
      for (const item of sort) {
        orderBy = this.commonService.firstUpper(item.field) + ' ' + (item.dir === undefined ? 'asc' : item.dir);
      }
    } else {
      orderBy = 'OrderId desc';
    }

    if (isactive) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += 'Active eq true';
    }
    if (searchtext.length > 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += `EquipmentName eq ${searchtext} `;
    }
    if (showAssignedUserId > 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += 'AssignedUserId eq ' + showAssignedUserId;
    }

    const params = new HttpParams()
      .set('$top', take.toString())
      .set('$skip', skip.toString())
      .set('$count', 'true')
      .set('$orderby', orderBy.toString())
      .set('$filter', filter.toString());

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      let result200 = PagedResultDtoOfOrderDto.fromJS(response);
      return Observable.of(result200);
    });
  }

  getAllWtihFilter(take: number,
                   skip: number,
                   sort: any,
                   showAssignedUserId: number,
                   filter: string): Observable<PagedResultDtoOfOrderDto> {
    const url = this.apiBaseUrl + 'GetPagedQuery';
    // Order by Clause
    let orderBy = '';
    if (sort !== undefined && sort.length > 0) {
      for (const item of sort) {
        orderBy = this.commonService.firstUpper(item.field) + ' ' + (item.dir === undefined ? 'asc' : item.dir);
      }
    } else {
      orderBy = 'OrderId desc';
    }

    if (showAssignedUserId > 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += 'AssignedUserId eq ' + showAssignedUserId;
    }

    console.log(filter);

    const params = new HttpParams()
      .set('$top', take.toString())
      .set('$skip', skip.toString())
      .set('$count', 'true')
      .set('$orderby', orderBy.toString())
      .set('$filter', filter.toString());


    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      const result200 = PagedResultDtoOfOrderDto.fromJS(response);
      return Observable.of(result200);
    });
  }

  bulkUpdate(input: any, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'BulkUpdate';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  bulkDelete(input: any, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'BulkDelete';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }
}

