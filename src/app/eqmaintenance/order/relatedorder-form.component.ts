import {
  Component,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import {
  Validators,
  FormGroup,
  FormControl
} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';


import {CommonService} from '../../shared/services/common.service';
import {SelectListDto} from '../../shared/models/selectListDto'
import {OrderDto} from '../order/service/order.model'

@Component({
  selector: 'kendo-grid-relatedorder-form',
  styles: [
    'input[type=text] { width: 100%; }'
  ],
  templateUrl: './html/relatedorder-form.component.html'
})
export class RelatedOrderFormComponent {
  public active = false;

  public relatedorderForm: FormGroup = new FormGroup({
    'orderId': new FormControl(),
    'statusId': new FormControl(),
    'equipmentId': new FormControl('', Validators.required),
    'orderTypeId': new FormControl('', Validators.required),
    'priorityId': new FormControl('', Validators.required),
    'dueDate': new FormControl(),
    'isWorkCompleted': new FormControl(false),
    'active': new FormControl(false),
  });

  @Input() public isNew = false;

  @Input()
  public set model(relatedorder: any) {
    if (relatedorder !== undefined) {

      if (relatedorder.dueDate !== (null || undefined)) {
        relatedorder.dueDate = new Date(relatedorder.dueDate);
      }
      else {
        relatedorder.dueDate = new Date();
      }

      this.relatedorderForm.reset(relatedorder);
      this.active = relatedorder !== undefined;
    }
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<OrderDto> = new EventEmitter();

  public statusList: SelectListDto[];
  public equipmentList: SelectListDto[];
  public orderTypeList: SelectListDto[];
  public priorityList: SelectListDto[];
  public assignedUserList: SelectListDto[];

  constructor(private commonService: CommonService) {
    this.init();
  }

  init(): void {

  }

  ngOnInit() {

    this.commonService.getAll('General', 'User').subscribe(eventResult => {
      this.assignedUserList = eventResult.results;
    });

    this.commonService.getAll('EquipmentMaintenance', 'Status').subscribe(eventResult => {
      this.statusList = eventResult.results;
    });

    this.commonService.getAll('EquipmentMaintenance', 'Equipment').subscribe(eventResult => {
      this.equipmentList = eventResult.results;
    });

    this.commonService.getAll('EquipmentMaintenance', 'OrderType').subscribe(eventResult => {
      this.orderTypeList = eventResult.results;
    });

    this.commonService.getAll('EquipmentMaintenance', 'Priority').subscribe(eventResult => {
      this.priorityList = eventResult.results;
    });

  }

  public onSave(e): void {
    e.preventDefault();
    this.save.emit(this.relatedorderForm.value);
    this.active = false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.active = false;
    this.cancel.emit();
  }
}
