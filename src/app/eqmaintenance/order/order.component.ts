import {
  Component,
  ViewEncapsulation,
  OnInit,
  HostListener
} from '@angular/core';
import {Router} from '@angular/router';
import 'rxjs/add/observable/fromEvent';
import {Observable} from 'rxjs/Observable';


import {State} from '@progress/kendo-data-query';
import {
  PageChangeEvent,
  GridDataResult,
  DataStateChangeEvent,
  SelectAllCheckboxState
} from '@progress/kendo-angular-grid';
import {FormGroup} from '@angular/forms';
import {OrderService} from './service/order.service'
import {
  IOrderDto,
  OrderDto,
  CreateOrderDto,
  EditOrderDto
} from './service/order.model'
import {UserSettingsDto} from '../../shared/usersettings/usersetting.model';
import {UserSettingsService} from '../../shared/usersettings/usersetting.service';
import {SortDescriptor} from '@progress/kendo-data-query';
import {CommonService} from '../../shared/services/common.service';
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {HubConnection} from '@aspnet/signalr-client';
import {SelectListDto} from '../../shared/models/selectListDto'
import {EqmSettings, User, UserRole} from '../../shared/models/eqmSettings'
import {environment} from '../../../environments/environment';


@Component({
  selector: 'order',
  templateUrl: './html/order.component.html',
  styleUrls: ['./css/order.component.css'],
  encapsulation: ViewEncapsulation.None,
})


export class OrderComponent implements OnInit {

  data: IOrderDto[];
  public siteList: SelectListDto[];

  public formGroup: FormGroup;
  public view: Observable<GridDataResult>;
  private editedRowIndex: number;

  public createDataItem: CreateOrderDto;
  public editDataItem: EditOrderDto;

  public isNew: boolean;
  public pageSize = 10;
  public skip = 0;
  public state: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };

  viewerContainerStyle = {
    position: 'relative',
    width: '1000px',
    height: '800px',
    ['font-family']: 'ms sans serif'
  };

  isBrowser: boolean;

  public gridView: GridDataResult;
  public deleteDataItem: any;
  private sort: SortDescriptor[] = [];
  public isactive: boolean = true;
  public searchtext: string = '';
  public allChecked = new Array();
  public openedReport: boolean = false;
  public openedBulkEdit: boolean = false;
  public showAssignedUserId: number = 0;
  public isShowOnlyMyAssignments: boolean = false;
  eqmSettings: EqmSettings = new EqmSettings();
  user: User = new User();
  userRole: UserRole = new UserRole();
  public mss: any;
  public isOrderSelected = true;
  public canEditOrders: boolean = false;
  public openReportViewer: boolean = false;
  private hubConnection: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  public openDetailViewer: boolean = false;
  public order: EditOrderDto;

  constructor(private orderService: OrderService,
              private commonService: CommonService,
              private router: Router,
              private eqmSettingsService: EqmSettingsService,
              private userSettingsService: UserSettingsService,
              private toasterService: ToasterHelperService) {
    this.mss = this.commonService.getCookies('Mss');
    this.eqmSettings = this.eqmSettingsService.getEqmSettings();
    this.user = this.eqmSettingsService.getUser();
    this.userRole = this.eqmSettingsService.getUserRoles();
    this.isShowOnlyMyAssignments = this.eqmSettings.showOnlyMyAssignments;
    this.canEditOrders = this.eqmSettings.canEditOrders;
  }

  ngOnInit() {
  }

  signalRConnection() {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');

        this.hubConnection
          .invoke('Subscribe', 'EquipmentMaintenance', 'Order', 'SiteId', this.eqmSettingsService.getEqmSettings().currentSiteId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
      const _responseText = messageJson;
      let result200: any = null;
      let data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      let resultData200 = this.commonService.toCamel(data);
      result200 = resultData200 ? OrderDto.fromJS(resultData200) : new OrderDto();

      var dataExist = this.containsObject(result200, this.gridView.data);

      if (operation.toLowerCase() === 'insert' && !dataExist) {
        this.gridLoading = false;
        this.gridView.data.unshift(result200);
        this.gridView = {
          data: this.gridView.data,
          total: (this.gridView.total + 1)
        };
      }

      if (operation.toLowerCase() === 'update') {
        this.gridLoading = false;
        this.gridView.data.forEach((element, index) => {
          if (element.orderId === result200.orderId) {
            if (this.isactive) {
              if (result200.active == this.isactive) {
                this.gridView.data[index] = result200;
              } else {
                operation = 'delete';
              }
            } else {
              this.gridView.data[index] = result200;
            }
          }
        });
      }

      if (operation.toLowerCase() === 'delete' && dataExist) {
        this.gridLoading = false;
        var index = null;
        this.gridView.data.forEach((element, i) => {
          if (element.orderId === result200.orderId) {
            index = i;
          }
        });
        if (index !== null) {
          this.gridView.data.splice(index, 1);
          this.gridView = {
            data: this.gridView.data,
            total: (this.gridView.total - 1)
          };
        }
      }
    });
  }

  containsObject(obj, list) {
    var x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].orderId === obj.orderId) {
        return true;
      }
    }

    return false;
  }

  // Dialog box
  opened: boolean = false;
  loading: boolean = true;
  gridLoading: boolean = true;

  public close(status) {

    var $this = this;
    if (status) {
      this.gridLoading = true;
      this.orderService.delete($this.deleteDataItem.orderId,
        response => {
          this.toasterService.success('', 'Order Removed Successfully');
          // this.loadGridData();
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    $this.deleteDataItem = null;
    $this.opened = false;
  }

  public open() {
    this.opened = true;
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.loadGridData();
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
  }

  private loadGridData(): void {
    this.gridLoading = true;
    if (!this.isShowOnlyMyAssignments) {
      this.showAssignedUserId = 0;
    } else {
      this.showAssignedUserId = this.user.userId;
    }

    this.orderService.getAll(this.pageSize, this.skip, this.sort, this.isactive, this.searchtext, this.showAssignedUserId)
      .subscribe(eventResult => {
        if (this.allChecked !== (null || undefined) && this.allChecked.length > 0) {
          this.allChecked.forEach((checkedEquipment => {
            eventResult.results.forEach((itm => {
              if (checkedEquipment === itm.equipmentId) {
                itm.orderReport = true;
              }
            }));
          }));
        }

        if (eventResult.results.length > 0) {
          this.gridView = {
            data: eventResult.results,
            total: eventResult.count
          };
        } else {
          this.gridView = {
            data: eventResult.results,
            total: 0
          };
        }

        this.gridLoading = false;
        this.loading = false;
      });
  }

  public addHandler({sender}) {
    this.editDataItem = new OrderDto();
    this.isNew = true;
  }

  public editHandler({dataItem}) {
    //this.editDataItem = dataItem;
    //this.isNew = false;
    //this.router.navigate(['eqmaintenance/orderdetail/', dataItem.orderId]);
  }

  public saveHandler(data) {
    if (this.isNew) {
      data.active = (data.active == null) ? false : data.active;
      const inputData: CreateOrderDto = data;

      inputData.orderId = 0;
      inputData.isWorkCompleted = false;
      inputData.statusId = 1;
      inputData.active = true;

      this.orderService.create(inputData,
        response => {
          if (response !== null) {
            this.toasterService.success('', 'Order Saved Successfully');
          } else {
            this.toasterService.error('', 'Order Saved failed');
          }

          //this.loadGridData();
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
    else {
      const inputData: OrderDto = data;
      this.orderService.update(inputData,
        response => {
          if (response !== null) {
            this.toasterService.success('', 'Order Updated Successfully');
          } else {
            this.toasterService.error('', 'Order Saved failed');
          }
          ;
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public cancelHandler() {

    this.editDataItem = undefined;
    this.formGroup = undefined;
  }

  public removeHandler({dataItem}) {
    this.deleteDataItem = dataItem;
    this.open();
  }

  public showActive() {
    this.isactive = !this.isactive;
    this.gridLoading = true;

    this.doSearch(this.state, this);
  }

  public onSearchKeyup() {
    this.skip = 0;
    this.pageSize = 10;
    this.gridLoading = true;
    this.loadGridData();
  }

  public orderSelection: number[] = [];
  public selectAllState: SelectAllCheckboxState = 'unchecked';

  public onSelectedKeysChange(e) {
    const len = this.orderSelection.length;
    if (len === 0) {
      this.selectAllState = 'unchecked';
      this.isOrderSelected = true;
    } else if (len > 0 && len < this.gridView.data.length) {
      this.selectAllState = 'indeterminate';
      this.isOrderSelected = false;
    } else {
      this.selectAllState = 'checked';
      this.isOrderSelected = false;
    }
  }

  public onSelectAllChange(checkedState: SelectAllCheckboxState) {
    if (checkedState === 'checked') {
      this.orderSelection = this.gridView.data.map((item) => item.orderId);
      this.selectAllState = 'checked';
    } else {
      this.orderSelection = [];
      this.selectAllState = 'unchecked';
      this.isOrderSelected = true;
    }
  }

  public exportOrders() {

    const len = this.orderSelection.length;
    if (len === 0) {
      this.toasterService.error('', 'Please select one or more orders first.');
      return false;
    }

    this.openReportViewer = true;
  }

  // Closes export orders dialog
  public closeReportViewer(data) {
    this.openReportViewer = false;

    this.orderSelection = [];

    this.loadGridData();
  }

  public closeReport(status) {
    this.openedReport = false;
  }

  public updateSelected() {

    const len = this.orderSelection.length;
    if (len === 0) {
      this.toasterService.error('', 'Please select one or more orders first.');
      return false;
    }

    this.openedBulkEdit = true;
  }

  public closeBulkEdit(event) {
    this.openedBulkEdit = false;
    this.orderSelection = [];
    this.loadGridData();
  }

  public ActionData: Array<any> = [{
    text: 'Delete',
    icon: 'trash'
  }];

  public onAction(e, dataItem) {
    if (e === undefined) {
      this.openDetailViewer = true;
      this.order = dataItem;
    } else {
      if (e.text === 'Delete') {
        if (!this.eqmSettings.canEditOrders) {
          this.toasterService.error('You do not have permission to remove order.', '');
          return false;
        } else {
          this.deleteDataItem = dataItem;
          this.open();
        }
      }
    }
  }

  public showOnlyMyAssignments() {
    this.gridLoading = true;
    this.isShowOnlyMyAssignments = !this.isShowOnlyMyAssignments;

    var data = new UserSettingsDto();

    data.active = true;
    data.roleId = this.eqmSettings.roleId; //this role should be specific of eqm module, and not general role
    data.userId = this.user.userId;
    data.userSettingsId = this.eqmSettings.userSettingsId;
    data.currentSiteId = this.eqmSettings.currentSiteId;
    data.showOnlyMyAssignments = this.isShowOnlyMyAssignments;

    this.userSettingsService.update(data,
      response => {
        this.toasterService.success('', 'User Setting Saved Successfully');

        //update cookies to take new changes
        this.eqmSettingsService.userSettings().subscribe();

        this.loadGridData();
        this.gridLoading = false;
      },
      error => {
        this.gridLoading = false;
        this.toasterService.errorMessage(error);
      }
    );
  }

  timeout = null;

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridLoading = true;

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.doSearch(state, this);
    }, 1000);
  }

  doSearch(state: any, that) {
    if (state.filter) {
      var search = that.commonService.getFilter(state.filter.filters, that.isactive);

      //search = "Active eq true and date(DueDate) lt 2018-03-03T00:00:00.000Z";
      that.orderService.getAllWtihFilter(state.take, state.skip, that.sort, that.showAssignedUserId, search)
        .subscribe(eventResult => {
          that.gridView = {
            data: eventResult.results,
            total: eventResult.count
          };
          that.loading = false;
          that.gridLoading = false;
        });

      this.skip = state.skip;
      this.pageSize = state.take;
    }
    else {
      that.loadGridData();
    }
  }

  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {
      this.cancelHandler();
      this.close(false);
      this.closeBulkEdit(null);
      this.closeReportViewer(false);
      this.closeReport(false);
    }
  }

  public siteSelectionChange(e): void {
    this.signalRConnection();
    this.loadGridData();
  }


  public closeDetailViewer(data) {
    this.openDetailViewer = false;
    this.order = new OrderDto();
  }
}
