import {
  Component,
  ViewEncapsulation,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import {Router} from '@angular/router';
import {
  Validators,
  FormGroup,
  FormControl
} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';

import {State} from '@progress/kendo-data-query';
import {
  PageChangeEvent,
  GridDataResult,
  DataStateChangeEvent
} from '@progress/kendo-angular-grid';
import {SortDescriptor} from '@progress/kendo-data-query';

import {OrderService} from './service/order.service'
import {OrderDto, CreateRelatedOrderDto} from './service/order.model'
import {CommonService} from '../../shared/services/common.service';
import {SelectListDto} from '../../shared/models/selectListDto'
import {EqmSettings, User} from '../../shared/models/eqmSettings'

import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';

import {OrderPartService} from '../orderpart/service/orderpart.service'
import {OrderPartDto} from '../orderpart/service/orderpart.model'

import {WorkRecordService} from '../workrecord/service/workrecord.service'
import {WorkRecordDto} from '../workrecord/service/workrecord.model'

import {RelatedOrderService} from './service/relatedorder.service'
import {CreatePartDto} from '../part/service/part.model';
import {PartService} from '../part/service/part.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'kendo-detail-viewer',
  templateUrl: './html/detailedtorder.component.html',
  styleUrls: ['./css/order.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DetailedOrderComponent {
  viewerContainerStyle = {
    position: 'relative',
    width: '100%',
    height: '800px',
    ['font-family']: 'ms sans serif'
  };

  @Input() public openDetailViewer = false;

  @Input()
  public set model(order: any) {

    if (order && order.orderId) {
      this.order = order;
      this.init();
    }
  }

  @Output() closeDetailViewer: EventEmitter<any> = new EventEmitter();
  order: any;
  eqmSettings: EqmSettings = new EqmSettings();
  user: User = new User();

  public azureStorageContainer: string;
  public pageSaving = false;
  public isfailureTypeEditable = true;
  public isWorkCompleted = false;
  public isOrderDelete = false;
  public isAssignedUser = false;
  public hasRightsToUpdateOrderStatus = false;

  public statusList: SelectListDto[];
  public statusListVm: SelectListDto[];
  public equipmentList: SelectListDto[];
  public equipmentListVm: SelectListDto[];
  public orderTypeList: SelectListDto[];
  public orderTypeListVm: SelectListDto[];
  public priorityList: SelectListDto[];
  public priorityListVm: SelectListDto[];
  public partList: SelectListDto[];
  public partListVm: SelectListDto[];
  public failureTypeList: SelectListDto[];
  public failureTypeListVm: SelectListDto[];

  public filterassignedUserList: SelectListDto[];
  public filterassignedUserListVm: SelectListDto[];

  partData: OrderPartDto[] = [];
  public isNewPart = false;
  public isPartDelete = false;
  public partDeleteItem: OrderPartDto;
  private partLoading = true;
  public partPageSize = 10;
  public partSkip = 0;
  private partSort: SortDescriptor[] = [];
  public isactive = false;
  public searchtext = '';

  wrData: WorkRecordDto[] = [];
  public isNewWr = false;
  public isWrDelete = false;
  public wrDeleteDataItem: WorkRecordDto;
  public wrLoading = true;

  public isNew: boolean;
  public opened = false;
  public reFormGroup: FormGroup;
  public reEditedRowIndex: number;
  public reEditDataItem: CreateRelatedOrderDto;
  public rePageSize = 10;
  public reSkip = 0;
  public reGridView: GridDataResult;
  public reDeleteDataItem: any;
  public reSort: SortDescriptor[] = [];
  public reIsactive = true;
  public reSearchtext = '';
  public reOpened = false;
  public reLoading = true;
  public reGridLoading = true;
  public addNewPartIfNotExist: any;

  public reState: State = {
    skip: 0,
    take: 5
  };

  public editForm: FormGroup = new FormGroup({
    'orderId': new FormControl(),
    'statusId': new FormControl(),
    'equipmentId': new FormControl('', Validators.required),
    'assignedUserId': new FormControl(),
    'orderTypeId': new FormControl('', Validators.required),
    'priorityId': new FormControl('', Validators.required),
    'failureTypeId': new FormControl(),
    'dueDate': new FormControl(''),
    'completedDate': new FormControl(''),
    'isWorkCompleted': new FormControl(),
    'isPlanned': new FormControl(),
    'active': new FormControl(),
    'description': new FormControl(),
    'requestType': new FormControl()
  });

  constructor(private orderService: OrderService,
              private router: Router,
              private relatedOrderService: RelatedOrderService,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private toasterService: ToasterHelperService,
              private orderPartService: OrderPartService,
              private workRecordService: WorkRecordService,
              private partService: PartService) {
    this.eqmSettings = this.eqmSettingsService.getEqmSettings();
    this.user = this.eqmSettingsService.getUser();
    this.azureStorageContainer = environment.azureStorageContainer;
  }

  init(): void {
    this.resetFlags();
    this.loadAllDropDowns();
    this.setFormData(this.order);
    this.workCompleteCheck();

    if (this.order.assignedUserId === this.user.userId) {
      this.isAssignedUser = true;
    }

    if (this.order.statusId === 8) { // CLOSED ORDER
      if (this.eqmSettings.canEditOrders) {
        this.hasRightsToUpdateOrderStatus = true;
      } else {
        this.hasRightsToUpdateOrderStatus = false;
      }
    }
    else if (this.isAssignedUser || this.eqmSettings.canWorkOnUnassignedOrders) {
      this.hasRightsToUpdateOrderStatus = true;
    }

    // LOAD PART VIEW
    this.loadParts();

    // LOAD WORK HISTORY VIEW
    this.loadWorkRecords();

    // LOAD RELATED ORDERS GRID
    this.loadRelatedOrderGridData();
  }

  resetFlags() {
    this.partLoading = true;
    this.wrLoading = true;
    this.reLoading = true;
    this.isAssignedUser = false;
    this.hasRightsToUpdateOrderStatus = false;
  }

  loadAllDropDowns() {
    this.commonService.getAll('General', 'User').subscribe(eventResult => {
      this.filterassignedUserListVm = eventResult.results;
      this.filterassignedUserList = this.filterassignedUserListVm = eventResult.results;
    });

    this.commonService.getAll('EquipmentMaintenance', 'Status')
      .subscribe(eventResult => {
        this.statusListVm = eventResult.results;
        this.statusList = this.statusListVm = eventResult.results;
      });

    this.commonService.getAll('EquipmentMaintenance', 'Equipment')
      .subscribe(eventResult => {
        this.equipmentListVm = eventResult.results;
        this.equipmentList = this.equipmentListVm = eventResult.results;
      });

    this.commonService.getAll('EquipmentMaintenance', 'OrderType')
      .subscribe(eventResult => {
        this.orderTypeListVm = eventResult.results;
        this.orderTypeList = this.orderTypeListVm = eventResult.results;
      });

    this.commonService.getAll('EquipmentMaintenance', 'Priority')
      .subscribe(eventResult => {
        this.priorityListVm = eventResult.results;
        this.priorityList = this.priorityListVm = eventResult.results;
      });

    this.commonService.getAll('EquipmentMaintenance', 'part')
      .subscribe(eventResult => {
        this.partListVm = eventResult.results;
        this.partList = this.partListVm = eventResult.results;
      });

    this.commonService.getAll('EquipmentMaintenance', 'FailureType')
      .subscribe(eventResult => {
        this.failureTypeListVm = eventResult.results;
        this.failureTypeList = this.failureTypeListVm = eventResult.results;
      });
  }

  setFormData(order) {
    if (order !== (null || undefined)) {
      this.enableDisableFailureTypeDropdown(this.user.userId, order.assignedUserId, this.eqmSettings.canWorkOnUnassignedOrders);

      if (order.dueDate !== (null || undefined)) {
        order.dueDate = new Date(order.dueDate);
      }
      else {
        order.dueDate = new Date();
      }

      if (order.completedDate !== (null || undefined)) {
        order.completedDate = new Date(order.completedDate);
      }
      else {
        order.completedDate = new Date();
      }
    }

    this.editForm.reset(order);
  }

  public enableDisableFailureTypeDropdown(loggedInUserId: number,
                                          assignedUserId: number,
                                          workOnUnassignedOrders: boolean) {
    if ((loggedInUserId === assignedUserId) || workOnUnassignedOrders) {
      this.isfailureTypeEditable = false;
    }
  }

  public onSave(e): void {
    this.pageSaving = true;

    e.preventDefault();

    const inputData: OrderDto = this.editForm.value;
    this.orderService.update(inputData,
      response => {
        this.pageSaving = false;
        this.toasterService.success('', 'Order updated Successfully');
      },
      error => {
        this.pageSaving = false;
        this.toasterService.errorMessage(error);
      }
    );
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.partData = new Array<OrderPartDto>();
    this.wrData = new Array<WorkRecordDto>();
    this.reEditDataItem = undefined;
    //this.reGridView = {
    //  data: new Array(),
    //  total: 0
    //};

    this.openDetailViewer = false;
    this.closeDetailViewer.emit();
  }

  public statusHandleFilter(filter: any): void {
    this.statusList = this.statusListVm
      .filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  public assignedUserHandleFilter(filter: any): void {
    this.filterassignedUserList = this.filterassignedUserListVm
      .filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  public priorityHandleFilter(filter: any): void {
    this.priorityList = this.priorityListVm
      .filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  public equipmentHandleFilter(filter: any): void {
    this.equipmentList = this.equipmentListVm
      .filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  public orderTypeHandleFilter(filter: any): void {
    this.orderTypeList = this.orderTypeListVm
      .filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  public partHandleFilter(filter: any): void {
    this.addNewPartIfNotExist = filter;
    this.partList = this.partListVm.filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  // Add new part if not exist
  addNewPart(objPart: any) {
    this.partLoading = true;
    const data: CreatePartDto = new CreatePartDto();
    data.partName = this.addNewPartIfNotExist;
    data.isStockItem = true;
    this.partService.create(data, (result: any) => {
      const obj: any = {text: result['partName'], value: result['partId']};
      this.partListVm.push(obj);
      this.partList = this.partListVm;
      objPart.partId = result['partId'];
      this.toasterService.success('', 'Part Added Successfully');
      this.partLoading = false;
    }, (error) => {
      this.toasterService.error('', 'Ops something went wrong');
      this.partLoading = false;
    });
  }

  public failureTypeHandleFilter(filter: any): void {
    this.failureTypeList = this.failureTypeListVm.filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  // AssignedUserId dropdown change event
  public selectionChange(value: any): void {
    this.enableDisableFailureTypeDropdown(this.user.userId, value, this.eqmSettings.canWorkOnUnassignedOrders);
  }

  public orderDeleteClose() {
    const $this = this;
    $this.isOrderDelete = true;
  }

  public removeOrder(e, status): void {

    this.pageSaving = true;

    if (e) {
      e.preventDefault();
    }

    if (status) {

      const inputData: OrderDto = this.editForm.value;

      this.orderService.delete(inputData.orderId,
        response => {
          this.toasterService.success('', 'Order Removed Successfully');
          this.pageSaving = false;
          this.closeForm();
        },
        error => {
          this.pageSaving = false;
          this.toasterService.errorMessage(error);
        }
      );
    } else {
      this.pageSaving = false;
    }

    const $this = this;
    $this.isOrderDelete = false;
  }

  error(message: string) {
    this.toasterService.error('Not authorized.', message);
  }

  public workCompleteCheck() {
    if (this.editForm.controls.isWorkCompleted.value) {
      this.editForm.controls.completedDate.setValue(new Date());
      this.isWorkCompleted = true;
    } else {
      this.isWorkCompleted = false;
    }
  }

  updatePartUrl(data: OrderPartDto) {
    data.partUserAvatar = 'assets/img/no_avatar.png';
  }

  private loadParts(): void {
    this.orderPartService.getAll(
      this.partPageSize,
      this.partSkip,
      this.partSort,
      this.isactive,
      this.searchtext,
      this.order.orderId)
      .subscribe(eventResult => {

        this.partData = eventResult.results;

        this.partData.forEach(itm => {
          itm.partUserAvatar = this.commonService.getUserThumbnail(itm.createdBy.toString());
        });

        this.partLoading = false;
      });
  }

  addPart() {
    const input: OrderPartDto = new OrderPartDto();
    input.partUserAvatar = this.commonService.getUserThumbnail(this.user.userId.toString());
    this.partData.unshift(input);
    this.isNewPart = true;
  }

  public partDeleteConfirm(part: OrderPartDto) {
    const $this = this;
    $this.isPartDelete = true;
    $this.partDeleteItem = part;
  }

  removePart(part: OrderPartDto, status) {
    const index = this.partData.indexOf(part);

    // REMOVE WORK RECORD FROM ARRAY, BECAUSE IT'S REMOVED
    if (status && index !== -1) {
      this.partData.splice(index, 1);
    }

    // CALL API TO REMOVE THIS DATA
    if (status && part.orderPartId) {
      this.orderPartService.delete(part.orderPartId,
        response => {
          this.toasterService.success('', 'Removed Successfully');
          this.partLoading = false;
        },
        error => {
          this.partLoading = false;
          this.toasterService.errorMessage(error);
        });
    }

    const $this = this;
    $this.isPartDelete = false;
  }

  savePart(part: OrderPartDto) {
    this.partLoading = true;

    if (!part.partId) {
      this.partLoading = false;
      this.toasterService.error('', 'Part is mondatory.');
      return false;
    }

    if (part.orderPartId) {
      this.orderPartService.update(part,
        response => {
          this.loadParts();
          this.partLoading = false;
          this.toasterService.success('', 'Part Updated Successfully');
        },
        error => {
          this.partLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    } else {
      part.active = true;
      part.orderPartId = 0;
      part.orderId = this.order.orderId;

      this.orderPartService.create(part,
        response => {
          this.loadParts();
          this.partLoading = false;
          this.toasterService.success('', 'Part Added Successfully');
        },
        error => {
          this.partLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  updateWrUrl(data: WorkRecordDto) {
    data.workRecordUserAvatar = 'assets/img/no_avatar.png';
  }

  private loadWorkRecords(): void {
    this.workRecordService.getAll(
      this.partPageSize,
      this.partSkip,
      this.partSort,
      this.isactive,
      this.searchtext,
      this.order.orderId)
      .subscribe(eventResult => {
        this.wrData = eventResult.results;

        this.wrData.forEach(itm => {
          itm.workRecordUserAvatar = this.commonService.getUserThumbnail(itm.createdBy.toString());
        });

        this.wrLoading = false;
      });
  }

  addWorkRecord() {
    const input: WorkRecordDto = new WorkRecordDto();
    input.workRecordUserAvatar = this.commonService.getUserThumbnail(this.user.userId.toString());
    this.wrData.unshift(input);
  }

  public wrDeleteConfirm(workRecord: WorkRecordDto) {
    const $this = this;
    $this.isWrDelete = true;
    $this.wrDeleteDataItem = workRecord;
  }

  removeWorkRecord(workRecord: WorkRecordDto, status) {
    const index = this.wrData.indexOf(workRecord);

    // REMOVE WORK RECORD FROM ARRAY, BECAUSE IT'S REMOVED
    if (status && index !== -1) {
      this.wrData.splice(index, 1);
    }

    if (status && workRecord.workRecordId) {
      this.workRecordService.delete(workRecord.workRecordId,
        response => {

          this.toasterService.success('', 'Work Record Removed Successfully');
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }

    const $this = this;
    $this.isWrDelete = false;
  }

  saveWorkRecord(workRecord: WorkRecordDto) {

    if (!workRecord.hours || !workRecord.technicalNotes) {
      this.toasterService.error('', 'Hours and Notes are mondatory.');
      return false;
    }

    this.wrLoading = true;

    if (workRecord.workRecordId) {
      this.workRecordService.update(workRecord,
        response => {
          this.loadWorkRecords();
          this.wrLoading = false;
          this.toasterService.success('', 'Work Record Updated Successfully');
        },
        error => {
          this.wrLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    } else {
      workRecord.active = true;
      workRecord.workRecordId = 0;
      workRecord.orderId = this.order.orderId;

      this.workRecordService.create(workRecord,
        response => {
          this.loadWorkRecords();
          this.wrLoading = false;
          this.toasterService.success('', 'Work Record Added Successfully');
        },
        error => {
          this.wrLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  public open() {
    this.opened = true;
  }

  private loadRelatedOrderGridData(): void {
    this.relatedOrderService.getAll(
      this.rePageSize,
      this.reSkip,
      this.reSort,
      this.reIsactive,
      this.reSearchtext,
      this.order.orderId)
      .subscribe(eventResult => {

        if (eventResult.results.length > 0) {
          this.reGridView = {
            data: eventResult.results,
            total: eventResult.count
          };
        }
        else {
          this.reGridView = {
            data: eventResult.results,
            total: 0
          };
        }

        this.reLoading = false;
        this.reGridLoading = false;
      });
  }

  public reSortChange(sort: SortDescriptor[]): void {
    this.reSort = sort;
    this.reGridLoading = true;
    this.loadRelatedOrderGridData();
  }

  protected rePageChange({skip, take}: PageChangeEvent): void {
    this.reSkip = skip;
    this.rePageSize = take;
    this.reGridLoading = true;
    this.loadRelatedOrderGridData();
  }

  private reCloseEditor(grid, rowIndex = this.reEditedRowIndex) {
    grid.closeRow(rowIndex);
    this.reEditedRowIndex = undefined;
    this.reFormGroup = undefined;
  }

  public reAddHandler({sender}) {
    if (!this.eqmSettings.canCreateRelatedOrders) {
      this.error('You do not have permission to add Related order.');
      return false;
    } else {
      this.reCloseEditor(sender);
      this.reEditDataItem = new CreateRelatedOrderDto();
      this.isNew = true;
    }
  }

  public reEditHandler({dataItem}) {
    this.reEditDataItem = dataItem;
    this.isNew = false;
  }

  public reSaveHandler(data) {
    this.reGridLoading = true;
    if (this.isNew) {
      const inputData: CreateRelatedOrderDto = data;

      inputData.isWorkCompleted = false;
      inputData.statusId = 1;
      inputData.active = true;
      inputData.parentOrderId = this.order.orderId;

      this.relatedOrderService.create(inputData,
        response => {
          this.toasterService.success('', 'Order Created Successfully');
          this.loadRelatedOrderGridData();
          this.reGridLoading = false;
        },
        error => {
          this.reGridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    else {
      const inputData: OrderDto = data;

      this.relatedOrderService.update(inputData,
        response => {
          this.toasterService.success('', 'Order Updated Successfully');
          this.loadRelatedOrderGridData();
          this.reGridLoading = false;
        },
        error => {
          this.reGridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  public reDeleteClose(status) {
    const $this = this;
    if (status) {
      this.reGridLoading = true;
      this.relatedOrderService.delete($this.reDeleteDataItem.orderId,
        response => {
          this.toasterService.success('', 'Order Removed Successfully');
          this.loadRelatedOrderGridData();
          this.reGridLoading = false;
        },
        error => {
          this.reGridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    $this.reDeleteDataItem = null;
    $this.reOpened = false;
  }

  public reCancelHandler(e) {
    this.reEditedRowIndex = undefined;
    this.reFormGroup = undefined;
    this.reEditDataItem = undefined;
  }

  public reRemoveHandler({dataItem}) {
    this.reDeleteDataItem = dataItem;
    this.reOpen();
  }

  public reOpen() {
    this.reOpened = true;
  }

  public reDataStateChange(state: DataStateChangeEvent): void {
    this.reState = state;
  }

  public reOnSearchKeyup() {
    this.reSkip = 0;
    this.rePageSize = 10;
    this.reGridLoading = true;
    this.loadRelatedOrderGridData();
  }

  public actionData: Array<any> = [{
    text: 'Delete',
    icon: 'trash'
  }];

  public onAction(e, dataItem) {
    if (e === undefined) {
      this.reEditDataItem = dataItem;
      this.isNew = false;
    } else {
      if (e.text === 'Delete') {
        this.reDeleteDataItem = dataItem;
        this.reOpen();
      }
    }
  }
}
