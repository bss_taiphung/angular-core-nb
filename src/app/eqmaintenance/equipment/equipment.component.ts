import {
  Component,
  ViewEncapsulation,
  OnInit,
  HostListener
} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {Observable} from 'rxjs/Observable';
import {State} from '@progress/kendo-data-query';
import {
  PageChangeEvent,
  GridDataResult,
  DataStateChangeEvent
} from '@progress/kendo-angular-grid';
import {FormGroup} from '@angular/forms';
import {EquipmentService} from './service/equipment.service'
import {SortDescriptor} from '@progress/kendo-data-query';
import {
  IEquipmentDto,
  CreateEquipmentDto,
  EquipmentDto
} from './service/equipment.model';
import {PartService} from '../part/service/part.service';
import {PartDto} from '../part/service/part.model';
import {OrderService} from '../order/service/order.service';
import {DialogService} from '@progress/kendo-angular-dialog';
import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {HubConnection} from '@aspnet/signalr-client';
import {
  EqmSettings,
  User,
  UserRole
} from '../../shared/models/eqmSettings'
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {SelectListDto} from '../../shared/models/selectListDto'
import {UserSettingsDto} from '../../shared/usersettings/usersetting.model';
import {UserSettingsService} from '../../shared/usersettings/usersetting.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'equipment',
  templateUrl: './html/equipment.component.html',
  styleUrls: ['./css/equipment.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class EquipmentComponent implements OnInit {

  data: IEquipmentDto[];
  public formGroup: FormGroup;
  public view: Observable<GridDataResult>;
  private editedRowIndex: number;
  public editDataItem: CreateEquipmentDto;
  public isNew: boolean;
  public pageSize = 10;
  public skip = 0;
  public state: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };
  public gridView: GridDataResult;
  public deleteDataItem: any;
  private sort: SortDescriptor[] = [];
  public isactive: boolean = true;
  public searchtext: string = '';
  private hubConnection: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  eqmSettings: EqmSettings = new EqmSettings();
  public azureStorageContainer: string;
  public siteList: SelectListDto[];
  public mss: any;
  user: User = new User();
  userRole: UserRole = new UserRole();

  public defaultEquipmentItem: { text: string, value: number } = {text: 'All', value: null};
  public equipmentList: SelectListDto[];
  public fileteredEquiomentList: SelectListDto[];
  public currentSiteId: number = 1;
  public eqipmentTypeId: number;
  public equipmentSaving: boolean = false;

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private equipmentService: EquipmentService,
              private userSettingsService: UserSettingsService,
              private eqmSettingsService: EqmSettingsService,
              private partService: PartService,
              private orderService: OrderService,
              private dialogService: DialogService) {
    this.mss = this.commonService.getCookies('Mss');
    this.eqmSettings = this.eqmSettingsService.getEqmSettings();
    this.user = this.eqmSettingsService.getUser();
    this.userRole = this.eqmSettingsService.getUserRoles();
    this.commonService.validateLogin();
    this.azureStorageContainer = environment.azureStorageContainer;

    if (this.eqmSettings && this.eqmSettings.currentSiteId) {
      this.currentSiteId = this.eqmSettings.currentSiteId;
    }
  }

  ngOnInit() {

    this.commonService.getAllWithId(this.currentSiteId, 'equipmentmaintenance', 'EquipmentType').subscribe(eventResult => {
      this.equipmentList = eventResult.results;
      this.fileteredEquiomentList = this.equipmentList = eventResult.results;
    });

  }

  signalRConnection() {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');

        this.hubConnection
          .invoke('Subscribe', 'EquipmentMaintenance', 'Equipment', 'SiteId', this.eqmSettingsService.getEqmSettings().currentSiteId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
      const _responseText = messageJson;
      let result200: any = null;
      let data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      let resultData200 = this.commonService.toCamel(data);
      result200 = resultData200 ? EquipmentDto.fromJS(resultData200) : new EquipmentDto();

      var dataExist = this.containsObject(result200, this.gridView.data);

      if (operation.toLowerCase() === 'insert' && !dataExist) {
        this.gridView.data.unshift(result200);
      }

      if (operation.toLowerCase() === 'update') {
        this.gridView.data.forEach((element, index) => {
          if (element.equipmentId === result200.equipmentId) {
            //
            if (this.isactive) {
              if (result200.active == this.isactive) {
                this.gridView.data[index] = result200;
              } else {
                operation = 'delete';
              }
            } else {
              this.gridView.data[index] = result200;
            }
          }
        });
      }

      if (operation.toLowerCase() === 'delete' && dataExist) {
        var index = null;
        this.gridView.data.forEach((element, i) => {
          if (element.equipmentId === result200.equipmentId) {
            index = i;
          }
        });
        if (index !== null) {
          this.gridView.data.splice(index, 1);
        }
      }
    });
  }

  containsObject(obj, list) {
    var x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].equipmentId === obj.equipmentId) {
        return true;
      }
    }

    return false;
  }

  // Dialog box
  opened: boolean = false;
  openedimage: boolean = false;
  imageUrl = '';
  loading: boolean = true;
  gridLoading: boolean = true;

  randomNumber = Math.floor(Math.random() * 1000000000);

  public close(status) {
    var $this = this;
    if (status) {
      this.gridLoading = true;
      this.equipmentService.delete($this.deleteDataItem.equipmentId,
        response => {
          this.toasterService.success('', 'Equipment Removed Successfully');
          this.gridLoading = false;
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    $this.deleteDataItem = null;
    $this.opened = false;
  }

  public open() {
    this.opened = true;
  }

  public showImage(imageUrl) {
    this.imageUrl = imageUrl;
    this.openedimage = true;
  }

  public closeImage() {
    this.imageUrl = 'http://www.astrofisicamas.cl/wp-content/uploads/2016/11/blank.jpg' + '?' + this.randomNumber;
    this.openedimage = false;
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.loadGridData();
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
  }

  private loadGridData(): void {
    this.equipmentService.getAll(this.pageSize, this.skip, this.sort, this.isactive, this.searchtext).subscribe(eventResult => {
      this.gridView = {
        data: eventResult.results,
        total: eventResult.count
      };

      this.gridLoading = false;
      this.loading = false;
    });
  }

  public addHandler({sender}) {
    if (!this.eqmSettings.canEditMasterData) {
      this.toasterService.error('You do not have permission to add new equipment.', '');
      return false;
    } else {
      this.closeEditor(sender);
      this.editDataItem = new EquipmentDto();
      this.isNew = true;
    }
  }

  public editHandler({dataItem}) {

    this.editDataItem = dataItem;
    this.isNew = false;
  }

  public saveHandler(data) {
    this.gridLoading = true;
    if (this.isNew) {
      data.departmentId = (data.departmentId == null) ? 0 : data.departmentId;
      data.active = (data.active == null) ? false : data.active;
      const inputData: CreateEquipmentDto = data;
      this.equipmentService.create(inputData,
        response => {
          this.toasterService.success('', 'Equipment Saved Successfully');
          this.gridLoading = false;
        },
        error => {
          this.toasterService.errorMessage(error);
          this.gridLoading = false;
        }
      );
    }
    else {
      const inputData: EquipmentDto = data;
      this.equipmentService.update(inputData,
        response => {
          this.toasterService.success('', 'Equipment Updated Successfully');
          this.gridLoading = false;
        },
        error => {
          this.toasterService.errorMessage(error);
          this.gridLoading = false;
        }
      );
    }
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public cancelHandler() {
    this.editDataItem = undefined;
    this.formGroup = undefined;
  }

  public removeHandler({dataItem}) {
    this.deleteDataItem = dataItem;
    this.open();
  }

  public showActive() {
    this.isactive = !this.isactive;
    this.gridLoading = true;

    this.doSearch(this.state, this);
  }

  public onSearchKeyup() {
    this.skip = 0;
    this.pageSize = 10;
    this.gridLoading = true;
    this.loadGridData();

  }

  // Link Popup
  public eqsearchtext: string = '';
  public linkDialog: boolean = false;
  public equipmentId: number = 0;
  public equipmentName: string = '';
  public eqgridView: GridDataResult;
  public eqpageSize = 10;
  public eqskip = 0;
  private eqsort: SortDescriptor[] = [];
  private linkgridLoading: boolean = false;

  public linkedEquipments: PartDto[] = [];
  public eqvalid: boolean = false;

  public linkPartDialog(equipment) {

    this.linkgridLoading = true;
    this.linkDialog = true;
    this.eqvalid = false;
    this.linkedEquipments.length = 0;
    this.equipmentId = equipment.equipmentId;
    this.equipmentName = equipment.name;
    this.loadParts();
    this.loadeqGridData();

  }

  public closeDialog() {
    this.linkDialog = false;

  }

  public eqsortChange(sort: SortDescriptor[]): void {
    this.eqsort = sort;
    this.linkgridLoading = true;
    this.loadeqGridData();
  }

  protected eqpageChange({skip, take}: PageChangeEvent): void {
    this.eqskip = skip;
    this.eqpageSize = take;
    this.linkgridLoading = true;
    this.loadeqGridData();
  }

  private loadParts() {
    this.partService.getAllByEquipmentId(this.equipmentId).subscribe(eventResult => {

      if (eventResult != undefined && eventResult.results != undefined && eventResult.results.length > 0) {
        this.linkedEquipments = eventResult.results;
      }
    });
  }

  private loadeqGridData(): void {
    this.partService.getAll(this.eqpageSize, this.eqskip, this.eqsort, false, this.eqsearchtext).subscribe(eventResult => {

      this.eqgridView = {
        data: eventResult.results,
        total: eventResult.count
      };
      this.linkgridLoading = false;
    });
  }

  public oneqSearchKeyup() {
    this.eqskip = 0;
    this.eqpageSize = 10;
    this.linkgridLoading = true;
    this.loadeqGridData();
  }

  public linkPart(equipment) {
    this.eqvalid = false;
    var index = this.linkedEquipments.findIndex(x => x.partName == equipment.partName);
    if (index == -1) {
      this.linkedEquipments.push(equipment);
    } else {
      this.eqvalid = true;
    }
  }

  public saveEquipments() {

    var linkids = this.linkedEquipments.map(a => a.partId);
    var inputData = {
      equipmentId: this.equipmentId,
      linkIds: linkids,
      partId: 0
    };

    this.partService.linkEquipments(inputData,
      response => {
        this.toasterService.success('', 'Compatible Parts Saved Successfully');
        this.gridLoading = false;
        this.linkDialog = false;
      },
      error => {
        this.linkDialog = false;
        this.gridLoading = false;
        this.toasterService.errorMessage(error);
      }
    );
  }


  // Order Popup
  public ordersearchtext: string = '';
  public orderDialog: boolean = false;
  public ordergridView: GridDataResult;
  public orderpageSize = 10;
  public orderskip = 0;
  private ordersort: SortDescriptor[] = [];

  public openOrderDialog(equipment) {
    this.orderDialog = true;
    this.equipmentId = equipment.equipmentId;
    this.equipmentName = equipment.name;
    this.loadOrderGridData();
  }

  public closeOrderDialog() {
    this.orderDialog = false;
  }

  public onorderSearchKeyup() {
    this.orderskip = 0;
    this.orderpageSize = 10;
    this.loadOrderGridData();
  }

  public ordersortChange(sort: SortDescriptor[]): void {
    this.ordersort = sort;
    this.loadOrderGridData();
  }

  protected orderpageChange({skip, take}: PageChangeEvent): void {
    this.orderskip = skip;
    this.orderpageSize = take;
    this.loadOrderGridData();
  }

  private loadOrderGridData(): void {
    let filter: string = 'EquipmentId eq ' + this.equipmentId;
    this.orderService.getAll(this.orderpageSize, this.orderskip, this.ordersort, false, this.ordersearchtext, 0, filter).subscribe(eventResult => {
      if (eventResult.results.length <= 0) {
        eventResult.count = 0;
      }
      this.ordergridView = {
        data: eventResult.results,
        total: eventResult.count
      };
    });
  }

  public ActionData: Array<any> = [{
    text: 'Compatible Parts',
    icon: 'link-horizontal'
  }, {
    text: 'Related Orders',
    icon: 'preview'
  }, {
    text: 'Equipment Image',
    icon: 'image'
  }, {
    text: 'Delete',
    icon: 'trash'
  }];

  public onAction(e, dataItem) {
    if (e === undefined) {
      if (!this.eqmSettings.canEditMasterData) {
        this.toasterService.error('You do not have permission to update equipment.', '');
        return false;
      } else {
        this.editDataItem = dataItem;
        this.isNew = false;
      }
    } else {
      if (e.text == 'Delete') {
        if (!this.eqmSettings.canEditMasterData) {
          this.toasterService.error('You do not have permission to delete equipment.', '');
          return false;
        } else {
          this.deleteDataItem = dataItem;
          this.open();
        }
      } else if (e.text == 'Compatible Parts') {
        this.linkPartDialog(dataItem);
      } else if (e.text == 'Related Orders') {
        this.openOrderDialog(dataItem);
      }
      else if (e.text == 'Equipment Image') {
        var newstr = dataItem.imageFileName.replace('/mss/', '/' + this.azureStorageContainer + '/');
        this.showImage(newstr);
      }
    }
  }

  timeout = null;

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridLoading = true;

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.doSearch(state, this);
    }, 1000);
  }

  doSearch(state: any, that) {
    if (state.filter) {
      var search = that.commonService.getFilter(state.filter.filters, that.isactive);

      that.equipmentService.getAllWtihFilter(state.take, state.skip, that.sort, search, that.eqipmentTypeId)
        .subscribe(eventResult => {
          that.gridView = {
            data: eventResult.results,
            total: eventResult.count
          };
          that.loading = false;
          that.gridLoading = false;
          that.equipmentSaving = false;
        });

      this.skip = state.skip;
      this.pageSize = state.take;
    }
    else {
      that.loadGridData();
    }
  }

  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {
      this.cancelHandler();
      this.close(false);
      this.closeDialog();
      this.closeImage();
      this.closeOrderDialog();
    }
  }

  public siteSelectionChange(e): void {
    this.gridLoading = true;
    this.signalRConnection();
    this.loadGridData();
  }

  private equipmentTypeHandleFilter(value) {
    this.fileteredEquiomentList = this.equipmentList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  public onEquipmentTypeSelectionChange(changedEquipmentTypeId: number): void {
    if (changedEquipmentTypeId) {
      this.equipmentSaving = true;
      this.eqipmentTypeId = changedEquipmentTypeId;
      this.doSearch(this.state, this);
    }
    else {
      this.eqipmentTypeId = null;
      this.equipmentSaving = true;
      this.doSearch(this.state, this);
    }
  }
}
