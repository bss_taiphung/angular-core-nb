import {
  Component,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import {Validators, FormGroup, FormControl} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';
import {EquipmentDto} from './service/equipment.model';
import {CommonService} from '../../shared/services/common.service';
import {SelectListDto} from '../../shared/models/selectListDto';
import {DomSanitizer} from '@angular/platform-browser';
import {GridDataResult, PageChangeEvent} from '@progress/kendo-angular-grid';
import {SortDescriptor} from '@progress/kendo-data-query/dist/es/sort-descriptor';
import {ProcessService} from '../equipment/service/process.service';
import {EqmSettings} from '../../shared/models/eqmSettings'
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'kendo-grid-equipment-form',
  styles: [
    'input[type=text] { width: 100%; }'
  ],
  templateUrl: './html/equipment-form.component.html'
})
export class EquipmentFormComponent {
  public active = false;
  public base64ImgFile: string;
  public editForm: FormGroup = new FormGroup({
    'equipmentId': new FormControl(),
    'name': new FormControl('', Validators.required),
    'serialNo': new FormControl(),
    'modelNumber': new FormControl(),
    'location': new FormControl(),
    'manufacturerId': new FormControl(),
    'equipmentTypeId': new FormControl(),
    'description': new FormControl(),
    'uploadEqupImg': new FormControl(),
    'imageFileName': new FormControl(),
    'imageFile': new FormControl(),
    'processId': new FormControl(),
    'active': new FormControl(false),
  });

  public departmentList: SelectListDto[];
  public processId: number = 0;
  public processName: string = '';
  public filterworkCenterIdList: SelectListDto[];
  public manufacturerList: SelectListDto[];
  public filtermanufacturerIdList: SelectListDto[];

  public equipmentList: SelectListDto[];
  public fileteredEquiomentList: SelectListDto[];
  public _azureStorageContainer: string;

  public eqmSettings: EqmSettings = new EqmSettings();
  public currentSiteId: number = 1;

  constructor(private commonService: CommonService,
              private domSanitizer: DomSanitizer,
              private processService: ProcessService,
              private eqmSettingsService: EqmSettingsService) {
    this._azureStorageContainer = environment.azureStorageContainer;
  }

  @Input() public isNew = false;

  @Input()
  public set model(equipment: EquipmentDto) {
    console.log(equipment)
    if (equipment == null) {
      this.processId = 0;
      this.processName = '';
    } else {
      this.processId = equipment.processId;
      this.processName = equipment.processName;
    }

    if (equipment !== undefined && equipment.imageFileName !== undefined) {
      const newstr = equipment.imageFileName.replace('/mss/', '/' + this._azureStorageContainer + '/');
      this.base64ImgFile = newstr;
    } else {
      this.base64ImgFile = 'http://www.astrofisicamas.cl/wp-content/uploads/2016/11/blank.jpg';
    }

    this.eqmSettings = this.eqmSettingsService.getEqmSettings();

    if (this.eqmSettings && this.eqmSettings.currentSiteId) {
      this.currentSiteId = this.eqmSettings.currentSiteId;
    }

    this.editForm.reset(equipment);
    this.active = equipment !== undefined;

    if (this.active) {
      this.commonService.getAll('equipmentmaintenance', 'Manufacturer').subscribe(eventResult => {
        this.manufacturerList = eventResult.results;
        this.filtermanufacturerIdList = this.manufacturerList = eventResult.results;
      });

      this.commonService.getAllWithId(this.currentSiteId, 'equipmentmaintenance', 'EquipmentType').subscribe(eventResult => {
        this.equipmentList = eventResult.results;
        this.fileteredEquiomentList = this.equipmentList = eventResult.results;
      });
    }
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<EquipmentDto> = new EventEmitter<EquipmentDto>();

  public onSave(e): void {
    e.preventDefault();
    this.save.emit(this.editForm.value);
    this.active = false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.active = false;
    this.cancel.emit();
  }

  public onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        const filetype = file.name.split('.')[1];
        this.editForm.controls['imageFile'].setValue(reader.result.split(',')[1]);
        this.editForm.controls['imageFileName'].setValue(file.name);
        const imgFile = reader.result.split(',')[1];
        this.base64ImgFile = 'data:image/' + filetype + ';base64,' + imgFile;
      };
    }
  }


  private ManufacturerIdListHandleFilter(value) {
    this.filtermanufacturerIdList = this.manufacturerList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private equipmentListHandleFilter(value) {
    this.fileteredEquiomentList = this.equipmentList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  // Process Grid
  public processDialog = false;
  public processsearchtext = '';
  public processgridView: GridDataResult;
  public processpageSize = 10;
  public processskip = 0;
  private processsort: SortDescriptor[] = [];

  public openProcess(equipment) {
    this.processDialog = true;
    this.loadprocessGridData();
  }

  public closeOprocessDialog() {
    this.processDialog = false;
  }

  public onprocessSearchKeyup() {
    this.processskip = 0;
    this.processpageSize = 10;
    this.loadprocessGridData();
  }

  public processsortChange(sort: SortDescriptor[]): void {
    this.processsort = sort;
    this.loadprocessGridData();
  }

  protected processpageChange({skip, take}: PageChangeEvent): void {
    this.processskip = skip;
    this.processpageSize = take;
    this.loadprocessGridData();
  }

  private loadprocessGridData(): void {

    this.processService.getAll(
      this.processpageSize,
      this.processskip,
      this.processsort,
      false,
      this.processsearchtext)
      .subscribe(eventResult => {

        if (eventResult != null) {
          if (eventResult.results.length <= 0) {
            eventResult.count = 0;
          }
          this.processgridView = {
            data: eventResult.results,
            total: eventResult.count
          };
        }
      });
  }

  public selectProcess(process) {
    this.processId = process.processId;
    this.processName = process.processName;
    this.editForm.controls['processId'].setValue(process.processId);
    this.processDialog = false;
  }

  deSelectProcess() {
    this.processId = null;
    this.processName = null;
    this.editForm.controls['processId'].setValue('');
    this.processDialog = false;
  }

  // Dialog box
  opened: boolean = false;
  public close(status) {
    console.log(status);
  }
}
