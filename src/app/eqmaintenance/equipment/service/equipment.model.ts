import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';

// region EquipmentDto
export interface IEquipmentDto {
  name: string;
  serialNo: string;
  modelNumber: string;
  location: string;
  processId: number;
  processName: string;
  manufacturerId: number;
  manufacturerName: string;
  createdByName: string;
  description: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  equipmentId: number;
  imageFileName: string;
  imageFile: string;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
  equipmentTypeId: number;
  equipmentTypeName: string;
}

export class EquipmentDto implements IEquipmentDto {
  name: string;
  serialNo: string;
  modelNumber: string;
  location: string;
  processId: number;
  processName: string;
  manufacturerId: number;
  manufacturerName: string;
  createdByName: string;
  description: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  equipmentId: number;
  imageFileName: string;
  imageFile: string;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
  departmentId: number;
  workCenterId: number;
  equipmentTypeId: number;
  equipmentTypeName: string;

  static fromJS(data: any): EquipmentDto {
    const result = new EquipmentDto();
    result.init(data);
    return result;
  }

  constructor(data?: IEquipmentDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.name = data['name'];
      this.serialNo = data['serialNo'];
      this.modelNumber = data['modelNumber'];
      this.location = data['location'];
      this.processId = data['processId'];
      this.processName = data['processName'];
      this.manufacturerId = data['manufacturerId'];
      this.manufacturerName = data['manufacturerName'];
      this.imageFileName = data['imageFileName'];
      this.imageFile = data['imageFile'];
      this.createdByName = data['createdByName'];
      this.description = data['description'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.equipmentId = data['equipmentId'];
      this.siteId = data['siteId'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.active = data['active'];
      this.departmentId = data['departmentId'];
      this.workCenterId = data['workCenterId'];
      this.equipmentTypeId = data['equipmentTypeId'];
      this.equipmentTypeName = data['equipmentTypeName'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['name'] = this.name;
    data['serialNo'] = this.serialNo;
    data['modelNumber'] = this.modelNumber;
    data['location'] = this.location;
    data['processId'] = this.processId;
    data['processName'] = this.processName;
    data['manufacturerId'] = this.manufacturerId;
    data['manufacturerName'] = this.manufacturerName;
    data['imageFileName'] = this.imageFileName;
    data['imageFile'] = this.imageFile;
    data['createdByName'] = this.createdByName;
    data['description'] = this.description;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['equipmentId'] = this.equipmentId;
    data['siteId'] = this.siteId;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;
    data['departmentId'] = this.departmentId;
    data['workCenterId'] = this.workCenterId;
    data['equipmentTypeId'] = this.equipmentTypeId;
    data['equipmentTypeName'] = this.equipmentTypeName;

    return data;
  }
}

// endregion

// region CreateEquipmentDto
export interface ICreateEquipmentDto {
  name: string;
  serialNo: string;
  modelNumber: string;
  location: string;
  processId: number;
  manufacturerId: number;
  imageFileName: string;
  imageFile: string;
  description: string;
  active: boolean;
  departmentId: number;
  workCenterId: number;
}

export class CreateEquipmentDto implements ICreateEquipmentDto {
  equipmentId = 0;
  name: string;
  serialNo: string;
  modelNumber: string;
  location: string;
  processId: number;
  manufacturerId: number;
  imageFileName: string;
  imageFile: string;
  createdByName: string;
  description: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  active = true;
  departmentId: number;
  workCenterId: number;

  static fromJS(data: any): EquipmentDto {
    const result = new EquipmentDto();
    result.init(data);
    return result;
  }

  constructor(data?: IEquipmentDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.equipmentId = 0;
      this.name = data['name'];
      this.serialNo = data['serialNo'];
      this.modelNumber = data['modelNumber'];
      this.location = data['location'];
      this.processId = data['processId'];
      this.manufacturerId = data['manufacturerId'];
      this.imageFileName = data['imageFileName'];
      this.imageFile = data['imageFile'];
      this.description = data['description'];
      this.active = (data['active'] == null) ? false : data['active'];
      this.departmentId = data['departmentId'];
      this.workCenterId = data['workCenterId'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['equipmentId'] = 0;
    data['name'] = this.name;
    data['serialNo'] = this.serialNo;
    data['modelNumber'] = this.modelNumber;
    data['location'] = this.location;
    data['processId'] = this.processId;
    data['manufacturerId'] = this.manufacturerId;
    data['imageFileName'] = this.imageFileName;
    data['imageFile'] = this.imageFile;
    data['createdByName'] = this.createdByName;
    data['description'] = this.description;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['siteId'] = this.siteId;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = (this.active == null) ? false : this.active;
    data['departmentId'] = this.departmentId;
    data['workCenterId'] = this.workCenterId;
    return data;
  }
}

// endregion

// region PagedResultDtoOfEquipmentDto
export interface IPagedResultDtoOfEquipmentDto {
  results: EquipmentDto[];
  count: number;
}

export class PagedResultDtoOfEquipmentDto implements IPagedResultDtoOfEquipmentDto {
  results: EquipmentDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfEquipmentDto {
    const result = new PagedResultDtoOfEquipmentDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfEquipmentDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(EquipmentDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }


  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    this.count = data['count'];
    return data;
  }
}

// endregion

// region ResultDtoOfEquipmentDto
export interface IResultDtoOfEquipmentDto {
  results: EquipmentDto[];
  count: number;
}

export class ResultDtoOfEquipmentDto implements IResultDtoOfEquipmentDto {
  results: EquipmentDto[];
  count: number;

  static fromJS(data: any): ResultDtoOfEquipmentDto {
    const result = new ResultDtoOfEquipmentDto();
    result.init(data);
    return result;
  }

  constructor(data?: IResultDtoOfEquipmentDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {

    if (data && data != null) {
      if (data && data.constructor === Array) {
        this.results = [];
        for (const item of data) {
          this.results.push(EquipmentDto.fromJS(item));
        }
      }

    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data = [];
      for (const item of this.results) {
        data.push(item.toJSON());
      }
    }

    return data;
  }
}

// endregion
