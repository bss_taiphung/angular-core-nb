import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';

import {CreateProcessDto, ProcessDto, PagedResultDtoOfProcessDto} from './process.model';
import {CommonService} from '../../../shared/services/common.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class ProcessService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService, private http: HttpClient) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/productionsupport/api/Process/';
  }


  /**
   * @return Success
   */
  // Create Process
  create(input: CreateProcessDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'InsertAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  update(input: ProcessDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'UpdateAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  delete(id: any, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'DeleteAsync';
    const params = new HttpParams().set('id', id);
    const options = {
      params: params
    };

    this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
  }

  protected processCreate(response: Response): Observable<ProcessDto> {
    const status = response.status;


    /*
    * Start edit by loiptx
    * const _headers: any = response.headers ? response.headers.toJSON() : {};
    * */
    const _headers: any = response.headers ? response.headers : {};
    /*end edit*/

    if (status === 200) {
      const _responseText = response.text();
      let result200: any = null;
      /*
      * Start edit by loiptx
      * Fix current Bugs when build
      * const resultData200 = _responseText == '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      * */
      const resultData200 = ''; // REVIEW
      // value === '' ? null : JSON.parse(value, this.jsonParseReviver);
      /*end edit*/
      result200 = resultData200 ? ProcessDto.fromJS(resultData200) : new ProcessDto();
      return Observable.of(result200);
    } else if (status !== 200 && status !== 204) {
      const _responseText = response.text();
      // return throwException("An unexpected server error occurred.", status, _responseText, _headers);
    }
    return Observable.of<ProcessDto>(<any>null);
  }

  firstUpper(input: string) {
    return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1) : '';
  }

  getAll(take: number, skip: number, sort: any, isactive: boolean, searchtext: string): Observable<PagedResultDtoOfProcessDto> {

    const url = this.apiBaseUrl + 'GetPagedQuery';

    // Order by Clause
    let orderBy = '';
    if (sort !== undefined && sort.length > 0) {
      for (const item of sort) {
        orderBy = this.firstUpper(item.field) + ' ' + (item.dir === undefined ? 'asc' : item.dir);
      }
    } else {
      orderBy = 'ProcessId desc';
    }

    let filter = '';
    if (isactive) {
      filter = 'Active eq true';
    }
    if (searchtext.length > 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      // filter += "substringof('" + searchtext +"',ProcessName) eq true";
      filter += `ProcessName eq  + ${searchtext} + `;
    }

    const params = new HttpParams()
      .set('$top', take.toString())
      .set('$skip', skip.toString())
      .set('$count', 'true')
      .set('$orderby', orderBy.toString())
      .set('$filter', filter.toString());


    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options)
      .flatMap((response) => {
        const result200 = PagedResultDtoOfProcessDto.fromJS(response);
        return Observable.of(result200);
      });
  }
}

