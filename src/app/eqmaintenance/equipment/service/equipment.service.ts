import {Injectable} from '@angular/core';
import {
  HttpClient,
  HttpParams,
  HttpHeaders
} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

import {
  CreateEquipmentDto,
  EquipmentDto,
  PagedResultDtoOfEquipmentDto,
  ResultDtoOfEquipmentDto
} from './equipment.model';
import {CommonService} from '../../../shared/services/common.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class EquipmentService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService,
              private http: HttpClient) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/equipmentmaintenance/api/Equipment/';
  }

  create(input: CreateEquipmentDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'InsertAsync';
    input.departmentId = 1;
    // input.workCenterId = null;
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  update(input: EquipmentDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'UpdateAsync';
    input.departmentId = 1;
    // input.workCenterId = 1;
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  delete(id: any, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'DeleteAsync';
    const params = new HttpParams().set('id', id);
    const options = {
      params: params
    };

    this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
  }

  getAll(take: number, skip: number, sort: any, isactive: boolean, searchtext: string): Observable<PagedResultDtoOfEquipmentDto> {
    const url = this.apiBaseUrl + 'GetPagedQuery';

    let orderBy = '';
    if (sort !== undefined && sort.length > 0) {
      for (const item of sort) {
        orderBy = this.commonService.firstUpper(item.field) + ' ' + (item.dir === undefined ? 'asc' : item.dir);
      }
    } else {
      orderBy = 'EquipmentId desc';
    }

    let filter = '';
    if (isactive) {
      filter = 'Active eq true';
    }
    if (searchtext.length > 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += `contains(Name, ${searchtext} + )`;
    }

    const params = new HttpParams()
      .set('$top', take.toString())
      .set('$skip', skip.toString())
      .set('$count', 'true')
      .set('$orderby', orderBy.toString())
      .set('$filter', filter.toString());

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options)
      .flatMap((response) => {
        const result200 = PagedResultDtoOfEquipmentDto.fromJS(response);
        return Observable.of(result200);
      });
  }

  getAllWtihFilter(take: number, skip: number, sort: any, filter: string, eqipmentTypeId: any): Observable<PagedResultDtoOfEquipmentDto> {
    const url = this.apiBaseUrl + 'GetPagedQuery';

    let orderBy = '';
    if (sort !== undefined && sort.length > 0) {
      for (const item of sort) {
        orderBy = this.commonService.firstUpper(item.field) + ' ' + (item.dir === undefined ? 'asc' : item.dir);
      }
    } else {
      orderBy = 'EquipmentId desc';
    }

    if (eqipmentTypeId) {
      if (filter) {
        filter += ' and EquipmentTypeId eq ' + eqipmentTypeId + '';
      }
      else {
        filter += ' EquipmentTypeId eq ' + eqipmentTypeId + '';
      }
    }

    const params = new HttpParams()
      .set('$top', take.toString())
      .set('$skip', skip.toString())
      .set('$count', 'true')
      .set('$orderby', orderBy.toString())
      .set('$filter', filter.toString());

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      const result200 = PagedResultDtoOfEquipmentDto.fromJS(response);
      return Observable.of(result200);
    });
  }

  getAllByPartId(partId: number): Observable<ResultDtoOfEquipmentDto> {
    const url = environment.bllApiBaseAddress
      + '/equipmentmaintenance/api/EquipmentParts/GetAllEquipmentsListByPartId';

    const params = new HttpParams()
      .set('PartId', partId.toString());

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };
    return this.http.request<Response>('get', url, options).flatMap((response) => {
      const result200 = ResultDtoOfEquipmentDto.fromJS(response);
      return Observable.of(result200);
    });
  }
}
