import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';


// region ProcessDto

export interface IProcessDto {
  processId: number;
  processGroupId: number;
  processTypeId: number;
  teamId: number;
  departmentId: number;
  processName: string;
  qadWorkCenter: string;
  teamSortOrder: string;
  priorityColor: string;
  processTypeName: string;
  departmentName: string;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
}

export class ProcessDto implements IProcessDto {
  processId: number;
  processGroupId: number;
  processTypeId: number;
  teamId: number;
  departmentId: number;
  processName: string;
  qadWorkCenter: string;
  teamSortOrder: string;
  priorityColor: string;
  processTypeName: string;
  departmentName: string;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;

  static fromJS(data: any): ProcessDto {
    const result = new ProcessDto();
    result.init(data);
    return result;
  }

  constructor(data?: IProcessDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {

      this.processId = data['processId'];
      this.processGroupId = data['processGroupId'];
      this.processTypeId = data['processTypeId'];
      this.teamId = data['teamId'];
      this.departmentId = data['departmentId'];
      this.processName = data['processName'];
      this.qadWorkCenter = data['qadWorkCenter'];
      this.teamSortOrder = data['teamSortOrder'];
      this.priorityColor = data['priorityColor'];
      this.processTypeName = data['processTypeName'];
      this.departmentName = data['departmentName'];
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.siteId = data['siteId'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.active = data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['processId'] = this.processId;
    data['processGroupId'] = this.processGroupId;
    data['processTypeId'] = this.processTypeId;
    data['teamId'] = this.teamId;
    data['departmentId'] = this.departmentId;
    data['processName'] = this.processName;
    data['qadWorkCenter'] = this.qadWorkCenter;
    data['teamSortOrder'] = this.teamSortOrder;
    data['priorityColor'] = this.priorityColor;
    data['processTypeName'] = this.processTypeName;
    data['departmentName'] = this.departmentName;
    data['createdByName'] = this.createdByName;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['siteId'] = this.siteId;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;

    return data;
  }
}

// endregion

// region CreateProcessDto

export interface ICreateProcessDto {
  processGroupId: number;
  processTypeId: number;
  teamId: number;
  departmentId: number;
  processName: string;
  qadWorkCenter: string;
  teamSortOrder: string;
  priorityColor: string;
  processTypeName: string;
  departmentName: string;
  active: boolean;
}

export class CreateProcessDto implements ICreateProcessDto {
  processId = 0;
  processGroupId: number;
  processTypeId: number;
  teamId: number;
  departmentId: number;
  processName: string;
  qadWorkCenter: string;
  teamSortOrder: string;
  priorityColor: string;
  processTypeName: string;
  departmentName: string;
  active = true;

  static fromJS(data: any): ProcessDto {
    const result = new ProcessDto();
    result.init(data);
    return result;
  }

  constructor(data?: IProcessDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.processId = 0;
      this.processGroupId = data['processGroupId'];
      this.processTypeId = data['processTypeId'];
      this.teamId = data['teamId'];
      this.departmentId = data['departmentId'];
      this.processName = data['processName'];
      this.qadWorkCenter = data['qadWorkCenter'];
      this.teamSortOrder = data['teamSortOrder'];
      this.priorityColor = data['priorityColor'];
      this.processTypeName = data['processTypeName'];
      this.departmentName = data['departmentName'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['processId'] = 0;
    data['processId'] = this.processId;
    data['processGroupId'] = this.processGroupId;
    data['processTypeId'] = this.processTypeId;
    data['teamId'] = this.teamId;
    data['departmentId'] = this.departmentId;
    data['processName'] = this.processName;
    data['qadWorkCenter'] = this.qadWorkCenter;
    data['teamSortOrder'] = this.teamSortOrder;
    data['priorityColor'] = this.priorityColor;
    data['processTypeName'] = this.processTypeName;
    data['departmentName'] = this.departmentName;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

// endregion

// region PagedResultDtoOfProcessDto

export interface IPagedResultDtoOfProcessDto {
  results: ProcessDto[];
  count: number;
}

export class PagedResultDtoOfProcessDto implements IPagedResultDtoOfProcessDto {
  results: ProcessDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfProcessDto {
    const result = new PagedResultDtoOfProcessDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfProcessDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(ProcessDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion
