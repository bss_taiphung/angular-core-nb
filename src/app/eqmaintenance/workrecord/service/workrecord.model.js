"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("rxjs/add/operator/map");
require("rxjs/add/operator/mergeMap");
require("rxjs/add/observable/fromPromise");
require("rxjs/add/observable/of");
require("rxjs/add/observable/throw");
require("rxjs/add/operator/map");
require("rxjs/add/operator/toPromise");
require("rxjs/add/operator/mergeMap");
require("rxjs/add/operator/catch");
var WorkRecordDto = /** @class */ (function () {
    function WorkRecordDto(data) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    this[property] = data[property];
            }
        }
    }
    WorkRecordDto.prototype.init = function (data) {
        if (data) {
            this.createdByName = data["createdByName"];
            this.createdDateTime = data["createdDateTime"];
            this.createdDate = data["createdDate"];
            this.modifiedByName = data["modifiedByName"];
            this.modifiedDateTime = data["modifiedDateTime"];
            this.createdBy = data["createdBy"];
            this.modifiedBy = data["modifiedBy"];
            this.active = data["active"];
            this.workRecordId = data["workRecordId"];
            this.orderId = data["orderId"];
            this.hours = data["hours"];
            this.technicalNotes = data["technicalNotes"];
        }
    };
    WorkRecordDto.fromJS = function (data) {
        var result = new WorkRecordDto();
        result.init(data);
        return result;
    };
    WorkRecordDto.prototype.toJSON = function (data) {
        data = typeof data === 'object' ? data : {};
        data["createdByName"] = this.createdByName;
        data["createdDateTime"] = this.createdDateTime;
        data["createdDate"] = this.createdDate;
        data["modifiedByName"] = this.modifiedByName;
        data["modifiedDateTime"] = this.modifiedDateTime;
        data["createdBy"] = this.createdBy;
        data["modifiedBy"] = this.modifiedBy;
        data["active"] = this.active;
        data["workRecordId"] = this.workRecordId;
        data["orderId"] = this.orderId;
        data["hours"] = this.hours;
        data["technicalNotes"] = this.technicalNotes;
        return data;
    };
    return WorkRecordDto;
}());
exports.WorkRecordDto = WorkRecordDto;
var PagedResultDtoOfWorkRecordDto = /** @class */ (function () {
    function PagedResultDtoOfWorkRecordDto(data) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    this[property] = data[property];
            }
        }
    }
    PagedResultDtoOfWorkRecordDto.prototype.init = function (data) {
        if (data && data != null) {
            if (data["results"] && data["results"].constructor === Array) {
                this.results = [];
                for (var _i = 0, _a = data["results"]; _i < _a.length; _i++) {
                    var item = _a[_i];
                    this.results.push(WorkRecordDto.fromJS(item));
                }
            }
            if (data["count"]) {
                this.count = data["count"];
            }
        }
    };
    PagedResultDtoOfWorkRecordDto.fromJS = function (data) {
        var result = new PagedResultDtoOfWorkRecordDto();
        result.init(data);
        return result;
    };
    PagedResultDtoOfWorkRecordDto.prototype.toJSON = function (data) {
        data = typeof data === 'object' ? data : {};
        if (this.results && this.results.constructor === Array) {
            data["results"] = [];
            for (var _i = 0, _a = this.results; _i < _a.length; _i++) {
                var item = _a[_i];
                data["results"].push(item.toJSON());
            }
        }
        if (this.count) {
            data["count"] = this.count;
        }
        return data;
    };
    return PagedResultDtoOfWorkRecordDto;
}());
exports.PagedResultDtoOfWorkRecordDto = PagedResultDtoOfWorkRecordDto;
//# sourceMappingURL=workrecord.model.js.map