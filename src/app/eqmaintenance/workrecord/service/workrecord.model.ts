import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

// region WorkRecordDto

export interface IWorkRecordDto {
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
  workRecordId: number;
  orderId: number;
  hours: number;
  technicalNotes: string;
  workRecordUserAvatar: string;
}

export class WorkRecordDto implements IWorkRecordDto {
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
  workRecordId: number;
  orderId: number;
  hours: number;
  technicalNotes: string;
  workRecordUserAvatar: string;

  constructor(data?: IWorkRecordDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.active = data['active'];
      this.workRecordId = data['workRecordId'];
      this.orderId = data['orderId'];
      this.hours = data['hours'];
      this.technicalNotes = data['technicalNotes'];
    }
  }

  static fromJS(data: any): WorkRecordDto {
    const result = new WorkRecordDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['createdByName'] = this.createdByName;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;
    data['workRecordId'] = this.workRecordId;
    data['orderId'] = this.orderId;
    data['hours'] = this.hours;
    data['technicalNotes'] = this.technicalNotes;

    return data;
  }
}

// endregion

// region PagedResultDtoOfWorkRecordDto

export interface IPagedResultDtoOfWorkRecordDto {
  results: WorkRecordDto[];
  count: number;
}

export class PagedResultDtoOfWorkRecordDto implements IPagedResultDtoOfWorkRecordDto {
  results: WorkRecordDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfWorkRecordDto {
    const result = new PagedResultDtoOfWorkRecordDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfWorkRecordDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(WorkRecordDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion
