import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';


// region RolesDto
export interface IRolesDto {
  roleId: number;
  roleName: string;
  canToggleSites: boolean;
  canEditOrders: boolean;
  canEditMasterData: boolean;
  canEditSchedules: boolean;
  canWorkOnUnassignedOrders: boolean;
  canAddPartsToOrders: boolean;
  canCreateRelatedOrders: boolean;
  canManageRoles: boolean;
  canAssignRoles: boolean;
  receiveSupervisorNotifications: boolean;
  modifiedDateTime: string;
  createdBy: number;
  modifiedBy: number;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  active: boolean;
}

export class RolesDto implements IRolesDto {
  roleId: number;
  roleName: string;
  canToggleSites: boolean;
  canEditOrders: boolean;
  canEditMasterData: boolean;
  canEditSchedules: boolean;
  canWorkOnUnassignedOrders: boolean;
  canAddPartsToOrders: boolean;
  canCreateRelatedOrders: boolean;
  canManageRoles: boolean;
  canAssignRoles: boolean;
  receiveSupervisorNotifications: boolean;
  modifiedDateTime: string;
  createdBy: number;
  modifiedBy: number;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  active: boolean;

  static fromJS(data: any): RolesDto {
    const result = new RolesDto();
    result.init(data);
    return result;
  }

  constructor(data ?: IRolesDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data ?: any) {
    if (data) {
      this.roleId = data['roleId'];
      this.roleName = data['roleName'];
      this.canToggleSites = data['canToggleSites'];
      this.canEditOrders = data['canEditOrders'];
      this.canEditMasterData = data['canEditMasterData'];
      this.canEditSchedules = data['canEditSchedules'];
      this.canWorkOnUnassignedOrders = data['canWorkOnUnassignedOrders'];
      this.canAddPartsToOrders = data['canAddPartsToOrders'];
      this.canCreateRelatedOrders = data['canCreateRelatedOrders'];
      this.canManageRoles = data['canManageRoles'];
      this.canAssignRoles = data['canAssignRoles'];
      this.receiveSupervisorNotifications = data['receiveSupervisorNotifications'];
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.active = data['active'];
    }
  }

  toJSON(data ?: any) {
    data = typeof data === 'object' ? data : {};

    data['roleId'] = this.roleId;
    data['roleName'] = this.roleName;
    data['canToggleSites'] = this.canToggleSites;
    data['canEditOrders'] = this.canEditOrders;
    data['canEditMasterData'] = this.canEditMasterData;
    data['canEditSchedules'] = this.canEditSchedules;
    data['canWorkOnUnassignedOrders'] = this.canWorkOnUnassignedOrders;
    data['canAddPartsToOrders'] = this.canAddPartsToOrders;
    data['canCreateRelatedOrders'] = this.canCreateRelatedOrders;
    data['canManageRoles'] = this.canManageRoles;
    data['canAssignRoles'] = this.canAssignRoles;
    data['receiveSupervisorNotifications'] = this.receiveSupervisorNotifications;
    data['createdByName'] = this.createdByName;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;

    return data;
  }
}

// endregion

// region CreateRolesDto
export interface ICreateRolesDto {
  roleId: number;
  roleName: string;
  canToggleSites: boolean;
  canEditOrders: boolean;
  canEditMasterData: boolean;
  canEditSchedules: boolean;
  canWorkOnUnassignedOrders: boolean;
  canAddPartsToOrders: boolean;
  canCreateRelatedOrders: boolean;
  canManageRoles: boolean;
  canAssignRoles: boolean;
  receiveSupervisorNotifications: boolean;
  active: boolean;
}

export class CreateRolesDto implements ICreateRolesDto {
  roleId = 0;
  roleName: string;
  canToggleSites: boolean;
  canEditOrders: boolean;
  canEditMasterData: boolean;
  canEditSchedules: boolean;
  canWorkOnUnassignedOrders: boolean;
  canAddPartsToOrders: boolean;
  canCreateRelatedOrders: boolean;
  canManageRoles: boolean;
  canAssignRoles: boolean;
  receiveSupervisorNotifications: boolean;
  active = true;

  static fromJS(data: any): RolesDto {
    const result = new RolesDto();
    result.init(data);
    return result;
  }

  constructor(data?: IRolesDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.roleId = 0;
      this.roleName = data['roleName'];
      this.canToggleSites = data['canToggleSites'];
      this.canEditOrders = data['canEditOrders'];
      this.canEditMasterData = data['canEditMasterData'];
      this.canEditSchedules = data['canEditSchedules'];
      this.canWorkOnUnassignedOrders = data['canWorkOnUnassignedOrders'];
      this.canAddPartsToOrders = data['canAddPartsToOrders'];
      this.canCreateRelatedOrders = data['canCreateRelatedOrders'];
      this.canManageRoles = data['canManageRoles'];
      this.canAssignRoles = data['canAssignRoles'];
      this.receiveSupervisorNotifications = data['receiveSupervisorNotifications'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['roleId'] = 0;
    data['roleName'] = this.roleName;
    data['canToggleSites'] = this.canToggleSites;
    data['canEditOrders'] = this.canEditOrders;
    data['canEditMasterData'] = this.canEditMasterData;
    data['canEditSchedules'] = this.canEditSchedules;
    data['canWorkOnUnassignedOrders'] = this.canWorkOnUnassignedOrders;
    data['canAddPartsToOrders'] = this.canAddPartsToOrders;
    data['canCreateRelatedOrders'] = this.canCreateRelatedOrders;
    data['canManageRoles'] = this.canManageRoles;
    data['canAssignRoles'] = this.canAssignRoles;
    data['receiveSupervisorNotifications'] = this.receiveSupervisorNotifications;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

// endregion

// region PagedResultDtoOfRolesDto
export interface IPagedResultDtoOfRolesDto {
  results: RolesDto[];
  count: number;
}

export class PagedResultDtoOfRolesDto implements IPagedResultDtoOfRolesDto {
  results: RolesDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfRolesDto {
    const result = new PagedResultDtoOfRolesDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfRolesDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(RolesDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion
