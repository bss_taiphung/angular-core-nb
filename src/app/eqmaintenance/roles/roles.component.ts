import {
  Component,
  HostListener,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {Observable} from 'rxjs/Observable';
import {
  SortDescriptor,
  State
} from '@progress/kendo-data-query';
import {
  DataStateChangeEvent,
  GridDataResult,
  PageChangeEvent
} from '@progress/kendo-angular-grid';
import {FormGroup} from '@angular/forms';
import {RolesService} from './service/roles.service'
import {
  CreateRolesDto,
  IRolesDto,
  RolesDto
} from './service/roles.model';

import {HubConnection} from '@aspnet/signalr-client';
import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {environment} from '../../../environments/environment';

import {
  EqmSettings,
  User,
  UserRole
} from '../../shared/models/eqmSettings'
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {SelectListDto} from '../../shared/models/selectListDto'
import {UserSettingsDto} from '../../shared/usersettings/usersetting.model';
import {UserSettingsService} from '../../shared/usersettings/usersetting.service';

@Component({
  selector: 'roles',
  templateUrl: './html/roles.component.html',
  styleUrls: ['./css/roles.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class RolesComponent implements OnInit {

  data: IRolesDto[];
  public formGroup: FormGroup;
  public view: Observable<GridDataResult>;
  private editedRowIndex: number;
  public editDataItem: CreateRolesDto;
  public isNew: boolean;
  public pageSize = 10;
  public skip = 0;
  public state: State = {
    skip: 0,
    take: 5,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };
  public gridView: GridDataResult;
  public deleteDataItem: any;
  private sort: SortDescriptor[] = [];
  public isactive: boolean = true;
  public searchtext: string = '';

  private hubConnection: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  eqmSettings: EqmSettings = new EqmSettings();
  user: User = new User();
  userRole: UserRole = new UserRole();
  public siteList: SelectListDto[];
  public mss: any;

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private userSettingsService: UserSettingsService,
              private rolesService: RolesService) {
    this.mss = this.commonService.getCookies('Mss');
    this.eqmSettings = this.eqmSettingsService.getEqmSettings();
    this.user = this.eqmSettingsService.getUser();
    this.userRole = this.eqmSettingsService.getUserRoles();
  }

  ngOnInit() {
  }

  signalRConnection() {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');

        this.hubConnection
          .invoke('Subscribe', 'EquipmentMaintenance', 'Role', 'SiteId', this.eqmSettingsService.getEqmSettings().currentSiteId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
      const _responseText = messageJson;
      let result200: any = null;
      let data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      let resultData200 = this.commonService.toCamel(data);
      result200 = resultData200 ? RolesDto.fromJS(resultData200) : new RolesDto();

      var dataExist = this.containsObject(result200, this.gridView.data);

      if (operation.toLowerCase() === 'insert' && !dataExist) {
        this.gridView.data.unshift(result200);
      }

      if (operation.toLowerCase() === 'update') {
        this.gridView.data.forEach((element, index) => {
          if (element.roleId === result200.roleId) {
            if (this.isactive) {
              if (result200.active == this.isactive) {
                this.gridView.data[index] = result200;
              } else {
                operation = 'delete';
              }
            } else {
              this.gridView.data[index] = result200;
            }
          }
        });
      }

      if (operation.toLowerCase() === 'delete' && dataExist) {
        var index = null;
        this.gridView.data.forEach((element, i) => {
          if (element.roleId === result200.roleId) {
            index = i;
          }
        });
        if (index !== null) {
          this.gridView.data.splice(index, 1);
        }
      }

    });
  }

  containsObject(obj, list) {
    var x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].statusId === obj.statusId) {
        return true;
      }
    }

    return false;
  }

  // Dialog box
  opened: boolean = false;
  loading: boolean = true;
  gridLoading: boolean = true;

  public close(status) {
    var $this = this;
    if (status) {
      this.gridLoading = true;
      this.rolesService.delete($this.deleteDataItem.roleId,
        response => {
          this.toasterService.success('', 'Role Removed Successfully');
          this.gridLoading = false;
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    $this.deleteDataItem = null;
    $this.opened = false;
  }

  public open() {
    this.opened = true;
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.loadGridData();
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
    this.loadGridData();
  }

  private loadGridData(): void {
    this.rolesService.getAll(this.pageSize, this.skip, this.sort, this.isactive, this.searchtext).subscribe(eventResult => {
      this.gridView = {
        data: eventResult.results,
        total: eventResult.count
      };
      this.loading = false;
      this.gridLoading = false;
    });
  }

  public addHandler({sender}) {
    //if (!this.eqmSettings.canEditMasterData) {
    //  this.toasterService.error('You do not have permission to add new status.', '');
    //  return false;
    //} else {
    this.closeEditor(sender);
    this.editDataItem = new CreateRolesDto();
    this.isNew = true;
    //}
  }

  public editHandler({dataItem}) {

    this.editDataItem = dataItem;
    this.isNew = false;
  }

  public saveHandler(data) {
    this.gridLoading = true;
    if (this.isNew) {
      data.active = (data.active == null) ? false : data.active;
      data.isInternal = (data.isInternal == null) ? false : data.isInternal;
      data.isExternal = (data.isExternal == null) ? false : data.isExternal;
      data.isPMS = (data.isPMS == null) ? false : data.isPMS;
      data.canClose = (data.canClose == null) ? false : data.canClose;
      data.isAssigned = (data.isAssigned == null) ? false : data.isAssigned;
      const inputData: CreateRolesDto = data;
      this.rolesService.create(inputData,
        response => {
          this.toasterService.success('', 'Role Saved Successfully');
          this.gridLoading = false;
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    else {
      const inputData: RolesDto = data;
      this.rolesService.update(inputData,
        response => {
          this.toasterService.success('', 'Role Updated Successfully');
          this.gridLoading = false;
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public cancelHandler() {
    this.editDataItem = undefined;
    this.formGroup = undefined;
  }

  public removeHandler({dataItem}) {
    this.deleteDataItem = dataItem;
    this.open();
  }

  public showActive() {
    this.isactive = !this.isactive;
    this.gridLoading = true;
    this.loadGridData();
  }

  public onSearchKeyup() {
    this.skip = 0;
    this.pageSize = 10;
    this.gridLoading = true;
    this.loadGridData();
  }

  public ActionData: Array<any> = [{
    text: 'Delete',
    icon: 'trash'
  }];

  public onAction(e, dataItem) {
    if (e === undefined) {
      //if (!this.eqmSettings.canEditMasterData) {
      //  this.toasterService.error('You do not have permission to update status.', '');
      //  return false;
      //} else {
      this.editDataItem = dataItem;
      this.isNew = false;
      //}
    } else {
      if (e.text === 'Delete') {
        //if (!this.eqmSettings.canEditMasterData) {
        //  this.toasterService.error('You do not have permission to delete status.', '');
        //  return false;
        //} else {
        this.deleteDataItem = dataItem;
        this.open();
        //}
      }
    }
  }

  timeout = null;

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridLoading = true;

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.doSearch(state, this);
    }, 1000);
  }

  doSearch(state: DataStateChangeEvent, that) {
    if (state.filter) {
      var search = that.commonService.getFilter(state.filter.filters);

      if (search === '') {
        that.loading = false;
        that.gridLoading = false;

        return;
      }

      that.rolesService.getAllWtihFilter(that.pageSize, that.skip, that.sort, that.isactive, search)
        .subscribe(eventResult => {
          that.gridView = {
            data: eventResult.results,
            total: eventResult.count
          };
          that.loading = false;
          that.gridLoading = false;
        });
    }
    else {
      that.loadGridData();
    }
  }

  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {
      this.cancelHandler();
      this.close(false);
    }
  }

  //public siteHandleFilter(filter: any): void {
  //  this.siteList = this.siteList.filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  //}

  //public updateCurrentSiteId() {
  //  this.gridLoading = true;

  //  var data = new UserSettingsDto();

  //  data.active = true;
  //  data.roleId = this.eqmSettings.roleId; //this role should be specific of eqm module, and not general role
  //  data.userId = this.user.userId;
  //  data.userSettingsId = this.eqmSettings.userSettingsId;
  //  data.currentSiteId = this.eqmSettings.currentSiteId;
  //  //data.showOnlyMyAssignments = this.isShowOnlyMyAssignments;

  //  this.userSettingsService.update(data,
  //    response => {
  //      this.toasterService.success("", 'User Site Updated Successfully');

  //      //update cookies to take new changes
  //      this.eqmSettingsService.userSettings().subscribe();

  //      this.loadGridData();
  //      this.gridLoading = false;
  //    },
  //    error => {
  //      this.gridLoading = false;
  //      this.toasterService.errorMessage(error);
  //    }
  //  );
  //}

  //public siteSelectionChange(changedSiteId: any): void {
  //  if (changedSiteId) {
  //    this.eqmSettings.currentSiteId = changedSiteId;
  //    this.updateCurrentSiteId();
  //  }
  //}
}
