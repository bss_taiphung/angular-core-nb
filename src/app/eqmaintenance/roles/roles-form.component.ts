import {
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';
import {RolesDto} from './service/roles.model';


@Component({
  selector: 'kendo-grid-role-form',
  templateUrl: './html/roles-form.component.html'
})
export class RoleFormComponent {
  public active = false;
  public editForm: FormGroup = new FormGroup({
    'roleId': new FormControl(),
    'roleName': new FormControl('', Validators.required),
    'canToggleSites': new FormControl(false),
    'canEditOrders': new FormControl(false),
    'canEditMasterData': new FormControl(false),
    'canEditSchedules': new FormControl(false),
    'canWorkOnUnassignedOrders': new FormControl(false),
    'canAddPartsToOrders': new FormControl(false),
    'canCreateRelatedOrders': new FormControl(false),
    'canManageRoles': new FormControl(false),
    'canAssignRoles': new FormControl(false),
    'receiveSupervisorNotifications': new FormControl(false),
    'active': new FormControl(false),
  });

  @Input() public isNew = false;

  @Input()
  public set model(status: RolesDto) {
    this.editForm.reset(status);
    this.active = status !== undefined;
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<RolesDto> = new EventEmitter();

  public onSave(e): void {
    e.preventDefault();
    this.save.emit(this.editForm.value);
    this.active = false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.active = false;
    this.cancel.emit();
  }
}
