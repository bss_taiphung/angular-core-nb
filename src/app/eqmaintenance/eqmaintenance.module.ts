import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ButtonsModule} from '@progress/kendo-angular-buttons';
import {DialogModule} from '@progress/kendo-angular-dialog';
import {GridModule} from '@progress/kendo-angular-grid';
import {DateInputsModule} from '@progress/kendo-angular-dateinputs';
import {DropDownsModule} from '@progress/kendo-angular-dropdowns';
import {TelerikReportingModule} from '@progress/telerik-angular-report-viewer';

import {GeneralModule} from '../general/general.module';

import {routing} from './eqmaintenance.routes';
import {SharedModule} from '../shared/index';

import {EqmaintenanceComponent} from './eqmaintenance.component';
import {EqmnavmenuComponent} from './eqmnavmenu/eqmnavmenu.component';
import {Notifications} from './notifications/notifications.component';
import {ManufacturerComponent} from './manufacturer/manufacturer.component';
import {ManufacturerFormComponent} from './manufacturer/manufacturer-form.component';
import {FailureTypeComponent} from './failure_type/failuretype.component';
import {FailureTypeFormComponent} from './failure_type/failuretype-form.component';
import {EquipmentTypeComponent} from './equipment-type/equipment-type.component';
import {EquipmentTypeFormComponent} from './equipment-type/equipment-type-form.component';
import {OrderTypeComponent} from './order_type/ordertype.component';
import {OrderTypeFormComponent} from './order_type/ordertype-form.component';
import {PriorityComponent} from './priority/priority.component';
import {PriorityFormComponent} from './priority/priority-form.component';
import {PMInactiveReasonComponent} from './pminactivereason/pminactivereason.component';
import {PMInactiveReasonFormComponent} from './pminactivereason/pminactivereason-form.component';
import {StatusComponent} from './status/status.component';
import {StatusFormComponent} from './status/status-form.component';
import {PartComponent} from './part/part.component';
import {PartFormComponent} from './part/part-form.component';
import {ScheduleTypeComponent} from './schedule_type/scheduletype.component';
import {ScheduleTypeFormComponent} from './schedule_type/scheduletype-form.component';
import {RolesComponent} from './roles/roles.component';
import {RoleFormComponent} from './roles/roles-form.component';
import {OrderComponent} from './order/order.component'
import {OrderDetailComponent} from './order/order.detail.component'
import {RelatedOrderFormComponent} from './order/relatedorder-form.component'
import {OrderFormComponent} from './order/order-form.component'

import {PMScheduleFormComponent} from './pmschedule/pmschedule-form.component';
import {EquipmentFormComponent} from './equipment/equipment-form.component'
import {ExportOrderComponent} from './order/exportorder.component'
import {BulkOrderUpdateComponent} from './order/bulkorder-update-form-component'
import {DetailedOrderComponent} from './order/detailedorder.component'


import {SiteComponent} from './shared/site/site.component';
import {EQMusersettingsComponent} from './eqmusersettings/eqmusersettings.component';
import {EQMusersettingsFormComponent} from './eqmusersettings/eqmusersettings-form.component';

import {FailureTypeService} from './failure_type/service/failuretype.service';
import {EquipmentTypeService} from './equipment-type/service/equipment-type.service';
import {ManufacturerService} from './manufacturer/service/manufacturer.service';
import {OrderTypeService} from './order_type/service/ordertype.service';
import {PriorityService} from './priority/service/priority.service';
import {ProcessService} from './equipment/service/process.service';
import {PMInactiveReasonService} from './pminactivereason/service/pminactivereason.service';
import {StatusService} from './status/service/status.service';
import {PartService} from './part/service/part.service';
import {ScheduleTypeService} from './schedule_type/service/scheduletype.service';
import {PMScheduleComponent} from './pmschedule/pmschedule.component';
import {PMScheduleService} from './pmschedule/service/pmschedule.service'
import {RolesService} from './roles/service/roles.service';

import {OrderService} from './order/service/order.service'
import {RelatedOrderService} from './order/service/relatedorder.service'
import {WorkRecordService} from './workrecord/service/workrecord.service'
import {OrderPartService} from './orderpart/service/orderpart.service'

import {EquipmentComponent} from './equipment/equipment.component'
import {EquipmentService} from './equipment/service/equipment.service'
import {UserSettingsService} from './eqmusersettings/service/eqmusersettings.service';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    ButtonsModule,
    GridModule,
    DialogModule,
    DateInputsModule,
    DropDownsModule,
    CommonModule,
    SharedModule,
    routing,
    TelerikReportingModule,
    GeneralModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  declarations: [
    EqmaintenanceComponent,
    Notifications,
    EqmnavmenuComponent,
    ManufacturerComponent,
    ManufacturerFormComponent,
    PMScheduleComponent,
    PMScheduleFormComponent,
    ManufacturerFormComponent,
    FailureTypeComponent,
    FailureTypeFormComponent,
    EquipmentTypeComponent,
    EquipmentTypeFormComponent,
    OrderTypeComponent,
    OrderTypeFormComponent,
    PriorityComponent,
    PriorityFormComponent,
    PMInactiveReasonComponent,
    PMInactiveReasonFormComponent,
    StatusComponent,
    StatusFormComponent,
    PartComponent,
    PartFormComponent,
    ScheduleTypeComponent,
    ScheduleTypeFormComponent,
    OrderComponent,
    OrderDetailComponent,
    RelatedOrderFormComponent,
    OrderFormComponent,
    BulkOrderUpdateComponent,
    EquipmentComponent,
    EquipmentFormComponent,
    ExportOrderComponent,
    DetailedOrderComponent,
    RolesComponent,
    RoleFormComponent,
    SiteComponent,
    EQMusersettingsComponent,
    EQMusersettingsFormComponent
  ],
  providers: [
    FailureTypeService,
    EquipmentTypeService,
    ManufacturerService,
    OrderTypeService,
    PriorityService,
    PMInactiveReasonService,
    StatusService,
    ScheduleTypeService,
    PMScheduleService,
    OrderService,
    PartService,
    ProcessService,
    WorkRecordService,
    OrderPartService,
    EquipmentService,
    RelatedOrderService,
    RolesService,
    UserSettingsService
  ]

})
export class EqmaintenanceModule {
}
