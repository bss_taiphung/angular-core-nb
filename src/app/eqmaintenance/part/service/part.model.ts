﻿import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';


// region PartDto

export interface IPartDto {
  partName: string;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  partId: number;
  siteId: number;
  partQuantity: number;
  partReOrderLevel: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
  isStockItem: boolean;
}

export class PartDto implements IPartDto {
  partName: string;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  partId: number;
  siteId: number;
  partQuantity: number;
  partReOrderLevel: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
  isStockItem: boolean;

  static fromJS(data: any): PartDto {
    const result = new PartDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPartDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }

      }
    }
  }

  init(data?: any) {
    if (data) {
      this.partName = data['partName'];
      this.partQuantity = data['partQuantity'];
      this.partReOrderLevel = data['partReOrderLevel'];
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.partId = data['partId'];
      this.siteId = data['siteId'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.active = data['active'];
      this.isStockItem = data['isStockItem'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['partName'] = this.partName;
    data['partQuantity'] = this.partQuantity;
    data['partReOrderLevel'] = this.partReOrderLevel;
    data['createdByName'] = this.createdByName;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['partId'] = this.partId;
    data['siteId'] = this.siteId;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;
    data['isStockItem'] = this.isStockItem;

    return data;
  }
}

// endregion

// region CreatePartDto
export interface ICreatePartDto {
  active: boolean;
  isStockItem: boolean;
}

export class CreatePartDto implements ICreatePartDto {
  partId = 0;
  partName: string;
  partQuantity: number;
  partReOrderLevel: number;
  active = true;
  isStockItem: boolean;

  static fromJS(data: any): PartDto {
    const result = new PartDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPartDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.partId = 0;
      this.partName = data['partName'];
      this.partQuantity = data['partQuantity'];
      this.partReOrderLevel = data['partReOrderLevel'];
      this.active = (data['active'] == null) ? false : data['active'];
      this.isStockItem = data['isStockItem'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['partId'] = 0;
    data['partName'] = this.partName;
    data['partQuantity'] = this.partQuantity;
    data['partReOrderLevel'] = this.partReOrderLevel;
    data['active'] = (this.active == null) ? false : this.active;
    data['isStockItem'] = this.isStockItem;
    return data;
  }
}

// endregion

// region PagedResultDtoOfPartDto

export interface IPagedResultDtoOfPartDto {
  results: PartDto[];
  count: number;
}

export class PagedResultDtoOfPartDto implements IPagedResultDtoOfPartDto {
  results: PartDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfPartDto {
    const result = new PagedResultDtoOfPartDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfPartDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(PartDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion

// region ResultDtoOfPartDto

export interface IResultDtoOfPartDto {
  results: PartDto[];
  count: number;
}

export class ResultDtoOfPartDto implements IResultDtoOfPartDto {
  results: PartDto[];
  count: number;

  static fromJS(data: any): ResultDtoOfPartDto {
    const result = new ResultDtoOfPartDto();
    result.init(data);
    return result;
  }

  constructor(data?: IResultDtoOfPartDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {

    if (data && data != null) {
      if (data && data.constructor === Array) {
        this.results = [];
        for (const item of data) {
          this.results.push(PartDto.fromJS(item));
        }
      }

    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data = [];
      for (const item of this.results) {
        data.push(item.toJSON());
      }
    }

    return data;
  }
}

// endregion
