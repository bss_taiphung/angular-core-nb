import {
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';
import {PartDto} from './service/part.model';


@Component({
  selector: 'kendo-grid-part-form',
  styles: [
    'input[type=text] { width: 100%; }'
  ],
  templateUrl: './html/part-form.component.html'
})
export class PartFormComponent {
  public active = false;
  public editForm: FormGroup = new FormGroup({
    'partId': new FormControl(),
    'partName': new FormControl('', Validators.required),
    'partQuantity': new FormControl(),
    'partReOrderLevel': new FormControl(),
    'isStockItem': new FormControl(false),
    'active': new FormControl(false),
  });

  @Input() public isNew = false;

  @Input()
  public set model(part: PartDto) {
    this.editForm.reset(part);
    this.active = part !== undefined;
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<PartDto> = new EventEmitter();

  public onSave(e): void {
    e.preventDefault();
    this.save.emit(this.editForm.value);
    this.active = false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.active = false;
    this.cancel.emit();
  }
}
