import {Component, HostListener, OnInit, ViewEncapsulation} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {Observable} from 'rxjs/Observable';
import {FormGroup} from '@angular/forms';
import {SortDescriptor, State} from '@progress/kendo-data-query';
import {DataStateChangeEvent, GridDataResult, PageChangeEvent} from '@progress/kendo-angular-grid';
import {HubConnection} from '@aspnet/signalr-client';

import {PartService} from './service/part.service'
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {EquipmentService} from '../equipment/service/equipment.service'

import {CreatePartDto, IPartDto, PartDto} from './service/part.model';
import {EquipmentDto} from '../equipment/service/equipment.model';

import {CommonService} from '../../shared/services/common.service';

import {EqmSettings, User, UserRole} from '../../shared/models/eqmSettings'
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';

import {SelectListDto} from '../../shared/models/selectListDto'
import {UserSettingsService} from '../../shared/usersettings/usersetting.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'part',
  templateUrl: './html/part.component.html',
  styleUrls: ['./css/part.component.css'],
  encapsulation: ViewEncapsulation.None
})


export class PartComponent implements OnInit {

  data: IPartDto[];
  public formGroup: FormGroup;
  public view: Observable<GridDataResult>;
  private editedRowIndex: number;
  public editDataItem: CreatePartDto;
  public isNew: boolean;
  public pageSize = 10;
  public skip = 0;
  public state: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };
  public gridView: GridDataResult;
  public deleteDataItem: any;
  private sort: SortDescriptor[] = [];
  public isactive: boolean = true;
  public searchtext: string = '';

  private hubConnection: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  eqmSettings: EqmSettings = new EqmSettings();
  user: User = new User();
  userRole: UserRole = new UserRole();
  public siteList: SelectListDto[];
  public mss: any;

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private partService: PartService,
              private userSettingsService: UserSettingsService,
              private equipmentService: EquipmentService) {
    this.mss = this.commonService.getCookies('Mss');
    this.eqmSettings = this.eqmSettingsService.getEqmSettings();
    this.user = this.eqmSettingsService.getUser();
    this.userRole = this.eqmSettingsService.getUserRoles();
    this.commonService.validateLogin();
  }

  ngOnInit() {
  }

  signalRConnection() {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');

        this.hubConnection
          .invoke('Subscribe', 'EquipmentMaintenance', 'Part', 'SiteId', this.eqmSettingsService.getEqmSettings().currentSiteId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
      const _responseText = messageJson;
      let result200: any = null;
      let data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      let resultData200 = this.commonService.toCamel(data);
      result200 = resultData200 ? PartDto.fromJS(resultData200) : new PartDto();

      var dataExist = this.containsObject(result200, this.gridView.data);

      if (operation.toLowerCase() === 'insert' && !dataExist) {
        this.gridView.data.unshift(result200);
      }

      if (operation.toLowerCase() === 'update') {
        this.gridView.data.forEach((element, index) => {
          if (element.partId === result200.partId) {
            if (this.isactive) {
              if (result200.active == this.isactive) {
                this.gridView.data[index] = result200;
              } else {
                operation = 'delete';
              }
            } else {
              this.gridView.data[index] = result200;
            }
          }
        });
      }

      if (operation.toLowerCase() === 'delete' && dataExist) {
        var index = null;
        this.gridView.data.forEach((element, i) => {
          if (element.partId === result200.partId) {
            index = i;
          }
        });
        if (index !== null) {
          this.gridView.data.splice(index, 1);
        }
        //this.gridView.data.forEach((element, index) => {
        //    if (element.partId === result200.partId) {
        //        this.gridView.data.splice(this.gridView.data[index], 1);
        //    }
        //});
      }
    });
  }

  containsObject(obj, list) {
    var x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].partId === obj.partId) {
        return true;
      }
    }

    return false;
  }

  // Dialog box

  opened: boolean = false;
  loading: boolean = true;
  gridLoading: boolean = true;

  public close(status) {
    var $this = this;
    if (status) {
      this.gridLoading = true;
      this.partService.delete($this.deleteDataItem.partId,
        response => {
          this.toasterService.success('', 'Part Removed Successfully');
          this.gridLoading = false;
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    $this.deleteDataItem = null;
    $this.opened = false;
  }

  public open() {
    this.opened = true;
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.loadGridData();
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
  }

  private loadGridData(): void {
    this.partService.getAll(this.pageSize, this.skip, this.sort, this.isactive, this.searchtext).subscribe(eventResult => {
      this.gridView = {
        data: eventResult.results,
        total: eventResult.count
      };
      this.loading = false;
      this.gridLoading = false;
    });
  }

  public addHandler({sender}) {
    if (!this.eqmSettings.canEditMasterData) {
      this.toasterService.error('You do not have permission to add new part.', '');
      return false;
    } else {
      this.closeEditor(sender);
      this.editDataItem = new CreatePartDto();
      this.isNew = true;
    }
  }

  public editHandler({dataItem}) {
    this.editDataItem = dataItem;
    this.isNew = false;
  }

  public saveHandler(data) {
    this.gridLoading = true;
    if (this.isNew) {
      data.active = (data.active == null) ? false : data.active;
      data.isStockItem = (data.isStockItem == null) ? false : data.isStockItem;
      const inputData: CreatePartDto = data;
      this.partService.create(inputData,
        response => {
          this.toasterService.success('', 'Part Saved Successfully');
          this.gridLoading = false;
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    else {
      const inputData: PartDto = data;
      this.partService.update(inputData,
        response => {
          this.toasterService.success('', 'Part Updated Successfully');
          this.gridLoading = false;
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public cancelHandler() {
    this.editDataItem = undefined;
    this.formGroup = undefined;
  }

  public removeHandler({dataItem}) {
    this.deleteDataItem = dataItem;
    this.open();
  }

  public showActive() {
    this.isactive = !this.isactive;
    this.gridLoading = true;

    this.doSearch(this.state, this);
  }

  public onSearchKeyup() {
    this.skip = 0;
    this.pageSize = 10;
    this.gridLoading = true;
    this.loadGridData();
  }

  // Link Popup
  public eqsearchtext: string = '';
  public linkDialog: boolean = false;
  public partId: number = 0;
  public partName: string = '';
  public eqgridView: GridDataResult;
  public eqpageSize = 10;
  public eqskip = 0;
  private eqsort: SortDescriptor[] = [];
  private linkgridLoading: boolean = false;

  public linkedEquipments: EquipmentDto[] = [];
  public eqvalid: boolean = false;

  public linqEquipment(part) {

    this.linkDialog = true;
    this.eqvalid = false;
    this.linkedEquipments.length = 0;
    this.partId = part.partId;
    this.partName = part.partName;
    this.linkgridLoading = true;
    this.loadEquipments(part.partId);
    this.loadeqGridData();
  }

  public closeDialog() {
    this.linkDialog = false;

  }

  public eqsortChange(sort: SortDescriptor[]): void {
    this.eqsort = sort;
    this.linkgridLoading = true;
    this.loadeqGridData();
  }

  protected eqpageChange({skip, take}: PageChangeEvent): void {
    this.eqskip = skip;
    this.eqpageSize = take;
    this.linkgridLoading = true;
    this.loadeqGridData();
  }

  private loadEquipments(partId) {
    this.equipmentService.getAllByPartId(partId).subscribe(eventResult => {
      if (eventResult != undefined && eventResult.results != undefined && eventResult.results.length > 0) {
        this.linkedEquipments = eventResult.results;
      }
    });
  }

  private loadeqGridData(): void {
    this.equipmentService.getAll(this.eqpageSize, this.eqskip, this.eqsort, false, this.eqsearchtext).subscribe(eventResult => {
      this.eqgridView = {
        data: eventResult.results,
        total: eventResult.count
      };
      this.linkgridLoading = false;
    });
  }

  public oneqSearchKeyup() {
    this.eqskip = 0;
    this.eqpageSize = 10;
    this.linkgridLoading = true;
    this.loadeqGridData();
  }

  public linkEquipment(equipment) {
    this.eqvalid = false;
    var index = this.linkedEquipments.findIndex(x => x.name == equipment.name);
    if (index == -1) {
      this.linkedEquipments.push(equipment);
    } else {
      this.eqvalid = true;
    }
  }

  public saveEquipments() {

    var linkids = this.linkedEquipments.map(a => a.equipmentId);
    var inputData = {
      equipmentId: 0,
      linkIds: linkids,
      partId: this.partId
    };

    this.partService.linkEquipments(inputData,
      response => {
        this.toasterService.success('', 'Equipment Parts Linked Successfully');
        this.linkDialog = false;
      },
      error => {
        this.linkDialog = false;
        this.toasterService.errorMessage(error);
      }
    );
  }

  public ActionData: Array<any> = [{
    text: 'Compatible Equipments',
    icon: 'link-horizontal'
  }, {
    text: 'Delete',
    icon: 'trash'
  }];

  public onAction(e, dataItem) {
    if (e === undefined) {
      if (!this.eqmSettings.canEditMasterData) {
        this.toasterService.error('You do not have permission to update part.', '');
        return false;
      } else {
        this.editDataItem = dataItem;
        this.isNew = false;
      }
    } else {
      if (e.text === 'Delete') {
        if (!this.eqmSettings.canEditMasterData) {
          this.toasterService.error('You do not have permission to delete part.', '');
          return false;
        } else {
          this.deleteDataItem = dataItem;
          this.open();
        }
      } else if (e.text == 'Compatible Equipments') {
        this.linqEquipment(dataItem);
      }
    }
  }

  timeout = null;

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridLoading = true;

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.doSearch(state, this);
    }, 1000);
  }

  doSearch(state: any, that) {
    if (state.filter) {
      var search = that.commonService.getFilter(state.filter.filters, that.isactive);

      that.partService.getAllWtihFilter(state.take, state.skip, that.sort, search)
        .subscribe(eventResult => {
          that.gridView = {
            data: eventResult.results,
            total: eventResult.count
          };
          that.loading = false;
          that.gridLoading = false;
        });

      this.skip = state.skip;
      this.pageSize = state.take;
    }
    else {
      that.loadGridData();
    }
  }

  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {
      this.cancelHandler();
      this.close(false);
      this.closeDialog();
    }
  }

  public siteSelectionChange(e): void {
    this.gridLoading = true;
    this.signalRConnection();
    this.loadGridData();
  }
}
