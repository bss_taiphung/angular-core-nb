import {
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';
import {PriorityDto} from './service/priority.model';

@Component({
  selector: 'kendo-grid-priority-form',
  styles: [
    'input[type=text] { width: 100%; }'
  ],
  templateUrl: './html/priority-form.component.html'
})
export class PriorityFormComponent {
  public active = false;
  public editForm: FormGroup = new FormGroup({
    'priorityId': new FormControl(),
    'description': new FormControl('', Validators.required),
    'active': new FormControl(false),
  });

  @Input() public isNew = false;

  @Input()
  public set model(priority: PriorityDto) {
    this.editForm.reset(priority);
    this.active = priority !== undefined;
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<PriorityDto> = new EventEmitter();

  public onSave(e): void {
    e.preventDefault();
    this.save.emit(this.editForm.value);
    this.active = false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.active = false;
    this.cancel.emit();
  }
}
