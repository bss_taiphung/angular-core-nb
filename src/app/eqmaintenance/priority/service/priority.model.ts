﻿import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';


// region PriorityDto

export interface IPriorityDto {
  description: string;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  priorityId: number;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
}

export class PriorityDto implements IPriorityDto {
  description: string;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  priorityId: number;
  siteId: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;

  static fromJS(data: any): PriorityDto {
    const result = new PriorityDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPriorityDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.description = data['description'];
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.priorityId = data['priorityId'];
      this.siteId = data['siteId'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.active = data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['description'] = this.description;
    data['createdByName'] = this.createdByName;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['priorityId'] = this.priorityId;
    data['siteId'] = this.siteId;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;

    return data;
  }
}

// endregion

// region CreatePriorityDto

export interface ICreatePriorityDto {
  description: string;
  active: boolean;
}

export class CreatePriorityDto implements ICreatePriorityDto {
  priorityId = 0;
  description: string;
  active = true;

  static fromJS(data: any): PriorityDto {
    const result = new PriorityDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPriorityDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.priorityId = 0;
      this.description = data['description'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['priorityId'] = 0;
    data['description'] = this.description;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

// endregion

// region PagedResultDtoOfPriorityDto

export interface IPagedResultDtoOfPriorityDto {
  results: PriorityDto[];
  count: number;
}

export class PagedResultDtoOfPriorityDto implements IPagedResultDtoOfPriorityDto {
  results: PriorityDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfPriorityDto {
    const result = new PagedResultDtoOfPriorityDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfPriorityDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(PriorityDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion
