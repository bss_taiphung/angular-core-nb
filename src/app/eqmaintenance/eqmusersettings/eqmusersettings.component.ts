import {
  Component,
  ViewEncapsulation,
  OnInit,
  HostListener
} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {Observable} from 'rxjs/Observable';
import {State} from '@progress/kendo-data-query';
import {
  PageChangeEvent,
  GridDataResult,
  DataStateChangeEvent
} from '@progress/kendo-angular-grid';
import {FormGroup} from '@angular/forms';
import {UserSettingsService} from './service/eqmusersettings.service'
import {SortDescriptor} from '@progress/kendo-data-query';
import {
  UsersettingsDto,
  CreateUsersettingsDto,
  IUsersettingsDto
} from './service/eqmusersettings.model';

import {HubConnection} from '@aspnet/signalr-client';
import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';

import {EqmSettings, User, UserRole} from '../../shared/models/eqmSettings'
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {SelectListDto} from '../../shared/models/selectListDto'
import {environment} from '../../../environments/environment';


@Component({
  selector: 'eqmusersettings',
  templateUrl: './html/eqmusersettings.component.html',
  styleUrls: ['./css/eqmusersettings.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class EQMusersettingsComponent implements OnInit {

  data: IUsersettingsDto[];
  public formGroup: FormGroup;
  public view: Observable<GridDataResult>;
  private editedRowIndex: number;
  public editDataItem: CreateUsersettingsDto;
  public isNew: boolean;
  public isDuplicateMember = false;
  public pageSize = 10;
  public skip = 0;
  public state: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };
  public gridView: GridDataResult;
  public deleteDataItem: any;
  private sort: SortDescriptor[] = [];
  public isactive = true;
  public searchtext = '';

  private hubConnection: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  eqmSettings: EqmSettings = new EqmSettings();
  user: User = new User();
  userRole: UserRole = new UserRole();
  public siteList: SelectListDto[];
  public mss: any;

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private userSettingsService: UserSettingsService) {
    this.mss = this.commonService.getCookies('Mss');
    this.eqmSettings = this.eqmSettingsService.getEqmSettings();
    this.user = this.eqmSettingsService.getUser();
    this.userRole = this.eqmSettingsService.getUserRoles();
  }

  ngOnInit() {
    this.loadGridData();
  }

  signalRConnection() {

    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');

        this.hubConnection
          .invoke('Subscribe', 'EquipmentMaintenance', 'UserSettings', 'SiteId', this.eqmSettingsService.getEqmSettings().currentSiteId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
      const _responseText = messageJson;
      let result200: any = null;
      const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      const resultData200 = this.commonService.toCamel(data);
      result200 = resultData200 ? UsersettingsDto.fromJS(resultData200) : new UsersettingsDto();

      const dataExist = this.containsObject(result200, this.gridView.data === undefined);

      if (operation.toLowerCase() === 'insert' && !dataExist) {
        this.gridView.data.unshift(result200);
      }

      if (operation.toLowerCase() === 'update') {
        this.gridView.data.forEach((element, index) => {
          if (element.userSettingId === result200.userSettingId) {
            if (this.isactive) {
              if (result200.active == this.isactive) {
                this.gridView.data[index] = result200;
              } else {
                operation = 'delete';
              }
            } else {
              this.gridView.data[index] = result200;
            }
          }
        });
      }

      if (operation.toLowerCase() === 'delete' && dataExist) {
        let index = null;
        this.gridView.data.forEach((element, i) => {
          if (element.userSettingId === result200.userSettingId) {
            index = i;
          }
        });
        if (index !== null) {
          this.gridView.data.splice(index, 1);
        }
      }

    });
  }

  containsObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].userSettingId === obj.userSettingId) {
        return true;
      }
    }

    return false;
  }

  // Dialog box
  opened = false;
  loading = true;
  gridLoading = true;

  public close(status) {
    const $this = this;
    if (status) {
      this.gridLoading = true;
      this.userSettingsService.delete($this.deleteDataItem.userSettingsId,
        response => {
          this.toasterService.success('', 'User Settings removed Successfully');
          this.userSettingformFlag = false;
          this.loadGridData();
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    $this.deleteDataItem = null;
    $this.opened = false;
  }

  public open() {
    this.opened = true;
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.loadGridData();
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
    this.loadGridData();
  }

  private loadGridData(): void {
    this.gridLoading = true;
    this.userSettingsService.getAll(this.pageSize, this.skip, this.sort, this.isactive, this.searchtext).subscribe(eventResult => {
      this.gridView = {
        data: eventResult.results,
        total: eventResult.count
      };
      this.loading = false;
      this.gridLoading = false;
    });
  }

  public addHandler({sender}) {
    this.closeEditor(sender);
    this.editDataItem = new CreateUsersettingsDto();
    this.isNew = true;
  }

  public editHandler({dataItem}) {

    this.editDataItem = dataItem;
    this.isNew = false;
  }

  public userSettingformFlag = false;

  public saveHandler(data) {
    this.gridLoading = true;
    if (this.isNew) {
      data.active = (data.active == null) ? false : data.active;
      const inputData: CreateUsersettingsDto = data;
      inputData.currentSiteId = null;
      inputData.showOnlyMyAssignments = null;
      this.userSettingsService.create(inputData,
        response => {
          if (response == false) {
            this.gridLoading = false;
            this.isNew = true;
            this.userSettingformFlag = true;
            this.isDuplicateMember = true;
            return false;
          }
          if (response != null) {
            this.toasterService.success('', 'User Settings Saved Successfully');
            this.loadGridData();
            this.gridLoading = false;
            this.userSettingformFlag = false;
          }
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
          console.log(error)
        }
      );
    }
    else {
      const inputData: UsersettingsDto = data;
      this.userSettingsService.update(inputData,
        response => {

          this.toasterService.success('', 'User Settings Updated Successfully');
          this.loadGridData();
          this.gridLoading = false;
          this.userSettingformFlag = false;
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public cancelHandler() {
    this.editDataItem = undefined;
    this.formGroup = undefined;
  }

  public removeHandler({dataItem}) {
    this.deleteDataItem = dataItem;
    this.open();
  }

  public showActive() {
    this.isactive = !this.isactive;
    this.gridLoading = true;
    this.loadGridData();
  }

  public onSearchKeyup() {
    this.skip = 0;
    this.pageSize = 10;
    this.gridLoading = true;
    this.loadGridData();
  }

  public ActionData: Array<any> = [{
    text: 'Delete',
    icon: 'trash'
  }];

  public onAction(e, dataItem) {
    if (e === undefined) {
      //if (!this.eqmSettings.canEditMasterData) {
      //  this.toasterService.error('You do not have permission to update status.', '');
      //  return false;
      //} else {
      this.editDataItem = dataItem;
      this.isNew = false;
      //}
    } else {
      if (e.text === 'Delete') {
        //if (!this.eqmSettings.canEditMasterData) {
        //  this.toasterService.error('You do not have permission to delete status.', '');
        //  return false;
        //} else {
        this.deleteDataItem = dataItem;
        this.open();
        //}
      }
    }
  }


  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {
      this.cancelHandler();
      this.close(false);
      this.userSettingformFlag = false;
    }
  }

  timeout = null;

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridLoading = true;

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.doSearch(state, this);
    }, 1000);
  }

  doSearch(state: any, that) {
    if (state.filter) {
      const search = that.commonService.getFilter(state.filter.filters, that.isactive);

      that.userSettingsService.getAllWtihFilter(state.take, state.skip, that.sort, search)
        .subscribe(eventResult => {
          that.gridView = {
            data: eventResult.results,
            total: eventResult.count
          };
          that.loading = false;
          that.gridLoading = false;
        });

      this.skip = state.skip;
      this.pageSize = state.take;
    }
    else {
      that.loadGridData();
    }
  }

  public siteSelectionChange(e): void {
    // this.signalRConnection();
    this.loadGridData();
  }
}
