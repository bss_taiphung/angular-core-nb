import {Injectable} from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import {
  PagedResultDtoOfUsersettingsDto,
  UsersettingsDto,
  CreateUsersettingsDto
} from './eqmusersettings.model';
import {CommonService} from '../../../shared/services/common.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class UserSettingsService {

  private apiBaseUrl;
  private baseResource;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService, private http: HttpClient) {
    this.baseResource = '/EquipmentMaintenance/api/UserSettings/';
    this.apiBaseUrl = environment.bllApiBaseAddress + this.baseResource;
  }

  create(input: CreateUsersettingsDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'InsertAsync';
    input.showOnlyMyAssignments = 1;
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  update(input: UsersettingsDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'UpdateAsync';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  delete(id: any, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'DeleteAsync';

    const params = new HttpParams().set('id', id);

    const options = {
      params: params
    };

    this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
  }

  getAll(take: number, skip: number, sort: any, isactive: boolean, searchtext: string): Observable<PagedResultDtoOfUsersettingsDto> {

    const url = this.apiBaseUrl + 'GetPagedQuery';
    const resource = this.baseResource + 'GetPagedQuery';

    // Order by Clause
    let orderBy = '';
    if (sort !== undefined && sort.length > 0) {
      for (const item of sort) {
        orderBy = this.commonService.firstUpper(item.field) + ' ' + (item.dir === undefined ? 'asc' : item.dir);
      }
    } else {
      orderBy = 'UserSettingsId desc';
    }

    let filter = '';
    if (isactive) {
      filter = 'Active eq true';
    }

    const params = new HttpParams()
      .set('$top', take.toString())
      .set('$skip', skip.toString())
      .set('$count', 'true')
      .set('$orderby', orderBy.toString())
      .set('$filter', filter.toString());


    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };


    return this.http.request<Response>('get', url, options)
      .flatMap((response) => {
        const result200 = PagedResultDtoOfUsersettingsDto.fromJS(response);
        return Observable.of(result200);
      });
  }

  getAllWtihFilter(take: number, skip: number, sort: any, filter: string): Observable<PagedResultDtoOfUsersettingsDto> {
    const url = this.apiBaseUrl + 'GetPagedQuery';

    let orderBy = '';
    if (sort !== undefined && sort.length > 0) {
      for (const item of sort) {
        orderBy = this.commonService.firstUpper(item.field) + ' ' + (item.dir == undefined ? 'asc' : item.dir);
      }
    } else {
      orderBy = 'UserSettingsId desc';
    }

    const params = new HttpParams()
      .set('$top', take.toString())
      .set('$skip', skip.toString())
      .set('$count', 'true')
      .set('$orderby', orderBy.toString())
      .set('$filter', filter.toString());

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      const result200 = PagedResultDtoOfUsersettingsDto.fromJS(response);
      return Observable.of(result200);
    });
  }
}

