import {
  Component,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import {
  Validators,
  FormGroup,
  FormControl
} from '@angular/forms';
import {UserSettingsService} from './service/eqmusersettings.service'
import 'rxjs/add/observable/fromEvent';
import {UsersettingsDto} from './service/eqmusersettings.model';
import {CommonService} from '../../shared/services/common.service';
import {SelectListDto} from '../../shared/models/selectListDto';
import {EqmSettings, User} from '../../shared/models/eqmSettings'
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';

@Component({
  selector: 'kendo-grid-eqmusersettings-form',
  templateUrl: './html/eqmusersettings-form.component.html'
})
export class EQMusersettingsFormComponent {
  public active = false;

  eqmSettings: EqmSettings = new EqmSettings();
  public editForm: FormGroup = new FormGroup({
    'userSettingsId': new FormControl(),
    'userId': new FormControl('', Validators.required),
    'roleId': new FormControl('', Validators.required),
    'currentSiteId': new FormControl('', Validators.required),
    'showOnlyMyAssignments': new FormControl(''),
    'active': new FormControl(false),
  });

  @Input() public isNew = false;

  @Input() public isDuplicateMember = false;

  @Input() public userSettingformFlag = false;


  public username: string;
  public currentSiteId = 1;
  public userIdList: SelectListDto[];
  public filteruserIDList: SelectListDto[];

  public currentSiteIdList: SelectListDto[];
  public filtercurrentSiteIdList: SelectListDto[];

  public filterroleIdList: SelectListDto[];
  public roleIdList: SelectListDto[];

  constructor(private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private userSettingsService: UserSettingsService) {

  }

  @Input()
  public set model(userSettings: UsersettingsDto) {
    if (userSettings !== undefined) {
      this.username = userSettings.userDisplayName;
      this.editForm.reset(userSettings);
      this.active = true;
      this.isDuplicateMember = false;
      this.userSettingformFlag = true;

      this.eqmSettings = this.eqmSettingsService.getEqmSettings();
      if (this.isNew) {
        this.editForm.get('currentSiteId').clearValidators();
      }
      if (this.eqmSettings && this.eqmSettings.currentSiteId) {
        this.currentSiteId = this.eqmSettings.currentSiteId;
      }

      this.loadAllDropDowns();
    }
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<UsersettingsDto> = new EventEmitter();


  public loadAllDropDowns() {

    this.commonService.getAllWithId(this.currentSiteId, 'general', 'User').subscribe(eventResult => {
      this.filteruserIDList = this.userIdList = eventResult.results;
    });

    this.commonService.getAll('EquipmentMaintenance', 'Role').subscribe(eventResult => {
      this.filterroleIdList = this.roleIdList = eventResult.results;
    });
    this.commonService.getAll('General', 'Site').subscribe(eventResult => {
      this.filtercurrentSiteIdList = this.currentSiteIdList = eventResult.results;
    });
  }

  public onSave(e): void {
    e.preventDefault();
    this.save.emit(this.editForm.value);
    this.closeForm();
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
    this.isDuplicateMember = false;
  }

  private closeForm(): void {
    this.userSettingformFlag = false;
    this.cancel.emit();
  }

  private userIDListHandleFilter(value) {
    this.filteruserIDList = this.userIdList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private roleIdListHandleFilter(value) {
    this.filterroleIdList = this.roleIdList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private currentSiteIdListHandleFilter(value) {
    this.filtercurrentSiteIdList = this.currentSiteIdList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
}
