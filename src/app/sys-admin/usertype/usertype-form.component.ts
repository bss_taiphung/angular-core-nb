import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import "rxjs/add/observable/fromEvent";
import { UserTypeDto } from './service/usertype.model';
import { SelectListDto } from "../../shared/models/selectListDto";
import {CommonService } from "../../shared/services/common.service";


@Component({
  selector: 'kendo-grid-usertype-form',
  styles: [
    'input[type=text] { width: 100%; }'
  ],
  templateUrl: './html/usertype-form.component.html'
})
export class UserTypeFormComponent {
  public openUserForm = false;
  public userRoleList: SelectListDto[];
  public filterUserRoleList: SelectListDto[];
  public editForm: FormGroup = new FormGroup({
    'userTypeId': new FormControl(),
    'userTypeName': new FormControl(),
    'defaultUserRoleId': new FormControl()
  });

  constructor(private commonService: CommonService) {

  }

  @Input() public isNew = false;

  @Input() public set model(user: UserTypeDto) {

    this.editForm.reset(user);
    this.openUserForm = user !== undefined;

    if (this.openUserForm) {
      this.commonService.getAll("general", "UserRole").subscribe(eventResult => {
        this.userRoleList = eventResult.results;
        this.filterUserRoleList = this.userRoleList = eventResult.results;
      });
    }
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<UserTypeDto> = new EventEmitter();

  public onSave(e): void {
    e.preventDefault();
    this.save.emit(this.editForm.value);
    this.openUserForm = false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.openUserForm = false;
    this.cancel.emit();
  }

  private defaultUserRoleIdListHandleFilter(value) {
    this.filterUserRoleList = this.userRoleList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
}
