import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, HttpModule } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

export class UserTypeDto implements IUserTypeDto {
  userTypeId: number;
  userTypeName: string;
  defaultUserRoleId: number;

  constructor(data?: IUserTypeDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.userTypeId = data["userTypeId"];
      this.userTypeName = data["userTypeName"];
      this.defaultUserRoleId = data["defaultUserRoleId"];
    }
  }

  static fromJS(data: any): UserTypeDto {
    let result = new UserTypeDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data["userTypeId"] = this.userTypeId;
    data["userTypeName"] = this.userTypeName;
    data["defaultUserRoleId"] = this.defaultUserRoleId;

    return data;
  }
}

export interface IUserTypeDto {
  userTypeId: number;
  userTypeName: string;
  defaultUserRoleId: number;
}

export class CreateUserTypeDto implements ICreateUserTypeDto {
  userTypeId: number;
  userTypeName: string;
  defaultUserRoleId: number;

  constructor(data?: IUserTypeDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.userTypeId = data["userTypeId"];
      this.userTypeName = data["userTypeName"];
      this.defaultUserRoleId = data["defaultUserRoleId"];
    }
  }

  static fromJS(data: any): UserTypeDto {
    let result = new UserTypeDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["userTypeId"] = this.userTypeId;
    data["userTypeName"] = this.userTypeName;
    data["defaultUserRoleId"] = this.defaultUserRoleId;
    
    return data;
  }
}

export interface ICreateUserTypeDto {
  userTypeId: number;
  userTypeName: string;
  defaultUserRoleId: number;
}

export class PagedResultDtoOfUserTypeDto implements IPagedResultDtoOfUserTypeDto {
  results: UserTypeDto[];
  count: number;

  constructor(data?: IPagedResultDtoOfUserTypeDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data["results"] && data["results"].constructor === Array) {
        this.results = [];
        for (let item of data["results"])
          this.results.push(UserTypeDto.fromJS(item));
      }
         this.count = data["count"];
    }
  }

  static fromJS(data: any): PagedResultDtoOfUserTypeDto {
    let result = new PagedResultDtoOfUserTypeDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data["results"] = [];
      for (let item of this.results)
        data["results"].push(item.toJSON());
    }
    if (this.count) {
      data["count"] = this.count;
    }
    return data;
  }
}

export interface IPagedResultDtoOfUserTypeDto {
  results: UserTypeDto[];
  count: number;
}
