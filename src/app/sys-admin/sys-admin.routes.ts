import {RouterModule, Routes} from '@angular/router';
import {SysAdminComponent} from './sys-admin.component';
import {UserComponent} from './user/user.component';
import {AuthGuard} from '../shared/guards/auth.guard'
import {UserRoleComponent} from './userrole/userrole.component';
import {UserTypeComponent} from './usertype/usertype.component';

export const routes: Routes = [
  // {
  //   path: '',
  //   redirectTo : 'sysadmin',
  //   pathMatch: "full" 
  // },
  // {
  //   path: '**',
  //   redirectTo : 'sysadmin',

  // },
  {
    path: '', component: SysAdminComponent,
    canActivate: [AuthGuard],
    children: [
      {path: 'user', component: UserComponent, canActivate: [AuthGuard],},
      {path: 'userrole', component: UserRoleComponent, canActivate: [AuthGuard],},
      {path: 'usertype', component: UserTypeComponent, canActivate: [AuthGuard],}
    ]
  },
];

export const routing = RouterModule.forChild(routes);
