import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { DialogModule } from '@progress/kendo-angular-dialog';
import { GridModule } from "@progress/kendo-angular-grid";
import { InputsModule } from "@progress/kendo-angular-inputs";
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { TelerikReportingModule } from '@progress/telerik-angular-report-viewer';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';

import { SysAdminComponent } from './sys-admin.component';
import { SysadminNavmenuComponent } from './sysadmin-navmenu/sysadmin-navmenu.component';
import { routing} from './sys-admin.routes';
import { SharedModule } from '../shared/index';
import { UserComponent } from "./user/user.component";
import { UserFormComponent } from "./user/user-form.component";
import { UserService } from "./user/service/user.service";
import { UserRoleComponent } from "./userrole/userrole.component";
import { UserRoleFormComponent } from "./userrole/userrole-form.component";
import { UserRoleService } from "./userrole/service/userrole.service";
import { UserTypeComponent } from "./usertype/usertype.component";
import { UserTypeFormComponent } from "./usertype/usertype-form.component";
import { UserTypeService } from "./usertype/service/usertype.service";

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    ButtonsModule,
    GridModule,
    InputsModule,
    DialogModule,
    DateInputsModule,
    DropDownsModule,
    CommonModule,
    SharedModule,
    routing,
   // ToasterModule,
    TelerikReportingModule,
    ShowHidePasswordModule.forRoot()
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  declarations: [
    SysAdminComponent,
    SysadminNavmenuComponent,
    UserComponent,
    UserFormComponent,
    UserRoleComponent,
    UserRoleFormComponent,
    UserTypeComponent,
    UserTypeFormComponent
  ],
  providers: [
    UserService,
    UserRoleService,
    UserTypeService
  ]
})
export class SysAdminModule { }
