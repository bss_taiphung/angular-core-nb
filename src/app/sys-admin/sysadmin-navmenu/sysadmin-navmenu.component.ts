import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-sysadmin-navmenu',
  templateUrl: './sysadmin-navmenu.component.html',
  styleUrls: ['./sysadmin-navmenu.component.scss']
})
export class SysadminNavmenuComponent implements OnInit {

  treeViewActive: boolean = false;
  private sysAdminMenu: any = [
    'user'
    ];
  constructor(private route: ActivatedRoute, private router: Router) {
    let path = location.href.split('/')[4];
    let isMenu = this.sysAdminMenu.find(res => res === path);
    if (isMenu)
      this.treeViewActive = true;
  }

  ngOnInit() {
  }

  activeMenu(path) {
    let isMenu = this.sysAdminMenu.find(res => res === path);
    if (isMenu)
      this.treeViewActive = true;
  }

}
