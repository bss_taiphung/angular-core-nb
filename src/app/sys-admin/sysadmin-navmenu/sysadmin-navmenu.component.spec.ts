import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SysadminNavmenuComponent } from './sysadmin-navmenu.component';

describe('SysadminNavmenuComponent', () => {
  let component: SysadminNavmenuComponent;
  let fixture: ComponentFixture<SysadminNavmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SysadminNavmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SysadminNavmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
