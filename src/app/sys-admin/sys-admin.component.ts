import { Component, OnInit, HostListener } from '@angular/core';
import { ToasterConfig } from 'angular2-toaster';

import { Globals } from '../shared/globals/globals';
import { ToasterHelperService } from "../shared/services/toasterHelper.service";

@Component({
  selector: 'app-sys-admin',
  template: '<router-outlet></router-outlet>'
})
export class SysAdminComponent implements OnInit {
  toasterconfig: ToasterConfig;
  constructor(private globals: Globals, private toaster: ToasterHelperService) {
    this.toasterconfig = toaster.toasterconfig;
  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    document.getElementById("contentWrapper").style.height = (window.innerHeight - 64) + 'px';
  }

  public hideRightMenu() {
    if (window.screen.width < 767) {
      this.globals.toogleMenu = false;
    }

    this.globals.userMenu = false;
    this.globals.userProfileMenu = false;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    document.getElementById("contentWrapper").style.height = (window.innerHeight - 64) + 'px';
  }

}
