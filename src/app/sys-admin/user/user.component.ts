import {Component, HostListener, OnInit, ViewEncapsulation} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {Observable} from 'rxjs/Observable';

import {HubConnection} from '@aspnet/signalr-client';
import {SortDescriptor, State} from '@progress/kendo-data-query';

import {DataStateChangeEvent, GridDataResult, PageChangeEvent} from '@progress/kendo-angular-grid';
import {FormGroup} from '@angular/forms';
import {UserService} from './service/user.service'
import {CreateUserDto, IUserDto, UserDto} from './service/user.model';

import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {UserRole} from '../../shared/models/eqmSettings'
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'user',
  templateUrl: './html/user.component.html',
  styleUrls: ['./css/user.component.css'],
  encapsulation: ViewEncapsulation.None
})


export class UserComponent implements OnInit {

  data: IUserDto[];
  public formGroup: FormGroup;
  public view: Observable<GridDataResult>;
  private editedRowIndex: number;
  public editDataItem: CreateUserDto;
  public isNew: boolean;
  public pageSize = 10;
  public skip = 0;
  public state: State = {
    skip: 0,
    take: 5,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };
  public gridView: GridDataResult;
  public deleteDataItem: any;
  private sort: SortDescriptor[] = [];
  public isactive = true;
  public searchtext = '';

  private hubConnection: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  roleSettings: UserRole = new UserRole();

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private userService: UserService) {
    this.roleSettings = this.eqmSettingsService.getUserRoles();
  }

  ngOnInit() {
    this.loadGridData();

    /*this.hubConnection = new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
     .start()
     .then(() => {
       console.log('Connection started!');

       this.hubConnection
         .invoke('Subscribe', 'General', 'User', 'SiteId', '5')
         .catch(err => console.error(err));
     })
     .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
     const _responseText = messageJson;
     let result200: any = null;
     let data = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
     let resultData200 = this.commonService.toCamel(data);
     result200 = resultData200 ? UserDto.fromJS(resultData200) : new UserDto();

     var dataExist = this.containsObject(result200, this.gridView.data);

     if (operation.toLowerCase() === "insert" && !dataExist) {
       this.gridView.data.unshift(result200);
     }

     if (operation.toLowerCase() === "update") {
       this.gridView.data.forEach((element, index) => {
         if (element.userId === result200.userId) {
           if (this.isactive) {
             if (result200.active == this.isactive) {
               this.gridView.data[index] = result200;
             } else {
               operation = "delete";
             }
           } else {
             this.gridView.data[index] = result200;
           }
         }
       });
     }

     if (operation.toLowerCase() === "delete" && dataExist) {
       var index = null;
       this.gridView.data.forEach((element, i) => {
         if (element.userId === result200.userId) {
           index = i;
         }
       });
       if (index !== null) {
         this.gridView.data.splice(index, 1);
       }
     }

    });*/

  }

  containsObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].userId === obj.userId) {
        return true;
      }
    }

    return false;
  }

  // Dialog box
  opened = false;
  loading = true;
  gridLoading = true;

  public close(user) {
    let $this = this;
    if (user) {
      this.gridLoading = true;
      this.userService.delete($this.deleteDataItem.userId,
        response => {
          this.toasterService.success('', 'User Removed Successfully');
          this.gridLoading = false;
          this.loadGridData(); // Remove after signalR code
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    $this.deleteDataItem = null;
    $this.opened = false;
  }

  public open() {
    this.opened = true;
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.loadGridData();
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
    this.loadGridData();
  }

  private loadGridData(): void {
    this.userService.getAll(this.pageSize, this.skip, this.sort, this.isactive, this.searchtext).subscribe(eventResult => {
      this.gridView = {
        data: eventResult.results,
        total: eventResult.count
      };
      this.loading = false;
      this.gridLoading = false;
    });
  }

  public addHandler({sender}) {
    this.roleSettings = this.eqmSettingsService.getUserRoles();
    if (!this.roleSettings.canAccessUserTypes) {
      this.toasterService.error('', 'You do not have permission to add new user.');
      return false;
    } else {
      this.closeEditor(sender);
      this.editDataItem = new CreateUserDto();
      this.isNew = true;
    }
  }

  public editHandler({dataItem}) {

    this.editDataItem = dataItem;
    this.isNew = false;
  }

  public saveHandler(data) {
    this.gridLoading = true;
    data.departmentId = (data.departmentId == null) ? 1 : data.departmentId;
    if (this.isNew) {
      const inputData: CreateUserDto = data;
      this.userService.create(inputData,
        response => {
          this.toasterService.success('', 'User Saved Successfully');
          this.gridLoading = false;
          this.loadGridData(); // Remove after signalR code
        },
        error => {

          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    else {
      const inputData: UserDto = data;
      this.userService.update(inputData,
        response => {
          this.toasterService.success('', 'User Updated Successfully');
          this.gridLoading = false;
          this.loadGridData(); // Remove after signalR code
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public cancelHandler() {
    this.editDataItem = undefined;
    this.formGroup = undefined;
  }

  public removeHandler({dataItem}) {
    this.deleteDataItem = dataItem;
    this.open();
  }

  public showActive() {
    this.isactive = !this.isactive;
    this.gridLoading = true;
    this.loadGridData();
  }

  public onSearchKeyup() {
    this.skip = 0;
    this.pageSize = 10;
    this.gridLoading = true;
    this.loadGridData();
  }

  public ActionData: Array<any> = [{
    text: 'Delete',
    icon: 'trash'
  }];

  public onAction(e, dataItem) {
    this.roleSettings = this.eqmSettingsService.getUserRoles();
    if (e === undefined) {
      if (!this.roleSettings.canAccessUserTypes) {
        this.toasterService.error('', 'You do not have permission to update user.');
        return false;
      } else {
        this.editDataItem = dataItem;
        this.isNew = false;
      }
    } else {
      if (!this.roleSettings.canAccessUserTypes) {
        this.toasterService.error('', 'You do not have permission to remove user.');
        return false;
      } else {
        if (e.text === 'Delete') {
          this.deleteDataItem = dataItem;
          this.open();
        }
      }
    }
  }


  timeout = null;

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridLoading = true;

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.doSearch(state, this);
    }, 1000);
  }

  doSearch(state: any, that) {
    if (state.filter) {
      let search = that.commonService.getFilter(state.filter.filters, that.isactive);

      that.userService.getAllWtihFilter(state.take, state.skip, that.sort, this.isactive, search)
        .subscribe(eventResult => {
          that.gridView = {
            data: eventResult.results,
            total: eventResult.count
          };
          that.loading = false;
          that.gridLoading = false;
        });

      this.skip = state.skip;
      this.pageSize = state.take;
    }
    else {
      that.loadGridData();
    }
  }

  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {
      this.cancelHandler();
      this.close(false);
    }
  }
}
