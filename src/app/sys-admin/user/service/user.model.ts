import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, HttpModule } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';


export class UserDto implements IUserDto {
  userId: number;
  firstName: string;
  lastName: string;
  userPIN: number;
  password: string;
  email: string;
  userTypeId: number;
  userTypeName: string;
  siteId: number;
  mobilePhone: string;
  pager: string;
  o365pw: string;
  o365user: string;
  proxCardCode: string;
  supervisorUserId: number;
  departmentId: number;
  userRoleId: number;
  imageFileName: string;
  imageFile: string;
  thumbnailFile: string;
  roleName: string;
  receiveEmailNotifications: boolean;
  receiveSMSNotifications: boolean;
  laborReporting:boolean;
  active: boolean;
  chgPWDNextLogin:boolean;
  constructor(data?: IUserDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.userId = data["userId"];
      this.firstName = data["firstName"];
      this.lastName = data["lastName"];
      this.userPIN = data["userPIN"];
      this.password = data["password"];
      this.email = data["email"];
      this.userTypeId = data["userTypeId"];
      this.userTypeName = data["userTypeName"];
      this.siteId = data["siteId"];
      this.mobilePhone = data["mobilePhone"];
      this.pager = data["pager"];
      this.o365pw = data["o365pw"];
      this.o365user = data["o365User"];
      this.proxCardCode = data["proxCardCode"];
      this.supervisorUserId = data["supervisorUserId"];
      this.departmentId = data["departmentId"];
      this.userRoleId = data["userRoleId"];
      this.roleName = data["roleName"];
      this.imageFileName = data["imageFileName"];
      this.imageFile = data["imageFile"];
      this.thumbnailFile = data["thumbnailFile"];
      this.receiveEmailNotifications = data["receiveEmailNotifications"];
      this.laborReporting=data['laborReporting'];
      this.receiveSMSNotifications = data["receiveSMSNotifications"];
      this.active = data["active"];
      this.chgPWDNextLogin=data["chgPWDNextLogin"];
    }
  }

  static fromJS(data: any): UserDto {
    let result = new UserDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data["userId"] = this.userId;
    data["firstName"] = this.firstName;
    data["lastName"] = this.lastName;
    data["userPIN"] = this.userPIN;
    data["password"] = this.password;
    data["email"] = this.email;
    data["userTypeId"] = this.userTypeId;
    data["userTypeName"] = this.userTypeName;
    data["siteId"] = this.siteId;
    data["mobilePhone"] = this.mobilePhone;
    data["pager"] = this.pager;
    data["o365pw"] = this.o365pw;
    data["o365user"] = this.o365user;
    data["proxCardCode"] = this.proxCardCode;
    data["supervisorUserId"] = this.supervisorUserId;
    data["departmentId"] = this.departmentId;
    data["userRoleId"] = this.userRoleId;
    data["roleName"] = this.roleName;
    data["imageFileName"] = this.imageFileName;
    data["imageFile"] = this.imageFile;
    data["thumbnailFile"] = this.thumbnailFile;
    data["receiveEmailNotifications"] = this.receiveEmailNotifications;
    data["laborReporting"]=this.laborReporting;
    data["receiveSMSNotifications"] = this.receiveSMSNotifications;
    data["active"] = this.active;
    data["chgPWDNextLogin"]=this.chgPWDNextLogin
    return data;
  }
}

export interface IUserDto {
  userId: number;
  firstName: string;
  lastName: string;
  userPIN: number;
  password: string;
  email: string;
  userTypeId: number;
  userTypeName: string;
  siteId: number;
  mobilePhone: string;
  pager: string;
  o365pw: string;
  o365user: string;
  proxCardCode: string;
  supervisorUserId: number;
  departmentId: number;
  userRoleId: number;
  roleName: string;
  imageFileName: string;
  imageFile: string;
  thumbnailFile: string;
  receiveEmailNotifications: boolean;
  laborReporting:boolean;
  receiveSMSNotifications: boolean;
  active: boolean;
  chgPWDNextLogin:boolean;
}

export class CreateUserDto implements ICreateUserDto {
  userId: number;
  firstName: string;
  lastName: string;
  userPIN: number;
  password: string;
  email: string;
  userTypeId: number;
  siteId: number;
  mobilePhone: string;
  pager: string;
  o365pw: string;
  o365user: string;
  proxCardCode: string;
  supervisorUserId: number;
  departmentId: number;
  userRoleId: number;
  imageFileName: string;
  imageFile: string;
  thumbnailFile: string;
  receiveEmailNotifications: boolean;
  laborReporting:boolean;
  receiveSMSNotifications: boolean;
  active: boolean;
  chgPWDNextLogin:boolean;

  constructor(data?: IUserDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.userId = data["userId"];
      this.firstName = data["firstName"];
      this.lastName = data["lastName"];
      this.userPIN = data["userPIN"];
      this.password = data["password"];
      this.email = data["email"];
      this.userTypeId = data["userTypeId"];
      this.siteId = data["siteId"];
      this.mobilePhone = data["mobilePhone"];
      this.pager = data["pager"];
      this.o365pw = data["o365pw"];
      this.o365user = data["o365User"];
      this.proxCardCode = data["proxCardCode"];
      this.supervisorUserId = data["supervisorUserId"];
      this.departmentId = data["departmentId"];
      this.userRoleId = data["userRoleId"];
      this.imageFileName = data["imageFileName"];
      this.imageFile = data["imageFile"];
      this.thumbnailFile = data["thumbnailFile"];
      this.receiveEmailNotifications = data["receiveEmailNotifications"];
      this.laborReporting=data["laborReporting"];
      this.receiveSMSNotifications = data["receiveSMSNotifications"];
      this.active = data["active"];
      this.chgPWDNextLogin=data["chgPWDNextLogin"];
    }
  }

  static fromJS(data: any): UserDto {
    let result = new UserDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["userId"] = this.userId;
    data["firstName"] = this.firstName;
    data["lastName"] = this.lastName;
    data["userPIN"] = this.userPIN;
    data["password"] = this.password;
    data["email"] = this.email;
    data["userTypeId"] = this.userTypeId;
    data["siteId"] = this.siteId;
    data["mobilePhone"] = this.mobilePhone;
    data["pager"] = this.pager;
    data["o365pw"] = this.o365pw;
    data["o365user"] = this.o365user;
    data["proxCardCode"] = this.proxCardCode;
    data["supervisorUserId"] = this.supervisorUserId;
    data["departmentId"] = this.departmentId;
    data["userRoleId"] = this.userRoleId;
    data["imageFileName"] = this.imageFileName;
    data["imageFile"] = this.imageFile;
    data["thumbnailFile"] = this.thumbnailFile;
    data["receiveEmailNotifications"] = this.receiveEmailNotifications;
    data["laborReporting"]=this.laborReporting;
    data["receiveSMSNotifications"] = this.receiveSMSNotifications;
    data["active"] = this.active;
    data["chgPWDNextLogin"]=this.chgPWDNextLogin;
    return data;
  }
}

export interface ICreateUserDto {
  userId: number;
  firstName: string;
  lastName: string;
  userPIN: number;
  password: string;
  email: string;
  userTypeId: number;
  siteId: number;
  mobilePhone: string;
  pager: string;
  o365pw: string;
  o365user: string;
  proxCardCode: string;
  supervisorUserId: number;
  departmentId: number;
  userRoleId: number;
  imageFileName: string;
  imageFile: string;
  thumbnailFile: string;
  receiveEmailNotifications: boolean;
  laborReporting:boolean;
  receiveSMSNotifications: boolean;
  active: boolean;
  chgPWDNextLogin:boolean;
}

export class PagedResultDtoOfUserDto implements IPagedResultDtoOfUserDto {
  results: UserDto[];
  count: number;

  constructor(data?: IPagedResultDtoOfUserDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data["results"] && data["results"].constructor === Array) {
        this.results = [];
        for (let item of data["results"])
          this.results.push(UserDto.fromJS(item));
      }
         this.count = data["count"];
    }
  }

  static fromJS(data: any): PagedResultDtoOfUserDto {
    let result = new PagedResultDtoOfUserDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data["results"] = [];
      for (let item of this.results)
        data["results"].push(item.toJSON());
    }
    if (this.count) {
      data["count"] = this.count;
    }
    return data;
  }
}

export interface IPagedResultDtoOfUserDto {
  results: UserDto[];
  count: number;
}
