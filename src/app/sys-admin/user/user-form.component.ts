import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { UserService } from "./service/user.service"
import "rxjs/add/observable/fromEvent";
import { UserDto } from "./service/user.model";
import { DomSanitizer } from '@angular/platform-browser';
import { CommonService } from "../../shared/services/common.service";
import { SelectListDto } from "../../shared/models/selectListDto";
import {environment} from '../../../environments/environment';

@Component({
  selector: 'kendo-grid-user-form',
  styles: [
    'input[type=text] { width: 100%; }'
  ],
  templateUrl: './html/user-form.component.html'
})
export class UserFormComponent {
  public openUserForm = false;
  public invalidPass = false;
  public invalidPin = false;
  public isDuplicateUserPinErrorMsg = false;
  public userFormLoading = false;
  public userRoleList: SelectListDto[];
  public filterUserRoleList: SelectListDto[];
  public usersList: SelectListDto[];
  public filterUsersList: SelectListDto[];
  public siteList: SelectListDto[];
  public filterSiteList: SelectListDto[];
  public userTypeList: SelectListDto[];
  public filterUserTypeList: SelectListDto[];
  public base64ImgFile: string;
  public _azureStorageContainer: string;
  public mask: string = '';
  public invalidateEmail:boolean = false;
  //public value: string = '';

  public editForm: FormGroup = new FormGroup({
    'userId': new FormControl(),
    'firstName': new FormControl('', Validators.required),
    'lastName': new FormControl('', Validators.required),
    'userPIN': new FormControl('', Validators.required),
    'password': new FormControl(''),
    'email': new FormControl(''),
    'userTypeId': new FormControl(false),
    'siteId': new FormControl(),
    'mobilePhone': new FormControl(false),
    'pager': new FormControl(false),
    'o365pw': new FormControl(false),
    'o365user': new FormControl(false),
    'proxCardCode': new FormControl(false),
    'supervisorUserId': new FormControl(false),
    'departmentId': new FormControl(false),
    'userRoleId': new FormControl(false, Validators.required),
    'uploadUserImg': new FormControl(),
    'imageFileName': new FormControl(),
    'imageFile': new FormControl(),
    'receiveEmailNotifications': new FormControl(false),
    'receiveSMSNotifications': new FormControl(false),
    'laborReporting':new FormControl(false),
    'active': new FormControl(false),
    'chgPWDNextLogin':new FormControl(false)
  });

  constructor(private commonService: CommonService, private domSanitizer: DomSanitizer, private userService: UserService) {
    this._azureStorageContainer = environment.azureStorageContainer;
  }

  @Input() public isNew = false;

  @Input() public set model(user: UserDto) {
    
    if (user != undefined) {
      user.password = '';
      user.o365pw = '';
    }

   
    this.mask = "(999) 000-00-00";
    this.editForm.reset(user);
    this.openUserForm = user !== undefined;
    //https://itwmss.blob.core.windows.net:443/mss-dev/general_useravatar/62.png
    if (user != undefined && !this.isNew) {
      var newstr = this.commonService.getUserThumbnail(user.userId.toString(),150,150);
      this.base64ImgFile = newstr;
    } else {
      this.base64ImgFile = "http://www.astrofisicamas.cl/wp-content/uploads/2016/11/blank.jpg";
    }

    if (this.openUserForm) {
      this.commonService.getAll("general", "User").subscribe(eventResult => {
        this.usersList = eventResult.results;
        this.filterUsersList = this.usersList = eventResult.results;
      });

      this.commonService.getAll("general", "UserRole").subscribe(eventResult => {
        this.userRoleList = eventResult.results;
        this.filterUserRoleList = this.userRoleList = eventResult.results;
      });

      this.commonService.getAll("general", "Site").subscribe(eventResult => {
        this.siteList = eventResult.results;
        this.filterSiteList = this.siteList = eventResult.results;
      });

      this.commonService.getAll("general", "UserType").subscribe(eventResult => {
        this.userTypeList = eventResult.results;
        this.filterUserTypeList = this.userTypeList = eventResult.results;
      });
    }
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<UserDto> = new EventEmitter();

  validateEmailFunction(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
 
  public onSave(e): void {
    e.preventDefault();
    let email = this.editForm.controls["email"].value;
    if (email != "") {
      if (!this.validateEmailFunction(email)) {
        this.invalidateEmail = true;
        return;
      } else {
        this.invalidateEmail = false;
      }
    }
    this.invalidPass = false;
    this.save.emit(this.editForm.value);
    this.openUserForm = false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.openUserForm = false;
    this.cancel.emit();
  }

  public validatePassword() {
    let password = this.editForm.controls["password"].value;

    if (password == null || password == "") {
      this.invalidPass = false;
      return true;
    }

    if (password.length < 8 || password.length > 12) {
      this.editForm.controls["password"].updateValueAndValidity();
      this.invalidPass = true;
      return false;
    }

    let hasUpper = /[A-Z]/.test(password);
    let hasLower = /[a-z]/.test(password);
    let hasNumber = /\d/.test(password);
    let hasSpecialChar = /(?=.*?[#?!@$%^&*-])/.test(password);

    if (hasUpper && hasLower && (hasNumber || hasSpecialChar)) {
      this.invalidPass = false;
      return true;
    }
    this.editForm.controls["password"].updateValueAndValidity();
    this.invalidPass = true;
    return false;
  }

  public validateUserPin() {
    let userPin = this.editForm.controls["userPIN"].value;

    if (userPin == "" || userPin == null) {
      this.isDuplicateUserPinErrorMsg = false;
      this.invalidPin = false;
      return true;
    }

    let hasNumber = /\d/.test(userPin);

    if (hasNumber) {
      this.invalidPin = false;
      this.userFormLoading = true;

      this.userService.isDuplicateUserPin(userPin, 
        response => {
          this.isDuplicateUserPinErrorMsg = !!response;
          this.userFormLoading = false;
        },
        error => {
          this.userFormLoading = false;
        }
      );

      return true;
    }
    this.editForm.controls["userPIN"].updateValueAndValidity();
    this.invalidPin = true;
    return false;
  }

  public findNextPin(event) {
    let userPin = this.editForm.controls["userPIN"].value;
    this.userFormLoading = true;
    this.isDuplicateUserPinErrorMsg = false;
    this.userService.findNextUserPin(userPin,
      response => {
      this.isDuplicateUserPinErrorMsg = false;
      this.editForm.controls["userPIN"].setValue(response);
      this.userFormLoading = false;
      },
      error => {
        this.isDuplicateUserPinErrorMsg = false;
        this.userFormLoading = false;
      }
    );
  }

  public onFileChange(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        var filetype = file.name.split('.')[1];
        this.editForm.controls["imageFile"].setValue(reader.result.split(',')[1]);
        this.editForm.controls["imageFileName"].setValue(file.name);
        var imgFile = reader.result.split(',')[1];
        this.base64ImgFile = "data:image/" + filetype + ";base64," + imgFile;
      };
    }
  }

  private userRoleIdListHandleFilter(value) {
    this.filterUserRoleList = this.userRoleList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private supervisorUserIdListHandleFilter(value) {
    this.filterUsersList = this.usersList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private siteListHandleFilter(value) {
    this.filterSiteList = this.siteList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private userTypeListHandleFilter(value) {
    this.filterUserTypeList = this.userTypeList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
}
