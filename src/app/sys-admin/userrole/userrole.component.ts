import {Component, HostListener, OnInit, ViewEncapsulation} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {Observable} from 'rxjs/Observable';
import {FormGroup} from '@angular/forms';

import {HubConnection} from '@aspnet/signalr-client';
import {SortDescriptor, State} from '@progress/kendo-data-query';
import {DataStateChangeEvent, GridDataResult, PageChangeEvent} from '@progress/kendo-angular-grid';

import {UserRoleService} from './service/userrole.service'
import {CreateUserRoleDto, IUserRoleDto, UserRoleDto} from './service/userrole.model';

import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {UserRole} from '../../shared/models/eqmSettings'
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'userrole',
  templateUrl: './html/userrole.component.html',
  styleUrls: ['./css/userrole.component.css'],
  encapsulation: ViewEncapsulation.None
})


export class UserRoleComponent implements OnInit {

  data: IUserRoleDto[];
  public formGroup: FormGroup;
  public view: Observable<GridDataResult>;
  private editedRowIndex: number;
  public editDataItem: CreateUserRoleDto;
  public isNew: boolean;
  public pageSize = 10;
  public skip = 0;
  public state: State = {
    skip: 0,
    take: 5,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };
  public gridView: GridDataResult;
  public deleteDataItem: any;
  private sort: SortDescriptor[] = [];
  public isactive = true;
  public searchtext = '';

  private hubConnection: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  roleSettings: UserRole = new UserRole();

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private userRoleService: UserRoleService,
              private eqmSettingsService: EqmSettingsService) {
    this.roleSettings = this.eqmSettingsService.getUserRoles();
  }

  ngOnInit() {
    this.loadGridData();

    /*this.hubConnection = new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
     .start()
     .then(() => {
       console.log('Connection started!');

       this.hubConnection
         .invoke('Subscribe', 'General', 'UserRole', 'SiteId', '5')
         .catch(err => console.error(err));
     })
     .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
     const _responseText = messageJson;
     let result200: any = null;
     let data = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
     let resultData200 = this.commonService.toCamel(data);
     result200 = resultData200 ? RoleDto.fromJS(resultData200) : new RoleDto();

     var dataExist = this.containsObject(result200, this.gridView.data);

     if (operation.toLowerCase() === "insert" && !dataExist) {
       this.gridView.data.unshift(result200);
     }

     if (operation.toLowerCase() === "update") {
       this.gridView.data.forEach((element, index) => {
         if (element.userRoleId === result200.userRoleId) {
           if (this.isactive) {
             if (result200.active == this.isactive) {
               this.gridView.data[index] = result200;
             } else {
               operation = "delete";
             }
           } else {
             this.gridView.data[index] = result200;
           }
         }
       });
     }

     if (operation.toLowerCase() === "delete" && dataExist) {
       var index = null;
       this.gridView.data.forEach((element, i) => {
         if (element.userRoleId === result200.userRoleId) {
           index = i;
         }
       });
       if (index !== null) {
         this.gridView.data.splice(index, 1);
       }
     }

    });*/

  }

  containsObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].userRoleId === obj.userRoleId) {
        return true;
      }
    }

    return false;
  }

  // Dialog box
  opened = false;
  loading = true;
  gridLoading = true;

  public close(userRole) {
    const $this = this;
    if (userRole) {
      this.gridLoading = true;
      this.userRoleService.delete($this.deleteDataItem.userRoleId,
        response => {
          this.toasterService.success('', 'UserRole Removed Successfully');
          this.loadGridData(); // Remove after signalR code
          this.gridLoading = false;
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    $this.deleteDataItem = null;
    $this.opened = false;
  }

  public open() {
    this.opened = true;
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.loadGridData();
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
    this.loadGridData();
  }

  private loadGridData(): void {
    this.userRoleService.getAll(this.pageSize, this.skip, this.sort, this.isactive, this.searchtext).subscribe(eventResult => {
      this.gridView = {
        data: eventResult.results,
        total: eventResult.count
      };
      this.loading = false;
      this.gridLoading = false;
    });
  }

  public addHandler({sender}) {
    this.roleSettings = this.eqmSettingsService.getUserRoles();
    if (!this.roleSettings.canAccessUserTypes) {
      this.toasterService.error('', 'You do not have permission to add new user role.');
      return false;
    } else {
      this.closeEditor(sender);
      this.editDataItem = new CreateUserRoleDto();
      this.isNew = true;
    }
  }

  public editHandler({dataItem}) {

    this.editDataItem = dataItem;
    this.isNew = false;
  }

  public saveHandler(data) {
    this.gridLoading = true;
    if (this.isNew) {

      const inputData: CreateUserRoleDto = data;
      this.userRoleService.create(inputData,
        response => {
          this.toasterService.success('', 'UserRole Saved Successfully');
          this.loadGridData(); // Remove after signalR code
          this.gridLoading = false;
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    else {
      const inputData: UserRoleDto = data;
      this.userRoleService.update(inputData,
        response => {
          this.toasterService.success('', 'UserRole Updated Successfully');
          this.loadGridData(); // Remove after signalR code
          this.gridLoading = false;
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public cancelHandler() {
    this.editDataItem = undefined;
    this.formGroup = undefined;
  }

  public removeHandler({dataItem}) {
    this.deleteDataItem = dataItem;
    this.open();
  }

  public showActive() {
    this.isactive = !this.isactive;
    this.gridLoading = true;
    this.loadGridData();
  }

  public onSearchKeyup() {
    this.skip = 0;
    this.pageSize = 10;
    this.gridLoading = true;
    this.loadGridData();
  }

  public ActionData: Array<any> = [{
    text: 'Delete',
    icon: 'trash'
  }];

  public onAction(e, dataItem) {
    this.roleSettings = this.eqmSettingsService.getUserRoles();
    if (e === undefined) {
      if (!this.roleSettings.canAccessUserTypes) {
        this.toasterService.error('', 'You do not have permission to update user role.');
        return false;
      } else {
        this.editDataItem = dataItem;
        this.isNew = false;
      }
    } else {
      if (e.text === 'Delete') {
        if (!this.roleSettings.canAccessUserTypes) {
          this.toasterService.error('', 'You do not have permission to remove user role.');
          return false;
        } else {
          this.deleteDataItem = dataItem;
          this.open();
        }
      }
    }
  }


  timeout = null;

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridLoading = true;

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.doSearch(state, this);
    }, 1000);
  }

  doSearch(state: DataStateChangeEvent, that) {
    if (state.filter) {
      const search = that.commonService.getFilter(state.filter.filters);

      if (search === '') {
        that.loading = false;
        that.gridLoading = false;

        return;
      }

      that.userRoleService.getAllWtihFilter(that.pageSize, that.skip, that.sort, that.isactive, search)
        .subscribe(eventResult => {
          that.gridView = {
            data: eventResult.results,
            total: eventResult.count
          };
          that.loading = false;
          that.gridLoading = false;
        });
    }
    else {
      that.loadGridData();
    }
  }

  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {
      this.cancelHandler();
      this.close(false);
    }
  }
}
