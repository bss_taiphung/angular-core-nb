import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import "rxjs/add/observable/fromEvent";
import { UserRoleDto } from './service/userrole.model';

@Component({
  selector: 'kendo-grid-userrole-form',
  styles: [
    'input[type=text] { width: 100%; }'
  ],
  templateUrl: './html/userrole-form.component.html'
})
export class UserRoleFormComponent {
  public openUserForm = false;
  public editForm: FormGroup = new FormGroup({
    'userRoleId': new FormControl(),
    'userRoleName': new FormControl('', Validators.required),
    'canAccessUserMaintenance': new FormControl(false),
    'canViewSysAdmin': new FormControl(false),
    'canViewProjects': new FormControl(false),
    'canViewEquipmentMaintenance': new FormControl(false),
    'canViewProcessLog': new FormControl(false),
    'canViewToolingManagement': new FormControl(false),
    'canViewReportingAndAnalytics': new FormControl(false),
    'canViewDocumentManagement': new FormControl(false),
    'canViewDispatch': new FormControl(false),
    'canViewQualityManagement': new FormControl(false),
    'canViewWIPTracking': new FormControl(false),
    'canViewSuppliers': new FormControl(false),
    'canViewProductionSupport': new FormControl(false),
    'canAccessUserTypes': new FormControl(false),
  });

  @Input() public isNew = false;

  @Input() public set model(user: UserRoleDto) {
    this.editForm.reset(user);
    this.openUserForm = user !== undefined;
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<UserRoleDto> = new EventEmitter();

  public onSave(e): void {
    e.preventDefault();
    this.save.emit(this.editForm.value);
    this.openUserForm = false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.openUserForm = false;
    this.cancel.emit();
  }
}
