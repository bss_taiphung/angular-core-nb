import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, HttpModule } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

export class UserRoleDto implements IUserRoleDto {
  userRoleId: number;
  userRoleName: string;
  canAccessUserMaintenance: boolean;
  canViewSysAdmin: boolean;
  canViewProjects: boolean;
  canViewEquipmentMaintenance: boolean;
  canViewProcessLog: boolean;
  canViewToolingManagement: boolean;
  canViewReportingAndAnalytics: boolean;
  canViewDocumentManagement: boolean;
  canViewDispatch: boolean;
  canViewQualityManagement: boolean;
  canViewWIPTracking: boolean;
  canViewSuppliers: boolean;
  canViewProductionSupport: boolean;
  canAccessUserTypes: boolean;

  constructor(data?: IUserRoleDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.userRoleId = data["userRoleId"];
      this.userRoleName = data["userRoleName"];
      this.canAccessUserMaintenance = data["canAccessUserMaintenance"];
      this.canViewSysAdmin = data["canViewSysAdmin"];
      this.canViewProjects = data["canViewProjects"];
      this.canViewEquipmentMaintenance = data["canViewEquipmentMaintenance"];
      this.canViewProcessLog = data["canViewProcessLog"];
      this.canViewToolingManagement = data["canViewToolingManagement"];
      this.canViewReportingAndAnalytics = data["canViewReportingAndAnalytics"];
      this.canViewDocumentManagement = data["canViewDocumentManagement"];
      this.canViewDispatch = data["canViewDispatch"];
      this.canViewQualityManagement = data["canViewQualityManagement"];
      this.canViewWIPTracking = data["canViewWIPTracking"];
      this.canViewDocumentManagement = data["canViewDocumentManagement"];
      this.canViewSuppliers = data["canViewSuppliers"];
      this.canViewProductionSupport = data["canViewProductionSupport"];
      this.canAccessUserTypes = data["canAccessUserTypes"];
    }
  }

  static fromJS(data: any): UserRoleDto {
    let result = new UserRoleDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data["userRoleId"] = this.userRoleId;
    data["userRoleName"] = this.userRoleName;
    data["canAccessUserMaintenance"] = this.canAccessUserMaintenance;
    data["canViewSysAdmin"] = this.canViewSysAdmin;
    data["canViewProjects"] = this.canViewProjects;
    data["canViewEquipmentMaintenance"] = this.canViewEquipmentMaintenance;
    data["canViewProcessLog"] = this.canViewProcessLog;
    data["canViewToolingManagement"] = this.canViewToolingManagement;
    data["canViewReportingAndAnalytics"] = this.canViewReportingAndAnalytics;
    data["canViewDocumentManagement"] = this.canViewDocumentManagement;
    data["canViewDispatch"] = this.canViewDispatch;
    data["canViewQualityManagement"] = this.canViewQualityManagement;
    data["canViewWIPTracking"] = this.canViewWIPTracking;
    data["canViewDocumentManagement"] = this.canViewDocumentManagement;
    data["canViewSuppliers"] = this.canViewSuppliers;
    data["canViewProductionSupport"] = this.canViewProductionSupport;
    data["canAccessUserTypes"] = this.canAccessUserTypes;
    return data;
  }
}

export interface IUserRoleDto {

  userRoleId: number;
  userRoleName: string;
  canAccessUserMaintenance: boolean;
  canViewSysAdmin: boolean;
  canViewProjects: boolean;
  canViewEquipmentMaintenance: boolean;
  canViewProcessLog: boolean;
  canViewToolingManagement: boolean;
  canViewReportingAndAnalytics: boolean;
  canViewDocumentManagement: boolean;
  canViewDispatch: boolean;
  canViewQualityManagement: boolean;
  canViewWIPTracking: boolean;
  canViewSuppliers: boolean;
  canViewProductionSupport: boolean;
  canAccessUserTypes: boolean;
}

export class CreateUserRoleDto implements ICreateUserRoleDto {
  userRoleId: number;
  userRoleName: string;
  canAccessUserMaintenance: boolean;
  canViewSysAdmin: boolean;
  canViewProjects: boolean;
  canViewEquipmentMaintenance: boolean;
  canViewProcessLog: boolean;
  canViewToolingManagement: boolean;
  canViewReportingAndAnalytics: boolean;
  canViewDocumentManagement: boolean;
  canViewDispatch: boolean;
  canViewQualityManagement: boolean;
  canViewWIPTracking: boolean;
  canViewSuppliers: boolean;
  canViewProductionSupport: boolean;
  canAccessUserTypes: boolean;

  constructor(data?: IUserRoleDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.userRoleId = data["userRoleId"];
      this.userRoleName = data["userRoleName"];
      this.canAccessUserMaintenance = data["canAccessUserMaintenance"];
      this.canViewSysAdmin = data["canViewSysAdmin"];
      this.canViewProjects = data["canViewProjects"];
      this.canViewEquipmentMaintenance = data["canViewEquipmentMaintenance"];
      this.canViewProcessLog = data["canViewProcessLog"];
      this.canViewToolingManagement = data["canViewToolingManagement"];
      this.canViewReportingAndAnalytics = data["canViewReportingAndAnalytics"];
      this.canViewDocumentManagement = data["canViewDocumentManagement"];
      this.canViewDispatch = data["canViewDispatch"];
      this.canViewQualityManagement = data["canViewQualityManagement"];
      this.canViewWIPTracking = data["canViewWIPTracking"];
      this.canViewDocumentManagement = data["canViewDocumentManagement"];
      this.canViewSuppliers = data["canViewSuppliers"];
      this.canViewProductionSupport = data["canViewProductionSupport"];
      this.canAccessUserTypes = data["canAccessUserTypes"];
    }
  }

  static fromJS(data: any): UserRoleDto {
    let result = new UserRoleDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["userRoleId"] = this.userRoleId;
    data["userRoleName"] = this.userRoleName;
    data["canAccessUserMaintenance"] = this.canAccessUserMaintenance;
    data["canViewSysAdmin"] = this.canViewSysAdmin;
    data["canViewProjects"] = this.canViewProjects;
    data["canViewEquipmentMaintenance"] = this.canViewEquipmentMaintenance;
    data["canViewProcessLog"] = this.canViewProcessLog;
    data["canViewToolingManagement"] = this.canViewToolingManagement;
    data["canViewReportingAndAnalytics"] = this.canViewReportingAndAnalytics;
    data["canViewDocumentManagement"] = this.canViewDocumentManagement;
    data["canViewDispatch"] = this.canViewDispatch;
    data["canViewQualityManagement"] = this.canViewQualityManagement;
    data["canViewWIPTracking"] = this.canViewWIPTracking;
    data["canViewDocumentManagement"] = this.canViewDocumentManagement;
    data["canViewSuppliers"] = this.canViewSuppliers;
    data["canViewProductionSupport"] = this.canViewProductionSupport;
    data["canAccessUserTypes"] = this.canAccessUserTypes;

    return data;
  }
}

export interface ICreateUserRoleDto {
  userRoleId: number;
  userRoleName: string;
  canAccessUserMaintenance: boolean;
  canViewSysAdmin: boolean;
  canViewProjects: boolean;
  canViewEquipmentMaintenance: boolean;
  canViewProcessLog: boolean;
  canViewToolingManagement: boolean;
  canViewReportingAndAnalytics: boolean;
  canViewDocumentManagement: boolean;
  canViewDispatch: boolean;
  canViewQualityManagement: boolean;
  canViewWIPTracking: boolean;
  canViewSuppliers: boolean;
  canViewProductionSupport: boolean;
  canAccessUserTypes: boolean;
}

export class PagedResultDtoOfUserRoleDto implements IPagedResultDtoOfUserRoleDto {
  results: UserRoleDto[];
  count: number;

  constructor(data?: IPagedResultDtoOfUserRoleDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data["results"] && data["results"].constructor === Array) {
        this.results = [];
        for (let item of data["results"])
          this.results.push(UserRoleDto.fromJS(item));
      }
         this.count = data["count"];
    }
  }

  static fromJS(data: any): PagedResultDtoOfUserRoleDto {
    let result = new PagedResultDtoOfUserRoleDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data["results"] = [];
      for (let item of this.results)
        data["results"].push(item.toJSON());
    }
    if (this.count) {
      data["count"] = this.count;
    }
    return data;
  }
}

export interface IPagedResultDtoOfUserRoleDto {
  results: UserRoleDto[];
  count: number;
}
