import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FocusModule} from 'angular2-focus';

import {ButtonsModule} from '@progress/kendo-angular-buttons';
import {DialogModule} from '@progress/kendo-angular-dialog';
import {GridModule} from '@progress/kendo-angular-grid';
import {InputsModule} from '@progress/kendo-angular-inputs';
import {DateInputsModule} from '@progress/kendo-angular-dateinputs';
import {DropDownsModule} from '@progress/kendo-angular-dropdowns';
import {TelerikReportingModule} from '@progress/telerik-angular-report-viewer';
import {ShowHidePasswordModule} from 'ngx-show-hide-password';
import {routing} from './toolingmanagement.routes';

import {SharedModule} from '../shared/index';
import {ToolingManagementComponent} from './toolingmanagement.component';
import {RequisitionFormComponent} from './requisition/requisition-form.component';
import {RequisitionComponent} from './requisition/requisition.component';
import {TabStripModule} from '@progress/kendo-angular-layout';
import {ToolSiteComponent} from '../toolingmanagement/shared/site/site.component';
import {RequisitionEditFormComponent} from './requisition/requisitionAdd-form.component';
import {RequisitionReasonComponent} from './requisitionReason/requisitionReason.component';
import {RequesitionReasonFormComponent} from './requisitionReason/requisitionReason-form.component';
import {RoleComponent} from './role/role.component';
import {RoleFormComponent} from './role/role-form.component';
import {ToolusersettingsComponent} from './toolusersettings/toolusersettings.component';
import {ToolusersettingsFormComponent} from './toolusersettings/toolusersettings-form.component';
import {RequisitionMaterialComponent} from './requisitionMaterial/requisitionMaterial.component';
import {RequesitionMatrialFormComponent} from './requisitionMaterial/requisitionMaterial-form.component';
import {RequisitionStatusComponent} from './requisitionStatus/requisitionStatus.component';
import {RequesitionStatusFormComponent} from './requisitionStatus/requisitionStatus-form.component';

import {RequisitionService} from './requisition/service/requisition.service';
import {RequisitionDetailService} from './requisition/service/requisitionDetail.service';
import {RequisitionToolBomService} from './requisition/service/requisitionToolBOM.service';
import {RequisitionDetailCategoryService} from './requisition/service/requisitionDetailCategory.service';
import {ToolUserSettingsService} from './toolusersettings/service/toolusersettings.service';
import {GeneralToolService} from './requisition/service/generalTool.service';
import {RequisitionReasonService} from './requisitionReason/service/requisitionReason.service';
import {ToolRoleService} from './role/service/role.service';
import {RequisitionMaterialService} from './requisitionMaterial/service/requisitionMaterial.service';
import {RequisitionStatusService} from './requisitionStatus/service/requisitionStatus.service';
import {ToolSharedService} from './shared/service/toolShared.service';
import {RequisitionWorkComponent} from './requisitionWork/requisition-work.component';
import {RequisitionWorkService} from './requisitionWork/service/requisition-work.service';
import {UpdatePrintComponent} from './updatePrints/update-print.component';
import {UpdatePrintService} from './updatePrints/service/update-print.service';
import {AutofocusDirective} from '../autofocus.directive'

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    ButtonsModule,
    GridModule,
    InputsModule,
    DialogModule,
    DateInputsModule,
    DropDownsModule,
    CommonModule,
    SharedModule,
    routing,
    TelerikReportingModule,
    ShowHidePasswordModule.forRoot(),
    TabStripModule,
    FocusModule.forRoot()
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  declarations: [
    ToolingManagementComponent,
    RequisitionComponent,
    RequisitionFormComponent,
    ToolSiteComponent,
    RequisitionEditFormComponent,
    RoleComponent,
    RoleFormComponent,
    ToolusersettingsComponent,
    ToolusersettingsFormComponent,
    RequisitionMaterialComponent,
    RequesitionMatrialFormComponent,
    RequisitionStatusComponent,
    RequesitionStatusFormComponent,
    RequisitionReasonComponent,
    RequesitionReasonFormComponent,
    RequisitionWorkComponent,
    UpdatePrintComponent,
    AutofocusDirective,

  ],
  providers: [
    RequisitionService,
    RequisitionDetailService,
    RequisitionToolBomService,
    RequisitionDetailCategoryService,
    ToolUserSettingsService,
    RequisitionReasonService,
    GeneralToolService,
    ToolRoleService,
    RequisitionMaterialService,
    RequisitionStatusService,
    ToolSharedService,
    RequisitionWorkService,
    UpdatePrintService
  ]
})
export class ToolingManagementModule {
}
