import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import "rxjs/add/observable/fromEvent";
import { CommonService } from "../../shared/services/common.service";
import { ParamRequisitionReason } from "./service/requisitionReason.model";
import { RequisitionReasonService } from "./service/requisitionReason.service";
import { ToasterHelperService } from "./../../shared/services/toasterHelper.service";
import { SelectListDto } from "../../shared/models/selectListDto";

@Component({
  selector: 'kendo-grid-requisitionReason-form',
  templateUrl: './html/requisitionReason-form.component.html'
})
export class RequesitionReasonFormComponent {
  public active = false;
  private isNew:boolean = true;
  private userSettingformFlag :boolean ;

  public currentSiteIdList: SelectListDto[];
  public filtercurrentSiteIdList: SelectListDto[];

  public reasonForm: FormGroup = new FormGroup({
    'requisitionReasonId': new FormControl(),
    'requisitionReasonName': new FormControl('', Validators.required),
    'description': new FormControl(''),
    'sortOrder': new FormControl(''),
    'siteId': new FormControl('', Validators.required),
    'active': new FormControl(false),
  });

  @Input() public set model(toolReason: any) {
    if (toolReason !== undefined) {
      this.reasonForm.reset(toolReason);
      this.isNew = false;
      this.isRequisitionformOpen = true;
    } else {
      this.isNew = true;
      this.reasonForm.reset();
    }
  }

  constructor(private commonService: CommonService,
    private requisitionReasonService: RequisitionReasonService,
    private toasterHelperService: ToasterHelperService) {
    this.userSettingformFlag = true;
  }

  @Input() public isRequisitionformOpen = false;

  @Output() cancel: EventEmitter<any> = new EventEmitter();
 
  ngOnInit() {

    this.commonService.getAll("General", "Site").subscribe(eventResult => {
      this.filtercurrentSiteIdList = this.currentSiteIdList = eventResult.results;
    });
  }

  private closeForm(): void {
    this.isRequisitionformOpen = false;
    this.cancel.emit();
  }


  private onCancel(event) {
    this.closeForm();
  }

  private onSave(): void {
    if (this.reasonForm.valid) {
      
      let paramReason: ParamRequisitionReason = this.reasonForm.value;
      if (this.isNew) {
        this.requisitionReasonService.create(paramReason,
          success => {
            this.toasterHelperService.success("Created", "Reason created sucessfully");
            this.reasonForm.reset();
            this.closeForm();
          },
          error => {

          });
      } else {
        this.requisitionReasonService.update(paramReason,
          success => {
            this.toasterHelperService.success("Updated", "Reason updated sucessfully");
            this.reasonForm.reset();
            this.closeForm();
          },
          error => {

          });
      } 

    }
  }
  private currentSiteIdListHandleFilter(value) {
    this.filtercurrentSiteIdList = this.currentSiteIdList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }


}
