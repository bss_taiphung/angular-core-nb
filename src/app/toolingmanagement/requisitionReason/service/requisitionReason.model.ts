export interface IParamRequisitionReason {
  requisitionReasonId: number;
  requisitionReasonName: string;
  description: string;
  siteId: number;
  sortOrder: number;
}

export class ParamRequisitionReason implements IParamRequisitionReason {
  requisitionReasonId: number;
  requisitionReasonName: string;
  description: string;
  siteId: number;
  sortOrder: number;
  constructor(data?: IParamRequisitionReason) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.requisitionReasonId = data["requisitionReasonId"];
      this.requisitionReasonName = data["requisitionReasonName"];
      this.description = data["description"];
      this.siteId = data["siteId"];
      this.sortOrder = data["sortOrder"];
    }
  }

  static fromJS(data: any): ParamRequisitionReason {
    let result = new ParamRequisitionReason();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["requisitionReasonId"] = this.requisitionReasonId;
    data["requisitionReasonName"] = this.requisitionReasonName;
    data["description"] = this.description;
    data["siteId"] = this.siteId;
    data["sortOrder"] = this.sortOrder;
    return data;
  }
}

export interface IRequisitionReasonDto {
  requisitionReasonId: number;
  requisitionReasonName: string;
  description: string;
  siteId: number;
  sortOrder: number;
  createdByName: string;
  modifiedByName: string;
  active: boolean;
  siteName: string;
}

export class RequisitionReasonDto implements IRequisitionReasonDto {
  requisitionReasonId: number;
  requisitionReasonName: string;
  description: string;
  siteId: number;
  sortOrder: number;
  createdByName: string;
  modifiedByName: string;
  active: boolean;
  siteName: string;
  constructor(data?: IRequisitionReasonDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (data) {
        this.requisitionReasonId = data["requisitionReasonId"];
        this.requisitionReasonName = data["requisitionReasonName"];
        this.description = data["description"];
        this.siteId = data["siteId"];
        this.sortOrder = data["sortOrder"];
        this.createdByName = data["createdByName"];
        this.modifiedByName = data["modifiedByName"];
        this.active = data["active"];
        this.siteName = data["siteName"];
      }
    }
  }

  static fromJS(data: any): RequisitionReasonDto {
    let result = new RequisitionReasonDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["requisitionReasonId"] = this.requisitionReasonId;
    data["requisitionReasonName"] = this.requisitionReasonName;
    data["description"] = this.description;
    data["siteId"] = this.siteId;
    data["sortOrder"] = this.sortOrder;
    data["createdByName"] = this.createdByName;
    data["modifiedByName"] = this.modifiedByName;
    data["active"] = this.active;
    data["siteName"] = this.siteName;
    return data;
  }
}

/*Reason paged model*/
export class PagedRequisitionReasonDto implements IPagedRequisitionReasonDto {
  results: RequisitionReasonDto[];
  count: number;

  constructor(data?: IPagedRequisitionReasonDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data["results"] && data["results"].constructor === Array) {
        this.results = [];
        for (let item of data["results"])
          this.results.push(RequisitionReasonDto.fromJS(item));
      }
      this.count = data["count"];
    }
  }

  static fromJS(data: any): PagedRequisitionReasonDto {
    let result = new PagedRequisitionReasonDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data["results"] = [];
      for (let item of this.results)
        data["results"].push(item.toJSON());
    }
    if (this.count) {
      data["count"] = this.count;
    }
    return data;
  }
}

export interface IPagedRequisitionReasonDto {
  results: RequisitionReasonDto[];
  count: number;
}


