import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import 'rxjs/add/observable/fromEvent';

import {HubConnection} from '@aspnet/signalr-client';
import {SortDescriptor, State} from '@progress/kendo-data-query';
import {DataStateChangeEvent, GridDataResult, PageChangeEvent} from '@progress/kendo-angular-grid';

import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {RequisitionReasonService} from './service/requisitionReason.service';
import {ParamRequisitionReason, RequisitionReasonDto} from './service/requisitionReason.model';
import {ToolSharedService} from '../shared/service/toolShared.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'Requisition-Reason',
  templateUrl: './html/requisitionReason.component.html',
  styleUrls: ['./css/requisitionReason.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class RequisitionReasonComponent implements OnInit {

  private hubConnection: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  loading = false;
  gridLoading = false;

  mss: any;

  isNew: boolean;
  pageSize = 10;
  skip = 0;
  state: State = {
    skip: 0,
    take: 5,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };
  gridView: GridDataResult;
  isRequisitionformOpen = false;
  sort: SortDescriptor[] = [];
  isactive = true;
  searchtext = '';
  isOpenedDeletedConformation = false;
  editReason: ParamRequisitionReason;
  deleteReason: ParamRequisitionReason;
  ActionData: Array<any> = [{
    text: 'Delete',
    icon: 'trash'
  }];

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private requisitionReasonService: RequisitionReasonService,
              private toolSharedService: ToolSharedService) {
    this.mss = this.commonService.getCookies('Mss');
    this.loading = true;

  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
    this.loadReasonGrid();
  }

  ngOnInit() {

    this.loading = true;
  }

  signalRConnection() {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');

        this.hubConnection
          .invoke('Subscribe',
            'ToolingManagement',
            'RequisitionReason',
            'SiteId',
            this.toolSharedService.getUserSettings().currentSiteId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated',
      (groupName: string, operation: string, messageJson: string) => {
        const _responseText = messageJson;
        let result200: any = null;
        const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
        const resultData200 = this.commonService.toCamel(data);
        result200 = resultData200 ? RequisitionReasonDto.fromJS(resultData200) : new RequisitionReasonDto();

        const dataExist = this.containsObject(result200, this.gridView.data);

        if (operation.toLowerCase() === 'insert' && !dataExist) {
          this.gridView.data.unshift(result200);
        }

        if (operation.toLowerCase() === 'update') {
          this.gridView.data.forEach((element, index) => {
            if (element.requisitionReasonId === result200.requisitionReasonId) {
              if (this.isactive) {
                if (result200.active === this.isactive) {
                  this.gridView.data[index] = result200;
                } else {
                  operation = 'delete';
                }
              } else {
                this.gridView.data[index] = result200;
              }
            }
          });
        }

        if (operation.toLowerCase() === 'delete' && dataExist) {
          let index = null;
          this.gridView.data.forEach((element, i) => {
            if (element.requisitionReasonId === result200.requisitionReasonId) {
              index = i;
            }
          });
          if (index !== null) {
            this.gridView.data.splice(index, 1);
          }
        }
      });
  }

  containsObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].requisitionReasonId === obj.requisitionReasonId) {
        return true;
      }
    }

    return false;
  }

  loadReasonGrid() {

    this.gridLoading = true;
    this.requisitionReasonService.getAll(this.pageSize, this.skip, this.sort, this.isactive, this.searchtext).subscribe(eventResult => {

      this.gridView = {
        data: eventResult.results,
        total: eventResult.count
      };
      this.loading = false;
      this.gridLoading = false;
    });
  }

  public showActive() {
    this.isactive = !this.isactive;
    this.gridLoading = true;
    this.loadReasonGrid();
  }

  dataStateChange(state: DataStateChangeEvent): void {
    this.searchtext = this.commonService.getFilter(state.filter.filters, this.isactive);
    this.loadReasonGrid();

  }

  sortChange(e) {
    console.log(e);
  }

  saveHandler(e) {
    console.log(e);
  }

  removeHandler(e) {
    console.log(e);
  }

  public onAction(e, dataItem: ParamRequisitionReason) {

    if (e === undefined) {
      this.editReason = dataItem;
      // this.isRequisitionformOpen = true;
    } else {
      if (e.text === 'Delete') {
        this.isOpenedDeletedConformation = true;
        this.deleteReason = dataItem;
      }
    }
  }

  onCloseDeleteConfomation(isConform: boolean = false) {
    if (isConform) {
      this.requisitionReasonService.delete(this.deleteReason.requisitionReasonId,
        sucess => {
          this.toasterService.success('Deleted', this.deleteReason.requisitionReasonName + ' is deleted sucessfully!');
          this.isOpenedDeletedConformation = false;
          this.deleteReason = undefined;
        }, error => {
          this.toasterService.error('Deleted', 'error while deleteing ' + this.deleteReason.requisitionReasonName);
          this.isOpenedDeletedConformation = false;
          this.deleteReason = undefined;
        });
    } else {
      this.isOpenedDeletedConformation = false;
    }
  }

  onAddRequisitionFormOpen() {

    this.isRequisitionformOpen = true;

  }

  onRequisitionFormCancel() {
    this.editReason = undefined;
    this.isRequisitionformOpen = false;
  }

  public siteSelectionChange(siteId): void {
    this.gridLoading = true;
    this.loadReasonGrid();
    this.signalRConnection();
  }
}
