import {Component, HostListener, OnInit, ViewEncapsulation} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {Observable} from 'rxjs/Observable';
import {FormGroup} from '@angular/forms';

import {SortDescriptor, State} from '@progress/kendo-data-query';
import {DataStateChangeEvent, GridDataResult, PageChangeEvent} from '@progress/kendo-angular-grid';
import {HubConnection} from '@aspnet/signalr-client';

import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {ToolSharedService} from '../shared/service/toolShared.service';
import {SelectListDto} from '../../shared/models/selectListDto';

import {ToolUserSettingsService} from '../toolusersettings/service/toolusersettings.service';
import {IUpdatePrintDto} from './service/update-print.model';
import {RequisitionDetailDto} from '../requisition/service/requisition.model';
import {UpdatePrintService} from './service/update-print.service';
import {RequisitionDetailService} from '../requisition/service/requisitionDetail.service';
import {RequisitionDetailCategoryService} from '../requisition/service/requisitionDetailCategory.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'update-print',
  templateUrl: './html/update-print.component.html',
  styleUrls: ['./css/update-print.component.css'],
  encapsulation: ViewEncapsulation.None
})


export class UpdatePrintComponent implements OnInit {

  data: IUpdatePrintDto[];
  public formGroup: FormGroup;
  public view: Observable<GridDataResult>;
  public editDataItem: RequisitionDetailDto;
  public isNew: boolean;
  public openRequisitionWorkDialog: boolean;
  public pageSize = 10;
  public showIncompleteOnly = true;
  public skip = 0;
  public state: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };
  public gridView: GridDataResult;
  public deleteDataItem: any;
  public isactive = true;
  public exclusionLoadingYes = false;
  public exclusionLoadingNo = false;
  public searchtext = '';
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  public userSetting: any;
  public siteList: SelectListDto[];
  public openUpdateDialog = false;
  private curentSiteId = 0;
  private exteraFilter = 'all';
  public mss: any;

  private selectedCategory = 0;
  private toolUserSetting: any;
  private requCategory: any[] = [];
  sort: SortDescriptor[] = [];
  private hubConnectionUpdatePrint: HubConnection;
  isOrderShownByMe = false;
  private editedRowIndex: number;

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private updatePrintService: UpdatePrintService,
              private requisitionDetailService: RequisitionDetailService,
              private toolUserSettingsService: ToolUserSettingsService,
              private toolSharedService: ToolSharedService,
              private requisitionDetailCategoryService: RequisitionDetailCategoryService) {
    this.userSetting = this.toolSharedService.getUserSettings();
  }

  ngOnInit() {
    this.exteraFilter = 'all';

  }

  signalRConnection() {
    /*Signal R*/
    this.hubConnectionUpdatePrint =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnectionUpdatePrint
      .start()
      .then(() => {
        console.log('Connection toolting RequisitionSiteCount started!' + 'ToolingManagement' + 'RequisitionDetail' + 'SiteId' + this.curentSiteId);

        this.hubConnectionUpdatePrint
          .invoke('Subscribe', 'ToolingManagement', 'RequisitionDetail', 'SiteId', this.curentSiteId.toString())
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));

    this.hubConnectionUpdatePrint.on('clientOnEntityUpdated',
      (groupName: string, operation: string, messageJson: string) => {
        debugger;
        const _responseText = messageJson;
        let result200: any = null;
        const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = this.commonService.toCamel(data);

        // result200 = resultData200 ? RequisitionDetailDto.fromJS(resultData200) : new RequisitionDetailDto();
        let dataExist = this.containsObject(result200, this.gridView.data);

        if (operation.toLowerCase() === 'bulkinsert') {
          result200.forEach(itm => {
            dataExist = this.containsObject(itm, this.gridView.data);
            if (!dataExist && itm.updatePrint === true && result200.updatePrintCompleted === !this.showIncompleteOnly) {
              this.gridView.data.unshift(itm);
            }
          });
        } else if (operation.toLowerCase() === 'insert' && !dataExist) {
          this.gridView.data.unshift(result200);
        }

        if (operation.toLowerCase() === 'update') {
          let elementExist = false;
          this.gridView.data.forEach((element, index) => {
            if (element.requisitionDetailId === result200.requisitionDetailId) {
              //
              if (this.isactive) {
                if (result200.active === this.isactive) {
                  if (result200.updatePrint === true && result200.updatePrintCompleted === !this.showIncompleteOnly) {
                    this.gridView.data.unshift(result200);
                  } else {
                    if (index !== null) {
                      this.gridView.data.splice(index, 1);
                    }
                  }
                  this.gridView.data[index] = result200;
                } else {
                  operation = 'delete';
                }
              } else {
                this.gridView.data[index] = result200;
              }
              elementExist = true;
            }
          });
          if (!elementExist) {
            if (result200.updatePrint === true && result200.updatePrintCompleted === !this.showIncompleteOnly) {
              this.gridView.data.unshift(result200);
            }
          }


        }

        if (operation.toLowerCase() === 'delete' && dataExist) {
          let index = null;
          this.gridView.data.forEach((element, i) => {
            if (element.requisitionDetailId === result200.requisitionDetailId) {
              index = i;
            }
          });
          if (index !== null) {
            this.gridView.data.splice(index, 1);
          }
        }
      });
  }

  public siteSelectionChange(data): void {
    this.curentSiteId = Number(data);
    this.toolUserSetting = this.toolSharedService.getUserSettings();
    this.curentSiteId = this.toolUserSetting.currentSiteId;
    this.fillCategories(true);
    this.signalRConnection();
  }

  onGridDataLoad(type) {
    this.selectedCategory = 0;
    this.exteraFilter = type;
    this.gridLoading = true;
    const updatePrintRequested = 'UpdatePrint eq true';
    this.requisitionDetailService.getAll(
      this.pageSize,
      this.skip,
      this.sort,
      this.isactive,
      this.searchtext,
      this.exteraFilter,
      this.isOrderShownByMe,
      this.selectedCategory,
      updatePrintRequested,
      this.showIncompleteOnly)
      .subscribe(eventResult => {
        this.gridView = {
          data: eventResult.results,
          total: eventResult.count
        };
        this.loading = false;
        this.gridLoading = false;
      });

  }

  containsObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].requisitionDetailId === obj.requisitionDetailId) {
        return true;
      }
    }

    return false;
  }

  // Dialog box
  opened: boolean = false;
  loading: boolean = true;
  gridLoading: boolean = true;

  fillCategories(isGridDataLoad: boolean = false): void {
    this.requisitionDetailCategoryService.getDataBySiteId(this.curentSiteId, true,
      sucess => {

        sucess.unshift(new Object({requisitionDetailCategoryName: 'All', requisitionDetailCategoryId: 0}));
        if (this.toolUserSetting.showRequistionCategory) {
          this.selectedCategory = this.toolUserSetting.showRequistionCategory;
        } else {
          this.selectedCategory = 0;
        }

        this.requCategory = sucess;
        if (isGridDataLoad) {
          this.onGridDataLoad(this.exteraFilter);
        }

      }, error => {

      });
  }

  public closeDialog() {
    this.openUpdateDialog = false;
  }

  public close(status) {
    const $this = this;
    if (status) {
      this.exclusionLoadingYes = true;
      this.editDataItem.updatePrintCompleted = true;
      this.editDataItem.updatePrintCompletedByUserId = this.userSetting.userId;
    } else {
      this.exclusionLoadingNo = true;
      this.editDataItem.updatePrintCompleted = false;
      this.editDataItem.updatePrintCompletedByUserId = null;
    }
    this.updatePrintService.update(this.editDataItem,
      (result) => {
        this.toasterService.success('', 'Operation has done successfully');
        this.exclusionLoadingYes = false;
        this.exclusionLoadingNo = false;
        this.openUpdateDialog = false;
      },
      (error) => {
        this.toasterService.errorMessage(error);
        this.exclusionLoadingYes = false;
        this.exclusionLoadingNo = false;
      })
    $this.deleteDataItem = null;
    $this.opened = false;
  }

  public open() {
    this.opened = true;
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.onGridDataLoad(this.exteraFilter);
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
  }

  public addHandler({sender}) {

    /* if (!this.eqmSettings.canEditMasterData) {
      this.toasterService.error('You do not have permission to add new manufacturer.', '');
      return false;
     } else {*/
    this.closeEditor(sender);
    this.editDataItem = new RequisitionDetailDto();
    this.isNew = true;

    /*}*/
  }

  public editHandler({dataItem}) {
    this.editDataItem = dataItem;
    this.isNew = false;
  }

  public saveHandler({dataItem}) {
    console.log(dataItem);
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public cancelHandler() {
    this.editDataItem = undefined;
    this.formGroup = undefined;
    this.onGridDataLoad(this.exteraFilter);
  }

  public removeHandler({dataItem}) {
    this.deleteDataItem = dataItem;
    this.open();
  }

  public onShowIncomplete() {
    this.showIncompleteOnly = !this.showIncompleteOnly;
    this.gridLoading = true;
    this.onGridDataLoad(this.exteraFilter);
  }

  public onShowOrderByMe() {
    this.isOrderShownByMe = !this.isOrderShownByMe;
    this.gridLoading = true;
    this.onGridDataLoad(this.exteraFilter);
  }

  public onSearchKeyup() {
    this.skip = 0;
    this.pageSize = 10;
    this.gridLoading = true;
    this.onGridDataLoad(this.exteraFilter);

  }

  public ActionData: Array<any> = [{
    text: 'Edit',
    icon: 'edit'
  }];

  public onAction(e, dataItem) {
    if (this.userSetting.isEngineer) {
      this.editDataItem = dataItem;
      this.openUpdateDialog = true;
    }
    else {
      this.toasterService.customErrorMessage('You don\'t permission to this feature');
    }

  }

  timeout = null;

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridLoading = true;

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.doSearch(state, this);
    }, 1000);
  }

  doSearch(state: any, that) {
    if (state.filter) {
      this.searchtext = that.commonService.getFilter(state.filter.filters, that.isactive);
      this.skip = state.skip;
      this.pageSize = state.take;
      that.onGridDataLoad(this.exteraFilter);
    }
    else {
      that.onGridDataLoad(this.exteraFilter);
    }
  }

  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {

    }
  }

  public siteHandleFilter(filter: any): void {
    this.siteList = this.siteList.filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  public updateCurrentSiteId() {
    this.gridLoading = true;

  }
}
