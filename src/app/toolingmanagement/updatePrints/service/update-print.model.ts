export class UpdatePrintDto implements IUpdatePrintDto {
  requisitionDetailId: number;
  requisitionId: number;
  station: string;
  detail: string;
  material: string;
  orderedByName: string;
  requisitionStatusName: number;
  isPrinted: boolean;
  updatePrintCompletedByName: string;
  updatePrintCompleted: string;
  updatePrintCompletedByUserId: string;
  active: boolean;
  toolNumber:string;

  constructor(data?: IUpdatePrintDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.requisitionDetailId = data["requisitionDetailId"];
      this.requisitionId = data["requisitionId"];
      this.station = data["station"];
      this.detail = data["detail"];
      this.material = data["material"];
      this.orderedByName = data["orderedByName"];
      this.requisitionStatusName = data["requisitionStatusName"];
      this.isPrinted = data["isPrinted"];
      this.updatePrintCompletedByName = data["updatePrintCompletedByName"];
      this.updatePrintCompleted = data["updatePrintCompleted"];
      this.updatePrintCompletedByUserId = data["updatePrintCompletedByUserId"];
      this.active = data["active"];
      this.toolNumber = data["toolNumber"];
    }
  }

  static fromJS(data: any): UpdatePrintDto {
    let result = new UpdatePrintDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data["requisitionDetailId"] = this.requisitionDetailId;
    data["requisitionId"] = this.requisitionId;
    data["station"] = this.station;
    data["detail"] = this.detail;
    data["material"] = this.material;
    data["isPrinted"] = this.isPrinted;
    data["orderedByName"] = this.orderedByName;
    data["requisitionStatusName"] = this.requisitionStatusName;
    data["updatePrintCompletedByName"] = this.updatePrintCompletedByName;
    data["updatePrintCompleted"] = this.updatePrintCompleted;
    data["updatePrintCompletedByUserId"] = this.updatePrintCompletedByUserId;
    data["toolNumber"] = this.toolNumber;
    data["active"] = this.active;

    return data;
  }
}

export interface IUpdatePrintDto {
  requisitionDetailId: number;
  requisitionId: number;
  station: string;
  detail: string;
  material: string;
  orderedByName: string;
  requisitionStatusName: number;
  isPrinted: boolean;
  updatePrintCompletedByName: string;
  updatePrintCompleted: string;
  updatePrintCompletedByUserId: string;
  active: boolean;
  toolNumber:string;
}

export class CreateUpdatePrintDto implements ICreateUpdatePrintDto {
  requisitionDetailId: number = 0;
  active: boolean = true;

  constructor(data?: IUpdatePrintDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.requisitionDetailId = 0;
      this.active = (data["active"] == null) ? false : data["active"];
    }
  }

  static fromJS(data: any): UpdatePrintDto {
    let result = new UpdatePrintDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["requisitionDetailId"] = 0;
    data["active"] = (this.active == null) ? false : this.active;
    return data;
  }
}

export interface ICreateUpdatePrintDto {
  active: boolean;
}

export class PagedResultDtoOfUpdatePrintDto implements IPagedResultDtoOfUpdatePrintDto {
  results: UpdatePrintDto[];
  count: number;

  constructor(data?: IPagedResultDtoOfUpdatePrintDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data["results"] && data["results"].constructor === Array) {
        this.results = [];
        for (let item of data["results"])
          this.results.push(UpdatePrintDto.fromJS(item));
      }
      this.count = data["count"];
    }
  }

  static fromJS(data: any): PagedResultDtoOfUpdatePrintDto {
    let result = new PagedResultDtoOfUpdatePrintDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data["results"] = [];
      for (let item of this.results)
        data["results"].push(item.toJSON());
    }
    if (this.count) {
      data["count"] = this.count;
    }
    return data;
  }
}

export interface IPagedResultDtoOfUpdatePrintDto {
  results: UpdatePrintDto[];
  count: number;
}


export interface IParamRequisition {
  requisitionId: number;
  siteId: number;
  toolId: number;
  orderedByUserId: number;
}

export class ParamRequisition implements IParamRequisition {
  requisitionId: number;
  siteId: number;
  toolId: number;
  orderedByUserId: number;

  constructor(data?: IParamRequisition) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (data) {
        this.requisitionId = data["requisitionId"];
        this.siteId = data["siteId"];
        this.toolId = data["toolId"];
        this.orderedByUserId = data["orderedByUserId"];
      }
    }
  }

  static fromJS(data: any): ParamRequisition {
    let result = new ParamRequisition();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["requisitionId"] = this.requisitionId;
    data["siteId"] = this.siteId;
    data["toolId"] = this.toolId;
    data["orderedByUserId"] = this.orderedByUserId;
    return data;
  }
}

export interface ITool {
  toolId: number;
  toolNumber: string;
  toolDescription: string;
  partId: number;
  projectId: number;
  siteId: number;
}

export class Tool implements ITool {
  toolId: number;
  toolNumber: string;
  toolDescription: string;
  partId: number;
  projectId: number;
  siteId: number;

  constructor(data?: IParamRequisition) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (data) {
        this.toolId = data["toolId"];
        this.toolNumber = data["toolNumber"];
        this.toolDescription = data["toolDescription"];
        this.partId = data["partId"];
        this.projectId = data["projectId"];
        this.siteId = data["siteId"];
      }
    }
  }

  static fromJS(data: any): Tool {
    let result = new Tool();
    result.init(data);
    return result;
  }

  static fromInitList(data?: any): any {
    let resultsList: Tool[]= [];
    if (data && data != null) {
      if ( data.constructor === Array) {
        for (let item of data)
          resultsList.push(Tool.fromJS(item));
      }
    }
    return resultsList;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["toolId"] = this.toolId;
    data["toolNumber"] = this.toolNumber;
    data["toolDescription"] = this.toolDescription;
    data["partId"] = this.partId;
    data["projectId"] = this.projectId;
    data["siteId"] = this.siteId;
    return data;
  }

}

export class ParamString {
  stringValue: string;
}
