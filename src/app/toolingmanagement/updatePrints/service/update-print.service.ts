import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, HttpModule } from '@angular/http';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

import { CommonService } from "../../../shared/services/common.service";
import { PagedResultDtoOfUpdatePrintDto } from './update-print.model';
import { ToolSharedService } from '../../shared/service/toolShared.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class UpdatePrintService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService, 
    private http: HttpClient,private toolSharedService: ToolSharedService) {
    this.apiBaseUrl = environment.bllApiBaseAddress + "/ToolingManagement/api/RequisitionDetail/";
  }
  getAll(take: number, skip: number, sort: any, isactive: boolean, searchtext: string,extraFilter: string = 'all',isOrderedbyMe:boolean = false , categoryId:number =0 ): Observable<PagedResultDtoOfUpdatePrintDto> {
   debugger;
    let url = this.apiBaseUrl + "GetPagedQuery";

    let orderBy = '';
    if (sort != undefined && sort.length > 0) {
      for (let item of sort)
        orderBy = this.commonService.firstUpper(item.field) + " " + (item.dir == undefined ? 'asc' : item.dir);
    }
    
    let filter = '';
    if (isactive) {
      filter = "Active eq true";
    }
    if (searchtext.length > 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter +=  searchtext ;
    }
    
    if (isOrderedbyMe) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += "OrderedByUserId eq " + this.toolSharedService.getUserSettings().userId + " ";
    }
    if (categoryId !==  0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += "RequisitionDetailCategoryId eq " + categoryId + " ";
    }
    if (extraFilter !== undefined && extraFilter !== null && extraFilter.toLowerCase() !== 'all') {
      
      switch (extraFilter.toLowerCase()) {
        case 'notprinted':
          filter += (filter.length > 0) ? ' and ' : '';
          filter += "isPrinted  eq false";
          filter += (filter.length > 0) ? ' and ' : '';
          filter += "Approved eq true";
          break;
        case 'pending':
          filter += (filter.length > 0) ? ' and ' : '';
          filter += "	Approved eq null";
          break;
        case 'approved':
          filter += (filter.length > 0) ? ' and ' : '';
          filter += "	Approved eq true";
          break;
        case 'rejected':
          filter += (filter.length > 0) ? ' and ' : '';
          filter += "	Approved eq false";
          break;
      }

    }

    const params = new HttpParams()
      .set('$top', take.toString())
      .set('$skip', skip.toString())
      .set('$count', 'true')
      .set('$orderby', orderBy.toString())
      .set('$filter', filter.toString());


    let options = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      }),
     params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      let result200 = PagedResultDtoOfUpdatePrintDto.fromJS(response);
      return Observable.of(result200);
    });
  }
  update(data:any,successCallBack,errorCallBack):any{
    let url=this.apiBaseUrl+"UpdateAsync";
   return this.commonService.httpPost(url,data,successCallBack,errorCallBack);
  }

}

