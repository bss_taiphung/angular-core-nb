export interface IParamRequisitionMaterial {
  requisitionMaterialId: number;
  material: string;
  materialHardness: string;
  siteId: number;
}

export class ParamRequisitionMaterial implements IParamRequisitionMaterial {
  requisitionMaterialId: number;
  material: string;
  materialHardness: string;
  siteId: number;
  constructor(data?: IParamRequisitionMaterial) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.requisitionMaterialId = data["requisitionMaterialId"];
      this.material = data["material"];
      this.materialHardness = data["materialHardness"];
      this.siteId = data["siteId"];
    }
  }

  static fromJS(data: any): ParamRequisitionMaterial {
    let result = new ParamRequisitionMaterial();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["requisitionMaterialId"] = this.requisitionMaterialId;
    data["material"] = this.material;
    data["materialHardness"] = this.materialHardness;
    data["siteId"] = this.siteId;
    return data;
  }
}

export interface IRequisitionMaterialDto {
   requisitionMaterialId :number;
material :string;
materialHardness :string;
siteId :string;
createdByName : string;
  modifiedByName: string;
  active: boolean;
  siteName :string;
}

export class RequisitionMaterialDto implements IRequisitionMaterialDto {
  requisitionMaterialId: number;
  material: string;
  materialHardness: string;
  siteId: string;
  createdByName: string;
  modifiedByName: string;
  active: boolean;
  siteName: string;
  constructor(data?: IRequisitionMaterialDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (data) {
        this.requisitionMaterialId = data["requisitionMaterialId"];
        this.material = data["material"];
        this.materialHardness = data["materialHardness"];
        this.siteId = data["siteId"];
        this.createdByName = data["createdByName"];
        this.modifiedByName = data["modifiedByName"];
        this.active = data["active"];
        this.siteName = data["siteName"];
      }
    }
  }

  static fromJS(data: any): RequisitionMaterialDto {
    let result = new RequisitionMaterialDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["requisitionMaterialId"] = this.requisitionMaterialId;
    data["material"] = this.material;
    data["materialHardness"] = this.materialHardness;
    data["siteId"] = this.siteId;
    data["createdByName"] = this.createdByName;
    data["modifiedByName"] = this.modifiedByName;
    data["active"] = this.active;
    data["siteName"] = this.siteName;
    return data;
  }
}

/*Material paged model*/
export class PagedRequisitionMaterialDto implements IPagedRequisitionMaterialDto {
  results: RequisitionMaterialDto[];
  count: number;

  constructor(data?: IPagedRequisitionMaterialDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data["results"] && data["results"].constructor === Array) {
        this.results = [];
        for (let item of data["results"])
          this.results.push(RequisitionMaterialDto.fromJS(item));
      }
      this.count = data["count"];
    }
  }

  static fromJS(data: any): PagedRequisitionMaterialDto {
    let result = new PagedRequisitionMaterialDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data["results"] = [];
      for (let item of this.results)
        data["results"].push(item.toJSON());
    }
    if (this.count) {
      data["count"] = this.count;
    }
    return data;
  }
}

export interface IPagedRequisitionMaterialDto {
  results: RequisitionMaterialDto[];
  count: number;
}


