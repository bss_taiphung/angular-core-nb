import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import "rxjs/add/observable/fromEvent";
import { CommonService } from "../../shared/services/common.service";
import { ParamRequisitionMaterial } from "./service/requisitionMaterial.model";
import { RequisitionMaterialService } from "./service/requisitionMaterial.service";
import { ToasterHelperService } from "./../../shared/services/toasterHelper.service";
import { SelectListDto } from "../../shared/models/selectListDto";

@Component({
  selector: 'kendo-grid-requesitionMatrial-form',
  templateUrl: './html/requisitionMaterial-form.component.html'
})
export class RequesitionMatrialFormComponent {
  public active = false;
  private isNew:boolean = true;
  private userSettingformFlag :boolean ;

  public currentSiteIdList: SelectListDto[];
  public filtercurrentSiteIdList: SelectListDto[];

  public materialForm: FormGroup = new FormGroup({
    'requisitionMaterialId': new FormControl(),
    'material': new FormControl('', Validators.required),
    'materialHardness': new FormControl(''),
    'siteId': new FormControl('', Validators.required),
    'active': new FormControl(false),
  });

  @Input() public set model(toolMaterial: any) {
    if (toolMaterial !== undefined) {
      this.materialForm.reset(toolMaterial);
      this.isNew = false;
      this.isRequisitionformOpen = true;
    } else {
      this.isNew = true;
      this.materialForm.reset();
    }
  }

  constructor(private commonService: CommonService,
    private requisitionMaterialService: RequisitionMaterialService,
    private toasterHelperService: ToasterHelperService) {
    this.userSettingformFlag = true;
  }

  @Input() public isRequisitionformOpen = false;

  @Output() cancel: EventEmitter<any> = new EventEmitter();
 
  ngOnInit() {

    this.commonService.getAll("General", "Site").subscribe(eventResult => {
      this.filtercurrentSiteIdList = this.currentSiteIdList = eventResult.results;
    });
  }

  private closeForm(): void {
    this.isRequisitionformOpen = false;
    this.cancel.emit();
  }


  private onCancel(event) {
    this.closeForm();
  }

  private onSave(): void {
    if (this.materialForm.valid) {
      
      let paramMaterial: ParamRequisitionMaterial = this.materialForm.value;
      if (this.isNew) {
        this.requisitionMaterialService.create(paramMaterial,
          success => {
            this.toasterHelperService.success("Created", "Material created sucessfully");
            this.materialForm.reset();
            this.closeForm();
          },
          error => {

          });
      } else {
        this.requisitionMaterialService.update(paramMaterial,
          success => {
            this.toasterHelperService.success("Updated", "Material updated sucessfully");
            this.materialForm.reset();
            this.closeForm();
          },
          error => {

          });
      } 

    }
  }
  private currentSiteIdListHandleFilter(value) {
    this.filtercurrentSiteIdList = this.currentSiteIdList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }


}
