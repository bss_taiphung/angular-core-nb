export interface IParamRole {

  roleId: number;
  roleName: number;
  canToggleSites: number;
  canApproveRequisitions: boolean;
  canLockRequisitions: boolean;
  receiveSupervisorNotifications: boolean;
  isEngineer: boolean;
  canEditRequisitions: boolean;

}

export class ParamRole implements IParamRole {
  roleId: number;
  roleName: number;
  canToggleSites: number;
  canApproveRequisitions: boolean;
  canLockRequisitions: boolean;
  receiveSupervisorNotifications: boolean;
  isEngineer: boolean;
  canEditRequisitions: boolean;
  active:boolean;
  constructor(data?: IParamRole) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {

      this.roleId = data["roleId"];
      this.roleName = data["roleName"];
      this.canToggleSites = data["canToggleSites"];
      this.canApproveRequisitions = data["canApproveRequisitions"];
      this.canLockRequisitions = data["canLockRequisitions"];
      this.receiveSupervisorNotifications = data["receiveSupervisorNotifications"];
      this.isEngineer = data["isEngineer"];
      this.canEditRequisitions = data["canEditRequisitions"];
      this.active = data["active"];
    }
  }

  static fromJS(data: any): ParamRole {
    let result = new ParamRole();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["roleId"] = this.roleId;
    data["roleName"] = this.roleName;
    data["canToggleSites"] = this.canToggleSites;
    data["canApproveRequisitions"] = this.canApproveRequisitions;
    data["canLockRequisitions"] = this.canLockRequisitions;
    data["receiveSupervisorNotifications"] = this.receiveSupervisorNotifications;
    data["isEngineer"] = this.isEngineer;
    data["canEditRequisitions"] = this.canEditRequisitions;
    data["active"] = this.active;
    return data;
  }
}

/*Role paged model*/
export class PagedResultDtoOfParamRole implements IPagedResultDtoOfParamRole{
  results: ParamRole[];
  count: number;

  constructor(data?: IPagedResultDtoOfParamRole) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data["results"] && data["results"].constructor === Array) {
        this.results = [];
        for (let item of data["results"])
          this.results.push(ParamRole.fromJS(item));
      }
      this.count = data["count"];
    }
  }

  static fromJS(data: any): PagedResultDtoOfParamRole {
    let result = new PagedResultDtoOfParamRole();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data["results"] = [];
      for (let item of this.results)
        data["results"].push(item.toJSON());
    }
    if (this.count) {
      data["count"] = this.count;
    }
    return data;
  }
}

export interface IPagedResultDtoOfParamRole {
  results: ParamRole[];
  count: number;
}


