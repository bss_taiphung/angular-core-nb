"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ParamRole = /** @class */ (function () {
    function ParamRole(data) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    this[property] = data[property];
            }
        }
    }
    ParamRole.prototype.init = function (data) {
        if (data) {
            this.roleId = data["roleId"];
            this.roleName = data["roleName"];
            this.canToggleSites = data["canToggleSites"];
            this.canApproveRequisitions = data["canApproveRequisitions"];
            this.canLockRequisitions = data["canLockRequisitions"];
            this.receiveSupervisorNotifications = data["receiveSupervisorNotifications"];
            this.isEngineer = data["isEngineer"];
            this.canEditRequisitions = data["canEditRequisitions"];
            this.active = data["active"];
        }
    };
    ParamRole.fromJS = function (data) {
        var result = new ParamRole();
        result.init(data);
        return result;
    };
    ParamRole.prototype.toJSON = function (data) {
        data = typeof data === 'object' ? data : {};
        data["roleId"] = this.roleId;
        data["roleName"] = this.roleName;
        data["canToggleSites"] = this.canToggleSites;
        data["canApproveRequisitions"] = this.canApproveRequisitions;
        data["canLockRequisitions"] = this.canLockRequisitions;
        data["receiveSupervisorNotifications"] = this.receiveSupervisorNotifications;
        data["isEngineer"] = this.isEngineer;
        data["canEditRequisitions"] = this.canEditRequisitions;
        data["active"] = this.active;
        return data;
    };
    return ParamRole;
}());
exports.ParamRole = ParamRole;
/*Role paged model*/
var PagedResultDtoOfParamRole = /** @class */ (function () {
    function PagedResultDtoOfParamRole(data) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    this[property] = data[property];
            }
        }
    }
    PagedResultDtoOfParamRole.prototype.init = function (data) {
        if (data && data != null) {
            if (data["results"] && data["results"].constructor === Array) {
                this.results = [];
                for (var _i = 0, _a = data["results"]; _i < _a.length; _i++) {
                    var item = _a[_i];
                    this.results.push(ParamRole.fromJS(item));
                }
            }
            this.count = data["count"];
        }
    };
    PagedResultDtoOfParamRole.fromJS = function (data) {
        var result = new PagedResultDtoOfParamRole();
        result.init(data);
        return result;
    };
    PagedResultDtoOfParamRole.prototype.toJSON = function (data) {
        data = typeof data === 'object' ? data : {};
        if (this.results && this.results.constructor === Array) {
            data["results"] = [];
            for (var _i = 0, _a = this.results; _i < _a.length; _i++) {
                var item = _a[_i];
                data["results"].push(item.toJSON());
            }
        }
        if (this.count) {
            data["count"] = this.count;
        }
        return data;
    };
    return PagedResultDtoOfParamRole;
}());
exports.PagedResultDtoOfParamRole = PagedResultDtoOfParamRole;
//# sourceMappingURL=role.model.js.map