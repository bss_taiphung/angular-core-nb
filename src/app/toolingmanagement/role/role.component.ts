import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {SortDescriptor, State} from '@progress/kendo-data-query';
import {DataStateChangeEvent, GridDataResult} from '@progress/kendo-angular-grid';
import {HubConnection} from '@aspnet/signalr-client';
import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {ToolRoleService} from './service/role.service';
import {ParamRole} from './service/role.model';

import {SelectListDto} from '../../shared/models/selectListDto'


@Component({
    selector: 'Requisition-role',
    templateUrl: './html/role.component.html',
    styleUrls: ['./css/role.component.css'],
    encapsulation: ViewEncapsulation.None
})

export class RoleComponent implements OnInit {

    private hubConnection: HubConnection;
    protected jsonParseReviver: (key: string, value: any) => any = undefined;
    loading = false;
    gridLoading = false;
    siteList: SelectListDto[];
    mss: any;

    isNew: boolean;
    pageSize = 10;
    skip = 0;
    state: State = {
        skip: 0,
        take: 5,

        // Initial filter descriptor
        filter: {
            logic: 'and',
            filters: [{field: '', operator: '', value: ''}]
        }
    };
    gridView: GridDataResult;
    isRequisitionformOpen = false;
    sort: SortDescriptor[] = [];
    isactive = true;
    searchtext = '';
    isOpenedDeletedConformation = false;
    editRole: ParamRole;
    deleteRole: ParamRole;
    ActionData: Array<any> = [{
        text: 'Delete',
        icon: 'trash'
    }];


    constructor(private toasterService: ToasterHelperService,
                private commonService: CommonService,
                private toolRoleService: ToolRoleService) {
        this.mss = this.commonService.getCookies('Mss');
        this.loading = true;

    }

    ngOnInit() {
        this.loading = true;
        this.loadRolesGrid();
    }

    loadRolesGrid() {

        this.gridLoading = false;
        this.toolRoleService.getAll(this.pageSize, this.skip, this.sort, this.isactive, this.searchtext).subscribe(eventResult => {

            this.gridView = {
                data: eventResult.results,
                total: eventResult.count
            };
            this.loading = false;
            this.gridLoading = false;
        });
    }

    dataStateChange(state: DataStateChangeEvent): void {
        this.searchtext = this.commonService.getFilter(state.filter.filters, this.isactive);
        this.loadRolesGrid();

    }

  sortChange(e) {
    console.log(e);
  }

  saveHandler(e) {
    console.log(e);
  }

  removeHandler(e) {
    console.log(e);
  }

    public onAction(e, dataItem: ParamRole) {
        if (e === undefined) {
            this.editRole = dataItem;
            //this.isRequisitionformOpen = true;
        } else {
            if (e.text === 'Delete') {
                this.isOpenedDeletedConformation = true;
                this.deleteRole = dataItem;
            }
        }
    }

    onCloseDeleteConfomation(isConform: boolean = false) {
        if (isConform) {
            this.toolRoleService.delete(this.deleteRole.roleId,
                sucess => {
                    this.toasterService.success('Deleted', this.deleteRole.roleName + ' is deleted sucessfully!');
                    this.isOpenedDeletedConformation = false;
                    this.deleteRole = undefined;
                }, error => {
                    this.toasterService.error('Deleted', 'error while deleteing ' + this.deleteRole.roleName);
                    this.isOpenedDeletedConformation = false;
                    this.deleteRole = undefined;
                });
        } else {
            this.isOpenedDeletedConformation = false;
        }
    }

    onAddRequisitionFormOpen() {

        this.isRequisitionformOpen = true;

    }

    onRequisitionFormCancel() {
        this.loadRolesGrid();
        this.isRequisitionformOpen = false;
        this.editRole = undefined;
    }


}
