import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import "rxjs/add/observable/fromEvent";
import { CommonService } from "../../shared/services/common.service";
import { ParamRole } from "../role/service/role.model";
import { ToolRoleService } from "./service/role.service";
import { ToasterHelperService } from "./../../shared/services/toasterHelper.service";
@Component({
  selector: 'kendo-grid-requesitionrole-form',
  templateUrl: './html/role-form.component.html'
})
export class RoleFormComponent {
  public active = false;
  private isNew:boolean = true;
  private userSettingformFlag :boolean ;

  public roleForm: FormGroup = new FormGroup({
    'roleId': new FormControl(),
    'roleName': new FormControl('', Validators.required),
    'canToggleSites': new FormControl(false),
    'canApproveRequisitions': new FormControl(false),
    'canLockRequisitions': new FormControl(false),
    'isEngineer': new FormControl(false),
    'canEditRequisitions': new FormControl(false),
    'active': new FormControl(false),
    'receiveSupervisorNotifications': new FormControl(false)
  });

  @Input() public set model(toolRole: any) {
    
    if (toolRole !== undefined) {
      this.roleForm.reset(toolRole);
      this.isNew = false;
      this.isRequisitionformOpen = true;
    } else {
      this.isNew = true;
    }
  }

  constructor(private commonService: CommonService,
    private toolRoleService: ToolRoleService,
    private toasterHelperService: ToasterHelperService) {
    this.userSettingformFlag = true;
  }

  @Input() public isRequisitionformOpen = false;

  @Output() cancel: EventEmitter<any> = new EventEmitter();
 
  
  private closeForm(): void {
    this.isRequisitionformOpen = false;
    this.cancel.emit();
  }


  private onCancel(event) {
    this.closeForm();
  }

  private onSave(): void {
    if (this.roleForm.valid) {
      let paramRole: ParamRole = this.roleForm.value;
      if (this.isNew) {
        paramRole.active = true;
        this.toolRoleService.create(paramRole,
          success => {
            this.toasterHelperService.success("Created", "Role created sucessfully");
            this.roleForm.reset();
            this.closeForm();
          },
          error => {

          });
      } else {
        this.toolRoleService.update(paramRole,
          success => {
            this.toasterHelperService.success("Updated", "Role updated sucessfully");
            this.roleForm.reset();
            this.closeForm();
          },
          error => {

          });
      } 

    }
    


  }

}
