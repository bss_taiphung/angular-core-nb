import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import "rxjs/add/observable/fromEvent";
import { CommonService } from "../../shared/services/common.service";
import { ParamRequisitionStatus} from "./service/requisitionStatus.model";
import { RequisitionStatusService } from "./service/requisitionStatus.service";
import { ToasterHelperService } from "./../../shared/services/toasterHelper.service";
import { SelectListDto } from "../../shared/models/selectListDto";

@Component({
  selector: 'kendo-grid-requisitionStatus-form',
  templateUrl: './html/requisitionStatus-form.component.html'
})
export class RequesitionStatusFormComponent {
  public active = false;
  private isNew:boolean = true;
  private userSettingformFlag :boolean ;

  public currentSiteIdList: SelectListDto[];
  public filtercurrentSiteIdList: SelectListDto[];

  public statusForm: FormGroup = new FormGroup({
    'requisitionStatusId': new FormControl(),
    'requisitionStatusName': new FormControl('', Validators.required),
    'siteId': new FormControl('', Validators.required),
    'canClose': new FormControl(false),
    'active': new FormControl(false),
  });

  @Input() public set model(toolMaterial: any) {
    if (toolMaterial !== undefined) {
      this.statusForm.reset(toolMaterial);
      this.isNew = false;
      this.isRequisitionformOpen = true;
    } else {
      this.isNew = true;
      this.statusForm.reset();
    }
  }

  constructor(private commonService: CommonService,
    private requisitionStatusService: RequisitionStatusService,
    private toasterHelperService: ToasterHelperService) {
    this.userSettingformFlag = true;
  }

  @Input() public isRequisitionformOpen = false;

  @Output() cancel: EventEmitter<any> = new EventEmitter();

 
  ngOnInit() {

    this.commonService.getAll("General", "Site").subscribe(eventResult => {
      this.filtercurrentSiteIdList = this.currentSiteIdList = eventResult.results;
    });
  }

  private closeForm(): void {
    this.isRequisitionformOpen = false;
    this.cancel.emit();
  }


  private onCancel(event) {
    this.closeForm();
  }

  private onSave(): void {
    if (this.statusForm.valid) {
      let paramMaterial: ParamRequisitionStatus = this.statusForm.value;
      if (this.isNew) {
        this.requisitionStatusService.create(paramMaterial,
          success => {
            this.toasterHelperService.success("Created", "Material created sucessfully");
            this.statusForm.reset();
            this.closeForm();
          },
          error => {

          });
      } else {
        this.requisitionStatusService.update(paramMaterial,
          success => {
            this.toasterHelperService.success("Updated", "Material updated sucessfully");
            this.statusForm.reset();
            this.closeForm();
          },
          error => {

          });
      } 

    }
  }
  private currentSiteIdListHandleFilter(value) {
    this.filtercurrentSiteIdList = this.currentSiteIdList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }


}
