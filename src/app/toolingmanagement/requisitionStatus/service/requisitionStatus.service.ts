import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, HttpModule } from '@angular/http';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import { CommonService } from "../../../shared/services/common.service";
import { ParamRequisitionStatus, PagedRequisitionStatusDto } from "./requisitionStatus.model";
import {environment} from '../../../../environments/environment';

@Injectable()
export class RequisitionStatusService {

  private apiBaseUrl;

  constructor(private commonService: CommonService, private http: HttpClient) {
    this.apiBaseUrl = environment.bllApiBaseAddress + "/ToolingManagement/api/RequisitionStatus/";
  }

  create(input: ParamRequisitionStatus, successCallback: any, errorCallback: any): any {
    let url = this.apiBaseUrl + "InsertAsync";
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  update(usersetting: ParamRequisitionStatus, successCallback: any, errorCallback: any) {
    let url = this.apiBaseUrl + "UpdateAsync";
    this.commonService.httpPost(url, usersetting, successCallback, errorCallback);
  }

  delete(id: any, successCallback: any, errorCallback: any): any {
    let url = this.apiBaseUrl + "DeleteAsync";

    let params = new HttpParams().set('id', id);

    let options = {
      params: params
    };

    this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
  }

  getAll(take: number, skip: number, sort: any, isactive: boolean, filter: string = ""): Observable<any>{
    let url = this.apiBaseUrl + "GetPagedQuery";
    //Order by Clause 
    let orderBy = '';
    if (sort != undefined && sort.length > 0) {
      for (let item of sort)
        orderBy = this.commonService.firstUpper(item.field) + " " + (item.dir == undefined ? 'asc' : item.dir);
    } else {
      orderBy = 'RequisitionStatusId desc';
    }

    if (isactive) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += " Active eq true";
    }

    const params = new HttpParams()
      .set('$top', take.toString())
      .set('$skip', skip.toString())
      .set('$count', 'true')
      .set('$orderby', orderBy.toString())
      .set('$filter', filter.toString());

    let options = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      }),
      params: params
    };
    return this.http.request<Response>('get', url, options).flatMap((response) => {
      let result200 = PagedRequisitionStatusDto.fromJS(response);
      return Observable.of(result200);
    });
  }
}

