export interface IParamRequisitionStatus {
  requisitionStatusId: number;
  requisitionStatusName: string;
  siteId: number;
  canClose:boolean;
}

export class ParamRequisitionStatus implements IParamRequisitionStatus {
  requisitionStatusId: number;
  requisitionStatusName: string;
  siteId: number;
  canClose: boolean;
  constructor(data?: IParamRequisitionStatus) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.requisitionStatusId = data["requisitionStatusId"];
      this.requisitionStatusName = data["requisitionStatusName"];
      this.canClose = data["canClose"];
      this.siteId = data["siteId"];
    }
  }

  static fromJS(data: any): ParamRequisitionStatus {
    let result = new ParamRequisitionStatus();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["requisitionStatusId"] = this.requisitionStatusId;
    data["requisitionStatusName"] = this.requisitionStatusName;
    data["canClose"] = this.canClose;
    data["siteId"] = this.siteId;
    return data;
  }
}

export interface IRequisitionStatusDto {
  requisitionStatusId: number;
  requisitionStatusName: string;
  siteId: number;
  canClose: boolean;
  siteName :string;
}

export class RequisitionStatusDto implements IRequisitionStatusDto {
  requisitionStatusId: number;
  requisitionStatusName: string;
  siteId: number;
  canClose: boolean;
  createdByName: string;
  modifiedByName: string;
  active: boolean;
  siteName: string;
  constructor(data?: IRequisitionStatusDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (data) {
        this.requisitionStatusId = data["requisitionStatusId"];
        this.requisitionStatusName = data["requisitionStatusName"];
        this.canClose = data["canClose"];
        this.siteId = data["siteId"];
        this.createdByName = data["createdByName"];
        this.modifiedByName = data["modifiedByName"];
        this.active = data["active"];
        this.siteName = data["siteName"];
      }
    }
  }

  static fromJS(data: any): RequisitionStatusDto {
    let result = new RequisitionStatusDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data = typeof data === 'object' ? data : {};
    data["requisitionStatusId"] = this.requisitionStatusId;
    data["requisitionStatusName"] = this.requisitionStatusName;
    data["canClose"] = this.canClose;
    data["siteId"] = this.siteId;
    data["createdByName"] = this.createdByName;
    data["modifiedByName"] = this.modifiedByName;
    data["active"] = this.active;
    data["siteName"] = this.siteName;
    return data;
  }
}

/*Status paged model*/
export class PagedRequisitionStatusDto implements IPagedRequisitionStatusDto {
  results: RequisitionStatusDto[];
  count: number;

  constructor(data?: IPagedRequisitionStatusDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data["results"] && data["results"].constructor === Array) {
        this.results = [];
        for (let item of data["results"])
          this.results.push(RequisitionStatusDto.fromJS(item));
      }
      this.count = data["count"];
    }
  }

  static fromJS(data: any): PagedRequisitionStatusDto {
    let result = new PagedRequisitionStatusDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data["results"] = [];
      for (let item of this.results)
        data["results"].push(item.toJSON());
    }
    if (this.count) {
      data["count"] = this.count;
    }
    return data;
  }
}

export interface IPagedRequisitionStatusDto {
  results: RequisitionStatusDto[];
  count: number;
}


