import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import 'rxjs/add/observable/fromEvent';

import {HubConnection} from '@aspnet/signalr-client';
import {SortDescriptor, State} from '@progress/kendo-data-query';
import {DataStateChangeEvent, GridDataResult, PageChangeEvent} from '@progress/kendo-angular-grid';

import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {RequisitionStatusService} from './service/requisitionStatus.service';
import {ParamRequisitionStatus, RequisitionStatusDto} from './service/requisitionStatus.model';
import {ToolSharedService} from '../shared/service/toolShared.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'Requisition-Status',
  templateUrl: './html/requisitionStatus.component.html',
  styleUrls: ['./css/requisitionStatus.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class RequisitionStatusComponent implements OnInit {

  private hubConnection: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  loading = false;
  gridLoading = false;

  mss: any;

  isNew: boolean;
  pageSize = 10;
  skip = 0;
  state: State = {
    skip: 0,
    take: 5,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };
  gridView: GridDataResult;
  isRequisitionformOpen = false;
  sort: SortDescriptor[] = [];
  isactive = true;
  searchtext = '';
  isOpenedDeletedConformation = false;
  editStatus: ParamRequisitionStatus;
  deleteStatus: ParamRequisitionStatus;
  ActionData: Array<any> = [{
    text: 'Delete',
    icon: 'trash'
  }];


  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private requisitionStatusService: RequisitionStatusService,
              private toolSharedService: ToolSharedService) {
    this.mss = this.commonService.getCookies('Mss');
    this.loading = true;

  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
    this.loadStatuslGrid();
  }

  ngOnInit() {

    this.loading = true;
  }

  signalRConnection() {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');

        this.hubConnection
          .invoke('Subscribe',
            'ToolingManagement',
            'RequisitionStatus',
            'SiteId',
            this.toolSharedService.getUserSettings().currentSiteId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated',
      (groupName: string, operation: string, messageJson: string) => {
        const _responseText = messageJson;
        let result200: any = null;
        const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
        const resultData200 = this.commonService.toCamel(data);
        result200 = resultData200 ? RequisitionStatusDto.fromJS(resultData200) : new RequisitionStatusDto();

        const dataExist = this.containsObject(result200, this.gridView.data);

        if (operation.toLowerCase() === 'insert' && !dataExist) {
          this.gridView.data.unshift(result200);
        }

        if (operation.toLowerCase() === 'update') {
          this.gridView.data.forEach((element, index) => {
            if (element.requisitionStatusId === result200.requisitionStatusId) {
              if (this.isactive) {
                if (result200.active === this.isactive) {
                  this.gridView.data[index] = result200;
                } else {
                  operation = 'delete';
                }
              } else {
                this.gridView.data[index] = result200;
              }
            }
          });
        }

        if (operation.toLowerCase() === 'delete' && dataExist) {
          let index = null;
          this.gridView.data.forEach((element, i) => {
            if (element.requisitionStatusId === result200.requisitionStatusId) {
              index = i;
            }
          });
          if (index !== null) {
            this.gridView.data.splice(index, 1);
          }
        }
      });
  }

  containsObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].requisitionStatusId === obj.requisitionStatusId) {
        return true;
      }
    }

    return false;
  }

  loadStatuslGrid() {

    this.gridLoading = true;
    this.requisitionStatusService.getAll(this.pageSize, this.skip, this.sort, this.isactive, this.searchtext).subscribe(eventResult => {
      this.gridView = {
        data: eventResult.results,
        total: eventResult.count
      };
      this.loading = false;
      this.gridLoading = false;
    });
  }

  public showActive() {
    this.isactive = !this.isactive;
    this.gridLoading = true;
    this.loadStatuslGrid();
  }

  dataStateChange(state: DataStateChangeEvent): void {
    this.searchtext = this.commonService.getFilter(state.filter.filters, this.isactive);
    this.loadStatuslGrid();

  }

  sortChange(e) {
    console.log(e);
  }

  saveHandler(e) {
    console.log(e);
  }

  removeHandler(e) {
    console.log(e);
  }

  public onAction(e, dataItem: ParamRequisitionStatus) {

    if (e === undefined) {
      this.editStatus = dataItem;
      // this.isRequisitionformOpen = true;
    } else {
      if (e.text === 'Delete') {
        this.isOpenedDeletedConformation = true;
        this.deleteStatus = dataItem;
      }
    }
  }

  onCloseDeleteConfomation(isConform: boolean = false) {
    if (isConform) {
      this.requisitionStatusService.delete(this.deleteStatus.requisitionStatusId,
        sucess => {
          this.toasterService.success('Deleted', this.deleteStatus.requisitionStatusName + ' is deleted sucessfully!');
          this.isOpenedDeletedConformation = false;
          this.deleteStatus = undefined;
          this.loadStatuslGrid();
        }, error => {
          this.toasterService.error('Deleted', 'error while deleteing ' + this.deleteStatus.requisitionStatusName);
          this.isOpenedDeletedConformation = false;
          this.deleteStatus = undefined;
        });
    } else {
      this.isOpenedDeletedConformation = false;
    }
  }

  onAddRequisitionFormOpen() {

    this.isRequisitionformOpen = true;

  }

  onRequisitionFormCancel() {
    this.editStatus = undefined;
    this.isRequisitionformOpen = false;

  }

  public siteSelectionChange(siteId): void {
    this.gridLoading = true;
    this.loadStatuslGrid();
    this.signalRConnection();
  }
}
