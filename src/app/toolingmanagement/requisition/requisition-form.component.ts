import {Component, EventEmitter, Input, Output, Renderer2, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';
import {DomSanitizer} from '@angular/platform-browser';

import {process, State} from '@progress/kendo-data-query';
import {DataStateChangeEvent, GridComponent, GridDataResult} from '@progress/kendo-angular-grid';

import {CommonService} from '../../shared/services/common.service';
import {ParamGenerateRequisitionAndDetail, RequisitionDetailDto, Tool} from './service/requisition.model';
import {GeneralToolService} from './service/generalTool.service';
import {RequisitionService} from './service/requisition.service';
import {RequisitionDetailService} from './service/requisitionDetail.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {ToolSharedService} from '../shared/service/toolShared.service';
import {RequisitionToolBomService} from './service/requisitionToolBOM.service';

const hasClass = (el, className) => new RegExp(className).test(el.className);

const isChildOf = (el, className) => {
  while (el && el.parentElement) {
    if (hasClass(el.parentElement, className)) {
      return true;
    }
    el = el.parentElement;
  }
  return false;
};

@Component({
  selector: 'kendo-grid-requisition-form',
  styles: [
    'input[type=text] { width: 100%; }'
  ],
  templateUrl: './html/requisition-form.component.html'
})
export class RequisitionFormComponent {

  public active = false;
  private toolList: any[] = [];
  private gridView: GridDataResult;
  @ViewChild(GridComponent) grid: GridComponent;
  private gridLoading = false;
  private userSetting: any;
  private requisitionDetailList: RequisitionDetailDto[] = [];
  public requisitionForm: FormGroup = new FormGroup({
    'requisitionId': new FormControl(),
    'siteId': new FormControl(''),
    'toolId': new FormControl('', Validators.required),
    'orderedByUserId': new FormControl('', Validators.required),
  });

  @Input()
  public isNew = false;

  @Input()
  public set model(requisitionDetailDto: RequisitionDetailDto) {
    this.requisitionForm.reset(requisitionDetailDto);
    this.active = requisitionDetailDto !== undefined;
    if (this.active) {
      this.userSetting = this.toolSharedService.getUserSettings();
      this.requisitionForm.controls['orderedByUserId'].setValue(this.userSetting.userId);
      this.requisitionForm.controls['siteId'].setValue(this.userSetting.currentSiteId);
      if (!this.userSetting.canEditRequisitions) {
        this.requisitionForm.controls['orderedByUserId'].disable();
      } else {
        this.requisitionForm.controls['orderedByUserId'].enable();
      }
      this.commonService.getAllWithId(this.userSetting.currentSiteId, 'General', 'Tool')
        .subscribe(result => {
          this.toolBomFilter = this.toolScanFilter = this.toolManualFilter = this.toolList = result.results;
        });
      this.requisitionNumber = 0;
    }
  }

  @Input()
  public selectedCategory = 0;

  @Input()
  public set siteId(siteId: number) {
    if (siteId > 0) {
      this.requisitionForm.controls['siteId'].setValue(siteId);
    }
  }

  @Output()
  cancel: EventEmitter<any> = new EventEmitter();


  private userIdList: any;
  private filteruserIdList: any;
  private requisitionList: any;
  private filterRequisitionList: any;
  private requisitionReasonList: any;
  private requisitionReasonFilterList: any;

  constructor(private commonService: CommonService,
              private domSanitizer: DomSanitizer,
              private generalToolService: GeneralToolService,
              private requisitionService: RequisitionService,
              private toasterHelperService: ToasterHelperService,
              private toolSharedService: ToolSharedService,
              private requisitionToolBomService: RequisitionToolBomService,
              private formBuilder: FormBuilder,
              private requisitionDetailService: RequisitionDetailService,
              private renderer: Renderer2) {


  }

  ngOnInit() {
    this.commonService.getAll('general', 'User').subscribe(eventResult => {
      if (eventResult.results != null && eventResult.results.length > 0) {
        eventResult.results.forEach(itm => {
          if (itm.extraDetail == null) {
            const item: any = new Object({
              userAvatar: this.commonService.getUserThumbnail(itm.value),
            });
            itm.extraDetail = item;
          }
        });
      }
      this.filteruserIdList = this.userIdList = eventResult.results;
    });

    this.commonService.getAll('ToolingManagement', 'Requisition').subscribe(eventResult => {
      this.filterRequisitionList = eventResult.results;
      this.filterRequisitionList.unshift(new Object({text: '', value: '', selected: false}));
      this.requisitionList = this.filterRequisitionList;
    });

    this.commonService.getAll('ToolingManagement', 'RequisitionReason').subscribe(eventResult => {
      this.requisitionReasonFilterList = this.requisitionReasonList = eventResult.results;
    });
    this.renderer.listen(
      'document',
      'click',
      ({target}) => {
        if (!isChildOf(target, 'k-grid-add-row')) {
          this.saveClick();
        }
      });
  }

  public saveClick(): void {
    if (this.formGroup && !this.formGroup.valid) {
      return;
    }

    this.saveRow();
  }

  public get isInEditingMode(): boolean {
    return this.editedRowIndex !== undefined || this.isNew;
  }

  private saveRow(): void {
    if (this.isInEditingMode) {
      if (this.isNew) {
        const requisitionDetailDto: RequisitionDetailDto = this.formGroup.value;
        this.requisitionDetailList.unshift(requisitionDetailDto);
        this.onRequisitionGenerateGridFill(this.requisitionDetailList);
        this.isNew = false;
      }
    }
    this.closeEditor(this.grid);
  }

  public removeHandlerNewRecord(value: any) {
    this.grid.closeRow(-1);
  }

  public formGroup: FormGroup = this.createFormGroup(new RequisitionDetailDto());

  public addHandler(value: any) {
    if (this.requisitionForm.valid) {
      if (!isNaN(value) && this.requisitionDetailList.length != 0) {
        return false;
      }
      this.isNew = true;
      this.closeEditor(this.grid);
      this.requisitionReasonFilterList = this.requisitionReasonList;
      this.formGroup = this.createFormGroup(new RequisitionDetailDto())
      this.grid.addRow(this.formGroup);
    }
  }

  private getRequisition(id: number): any {
    return this.requisitionReasonList.find(x => x.value === id);
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  public requisitionDetailGroup: FormGroup;
  private editedRowIndex: number;
  private isRequisitionSelectorOpen = false;
  private requisitionState: State = {
    skip: 0,
    take: 10,
  };
  private requisitionGridList: any[] = [];
  private requisitionGridView: GridDataResult;
  private requisitionNumber: number;
  private requisitionGridLoading = false;

  private dataStateRequisitionStateChange(state: DataStateChangeEvent): void {
    this.requisitionState = state;
    this.requisitionGridView = process(this.requisitionGridList, this.requisitionState);
  }

  private bindAllRequisition(userId) {
    this.requisitionGridLoading = true;
    this.requisitionService.getAllReuisitionsSiteWise(userId,
      success => {
        this.requisitionGridList = success;
        this.requisitionGridView = {
          data: success,
          total: success.length
        };
        this.requisitionGridLoading = false;
      },
      error => {
        this.requisitionGridLoading = false;
      });
  }

  private onRequisitionPoupOpen() {
    this.isRequisitionSelectorOpen = true;
    this.bindAllRequisition(this.userSetting.canEditRequisitions ? 0 : this.userSetting.userId);
  }


  private oncloseRequisitionSlecctor() {
    this.isRequisitionSelectorOpen = false;
  }

  public saveHandler({sender, rowIndex, formGroup, isNew}) {
    const requisitionDetailDto: RequisitionDetailDto = formGroup.value;
    this.requisitionDetailList.unshift(requisitionDetailDto);
    this.onRequisitionGenerateGridFill(this.requisitionDetailList);

    sender.closeRow(rowIndex);
  }

  onRequisitionGenerateGridFill(data: any) {
    this.gridView = {
      data: data,
      total: data.length
    };
    this.gridLoading = false;
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.requisitionDetailGroup = undefined;
  }


  private closeForm(): void {
    this.active = false;
    this.importedData = [];
    this.requisitionScanData = [];
    this.toolBomData = [];
    this.scanGridView = undefined;
    this.gridView = undefined;
    this.bomGridLoading = undefined;
    this.isToolScanDisable = false;
    this.scanGridLoading = false;
    this.scanSelectedTool = 0;
    this.requisitionNumber = 0;
    this.cancel.emit();
  }

  private userIdListHandleFilter(value) {
    this.filteruserIdList = this.userIdList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);

  }

  private isRequistionAddOpen = false;

  private onAddEditRequisition() {
    this.isRequistionAddOpen = true;
  }

  private onCancelRequisitionSelect() {
    this.isRequistionAddOpen = false;
  }

  public detailGridState: State;

  private dataStateChange(state: DataStateChangeEvent): void {
    this.detailGridState = state;
    this.gridView = process(this.requisitionDetailList, this.detailGridState);
  }


  private removeHandler(args: any, data: any) {
    if (data.requisitionDetailId != null) {
      this.requisitionDetailList = this.requisitionDetailList.filter(fd => fd.requisitionDetailId !== data.requisitionDetailId);
    } else {
      this.requisitionDetailList = this.requisitionDetailList.filter(fd => fd.station !== data.station || fd.detail !== data.detail || fd.quantityOrdered !== data.quantityOrdered);
    }

    this.onRequisitionGenerateGridFill(this.requisitionDetailList);
  }


  /*Submit Requisition*/
  onSubmitRequisition() {
    if (this.requisitionForm.valid) {
      const paramGenerateRequisitionAndDetail: ParamGenerateRequisitionAndDetail =
        new ParamGenerateRequisitionAndDetail();
      this.requisitionForm.controls['requisitionId'].setValue(this.requisitionNumber);
      paramGenerateRequisitionAndDetail.requisition = this.requisitionForm.value;
      if (this.requisitionForm.controls['toolId'].disabled) {
        paramGenerateRequisitionAndDetail.requisition.toolId = this.requisitionForm.controls['toolId'].value;
      }

      paramGenerateRequisitionAndDetail.details = this.requisitionDetailList;

      this.requisitionService.generateRequisitionAndDetail(paramGenerateRequisitionAndDetail,
        success => {
          if (success === true) {
            this.toasterHelperService.success('Created', 'Requisition detail created!');
            this.closeForm();
          } else {
            this.toasterHelperService.error('Error', 'Requisition falied to create! Please try again  ' + success);
          }

        },
        error => {

          this.toasterHelperService.error('Server side error', JSON.stringify(error.error.exceptionMessage));
        });
    }
  }

  private isLoadDefaultRequisition = true;

  onRequisitionNumberChange(event, dataItem) {
    if (dataItem === undefined) {
      this.requisitionDetailList = [];
      this.onRequisitionGenerateGridFill(this.requisitionDetailList);
      this.requisitionForm.controls['toolId'].reset('');
      this.requisitionForm.controls['toolId'].enable();
      this.requisitionNumber = 0;
      this.isRequisitionSelectorOpen = false;
    } else {
      this.requisitionNumber = dataItem.requisitionId;
      if (dataItem.toolId > 0) {
        this.requisitionForm.controls['toolId'].setValue(Number(dataItem.toolId));
        this.requisitionForm.controls['toolId'].disable();
      } else {
        this.requisitionForm.controls['toolId'].reset('');
        this.requisitionForm.controls['toolId'].enable();
      }
      this.isRequisitionSelectorOpen = false;
      if (dataItem.toolId !== null &&
        dataItem.toolId !== undefined &&
        this.requisitionForm.valid &&
        this.isLoadDefaultRequisition) {
        this.gridLoading = true;
        this.requisitionDetailService.getrequisitionsDetails(this.requisitionNumber,
          dataItem.toolId,
          this.requisitionForm.controls['orderedByUserId'].value,
          success => {
            this.requisitionDetailList = success;
            this.onRequisitionGenerateGridFill(success);

          },
          error => {

          });
      } else {

      }
    }
  }

  private requisitionListHandleFilter(value) {
    this.filterRequisitionList =
      this.requisitionList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  public onRemoveGridRow(sender, dataItem) {
    // this.requisitionDetailList = this.requisitionDetailList.filter((req) => req.station !== dataItem.station);
    this.onRequisitionGenerateGridFill(this.requisitionDetailList);

    sender.cancelCell();
  }

  public cellClickHandler({sender, rowIndex, columnIndex, dataItem, isEdited}) {
    // { sender, rowIndex, columnIndex, dataItem, isEdited }
    if (!isEdited) {
      this.requisitionReasonFilterList = this.requisitionReasonList;
      sender.editCell(rowIndex, columnIndex, this.createFormGroup(this.gridView.data[rowIndex]));
    }
  }

  public cellCloseHandler(args: any) {
    const {formGroup, dataItem, rowIndex} = args;
    if (formGroup.dirty) {
      this.gridView.data[rowIndex] = formGroup.value;
      this.requisitionList[rowIndex] = formGroup.value;
    }
  }


  public createFormGroup(dataItem: any): FormGroup {
    const date = new Date();
    date.setDate(date.getDate() + 60);
    return this.formBuilder.group({
      'station': new FormControl(dataItem.station),
      'detail': new FormControl(dataItem.detail),
      'description': new FormControl(dataItem.description),
      'dateDue': new FormControl(dataItem.dateDue !== undefined ? dataItem.dateDue : date),
      'quantityOrdered': new FormControl(dataItem.quantityOrdered),
      'requisitionReasonId': new FormControl(dataItem.requisitionReasonId, Validators.required),
      'updatePrintCompleted': new FormControl(dataItem.updatePrintCompleted),
      'requisitionDetailCategoryId': new FormControl(this.selectedCategory === 0 ? null : this.selectedCategory),
    });
  }

  private toolManualFilter: any[] = [];
  private toolScanFilter: any[] = [];
  private toolBomFilter: any[] = [];

  private toolManualHandleFilter(value) {
    this.toolManualFilter = this.toolList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private toolScanHandleFilter(value) {
    this.toolScanFilter = this.toolList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private toolBomHandleFilter(value) {
    this.toolBomFilter = this.toolList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private manualReasonHandleFilter(value) {
    this.requisitionReasonFilterList = this.requisitionReasonList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  /*General*/
  private selectedTab = 0;

  public onRequisitionTabSelect(e) {
    this.selectedTab = e.index;
    switch (e.index) {
      case 0:
        break;
      case 1:
        this.isRequisitionFocus = true;
        this.requisitionScanData = '';
        break;
      case 2:
        this.toolBomgridView = {
          data: [],
          total: 0
        };
        this.toolBomData = [];
        this.importedData = [];
        break;
    }
  }


  /*Scan input*/
  private scanGridView: GridDataResult;
  private scanGridLoading = false;
  private requisitionScanData;
  private scanSelectedTool;
  private isToolScanDisable = false;
  private isRequisitionFocus = false;

  loadScanGrid(scantext: string) {
    this.scanGridLoading = true;
    this.requisitionService.scanRequisitionDetail(scantext, success => {
      if (success.count > 0) {
        this.scanGridView = {
          data: success,
          total: success.length
        };
      } else {
        this.toasterHelperService.info('Data not available', 'No requisition found!');
      }
      this.scanGridLoading = false;
    }, error => {
      this.toasterHelperService.error('Error', 'Invalid barcode');
      this.scanGridLoading = false;
    });
  }

  onBlurRequisitionScan() {
    this.isRequisitionFocus = false;
    if (this.requisitionScanData !== null &&
      this.requisitionScanData !== undefined &&
      this.requisitionScanData.toString().trim() !== '') {
      this.loadScanGrid(this.requisitionScanData);
    }

  }

  onRequisitionNumberScanChange(event) {
    const toolId = this.requisitionList
      .filter((s) => s.text.toLowerCase().indexOf(event.toLowerCase()) === 0)[0].value;

    if (toolId > 0) {
      this.scanSelectedTool = Number(toolId);
      this.isToolScanDisable = true;
    } else {
      this.scanSelectedTool = '';
      this.isToolScanDisable = true;
    }

  }

  /*Tool BOM import */
  private toolBomgridView: GridDataResult;
  private bomGridLoading = false;
  private importedData: any[] = [];
  private toolBomData: any[] = [];
  private bomToolId: number;

  onToolBomChange(value: any) {
    if (value.value > 0) {
      this.loadBomToolDataGrid(value.value);
      this.bomToolId = value.value;
    }
  }

  loadBomToolDataGrid(toolId: number) {
    this.bomGridLoading = true;
    this.requisitionToolBomService.getAllTool(toolId,
      success => {
        this.toolBomgridView = {
          data: success,
          total: success.length
        };
        this.toolBomData = success;
        this.bomGridLoading = false;
      },
      error => {
        this.bomGridLoading = false;
      });
  }

  onBomSelect(value) {
    value.deselectedRows.forEach((value) => {
      if (value.dataItem !== undefined) {
        this.importedData = this.importedData
          .filter(bom => bom.toolBOMId !== value.dataItem.toolBOMId);
      }
    });
    value.selectedRows.forEach((value) => {
      this.importedData.push(this.toolBomData[value.index]);
    });
  }

  onImportToolData() {
    const importData: RequisitionDetailDto[] = [];
    const date = new Date();
    date.setDate(date.getDate() + 60);
    this.importedData.forEach(data => {
      data.quantityOrdered = data.quantity;
      data.dateDue = date;
      data.requisitionDetailCategoryId = this.selectedCategory === 0 ? null : this.selectedCategory;
      importData.push(RequisitionDetailDto.fromJS(data));
    });
    this.requisitionDetailList = importData;
    this.onRequisitionGenerateGridFill(importData);
    this.selectedTab = 0;
    this.requisitionForm.controls['toolId'].setValue(this.bomToolId);
    this.bomToolId = undefined;
  }
}
