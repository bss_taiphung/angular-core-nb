// region IRequisitionDetailDto
export interface IRequisitionDetailDto {
  requisitionDetailId: number;
  requisitionId: number;
  station: string;
  detail: string;
  description: string;
  material: string;
  materialHardness: string;
  dateDue: Date;
  dateCompleted: Date;
  quantityOrdered: number;
  vendorId: number;
  lastProcessId: number;
  requisitionReasonId: number;
  approved: boolean;
  locked: boolean;
  denialReason: string;
  approvedBy: number;
  updatePrintRequested: boolean;
  isPrinted: boolean;
  requisitionStatusId: number;
  priority: string;
  requisitionDetailCategoryId: string;
  updatePrintCompleted: boolean;
  updatePrintCompletedByUserId: string;
  updatePrint: boolean;
  active: boolean;
  orderedByName: string;
  requisitionStatusName: string;
  updatePrintCompletedByName: string;
  requisitionReasonName: string;
  reasonDisplay: string;
  quantityCompleted: number;
  quantityOpen: number;
}

export class RequisitionDetailDto implements IRequisitionDetailDto {
  requisitionDetailId: number;
  requisitionId: number;
  station: string;
  detail: string;
  description: string;
  material: string;
  materialHardness: string;
  dateDue: Date;
  dateCompleted: Date;
  quantityOrdered: number;
  vendorId: number;
  lastProcessId: number;
  requisitionReasonId: number;
  approved: boolean;
  locked: boolean;
  denialReason: string;
  approvedBy: number;
  updatePrintRequested: boolean;
  isPrinted: boolean;
  requisitionStatusId: number;
  priority: string;
  requisitionDetailCategoryId: string;
  updatePrintCompleted: boolean;
  updatePrintCompletedByUserId: string;
  updatePrint: boolean;
  active: boolean;
  toolNumber: string;
  requisitionStatusName: string;
  updatePrintCompletedByName: string;
  orderedByName: string;
  reqisitionStatusName: string;
  requisitionReasonName: string;
  reasonDisplay: string;
  quantityCompleted: number;
  quantityOpen: number;

  static fromJS(data: any): RequisitionDetailDto {
    const result = new RequisitionDetailDto();
    result.init(data);
    return result;
  }

  constructor(data?: IRequisitionDetailDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.requisitionDetailId = data['requisitionDetailId'];
      this.requisitionId = data['requisitionId'];
      this.station = data['station'];
      this.detail = data['detail'];
      this.description = data['description'];
      this.material = data['material'];
      this.materialHardness = data['materialHardness'];
      this.dateDue = data['dateDue'] !== null ? new Date(data['dateDue']) : null;
      this.dateCompleted = data['dateCompleted'] !== null ? new Date(data['dateCompleted']) : null;
      this.quantityOrdered = data['quantityOrdered'];
      this.vendorId = data['vendorId'];
      this.lastProcessId = data['lastProcessId'];
      this.requisitionReasonId = data['requisitionReasonId'];
      this.approved = data['approved'];
      this.locked = data['locked'];
      this.denialReason = data['denialReason'];
      this.approvedBy = data['approvedBy'];
      this.updatePrintRequested = data['updatePrintRequested'];
      this.orderedByName = data['orderedByName'];
      this.requisitionStatusName = data['requisitionStatusName'];
      this.isPrinted = data['isPrinted'];
      this.updatePrintCompletedByName = data['updatePrintCompletedByName'];
      this.requisitionStatusId = data['requisitionStatusId'];
      this.priority = data['priority'];
      this.requisitionDetailCategoryId = data['requisitionDetailCategoryId'];
      this.updatePrintCompleted = data['updatePrintCompleted'];
      this.updatePrintCompletedByUserId = data['updatePrintCompletedByUserId'];
      this.updatePrint = data['updatePrint'];
      this.active = data['active'];
      this.toolNumber = data['toolNumber'];
      this.requisitionReasonName = data['requisitionReasonName'];
      this.reasonDisplay = data['reasonDisplay'];
      this.quantityCompleted = data['quantityCompleted'];
      this.quantityOpen = data['quantityOpen'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['requisitionDetailId'] = this.requisitionDetailId;
    data['requisitionId'] = this.requisitionId;
    data['station'] = this.station;
    data['detail'] = this.detail;
    data['description'] = this.description;
    data['material'] = this.material;
    data['materialHardness'] = this.materialHardness;
    data['dateDue'] = this.dateDue;
    data['dateCompleted'] = this.dateCompleted;
    data['quantityOrdered'] = this.quantityOrdered;
    data['vendorId'] = this.vendorId;
    data['lastProcessId'] = this.lastProcessId;
    data['requisitionReasonId'] = this.requisitionReasonId;
    data['approved'] = this.approved;
    data['locked'] = this.locked;
    data['denialReason'] = this.denialReason;
    data['approvedBy'] = this.approvedBy;
    data['updatePrintRequested'] = this.updatePrintRequested;
    data['isPrinted'] = this.isPrinted;
    data['requisitionStatusId'] = this.requisitionStatusId;
    data['priority'] = this.priority;
    data['requisitionDetailCategoryId'] = this.requisitionDetailCategoryId;
    data['updatePrintCompleted'] = this.updatePrintCompleted;
    data['updatePrintCompletedByUserId'] = this.updatePrintCompletedByUserId;

    data['updatePrint'] = this.updatePrint;
    data['toolNumber'] = this.toolNumber;
    data['active'] = this.active;
    data['orderedByName'] = this.orderedByName;
    data['requisitionStatusName'] = this.requisitionStatusName;
    data['updatePrintCompletedByName'] = this.updatePrintCompletedByName;
    data['requisitionReasonName'] = this.requisitionReasonName;
    data['reasonDisplay'] = this.reasonDisplay;
    data['quantityCompleted'] = this.quantityCompleted;
    data['quantityOpen'] = this.quantityOpen;
    return data;
  }
}

// endregion

// region ICreateRequisitionDetailDto
export interface ICreateRequisitionDetailDto {
  active: boolean;
}

export class CreateRequisitionDetailDto implements ICreateRequisitionDetailDto {
  requisitionDetailId = 0;
  active = true;

  static fromJS(data: any): RequisitionDetailDto {
    const result = new RequisitionDetailDto();
    result.init(data);
    return result;
  }

  constructor(data?: IRequisitionDetailDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.requisitionDetailId = 0;
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['requisitionDetailId'] = 0;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

// endregion

// region IPagedResultDtoOfRequisitionDetailDto
export interface IPagedResultDtoOfRequisitionDetailDto {
  results: RequisitionDetailDto[];
  count: number;
}

export class PagedResultDtoOfRequisitionDetailDto implements IPagedResultDtoOfRequisitionDetailDto {
  results: RequisitionDetailDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfRequisitionDetailDto {
    const result = new PagedResultDtoOfRequisitionDetailDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfRequisitionDetailDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(RequisitionDetailDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion

// region IParamGenerateRequisitionAndDetailDto
export interface IParamGenerateRequisitionAndDetailDto {
  requisitionDetailId: number;
}

export class ParamGenerateRequisitionAndDetailDto implements IParamGenerateRequisitionAndDetailDto {
  requisitionDetailId = 0;
  active = true;

  static fromJS(data: any): ParamGenerateRequisitionAndDetailDto {
    const result = new ParamGenerateRequisitionAndDetailDto();
    result.init(data);
    return result;
  }

  constructor(data?: IParamGenerateRequisitionAndDetailDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.requisitionDetailId = 0;
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['requisitionDetailId'] = 0;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

// endregion

// region IRetRequisitionDetailCategory
export interface IRetRequisitionDetailCategory {
  requisitionDetailCategoryId: number;
  requisitionDetailCategoryName: string;
  siteId: number;
}

export class RetRequisitionDetailCategory implements IRetRequisitionDetailCategory {
  requisitionDetailCategoryId: number;
  requisitionDetailCategoryName: string;
  siteId: number;

  static fromJS(data: any): RetRequisitionDetailCategory {
    const result = new RetRequisitionDetailCategory();
    result.init(data);
    return result;
  }

  constructor(data?: IRetRequisitionDetailCategory) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (data) {
        this.requisitionDetailCategoryId = data['requisitionDetailCategoryId'];
        this.requisitionDetailCategoryName = data['requisitionDetailCategoryName'];
        this.siteId = data['siteId'];
      }
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['requisitionDetailCategoryId'] = this.requisitionDetailCategoryId;
    data['requisitionDetailCategoryName'] = this.requisitionDetailCategoryName;
    data['siteId'] = this.siteId;
    return data;
  }
}

// endregion

// region IParamGenerateRequisitionAndDetail
export interface IParamGenerateRequisitionAndDetail {
  requisition: ParamRequisition;
  details: RequisitionDetailDto[];

}

export class ParamGenerateRequisitionAndDetail implements IParamGenerateRequisitionAndDetail {
  requisition: ParamRequisition;
  details: RequisitionDetailDto[];

  static fromJS(data: any): RetRequisitionDetailCategory {
    const result = new RetRequisitionDetailCategory();
    result.init(data);
    return result;
  }

  constructor(data?: IRetRequisitionDetailCategory) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (data) {
        this.requisition = data['requisition'];
        this.details = data['details'];
      }
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['requisition'] = this.requisition;
    data['details'] = this.details;
    return data;
  }

}

// endregion

// region IParamRequisition
export interface IParamRequisition {
  requisitionId: number;
  siteId: number;
  toolId: number;
  orderedByUserId: number;
}

export class ParamRequisition implements IParamRequisition {
  requisitionId: number;
  siteId: number;
  toolId: number;
  orderedByUserId: number;

  static fromJS(data: any): ParamRequisition {
    const result = new ParamRequisition();
    result.init(data);
    return result;
  }

  constructor(data?: IParamRequisition) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (data) {
        this.requisitionId = data['requisitionId'];
        this.siteId = data['siteId'];
        this.toolId = data['toolId'];
        this.orderedByUserId = data['orderedByUserId'];
      }
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['requisitionId'] = this.requisitionId;
    data['siteId'] = this.siteId;
    data['toolId'] = this.toolId;
    data['orderedByUserId'] = this.orderedByUserId;
    return data;
  }
}

// endregion

// region ITool
export interface ITool {
  toolId: number;
  toolNumber: string;
  toolDescription: string;
  partId: number;
  projectId: number;
  siteId: number;
}

export class Tool implements ITool {
  toolId: number;
  toolNumber: string;
  toolDescription: string;
  partId: number;
  projectId: number;
  siteId: number;

  static fromJS(data: any): Tool {
    const result = new Tool();
    result.init(data);
    return result;
  }

  static fromInitList(data?: any): any {
    const resultsList: Tool[] = [];
    if (data && data != null) {
      if (data.constructor === Array) {
        for (const item of data) {
          resultsList.push(Tool.fromJS(item));
        }
      }
    }
    return resultsList;
  }

  constructor(data?: IParamRequisition) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (data) {
        this.toolId = data['toolId'];
        this.toolNumber = data['toolNumber'];
        this.toolDescription = data['toolDescription'];
        this.partId = data['partId'];
        this.projectId = data['projectId'];
        this.siteId = data['siteId'];
      }
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['toolId'] = this.toolId;
    data['toolNumber'] = this.toolNumber;
    data['toolDescription'] = this.toolDescription;
    data['partId'] = this.partId;
    data['projectId'] = this.projectId;
    data['siteId'] = this.siteId;
    return data;
  }

}

// endregion

export class ParamString {
  stringValue: string;
}
