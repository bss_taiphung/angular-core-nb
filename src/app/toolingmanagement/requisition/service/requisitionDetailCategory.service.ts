import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, HttpModule } from '@angular/http';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

import { CommonService } from "../../../shared/services/common.service";
import { RetRequisitionDetailCategory} from "./requisition.model";
import {environment} from '../../../../environments/environment';

@Injectable()
export class RequisitionDetailCategoryService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService, private http: HttpClient) {
    this.apiBaseUrl = environment.bllApiBaseAddress + "/ToolingManagement/api/RequisitionDetailCategory/";
  }

  //create(input: CreateUserDto, successCallback: any, errorCallback: any): any {
  //  let url = this.apiBaseUrl + "InsertAsync";
  //  this.commonService.httpPost(url, input, successCallback, errorCallback);
  //}

  //update(input: UserDto, successCallback: any, errorCallback: any): any {
  //  let url = this.apiBaseUrl + "UpdateAsync";
  //  this.commonService.httpPost(url, input, successCallback, errorCallback);
  //}

  //delete(id: any, successCallback: any, errorCallback: any): any {
  //  let url = this.apiBaseUrl + "DeleteAsync";
  //  let params = new HttpParams().set('id', id);

  //  let options = {
  //    params: params
  //  };

  //  this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
  //}

  getAll(): Observable<RetRequisitionDetailCategory> {
    
    let url = this.apiBaseUrl + "GetList";
    //let params = new HttpParams().set('siteid', siteId.toString());

    let options = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      }),
     // params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      let result200 = RetRequisitionDetailCategory.fromJS(response);
      return Observable.of(result200);
    });
  }

  //getUserSetting(userId:number): Observable<any> {let url = this.apiBaseUrl + "GetUserSetting";
  //  let params = new HttpParams().set('UserId', userId.toString());
    
  //  let options = {
  //    headers: new HttpHeaders({
  //      "Content-Type": "application/json",
  //      "Accept": "application/json"
  //    }),
  //    params: params
  //  };

  //  return this.http.request<Response>('get', url, options).flatMap((response) => {
  //    let result = UserSettingDto.fromJS(response);
  //    debugger;
  //    return Observable.of(result);
  //  });
  //}debugger

  getDataBySiteId(siteId: number, isactive: boolean = true, successCallback: any, errorCallback:any) {

    let url = this.apiBaseUrl + "GetDataBySiteId";
    let params = new HttpParams().set('siteId', siteId.toString());
   
    let options = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      }),
      params: params
    };

    this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
  }


  //getAll(): Observable<any> {
  //  this.commonService.getAll("ToolingManagement", "RequisitionDetail").subscribe(eventResult => {
  //    let a = eventResult.results;
  //    return eventResult;
  //  });
  //}


}

