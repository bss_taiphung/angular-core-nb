import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

import {CommonService} from '../../../shared/services/common.service';
import {ParamGenerateRequisitionAndDetail, ParamString} from './requisition.model';
import {ToolSharedService} from '../../shared/service/toolShared.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class RequisitionService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService, private http: HttpClient,
              private toolSharedService: ToolSharedService) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/ToolingManagement/api/Requisition/';
  }


  generateRequisitionAndDetail(input: ParamGenerateRequisitionAndDetail, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'generateRequisitionAndDetail';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  scanRequisitionDetail(text: string, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'ScanRequisitionDetail';
    const dataParam: ParamString = new ParamString();
    dataParam.stringValue = text;
    this.commonService.httpPost(url, dataParam, successCallback, errorCallback);
  }

  getAllReuisitionsSiteWise(orderedByUserId: number, successCallback, errorCallback) {
    const url = this.apiBaseUrl + 'SearchAsync';
    let siteData: any;
    if (orderedByUserId > 0) {
      siteData = {
        'SiteId': this.toolSharedService.getUserSettings().currentSiteId.toString(),
        'OrderedByUserId': orderedByUserId.toString()
      }
    } else {
      siteData = {
        'SiteId': this.toolSharedService.getUserSettings().currentSiteId.toString()
      }
    }

    this.commonService.httpPost(url, siteData, successCallback, errorCallback);
  }
}
