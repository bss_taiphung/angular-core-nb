import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';

import {CommonService} from '../../../shared/services/common.service';
import {PagedResultDtoOfRequisitionDetailDto, RequisitionDetailDto} from './requisition.model';
import {ToolSharedService} from '../../shared/service/toolShared.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class RequisitionDetailService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService, private http: HttpClient, private toolSharedService: ToolSharedService) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/ToolingManagement/api/RequisitionDetail/';
  }

  /*create(input: CreateUserDto, successCallback: any, errorCallback: any): any {
   let url = this.apiBaseUrl + "InsertAsync";
   this.commonService.httpPost(url, input, successCallback, errorCallback);
  }*/

  update(input: RequisitionDetailDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'UpdateAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  scanToWork(requisitionDetailId: any): any {
    const url = this.apiBaseUrl + 'GetDataByRequisitionDetailId';
    const params = new HttpParams()
      .set('requisitionDetailId', requisitionDetailId.toString());


    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };
    return this.http.get(url, options);
  }

  delete(id: any, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'DeleteAsync';
    const params = new HttpParams().set('id', id);
    const options = {
      params: params
    };

    this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
  }

  getAll(take: number, skip: number, sort: any, isactive: boolean, searchtext: string, extraFilter: string = 'all', isOrderedbyMe: boolean = false, categoryId: number = 0, updatePrintRequested: any = null, updatePrintCompleted: boolean = null): Observable<PagedResultDtoOfRequisitionDetailDto> {
    const url = this.apiBaseUrl + 'GetPagedQuery';

    let orderBy = '';
    if (sort != undefined && sort.length > 0) {
      for (const item of sort) {
        orderBy = this.commonService.firstUpper(item.field) + ' ' + (item.dir == undefined ? 'asc' : item.dir);
      }
    } else {
      orderBy = 'RequisitionDetailId desc';
    }

    let filter = '';
    if (isactive) {
      filter = 'Active eq true';
    }
    if (searchtext.length > 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += searchtext;
    }
    if (updatePrintRequested) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += updatePrintRequested;
    }
    if (updatePrintCompleted) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += 'UpdatePrintCompleted eq ' + false;
      // +++++ filter +=" or UpdatePrintCompleted eq "+null;
    }
    if (isOrderedbyMe) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += 'OrderedByUserId eq ' + this.toolSharedService.getUserSettings().userId + ' ';
    }
    if (categoryId !== 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += 'RequisitionDetailCategoryId eq ' + categoryId + ' ';
    }
    if (extraFilter !== undefined && extraFilter !== null && extraFilter.toLowerCase() !== 'all') {

      switch (extraFilter.toLowerCase()) {
        case 'notprinted':
          filter += (filter.length > 0) ? ' and ' : '';
          filter += 'isPrinted  eq false';
          filter += (filter.length > 0) ? ' and ' : '';
          filter += 'Approved eq true';
          break;
        case 'pending':
          filter += (filter.length > 0) ? ' and ' : '';
          filter += '	Approved eq null';
          break;
        case 'approved':
          filter += (filter.length > 0) ? ' and ' : '';
          filter += '	Approved eq true';
          break;
        case 'rejected':
          filter += (filter.length > 0) ? ' and ' : '';
          filter += '	Approved eq false';
          break;
      }

    }

    const params = new HttpParams()
      .set('$top', take.toString())
      .set('$skip', skip.toString())
      .set('$count', 'true')
      .set('$orderby', orderBy.toString())
      .set('$filter', filter.toString());


    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      const result200 = PagedResultDtoOfRequisitionDetailDto.fromJS(response);
      return Observable.of(result200);
    });
  }


  getrequisitionsDetails(reqisitionId: number, toolId: number, orderByUserId: number, successCallback, errorCallback): any {
    const url = this.apiBaseUrl + 'SearchAsync';
    const siteData = {'RequisitionId': reqisitionId.toString(), 'ToolId': toolId.toString(), 'OrderedByUserId': orderByUserId.toString()}
    this.commonService.httpPost(url, siteData, successCallback, errorCallback);
  }

  /*getAll(): Observable<any> {
   this.commonService.getAll("ToolingManagement", "RequisitionDetail").subscribe(eventResult => {
     let a = eventResult.results;
     return eventResult;
   });
  }*/
}

