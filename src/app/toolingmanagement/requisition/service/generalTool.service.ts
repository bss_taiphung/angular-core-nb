import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, HttpModule } from '@angular/http';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

import { CommonService } from "../../../shared/services/common.service";
import { Tool } from "./requisition.model";
import { ToolSharedService} from "../../shared/service/toolShared.service";
import {environment} from '../../../../environments/environment';

@Injectable()
export class GeneralToolService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService, private http: HttpClient, private toolSharedService: ToolSharedService) {
    this.apiBaseUrl = environment.bllApiBaseAddress + "/General/api/Tool/";
  }
  

  //getAll(): Observable<any> {
  //  this.commonService.getAll("General", "RequisitionDetail").subscribe(eventResult => {
  //    let a = eventResult.results;
  //    return eventResult;
  //  });
  //}

  getAll() {

    let url = this.apiBaseUrl + "GetList";

    let filter = '';
    
    filter = "Active eq true";
   
    filter += (filter.length > 0) ? ' and ' : '';
    filter += "SiteId eq " + this.toolSharedService.getUserSettings().currentSiteId + " ";

  

    const params = new HttpParams()
      .set('$filter', filter.toString());
    let options = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      }),
       params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      let result200 = Tool.fromInitList(response);
      return Observable.of(result200);
    });
  }


  getDataSitewise(siteId) {
    let url = this.apiBaseUrl + "GetSelectListBySiteId";

   const params = new HttpParams()
      .set('siteId', this.toolSharedService.getUserSettings().currentSiteId );
    let options = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      let result200 = Tool.fromInitList(response);
      return Observable.of(result200);
    });
  }


}

