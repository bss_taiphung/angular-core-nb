import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Validators, FormGroup, FormControl, ReactiveFormsModule} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';
import {DomSanitizer} from '@angular/platform-browser';
import {CommonService} from '../../shared/services/common.service';
import {SelectListDto} from '../../shared/models/selectListDto';
import {RequisitionDetailDto} from './service/requisition.model';
import {RequisitionDetailService} from './service/requisitionDetail.service';
import {ToolSharedService} from '../shared/service/toolShared.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';

@Component({
  selector: 'kendo-grid-requisitionEdit-form',
  styles: [
    'input[type=text] { width: 100%; } label{font-weight: bold !important;}'
  ],
  templateUrl: './html/requisitionEdit-form.component.html'
})
export class RequisitionEditFormComponent {
  public active = false;
  private exclusionLoading: boolean = false;
  private userSetting: any;
  public requisitionDetailForm: FormGroup = new FormGroup({
    'requisitionDetailId': new FormControl(),
    'requisitionId': new FormControl(),
    'toolNumber': new FormControl(),
    'detail': new FormControl(),
    'station': new FormControl('', Validators.required),
    'description': new FormControl(),
    'material': new FormControl(),
    'materialHardness': new FormControl(),
    'dateDue': new FormControl(),
    'dateCompleted': new FormControl(),
    'quantityOrdered': new FormControl(),
    'vendorId': new FormControl(),
    'lastProcessId': new FormControl(),
    'requisitionReasonId': new FormControl(),
    'locked': new FormControl(),
    'denialReason': new FormControl(),
    'approvedBy': new FormControl(),
    'updatePrintRequested': new FormControl(),
    'isPrinted': new FormControl(),
    'requisitionStatusId': new FormControl(),
    'priority': new FormControl(),
    'requisitionDetailCategoryId': new FormControl(),
    'updatePrintCompleted': new FormControl(),
    'updatePrintCompletedByUserId': new FormControl(),
    'approved': new FormControl(),
    'active': new FormControl(false),

  });

  private reqApproveList: any[] = [];
  private reqPriority: any[] = [];
  private reqApproveFilterList: any[] = [];
  private reqPriorityFilter: any[] = [];
  private completedQty: any;
  @Input() public isNew = false;

  @Input() public isRequisitionEditActive = false;

  @Input()
  public set model(requisitionDetailDto: RequisitionDetailDto) {
    if (requisitionDetailDto !== undefined) {
      this.completedQty = requisitionDetailDto.quantityCompleted;
      let s = this.userSetting;
      let result = new RequisitionDetailDto();
      result.init(requisitionDetailDto);
      this.requisitionDetailForm.reset(result);
      this.requisitionDetailForm.controls['updatePrintRequested'].setValue(requisitionDetailDto.updatePrint);
      this.active = true;
      this.fillAlldropdown();
      this.checkPermission();

    }

  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<RequisitionDetailDto> = new EventEmitter();

  private userIdList: any;
  private filteruserIdList: any;

  constructor(private commonService: CommonService,
              private domSanitizer: DomSanitizer,
              private requisitionDetailService: RequisitionDetailService,
              private toolSharedService: ToolSharedService,
              private toasterHelperService: ToasterHelperService) {


  }

  ngOnInit() {
    this.userSetting = this.toolSharedService.getUserSettings();
    this.reqApproveList.push(new Object(
      {
        text: '<select>',
        value: null
      }));
    this.reqApproveList.push(new Object({
      text: 'Yes',
      value: true
    }));
    this.reqApproveList.push(new Object({
      text: 'No',
      value: false
    }));
    this.reqApproveFilterList = this.reqApproveList;
    this.reqPriority.push(new Object(
      {
        text: 'A',
        value: 'A'
      }));
    this.reqPriority.push(new Object({
      text: 'B',
      value: 'B'
    }));
    this.reqPriority.push(new Object({
      text: 'C',
      value: 'C'
    }));
    this.reqPriorityFilter = this.reqPriority;
  }

  public onTabSelect(e) {
  }

  public onSave(e): void {
    this.exclusionLoading = true;
    this.requisitionDetailService.update(this.requisitionDetailForm.value, success => {
      if (success === true) {
        this.exclusionLoading = false;
        this.toasterHelperService.success('Updated', 'Reqision updated successfully.');
        this.closeForm();
      } else {
        this.toasterHelperService.error('Not Updated', 'Reqision not updated.');
        this.exclusionLoading = false;
      }

    }, error => {
      this.toasterHelperService.error('Error', 'Server side error.');
      this.exclusionLoading = false;
    });
    //this.save.emit(this.requisitionDetailForm.value);
    this.active = false;
  }


  private closeForm(): void {
    this.isRequisitionEditActive = false;
    this.completedQty = null;
    this.requisitionDetailForm.reset();
    this.exclusionLoading = false;
    this.active = false;
    this.cancel.emit();
  }


  private onMaterialChange(value) {
    let materialData = this.materialList.filter((m) => m.value === value);
    this.requisitionDetailForm.controls['material'].setValue(value.text);
  }

  private materialList: any[] = [];
  private materialFilterList: any[] = [];
  private reasonList: any[] = [];
  private reasonFilterList: any[] = [];
  private statusList: any[] = [];
  private statusFilterList: any[] = [];
  private venderList: any[] = [];
  private venderFilterList: any[] = [];

  private fillAlldropdown() {

    this.commonService.getAll('ToolingManagement', 'RequisitionMaterial').subscribe(eventResult => {
      this.materialFilterList = this.materialList = eventResult.results;
    });
    this.commonService.getAll('ToolingManagement', 'RequisitionReason').subscribe(eventResult => {
      this.reasonFilterList = this.reasonList = eventResult.results;
    });
    this.commonService.getAll('ToolingManagement', 'RequisitionStatus').subscribe(eventResult => {
      this.statusFilterList = this.statusList = eventResult.results;
    });

    this.commonService.getAll('general', 'User').subscribe(eventResult => {
      if (eventResult.results != null && eventResult.results.length > 0) {
        eventResult.results.forEach(itm => {
          if (itm.extraDetail == null) {
            var item: any = new Object({
              userAvatar: this.commonService.getUserThumbnail(itm.value),
            });
            itm.extraDetail = item;
          }
        });
      }
      this.filteruserIdList = this.userIdList = eventResult.results;
    });

    this.commonService.getAllWithId(this.userSetting.currentSiteId, 'General', 'Vendor').subscribe(result => {
      result.results.unshift(new SelectListDto({text: '', value: null, isSelected: false}));
      this.venderFilterList = this.venderList = result.results;
    });
  }

  private userIdListHandleFilter(value) {
    this.filteruserIdList = this.userIdList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);

  }

  private checkPermission() {
    if (this.userSetting.canEditRequisitions) {
      this.requisitionDetailForm.controls['detail'].enable();
      this.requisitionDetailForm.controls['station'].enable();
      this.requisitionDetailForm.controls['description'].enable();
      this.requisitionDetailForm.controls['material'].enable();
      this.requisitionDetailForm.controls['materialHardness'].enable();
      this.requisitionDetailForm.controls['dateDue'].enable();
      this.requisitionDetailForm.controls['dateCompleted'].enable();
      this.requisitionDetailForm.controls['quantityOrdered'].enable();
      this.requisitionDetailForm.controls['vendorId'].enable();
      this.requisitionDetailForm.controls['lastProcessId'].enable();
      this.requisitionDetailForm.controls['requisitionReasonId'].enable();
      if (this.userSetting.canLockRequisitions) {
        this.requisitionDetailForm.controls['locked'].enable();
      } else {
        this.requisitionDetailForm.controls['locked'].disable();
      }
      this.requisitionDetailForm.controls['denialReason'].enable();
      this.requisitionDetailForm.controls['approvedBy'].enable();
      this.requisitionDetailForm.controls['updatePrintRequested'].enable();
      this.requisitionDetailForm.controls['isPrinted'].enable();
      this.requisitionDetailForm.controls['requisitionStatusId'].enable();
      this.requisitionDetailForm.controls['priority'].enable();
      this.requisitionDetailForm.controls['requisitionDetailCategoryId'].enable();
      this.requisitionDetailForm.controls['updatePrintCompleted'].enable();
      this.requisitionDetailForm.controls['updatePrintCompletedByUserId'].enable();
      if (this.userSetting.canApproveRequisitions) {
        this.requisitionDetailForm.controls['approved'].enable();
      } else {
        this.requisitionDetailForm.controls['approved'].disable();
      }
      this.requisitionDetailForm.controls['active'].enable();
    } else {
      this.requisitionDetailForm.controls['detail'].disable();
      this.requisitionDetailForm.controls['station'].disable();
      this.requisitionDetailForm.controls['description'].disable();
      this.requisitionDetailForm.controls['material'].disable();
      this.requisitionDetailForm.controls['materialHardness'].disable();
      this.requisitionDetailForm.controls['dateDue'].disable();
      this.requisitionDetailForm.controls['dateCompleted'].disable();
      this.requisitionDetailForm.controls['quantityOrdered'].disable();
      this.requisitionDetailForm.controls['vendorId'].disable();
      this.requisitionDetailForm.controls['lastProcessId'].disable();
      this.requisitionDetailForm.controls['requisitionReasonId'].disable();
      if (this.userSetting.canLockRequisitions) {
        this.requisitionDetailForm.controls['locked'].enable();
      } else {
        this.requisitionDetailForm.controls['locked'].disable();
      }
      this.requisitionDetailForm.controls['denialReason'].disable();
      this.requisitionDetailForm.controls['approvedBy'].disable();
      this.requisitionDetailForm.controls['updatePrintRequested'].disable();
      this.requisitionDetailForm.controls['isPrinted'].disable();
      this.requisitionDetailForm.controls['requisitionStatusId'].disable();
      this.requisitionDetailForm.controls['priority'].disable();
      this.requisitionDetailForm.controls['requisitionDetailCategoryId'].disable();
      this.requisitionDetailForm.controls['updatePrintCompleted'].disable();
      this.requisitionDetailForm.controls['updatePrintCompletedByUserId'].disable();
      if (this.userSetting.canApproveRequisitions) {
        this.requisitionDetailForm.controls['approved'].enable();
      } else {
        this.requisitionDetailForm.controls['approved'].disable();
      }
      this.requisitionDetailForm.controls['active'].disable();

    }
  }

  /*Combox Filters*/
  materialHandleFilter(value) {
    this.materialFilterList = this.materialList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  reasonHandleFilter(value) {
    this.reasonFilterList = this.reasonList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  statusHandleFilter(value) {
    this.statusFilterList = this.statusList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  venderHandleFilter(value) {
    this.venderFilterList = this.venderList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private reqApprovedHandleFilter(value) {
    this.reqApproveFilterList = this.reqApproveList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private priorityHandleFilter(value) {
    this.reqPriorityFilter = this.reqPriority.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
}
  

