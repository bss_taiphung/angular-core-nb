import {Component, HostListener, OnInit, ViewEncapsulation} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {Observable} from 'rxjs/Observable';
import {FormGroup} from '@angular/forms';

import {HubConnection} from '@aspnet/signalr-client';
import {SortDescriptor, State} from '@progress/kendo-data-query';
import {DataStateChangeEvent, GridDataResult, PageChangeEvent} from '@progress/kendo-angular-grid';

import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {CreateRequisitionDetailDto, IRequisitionDetailDto, RequisitionDetailDto} from './service/requisition.model';
import {RequisitionService} from './service/requisition.service';
import {RequisitionDetailService} from './service/requisitionDetail.service';
import {ToolSharedService} from '../shared/service/toolShared.service';
import {SelectListDto} from '../../shared/models/selectListDto';
import {CreateUsersettingsDto} from '../toolusersettings/service/toolusersettings.model';


import {RequisitionToolBomService} from './service/requisitionToolBOM.service';
import {RequisitionDetailCategoryService} from './service/requisitionDetailCategory.service';
import {ToolUserSettingsService} from '../toolusersettings/service/toolusersettings.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'requisition',
  templateUrl: './html/requisition.component.html',
  styleUrls: ['./css/requisition.component.css'],
  encapsulation: ViewEncapsulation.None
})


export class RequisitionComponent implements OnInit {

  data: IRequisitionDetailDto[];
  formGroup: FormGroup;
  view: Observable<GridDataResult>;
  private editedRowIndex: number;
  editDataItem: CreateRequisitionDetailDto;
  workDataItem: RequisitionDetailDto;
  isNew: boolean;
  requisitionDetailIdToScan: any;
  isScanningWork = false;
  openRequisitionWorkDialog: boolean;
  pageSize = 10;
  openScanToWork = false;
  skip = 0;
  isLoadingUpdatingShowByme = false;
  state: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };
  gridView: GridDataResult;
  deleteDataItem: any;
  sort: SortDescriptor[] = [];
  isactive = true;
  isOrderShownByMe = true;
  searchtext = '';
  private hubConnectionRequisitionSiteCount: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  private requCategory: any[] = [];
  private requFilteredCategory: any[] = [];

  siteList: SelectListDto[];
  private curentSiteId = 0;
  private exteraFilter = 'all';
  mss: any;
  private selectedCategory = null;

  /**
   For requisitionpoup
   */
  private isRequisitionEditPopupActive = false;
  private reqisitionEditModel: RequisitionDetailDto;

  constructor(private toasterService: ToasterHelperService, private commonService: CommonService,
              private requisitionService: RequisitionService,
              private requisitionToolBomService: RequisitionToolBomService,
              private requisitionDetailCategoryService: RequisitionDetailCategoryService,
              private toolUserSettingsService: ToolUserSettingsService,
              private requisitionDetailService: RequisitionDetailService,
              private toolSharedService: ToolSharedService) {
  }

  ngOnInit() {
    this.exteraFilter = 'all';
    this.selectedCategory = null;


  }

  checkFilter(data): boolean {
    let isResult = false;
    if (this.exteraFilter === 'all') {
      isResult = true;
    } else {
      switch (this.exteraFilter.toLowerCase()) {
        case 'notprinted':
          if (data.isPrinted === false && data.approved === true) {
            isResult = true;
          }
          break;
        case 'pending':
          if (data.approved === undefined && data.approved === null) {
            isResult = true;
          }
          break;
        case 'approved':
          if (data.approved === true) {
            isResult = true;
          }
          break;
        case 'rejected':
          if (data.approved === false) {
            isResult = true;
          }
          break;
      }
    }
    if (isResult && this.isOrderShownByMe) {
      if (data.orderedByUserId !== this.toolSharedService.getUserSettings().userId) {
        isResult = false;
      }
    }
    return isResult;
  }

  signalRConnection() {
    /*Signal R*/
    this.hubConnectionRequisitionSiteCount = new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnectionRequisitionSiteCount
      .start()
      .then(() => {

        //console.log('Connection toolting RequisitionSiteCount started!' + 'ToolingManagement' + 'RequisitionDetail' + 'SiteId' + this.curentSiteId);

        this.hubConnectionRequisitionSiteCount
          .invoke('Subscribe', 'ToolingManagement', 'RequisitionDetail', 'SiteId', this.curentSiteId.toString())
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));

    this.hubConnectionRequisitionSiteCount.on('clientOnEntityUpdated',
      (groupName: string,
       operation: string,
       messageJson: string) => {
        const _responseText = messageJson;
        let result200: any = null;
        const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = this.commonService.toCamel(data);

        // result200 = resultData200 ? RequisitionDetailDto.fromJS(resultData200) : new RequisitionDetailDto();
        let dataExist = this.containsObject(result200, this.gridView.data);

        if (operation.toLowerCase() === 'bulkinsert') {
          result200.forEach(itm => {
            dataExist = this.containsObject(itm, this.gridView.data);
            if (!dataExist && this.checkFilter(itm)) {
              this.gridView.data.unshift(itm);
            }

          });
        } else if (operation.toLowerCase() === 'insert' && !dataExist && this.checkFilter(result200)) {
          this.gridView.data.unshift(result200);
        }

        if (operation.toLowerCase() === 'update') {
          this.gridView.data.forEach((element, index) => {
            if (element.requisitionDetailId === result200.requisitionDetailId) {
              if (this.isactive) {
                if (result200.active === this.isactive && this.checkFilter(result200)) {
                  this.gridView.data[index] = result200;
                } else {
                  operation = 'delete';
                }
              } else {
                if (this.checkFilter(result200)) {
                  this.gridView.data[index] = result200;
                } else {
                  operation = 'delete';
                }
              }
            }
          });
        }

        if (operation.toLowerCase() === 'delete' && dataExist) {
          let index = null;
          this.gridView.data.forEach((element, i) => {
            if (element.requisitionDetailId === result200.requisitionDetailId) {
              index = i;
            }
          });
          if (index !== null) {
            this.gridView.data.splice(index, 1);
          }
        }
      });
  }

  onGridDataLoad(type) {
    this.exteraFilter = type;
    this.gridLoading = true;
    this.requisitionDetailService.getAll(
      this.pageSize,
      this.skip,
      this.sort,
      this.isactive,
      this.searchtext,
      this.exteraFilter,
      this.isOrderShownByMe,
      this.selectedCategory)
      .subscribe(eventResult => {
        this.gridView = {
          data: eventResult.results,
          total: eventResult.count
        };
        this.loading = false;
        this.gridLoading = false;
      });

  }

  startScanWork() {
    if (this.requisitionDetailIdToScan) {
      this.isScanningWork = true;
      this.requisitionDetailService.scanToWork(parseInt(this.requisitionDetailIdToScan)).subscribe((result) => {
        if (result) {
          this.requisitionDetailIdToScan = null;
          this.workDataItem = result;
          this.openRequisitionWorkDialog = true;
          this.openScanToWork = false;
          this.isScanFocus = false;
        }
        else {
          this.toasterService.error('Invalid', 'RequsitionDetailId is invalid.');
        }
        this.isScanningWork = false;
      }, (error) => {
        this.toasterService.errorMessage(error);
        this.isScanningWork = false;
      })
    }

  }

  containsObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].requisitionDetailId === obj.requisitionDetailId) {
        return true;
      }
    }

    return false;
  }

  /*private loadGridData(): void {
   this.gridLoading = true;
   this.requisitionService.getAll(this.pageSize, this.skip, this.sort, this.isactive, this.searchtext).subscribe(eventResult => {
     this.gridView = {
       data: eventResult.results,
       total: eventResult.count
     };
     this.loading = false;
     this.gridLoading = false;
   });
    this.loading = false;
    this.gridLoading = false;

  }*/

  // Dialog box
  opened = false;
  loading = true;
  gridLoading = true;
  isScanFocus = false;

  public closeScanToWork() {
    this.openScanToWork = false;
    this.requisitionDetailIdToScan = undefined;
  }

  public scanToWork() {
    debugger;
    this.openScanToWork = true;
    this.isScanFocus = true;
  }

  public close(status) {

    const $this = this;
    if (status) {
      this.gridLoading = true;
      this.requisitionDetailService.delete($this.deleteDataItem.requisitionDetailId,
        response => {
          this.toasterService.success('', 'Requisition Detail Removed Successfully');
          this.gridLoading = false;
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    $this.deleteDataItem = null;
    $this.opened = false;
  }

  public open() {
    this.opened = true;
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.onGridDataLoad(this.exteraFilter);
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
  }

  public addHandler({sender}) {

    //if (!this.eqmSettings.canEditMasterData) {
    //  this.toasterService.error('You do not have permission to add new manufacturer, '');
    //  return false;
    //} else {
    this.closeEditor(sender);
    this.editDataItem = new CreateRequisitionDetailDto();
    this.isNew = true;

    //}
  }

  public editHandler({dataItem}) {
    this.editDataItem = dataItem;
    this.isNew = false;
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public cancelHandler() {
    this.editDataItem = undefined;
    this.formGroup = undefined;
    //   this.onGridDataLoad(this.exteraFilter);
  }

  public removeHandler({dataItem}) {
    this.deleteDataItem = dataItem;
    this.open();
  }

  public onShowActive() {
    this.isactive = !this.isactive;
    this.gridLoading = true;
    this.onGridDataLoad(this.exteraFilter);
  }

  public onShowOrderByMe() {
    if (!this.isLoadingUpdatingShowByme) {
      this.isOrderShownByMe = !this.isOrderShownByMe;
      this.toolUserSetting.showOrderedByMe = this.isOrderShownByMe;
      const data = CreateUsersettingsDto.fromJS(this.toolUserSetting);
      this.isLoadingUpdatingShowByme = true;
      this.toolUserSettingsService.update(data,
        success => {
          this.isLoadingUpdatingShowByme = false;
          this.toolSharedService.getUserSettingById().subscribe(eventResult => {
            this.toolUserSetting = eventResult;
            this.isOrderShownByMe = this.toolUserSetting.showOrderedByMe;
            this.toasterService.success('Update', 'User setting updated successfully.');
          });
        },
        error => {
          this.isLoadingUpdatingShowByme = false;
          this.toasterService.error('Error', 'Error in updating usersetting.');
        });
      this.gridLoading = true;
      this.onGridDataLoad(this.exteraFilter);
    }
  }

  public onSearchKeyup() {
    this.skip = 0;
    this.pageSize = 10;
    this.gridLoading = true;
    this.onGridDataLoad(this.exteraFilter);

  }

  public ActionData: Array<any> = [{
    text: 'Edit',
    icon: 'edit'
  }, {
    text: 'Delete',
    icon: 'trash'
  }];

  public closeRequisitionWorkDialog() {
    this.openRequisitionWorkDialog = false;
    this.workDataItem = undefined;
  }

  public onAction(e, dataItem) {
    if (e === undefined) {
      this.workDataItem = dataItem;
      this.openRequisitionWorkDialog = true;
    } else {
      switch (e.text) {
        case 'Edit':
          this.reqisitionEditModel = dataItem;
          this.isRequisitionEditPopupActive = true;
          break;
        case 'Delete':
          this.deleteDataItem = dataItem;
          this.opened = true;
          break;
      }
    }
  }

  timeout = null;

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridLoading = true;

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.doSearch(state, this);
    }, 1000);
  }

  doSearch(state: any, that) {
    if (state.filter) {
      this.searchtext = that.commonService.getFilter(state.filter.filters, that.isactive);
      this.skip = state.skip;
      this.pageSize = state.take;
      that.onGridDataLoad(this.exteraFilter);
    }
    else {
      that.onGridDataLoad(this.exteraFilter);
    }
  }

  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {

    }
  }

  public siteHandleFilter(filter: any): void {
    this.siteList = this.siteList.filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  public updateCurrentSiteId() {
    this.gridLoading = true;

    //var data = new UserSettingsDto();

    //data.active = true;
    //data.roleId = this.eqmSettings.roleId; //this role should be specific of eqm module, and not general role
    //data.userId = this.user.userId;
    //data.userSettingsId = this.eqmSettings.userSettingsId;
    //data.currentSiteId = this.eqmSettings.currentSiteId;
    ////data.showOnlyMyAssignments = this.isShowOnlyMyAssignments;

    //this.userSettingsService.update(data,
    //  response => {
    //    this.toasterService.success("", 'User Site Updated Successfully');

    //    //update cookies to take new changes
    //    this.eqmSettingsService.userSettings().subscribe();

    //    this.loadGridData();
    //    this.gridLoading = false;
    //  },
    //  error => {debugger;
    //    this.gridLoading = false;
    //    this.toasterService.errorMessage(error);
    //  }
    //);
  }

  private toolUserSetting: any;

  public siteSelectionChange(data): void {
    this.curentSiteId = Number(data);
    this.toolUserSetting = this.toolSharedService.getUserSettings();
    this.curentSiteId = this.toolUserSetting.currentSiteId;
    debugger;
    this.isOrderShownByMe = this.toolUserSetting.showOrderedByMe;
    this.signalRConnection();
    this.fillCategories(true);

  }

  fillCategories(isGridDataLoad: boolean = false): void {
    this.requisitionDetailCategoryService.getDataBySiteId(this.curentSiteId, true,
      sucess => {

        sucess.unshift(new Object({requisitionDetailCategoryName: 'All', requisitionDetailCategoryId: 0}));
        if (this.toolUserSetting.showRequistionCategory) {
          this.selectedCategory = this.toolUserSetting.showRequistionCategory;
        } else {
          this.selectedCategory = 0;
        }

        this.requCategory = this.requFilteredCategory = sucess;
        if (isGridDataLoad) {
          this.onGridDataLoad(this.exteraFilter);
        }

      }, error => {

      });
  }

  onCategoryChange(category: any) {
    if (category !== undefined) {
      this.selectedCategory = category.requisitionDetailCategoryId;
      this.onGridDataLoad(this.exteraFilter);
    } else {
      this.selectedCategory = this.selectedCategory;
    }
  }

  private categoryHandleFilter(value) {
    this.requFilteredCategory = this.requCategory.filter((s) => s.requisitionDetailCategoryName.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  /**
   For requisitionpoup
   */
  onCalncelRequisitionEdit() {
    this.isRequisitionEditPopupActive = false;
    this.reqisitionEditModel = undefined;
  }
}
