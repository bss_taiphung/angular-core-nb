import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {FormGroup} from '@angular/forms';

import {CommonService} from '../../../shared/services/common.service';
import {ToasterHelperService} from '../../../shared/services/toasterHelper.service';
import {ToolUserSettingsService} from '../../toolusersettings/service/toolusersettings.service';
import {SelectListDto} from '../../../shared/models/selectListDto'
import {CreateUsersettingsDto, UserSettingDto} from '../../toolusersettings/service/toolusersettings.model';
import {ToolSharedService} from '../service/toolShared.service';

@Component({
  selector: 'grid-header-tool',
  templateUrl: './html/site.component.html',
  encapsulation: ViewEncapsulation.None,
})

export class ToolSiteComponent implements OnInit {

  public formGroup: FormGroup;

  currentSiteId: any;

  siteList: SelectListDto[];
  siteListVm: SelectListDto[];

  siteSaving = false;
  mss: any;
  helpLink: string;
  page: string;

  currentToolUserSetting: UserSettingDto;

  @Input()
  public set link(data: any) {
    if (data) {
      this.helpLink = data;
    }
  }

  @Input()
  public set pageName(data: any) {
    if (data) {
      this.page = data;
    }
  }

  @Output() siteSelectionChange: EventEmitter<any> = new EventEmitter();

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private toolUserSettingsService: ToolUserSettingsService,
              private toolSharedService: ToolSharedService) {

  }

  getSiteList() {
    this.commonService.getAll('General', 'Site').subscribe(eventResult => {
      this.siteListVm = eventResult.results;
      this.siteList = this.siteListVm = eventResult.results;
      this.currentSiteId = this.currentToolUserSetting.currentSiteId;
      this.siteSelectionChange.emit(this.currentSiteId);
    });
  }

  ngOnInit() {
    this.currentToolUserSetting = this.toolSharedService.getUserSettings();
    if (this.currentToolUserSetting) {
      this.getSiteList();
    } else {
      this.toolSharedService.getUserSettingById().subscribe(eventResult => {
        this.currentToolUserSetting = eventResult;
        this.getSiteList();
      });
    }
    this.currentToolUserSetting = this.toolSharedService.getUserSettings();
  }

  public siteHandleFilter(filter: any): void {
    this.siteList = this.siteListVm.filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  public updateCurrentSiteId() {
    let data = new CreateUsersettingsDto();
    data.currentSiteId = this.currentToolUserSetting.currentSiteId;
    data.userSettingId = this.currentToolUserSetting.userSettingId;
    this.toolSharedService.updateCurrentSiteId(data,
      response => {
        this.currentSiteId = this.currentToolUserSetting.currentSiteId;
        this.toasterService.success('', 'User Site Updated Successfully');
        this.siteSaving = false;
        this.toolSharedService.getUserSettingById().subscribe(eventResult => {
          this.currentToolUserSetting = eventResult;
          this.siteSelectionChange.emit(this.currentSiteId);
        });
      },
      error => {
        this.toasterService.errorMessage(error);
        this.siteSaving = false;
      }
    );
  }

  public onSiteSelectionChange(changedSiteId: any): void {
    if (changedSiteId) {
      this.siteSaving = true;
      this.currentToolUserSetting.currentSiteId = changedSiteId.value;
      this.updateCurrentSiteId();
    }
  }
}
