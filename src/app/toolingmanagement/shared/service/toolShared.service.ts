import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {CreateUsersettingsDto, UserSettingDto} from '../../toolusersettings/service/toolusersettings.model';
import {CommonService} from '../../../shared/services/common.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class ToolSharedService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService, private http: HttpClient) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/ToolingManagement/api/UserSetting/';
  }

  getUserSettingById(): Observable<any> {
    const url = this.apiBaseUrl + 'GetUserSetting';

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      this.setUserSettings(this.commonService.toCamel(response));
      return Observable.of(response);
    });
  }

  updateCurrentSiteId(input: CreateUsersettingsDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'UpdateCurrentSiteId';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  setUserSettings(userSetting) {
    this.commonService.setCookies('toolUserSetting', JSON.stringify(UserSettingDto.fromJS(userSetting)));
  }

  getUserSettings(): any {
    const usersetting = this.commonService.getCookies('toolUserSetting');
    if (usersetting === '') {
      return undefined;
    }
    return JSON.parse(usersetting);
  }
}

