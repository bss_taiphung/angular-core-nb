import {RouterModule, Routes} from '@angular/router';

import {AuthGuard} from '../shared/guards/auth.guard';
import {RoleComponent} from './role/role.component';

import {ToolingManagementComponent} from './toolingmanagement.component';
import {RequisitionComponent} from './requisition/requisition.component';
import {ToolusersettingsComponent} from './toolusersettings/toolusersettings.component';
import {RequisitionMaterialComponent} from './requisitionMaterial/requisitionMaterial.component';
import {RequisitionStatusComponent} from './requisitionStatus/requisitionStatus.component';
import {RequisitionReasonComponent} from './requisitionReason/requisitionReason.component';
import {UpdatePrintComponent} from './updatePrints/update-print.component';

export const routes: Routes = [
  {
    path: '', component: ToolingManagementComponent,
    canActivate: [AuthGuard],
    children: [
      {path: 'requisition', component: RequisitionComponent, canActivate: [AuthGuard]},
      {path: 'updatePrint', component: UpdatePrintComponent, canActivate: [AuthGuard]},
      {path: 'role', component: RoleComponent, canActivate: [AuthGuard]},
      {path: 'userSetting', component: ToolusersettingsComponent, canActivate: [AuthGuard]},
      {path: 'material', component: RequisitionMaterialComponent, canActivate: [AuthGuard]},
      {path: 'status', component: RequisitionStatusComponent, canActivate: [AuthGuard]},
      {path: 'reason', component: RequisitionReasonComponent, canActivate: [AuthGuard]}
    ]
  }
];

export const routing = RouterModule.forChild(routes);
