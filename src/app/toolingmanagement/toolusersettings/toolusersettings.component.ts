import { Component, Inject, ViewEncapsulation, OnInit,HostListener } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import "rxjs/add/observable/fromEvent";
import { Observable } from 'rxjs/Observable';
import { process, GroupDescriptor, State, aggregateBy } from '@progress/kendo-data-query';
import { PageChangeEvent, GridComponent, GridDataResult, DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { ToolUserSettingsService } from "./service/toolusersettings.service";
import { SortDescriptor, orderBy } from '@progress/kendo-data-query';
import { PagedResultDtoOfUsersettingsDto, UserSettingDto, CreateUsersettingsDto, IUserSettingDto } from "./service/toolusersettings.model";

import { HubConnection } from '@aspnet/signalr-client';
import { CommonService } from "../../shared/services/common.service";
import { environment } from '../../../environments/environment';
import { ToasterHelperService } from "../../shared/services/toasterHelper.service";

import { SelectListDto } from "../../shared/models/selectListDto"


@Component({
  selector: "toolUserSettings",
  templateUrl: "./html/toolusersettings.component.html",
  styleUrls: ["./css/toolusersettings.component.css"],
  encapsulation: ViewEncapsulation.None
})

export class ToolusersettingsComponent implements OnInit {

  data: IUserSettingDto[];
  public formGroup: FormGroup;
  public view: Observable<GridDataResult>;
  private editedRowIndex: number;
  public editDataItem: CreateUsersettingsDto;
  public isNew: boolean;
  public isDuplicateMember: boolean =false;
  public pageSize = 10;
  public skip = 0;
  public state: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{ field: '', operator: '', value: '' }]
    }
  };
  public gridView: GridDataResult;
  public deleteDataItem: any;
  private sort: SortDescriptor[] = [];
  public isactive: boolean = true;
  public searchtext: string = '';

  private hubConnection: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  //eqmSettings: EqmSettings = new EqmSettings();
  //user: User = new User();
  //userRole: UserRole = new UserRole();
  public siteList: SelectListDto[];
  public mss: any;

  constructor(private toasterService: ToasterHelperService,
    private commonService: CommonService,
    private userSettingsService: ToolUserSettingsService) {
    this.mss = this.commonService.getCookies("Mss");
    //this.eqmSettings = this.eqmSettingsService.getEqmSettings();
    //this.user = this.eqmSettingsService.getUser();
    //this.userRole = this.eqmSettingsService.getUserRoles();
  }

  ngOnInit() {
    this.loadGridData();
  }

  containsObject(obj, list) {
    var x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].statusId === obj.statusId) {
        return true;
      }
    }

    return false;
  }

  // Dialog box 
  opened: boolean = false;
  loading: boolean = true;
  gridLoading: boolean = true;

  public close(status) {
    var $this = this;
    if (status) {
      this.gridLoading = true;
      this.userSettingsService.delete($this.deleteDataItem.userSettingsId,
        response => {
          this.toasterService.success("", 'User Settings removed Successfully');
          this.userSettingformFlag = false;
          this.loadGridData();
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    $this.deleteDataItem = null;
    $this.opened = false;
  }

  public open() {
    this.opened = true;
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.loadGridData();
  }

  protected pageChange({ skip, take }: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
    this.loadGridData();
  }

  private loadGridData(): void {
    this.userSettingsService.getAll(this.pageSize, this.skip, this.sort, this.isactive, this.searchtext).subscribe(eventResult => {
      this.gridView = {
        data: eventResult.results,
        total: eventResult.count
      };
      this.loading = false;
      this.gridLoading = false;
    });
  }

  public addHandler({ sender }) {
    
      this.closeEditor(sender);
      this.editDataItem = new CreateUsersettingsDto();
      this.isNew = true;
  }

  public editHandler({ dataItem }) {

    this.editDataItem = dataItem;
    this.isNew = false;
  }

  public userSettingformFlag: boolean = false;
  public saveHandler(data) {
    this.gridLoading = true;
    if (this.isNew) {
      data.active = (data.active == null) ? false : data.active;
      const inputData: CreateUsersettingsDto = data;
      inputData.currentSiteId = null;
      
      this.userSettingsService.create(inputData,
        response => {
          if (response == false) {
            this.gridLoading = false;
            this.isNew = true; this.userSettingformFlag = true; this.isDuplicateMember = true; return false;
          }
          if (response != null) {
            this.toasterService.success("", 'User Settings Saved Successfully');
            this.loadGridData();
            this.gridLoading = false;
            this.userSettingformFlag = false;
          }
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
          console.log(error);
        }
      );
    }
    else {
      const inputData: CreateUsersettingsDto = data;
      this.userSettingsService.update(inputData,
        response => {
          
          this.toasterService.success("", 'User Settings Updated Successfully');
          this.loadGridData();
          this.gridLoading = false;
          this.userSettingformFlag = false;
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public cancelHandler() {
    this.editDataItem = undefined;
    this.formGroup = undefined;
  }

  public removeHandler({ dataItem }) {
    this.deleteDataItem = dataItem;
    this.open();
  }

  public showActive() {
    this.isactive = !this.isactive;
    this.gridLoading = true;
    this.loadGridData();
  }

  public onSearchKeyup() {
    this.skip = 0;
    this.pageSize = 10;
    this.gridLoading = true;
    this.loadGridData();
  }

  public ActionData: Array<any> = [{
    text: 'Delete',
    icon: 'trash'
  }];

  public onAction(e, dataItem) {
    if (e === undefined) {
      //if (!this.eqmSettings.canEditMasterData) {
      //  this.toasterService.error('You do not have permission to update status.', '');
      //  return false;
      //} else {
        this.editDataItem = dataItem;
        this.isNew = false;
      //}
    } else {
      if (e.text === "Delete") {
        //if (!this.eqmSettings.canEditMasterData) {
        //  this.toasterService.error('You do not have permission to delete status.', '');
        //  return false;
        //} else {
          this.deleteDataItem = dataItem;
          this.open();
        //}
      }
    }
  }

 

  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {
      this.cancelHandler();
      this.close(false);
      this.userSettingformFlag = false;
    }
  }

  timeout = null;
  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridLoading = true;

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => { this.doSearch(state, this); }, 1000);
  }

  doSearch(state: any, that) {
    if (state.filter) {
      var search = that.commonService.getFilter(state.filter.filters, that.isactive);

      that.userSettingsService.getAllWtihFilter(state.take, state.skip, that.sort, search)
        .subscribe(eventResult => {
          that.gridView = {
            data: eventResult.results,
            total: eventResult.count
          };
          that.loading = false;
          that.gridLoading = false;
        });

      this.skip = state.skip;
      this.pageSize = state.take;
    }
    else {
      that.loadGridData();
    }
  }

  public siteSelectionChange(e): void {
    this.gridLoading = true;
    this.loadGridData();
  }
}
