"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CreateUsersettingsDto = /** @class */ (function () {
    function CreateUsersettingsDto(data) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    this[property] = data[property];
            }
        }
    }
    CreateUsersettingsDto.prototype.init = function (data) {
        if (data) {
            this.userSettingId = data["userSettingId"];
            this.userId = data["userId"];
            this.roleId = data["roleId"];
            this.currentSiteId = data["currentSiteId"];
            this.createdDateTime = data["createdDateTime"];
            this.modifiedDateTime = data["modifiedDateTime"];
            this.createdBy = data["createdBy"];
            this.modifiedBy = data["modifiedBy"];
            this.active = data["active"];
            this.showOrderedByMe = data["showOrderedByMe"];
            this.showRequistionCategory = data["showRequistionCategory"];
        }
    };
    CreateUsersettingsDto.fromJS = function (data) {
        var result = new CreateUsersettingsDto();
        result.init(data);
        return result;
    };
    CreateUsersettingsDto.prototype.toJSON = function (data) {
        data = typeof data === 'object' ? data : {};
        data["userSettingId"] = this.userSettingId;
        data["userId"] = this.userId;
        data["roleId"] = this.roleId;
        data["currentSiteId"] = this.currentSiteId;
        data["createdDateTime"] = this.createdDateTime;
        data["createdDateTime"] = this.createdDateTime;
        data["modifiedDateTime"] = this.modifiedDateTime;
        data["createdBy"] = this.createdBy;
        data["modifiedBy"] = this.modifiedBy;
        data["active"] = this.active;
        data["showOrderedByMe"] = this.showOrderedByMe;
        data["showRequistionCategory"] = this.showRequistionCategory;
        return data;
    };
    return CreateUsersettingsDto;
}());
exports.CreateUsersettingsDto = CreateUsersettingsDto;
var UserSettingDto = /** @class */ (function () {
    function UserSettingDto(data) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    this[property] = data[property];
            }
        }
    }
    UserSettingDto.prototype.init = function (data) {
        if (data) {
            if (data) {
                this.userSettingId = data["userSettingId"];
                this.userId = data["userId"];
                this.roleId = data["roleId"];
                this.currentSiteId = data["currentSiteId"];
                this.showOrderedByMe = data["showOrderedByMe"];
                this.showRequistionCategory = data["showRequistionCategory"];
                this.active = data["active"];
                this.siteName = data["siteName"];
                this.userDisplayName = data["userDisplayName"];
                this.roleName = data["roleName"];
            }
        }
    };
    UserSettingDto.fromJS = function (data) {
        var result = new UserSettingDto();
        result.init(data);
        return result;
    };
    UserSettingDto.prototype.toJSON = function (data) {
        data = typeof data === 'object' ? data : {};
        data["userSettingId"] = this.userSettingId;
        data["userId"] = this.userId;
        data["roleId"] = this.roleId;
        data["currentSiteId"] = this.currentSiteId;
        data["showOrderedByMe"] = this.showOrderedByMe;
        data["showRequistionCategory"] = this.showRequistionCategory;
        data["active"] = this.active;
        data["siteName"] = this.siteName;
        data["userDisplayName"] = this.userDisplayName;
        data["roleName"] = this.roleName;
        return data;
    };
    return UserSettingDto;
}());
exports.UserSettingDto = UserSettingDto;
var PagedResultDtoOfUsersettingsDto = /** @class */ (function () {
    function PagedResultDtoOfUsersettingsDto(data) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    this[property] = data[property];
            }
        }
    }
    PagedResultDtoOfUsersettingsDto.prototype.init = function (data) {
        if (data && data != null) {
            if (data["results"] && data["results"].constructor === Array) {
                this.results = [];
                for (var _i = 0, _a = data["results"]; _i < _a.length; _i++) {
                    var item = _a[_i];
                    this.results.push(UserSettingDto.fromJS(item));
                }
            }
            this.count = data["count"];
        }
    };
    PagedResultDtoOfUsersettingsDto.fromJS = function (data) {
        var result = new PagedResultDtoOfUsersettingsDto();
        result.init(data);
        return result;
    };
    PagedResultDtoOfUsersettingsDto.prototype.toJSON = function (data) {
        data = typeof data === 'object' ? data : {};
        if (this.results && this.results.constructor === Array) {
            data["results"] = [];
            for (var _i = 0, _a = this.results; _i < _a.length; _i++) {
                var item = _a[_i];
                data["results"].push(item.toJSON());
            }
        }
        if (this.count) {
            data["count"] = this.count;
        }
        return data;
    };
    return PagedResultDtoOfUsersettingsDto;
}());
exports.PagedResultDtoOfUsersettingsDto = PagedResultDtoOfUsersettingsDto;
//# sourceMappingURL=toolusersettings.model.js.map