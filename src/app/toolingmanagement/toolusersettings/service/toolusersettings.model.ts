export interface ICreateUsersettingsDto {
  userSettingId: number;
  userId: number;
  roleId: number;
  currentSiteId: number;
  createdBy: number;
  createdDateTime: string;
  modifiedBy: number;
  modifiedDateTime: string;
  showOrderedByMe: number;
  showRequistionCategory: number;
  active: boolean;

}

export class CreateUsersettingsDto implements ICreateUsersettingsDto {
  userSettingId: number;
  userId: number;
  roleId: number;
  currentSiteId: number;
  createdBy: number;
  createdDateTime: string;
  modifiedBy: number;
  modifiedDateTime: string;
  showOrderedByMe: number;
  showRequistionCategory: number;
  active: boolean;
  constructor(data?: ICreateUsersettingsDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.userSettingId = data["userSettingId"];
      this.userId = data["userId"];
      this.roleId = data["roleId"];
      this.currentSiteId = data["currentSiteId"];
      this.createdDateTime = data["createdDateTime"];
      this.modifiedDateTime = data["modifiedDateTime"];
      this.createdBy = data["createdBy"];
      this.modifiedBy = data["modifiedBy"];
      this.active = data["active"];
      this.showOrderedByMe = data["showOrderedByMe"];
      this.showRequistionCategory = data["showRequistionCategory"];

    }
  }

  static fromJS(data: any): CreateUsersettingsDto {
    let result = new CreateUsersettingsDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data["userSettingId"] = this.userSettingId;
    data["userId"] = this.userId;
    data["roleId"] = this.roleId;
    data["currentSiteId"] = this.currentSiteId;
    data["createdDateTime"] = this.createdDateTime;
    data["createdDateTime"] = this.createdDateTime;
    data["modifiedDateTime"] = this.modifiedDateTime;
    data["createdBy"] = this.createdBy;
    data["modifiedBy"] = this.modifiedBy;
    data["active"] = this.active;
    data["showOrderedByMe"] = this.showOrderedByMe;
    data["showRequistionCategory"] = this.showRequistionCategory;

    return data;
  }
}

/*User settings*/
export interface IUserSettingDto {
  userSettingId: number;
  userId: number;
  roleId: number;
  currentSiteId: number;
  showOrderedByMe: boolean;
  showRequistionCategory: number;
  active: boolean;
  siteName: string;
  userDisplayName: string;
  roleName: string;
  canToggleSites: number; /*To get role permission*/
  canApproveRequisitions: boolean;
  canLockRequisitions: boolean;
  receiveSupervisorNotifications: boolean;
  isEngineer: boolean;
  canEditRequisitions: boolean;
}

export class UserSettingDto implements IUserSettingDto {
  userSettingId: number;
  userId: number;
  roleId: number;
  currentSiteId: number;
  showOrderedByMe: boolean;
  showRequistionCategory: number;
  active: boolean;
  siteName: string;
  userDisplayName: string;
  roleName: string;
  canToggleSites: number;
  canApproveRequisitions: boolean;
  canLockRequisitions: boolean;
  receiveSupervisorNotifications: boolean;
  isEngineer: boolean;
  canEditRequisitions: boolean;
  constructor(data?: IUserSettingDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (data) {
        this.userSettingId = data["userSettingId"];
        this.userId = data["userId"];
        this.roleId = data["roleId"];
        this.currentSiteId = data["currentSiteId"];
        this.showOrderedByMe = data["showOrderedByMe"];
        this.showRequistionCategory = data["showRequistionCategory"];
        this.active = data["active"];
        this.siteName = data["siteName"];
        this.userDisplayName = data["userDisplayName"];
        this.roleName = data["roleName"];
        this.canToggleSites = data["canToggleSites"];
        this.canApproveRequisitions = data["canApproveRequisitions"];
        this.canLockRequisitions = data["canLockRequisitions"];
        this.receiveSupervisorNotifications = data["receiveSupervisorNotifications"];
        this.isEngineer = data["isEngineer"];
        this.canEditRequisitions = data["canEditRequisitions"];
      }
    }
  }

  static fromJS(data: any): UserSettingDto {
    let result = new UserSettingDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["userSettingId"] = this.userSettingId;
    data["userId"] = this.userId;
    data["roleId"] = this.roleId;
    data["currentSiteId"] = this.currentSiteId;
    data["showOrderedByMe"] = this.showOrderedByMe;
    data["showRequistionCategory"] = this.showRequistionCategory;
    data["active"] = this.active;
    data["siteName"] = this.siteName;
    data["userDisplayName"]=this.userDisplayName;
    data["roleName"] = this.roleName;
    data["canToggleSites"] = this.canToggleSites;
    data["canApproveRequisitions"] = this.canApproveRequisitions;
    data["canLockRequisitions"] = this.canLockRequisitions;
    data["receiveSupervisorNotifications"] = this.receiveSupervisorNotifications;
    data["isEngineer"] = this.isEngineer;
    data["canEditRequisitions"] = this.canEditRequisitions;
    return data;
  }
}


export class PagedResultDtoOfUsersettingsDto implements IPagedResultDtoOfUsersettingsDto {
  results: UserSettingDto[];
  count: number;

  constructor(data?: IPagedResultDtoOfUsersettingsDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data["results"] && data["results"].constructor === Array) {
        this.results = [];
        for (let item of data["results"])
          this.results.push(UserSettingDto.fromJS(item));
      }
      this.count = data["count"];
    }
  }

  static fromJS(data: any): PagedResultDtoOfUsersettingsDto {
    let result = new PagedResultDtoOfUsersettingsDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data["results"] = [];
      for (let item of this.results)
        data["results"].push(item.toJSON());
    }
    if (this.count) {
      data["count"] = this.count;
    }
    return data;
  }
}

export interface IPagedResultDtoOfUsersettingsDto {
  results: UserSettingDto[];
  count: number;
}

