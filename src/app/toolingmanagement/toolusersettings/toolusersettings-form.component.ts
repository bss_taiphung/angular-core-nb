import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {ToolUserSettingsService} from './service/toolusersettings.service'
import 'rxjs/add/observable/fromEvent';
import {CommonService} from '../../shared/services/common.service';
import {SelectListDto} from '../../shared/models/selectListDto';
import {RequisitionDetailCategoryService} from '../requisition/service/requisitionDetailCategory.service';

@Component({
  selector: 'kendo-grid-toolUsersettings-form',
  templateUrl: './html/toolusersettings-form.component.html'
})
export class ToolusersettingsFormComponent {
  public active = false;


  public editForm: FormGroup = new FormGroup({
    'userSettingId': new FormControl(),
    'userId': new FormControl('', Validators.required),
    'roleId': new FormControl('', Validators.required),
    'currentSiteId': new FormControl('', Validators.required),
    'showOrderedByMe': new FormControl(''),
    'showRequistionCategory': new FormControl(''),
    'active': new FormControl(false),
  });

  @Input() public isNew = false;

  @Input() public isDuplicateMember = false;

  @Input() public userSettingformFlag = false;


  public username: string;
  public currentSiteId = 1;
  public userIdList: SelectListDto[];
  public filteruserIDList: SelectListDto[];

  public currentSiteIdList: SelectListDto[];
  public filtercurrentSiteIdList: SelectListDto[];

  public filterroleIdList: SelectListDto[];
  public roleIdList: SelectListDto[];

  private filterCategoryList: any[] = [];
  private categoryList: any[] = [];

  constructor(private commonService: CommonService,
              private userSettingsService: ToolUserSettingsService,
              private requisitionDetailCategoryService: RequisitionDetailCategoryService) {

  }

  @Input()
  public set model(userSettings: any) {
    if (userSettings !== undefined && Object.keys(userSettings).length > 0) {
      this.editForm.reset(userSettings);
      this.active = true;
      this.isDuplicateMember = false;
      this.userSettingformFlag = true;

      if (this.isNew) {
        this.editForm.get('currentSiteId').clearValidators();
      }
      this.requisitionDetailCategoryService.getDataBySiteId(userSettings.currentSiteId, true, sucess => {
        this.filterCategoryList = this.categoryList = sucess;
      }, error => {

      });

      this.loadAllDropDowns();

    }
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<any> = new EventEmitter();


  public loadAllDropDowns() {

    this.commonService.getAllWithId(this.currentSiteId, 'general', 'User').subscribe(eventResult => {
      this.filteruserIDList = this.userIdList = eventResult.results;
    });

    this.commonService.getAll('ToolingManagement', 'Role').subscribe(eventResult => {
      this.filterroleIdList = this.roleIdList = eventResult.results;
    });
    this.commonService.getAll('General', 'Site').subscribe(eventResult => {
      this.filtercurrentSiteIdList = this.currentSiteIdList = eventResult.results;
    });
  }

  onSiteChange(value) {
    if (value.value !== undefined && value.value > 0) {
      this.requisitionDetailCategoryService.getDataBySiteId(value.value, true, sucess => {
        this.filterCategoryList = this.categoryList = sucess;
      }, error => {

      });
    }

  }

  public onSave(e): void {
    e.preventDefault();
    this.save.emit(this.editForm.value);
    this.closeForm();
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
    this.isDuplicateMember = false;
  }

  private closeForm(): void {
    this.userSettingformFlag = false;
    this.cancel.emit();
  }

  private userIDListHandleFilter(value) {
    this.filteruserIDList = this.userIdList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private roleIdListHandleFilter(value) {
    this.filterroleIdList = this.roleIdList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private currentSiteIdListHandleFilter(value) {
    this.filtercurrentSiteIdList = this.currentSiteIdList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private requistionCategoryListHandleFilter(value) {
    this.filterCategoryList = this.categoryList
      .filter((s) => s.requisitionDetailCategoryName.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
}
