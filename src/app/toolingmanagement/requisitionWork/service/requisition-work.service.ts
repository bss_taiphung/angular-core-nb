import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

import {CommonService} from '../../../shared/services/common.service';
import {ToasterHelperService} from '../../../shared/services/toasterHelper.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class RequisitionWorkService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService, private http: HttpClient,
              private toasterService: ToasterHelperService) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/ToolingManagement/api/RequisitionDetailProcess/';
  }


  update(data: any): any {
    const baseUrl = environment.bllApiBaseAddress + '/ToolingManagement/api/Requisition/';
    const url = baseUrl + 'WorkRequisitionDetail';

    return this.http.post(url, data);

  }

  getRequisitionDetailProcess(requisitionDetailId: any): any {
    const url = this.apiBaseUrl + 'GenerateRequisitionDetailProcess';
    const params = new HttpParams()
      .set('requisitionDetailId', requisitionDetailId)

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };
    return this.http.get(url, options);
  }

}

