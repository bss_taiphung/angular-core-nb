import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import 'rxjs/add/observable/fromEvent';

import {HubConnection} from '@aspnet/signalr-client';
import {GridDataResult, PageChangeEvent} from '@progress/kendo-angular-grid';
import {SortDescriptor, State} from '@progress/kendo-data-query';

import {SelectListDto} from '../../shared/models/selectListDto';
import {EqmSettings, User, UserRole} from '../../shared/models/eqmSettings';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {CommonService} from '../../shared/services/common.service';
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';

import {UserSettingsService} from '../../eqmaintenance/eqmusersettings/service/eqmusersettings.service';
import {EquipmentService} from '../../eqmaintenance/equipment/service/equipment.service';
import {RequisitionWorkService} from './service/requisition-work.service';
import {RequisitionDetailDto} from '../requisition/service/requisition.model';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'requisition-work-dialog',
  styles: [
    'input[type=text] { width: 100%; }'
  ],
  templateUrl: './html/requisition-work.component.html'
})
export class RequisitionWorkComponent implements OnInit {
  @Input() public openRequisitionWorkDialog = false;
  eqmSettings: EqmSettings = new EqmSettings();
  user: User = new User();
  requisitionDetailProcess: any[] = [];
  public requisitionDetailDto: RequisitionDetailDto;
  userRole: UserRole = new UserRole();
  private sort: SortDescriptor[] = [];
  public isSubmitted = false;
  processList: SelectListDto[] = [];
  processFilterList: SelectListDto[] = [];
  xferToFilterList: SelectListDto[] = [];
  transactionTypeList: any = [];
  transactionTypeFilterList: any = [];
  loading = true;
  isactive = true;
  private hubConnectionRequisitionWork: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  public gridView: GridDataResult;
  public pageSize = 10;
  public showXferTo = false;
  public skip = 0;
  gridLoading = true;
  public state: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };

  public editForm: FormGroup = new FormGroup({
    'quantity': new FormControl('', Validators.required),
    'transactionType': new FormControl('', Validators.required),
    'processId': new FormControl(),
    'transferToProcessId': new FormControl(),
  });

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private requisitionWorkService: RequisitionWorkService,
              private userSettingsService: UserSettingsService,
              private equipmentService: EquipmentService,
              private formBuilder: FormBuilder,
              private router: Router) {
    this.eqmSettings = this.eqmSettingsService.getEqmSettings();
    this.user = this.eqmSettingsService.getUser();
    this.userRole = this.eqmSettingsService.getUserRoles();
    this.commonService.validateLogin();
    this.transactionTypeFilterList = this.transactionTypeList = [{'text': '', value: ''}, {
      'text': 'Transfer',
      value: 'Transfer'
    }, {'text': 'Complete', value: 'Complete'}];
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();

  ngOnInit() {

  }

  @Input()
  public set model(requisitionDetailDto: RequisitionDetailDto) {
    if (requisitionDetailDto !== undefined) {
      this.requisitionDetailDto = requisitionDetailDto;
      if (this.requisitionDetailDto) {
        this.getRequisitionDetailProcess(this.requisitionDetailDto.requisitionDetailId);
        this.getProcessList();
        this.signalRConnection();
      }
    }
  }

  signalRConnection() {
    this.hubConnectionRequisitionWork =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnectionRequisitionWork
      .start()
      .then(() => {
        console.log('Connection toolting RequisitionWork started!');

        this.hubConnectionRequisitionWork
          .invoke('Subscribe', 'ToolingManagement', 'RequisitionDetailProcess', 'RequisitionDetailId', this.requisitionDetailDto.requisitionDetailId.toString())
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));

    this.hubConnectionRequisitionWork.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
      const _responseText = messageJson;
      let result200: any = null;
      const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      result200 = this.commonService.toCamel(data);

      // result200 = resultData200 ? RequisitionDetailDto.fromJS(resultData200) : new RequisitionDetailDto();
      const dataExist = this.containsObject(result200, this.gridView.data);

      if (operation.toLowerCase() === 'insert' && !dataExist) {
        this.gridView.data.unshift(result200);
        if (result200.quantityCompleted > 0) {
          this.requisitionDetailDto.quantityCompleted += result200.quantityCompleted;
        }
      }

      if (operation.toLowerCase() === 'update') {

        this.gridView.data.forEach((element, index) => {
          if (element.requisitionDetailProcessId === result200.requisitionDetailProcessId) {
            //
            if (this.isactive) {
              if (result200.active === this.isactive) {
                this.gridView.data[index] = result200;
                if (result200.quantityCompleted > 0) {
                  this.requisitionDetailDto.quantityCompleted -= element.quantityCompleted;
                  this.requisitionDetailDto.quantityCompleted += result200.quantityCompleted;
                }
              } else {
                operation = 'delete';
              }
            } else {
              this.gridView.data[index] = result200;
              if (result200.quantityCompleted > 0) {
                this.requisitionDetailDto.quantityCompleted -= element.quantityCompleted;
                this.requisitionDetailDto.quantityCompleted += result200.quantityCompleted;
              }
            }
          }
        });

      }

      if (operation.toLowerCase() === 'delete' && dataExist) {
        let index = null;
        this.gridView.data.forEach((element, i) => {
          if (element.requisitionDetailProcessId === result200.requisitionDetailProcessId) {
            index = i;
          }
        });
        if (index !== null) {
          if (result200.quantityCompleted > 0) {
            this.requisitionDetailDto.quantityCompleted -= result200.quantityCompleted;
          }
          this.gridView.data.splice(index, 1);
        }
      }
    });
  }

  containsObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].requisitionDetailProcessId === obj.requisitionDetailProcessId) {
        return true;
      }
    }

    return false;
  }

  public loadXferTo(value: any) {
    if (this.editForm.value.transactionType === 'Transfer') {
      this.showXferTo = true;
    }
    else {
      this.showXferTo = false;
    }

  }

  public getRequisitionDetailProcess(requisitionDetailId) {
    this.gridView = null;
    this.requisitionWorkService.getRequisitionDetailProcess(requisitionDetailId)
      .subscribe(result => {
        let completedTasks = 0;
        if (result !== null && result !== undefined) {
          result.forEach((data) => {
            completedTasks += data.quantityCompleted;
          });
        }
        if (completedTasks > 0) {
          this.requisitionDetailDto.quantityCompleted = completedTasks;
        }
        this.requisitionDetailProcess = result;
        this.gridView = {
          data: this.requisitionDetailProcess.slice(this.skip, this.skip + this.pageSize),
          total: this.requisitionDetailProcess.length
        };
        this.loading = false;
        this.gridLoading = false;
      });
    ;
  }

  getProcessList() {
    let dept = this.editForm.value.department;
    if (!dept) {
      dept = -1;
    }
    this.commonService.getAll('ProductionSupport', 'Process').subscribe(eventResult => {
      const data = eventResult.results;
      data.unshift(new SelectListDto({text: '', value: '', isSelected: false}));
      this.xferToFilterList = this.processFilterList = this.processList = data;
    });
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.getRequisitionDetailProcess(this.requisitionDetailDto.requisitionDetailId);

  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
    this.getRequisitionDetailProcess(this.requisitionDetailDto.requisitionDetailId);

  }

  public saveHandler() {
    this.loading = true;
    this.isSubmitted = true;
    let data: any = {}
    data = this.editForm.value;
    data.requisitionDetailId = this.requisitionDetailDto.requisitionDetailId;
    data.createdBy = this.user.userId;
    this.requisitionWorkService.update(data).subscribe(
      (result) => {
        this.toasterService.success('', 'Operation has done successfully');
        this.isSubmitted = false;
        this.cancelForm(0);
      },
      (error) => {
        this.toasterService.errorMessage(error);
        this.isSubmitted = false;

      }
    );

  }

  private closeForm(): void {
    this.openRequisitionWorkDialog = false;
    this.hubConnectionRequisitionWork.stop();
    this.cancel.emit();
  }

  public onCancel(value: any) {
    this.openRequisitionWorkDialog = false;
    this.cancel.emit();
  }

  public cancelForm(value: any) {
    this.editForm.controls['quantity'].setValue(0);
    this.editForm.controls['transactionType'].setValue('');
    this.editForm.controls['processId'].setValue('');
    this.editForm.controls['transferToProcessId'].setValue('');
    this.showXferTo = false;
    // this.editForm.reset();
  }

  transtypeHandleFilter(value) {
    this.transactionTypeFilterList = this.transactionTypeList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  xferToHandleFilter(value) {
    this.xferToFilterList = this.processList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  processHandleFilter(value) {
    this.processFilterList = this.processList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
}
