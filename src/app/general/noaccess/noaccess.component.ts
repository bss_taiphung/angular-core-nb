import {
  Component,
  OnInit
} from '@angular/core';

@Component({
  selector: 'app-noaccess',
  templateUrl: './noaccess.component.html',
  styleUrls: ['./noaccess.component.scss']
})
export class NoAccessComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
