import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {UnauthorizedComponent} from './unauthorized/unauthorized.component';
import {NotificationsComponent} from './notifications/notifications.component';
import {MessagesComponent} from './messages/messages.component';
import {AuthGuard} from '../shared/guards/auth.guard'
import {GeneralComponent} from './general.component';
import {NoAccessComponent} from './noaccess/noaccess.component';

export const routes: Routes = [
  {
    path: '',
    component: GeneralComponent,
    children: [
      {path: 'dashboard', component: DashboardComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'unauthorized', component: UnauthorizedComponent, data: {preload: false}},
      {path: 'noaccess', component: NoAccessComponent, data: {preload: false}},
      {path: 'notifications', component: NotificationsComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'messages', component: MessagesComponent, data: {preload: false}, canActivate: [AuthGuard]},
    ]
  }
];

export const routing = RouterModule.forChild(routes);
