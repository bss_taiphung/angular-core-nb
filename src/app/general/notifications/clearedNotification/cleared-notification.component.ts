import {
  Component,
  OnInit,
  Output,
  Input,
  EventEmitter
} from '@angular/core'
import {NotificationDto} from '../service/notifications.model'

@Component({
  selector: 'cleared-notification',
  templateUrl: './cleared-notification.component.html',
  styleUrls: ['../notifications.component.scss']
})

export class ClearedNotification implements OnInit {

  @Input() notifications: Array<NotificationDto>;
  @Input() showHide: boolean;
  @Input() groups: string[];

  @Output() onRestoreAllNotification = new EventEmitter<any>();
  @Output() onRestoreNotification: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
  }

  @Output() onNavigatePage: EventEmitter<any> = new EventEmitter();

  navigateToPage(data: NotificationDto) {
    this.onNavigatePage.emit({data});
  }

  restoreNotification(data: NotificationDto[], index: number, group: string) {
    this.onRestoreNotification.emit({data, index, group});
  }

  restoreAllNotification(data: NotificationDto[], group: string) {
    this.onRestoreAllNotification.emit({data, group});
  }

  countNotificationSection(data: NotificationDto[], key: any) {
    let counter = 0;
    data.forEach((item: any) => {
      if (!item[key]) {
        counter++;
      }
    });
    return counter ? true : false;
  }

  updateUrl(data: NotificationDto) {
    data.notificationUserAvatar = 'assets/img/no_avatar.png';
  }
}
