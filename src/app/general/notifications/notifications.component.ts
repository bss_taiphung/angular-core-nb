import {Component, OnInit} from '@angular/core';
import {
  BulkupdateNotificationDto,
  CreateNotificationExclusionDto,
  NotificationDto,
  NotificationExclusionDto
} from './service/notifications.model';
import {NotificationsService} from './service/notifications.service'
import {
  filter,
  forEach,
  groupBy,
  includes,
  orderBy,
  remove,
  replace
} from 'lodash';
import {Router} from '@angular/router';

import {SortDescriptor} from '@progress/kendo-data-query';
import {HubConnection} from '@aspnet/signalr-client';

import {
EqmSettings,
User,
UserRole
} from '../../shared/models/eqmSettings'
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {CommonService} from '../../shared/services/common.service';
import {FormControl, FormGroup} from '@angular/forms';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  today = Date.now();
  groups: string[];
  notifications: any;
  clearNotifications: any;
  pinnedNotification: Array<NotificationDto> = [];
  allNoficationdata: Array<NotificationDto> = [];
  notificationCounter: number = 0;
  unreadNotification: number = 0;
  showHide: boolean;
  loading: boolean = false;
  private clearLoading: boolean = false;
  private showDataLoadProcess: boolean = false;
  pageloading: boolean = true;
  eqmSettings: EqmSettings = new EqmSettings();
  user: User = new User();
  userRole: UserRole = new UserRole();
  public azureStorageContainer: string;
  public searchtext: string = '';
  private sort: SortDescriptor[] = [];
  public pageSize = 50;
  public skip = 0;
  public clearSkip = 0;

  private hubConnection: HubConnection;

  constructor(private notificationsService: NotificationsService,
              private router: Router,
              private commonService: CommonService,
              private toasterService: ToasterHelperService,
              private eqmSettingsService: EqmSettingsService) {
    this.eqmSettings = this.eqmSettingsService.getEqmSettings();
    this.user = this.eqmSettingsService.getUser();
    this.userRole = this.eqmSettingsService.getUserRoles();
    this.azureStorageContainer = environment.azureStorageContainer;
  }

  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  ngOnInit() {
    this.getNotificationList();
    this.getNotificationList(true);
    this.showHide = true;
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');

        this.hubConnection
          .invoke('Subscribe', 'General', 'Notification', 'UserId', this.user.userId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :'));

    this.hubConnection.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
      const _responseText = messageJson;
      let result200: any = null;
      let data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      let resultData200 = this.commonService.toCamel(data);
      if (operation.toLowerCase() === 'bulkupdate') {
        result200 = resultData200 ? BulkupdateNotificationDto.fromJS(resultData200).results : new NotificationDto();
        result200.forEach(itm => {
          itm.notificationUserAvatar = this.commonService.getUserThumbnail(itm.createdBy);
        });
      } else {
        result200 = resultData200 ? NotificationDto.fromJS(resultData200) : new NotificationDto();
        result200.notificationUserAvatar = this.commonService.getUserThumbnail(result200.createdBy);
      }

      var dataExist = this.containsObject(result200, this.notifications);

      if (operation.toLowerCase() === 'insert' && !dataExist) {
        this.allNoficationdata.push(result200);
        this.updateNotificationList();
      }

      if (operation.toLowerCase() === 'update') {
        this.allNoficationdata.forEach((element, index) => {
          if (element.notificationId === result200.notificationId) {
            this.allNoficationdata[index] = result200;
            this.updateNotificationList();
          }
        });
      }
      else if (operation.toLowerCase() === 'bulkupdate') {

        forEach(result200,
          (item) => {
            this.allNoficationdata.forEach((element, index) => {
              if (element.notificationId === item.notificationId) {
                this.allNoficationdata[index] = item;

              }
            });
          });
        this.updateNotificationList();

      }

      if (operation.toLowerCase() === 'delete' && dataExist) {
        var index = null;
        this.allNoficationdata.forEach((element, i) => {
          if (element.notificationId === result200.notificationId) {
            index = i;
          }
        });
        if (index !== null) {
          this.allNoficationdata.splice(index, 1);
          this.updateNotificationList();
        }
      }

    });
  }

  containsObject(obj, list) {
    var x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].notificationId === obj.notificationId) {
        return true;
      }
    }

    return false;
  }

  clearedStatus() {
    this.showHide = false;
  }

  updatesStatus() {
    this.showHide = true;
  }

  getNotificationList(isClearData: boolean = false) {
    this.showDataLoadProcess = true;
    let pageSize: number = 0;
    let skip: number = 0;
    if (isClearData) {
      pageSize = this.pageSize;
      skip = this.clearSkip;
      this.clearLoading = false;
    } else {
      pageSize = this.pageSize;
      skip = this.skip;
      this.loading = false;
    }

    this.notificationsService.getAll(this.user.userId, pageSize, skip, this.sort, isClearData).subscribe(
      eventResult => {

        let results: NotificationDto[] = eventResult.results;

        results.forEach(itm => {
          itm.notificationUserAvatar = this.commonService.getUserThumbnail(itm.createdBy);
        });
        if (eventResult.results.length < pageSize)

          if (isClearData) {
            this.clearLoading = true;

          } else {
            this.loading = true;
          }
        if (this.allNoficationdata.length === 0) {
          this.allNoficationdata = results;
        } else {
          forEach(results,
            (item) => {
              let isExits = false;
              this.allNoficationdata.forEach((element, index) => {
                if (element.notificationId === item.notificationId) {
                  isExits = true;
                }
              });
              if (!isExits) {
                this.allNoficationdata.push(item);
              } else {
                isExits = false;
              }
            });
        }
        //results.forEach(itm => {
        //  this.allNoficationdata.push(itm);
        //});

        //this.skip = (this.allNoficationdata.length - 1);
        this.updateNotificationList();
        this.pageloading = false;
        this.showDataLoadProcess = false;
      });
  }

  private updateNotificationList() {

    this.allNoficationdata = orderBy(this.allNoficationdata, ['createdDateTimeObj'], ['asc']);
    this.manageNotifications(this.allNoficationdata);
    // this.notificationCounter = (filter(this.allNoficationdata, (f) => { return f.active })).length;
    this.pinnedNotification = orderBy(filter(this.allNoficationdata, (f) => {
      return f.isPinned && f.active
    }), ['createdDateTimeObj'], ['desc']);
    forEach(this.allNoficationdata, function (newList, key) {
      newList.DisplayNotificationText = newList.notificationText;
      for (var i = 1; i <= newList.retRelatedObject.length; i++) {
        const search = includes(newList.notificationText, '[' + i + ']');
        if (search == true) {
          newList.DisplayNotificationText = replace(newList.DisplayNotificationText, '[' + i + ']', newList.retRelatedObject[i - 1].objectDisplay);
        }
      }
    });
  }

  private manageNotifications(values: NotificationDto[]) {
    this.groups = [];
    let notificationFilter = (filter(values, (f) => {
      return !f.isPinned
    }));
    notificationFilter.forEach((item: NotificationDto, index: number) => {
      let date = item.createdDateTime.split('-');
      notificationFilter[index].orderByDate = `${date[0]}-${date[1]}`;
    });
    this.notifications = groupBy(notificationFilter, 'orderByDate');
    let revGroup = Object.getOwnPropertyNames(this.notifications);
    for (let r = revGroup.length - 1; r >= 0; r--)
      this.groups.push(revGroup[r]);
    for (var group in this.notifications) {
      let notification = this.notifications[group];
      this.notifications[group] = orderBy(notification, ['createdDateTimeObj'], ['desc']);
    }

  }

  countNotificationActive(data: NotificationDto[], key: any) {
    let counter = 0;
    data.forEach((item: any) => {
      if (item[key]) counter++;
    });
    return counter ? true : false;
  }

  // Exclusion Options
  isExclusionOptionDialog: boolean = false;
  public exclusionOptionForm: FormGroup = new FormGroup({
    'excludeEmail': new FormControl(),
    'excludeSMS': new FormControl(),
  });

  private notificationExclusion: NotificationExclusionDto;

  exclusionOptions(param: any) {
    this.notificationExclusion = new NotificationExclusionDto();
    this.notificationExclusion.event = param.data.event;
    this.notificationExclusion.module = param.data.module;
    this.notificationExclusion.createdBy = param.data.createdBy;
    this.notificationExclusion.parentObjectType = param.data.parentObjectType;
    this.notificationsService.getNotificationExclusionOptions(this.notificationExclusion,
      response => {
        response.excludeEmail = !response.excludeEmail;
        response.excludeSMS = !response.excludeSMS;
        this.exclusionOptionForm.reset(response);
        this.notificationExclusion.notificationExclusionId = response.notificationExclusionId;
        this.isExclusionOptionDialog = true;
      },
      error => {
        this.toasterService.errorMessage(error);
      }
    );
  }

  closeExclusionOptionDialog() {
    this.isExclusionOptionDialog = false;
  }

  exclusionLoading: boolean = false;

  saveExclusionOption(e) {
    e.preventDefault();
    this.exclusionLoading = true;
    const setExclusionOption: CreateNotificationExclusionDto = this.exclusionOptionForm.value;
    setExclusionOption.excludeSMS = !setExclusionOption.excludeSMS;
    setExclusionOption.excludeEmail = !setExclusionOption.excludeEmail;
    setExclusionOption.notificationExclusionId = this.notificationExclusion.notificationExclusionId;
    setExclusionOption.event = this.notificationExclusion.event;
    setExclusionOption.module = this.notificationExclusion.module;
    setExclusionOption.parentObjectType = this.notificationExclusion.parentObjectType;
    setExclusionOption.createdBy = this.notificationExclusion.createdBy;
    setExclusionOption.modifiedBy = this.user.userId;

    this.notificationsService.setNotificationExclusionOptions(setExclusionOption,
      response => {
        this.toasterService.success('', 'Notification Exclusion options set successfully');
        this.exclusionLoading = false;
        this.isExclusionOptionDialog = false;
      },
      error => {
        this.toasterService.errorMessage(error);
        this.exclusionLoading = false;
      }
    );

  }


  markAsReadNotification(param: any) {
    if (!param.data.isRead) {
      param.data.isRead = true;
      this.notificationsService.update(param.data,
        response => {
          this.toasterService.success('', 'Notification Mark As Read');
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  markAsAllReadNotification(param: any) {
    for (let x of param.data) {
      x.isRead = true;
    }
    this.notificationsService.markAsAllReadNofication(param.data,
      response => {
        this.toasterService.success('', 'Notification Mark As All Read Nofication');
      },
      error => {
        this.toasterService.errorMessage(error);
      }
    );

  }

  pinNotification(param: any) {
    let pinnedMsg = '';
    if (param.data.isPinned) {
      param.data.isPinned = false;
      this.pinnedNotification = remove(this.pinnedNotification, function (item: NotificationDto) {
        return item.notificationId !== param.data.notificationId;
      });

      let date = param.data.createdDateTime.split('-');
      let group = param.data.orderByDate = `${date[0]}-${date[1]}`;
      let groupFlag = false;

      //
      if (groupFlag) {
        this.notifications[group].push(param.data);
      } else {
        for (var group in this.notifications) {
          let notification = this.notifications[group];
          this.notifications[group] = orderBy(notification, ['createdDateTimeObj'], ['desc']);
        }
      }
      //  this.notifications[group] = orderBy(this.notifications[group], ["createdDateTimeObj"], ["desc"]);
      pinnedMsg = 'Notification Unpinned';
    }
    else {
      param.data.isPinned = true;
      this.notifications[param.group] = remove(this.notifications[param.group], function (item: NotificationDto) {
        return item.notificationId != param.data.notificationId;
      });
      this.pinnedNotification.push(param.data);
      this.pinnedNotification = orderBy(this.pinnedNotification, ['createdDateTimeObj'], ['desc']);
      pinnedMsg = 'Notification Pinned';
    }
    this.notificationsService.update(param.data,
      response => {
        this.toasterService.success('', pinnedMsg);
      },
      error => {
        this.toasterService.errorMessage(error);
      }
    );
  }

  clearAllNotification(param: { data: NotificationDto, group: string }) {
    let isPinned: NotificationDto[] = [];
    forEach(param.data,
      (item) => {
        item.active = false;

      });
    this.notificationsService.clearAllNofication(param.data,
      response => {
        this.toasterService.success('', 'Clear all Notification');
      },
      error => {
        this.toasterService.errorMessage(error);
      }
    );

  };

  clearNotification(param: any) {
    param.data.active = false;
    //if (param.data.isPinned) {
    //  this.pinnedNotification = remove(this.pinnedNotification, (item) => {
    //    return item.notificationId != param.data.notificationId;
    //  });
    //  this.notifications[param.group].push(param.data);
    //}

    this.notificationsService.update(param.data,
      response => {
        this.toasterService.success('', 'Notification Clear');
      },
      error => {
        this.toasterService.errorMessage(error);
      }
    );
  }

  private navigateUrl = [
    {url: 'app/eqmaintenance/manufacturer', index: 'manufacturer'},
    {url: 'app/eqmaintenance/part', index: 'part'},
    {url: 'app/eqmaintenance/scheduletype', index: 'scheduletype'},
    {url: 'app/eqmaintenance/ordertype', index: 'ordertype'},
    {url: 'app/eqmaintenance/failure-type', index: 'failure-type'},
    {url: 'app/eqmaintenance/priority', index: 'priority'},
    {url: 'app/eqmaintenance/status', index: 'statuses'},
    {url: 'app/eqmaintenance/equipment', index: 'equipment'},
    {url: 'app/eqmaintenance/pmschedule', index: 'pmschedule'},
    {url: 'app/eqmaintenance/orders', index: 'orders'}
  ];

  navigatePage(notify: any) {
    var indexUrl = notify.data.parentObjectDisplay.split('/')[0];
    if (notify.data.parentObjectDisplay.includes('>')) {
      indexUrl = notify.data.parentObjectDisplay.split('>')[1].trim();
    }
    var result = this.navigateUrl.filter(res => res.index === indexUrl.toLowerCase());
    if (result != undefined) {
      this.router.navigate([result[0].url]);
    }
  }

  restoreAllNotification(param: { data: NotificationDto, group: string }) {

    forEach(param.data, (value) => {
      value.active = true;
      value.isPinned = false;
    });
    console.dir(param.data);
    this.notificationsService.restoreAllNotification(param.data,
      response => {
        this.toasterService.success('', 'Notification restore all');
      },
      error => {
        this.toasterService.errorMessage(error);
      }
    );
  }

  restoreNotification(param: any) {

    param.data.active = true;
    param.data.isPinned = false;
    this.notificationsService.update(param.data,
      response => {
        this.toasterService.success('', 'Notification restore');
      },
      error => {
        this.toasterService.errorMessage(error);
      }
    );
  }

  public loadMoredata() {
    this.skip += this.pageSize;
    this.getNotificationList();
  }

  public loadMoreCleardata() {
    this.clearSkip += this.pageSize;
    this.getNotificationList(true);
  }
}
