import {
  Component,
  OnInit,
  Output,
  Input,
  EventEmitter
} from '@angular/core'
import {NotificationDto} from '../service/notifications.model'

@Component({
  selector: 'pinned-notification',
  templateUrl: './pinned-notification.html',
  styleUrls: ['../notifications.component.scss']
})
export class PinnedNotification implements OnInit {

  @Input() pinnedNotification: Array<NotificationDto>;
  @Input() showHide: boolean;

  @Output() onMarkAsAllReadNotification = new EventEmitter<any>();
  @Output() onClearAllNotification = new EventEmitter<any>();
  @Output() onMarkAsReadNotification = new EventEmitter<any>();
  @Output() onClearNotification = new EventEmitter<any>();
  @Output() onPinNotification: EventEmitter<any> = new EventEmitter();
  @Output() onExclusionOptions = new EventEmitter<any>();

  ngOnInit() {
  }

  @Output() onNavigatePage: EventEmitter<any> = new EventEmitter();

  navigateToPage(data: NotificationDto) {
    this.onNavigatePage.emit({data});
  }

  pinNotification(data: NotificationDto, index: number, group: string) {
    this.onPinNotification.emit({data, index, group});
  }

  exclusionOptions(data: NotificationDto) {
    this.onExclusionOptions.emit({data});
  }

  clearNotification(data: NotificationDto, index: number, group: string) {
    this.onClearNotification.emit({data, index, group});
  }

  markAsReadNotification(data: NotificationDto) {
    this.onMarkAsReadNotification.emit({data});
  }

  markAsAllReadNotification(data: Array<NotificationDto>, group: string) {
    this.onMarkAsAllReadNotification.emit({data, group});
  }

  clearAllNotification(data: Array<NotificationDto>, group: string = 'PinNotification') {
    this.onClearAllNotification.emit({data, group});
  }

  updateUrl(data: NotificationDto) {
    data.notificationUserAvatar = 'assets/img/no_avatar.png';
  }
}
