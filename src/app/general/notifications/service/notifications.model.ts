export interface INotificationDto {
  notificationId: string;
  module: number;
  event: string;
  relatedObjectJson: string;
  notificationText: string;
  displayNotificationText: string;
  orderByDate?: string;
  parentObjectType: number;
  parentObjectId: any;
  parentObjectDisplay: string;
  parentObjectAppRelativeUrl: string;
  userId: any;
  isRead: boolean;
  isPinned: boolean;
  notificationHtml: string;
  parentHtml: string;
  retRelatedObject: RetRelatedObject;
  modifiedDateTime: string;
  modifiedBy: any;
  createdBy: any;
  userDisplayName: string;
  createdDateTime: string;
  createdDateTimeObj: Date;
  createdByName: string;
  active: boolean;
  notificationUserAvatar: string;
}

export class NotificationDto implements INotificationDto {
  notificationId: string;
  module: number;
  event: string;
  relatedObjectJson: string;
  notificationText: string;
  displayNotificationText: string;
  orderByDate?: string;
  parentObjectType: number;
  parentObjectId: any;
  parentObjectDisplay: string;
  parentObjectAppRelativeUrl: string;
  userId: any;
  isRead: boolean;
  isPinned: boolean;
  notificationHtml: string;
  parentHtml: string;
  retRelatedObject: any;
  userDisplayName: string;
  modifiedDateTime: string;
  modifiedBy: any;
  createdBy: any;
  createdByName: string;
  createdDateTime: string;
  active: boolean;
  notificationUserAvatar: string;
  createdDateTimeObj: Date;

  static fromJS(data: any): NotificationDto {
    const result = new NotificationDto();
    result.init(data);
    return result;
  }

  constructor(data?: INotificationDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.notificationId = data['notificationId'];
      this.module = data['module'];
      this.event = data['event'];
      this.relatedObjectJson = data['relatedObjectJSON'];
      this.displayNotificationText = data['displayNotificationText'];
      this.notificationText = data['notificationText'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.orderByDate = data['orderByDate'];
      this.parentObjectType = data['parentObjectType'];
      this.parentObjectId = data['parentObjectId'];
      this.parentObjectDisplay = data['parentObjectDisplay'];
      this.parentObjectAppRelativeUrl = data['parentObjectAppRelativeURL'];
      this.userId = data['userId'];
      this.isRead = data['isRead'];
      this.isPinned = data['isPinned'];
      this.notificationHtml = data['notificationHtml'];
      this.parentHtml = data['parentHtml'];
      this.retRelatedObject = RetRelatedObject.fromJS(JSON.parse(data['relatedObjectJSON']));
      this.userDisplayName = data['userDisplayName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.modifiedBy = data['modifiedBy'];
      this.createdBy = data['createdBy'];
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDateTimeObj = new Date(data['createdDateTime']);
      this.active = data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['notificationId'] = this.notificationId;
    data['module'] = this.module;
    data['event'] = this.event;
    data['relatedObjectJSON'] = this.relatedObjectJson;
    data['displayNotificationText'] = this.displayNotificationText;
    data['notificationText'] = this.notificationText;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['orderByDate'] = this.orderByDate;
    data['parentObjectType'] = this.parentObjectType;
    data['parentObjectId'] = this.parentObjectId;
    data['parentObjectDisplay'] = this.parentObjectDisplay;
    data['parentObjectAppRelativeURL'] = this.parentObjectAppRelativeUrl;
    data['userId'] = this.userId;
    data['isRead'] = this.isRead;
    data['isPinned'] = this.isPinned;
    data['notificationHtml'] = this.notificationHtml;
    data['parentHtml'] = this.parentHtml;
    data['retRelatedObject'] = this.retRelatedObject;
    data['userDisplayName'] = this.userDisplayName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['modifiedBy'] = this.modifiedBy;
    data['createdBy'] = this.createdBy;
    data['createdByName'] = this.createdByName;
    data['createdDateTime'] = this.createdDateTime;
    data['active'] = this.active;

    return data;
  }
}


export interface IRetRelatedObject {
  objectType: any;
  objectId: any;
  objectDisplay: string;
  objectAppRelativeURL: string;
  notificationTextShortCodeId: any;
}

export class RetRelatedObject implements IRetRelatedObject {
  objectType: any;
  objectId: any;
  objectDisplay: string;
  objectAppRelativeURL: string;
  notificationTextShortCodeId: any;

  constructor(data?: IRetRelatedObject) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.objectType = data['objectType'];
      this.objectId = data['ObjectId'];
      this.objectDisplay = data['ObjectDisplay'];
      this.objectAppRelativeURL = data['OBSOLETE'];
      this.objectAppRelativeURL = data['ObjectAppRelativeURL'];
      this.notificationTextShortCodeId = data['NotificationTextShortCodeId'];
    }
  }

  static fromJS(data: any): Array<RetRelatedObject> {
    const results = [];
    for (const item of data) {
      const obj = new RetRelatedObject();
      obj.init(item);
      results.push(obj);
    }
    return results;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['ObjectType'] = this.objectType;
    data['ObjectId'] = this.objectId;
    data['ObjectDisplay'] = this.objectDisplay;
    data['ObjectAppRelativeURL'] = this.objectAppRelativeURL;
    data['NotificationTextShortCodeId'] = this.notificationTextShortCodeId;

    return data;
  }

}

//export interface IPagedResultDtoOfNotificationDto {
//  results: NotificationDto[];
//  count: number;
//}

//export class PagedResultDtoOfNotificationDto implements IPagedResultDtoOfNotificationDto {
//  results: NotificationDto[];
//  count: number;

//  constructor(data?: IPagedResultDtoOfNotificationDto) {
//    if (data) {
//      for (var property in data) {
//        if (data.hasOwnProperty(property))
//          (<any>this)[property] = (<any>data)[property];
//      }
//    }
//  }

//  init(data?: any) {
//    if (data && data != null) {
//      this.results = [];
//      for (let item of data)
//        this.results.push(NotificationDto.fromJS(item));
//    }
//  }

//  static fromJS(data: any): PagedResultDtoOfNotificationDto {
//    let result = new PagedResultDtoOfNotificationDto();
//    result.init(data);
//    return result;
//  }

//  toJSON(data?: any) {
//    data = typeof data === 'object' ? data : {};
//    if (this.results && this.results.constructor === Array) {
//      data["results"] = [];
//      for (let item of this.results)
//        data["results"].push(item.toJSON());

//    }
//    if (this.count) {
//      data["count"] = this.count;
//    }
//    return data;
//  }

//}

// region IPagedResultDtoOfNotificationDto

export interface IPagedResultDtoOfNotificationDto {
  results: NotificationDto[];
  count: number;
}

export class BulkupdateNotificationDto implements IPagedResultDtoOfNotificationDto {
  results: NotificationDto[];
  count: number;

  static fromJS(data: any): BulkupdateNotificationDto {

    const result = new BulkupdateNotificationDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfNotificationDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      this.results = [];
      for (const item of data) {
        this.results.push(NotificationDto.fromJS(item));
      }

    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }

    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }

}

export class PagedResultDtoOfNotificationDto implements IPagedResultDtoOfNotificationDto {
  results: NotificationDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfNotificationDto {
    const result = new PagedResultDtoOfNotificationDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfNotificationDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(NotificationDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion


// region INotificationExclusionDto

export interface INotificationExclusionDto {
  notificationExclusionId: number;
  module: number;
  parentObjectType: number;
  event: string;
  excludeEmail: boolean;
  excludeSMS: boolean;
  createdBy: number;
  createdByName: string;
  modifiedByName: string;
}

export class NotificationExclusionDto implements INotificationExclusionDto {
  notificationExclusionId: number;
  module: number;
  parentObjectType: number;
  event: string;
  excludeEmail: boolean;
  excludeSMS: boolean;
  createdBy: number;
  createdByName: string;
  modifiedByName: string;

  static fromJS(data: any): NotificationExclusionDto {
    const result = new NotificationExclusionDto();
    result.init(data);
    return result;
  }

  constructor(data?: INotificationExclusionDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.notificationExclusionId = data['notificationExclusionId'];
      this.module = data['module'];
      this.parentObjectType = data['parentObjectType'];
      this.event = data['event'];
      this.excludeEmail = data['excludeEmail'];
      this.excludeSMS = data['excludeSMS'];
      this.createdBy = data['createdBy'];
      this.createdByName = data['createdByName'];
      this.modifiedByName = data['modifiedByName'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['notificationExclusionId'] = this.notificationExclusionId;
    data['module'] = this.module;
    data['parentObjectType'] = this.parentObjectType;
    data['event'] = this.event;
    data['excludeEmail'] = this.excludeEmail;
    data['excludeSMS'] = this.excludeSMS;
    data['createdBy'] = this.createdBy;
    data['createdByName'] = this.createdByName;
    data['modifiedByName'] = this.modifiedByName;

    return data;
  }
}

// endregion


// region ICreateNotificationExclusionDto
export interface ICreateNotificationExclusionDto {
  notificationExclusionId: number;
  module: number;
  parentObjectType: number;
  event: string;
  excludeEmail: boolean;
  excludeSMS: boolean;
  createdBy: number;
  modifiedBy: number;
}

export class CreateNotificationExclusionDto implements ICreateNotificationExclusionDto {
  notificationExclusionId: number;
  module: number;
  parentObjectType: number;
  event: string;
  excludeEmail: boolean;
  excludeSMS: boolean;
  createdBy: number;
  modifiedBy: number;

  static fromJS(data: any): NotificationExclusionDto {
    const result = new NotificationExclusionDto();
    result.init(data);
    return result;
  }

  constructor(data?: INotificationExclusionDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.notificationExclusionId = data['notificationExclusionId'];
      this.module = data['module'];
      this.parentObjectType = data['parentObjectType'];
      this.event = data['event'];
      this.excludeEmail = data['excludeEmail'];
      this.excludeSMS = data['excludeSMS'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['notificationExclusionId'] = this.notificationExclusionId;
    data['module'] = this.module;
    data['parentObjectType'] = this.parentObjectType;
    data['event'] = this.event;
    data['excludeEmail'] = this.excludeEmail;
    data['excludeSMS'] = this.excludeSMS;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    return data;
  }
}

// endregion

