import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {
  CreateNotificationExclusionDto,
  NotificationDto,
  PagedResultDtoOfNotificationDto
} from './notifications.model';
import {CommonService} from '../../../shared/services/common.service';
import {filter, find, forEach, groupBy, includes, orderBy, remove, replace} from 'lodash';
import {environment} from '../../../../environments/environment';

@Injectable()
export class NotificationsService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;


  constructor(private commonService: CommonService, private http: HttpClient) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/General/api/Notification/';
  }

  create(input: NotificationDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'InsertAsync';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  update(input: NotificationDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'UpdateAsync';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  markAsAllReadNofication(input: NotificationDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'MarkAsAllReadNofication';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  clearAllNofication(input: NotificationDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'ClearAllNofication';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  restoreAllNotification(input: NotificationDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'RestoreAllNotification';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  /*getAll(userid): Observable<PagedResultDtoOfNotificationDto> {

   let url = this.apiBaseUrl + "GetNotification";
   let macAddress = this.commonService.getMacAddress();

   const params = new HttpParams()
     .set('userid', userid.toString());

   let options = {
     headers: new HttpHeaders({
       "Content-Type": "application/json",
       "Accept": "application/json",
       "MacAddress": macAddress
     }),
     params: params
   };

   return this.http.request<Response>('get', url, options).flatMap((response) => {
     let result200 = PagedResultDtoOfNotificationDto.fromJS(response);
     return Observable.of(result200);
   });

  }*/


  getAll(userid: number, take: number, skip: number, sort: any, isClearData: boolean): Observable<PagedResultDtoOfNotificationDto> {

    const url = this.apiBaseUrl + 'GetPagedQuery';

    // Order by Clause
    let orderBy = '';
    if (sort !== undefined && sort.length > 0) {
      for (const item of sort) {
        orderBy = this.commonService.firstUpper(item.field) + ' ' + (item.dir === undefined ? 'asc' : item.dir);
      }
    } else {
      orderBy = 'NotificationId desc';
    }

    let filter = '';
    filter = 'UserId eq ' + userid.toString();
    if (isClearData) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += ' Active eq false';
    } else {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += ' Active eq true';
    }

    const params = new HttpParams()
      .set('$top', take.toString())
      .set('$skip', skip.toString())
      .set('$count', 'true')
      .set('$orderby', orderBy.toString())
      .set('$filter', filter.toString());


    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      const result200 = PagedResultDtoOfNotificationDto.fromJS(response);
      return Observable.of(result200);
    });
  }


  getUnreadNotifications(userid): any {

    const url = this.apiBaseUrl + 'GetUnreadNotifications';
    const macAddress = this.commonService.getMacAddress();

    const params = new HttpParams()
      .set('userid', userid.toString());

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'MacAddress': macAddress
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      return Observable.of(response);
    });

  }

  getNotificationExclusionOptions(notificationData: any, successCallback: any, errorCallback: any): any {
    const url = environment.bllApiBaseAddress + '/General/api/NotificationExclusion/' + 'GetNotificationExclusion';

    this.commonService.httpPost(url, notificationData, successCallback, errorCallback);
  }

  setNotificationExclusionOptions(input: CreateNotificationExclusionDto, successCallback: any, errorCallback: any): any {
    const url = environment.bllApiBaseAddress + '/General/api/NotificationExclusion/' + 'SetNotificationExclusion';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

}


