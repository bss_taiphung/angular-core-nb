import {
  Component,
  OnInit,
  Output,
  Input,
  EventEmitter
} from '@angular/core'
import {NotificationDto} from '../service/notifications.model'

@Component({
  selector: 'notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['../notifications.component.scss']
})
export class NotificationList implements OnInit {

  @Input() notifications: Array<NotificationDto>;
  @Input() showHide: boolean;
  @Input() groups: string[];
  @Input() allNoficationdata: any;

  @Output() onMarkAsAllReadNotification = new EventEmitter<any>();
  @Output() onClearAllNotification = new EventEmitter<any>();
  @Output() onExclusionOptions = new EventEmitter<any>();
  @Output() onMarkAsReadNotification = new EventEmitter<any>();
  @Output() onClearNotification = new EventEmitter<any>();
  @Output() onPinNotification: EventEmitter<any> = new EventEmitter();
  @Output() onNavigatePage: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
  }

  pinNotification(data: NotificationDto, index: number, group: string) {
    this.onPinNotification.emit({data, index, group});
  }

  clearNotification(data: NotificationDto, index: number, group: string) {
    this.onClearNotification.emit({data, index, group});
  }

  markAsReadNotification(data: NotificationDto) {
    this.onMarkAsReadNotification.emit({data});
  }

  exclusionOptions(data: NotificationDto) {
    this.onExclusionOptions.emit({data});
  }

  markAsAllReadNotification(data: Array<NotificationDto>, group: string) {
    this.onMarkAsAllReadNotification.emit({data, group});
  }

  clearAllNotification(data: Array<NotificationDto>, group: string) {
    this.onClearAllNotification.emit({data, group});
  }

  navigateToPage(data: NotificationDto) {
    this.onNavigatePage.emit({data});
  }

  countNotificationActive(data: NotificationDto[], key: any) {
    let counter = 0;
    data.forEach((item: any) => {
      if (item[key]) counter++;
    });
    return counter ? true : false;
  }

  updateUrl(data: NotificationDto) {
    data.notificationUserAvatar = 'assets/img/no_avatar.png';
  }
}
