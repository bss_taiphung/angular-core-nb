import {Component} from '@angular/core';

@Component({
  selector: 'app-general',
  template: '<router-outlet></router-outlet>',
})
export class GeneralComponent {
}
