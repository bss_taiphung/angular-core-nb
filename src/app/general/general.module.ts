import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {GeneralComponent} from './general.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {UnauthorizedComponent} from './unauthorized/unauthorized.component';
import {NoAccessComponent} from './noaccess/noaccess.component';
import {MessagesComponent} from './messages/messages.component'
import {NotificationsComponent} from './notifications/notifications.component'
import {NotificationList} from './notifications/notificationsList/notification-list.component'
import {PinnedNotification} from './notifications/pinnedNotification/pinned-notification.component'
import {ClearedNotification} from './notifications/clearedNotification/cleared-notification.component'

// service
import {NotificationsService} from './notifications/service/notifications.service'
import {routing} from './general.routes';

import {ButtonsModule} from '@progress/kendo-angular-buttons';
import {DialogModule} from '@progress/kendo-angular-dialog';
import {InputsModule} from '@progress/kendo-angular-inputs';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonsModule,
    InputsModule,
    DialogModule,
    routing
  ],
  declarations: [
    DashboardComponent,
    GeneralComponent,
    MessagesComponent,
    NotificationList,
    PinnedNotification,
    NotificationsComponent,
    ClearedNotification,
    UnauthorizedComponent,
    NoAccessComponent,
  ],
  providers: [
    NotificationsService
  ]

})
export class GeneralModule {
}
