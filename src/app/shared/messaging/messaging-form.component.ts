import {
  AfterViewInit, ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import {
  filter,
  find,
  forEach,
  groupBy,
  includes,
  orderBy,
  remove,
  replace
} from 'lodash';
import {HubConnection} from '@aspnet/signalr-client';

import {CommonService} from '../services/common.service'
import {MessageService} from './service/messaging.service'
import {MessageContextService} from './service/messagecontext.service'
import {MessageContextFollowerService} from './service/messagecontextfollower.service'

import {ToasterHelperService} from '../services/toasterHelper.service'
import {MessageContextFollowerDto} from './service/messaging.model'
import {EqmSettingsService} from '../services/eqmsettings.service'
import {environment} from '../../../environments/environment';

declare var kendo: any;


@Component({
  selector: 'messaging-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./messaging.component.css'],
  templateUrl: './messaging-form.component.html'
})

export class MessagingFormComponent implements OnInit, AfterViewInit {
  private userId: number;
  private hubConnection: HubConnection;
  private hubConnection1: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  public azureStorageContainer: string;
  private userid = 0;

  timeout = null;

  constructor(private commonService: CommonService,
              private toasterService: ToasterHelperService,
              private messageService: MessageService,
              private messageContextService: MessageContextService,
              private messageContextFollowerService: MessageContextFollowerService,
              private eqmSettingsService: EqmSettingsService) {
    this.userId = this.eqmSettingsService.getUser().userId;
    this.azureStorageContainer = environment.azureStorageContainer;
  }

  ngAfterViewInit(): void {
    this.messageContextSignalR();
  }

  ngOnInit(): void {

  }

  public messageDialogActive = false;
  public userAvatar: any;

  private messageContextId = 0;
  chatLoading = false;

  @Input() public createChannelOpen = false;
  @Input() public directmessageDialogActive = false;
  @Input() public messagePopUp = false;

  @Input()
  public set model(data: any) {
    if (data && data === true) {
      this.messageDialogActive = this.messagePopUp !== undefined;
      this.userid = this.eqmSettingsService.getUser().userId;
      this.resetData();
      this.getAllChatChannelList();
    }
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();

  public onSave(e): void {
  }

  public openChannelDialogForm() {
    this.createChannelOpen = true;
  }

  public openDirectMessageForm() {
    this.directmessageDialogActive = true;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.messageDialogActive = false;
    this.messagePopUp = false;
    this.createChannelOpen = false;
    this.directmessageDialogActive = false;
    this.cancel.emit();
  }

  updateUserAvatarToDefault() {
    this.userAvatar = 'assets/img/no_avatar.png';
  }

  closeNewChannelDialog() {
    this.createChannelOpen = false;
  }

  closeDirectmessageDialog() {
    this.directmessageDialogActive = false;
  }

  private allChannalist: any = [];
  private directMessageFollowerList: any = [];
  private chatChannelList: any = [];

  messageContextSignalR() {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started Mesaage Context Follwer!');

        this.hubConnection
          .invoke('Subscribe', 'General', 'MessageContextFollower', 'CreatedBy', this.userId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
      const _responseText = messageJson;
      let result200: any = null;
      const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      const resultData200 = this.commonService.toCamel(data);
      result200 = resultData200 ? MessageContextFollowerDto.fromJS(resultData200) : new MessageContextFollowerDto();

      const dataExist = this.containsObject(result200, this.allChannalist);

      if (operation.toLowerCase() === 'insert' && !dataExist) {
        this.allChannalist.push(result200);
      }

      if (operation.toLowerCase() === 'update') {
        this.allChannalist.forEach((element, index) => {
          if (element.messageContextId === result200.messageContextId) {
            this.allChannalist[index] = result200;
          }
        });
      }

      if (operation.toLowerCase() === 'delete' && dataExist) {
        let index = null;
        this.allChannalist.forEach((element, i) => {
          if (element.messageContextId === result200.messageContextId) {
            index = i;
          }
        });
        if (index !== null) {
          this.allChannalist.splice(index, 1);
        }

      }
      this.setChatcontext();
    });
  }

  containsObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].messageContextId === obj.messageContextId) {
        return true;
      }
    }

    return false;
  }


  getAllChatChannelList() {
    this.chatLoading = true;
    this.messageContextService.getAllChatChannelList(this.userid.toString(),
      eventResult => {
        this.allChannalist = eventResult;
        this.setChatcontext();
        this.chatLoading = false;
      }, error => {
        this.chatLoading = false;
      });
  }

  setChatcontext() {
    this.chatChannelList = this.allChannalist.filter(function (obj) {
      return obj.canChangeFollowers;
    });
    this.directMessageFollowerList = this.allChannalist.filter(function (obj) {
      return !obj.canChangeFollowers;
    });
  }


  resetData() {
    this.chatChannelData = undefined;
    this.selectedChnnel = undefined;
  }

  private chatChannelData: any = undefined;
  selectedChnnel: any = '';

  onClickChatChannel(chatChannelData) {
    this.chatChannelData = chatChannelData;
    this.selectedChnnel = chatChannelData.messageContextId;
    this.messageContextId = chatChannelData.messageContextId;
  }


  toggleChatPanel() {
    if (document.getElementById('chatPannel').classList.contains('toggelChatPannelActive')) {
      document.getElementById('chatPannel').classList.toggle('toggelChatPannelActive');
    }
    else {
      document.getElementById('chatPannel').classList.add('toggelChatPannelActive');
    }
  }
}
