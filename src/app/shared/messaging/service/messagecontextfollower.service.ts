import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {
  HttpClient,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {CookieService} from 'ngx-cookie-service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {filter, find, forEach, groupBy, includes, orderBy, remove, replace} from 'lodash';

import {
  CreateMessageContextFollowerDto,
  PagedResultDtoOfMessageContextFollowerDto,
  ParamMessageContextFollower
} from './messaging.model';

import {CommonService} from '../../../shared/services/common.service';
import {ResultDtoOfSelectListDto} from '../../models/selectListDto';
import {environment} from '../../../../environments/environment';

@Injectable()
export class MessageContextFollowerService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService,
              private http: HttpClient, private cookieService: CookieService) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/General/api/MessageContextFollower/';
  }


  createChannelContextFollower(input: CreateMessageContextFollowerDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'InsertAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  createMessageContextFollower(input: CreateMessageContextFollowerDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'InsertFollwersAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  getAllDirectMessageContext(userid: any, messageContextId: number = 0) {
    const url = this.apiBaseUrl + 'GetMessageContextList';
    let filter = '';
    filter += (filter.length > 0) ? ' and ' : '';
    filter += ' CanChangeFollowers eq false';
    filter += (filter.length > 0) ? ' and ' : '';
    filter += ' CreatedBy ne ' + userid;
    filter += (filter.length > 0) ? ' and ' : '';
    filter += ' ContextName eq null';
    if (messageContextId > 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += 'MessageContextId eq ' + messageContextId;
    }
    const params = new HttpParams()
      .set('$filter', filter.toString());


    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      const result200 = PagedResultDtoOfMessageContextFollowerDto.fromJS(response);
      return Observable.of(result200);
    });
  }

  deleteMessageContextFollower(input: any,
                               successCallback: any,
                               errorCallback: any): any {
    const url = this.apiBaseUrl + 'DeleteFollowors';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  getAllMessagesFollowors(messageContextFollowersData,
                          successCallback: any,
                          errorCallback: any): any {
    const url = this.apiBaseUrl + 'GetUserFollowMessageContext';
    this.commonService.httpPost(url, messageContextFollowersData, successCallback, errorCallback);
  }

  addUpdateMembers(paramMessageContextFollower: ParamMessageContextFollower, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'InsertDeleteBulkFollwersAsync';
    this.commonService.httpPost(url, paramMessageContextFollower, successCallback, errorCallback);
  }

  getfollowersListByMessegeContextId(messegeContextId: number): Observable<ResultDtoOfSelectListDto> {
    const url = this.apiBaseUrl + 'GetfollowersListByMessegeContextId';
    const params = new HttpParams().set('messegeContextId', messegeContextId.toString());

    const options = {
      params: params
    };
    return this.http.request('get', url, options).flatMap((response) => {
      const result200 = ResultDtoOfSelectListDto.fromJS(response);
      return Observable.of(result200);
    });
  }
}


