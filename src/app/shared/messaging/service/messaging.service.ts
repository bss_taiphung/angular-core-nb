import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {
  HttpClient,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {
  filter,
  find,
  forEach,
  groupBy,
  includes,
  orderBy,
  remove,
  replace
} from 'lodash';

import {CreateMessageDto, PagedResultDtoOfMessageDto} from './messaging.model';
import {CommonService} from '../../../shared/services/common.service';
import {environment} from '../../../../environments/environment';
import {ApiService} from '../../services/api.service';

@Injectable()
export class MessageService extends ApiService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService, private http: HttpClient, private cookieService: CookieService) {
    super();
    this.apiBaseUrl = environment.bllApiBaseAddress + '/General/api/Message/';
  }

  createMessage(input: CreateMessageDto,
                successCallback: any,
                errorCallback: any): any {
    const url = this.apiBaseUrl + 'InsertAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  update(input: any,
         successCallback: any,
         errorCallback: any): any {
    const url = this.apiBaseUrl + 'UpdateAsync';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  delete(id: any, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'DeleteAsync';
    const params = new HttpParams().set('id', id);

    const options = {
      params: params
    };
    this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
  }


  getAllMessages(messageContextId: number): Observable<PagedResultDtoOfMessageDto> {
    return this.http.get(`${this.apiBaseUrl}GetMessages`, this.getOptions({params: {id: messageContextId}}))
      .map((response) => PagedResultDtoOfMessageDto.fromJS(response))
      .catch(this.handleError);
  }

  getMessagesUnreadCount(userid, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'GetMessagesUnreadCount';
    const macAddress = this.commonService.getMacAddress();

    const params = new HttpParams()
      .set('UserId', userid.toString());

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'MacAddress': macAddress
      }),
      params: params
    };
    this.commonService.httpRequest('get', url, options, successCallback, errorCallback);
  }

}


