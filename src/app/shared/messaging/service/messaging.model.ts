//region IMessageDto

export interface IMessageDto {
  messageId: number;
  messageContextId: number;
  messageText: string;
  creatorEditable: boolean;
  modifiedDateTime: string;
  modifiedBy: any;
  createdBy: any;
  createdDateTime: string;
  createdDateTimeObj: Date;
  modifiedByName: string;
  createdByName: string;
  active: boolean;
}

export class MessageDto implements IMessageDto {
  messageId: number;
  messageContextId: number;
  messageText: string;
  creatorEditable: boolean;
  modifiedDateTime: string;
  modifiedBy: any;
  createdBy: any;
  createdDateTime: string;
  createdDateTimeObj: Date;
  modifiedByName: string;
  createdByName: string;
  active: boolean;

  static fromJS(data: any): MessageDto {
    const result = new MessageDto();
    result.init(data);
    return result;
  }

  constructor(data?: IMessageDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.messageId = data['messageId'];
      this.messageContextId = data['messageContextId'];
      this.messageText = data['messageText'];
      this.creatorEditable = data['creatorEditable'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.modifiedBy = data['modifiedBy'];
      this.createdBy = data['createdBy'];
      this.modifiedByName = data['modifiedByName'];
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDateTimeObj = new Date(data['createdDateTime']);
      this.active = data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['messageId'] = this.messageId;
    data['messageContextId'] = this.messageContextId;
    data['messageText'] = this.messageText;
    data['creatorEditable'] = this.creatorEditable;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['modifiedBy'] = this.modifiedBy;
    data['createdBy'] = this.createdBy;
    data['createdDateTime'] = this.createdDateTime;
    data['createdByName'] = this.createdByName;
    data['modifiedByName'] = this.modifiedByName;
    data['active'] = this.active;

    return data;
  }
}

// endregion

// region ICreateMessageDto
export interface ICreateMessageDto {
  messageId: number;
  messageContextId: number;
  messageText: string;
  creatorEditable: boolean;
  active: boolean;
}

export class CreateMessageDto implements ICreateMessageDto {
  messageId = 0;
  messageContextId: number;
  messageText: string;
  creatorEditable: boolean;
  active = true;

  static fromJS(data: any): MessageDto {
    const result = new MessageDto();
    result.init(data);
    return result;
  }

  constructor(data?: IMessageDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.messageId = 0;
      this.messageContextId = data['messageContextId'];
      this.messageText = data['messageText'];
      this.creatorEditable = (data['creatorEditable'] == null) ? false : data['creatorEditable'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['messageId'] = 0;
    data['messageContextId'] = this.messageContextId;
    data['messageText'] = this.messageText;
    data['creatorEditable'] = (this.creatorEditable == null) ? false : this.creatorEditable;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

// endregion

// region IPagedResultDtoOfMessageDto
export interface IPagedResultDtoOfMessageDto {
  results: MessageDto[];
  count: number;
}

export class PagedResultDtoOfMessageDto implements IPagedResultDtoOfMessageDto {
  results: MessageDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfMessageDto {
    const result = new PagedResultDtoOfMessageDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfMessageDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      this.results = [];
      for (const item of data) {
        this.results.push(MessageDto.fromJS(item));
      }
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }

    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }

}

// endregion

// region IMessageContextDto

export interface IMessageContextDto {
  messageContextId: number;
  module: number;
  objectType: number;
  objectId: number;
  canChangeFollowers: boolean;
  contextName: string;
  modifiedDateTime: string;
  modifiedBy: any;
  createdBy: any;
  createdDateTime: string;
  modifiedByName: string;
  createdByName: string;
  active: boolean;
}

export class MessageContextDto implements IMessageContextDto {
  messageContextId: number;
  module: number;
  objectType: number;
  objectId: number;
  canChangeFollowers: boolean;
  contextName: string;
  modifiedDateTime: string;
  modifiedBy: any;
  createdBy: any;
  createdDateTime: string;
  modifiedByName: string;
  createdByName: string;
  active: boolean;
  chatAvatar: string;
  displayName: string;
  displayId: string;
  imageFileName: string;
  imageFile: string;
  thumbnailFile: string;
  userId: number;

  static fromJS(data: any): MessageContextDto {
    const result = new MessageContextDto();
    result.init(data);
    return result;
  }

  constructor(data?: IMessageContextDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.messageContextId = data['messageContextId'];
      this.module = data['module'];
      this.objectType = data['objectType'];
      this.objectId = data['objectId'];
      this.canChangeFollowers = data['canChangeFollowers'];
      this.contextName = data['contextName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.modifiedBy = data['modifiedBy'];
      this.createdBy = data['createdBy'];
      this.modifiedByName = data['modifiedByName'];
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.displayName = data['displayName'];
      this.displayId = data['displayId'];
      this.imageFileName = data['imageFileName'];
      this.imageFile = data['imageFile'];
      this.thumbnailFile = data['thumbnailFile'];
      this.userId = data['userId'];
      this.active = data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['messageContextId'] = this.messageContextId;
    data['module'] = this.module;
    data['objectType'] = this.objectType;
    data['objectId'] = this.objectId;
    data['canChangeFollowers'] = this.canChangeFollowers;
    data['contextName'] = this.contextName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['modifiedBy'] = this.modifiedBy;
    data['createdBy'] = this.createdBy;
    data['createdDateTime'] = this.createdDateTime;
    data['createdByName'] = this.createdByName;
    data['modifiedByName'] = this.modifiedByName;
    data['displayName'] = this.displayName;
    data['displayId'] = this.displayId;
    data['imageFileName'] = this.imageFileName;
    data['imageFile'] = this.imageFile;
    data['thumbnailFile'] = this.thumbnailFile;
    data['userId'] = this.userId;
    data['active'] = this.active;

    return data;
  }
}

// endregion

// region ICreateMessageContextDto

export interface ICreateMessageContextDto {
  messageContextId: number;
  module: number;
  objectType: number;
  objectId: number;
  canChangeFollowers: boolean;
  contextName: string;
  active: boolean;
}

export class CreateMessageContextDto implements ICreateMessageContextDto {
  messageContextId: number;
  module: number;
  objectType: number;
  objectId: number;
  canChangeFollowers: boolean;
  contextName: string;
  active: boolean;

  static fromJS(data: any): MessageDto {
    const result = new MessageDto();
    result.init(data);
    return result;
  }

  constructor(data?: IMessageContextDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.messageContextId = 0;
      this.module = data['module'];
      this.objectType = data['objectType'];
      this.objectId = data['objectId'];
      this.contextName = data['contextName'];
      this.canChangeFollowers = (data['canChangeFollowers'] == null) ? false : data['canChangeFollowers'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['messageContextId'] = 0;
    data['module'] = this.module;
    data['objectType'] = this.objectType;
    data['objectId'] = this.objectId;
    data['contextName'] = this.contextName;
    data['canChangeFollowers'] = (this.canChangeFollowers == null) ? false : this.canChangeFollowers;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

// endregion

// region IPagedResultDtoOfMessageContextDto

export interface IPagedResultDtoOfMessageContextDto {
  results: MessageContextDto[];
  count: number;
}

export class PagedResultDtoOfMessageContextDto implements IPagedResultDtoOfMessageContextDto {
  results: MessageContextDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfMessageContextDto {
    const result = new PagedResultDtoOfMessageContextDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfMessageContextDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(MessageContextDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }

    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }

}

// endregion

// region IMessageContextFollowerDto

export interface IMessageContextFollowerDto {
  messageContextFollowerId: number;
  messageContextId: number;
  userId: number;
  unreadCount: number;
  module: number;
  objectType: number;
  objectId: number;
  canChangeFollowers: boolean;
  contextName: string;
  modifiedDateTime: string;
  modifiedBy: any;
  createdBy: any;
  displayId: number;
  displayName: string;
  createdDateTime: string;
  modifiedByName: string;
  createdByName: string;
  active: boolean;
}

export class MessageContextFollowerDto implements IMessageContextFollowerDto {
  messageContextFollowerId: number;
  messageContextId: number;
  userId: number;
  unreadCount: number;
  module: number;
  objectType: number;
  objectId: number;
  canChangeFollowers: boolean;
  contextName: string;
  modifiedDateTime: string;
  modifiedBy: any;
  createdBy: any;
  displayId: number;
  displayName: string;
  createdDateTime: string;
  modifiedByName: string;
  createdByName: string;
  active: boolean;

  static fromJS(data: any): MessageContextFollowerDto {
    const result = new MessageContextFollowerDto();
    result.init(data);
    return result;
  }

  constructor(data?: IMessageContextFollowerDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.messageContextFollowerId = data['messageContextFollowerId'];
      this.messageContextId = data['messageContextId'];
      this.module = data['module'];
      this.objectType = data['objectType'];
      this.objectId = data['objectId'];
      this.unreadCount = data['unreadCount'];
      this.canChangeFollowers = data['canChangeFollowers'];
      this.contextName = data['contextName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.modifiedBy = data['modifiedBy'];
      this.createdBy = data['createdBy'];
      this.modifiedByName = data['modifiedByName'];
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.displayName = data['displayName'];
      this.displayId = data['displayId'];
      this.userId = data['userId'];
      this.active = data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['messageContextFollowerId'] = this.messageContextFollowerId;
    data['messageContextId'] = this.messageContextId;
    data['module'] = this.module;
    data['objectType'] = this.objectType;
    data['objectId'] = this.objectId;
    data['unreadCount'] = this.unreadCount;
    data['canChangeFollowers'] = this.canChangeFollowers;
    data['contextName'] = this.contextName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['modifiedBy'] = this.modifiedBy;
    data['createdBy'] = this.createdBy;
    data['createdDateTime'] = this.createdDateTime;
    data['createdByName'] = this.createdByName;
    data['modifiedByName'] = this.modifiedByName;
    data['displayName'] = this.displayName;
    data['displayId'] = this.displayId;
    data['userId'] = this.userId;
    data['active'] = this.active;

    return data;
  }
}

// endregion


// region ICreateMessageContextFollowerDto

export interface ICreateMessageContextFollowerDto {
  messageContextFollowerId: number;
  messageContextId: number;
  createdBy: any;
  active: boolean;
}

export class CreateMessageContextFollowerDto implements ICreateMessageContextFollowerDto {
  messageContextFollowerId: number;
  messageContextId: number;
  createdBy: any;
  active: boolean;

  static fromJS(data: any): MessageContextFollowerDto {
    const result = new MessageContextFollowerDto();
    result.init(data);
    return result;
  }

  constructor(data?: IMessageContextFollowerDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.messageContextFollowerId = 0;
      this.messageContextId = data['messageContextId'];
      this.createdBy = data['createdBy'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['messageContextFollowerId'] = 0;
    data['messageContextId'] = this.messageContextId;
    data['createdBy'] = this.createdBy;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

// endregion

// region IPagedResultDtoOfMessageContextFollowerDto

export interface IPagedResultDtoOfMessageContextFollowerDto {
  results: MessageContextFollowerDto[];
  count: number;
}

export class PagedResultDtoOfMessageContextFollowerDto implements IPagedResultDtoOfMessageContextFollowerDto {
  results: MessageContextFollowerDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfMessageContextFollowerDto {
    const result = new PagedResultDtoOfMessageContextFollowerDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfMessageContextFollowerDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(MessageContextFollowerDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }

    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }

}

// endregion

// region IParamMessageContextFollower

export interface IParamMessageContextFollower {
  messageContextFollowerId: number;
  messageContextId: number;
  membersList: any;
}

export class ParamMessageContextFollower implements IParamMessageContextFollower {
  messageContextFollowerId: number;
  messageContextId: number;
  membersList: any;

  static fromJS(data: any): ParamMessageContextFollower {
    const result = new ParamMessageContextFollower();
    result.init(data);
    return result;
  }

  constructor(data?: IParamMessageContextFollower) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.messageContextFollowerId = data['messageContextFollowerId'];
      this.messageContextId = data['messageContextId'];
      this.membersList = data['membersList'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['messageContextFollowerId'] = this.messageContextFollowerId;
    data['messageContextId'] = this.messageContextId;
    data['membersList'] = this.membersList;

    return data;
  }

}

// endregion

export enum ChatObjectType {
  Project = 0,
  Task = 1,
  HoldTag = 2,
  Concern = 3,
  Quote = 4,
  RFQ = 5,
  Part = 6,
  Customer = 7,
  CustomerPart = 8,
  LogEntry = 9,
  WIPID = 10,
  MaintenanceOrder = 11,
  Process = 12,
  WorkOrder = 13,
  ProductionStandard = 14,
  Area = 15,
  Department = 16,
  Team = 17,
  Site = 18,
  User = 19,
  None = 20
}

export enum ChatModuleType {
  Documents = 0,
  ProcessLog = 1,
  QualityManagementSystem = 2,
  EquipmentMaintenance = 3,
  ToolingManagementSystem = 4,
  Projects = 5,
  SalesQuoting = 6,
  General = 7
}
