import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {
  CreateMessageContextDto,
  PagedResultDtoOfMessageContextDto
} from './messaging.model';
import {CommonService} from '../../../shared/services/common.service';
import {
  filter,
  find,
  forEach,
  groupBy,
  includes,
  orderBy,
  remove,
  replace
} from 'lodash';
import {CookieService} from 'ngx-cookie-service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class MessageContextService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService, private http: HttpClient, private cookieService: CookieService) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/General/api/MessageContext/';
  }


  getUnreadNotifications(userid, successCallback: any, errorCallback: any): any {

    const url = this.apiBaseUrl + 'GetUnreadCounts';
    const macAddress = this.commonService.getMacAddress();

    const params = new HttpParams()
      .set('UserId', userid.toString());

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'MacAddress': macAddress
      }),
      params: params
    };
    this.commonService.httpRequest('get', url, options, successCallback, errorCallback);
  }

  getAllDirectMessageFolloerList(userid, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'GetDirectFollowerList';
    const macAddress = this.commonService.getMacAddress();

    const params = new HttpParams()
      .set('UserId', userid.toString());

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'MacAddress': macAddress
      }),
      params: params
    };
    this.commonService.httpRequest('get', url, options, successCallback, errorCallback);
  }

  getAllChatChannelList(userid, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'GetUnreadCounts';
    const macAddress = this.commonService.getMacAddress();

    const params = new HttpParams()
      .set('UserId', userid.toString());

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'MacAddress': macAddress
      }),
      params: params
    };
    this.commonService.httpRequest('get', url, options, successCallback, errorCallback);
  }

  create(input: CreateMessageContextDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'InsertAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }
  update(input: CreateMessageContextDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'UpdateAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  deleteMessageContext(id: any, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'DeleteAsync';
    const params = new HttpParams().set('id', id);
    const options = {
      params: params
    };
    this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
  }

  getMessageContextCreator(messageContextData, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'IsUserCreateMessageContext';
    this.commonService.httpPost(url, messageContextData, successCallback, errorCallback);
  }

  getMessageContextDetails(messageContextId: number = 0,
                           objectType: number = 0,
                           objectId: number = 0,
                           module: number = 0,
                           contextFlag: boolean = false) {
    const url = this.apiBaseUrl + 'GetMessageContextList';
    let filter = '';

    if (messageContextId > 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += 'MessageContextId eq ' + messageContextId;
    } else {
      if (!contextFlag) {
        filter += 'ContextName ne null';
        filter += (filter.length > 0) ? ' and ' : '';
        filter += 'CanChangeFollowers eq true';
      }
    }
    if (objectType > 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += 'ObjectType eq ' + objectType;
    }
    if (objectId > 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += 'ObjectId eq ' + objectId;
    }
    if (module > 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += 'Module eq ' + module;
    }

    const params = new HttpParams()
      .set('$filter', filter.toString());


    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {

      const result200 = PagedResultDtoOfMessageContextDto.fromJS(response);
      return Observable.of(result200);
    });
  }
}

