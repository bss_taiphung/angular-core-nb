import {
  AfterViewInit, ChangeDetectionStrategy,
  Component,
  Input
} from '@angular/core';

import {MessageService} from '../service/messaging.service'
import {MessageDto} from '../service/messaging.model'
import {ToasterHelperService} from '../../services/toasterHelper.service'
import {EqmSettingsService} from '../../services/eqmsettings.service'

@Component({
  selector: 'chat-message',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./chatmessage.component.css'],
  templateUrl: './chatmessage.component.html'
})

export class ChatMessageComponent implements AfterViewInit {
  private userId: number;
  isEdit = false;
  commentButtonName = 'Send Message';

  constructor(private messageService: MessageService,
              private eqmSettingsService: EqmSettingsService,
              private toasterService: ToasterHelperService) {
    this.userId = this.eqmSettingsService.getUser().userId;
  }

  ngAfterViewInit(): void {
    let ele: any = document.getElementsByClassName('k-dialog-content')[0];
    ele.style.overflow = 'hidden';
    ele.style.height = '100vh';
  }

  lastMonth = new Date();
  message: any = [];

  @Input()
  public set model(chatData: any) {
    if (chatData !== undefined) {
      this.message = chatData;
      this.lastMonth.setMonth(this.lastMonth.getMonth() - 1);
      this.commentButtonName = 'Update';
    }
  }

  userAvatar: any = '';

  updateUserAvatarToDefault() {
    this.userAvatar = 'assets/img/no_avatar.png';
  }

  private editChatMessage: any;
  private selectedMessgage: any = null;

  editTextboxMessage(message) {
    this.editChatMessage = message.messageText;
    this.selectedMessgage = message;
    this.isEdit = true;
  }

  updateMessge(updateMessage) {
    if (updateMessage === '') {
      this.isEdit = false;
      return;
    }
    if (this.selectedMessgage != null && this.selectedMessgage != undefined) {
      this.isEdit = false;
      const updateData: MessageDto = new MessageDto();
      updateData.messageId = this.selectedMessgage.messageId;
      updateData.messageContextId = this.selectedMessgage.messageContextId;
      updateData.creatorEditable = true;
      updateData.active = true;
      updateData.messageText = updateMessage;
      this.messageService.update(updateData,
        response => {
          //  this.toasterService.success("", 'Message updated Successfully');
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  deleteMessage(message) {
    this.isEdit = false;
    this.messageService.delete(message.messageId,
      response => {
        if (response) {
          this.toasterService.success('', 'Message removed');
          document.getElementById('message' + this.selectedMessgage.messageId).innerHTML =
            'This message has been removed';
        } else {
          this.toasterService.error('', 'you cannot delete this message');
        }

      },
      error => {
        this.toasterService.errorMessage(error);
      }
    );
  }
}
