import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import {HubConnection} from '@aspnet/signalr-client';

import {MessageContextService} from '../service/messagecontext.service'
import {MessageContextFollowerService} from '../service/messagecontextfollower.service'
import {
  ChatModuleType,
  CreateMessageContextDto,
  CreateMessageContextFollowerDto
} from '../service/messaging.model'
import {ToasterHelperService} from '../../services/toasterHelper.service';
import {CommonService} from '../../services/common.service';
import {EqmSettingsService} from '../../services/eqmsettings.service'

@Component({
  selector: 'kendo-New-channels',
  templateUrl: './newchannel.component.html'
})
export class NewChannelFormComponent implements OnInit {


  private hubConnection: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  private user: any = null;

  constructor(private messageContextService: MessageContextService,
              private messageContextFollowerService: MessageContextFollowerService,
              private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService) {

  }

  ngOnInit(): void {
    this.user = this.eqmSettingsService.getUser();
  }

  @Input() public createChannelOpen = false;
  @Output() closeNewChannelDialog: EventEmitter<any> = new EventEmitter();
  @Output() newChannelCreated: EventEmitter<any> = new EventEmitter();


  private closeForm(): void {
    this.createChannelOpen = false;
    this.closeNewChannelDialog.emit();
  }

  public channelForm: FormGroup = new FormGroup({
    'messageContextId': new FormControl(),
    'contextName': new FormControl('', Validators.required),
    'active': new FormControl(false),
  });


  onClickChatChannel() {
    const inputdata: CreateMessageContextDto = this.channelForm.value;
    inputdata.objectId = null;
    inputdata.objectType = null;
    inputdata.module = ChatModuleType.ToolingManagementSystem;
    inputdata.canChangeFollowers = true;
    inputdata.active = true;
    this.messageContextService.create(inputdata,
      response => {
        const inputFollwerData: CreateMessageContextFollowerDto = new CreateMessageContextFollowerDto();
        inputFollwerData.messageContextId = response.messageContextId;
        inputFollwerData.active = true;
        this.messageContextFollowerService.createChannelContextFollower(inputFollwerData,
          response => {
            this.toasterService.success('', 'Channel Create Successfully');
            this.channelForm.reset();
            this.closeForm();
          },
          error => {
            this.toasterService.errorMessage(error);
          });
      },
      error => {
        this.toasterService.errorMessage(error);
      }
    );
  }
}
