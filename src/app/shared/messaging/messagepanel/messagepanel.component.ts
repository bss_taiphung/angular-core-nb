import {
  AfterViewInit, ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostListener,
  Input,
  Output
} from '@angular/core';
import {HubConnection} from '@aspnet/signalr-client';

import {
  filter,
  forEach,
  groupBy,
  includes,
  orderBy,
  remove,
  replace
} from 'lodash';

import {FormControl, FormGroup} from '@angular/forms';
import {MessageService} from '../service/messaging.service'
import {MessageContextService} from '../service/messagecontext.service'
import {MessageContextFollowerService} from '../service/messagecontextfollower.service'
import {ToasterHelperService} from '../../services/toasterHelper.service'
import {EqmSettingsService} from '../../services/eqmsettings.service'
import {
  CreateMessageContextDto,
  CreateMessageDto,
  MessageContextDto,
  MessageContextFollowerDto,
  MessageDto,
  ParamMessageContextFollower
} from '../service/messaging.model'
import {CommonService} from '../../services/common.service'
import {SelectListDto} from '../../../shared/models/selectListDto';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'message-panel',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./messagepanel.component.css'],
  templateUrl: './messagepanel.component.html'
})
export class MessagePanelComponent implements AfterViewInit {
  userId: number;

  private lastMonth = new Date();
  chatLoader = false;
  private hubConnection: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  public azureStorageContainer: string;
  private channelMessageList: any = [];

  messagesList: any = [];
  private selectedMessageContextId = 0;
  public month = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'November', 'Dec'];
  groups = [];
  filterUserIdList: SelectListDto[] = [];
  private userList: SelectListDto[] = [];

  isMessageShow = false;
  _objectId = 0;

  chatContextDetails: MessageContextDto = new MessageContextDto();
  @Input()
  public moduleName = 0;
  @Input()
  public objectType = 0;

  @Input()
  public set objectId(objectIdData: number) {
    if (!!objectIdData) {
      this.channelMessageList = [];
      this._objectId = objectIdData;
      this.getMessageContextForObject();
    }
  }

  @Input()
  public set chatChannelData(contextDetails: any) {
    if (contextDetails !== undefined) {
      this.resetFlags();
      this.channelMessageList = [];
      this.chatContextDetails = MessageContextDto.fromJS(contextDetails);
      this.selectedMessageContextId = this.chatContextDetails.messageContextId;
      if (this.chatContextDetails.contextName == null && this.chatContextDetails.contextName == undefined) {
        if (this.chatContextDetails.messageContextId !== undefined) {
          this.chatContextDetails.chatAvatar = this.commonService.getUserThumbnail(this.chatContextDetails.displayId);
        }
        this.chatContextDetails.createdByName = this.chatContextDetails.displayName;
      } else {
        if (this.chatContextDetails.messageContextId !== undefined) {
          this.chatContextDetails.chatAvatar = this.commonService.getUserThumbnail(this.chatContextDetails.messageContextId.toString());
        }
      }
      this.getChatMessage(this.chatContextDetails.messageContextId);
      //this.getMessageContext(this.selectedMessageContextId);
    }
  }


  constructor(public commonService: CommonService,
              private toasterService: ToasterHelperService,
              private messageService: MessageService,
              private messageContextService: MessageContextService,
              private messageContextFollowerService: MessageContextFollowerService,
              private eqmSettingsService: EqmSettingsService) {
    this.userId = this.eqmSettingsService.getUser().userId;
    this.azureStorageContainer = environment.azureStorageContainer;
    this.lastMonth.setMonth(this.lastMonth.getMonth() - 1);
  }

  ngAfterViewInit(): void {
    const ele: any = document.getElementsByClassName('k-dialog-content')[0];
    ele.style.overflow = 'hidden';
    this.scrollTimeout = setTimeout(() => {
      this.scrollDownToChat();
    }, 500);
    if (this._objectId == undefined || this._objectId === 0) {
      if (document.getElementById('msgChatHeader').classList.contains('msgChatHeader')) {
        document.getElementById('msgChatHeader').classList.toggle('msgChatHeader');
      }
      document.getElementById('messagePanel').classList.toggle('scrollBarMeaasgeBoxPageDetail');
    } else {
      document.getElementById('messagePanel').classList.toggle('scrollBarMeaasgeBox');
    }


    this.commonService.getAll('General', 'User').subscribe(eventResult => {
      eventResult.results.forEach((element, i) => {
        if (element.value.toString() === this.userId.toString()) {
          eventResult.results.splice(i, 1);
        }
      });
      if (eventResult.results != null && eventResult.results.length > 0) {
        eventResult.results.forEach(itm => {
          if (itm.userAvatar == null) {
            itm.userAvatar = this.commonService.getUserThumbnail(itm.value);
          }
        });
      }
      this.filterUserIdList = this.userList = eventResult.results;
    });

  }

  messageContextSignalR(id) {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started User Chat Context Follwer !');

        this.hubConnection
          .invoke('Subscribe', 'General', 'Message', 'MessageContextId', id)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated',
      (groupName: string, operation: string, messageJson: string) => {
        const _responseText = messageJson;
        let result200: any = null;
        const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
        const resultData200 = this.commonService.toCamel(data);

        result200 = resultData200 ? MessageDto.fromJS(resultData200) : new MessageDto();
        result200.chatAvatar = this.commonService.getUserThumbnail(result200.createdBy);
        let dataExist = this.containsObject(result200, this.channelMessageList);
        result200.chatAvatar = this.commonService.getUserThumbnail(result200.createdBy);
        if (operation.toLowerCase() === 'insert' && !dataExist && result200.messageContextId === this.selectedMessageContextId) {
          const length = this.channelMessageList.length;
          if (this.channelMessageList[length - 1].messageId === 0
            && this.channelMessageList[length - 1].messageText === result200.messageText) {
            this.channelMessageList[length - 1].messageId = result200.messageId;
            operation = 'update';
            dataExist = true;
          } else {
            this.channelMessageList.push(result200);
            this.updateChatMessages();
          }
        }

        if (operation.toLowerCase() === 'update'
          && dataExist
          && result200.messageContextId === this.selectedMessageContextId) {
          this.channelMessageList.forEach((element, index) => {
            if (element.messageId === result200.messageId) {
              this.channelMessageList[index] = result200;
              this.updateChatMessages();
            }
          });
        }

        if (operation.toLowerCase() === 'delete' && dataExist) {
          let index = null;
          this.channelMessageList.forEach((element, i) => {
            if (element.messageId === result200.messageId) {
              index = i;
            }
          });
          if (index !== null) {
            this.channelMessageList.splice(index, 1);
            this.updateChatMessages();
          }
        }
      });
  }

  containsObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].messageId === obj.messageId) {
        return true;
      }
    }

    return false;
  }

  containsMessageObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].createdDateTime === obj.createdDateTime && list[x].messageId === 0) {
        return true;
      }
    }

    return false;
  }

  private getMessageContext(messageContexId) {
    this.chatLoader = true;
    this.messageContextService.getMessageContextDetails(messageContexId).subscribe(eventResult => {
        this.chatLoader = false;
        if (eventResult.results.length > 0) {
          this.chatContextDetails = eventResult.results[0];

          if (this.chatContextDetails.contextName == null) {
            this.chatContextDetails.createdByName = null;
            this.getDirectMessageDetail(messageContexId);
            this.getChatMessage(this.selectedMessageContextId);
          } else {
            this.chatContextDetails.chatAvatar = this.commonService.getUserThumbnail(this.chatContextDetails.messageContextId.toString());
            this.getChatMessage(this.selectedMessageContextId);
          }
        }
      },
      error => {
        this.chatLoader = false;
      }
    );
  }

  private getMessageContextForObject() {
    this.chatLoader = true;
    this.messageContextService.getMessageContextDetails(
      0,
      this.objectType,
      this._objectId,
      this.moduleName,
      true)
      .subscribe(eventResult => {
        this.chatLoader = false;
        if (eventResult.results.length === 0) {
          this.isMessageShow = true;
        }
        else {
          this.chatContextDetails = eventResult.results[0];
          this.selectedMessageContextId = this.chatContextDetails.messageContextId;
          this.getChatMessage(this.selectedMessageContextId);
        }
      });
  }

  private getDirectMessageDetail(messageContexId) {
    this.chatLoader = true;
    this.messageContextFollowerService.getAllDirectMessageContext(this.userId, messageContexId).subscribe(
      eventResult => {
        if (eventResult.results.length > 0) {
          this.chatContextDetails.createdByName = eventResult.results[0].createdByName;
          // this.chatContextDetails.chatAvatar = this.commonService.getUserThumbnail(eventResult.results[0].);
        }
        this.chatLoader = false;
      });
  }


  private getChatMessage(messageContextId) {
    this.messageContextSignalR(messageContextId);
    this.chatLoader = true;
    this.messageService.getAllMessages(messageContextId)
      .finally(() => {
        this.chatLoader = false;
        this.isMessageShow = true;
      })
      .subscribe(eventResult => {
        this.channelMessageList = eventResult.results;
        this.updateChatMessages();
      });
  }

  private scrollTimeout = null;

  updateChatMessages() {
    this.channelMessageList.forEach((item: any, index: number) => {
      item.chatAvatar = this.commonService.getUserThumbnail(item.createdBy);
      const day = new Date(item.createdDateTime).getDate();
      const month = this.month[new Date(item.createdDateTime).getMonth()];
      const year = new Date(item.createdDateTime).getFullYear();
      const hours = ('0' + (new Date(item.createdDateTime).getHours())).slice(-2);
      const minute = ('0' + (new Date(item.createdDateTime).getMinutes())).slice(-2);
      this.channelMessageList[index].orderByDate = `${month} ${day}, ${year}`;
      this.channelMessageList[index].dateTime = `${hours}:${minute}`;
    });
    this.messagesList = groupBy(this.channelMessageList, 'orderByDate');
    this.groups = Object.getOwnPropertyNames(this.messagesList);
    let priviousUserID = 0;
    for (const group in this.messagesList) {
      const message = this.messagesList[group];
      // if (this._objectId === 0 || this._objectId === undefined)
      this.messagesList[group] = orderBy(message, ['createdDateTimeObj'], ['asc']);
      priviousUserID = 0;
      for (const index in this.messagesList[group]) {
        if (priviousUserID === 0 || priviousUserID !== this.messagesList[group][index].createdBy) {
          priviousUserID = this.messagesList[group][index].createdBy;
          this.messagesList[group][index].isShow = true;
        } else {
          this.messagesList[group][index].isShow = false;
        }
        //else
        //this.messagesList[group] = orderBy(message, ["createdDateTimeObj"], ["desc"]);
      }

      this.scrollTimeout = setTimeout(() => {
        this.scrollDownToChat();
      }, 500);

    }
    this.chatLoader = false;
  }


  @Output()
  removeChatChannel: EventEmitter<any> = new EventEmitter();

  scrollDownToChat() {
    const messagePanelDiv = document.getElementById('messagePanel');
    if (messagePanelDiv != null) {
      messagePanelDiv.scrollTop = messagePanelDiv.scrollHeight + 80;
    }
  }

  sendMessageToChannel(sendMessage) {
    const inputdata: CreateMessageDto = new CreateMessageDto();
    if (this.chatContextDetails.messageContextId > 0) {
      inputdata.messageContextId = this.chatContextDetails.messageContextId;
      inputdata.messageText = sendMessage;
      inputdata.creatorEditable = true;
      inputdata.active = true;

      const msgData: MessageDto = new MessageDto();
      msgData.messageId = 0;
      msgData.messageContextId = inputdata.messageContextId;
      msgData.messageText = inputdata.messageText;
      msgData.creatorEditable = inputdata.creatorEditable;
      msgData.active = inputdata.active;
      msgData.createdDateTimeObj = new Date();
      msgData.createdDateTime = msgData.createdDateTimeObj.toString();
      msgData.createdBy = this.userId;
      msgData.createdByName = this.eqmSettingsService.getUser().displayName;
      if (this.channelMessageList.length) {
        this.channelMessageList[this.channelMessageList.length - 1].creatorEditable = false;
      }
      this.channelMessageList.push(msgData);
      this.updateChatMessages();
      this.messageService.createMessage(inputdata,
        response => {
          // this.toasterService.success("", "Message Send Successfully");
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    } else {
      const inputdata: CreateMessageContextDto = new CreateMessageContextDto();
      inputdata.objectId = this._objectId;
      inputdata.objectType = this.objectType;
      inputdata.module = this.moduleName;
      inputdata.canChangeFollowers = false;
      inputdata.active = true;
      this.messageContextService.create(inputdata,
        response => {
          this.chatContextDetails.messageContextId = response.messageContextId;
          this.selectedMessageContextId = this.chatContextDetails.messageContextId;
          this.sendMessageToChannel(sendMessage);
          this.getChatMessage(this.chatContextDetails.messageContextId);
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }

  }

  resetFlags() {
    this.chatContextDetails = new MessageContextDto();
    this.isMessageShow = false;
    this.selectedMessageContextId = 0;
    this.channelMessageList = [];
    this.messagesList = [];
    this.groups = [];
    this.chatHeader = '';
  }

  updateMessageContextName(event, value) {
    if (event.keyCode === 13) {
      if (value !== null) {
        const inputdata: CreateMessageContextDto = this.chatContextDetails;
        inputdata.contextName = value;
        this.messageContextService.update(inputdata,
          response => {
            this.toasterService.success('', 'Channel Name Update Successfully');
          },
          error => {
            this.toasterService.errorMessage(error);
          }
        );
      }
    }
  }

  openActionMenu = false;

  openActionMenuClick() {
    this.openActionMenu = !this.openActionMenu;
  }


  /**
   follower
   */
  isAddMembersDialogOpen = false;
  panelMembers: SelectListDto[] = [];
  isAddMembersLoaded = false;

  addMembersPopupOpen() {
    this.getSelectedMembers();
    this.isAddMembersDialogOpen = true;

  }

  addMembersPopupClose() {
    this.isAddMembersLoaded = false;
    this.isAddMembersDialogOpen = false;
  }

  onMembersSave() {
    const users = this.panelMembers.map(a => a.value);
    const paramMessageContextFollower: ParamMessageContextFollower = new ParamMessageContextFollower();
    paramMessageContextFollower.membersList = users;
    paramMessageContextFollower.messageContextId = this.selectedMessageContextId;
    this.isAddMembersLoaded = true;
    this.messageContextFollowerService.addUpdateMembers(paramMessageContextFollower,
      success => {
        if (success.toString().toLocaleLowerCase() === 'true') {
          this.toasterService.success('Follower list updated', 'Messege list updated successfully.');
          this.addMembersPopupClose();
        } else {
          this.toasterService.error('Follower list failed', 'Messege list not updated. Please try againe.');
          this.isAddMembersLoaded = false;
        }

      },
      error => {
        this.toasterService.error('Server error', 'Follower list failed');
        this.isAddMembersLoaded = false;
      });
  }

  getSelectedMembers() {
    this.isAddMembersLoaded = true;
    this.messageContextFollowerService.getfollowersListByMessegeContextId(this.selectedMessageContextId).subscribe(
      eventResult => {
        eventResult.results.forEach((element, i) => {
          if (element.value.toString() === this.userId.toString()) {
            eventResult.results.splice(i, 1);
          }
        });
        if (eventResult.results != null && eventResult.results.length > 0) {
          eventResult.results.forEach(itm => {
            if (itm.userAvatar == null) {
              itm.userAvatar = this.commonService.getUserThumbnail(itm.value);
            }
          });
        }
        this.panelMembers = eventResult.results;
        this.isAddMembersLoaded = false;
      });

  }

  /*Delete Group OR follower remove  */
  isDeleteConformation = false;

  removeChannel() {
    this.isAddMembersLoaded = true;
    if (this.chatContextDetails.createdBy === this.userId) {  // To remove Channel
      this.messageContextService.deleteMessageContext(this.selectedMessageContextId,
        successCallback => {
          this.resetFlags();
          this.toasterService.success('', 'Channel Remove Successfully');
          this.removeChatChannel.emit();
          this.isAddMembersLoaded = false;
        },
        errorCallback => {
          this.toasterService.errorMessage(errorCallback);
          this.isAddMembersLoaded = false;
        });
    } else { // To remove follower from Channel;
      const inputFollwerData: MessageContextFollowerDto = new MessageContextFollowerDto();
      inputFollwerData.messageContextId = this.selectedMessageContextId;
      inputFollwerData.createdBy = this.eqmSettingsService.getUser().userId;
      this.messageContextFollowerService.deleteMessageContextFollower(inputFollwerData,
        successCallback => {
          this.isMessageShow = false;
          this.toasterService.success('', 'Channel Unfollowed');
          this.isAddMembersLoaded = false;
          this.onCloseDeleteConfirmation();
        },
        errorCallback => {
          this.toasterService.errorMessage(errorCallback);
          this.isAddMembersLoaded = false;
        });
    }
  }

  onChannelDelete() {
    this.openActionMenu = !this.openActionMenu;
    this.isDeleteConformation = true;
  }

  onCloseDeleteConfirmation() {
    this.isDeleteConformation = false;
  }


  userAvatar: any = '';

  updateUserAvatarToDefault() {
    this.userAvatar = 'assets/img/no_avatar.png';
  }

  isUpdateChannelIcon = false;

  updateChannelIconForm: FormGroup = new FormGroup({
    'uploadUserImg': new FormControl(),
    'imageFileName': new FormControl(),
    'imageFile': new FormControl(),
    'contextName': new FormControl()
  });

  onChannelUpdateIcon() {
    this.openActionMenu = !this.openActionMenu;
    this.isUpdateChannelIcon = true;
    this.updateChannelIconForm.controls['contextName'].setValue(this.chatContextDetails.contextName);
    const newstr = this.commonService.getChannelThumbnail(this.chatContextDetails.messageContextId.toString(), 125, 125);
    this.base64ImgFile = newstr;
  }

  onCloseChannelUpdateIcon() {
    this.isUpdateChannelIcon = false;
  }

  loading = false;

  updateChannelIcon(e) {
    e.preventDefault();
    this.loading = true;
    const inputData: MessageContextDto = new MessageContextDto();
    inputData.messageContextId = this.chatContextDetails.messageContextId;
    inputData.contextName = this.updateChannelIconForm.controls['contextName'].value;
    inputData.imageFile = this.updateChannelIconForm.controls['imageFile'].value;
    inputData.imageFileName = this.updateChannelIconForm.controls['imageFileName'].value;
    this.messageContextService.update(inputData,
      response => {
        this.loading = false;
        this.isUpdateChannelIcon = false;
        this.toasterService.success('', 'Channel Details Updated');
      },
      error => {
        this.loading = false;
        this.toasterService.errorMessage(error);
      }
    );
  }

  public base64ImgFile: string;

  public onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        const filetype = file.name.split('.')[1];
        this.updateChannelIconForm.controls['imageFile'].setValue(reader.result.split(',')[1]);
        this.updateChannelIconForm.controls['imageFileName'].setValue(file.name);
        // this.readFiles(file);
        const imgFile = reader.result.split(',')[1];
        this.base64ImgFile = 'data:image/' + filetype + ';base64,' + imgFile;
      };
    }
  }

  chatHeader = '';

  @HostListener('window:scroll', ['$event'])
  onScroll(event: any) {

    const childArray = [];
    let countdata = 0; // event.target.scrollHeight;
    for (let i = 0; i < event.target.children.length; i++) {
      countdata = countdata + event.target.children[i].scrollHeight;
      childArray.push(countdata);
    }

    let childIndex = 0;
    for (let i = childArray.length - 1; i >= 0; i--) {
      if (i === 0) {
        if (childArray[i] < event.target.scrollTop) {
          childIndex = i;
          break;
        }
      } else if (event.target.scrollTop > childArray[i - 1] && event.target.scrollTop < childArray[i]) {
        childIndex = i;
        break;
      }
    }
    const ele = event.target.children[childIndex];
    if (ele.getElementsByClassName('tw-date-header__text')[0] !== undefined) {
      this.chatHeader = ele.getElementsByClassName('tw-date-header__text')[0].innerText;
    }
  }

  getPos(el) {
    let topPos
    for (topPos = 0;
         el != null;
         topPos += el.offsetTop, el = el.offsetParent) {
    }
    console.log('top ' + topPos);
  }
}
