import {
  AfterViewInit, ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';

import {CommonService} from '../../../shared/services/common.service';
import {SelectListDto} from '../../../shared/models/selectListDto';
import {MessageContextService} from '../service/messagecontext.service'
import {MessageContextFollowerService} from '../service/messagecontextfollower.service'
import {
  ChatModuleType,
  CreateMessageContextDto,
  CreateMessageContextFollowerDto
} from '../service/messaging.model'
import {ToasterHelperService} from '../../services/toasterHelper.service';
import {EqmSettingsService} from '../../services/eqmsettings.service'
import {HubConnection} from '@aspnet/signalr-client';

@Component({
  selector: 'kendo-directmessage',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './directmessage.component.html'
})
export class DirectmessageFormComponent implements OnInit, AfterViewInit {


  private hubConnection: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  private userId = 0;

  constructor(private commonService: CommonService,
              private toasterService: ToasterHelperService,
              private messageContextService: MessageContextService,
              private messageContextFollowerService: MessageContextFollowerService,
              private eqmSettingsService: EqmSettingsService) {
  }

  private selectedUser: any;
  public userList: SelectListDto[];
  public filterUserIdList: SelectListDto[];

  ngOnInit(): void {
    this.userId = this.eqmSettingsService.getUser().userId;
  }

  ngAfterViewInit(): void {
    this.commonService.getAll('General', 'User')
      .subscribe(eventResult => {
      eventResult.results.forEach((element, i) => {
        if (element.value.toString() === this.userId.toString()) {
          eventResult.results.splice(i, 1);
        }
      });
      this.filterUserIdList = this.userList = eventResult.results;
    });
  }

  @Input() public directmessageDialogActive = false;

  @Output() closeDirectmessageDialog: EventEmitter<any> = new EventEmitter();
  @Output() newChannelCreated: EventEmitter<any> = new EventEmitter();

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.directmessageDialogActive = false;
    this.closeDirectmessageDialog.emit();
  }

  private chatChannelOpen = false;

  userValid = false;

  private onClickMessageChannel() {
    if (this.selectedUser !== undefined) {
      const inputdata: CreateMessageContextDto = new CreateMessageContextDto();
      inputdata.module = ChatModuleType.EquipmentMaintenance;
      inputdata.canChangeFollowers = false;
      inputdata.active = true;
      this.messageContextService.create(inputdata,
        response => {
          const inputFollwerData: CreateMessageContextFollowerDto = new CreateMessageContextFollowerDto();
          inputFollwerData.messageContextId = response.messageContextId;
          inputFollwerData.createdBy = this.selectedUser;
          inputFollwerData.active = true;
          this.messageContextFollowerService.createMessageContextFollower(inputFollwerData,
            response => {
              this.toasterService.success('', 'Message Create Successfully');
              this.selectedUser = null;
              this.directmessageDialogActive = false;
              this.closeDirectmessageDialog.emit(true);
            },
            error => {
              this.toasterService.errorMessage(error);
            });
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    } else {
      this.userValid = true;
    }
  }

  onUserSelectionChange(selectedUser) {
    this.selectedUser = selectedUser;
  }

  userHandleFilter(filter: any) {
    this.filterUserIdList = this.userList
      .filter((u) => u.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }
}
