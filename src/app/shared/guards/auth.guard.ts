import {Injectable} from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot
} from '@angular/router';
import {CommonService} from '../services/common.service'
import {PermissionService} from '../services/permission.service'
import {EqmSettingsService} from '../services/eqmsettings.service'

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private permissionService: PermissionService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    let url = state.url.split('/');
    if (this.commonService.getCookies('usertoken')) {
      if (state.url !== '/') {
        this.permissionService.checkPermission(state.url);
        return true;
      }
      // return true;
    }

    if (state.url.trim() !== '/') {
      this.router.navigate(['/auth/login'], {queryParams: {returnUrl: state.url}});
    } else {
      this.router.navigate(['/auth/login']);
    }
  }
}
