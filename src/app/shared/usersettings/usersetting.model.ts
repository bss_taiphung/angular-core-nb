import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

// region IUserSettingsDto

export interface IUserSettingsDto {
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
  userSettingsId: number;
  userId: number;
  roleId: number;
  currentSiteId: number;
  showOnlyMyAssignments: boolean;
}

export class UserSettingsDto implements IUserSettingsDto {
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
  userSettingsId: number;
  userId: number;
  roleId: number;
  currentSiteId: number;
  showOnlyMyAssignments: boolean;

  static fromJS(data: any): UserSettingsDto {
    const result = new UserSettingsDto();
    result.init(data);
    return result;
  }

  constructor(data?: IUserSettingsDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.active = data['active'];
      this.userSettingsId = data['userSettingsId'];
      this.userId = data['userId'];
      this.roleId = data['roleId'];
      this.currentSiteId = data['currentSiteId'];
      this.showOnlyMyAssignments = data['showOnlyMyAssignments'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['createdByName'] = this.createdByName;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;
    data['userSettingsId'] = this.userSettingsId;
    data['userId'] = this.userId;
    data['roleId'] = this.roleId;
    data['currentSiteId'] = this.currentSiteId;
    data['showOnlyMyAssignments'] = this.showOnlyMyAssignments;

    return data;
  }
}

// endregion

