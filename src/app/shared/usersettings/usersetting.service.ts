import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

import {UserSettingsDto} from './usersetting.model'
import {CommonService} from '../services/common.service';
import {environment} from '../../../environments/environment';

@Injectable()
export class UserSettingsService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService, private http: HttpClient, @Inject('ORIGIN_URL') originUrl: string) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/equipmentmaintenance/api/UserSettings/';
  }

  create(input: UserSettingsDto, successCallback: any, errorCallback: any): any {
    // this.commonService.httppost(input, successCallback, errorCallback, "UserSettings", "Save");
    const url = this.apiBaseUrl + 'InsertAsync';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  update(input: UserSettingsDto, successCallback: any, errorCallback: any): any {
    // this.commonService.httppost(input, successCallback, errorCallback, "UserSettings", "Save");
    const url = this.apiBaseUrl + 'UpdateAsync';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  delete(input: UserSettingsDto, successCallback: any, errorCallback: any): any {
    /*this.commonService.httppost(input, successCallback, errorCallback, "UserSettings", "Delete");
    let url = this.apiBaseUrl + "DeleteAsync";

    let params = new HttpParams().set('id', id);

    let options = {
     params: params
    };

    this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
*/
  }

  get(input: UserSettingsDto, successCallback: any, errorCallback: any): any {
    // this.commonService.httppost(input, successCallback, errorCallback, "UserSettings","GetById");
  }

  firstUpper(input: string) {
    return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1) : '';
  }
}

