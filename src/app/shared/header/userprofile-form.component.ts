import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input, OnChanges,
  Output, SimpleChanges
} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';
import {Router} from '@angular/router';

import {CommonService} from '../../shared/services/common.service';
import {User} from '../../shared/models/eqmSettings'
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';

import {UserProfileService} from './service/userProfile.service'
import {UserProfileDto} from './service/userProfile.model'
import {HeaderComponent} from './header.component'
import {environment} from '../../../environments/environment';

@Component({
  selector: 'kendo-userProfile-form',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './userprofile-form.component.html',
  styleUrls: ['./userprofile.component.css']
})

export class UserProfileFormComponent implements OnChanges {
  viewerContainerStyle = {
    position: 'relative',
    width: '100%',
    height: '800px',
    ['font-family']: 'ms sans serif'
  };

  user: User = new User();
  public profileFormActive = false;
  public mask = '0(000)000-0000';
  public base64ImgFile: string = 'assets/img/no_user.jpg';
  public azureStorageContainer: string;
  public loading = false;

  public editForm: FormGroup = new FormGroup({
    'receiveEmailNotifications': new FormControl(),
    'receiveSmsNotifications': new FormControl(),
    'email': new FormControl(),
    'mobilePhone': new FormControl(),
    'password': new FormControl(),
    'uploadUserImg': new FormControl(),
    'imageFileName': new FormControl(),
    'imageFile': new FormControl(),
    'o365pw': new FormControl(),
    'o365user': new FormControl(),
    'thumbnailFile': new FormControl()
  });

  @Input() public openProfileDialog = false;

  @Input()
  public set model(flag: any) {
    if (flag) {
      this.profileFormActive = true;
      this.init();
    }
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();

  @Input() userProfile: UserProfileDto = new UserProfileDto();

  constructor(private userProfileService: UserProfileService,
              private router: Router,
              public header: HeaderComponent,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private toasterService: ToasterHelperService) {
    this.azureStorageContainer = environment.azureStorageContainer;
    this.user = this.eqmSettingsService.getUser();
  }

  private loadAvatar() {
    if (this.userProfile && this.userProfile.userId) {
      this.base64ImgFile = this.commonService.getUserThumbnail(this.userProfile.userId.toString(), 125, 125);
    }
  }

  private loadData(): void {
    const user = new UserProfileDto();
    user.userId = this.user.userId;

    this.userProfileService.get(user, response => {
        this.userProfile = UserProfileDto.fromJS(response);

        this.userProfile.password = '';

        this.loadAvatar();

        this.editForm.reset(this.userProfile);
      },
      error => {
        this.toasterService.errorMessage(error);
      }
    );
  }

  init(): void {
    this.loadData();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.userProfile && changes.userProfile.currentValue) {
      this.loadAvatar();
    }
  }
  updateUrl() {
    console.log('No Image Found in Blob for this user.');
    this.base64ImgFile = 'assets/img/no_user.jpg';
  }

  public onSave(e): void {

    e.preventDefault();
    this.loading = true;

    const inputData: UserProfileDto = this.userProfile;

    //prepare user inputs to update
    inputData.receiveEmailNotifications = this.editForm.controls['receiveEmailNotifications'].value;
    inputData.receiveSmsNotifications = this.editForm.controls['receiveSmsNotifications'].value;
    inputData.mobilePhone = this.editForm.controls['mobilePhone'].value;
    inputData.email = this.editForm.controls['email'].value;
    inputData.imageFile = this.editForm.controls['imageFile'].value;
    inputData.imageFileName = this.editForm.controls['imageFileName'].value;
    inputData.o365user = this.editForm.controls['o365user'].value;
    inputData.o365pw = this.editForm.controls['o365pw'].value;
    inputData.password = this.editForm.controls['password'].value;
    inputData.thumbnailFile = this.editForm.controls['thumbnailFile'].value;
    //post objet to update profile
    this.userProfileService.update(inputData,
      response => {
        this.loading = false;

        //Update header
        this.header.loadProfilePic();

        this.toasterService.success('', 'User Profile Updated Successfully');
      },
      error => {
        this.loading = false;

        this.toasterService.errorMessage(error);
      }
    );
  }

  validateEmailFunction() {
    const email = this.editForm.controls['email'].value;
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const isProperEmail = re.test(String(email).toLowerCase());

    if (!isProperEmail) {
      this.toasterService.error('Validation failed', 'Please Enter Valid Email Address.');
    }

    return false;
  }

  public validatePassword() {
    const password = this.editForm.controls['password'].value;

    if (password == null || password === '') {
      return true;
    }

    if (password.length < 8 || password.length > 12) {
      this.editForm.controls['password'].updateValueAndValidity();
      this.toasterService.error('Validation failed', 'Passwrod must contain values In Between 8 to 12 Characters.');
      return false;
    }

    const hasUpper = /[A-Z]/.test(password);
    const hasLower = /[a-z]/.test(password);
    const hasNumber = /\d/.test(password);
    const hasSpecialChar = /(?=.*?[#?!@$%^&*-])/.test(password);

    if (!hasUpper) {
      this.toasterService.error('Validation failed', 'Passwrod must contain one upper case character.');
      return false;
    }

    if (!hasLower) {
      this.toasterService.error('Validation failed', 'Passwrod must contain one lower case character.');
      return false;
    }

    if (!hasNumber || !hasSpecialChar) {
      this.toasterService.error('Validation failed', 'Passwrod must contain one numeric character or special character.');
      return false;
    }

    this.editForm.controls['password'].updateValueAndValidity();
    return false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.profileFormActive = false;
    this.cancel.emit();
  }

  public onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        const filetype = file.name.split('.')[1];
        this.editForm.controls['imageFile'].setValue(reader.result.split(',')[1]);
        this.editForm.controls['imageFileName'].setValue(file.name);
        // this.readFiles(file);
        const imgFile = reader.result.split(',')[1];
        this.base64ImgFile = 'data:image/' + filetype + ';base64,' + imgFile;
      };
    }
  }


  /* readFile(file, reader, callback) {
    // Set a callback funtion to fire after the file is fully loaded
    reader.onload = () => {
      // callback with the results
      callback(reader.result);
    }

    // Read the file
    reader.readAsDataURL(file);
   }

   readFiles(files, index = 0) {
    let reader = new FileReader();
      this.readFile(files, reader, (result) => {
        var img = document.createElement("img");
        img.src = result;
        this.resize(img, 35, 35, (resized_jpeg, before, after) => {

        });
      });
   }


   resize(img, MAX_WIDTH: number, MAX_HEIGHT: number, callback) {
    // This will wait until the img is loaded before calling this function
    return img.onload = () => {
      console.log("img loaded");
      // Get the images current width and height
      var width = img.width;
      var height = img.height;

      // Set the WxH to fit the Max values (but maintain proportions)
      if (width > height) {
        if (width > MAX_WIDTH) {
          height *= MAX_WIDTH / width;
          width = MAX_WIDTH;
        }
      } else {
        if (height > MAX_HEIGHT) {
          width *= MAX_HEIGHT / height;
          height = MAX_HEIGHT;
        }
      }

      // create a canvas object
      var canvas = document.createElement("canvas");

      // Set the canvas to the new calculated dimensions
      canvas.width = width;
      canvas.height = height;
      var ctx = canvas.getContext("2d");

      ctx.drawImage(img, 0, 0, MAX_WIDTH, MAX_HEIGHT);

      // Get this encoded as a jpeg
      // IMPORTANT: 'jpeg' NOT 'jpg'
      var dataUrl = canvas.toDataURL('image/png');
      this.editForm.controls["thumbnailFile"].setValue(dataUrl.split(',')[1]);
      return dataUrl;
      //// callback with the results
      //callback(dataUrl, img.src.length, dataUrl.length);
    };
   }*/
}
