import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

// region IUserProfileDto

export interface IUserProfileDto {
  active: boolean;
  userId: number;
  firstName: string;
  lastName: string;
  displayName: string;
  userPin: number;
  email: string;
  password: string;
  userTypeId: number;
  siteId: number;
  mobilePhone: number;
  pager: number;
  o365user: string;
  o365pw: string;
  proxCardCode: string;
  supervisorUserId: number;
  departmentId: number;
  userRoleId: number;
  receiveEmailNotifications: boolean;
  receiveSmsNotifications: boolean;
  defaultUrl: string;
  unreadNotifications: number;
  supervisorName: string;
  departmentName: string;
  thumbnailFile: string;
}

export class UserProfileDto implements IUserProfileDto {
  active: boolean;
  userId: number;
  firstName: string;
  lastName: string;
  displayName: string;
  userPin: number;
  email: string;
  password: string;
  userTypeId: number;
  siteId: number;
  mobilePhone: number;
  pager: number;
  o365user: string;
  o365pw: string;
  proxCardCode: string;
  supervisorUserId: number;
  departmentId: number;
  userRoleId: number;
  receiveEmailNotifications: boolean;
  receiveSmsNotifications: boolean;
  defaultUrl: string;
  unreadNotifications: number;
  supervisorName: string;
  departmentName: string;
  imageFileName: string;
  imageFile: string;
  thumbnailFile: string;

  static fromJS(data: any): UserProfileDto {
    const result = new UserProfileDto();
    result.init(data);
    return result;
  }

  constructor(data?: IUserProfileDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {

      this.active = data['active'];
      this.userId = data['userId'];
      this.firstName = data['firstName'];
      this.lastName = data['lastName'];
      this.displayName = data['displayName'];
      this.userPin = data['userPIN'];
      this.email = data['email'];
      this.password = data['password'];
      this.userTypeId = data['userTypeId'];
      this.siteId = data['siteId'];
      this.mobilePhone = data['mobilePhone'];
      this.pager = data['pager'];
      this.o365user = data['o365user'];
      this.o365pw = data['o365pw'];
      this.proxCardCode = data['proxCardCode'];
      this.supervisorUserId = data['supervisorUserId'];
      this.departmentId = data['departmentId'];
      this.userRoleId = data['userRoleId'];
      this.receiveEmailNotifications = data['receiveEmailNotifications'];
      this.receiveSmsNotifications = data['receiveSMSNotifications'];
      this.defaultUrl = data['defaultURL'];
      this.unreadNotifications = data['unreadNotifications'];
      this.supervisorName = data['supervisorName'];
      this.departmentName = data['departmentName'];
      this.imageFileName = data['imageFileName'];
      this.imageFile = data['imageFile'];
      this.thumbnailFile = data['thumbnailFile'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['active'] = this.active;
    data['userId'] = this.userId;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['displayName'] = this.displayName;
    data['userPIN'] = this.userPin;
    data['email'] = this.email;
    data['password'] = this.password;
    data['userTypeId'] = this.userTypeId;
    data['siteId'] = this.siteId;
    data['mobilePhone'] = this.mobilePhone;
    data['pager'] = this.pager;
    data['o365user'] = this.o365user;
    data['o365pw'] = this.o365pw
    data['proxCardCode'] = this.proxCardCode;
    data['supervisorUserId'] = this.supervisorUserId;
    data['departmentId'] = this.departmentId;
    data['userRoleId'] = this.userRoleId;
    data['receiveEmailNotifications'] = this.receiveEmailNotifications;
    data['receiveSMSNotifications'] = this.receiveSmsNotifications;
    data['defaultURL'] = this.defaultUrl;
    data['unreadNotifications'] = this.unreadNotifications;
    data['supervisorName'] = this.supervisorName;
    data['departmentName'] = this.departmentName;
    data['imageFileName'] = this.imageFileName;
    data['imageFile'] = this.imageFile;
    data['thumbnailFile'] = this.thumbnailFile;
    return data;
  }
}

// endregion
