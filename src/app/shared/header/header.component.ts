import {
  AfterViewInit, ChangeDetectionStrategy,
  Component,
  EventEmitter,
  OnInit,
  Output
} from '@angular/core';
import {Router} from '@angular/router';
import 'rxjs/add/observable/fromEvent';

import {Globals} from '../globals/globals';
import {
  EqmSettings,
  User,
  UserRole
} from '../models/eqmSettings'
import {EqmSettingsService} from '../services/eqmsettings.service';
import {CommonService} from '../services/common.service';

import {UserProfileService} from './service/userProfile.service'
import {UserProfileDto} from './service/userProfile.model'
import {HubConnection} from '@aspnet/signalr-client';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-header',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  viewerContainerStyle = {
    position: 'relative',
    width: '1000px',
    height: '800px',
    ['font-family']: 'ms sans serif'
  };

  public logoImageUrl = 'assets/img/logo_transparent.png';
  public userImageUrl = 'assets/img/no_avatar.png';
  public iconImageUrl = 'assets/img/top-left-icon.png';
  public reportImageUrl = 'assets/img/chart-icon.png';
  public docImageUrl = 'assets/img/document-icons.png';
  public processImageUrl = 'assets/img/process-icon.png';
  public qualityImageUrl = 'assets/img/quality-icon.png';
  public euipImageUrl = 'assets/img/maintenance-icon.png';
  public projImageUrl = 'assets/img/document-icon.png';
  public adminImageUrl = 'assets/img/admin.png';

  public userName: string;
  public projectsUrl: string;

  toggeleModule = false;
  isEqmAccess = false;
  isSysadminAccess = false;
  isProjectAccess = false;
  isDocumentAccess = false;
  isToolingManagementAccess = false;

  public openProfileDialog: boolean;
  public openLaborDialog: boolean;
  public openChangePwdNextLoginDialog: boolean;
  public profileFlag: boolean;
  public strShortHeaderTitle: string;
  public strLongHeaderTitle: string;
  private hubConnection: HubConnection;

  @Output() modulesChange: EventEmitter<any> = new EventEmitter();

  eqmSettings: EqmSettings = new EqmSettings();
  user: User = new User();
  userRole: UserRole = new UserRole();
  userProfile: UserProfileDto = new UserProfileDto();
  public azureStorageContainer: string;
  laborHour: any = 0;

  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private globals: Globals,
              private router: Router,
              private eqmSettingsService: EqmSettingsService,
              private userProfileService: UserProfileService,
              private commonService: CommonService) {
    this.eqmSettings = this.eqmSettingsService.getEqmSettings();
    this.user = this.eqmSettingsService.getUser();
    this.userRole = this.eqmSettingsService.getUserRoles();
    this.azureStorageContainer = environment.azureStorageContainer;
  }

  ngOnInit() {

    this.loadProfilePic();
    const module = this.commonService.getCookies('module');
    if (module === 'eqm') {
      this.strShortHeaderTitle = 'EQM';
      this.strLongHeaderTitle = 'Equipment Maintenance';
    }
    else if (module === 'sysadmin') {
      this.strShortHeaderTitle = 'SYS';
      this.strLongHeaderTitle = 'System Administration';
    }
    else if (module === 'projects') {
      this.strShortHeaderTitle = 'PRJ';
      this.strLongHeaderTitle = 'Projects';
    }
    else if (module === 'documents') {
      this.strShortHeaderTitle = 'DOC';
      this.strLongHeaderTitle = 'Document Control';
    }
    else if (module === 'toolingmanagement') {
      this.strShortHeaderTitle = 'TLM';
      this.strLongHeaderTitle = 'Tool Management';
    }
    else if (module === 'sales') {
      this.strShortHeaderTitle = 'SLS';
      this.strLongHeaderTitle = 'Sales';
    }
    else {
      this.strShortHeaderTitle = 'EQM';
      this.strLongHeaderTitle = 'Equipment Maintenance';
    }

    this.userName = this.user.displayName;
    if (this.eqmSettings && this.userRole) {
      if (this.eqmSettingsService.getEqmSettings().laborHour != null) {
        this.laborHour = (this.eqmSettingsService.getEqmSettings().laborHour).toFixed(1);
      }
    }

    if (this.userRole) {
      this.signalRConnectionForLaborHours();
      this.isEqmAccess = this.userRole.canViewEquipmentMaintenance;
      this.isSysadminAccess = this.userRole.canViewSysAdmin;
      this.isProjectAccess = this.userRole.canViewProjects;
      this.isDocumentAccess = this.userRole.canViewDocumentManagement;
      this.isToolingManagementAccess = true;
    }
    if (this.eqmSettings.chgPWDNextLogin) {
      this.openChangePwdNextLoginDialog = true;
    }
  }

  loadLaborDialog() {
    this.openLaborDialog = true;
  }

  closeChangePwdNextLoginDialog() {
    this.openChangePwdNextLoginDialog = false;
  }

  signalRConnectionForLaborHours() {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started Labor hours!');
        console.log('User Id' + this.user.userId);
        this.hubConnection
          .invoke('Subscribe', 'General', 'LaborHours', 'UserId', this.user.userId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated',
      (groupName: string, operation: string, messageJson: string) => {
        const _responseText = messageJson;
        const result200: any = null;
        debugger;
        const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
        if (operation.toLowerCase() === 'update') {
          this.laborHour = (data.Hours).toFixed(1);
        }
      });
  }

  public loadProfilePic() {

    if (this.user && this.user.userId) {
      const user = new UserProfileDto();
      user.userId = this.user.userId;

      this.userProfileService.get(user, response => {
          this.userProfile = UserProfileDto.fromJS(response);

          if (this.userProfile !== undefined) {
            const newstr = this.commonService.getUserThumbnail(user.userId.toString(), 150, 150);
            this.userImageUrl = newstr;
          }
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  updateUrl(e) {
    console.log('No Image Found in Blob for this user.');
    this.userImageUrl = 'assets/img/no_avatar.png';
  }

  // sidebar-collapse
  public togglemenu() {
    if (this.globals.toogleMenu) {
      this.globals.toogleMenu = false;
    } else {
      this.globals.toogleMenu = true;
    }
  };

  // usermenu-toggle
  public toogleUserMenu() {
    this.globals.userProfileMenu = false;
    if (this.globals.userMenu) {
      this.globals.userMenu = false;
    } else {
      this.globals.userMenu = true;
    }
  }

  // userProfile menu
  public toggleUserProfileMenu() {
    this.globals.userMenu = false;
    if (this.globals.userProfileMenu) {
      this.globals.userProfileMenu = false;
    } else {
      this.globals.userProfileMenu = true;
    }
  }

  public logout(e) {
    e.preventDefault();
    this.commonService.deleteAllCookies();
    this.router.navigate(['/auth/login']);
  }

  openProfile(data) {
    this.openProfileDialog = true;
    this.profileFlag = true;
  }

  selectMenu(module) {
    if (module === 'sysadmin') {
      this.strShortHeaderTitle = 'SYS';
      this.strLongHeaderTitle = 'System Administration';
    }
    else if (module === 'documents') {
      this.strShortHeaderTitle = 'DOC';
      this.strLongHeaderTitle = 'Document Control';
    }
    else if (module === 'projects') {
      this.strShortHeaderTitle = 'PRJ';
      this.strLongHeaderTitle = 'Projects';
    }
    else if (module === 'toolingmanagement') {
      this.strShortHeaderTitle = 'TLM';
      this.strLongHeaderTitle = 'Tool Management';
    }
    else if (module === 'sales') {
      this.strShortHeaderTitle = 'SLS';
      this.strLongHeaderTitle = 'Sales';
    }
    else {
      this.strShortHeaderTitle = 'EQM';
      this.strLongHeaderTitle = 'Equipment Maintenance';
    }
    this.commonService.setCookies('module', module);
    this.modulesChange.emit(module);
  }

  public closeProfileViewer(data) {
    this.openProfileDialog = false;
    this.profileFlag = false;
  }

  public closeLaborReporting() {
    this.openLaborDialog = false;
  }
}
