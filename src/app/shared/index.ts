import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {ButtonsModule} from '@progress/kendo-angular-buttons';
import {DialogModule} from '@progress/kendo-angular-dialog';
import {GridModule} from '@progress/kendo-angular-grid';
import {DateInputsModule} from '@progress/kendo-angular-dateinputs';
import {DropDownsModule} from '@progress/kendo-angular-dropdowns';
import {InputsModule} from '@progress/kendo-angular-inputs';
import {PopupModule} from '@progress/kendo-angular-popup';

import {Globals} from './globals/globals';

import {LaborFormComponent} from './labor/labor-form.component';
import {ChangePasswordNextLoginComponent} from './chg.pwd.next.login/chg-pwd-next-login.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {NavmenuComponent} from './navmenu/navmenu.component';
import {UserProfileFormComponent} from './header/userprofile-form.component'
import {MessagingFormComponent} from './messaging/messaging-form.component'
import {ChatMessageComponent} from './messaging/chatmessage/chatmessage.component'
import {NewChannelFormComponent} from './messaging/newchannel/newchannel.component'
import {DirectmessageFormComponent} from './messaging/directmessage/directmessage.component'
import {MessagePanelComponent} from './messaging/messagepanel/messagepanel.component'
import {ShowHidePasswordModule} from 'ngx-show-hide-password';
import {CustomTextEditorComponent} from './customtexteditor/customtexteditor.component';
import {FileUploadComponent} from './fileUpload/fileUpload.component'
import {DemoTinymceComponent} from './customtexteditor/customtexteditorTiny.component';

import {AuthGuard} from './guards/auth.guard'

import {APIHelper} from './services/apiHelper';
import {ToasterHelperService} from './services/toasterHelper.service';

import {CommonService} from './services/common.service';
import {SignalRService} from './services/signalR.service';
import {AuthenticationService} from './services/auth.service'
import {EqmSettingsService} from './services/eqmsettings.service'
import {UserSettingsService} from './usersettings/usersetting.service'
import {PermissionService} from './services/permission.service';
import {UserProfileService} from './header/service/userProfile.service'
import {MessageService} from './messaging/service/messaging.service'
import {MessageContextService} from './messaging/service/messagecontext.service'
import {MessageContextFollowerService} from './messaging/service/messagecontextfollower.service'


import {CKEditorModule} from 'ng2-ckeditor';
import {MentionModule} from 'angular-mentions/mention';
import {EditorModule} from '@tinymce/tinymce-angular';
import {LaborService} from './labor/service/labor.service';
import {LaborInputComponent} from './labor/labor-form.component.inputting';
import {ChangePwdNextLoginService} from './chg.pwd.next.login/service/chg-pwd.service';
import {PopupAnchorDirective} from './popupAnchor/popup.anchor-target.directive';
// import {IDto} from './models/IDto';
// import {BaseDto} from './models/BaseDto';

// import { QuillEditorModule } from 'ngx-quill-editor';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonsModule,
    GridModule,
    DialogModule,
    DateInputsModule,
    DropDownsModule,
    CommonModule,
    InputsModule,
    //QuillEditorModule,
    CKEditorModule,
    MentionModule,
    EditorModule,
    ShowHidePasswordModule.forRoot(),
    PopupModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    NavmenuComponent,
    UserProfileFormComponent,
    MessagingFormComponent,
    RouterModule,
    FileUploadComponent,
    CustomTextEditorComponent,
    ChatMessageComponent,
    CKEditorModule,
    MessagePanelComponent,
    DemoTinymceComponent,
    LaborFormComponent,
    LaborInputComponent,
    ChangePasswordNextLoginComponent
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    NavmenuComponent,
    MessagingFormComponent,
    ChatMessageComponent,
    NewChannelFormComponent,
    DirectmessageFormComponent,
    MessagePanelComponent,
    UserProfileFormComponent,
    FileUploadComponent,
    CustomTextEditorComponent,
    DemoTinymceComponent,
    LaborFormComponent,
    LaborInputComponent,
    ChangePasswordNextLoginComponent,
    PopupAnchorDirective

  ],
  providers: [
    Globals,
    ToasterHelperService,
    CommonService,
    SignalRService,
    APIHelper,
    AuthenticationService,
    AuthGuard,
    EqmSettingsService,
    UserSettingsService,
    PermissionService,
    UserProfileService,
    MessageService,
    MessageContextService,
    MessageContextFollowerService,
    LaborService,
    ChangePwdNextLoginService
  ]

})
export class SharedModule {
}
