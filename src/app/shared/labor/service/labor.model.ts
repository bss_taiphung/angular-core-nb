﻿import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

// region ILaborDto

export interface ILaborDto {
  laborPartId: number
  partId: number
  processId ?: number
  station: string
  detail: string
  requisition: string
  hours: number
  confirmed: boolean
  workDate: any
  modifiedDateTime: any
  modifiedBy: any
  createdBy: any
  createdDateTime: any
  active: boolean
}

export class LaborDto implements ILaborDto {
  laborPartId: number
  partId: number
  processId ?: number
  station: string
  detail: string
  requisition: string
  hours: number
  confirmed: boolean
  workDate: any
  modifiedDateTime: any
  modifiedBy: any
  createdBy: any
  createdDateTime: any
  active: boolean
  toolId: number;
  laborType: any;

  static fromJS(data: any): LaborDto {
    const result = new LaborDto();
    result.init(data);
    return result;
  }

  constructor(data?: ILaborDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.laborPartId = data['laborPartId'];
      this.partId = data['partId'];
      this.processId = data['processId'];
      this.station = data['station'];
      this.createdDateTime = data['createdDateTime'];
      this.detail = data['detail'];
      this.requisition = data['requisition'];
      this.hours = data['hours'];
      this.confirmed = data['confirmed'];
      this.workDate = data['workDate'];
      this.createdBy = data['createdBy'];
      this.createdDateTime = data['createdDateTime']
      this.modifiedBy = data['modifiedBy'];
      this.modifiedDateTime = data['modifiedDateTime']
      this.active = data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['laborPartId'] = this.laborPartId
    data['partId'] = this.partId;
    data['processId'] = this.processId;
    data['station'] = this.station;
    data['createdDateTime'] = this.createdDateTime;
    data['detail'] = this.detail;
    data['requisition'] = this.requisition;
    data['hours'] = this.hours;
    data['confirmed'] = this.confirmed;
    data['workDate'] = this.workDate;
    data['createdBy'] = this.createdBy;
    data['createdDateTime'] = this.createdDateTime
    data['modifiedBy'] = this.modifiedBy;
    data['modifiedDateTime'] = this.modifiedDateTime
    data['active'] = this.active;

    return data;
  }
}

// endregion

// region ICreatePartDto

export interface ICreatePartDto {
  active: boolean;
  isStockItem: boolean;
}

export class CreatePartDto implements ICreatePartDto {
  partId = 0;
  partName: string;
  partQuantity: number;
  partReOrderLevel: number;
  active = true;
  isStockItem: boolean;

  static fromJS(data: any): LaborDto {
    const result = new LaborDto();
    result.init(data);
    return result;
  }

  constructor(data?: ILaborDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.partId = 0;
      this.partName = data['partName'];
      this.partQuantity = data['partQuantity'];
      this.partReOrderLevel = data['partReOrderLevel'];
      this.active = (data['active'] == null) ? false : data['active'];
      this.isStockItem = data['isStockItem'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['partId'] = 0;
    data['partName'] = this.partName;
    data['partQuantity'] = this.partQuantity;
    data['partReOrderLevel'] = this.partReOrderLevel;
    data['active'] = (this.active == null) ? false : this.active;
    data['isStockItem'] = this.isStockItem;
    return data;
  }
}

// endregion

// region IPagedResultDtoOfPartDto

export interface IPagedResultDtoOfPartDto {
  results: LaborDto[];
  count: number;
}

export class PagedResultDtoOfPartDto implements IPagedResultDtoOfPartDto {
  results: LaborDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfPartDto {
    const result = new PagedResultDtoOfPartDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfPartDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(LaborDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion

// region IResultDtoOfPartDto

export interface IResultDtoOfPartDto {
  results: LaborDto[];
  count: number;
}

export class ResultDtoOfPartDto implements IResultDtoOfPartDto {
  results: LaborDto[];
  count: number;

  static fromJS(data: any): ResultDtoOfPartDto {
    const result = new ResultDtoOfPartDto();
    result.init(data);
    return result;
  }

  constructor(data?: IResultDtoOfPartDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {

    if (data && data != null) {
      if (data && data.constructor === Array) {
        this.results = [];
        for (const item of data) {
          this.results.push(LaborDto.fromJS(item));
        }
      }

    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data = [];
      for (const item of this.results) {
        data.push(item.toJSON());
      }
    }

    return data;
  }
}

// endregion
