import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {
  HttpClient,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';

import {
  LaborDto,
  PagedResultDtoOfPartDto,
  ResultDtoOfPartDto
} from './labor.model';
import {CommonService} from '../../../shared/services/common.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class LaborService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService, private http: HttpClient) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/general/api/Labor/';
  }

  create(input: LaborDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'InsertLaborTypeAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  update(input: LaborDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'UpdateAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  delete(data: any): any {
    const url = this.apiBaseUrl + 'DeleteAsync';
    return this.http.post(url, data)
  }

  getProcessData(departmentId: any): any {
    const url = environment.bllApiBaseAddress + '/ProductionSupport/api/Process/getProcessData';
    const params = new HttpParams()
      .set('departmentId', departmentId)

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };
    return this.http.get(url, options);
  }

  linkEquipments(input: any, successCallback: any, errorCallback: any): any {
    const url = environment.bllApiBaseAddress + '/equipmentmaintenance/api/EquipmentParts/' + 'InsertAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  getAll(take: number, skip: number, sort: any, isactive: boolean, searchtext: string): Observable<PagedResultDtoOfPartDto> {

    const url = this.apiBaseUrl + 'GetLabor';


    const params = new HttpParams()
      .set('userId', '7')
    // .set('$workdate', null)
    // .set('$laborPartRecords', null)
    // .set('laborToolRecords',null)


    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: {'userId': '7'}
    };

    return this.http.request<Response>('post', url, options).flatMap((response) => {
      const result200 = PagedResultDtoOfPartDto.fromJS(response);
      return Observable.of(result200);
    });

  }

  getLabor(reqData: any): any {
    const url = this.apiBaseUrl + 'GetLabor';
    return this.http.post(url, reqData)
  }

  getAllWtihFilter(take: number, skip: number, sort: any, filter: string): Observable<PagedResultDtoOfPartDto> {
    const url = this.apiBaseUrl + 'GetPagedQuery';

    let orderBy = '';
    if (sort != undefined && sort.length > 0) {
      for (const item of sort) {
        orderBy = this.commonService.firstUpper(item.field) + ' ' + (item.dir == undefined ? 'asc' : item.dir);
      }
    } else {
      orderBy = 'PartId desc';
    }

    const params = new HttpParams()
      .set('$top', take.toString())
      .set('$skip', skip.toString())
      .set('$count', 'true')
      .set('$orderby', orderBy.toString())
      .set('$filter', filter.toString());

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      const result200 = PagedResultDtoOfPartDto.fromJS(response);
      return Observable.of(result200);
    });
  }

  getAllByEquipmentId(equipmentId: number): Observable<ResultDtoOfPartDto> {
    const url = environment.bllApiBaseAddress + '/equipmentmaintenance/api/EquipmentParts/GetAllPartsListByEquipmentId';
    const params = new HttpParams()
      .set('EquipmentId', equipmentId.toString());

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };
    return this.http.request<Response>('get', url, options).flatMap((response) => {
      const result200 = ResultDtoOfPartDto.fromJS(response);
      return Observable.of(result200);
    });
  }
}
