import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {HubConnection} from '@aspnet/signalr-client';
import {GridDataResult, PageChangeEvent} from '@progress/kendo-angular-grid';
import {SortDescriptor, State} from '@progress/kendo-data-query';

import {LaborService} from './service/labor.service'
import {LaborDto} from './service/labor.model';
import {ToasterHelperService} from '../services/toasterHelper.service';
import {CommonService} from '../services/common.service';
import {EqmSettingsService} from '../services/eqmsettings.service';
import {UserSettingsService} from '../../eqmaintenance/eqmusersettings/service/eqmusersettings.service';
import {EquipmentService} from '../../eqmaintenance/equipment/service/equipment.service';
import {EqmSettings, User, UserRole} from '../models/eqmSettings';
import {SelectListDto} from '../models/selectListDto';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'kendo-grid-labor-form',
  styleUrls: ['./css/labor.component.css'],
  templateUrl: './html/labor-form.component.html'
})
export class LaborFormComponent implements OnInit {
  @Input() public openLaborDialog = false;
  eqmSettings: EqmSettings = new EqmSettings();
  user: User = new User();
  userRole: UserRole = new UserRole();
  public siteList: SelectListDto[];
  public mss: any;
  private sort: SortDescriptor[] = [];
  public isactive = true;
  public gridView: GridDataResult;
  public pageSize = 10;
  public skip = 0;
  private hubConnectionPart: HubConnection;
  private hubConnectionTool: HubConnection;
  public searchtext = '';
  loading = true;
  private laborModelList: LaborDto[] = [];
  private gridLoading = true;
  public openInputDialog = false;
  public openDeleteDialog = false;
  public selectedWorkDate: Date;
  public deleteDataItem: any;
  public editDataItem: any;
  public state: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };
  public laborTypeList: any = [
    {'text': 'Part', value: 1},
    {'text': 'Tool', value: 2}
  ];
  public showLoadingSpinner = false;
  public savingData = false;
  public showPartDropdown: boolean;
  public showToolDropdown: boolean;
  public deptList: any;
  public partList: any;
  public toolList: any;
  public processList: any;
  private editedRowIndex: number;
  public isNew = false;
  public selectedProcess: any = {'laborDetail': null, 'laborStation': null, 'laborRequisition': null};
  public formGroup: FormGroup = new FormGroup({
    'laborType': new FormControl('', Validators.required),
    'departmentId': new FormControl('', Validators.required),
    'itemId': new FormControl(''),
    'processId': new FormControl(''),
    'workDate': new FormControl('', Validators.required),
    'hours': new FormControl('', Validators.required),
    'confirmed': new FormControl(false),
    'station': new FormControl({value: '', disabled: !this.selectedProcess.laborStation}),
    'detail': new FormControl({value: '', disabled: !this.selectedProcess.laborDetail}),
    'requisition': new FormControl({value: '', disabled: !this.selectedProcess.laborRequisition}),
  });

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private laborService: LaborService,
              private userSettingsService: UserSettingsService,
              private equipmentService: EquipmentService) {
    this.mss = this.commonService.getCookies('Mss');
    this.eqmSettings = this.eqmSettingsService.getEqmSettings();
    this.user = this.eqmSettingsService.getUser();
    this.userRole = this.eqmSettingsService.getUserRoles();
    this.commonService.validateLogin();
    const today = new Date();
    this.selectedWorkDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());

  }


  @Input()
  public set model(part: LaborDto) {
    this.openLaborDialog = part !== undefined;
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.loadGridData();
    this.laborPartSignalRConnection();
    this.laborToolSignalRConnection();
    this.getDeptList();
    this.getProcessList();
    this.getPartList();
    this.getToolList();
  }

  getDeptList() {
    this.commonService.getAllWithId(this.eqmSettings.currentSiteId, 'General', 'Department')
      .subscribe(eventResult => {
        this.deptList = eventResult.results;
      });
  }

  loadDataOnDeptChange(value: any) {
    this.getProcessList();
  }

  getPartList() {
    this.commonService.getAllWithId(this.eqmSettings.currentSiteId, 'General', 'Part')
      .subscribe(eventResult => {
        this.partList = eventResult.results;
      });
  }

  getToolList() {
    this.commonService.getAllWithId(this.eqmSettings.currentSiteId, 'General', 'Tool')
      .subscribe(eventResult => {
        this.toolList = eventResult.results;
      });
  }

  getProcessList() {
    if (this.formGroup.value.departmentId !== '' && this.formGroup.value.departmentId !== undefined) {
      this.laborService.getProcessData(this.formGroup.value.departmentId).subscribe(result => {
        this.processList = result;
        for (let i = 0; i < result.length; i++) {
          if (this.processList[i].processId === this.formGroup.value.processId) {
            this.selectedProcess.laborDetail = this.processList[i].laborDetail;
            this.selectedProcess.laborStation = this.processList[i].laborStation;
            this.selectedProcess.laborRequisition = this.processList[i].laborRequisition;
          }
        }
      });
    }
  }

  public showLaborTypeDropdown(value: any) {
    if (this.formGroup.value.laborType === 1) {
      this.showPartDropdown = true;
      this.showToolDropdown = false;
    }
    else if (this.formGroup.value.laborType === 2) {
      this.showPartDropdown = false;
      this.showToolDropdown = true;
    }
  }

  public editFormGroup(dataItem: any) {
    this.formGroup = new FormGroup({
      'departmentId': new FormControl(dataItem.departmentId, Validators.required),
      'processId': new FormControl(dataItem.processId),
      'itemId': new FormControl(dataItem.itemId),
      'hours': new FormControl(dataItem.hours, Validators.required),
      'confirmed': new FormControl(dataItem.confirmed),
      'station': new FormControl({value: dataItem.station, disabled: !this.selectedProcess.laborStation}),
      'detail': new FormControl({value: dataItem.detail, disabled: !this.selectedProcess.laborDetail}),
      'requisition': new FormControl({value: dataItem.requisition, disabled: !this.selectedProcess.laborRequisition}),
    });
    if (dataItem.laborType === 'Part') {
      this.showPartDropdown = true;
      this.showToolDropdown = false;
      this.formGroup.addControl('partId', new FormControl(dataItem.partId, Validators.required));
      this.formGroup.removeControl('toolId');
    }
    else if (dataItem.laborType === 'Tool') {
      this.formGroup.addControl('toolId', new FormControl(dataItem.toolId, Validators.required));
      this.formGroup.removeControl('partId');
      this.showPartDropdown = false;
      this.showToolDropdown = true;
    }
  }

  public addFormGroup() {
    this.formGroup = new FormGroup({
      'laborType': new FormControl('', Validators.required),
      'itemId': new FormControl('', Validators.required),
      'departmentId': new FormControl('', Validators.required),
      'processId': new FormControl(''),
      'hours': new FormControl('', Validators.required),
      'confirmed': new FormControl(''),
      'station': new FormControl({value: '', disabled: !this.selectedProcess.laborStation}),
      'detail': new FormControl({value: '', disabled: !this.selectedProcess.laborDetail}),
      'requisition': new FormControl({value: '', disabled: !this.selectedProcess.laborRequisition}),
    });

  }

  public editHandler({sender, rowIndex, dataItem}) {
    this.isNew = false;
    this.closeEditor(sender);
    this.editFormGroup(dataItem);
    this.editDataItem = dataItem;
    this.getProcessList();
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formGroup);
  }

  public cancelHandler({sender, rowIndex}) {
    this.closeEditor(sender, rowIndex);
  }

  public addHandler({sender}) {
    this.closeEditor(sender);
    this.addFormGroup();
    sender.addRow(this.formGroup);
    this.isNew = true;
  }

  public saveHandler({sender, rowIndex, formGroup, isNew}): void {

    this.savingData = true;
    const data = formGroup.value;
    data.createdBy = this.user.userId;
    const workDate: Date = new Date();
    workDate.setDate(this.selectedWorkDate.getDate())
    data.workDate = workDate;
    if (!isNew) {
      if (this.editDataItem.laborType === 'Part') {
        data.laborType = 1;
        data.partId = this.formGroup.value.itemId;
        data.laborPartId = this.editDataItem.laborPartId;
      }
      else if (this.editDataItem.laborType === 'Tool') {
        data.laborType = 2;
        data.toolId = this.formGroup.value.itemId;
        data.laborToolId = this.editDataItem.laborToolId;
      }
    }
    else {
      data.laborType = this.formGroup.value.laborType;
      if (data.laborType === 1) {
        data.partId = this.formGroup.value.itemId;
      }
      else if (data.laborType === 2) {
        data.toolId = this.formGroup.value.itemId;
      }
    }
    this.laborService.create(data,
      response => {
        this.toasterService.success('', 'Labor Data Saved Successfully');
        this.loadGridData();
        sender.closeRow(rowIndex);
        this.savingData = false;
      },
      error => {
        this.toasterService.errorMessage(error);
        this.savingData = false;
      }
    );
    this.isNew = false;

  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public close(status: boolean) {
    if (status) {
      this.showLoadingSpinner = true;
      this.laborService.delete(this.deleteDataItem).subscribe(
        (result) => {
          this.toasterService.success('', 'Data removed successfully');
          this.openDeleteDialog = false;
          this.showLoadingSpinner = false;
          this.loadGridData();
        },
        (error) => this.toasterService.errorMessage(error))
    }
    else {
      this.openDeleteDialog = false;
    }
  }

  public removeHandler({sender, rowIndex, dataItem}) {
    if (dataItem.laborType === 'Part') {
      this.deleteDataItem = {'laborType': dataItem.laborType, 'id': dataItem.laborPartId}
    }
    else if (dataItem.laborType === 'Tool') {
      this.deleteDataItem = {'laborType': dataItem.laborType, 'id': dataItem.laborToolId}
    }
    this.openDeleteDialog = true;
  }

  laborToolSignalRConnection() {
    this.hubConnectionTool =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnectionTool
      .start()
      .then(() => {
        console.log('Connection started labor Tool!');

        this.hubConnectionTool
          .invoke('Subscribe',
            'General',
            'LaborTool',
            'CreatedBy',
            this.user.userId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnectionTool.on('clientOnEntityUpdated',
      (groupName: string, operation: string, messageJson: string) => {
        debugger;
        const _responseText = messageJson;
        const result200: any = null;
        const data = JSON.parse(messageJson);
        const resultData200 = this.commonService.toCamel(data);
        const dataExist = this.containsToolObject(resultData200, this.gridView.data);
        if (operation.toLowerCase() === 'insert' && !dataExist) {
          this.gridView.data.unshift(resultData200);
        }
        if (operation.toLowerCase() === 'update') {
          this.gridView.data.forEach((element, index) => {
            if (element.laborToolId === resultData200.laborToolId) {
              if (this.isactive) {
                if (resultData200.active === this.isactive) {
                  this.gridView.data[index] = resultData200;
                } else {
                  operation = 'delete';
                }
              } else {
                this.gridView.data[index] = resultData200;
              }
            }
          });
        }
        if (operation.toLowerCase() === 'delete' && dataExist) {
          let index = null;
          this.gridView.data.forEach((element, i) => {
            if (element.laborToolId === resultData200.laborToolId) {
              index = i;
            }
          });
          if (index !== null) {
            this.gridView.data.splice(index, 1);
          }
        }

      });
  }

  public ActionData: Array<any> = [{
    text: 'Delete',
    icon: 'trash'
  }];

  containsPartObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].laborPartId === obj.laborPartId) {
        return true;
      }
    }

    return false;
  }

  containsToolObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].laborToolId === obj.laborToolId) {
        return true;
      }
    }

    return false;
  }

  laborPartSignalRConnection() {
    this.hubConnectionPart =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnectionPart
      .start()
      .then(() => {
        console.log('Connection started labor Part!');

        this.hubConnectionPart
          .invoke('Subscribe',
            'General',
            'LaborPart',
            'CreatedBy',
            this.user.userId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnectionPart.on('clientOnEntityUpdated',
      (groupName: string, operation: string, messageJson: string) => {
        const _responseText = messageJson;
        const result200: any = null;
        const data = JSON.parse(messageJson);
        const resultData200 = this.commonService.toCamel(data);
        const dataExist = this.containsPartObject(resultData200, this.gridView.data);
        if (operation.toLowerCase() === 'insert' && !dataExist) {
          this.gridView.data.unshift(resultData200);
        }
        if (operation.toLowerCase() === 'update') {
          this.gridView.data.forEach((element, index) => {
            if (element.laborPartId === resultData200.laborPartId) {
              if (this.isactive) {
                if (resultData200.active === this.isactive) {
                  this.gridView.data[index] = resultData200;
                } else {
                  operation = 'delete';
                }
              } else {
                this.gridView.data[index] = resultData200;
              }
            }
          });
        }
        if (operation.toLowerCase() === 'delete' && dataExist) {
          let index = null;
          this.gridView.data.forEach((element, i) => {
            if (element.laborPartId === resultData200.laborPartId) {
              index = i;
            }
          });
          if (index !== null) {
            this.gridView.data.splice(index, 1);
          }
        }

      });
  }

  openLaborInputDialog() {
    this.openInputDialog = true;
  }

  closeLaborInputDialog() {
    this.openInputDialog = false;
  }

  onWorkDateChange(value: any) {
    this.selectedWorkDate = value;
    this.loadGridData();
  }

  private closeForm(): void {
    this.openLaborDialog = false;
    this.cancel.emit();
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.loadGridData();
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
    this.loadGridData();
  }

  private loadGridData(): void {
    this.laborModelList = [];
    this.gridLoading = true;
    this.loading = true;
    const workDate: Date = new Date();
    workDate.setDate(this.selectedWorkDate.getDate())
    const reqData = {'userId': this.user.userId, 'workDate': workDate}
    this.laborService.getLabor(reqData).subscribe(eventResult => {
      for (let i = 0; i < eventResult['laborPartRecords'].length; i++) {
        this.laborModelList.push(eventResult['laborPartRecords'][i])

      }
      for (let i = 0; i < eventResult['laborToolRecords'].length; i++) {
        this.laborModelList.push(eventResult['laborToolRecords'][i])
      }
      this.gridView = {
        data: this.laborModelList.slice(this.skip, this.skip + this.pageSize),
        total: this.laborModelList.length
      };
      this.loading = false;
      this.gridLoading = false;
    });
  }
}
