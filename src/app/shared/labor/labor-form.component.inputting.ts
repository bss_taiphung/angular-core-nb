import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';

import {LaborService} from './service/labor.service'
import {LaborDto} from './service/labor.model';
import {ToasterHelperService} from '../services/toasterHelper.service';
import {CommonService} from '../services/common.service';
import {EqmSettingsService} from '../services/eqmsettings.service';
import {UserSettingsService} from '../../eqmaintenance/eqmusersettings/service/eqmusersettings.service';
import {EquipmentService} from '../../eqmaintenance/equipment/service/equipment.service';
import {
  EqmSettings,
  User,
  UserRole
} from '../models/eqmSettings';
import {SelectListDto} from '../models/selectListDto';


@Component({
  selector: 'kendo-input-labor-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./css/labor.component.css'],
  templateUrl: './html/labor.component.html'
})
export class LaborInputComponent implements OnInit {
  @Input() public openInputDialog = false;
  eqmSettings: EqmSettings = new EqmSettings();
  user: User = new User();
  userRole: UserRole = new UserRole();
  public siteList: SelectListDto[];
  loading: boolean = true;
  private laborModelList: LaborDto[] = []
  private gridLoading: boolean = true;
  public deptList: any;
  public partList: any;
  public toolList: any;
  public processList: any;
  public today: any = new Date();
  public currentDate: any;
  public selectedProcess: any = {'laborDetail': null, 'laborStation': null, 'laborRequisition': null};
  public selectedDept: any = {'text': 'Not Specified', value: 1};
  public selectedLaborType: any;
  public showPartDropdown: boolean;
  public exclusionLoading: boolean = false;
  public showToolDropdown: boolean;
  public laborTypeList: any = [
    {'text': 'Part Labor', value: 1},
    {'text': 'Tool Labor', value: 2}
  ];
  public editForm: FormGroup = new FormGroup({
    'laborType': new FormControl('', Validators.required),
    'department': new FormControl('', Validators.required),
    'processId': new FormControl(''),
    'workDate': new FormControl('', Validators.required),
    'hours': new FormControl('', Validators.required),
    'confirmed': new FormControl(false),
    'station': new FormControl({value: '', disabled: !this.selectedProcess.laborStation}),
    'detail': new FormControl({value: '', disabled: !this.selectedProcess.laborDetail}),
    'requisition': new FormControl({value: '', disabled: !this.selectedProcess.laborRequisition}),
  });

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private laborService: LaborService,
              private userSettingsService: UserSettingsService,
              private equipmentService: EquipmentService) {
    this.eqmSettings = this.eqmSettingsService.getEqmSettings();
    this.user = this.eqmSettingsService.getUser();
    this.userRole = this.eqmSettingsService.getUserRoles();
    this.commonService.validateLogin();
    this.currentDate = new Date();
    this.selectedLaborType = this.editForm.value.laborType;
  }


  @Input()
  public set model(part: LaborDto) {
    this.openInputDialog = part !== undefined;
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.getDeptList();
    this.getProcessList();
    this.getPartList();
    this.getToolList();
  }

  public showLaborTypeDropdown(value: any) {
    if (this.editForm.value.laborType == 1) {
      this.showPartDropdown = true;
      this.showToolDropdown = false;
      this.editForm.addControl('partId', new FormControl('', Validators.required));
      this.editForm.removeControl('toolId');
    }
    else if (this.editForm.value.laborType == 2) {
      this.editForm.addControl('toolId', new FormControl('', Validators.required));
      this.editForm.removeControl('partId');
      this.showPartDropdown = false;
      this.showToolDropdown = true;
    }
  }

  public saveHandler() {
    this.exclusionLoading = true;
    const data: LaborDto = this.editForm.value;
    let workDate: Date = new Date();
    workDate.setDate(this.editForm.value.workDate.getDate())
    data.active = true;

    data.workDate = workDate;
    data.createdBy = this.user.userId;
    this.laborService.create(data,
      response => {
        this.toasterService.success('', 'Labor Data Saved Successfully');
        this.editForm.reset();
        this.cancel.emit();
        this.exclusionLoading = false;
      },
      error => {
        this.toasterService.errorMessage(error);
        this.exclusionLoading = false;
        //  this.cancel.emit();
      }
    );
  }

  loadDataOnDeptChange(value: any) {
    this.getProcessList();
  }

  getDeptList() {
    this.commonService.getAllWithId(this.eqmSettings.currentSiteId, 'General', 'Department').subscribe(eventResult => {
      this.deptList = eventResult.results;
    });
  }

  getPartList() {
    this.commonService.getAllWithId(this.eqmSettings.currentSiteId, 'General', 'Part').subscribe(eventResult => {
      this.partList = eventResult.results;
    });
  }

  getToolList() {
    this.commonService.getAllWithId(this.eqmSettings.currentSiteId, 'General', 'Tool').subscribe(eventResult => {
      this.toolList = eventResult.results;
    });
  }

  getProcessList() {
    let dept = this.editForm.value.department;
    if (!dept) dept = -1;
    if (this.editForm.value.department !== '' && this.editForm.value.department != undefined) {
      this.laborService.getProcessData(this.editForm.value.department).subscribe(result => {
        this.processList = result;
        for (var i = 0; i < result.length; i++) {
          if (this.processList[i].processId == this.editForm.value.processId) {
            this.selectedProcess.laborDetail = this.processList[i].laborDetail;
            this.selectedProcess.laborStation = this.processList[i].laborStation;
            this.selectedProcess.laborRequisition = this.processList[i].laborRequisition;
          }
        }
        console.log(this.processList)
      });
    }
  }

  private closeForm(): void {
    this.openInputDialog = false;
    this.cancel.emit();
  }

  public onCancel(value: any) {
    this.openInputDialog = false;
    this.cancel.emit();
  }
}

