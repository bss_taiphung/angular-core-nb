﻿import {Injectable} from '@angular/core';

@Injectable()
export class Globals {
  toogleMenu: boolean = true;
  userMenu: boolean = false;
  isUserMenuHidden: boolean = false;
  userProfileMenu: boolean = false;
}
