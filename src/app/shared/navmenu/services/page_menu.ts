export const menuForEqm = [
  {
    path: 'general/dashboard',
    data: {
      menu: {
        title: 'Dashboard',
        icon: 'fa fa-dashboard',
      }
    }
  },
  {
    path: 'general/notifications',
    data: {
      menu: {
        title: 'Notification',
        icon: 'fa fa-bell',
        templateText: '0'
      }
    }
  },
  {
    path: 'general/messages',
    data: {
      menu: {
        title: 'Messaging',
        icon: 'fa fa-comments',
        templateText: '0',
        click: 'true'
      }
    }
  },
  {
    path: 'eqmaintenance',
    data: {
      menu: {
        title: 'Master Data',
        icon: 'fa fa-table'
      }
    },
    children: [
      {
        path: 'manufacturer',
        data: {
          menu: {
            title: 'Manufacturer',
            icon: 'fa fa-circle-o'
          }
        }
      },
      {
        path: 'part',
        data: {
          menu: {
            title: 'Part',
            icon: 'fa fa-circle-o',
          }
        }
      },
      {
        path: 'scheduletype',
        data: {
          menu: {
            title: 'Schedule Type',
            icon: 'fa fa-circle-o',
          }
        }
      },
      {
        path: 'ordertype',
        data: {
          menu: {
            title: 'Order Type',
            icon: 'fa fa-circle-o',
          }
        }
      },
      {
        path: 'failure-type',
        data: {
          menu: {
            title: 'Failure Type',
            icon: 'fa fa-circle-o',
          }
        }
      },
      {
        path: 'equipment-type',
        data: {
          menu: {
            title: 'Equipment Type',
            icon: 'fa fa-circle-o',
          }
        }
      },
      {
        path: 'priority',
        data: {
          menu: {
            title: 'Priority',
            icon: 'fa fa-circle-o',
          }
        }
      },
      {
        path: 'pmreason',
        data: {
          menu: {
            title: 'PM Inactive',
            icon: 'fa fa-circle-o',
          }
        }
      },
      {
        path: 'status',
        data: {
          menu: {
            title: 'Status',
            icon: 'fa fa-circle-o',
          }
        }
      }
    ]
  },
  {
    path: 'eqmaintenance/equipment',
    data: {
      menu: {
        title: 'Equipment',
        icon: 'fa fa-minus-square',
      }
    }
  },
  {
    path: 'eqmaintenance/pmschedule',
    data: {
      menu: {
        title: 'PM Schedules',
        icon: 'fa fa-calendar',
      }
    }
  },
  {
    path: 'eqmaintenance/orders',
    data: {
      menu: {
        title: 'Orders',
        icon: 'fa fa-first-order',
      }
    }
  },
  {
    path: 'eqmaintenance/administration',
    data: {
      menu: {
        title: 'Administration',
        icon: 'fa fa-cog'
      }
    },
    children: [
      {
        path: 'roles',
        data: {
          menu: {
            title: 'Manage Roles',
            icon: 'fa fa-circle-o'
          }
        }
      },
      {
        path: 'eqmusersettings',
        data: {
          menu: {
            title: 'Manage User Settings',
            icon: 'fa fa-circle-o'
          }
        }
      }
    ]
  },
  {
    path: 'sysadmin',
    data: {
      menu: {
        title: 'Users',
        icon: 'fa fa-user'
      }
    },
    children: [
      {
        path: 'user',
        data: {
          menu: {
            title: 'Manage Users',
            icon: 'fa fa-circle-o'
          }
        }
      },
      {
        path: 'userrole',
        data: {
          menu: {
            title: 'Manage Roles',
            icon: 'fa fa-circle-o',
          }
        }
      },
      {
        path: 'usertype',
        data: {
          menu: {
            title: 'Manage User Types',
            icon: 'fa fa-circle-o',
          }
        }
      }
    ]
  },
  {
    path: 'project',
    data: {
      menu: {
        title: 'Current workspace',
        icon: 'fa fa-cube'
      }
    },
    children: [
      {
        path: 'workspaceteam',
        data: {
          menu: {
            title: 'Workspace Team',
            icon: 'fa fa-users',
          }
        }
      },
      {
        path: 'tagmaster',
        data: {
          menu: {
            title: 'Tag Master',
            icon: 'fa fa-tags'
          }
        }
      },
      {
        path: 'projectslist',
        data: {
          menu: {
            title: 'Projects',
            icon: 'fa fa-building-o',
          }
        }
      },
      {
        path: 'projectgroups',
        data: {
          menu: {
            title: 'Projects Groups',
            icon: 'fa fa-building-o',
          }
        }
      }
    ]
  },
  {
    path: 'project',
    data: {
      menu: {
        title: 'Administration',
        icon: 'fa fa-cogs'
      }
    },
    children: [
      {
        path: 'workspace',
        data: {
          menu: {
            title: 'Manage Workspaces',
            icon: 'fa fa-cubes',
          }
        }
      },
      {
        path: 'roles',
        data: {
          menu: {
            title: 'Manage Roles',
            icon: 'fa fa-circle-o'
          }
        }
      },
      {
        path: 'usersettings',
        data: {
          menu: {
            title: 'Manage User Settings',
            icon: 'fa fa-circle-o'
          }
        }
      }
    ]
  },
  {
    path: 'document',
    data: {
      menu: {
        title: 'Documentations',
        icon: 'fa fa-file'
      }
    },
    children: [
      {
        path: 'linkdocument',
        data: {
          menu: {
            title: 'Manage document',
            icon: 'fa fa-circle-o',
          }
        }
      }
    ]
  }
];

