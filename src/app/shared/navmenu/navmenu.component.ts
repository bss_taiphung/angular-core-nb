import {
  AfterViewInit, ChangeDetectionStrategy,
  Component,
  Input,
  OnInit
} from '@angular/core';
import {Router} from '@angular/router';
import {HubConnection} from '@aspnet/signalr-client';
import {
  cloneDeep,
  filter,
  forEach,
  groupBy,
  includes,
  orderBy,
  remove,
  replace
} from 'lodash';

import {PermissionService} from '../services/permission.service';
import {EqmSettingsService} from '../services/eqmsettings.service'
import {CommonService} from '../services/common.service';
import {NotificationsService} from '../../general/notifications/service/notifications.service';
import {
  EqmSettings,
  User,
  UserRole
} from '../../shared/models/eqmSettings';
import {ProjectSettingsService} from '../../project/shared/services/projectsettings.service';
import {ProjectSettingsDto} from '../../project/shared/services/models/projectSettings-model';
import {MessageService} from 'app/shared/messaging/service/messaging.service';
import {MessageContextFollowerDto} from '../messaging/service/messaging.model';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'shared-nav-menu',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './navmenu.component.html',
  styleUrls: ['./navmenu.component.scss']
})
export class NavmenuComponent implements AfterViewInit {

  menuItem: any;
  private pagesMenu: any = [];
  private Pages_Menu: any = [];
  private toBeRemove: any = [];
  private pagesMenuForSecondLevel: any = [];

  private hubConnection: HubConnection;
  private hubConnectionMsg: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  private notificationlist = [];

  public editDataItem: any;
  public messagePopUp: boolean;

  user: User = new User();
  userRole: UserRole = new UserRole();
  accessPermission: EqmSettings = new EqmSettings();
  workspaceSetting: ProjectSettingsDto = new ProjectSettingsDto();

  @Input()
  public set changeNaveMenu(data: any) {
    if (data !== undefined) {
      this.setMenulist();
    }
  }

  timeout = null;

  constructor(private notificationsService: NotificationsService,
              private messageService: MessageService,
              private permissionService: PermissionService,
              private eqmSettingsService: EqmSettingsService,
              private commonService: CommonService,
              private projectSettingsService: ProjectSettingsService,
              private router: Router) {

    this.userRole = this.eqmSettingsService.getUserRoles();
    this.workspaceSetting = this.projectSettingsService.getWorkSpaceSettings();
    this.accessPermission = this.eqmSettingsService.getEqmSettings();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.notificationSignalR();
    }, 2000);
    setTimeout(() => {
      this.chatSignalR();
    }, 2000);
  }

  setMenulist() {
    this.user = this.eqmSettingsService.getUser();
    this.Pages_Menu = [];
    this.Pages_Menu.push({
      path: 'general/dashboard',
      data: {
        menu: {
          title: 'Dashboard',
          icon: 'fa fa-dashboard',
        }
      }
    });
    this.Pages_Menu.push({
      path: 'general/notifications',
      data: {
        menu: {
          title: 'Notification',
          icon: 'fa fa-bell',
          templateText: this.user.unreadNotifications
        }
      }
    });
    this.Pages_Menu.push({
      path: 'general/messages',
      data: {
        menu: {
          title: 'Messaging',
          icon: 'fa fa-comments',
          templateText: this.user.unreadMessages,
          click: 'true'
        }
      }
    });
    const selectedModule = this.commonService.getCookies('module');
    if (selectedModule === 'eqm') {
      this.loadEqmMenu();
    }

    if (selectedModule === 'sysadmin') {
      this.loadSysAdminMenu();
    }

    if (selectedModule === 'projects') {
      this.loadProjectMenu();
    }

    if (selectedModule === 'documents') {
      this.loadDocumentMenu();
    }

    if (selectedModule === 'toolingmanagement') {
      this.loadToolingManagementMenu();
    }

    if (selectedModule === 'sales') {
      this.loadSalesMenu();
    }

    this.menuItem = this.Pages_Menu;
    switch (selectedModule) {
      case 'eqm': {
        this.router.navigate(['app/eqmaintenance/orders']);
        break;
      }
      case 'sysadmin': {
        this.router.navigate(['app/sysadmin/user']);
        break;
      }
      case 'projects': {
        this.router.navigate(['app/project/projectslist']);
        break;
      }
      case 'documents': {
        this.router.navigate(['app/document/linkdocument']);
        break;
      }
      case 'toolingmanagement': {
        this.router.navigate(['app/toolingmanagement/requisition']);
        break;
      }
      case 'sales': {
        this.router.navigate(['app/sales/customers']);
        break;
      }
      case 'dashboard': {
        this.router.navigate(['app/general/dashboard']);
        break;
      }
      default: {
        // this.router.navigate(['auth/login']);
        this.router.navigate(['app/general/dashboard']);
        break;
      }
    }
    setTimeout(() => {
      this.setUnReadNotificationCounter(this.user.unreadNotifications);
    }, 3000);
    setTimeout(() => {
      this.setUnreadMessageCounter(this.user.unreadMessages);
    }, 4000);
  }

  loadEqmMenu() {
    this.user = this.eqmSettingsService.getUser();
    this.userRole = this.eqmSettingsService.getUserRoles();
    this.accessPermission = this.eqmSettingsService.getEqmSettings();
    let childrenitem = [];
    childrenitem.push({
      path: 'manufacturer',
      data: {
        menu: {
          title: 'Manufacturer',
          icon: 'fa fa-circle-o'
        }
      }
    });
    childrenitem.push({
      path: 'part',
      data: {
        menu: {
          title: 'Part',
          icon: 'fa fa-circle-o',
        }
      }
    });
    childrenitem.push({
      path: 'scheduletype',
      data: {
        menu: {
          title: 'Schedule Type',
          icon: 'fa fa-circle-o',
        }
      }
    });
    childrenitem.push({
      path: 'ordertype',
      data: {
        menu: {
          title: 'Order Type',
          icon: 'fa fa-circle-o',
        }
      }
    });
    childrenitem.push({
      path: 'failure-type',
      data: {
        menu: {
          title: 'Failure Type',
          icon: 'fa fa-circle-o',
        }
      }
    });
    childrenitem.push({
      path: 'equipment-type',
      data: {
        menu: {
          title: 'Equipment Type',
          icon: 'fa fa-circle-o',
        }
      }
    });

    childrenitem.push({
      path: 'priority',
      data: {
        menu: {
          title: 'Priority',
          icon: 'fa fa-circle-o',
        }
      }
    });

    childrenitem.push({
      path: 'pmreason',
      data: {
        menu: {
          title: 'PM Inactive',
          icon: 'fa fa-circle-o',
        }
      }
    });

    childrenitem.push({
      path: 'status',
      data: {
        menu: {
          title: 'Status',
          icon: 'fa fa-circle-o',
        }
      }
    });

    this.Pages_Menu.push({
      path: 'eqmaintenance',
      data: {
        menu: {
          title: 'Master Data',
          icon: 'fa fa-table'
        }
      },
      children: childrenitem
    });


    this.Pages_Menu.push({
      path: 'eqmaintenance/equipment',
      data: {
        menu: {
          title: 'Equipment',
          icon: 'fa fa-minus-square',
        }
      }
    });
    this.Pages_Menu.push({
      path: 'eqmaintenance/pmschedule',
      data: {
        menu: {
          title: 'PM Schedules',
          icon: 'fa fa-calendar',
        }
      }
    });

    this.Pages_Menu.push({
      path: 'eqmaintenance/orders',
      data: {
        menu: {
          title: 'Orders',
          icon: 'fa fa-first-order',
        }
      }
    });


    childrenitem = [];
    if (this.accessPermission && this.accessPermission.canManageRoles) {
      childrenitem.push({
        path: 'roles',
        data: {
          menu: {
            title: 'Roles',
            icon: 'fa fa-user'
          }
        }
      });
    }
    if (this.accessPermission && this.accessPermission.canAssignRoles) {
      childrenitem.push({
        path: 'eqmusersettings',
        data: {
          menu: {
            title: 'User Settings',
            icon: 'fa fa-cog'
          }
        }
      });
    }
    if (childrenitem.length > 0) {
      this.Pages_Menu.push({
        path: 'eqmaintenance/administration',
        data: {
          menu: {
            title: 'Administration',
            icon: 'fa fa-cogs'
          }
        },
        children: childrenitem
      });
    }
  }

  loadSysAdminMenu() {
    this.workspaceSetting = this.projectSettingsService.getWorkSpaceSettings();
    if (this.userRole && this.userRole.canViewSysAdmin) {
      const childrenitem = [];
      childrenitem.push({
        path: 'user',
        data: {
          menu: {
            title: 'Manage Users',
            icon: 'fa fa-circle-o'
          }
        }
      });
      childrenitem.push({
        path: 'userrole',
        data: {
          menu: {
            title: 'Manage Roles',
            icon: 'fa fa-user',
          }
        }
      });
      childrenitem.push({
        path: 'usertype',
        data: {
          menu: {
            title: 'Manage User Types',
            icon: 'fa fa-circle-o',
          }
        }
      });

      this.Pages_Menu.push({
        path: 'sysadmin',
        data: {
          menu: {
            title: 'Users',
            icon: 'fa fa-user'
          }
        },
        children: childrenitem
      });
    }
  }

  loadProjectMenu() {
    this.workspaceSetting = this.projectSettingsService.getWorkSpaceSettings();
    let childrenitem = [];

    childrenitem.push({
      path: 'projectslist',
      data: {
        menu: {
          title: 'Projects',
          icon: 'fa fa-building-o',
          click: 'false'
        }
      }
    });

    if (this.workspaceSetting &&
      (this.workspaceSetting.canCreateWorkspaces ||
        this.workspaceSetting.globalWorkspaceAdmin ||
        this.workspaceSetting.globalWorkspaceViewing)) {

      childrenitem.push({
        path: 'projectgroups',
        data: {
          menu: {
            title: 'Projects Groups',
            icon: 'fa fa-building-o',
            click: 'false'
          }
        }
      });
    }

    childrenitem.push({
      path: 'tagmaster',
      data: {
        menu: {
          title: 'Tag Master',
          icon: 'fa fa-tags',
          click: 'false'
        }
      }
    });

    childrenitem.push({
      path: 'workspaceteam',
      data: {
        menu: {
          title: 'Workspace Management',
          icon: 'fa fa-users',
          click: 'false'
        }
      }
    });

    this.Pages_Menu.push({
      path: 'project',
      data: {
        menu: {
          title: 'Current workspace',
          icon: 'fa fa-cube',
          click: 'true'
        }
      },
      children: childrenitem
    });
    childrenitem = [];
    if (this.workspaceSetting &&
      (this.workspaceSetting.globalWorkspaceAdmin ||
        this.workspaceSetting.globalWorkspaceViewing)) {


      //childrenitem.push({
      //  path: 'workspace',
      //  data: {
      //    menu: {
      //      title: 'Manage Workspaces',
      //      icon: 'fa fa-cubes',
      //    }
      //  }
      //});
    }
    if (this.workspaceSetting &&
      (this.workspaceSetting.globalWorkspaceAdmin)) {
      childrenitem.push({
        path: 'roles',
        data: {
          menu: {
            title: 'Manage Roles',
            icon: 'fa fa-user'
          }
        }
      });
      childrenitem.push({
        path: 'usersettings',
        data: {
          menu: {
            title: 'User Settings',
            icon: 'fa fa-cog'
          }
        }
      });
    }
    if (childrenitem.length > 0) {
      this.Pages_Menu.push({
        path: 'project',
        data: {
          menu: {
            title: 'Administration',
            icon: 'fa fa-cogs'
          }
        },
        children: childrenitem
      });
    }

  }

  loadDocumentMenu() {
    this.Pages_Menu.push({
      path: 'document/linkdocument',
      data: {
        menu: {
          title: 'Manage Documents',
          icon: 'fa fa-file'
        }
      }
    });
  }

  loadSalesMenu() {

    let childrenItems = [];
    childrenItems.push({
      path: 'customers',
      data: {
        menu: {
          title: 'Customers',
          icon: 'fa fa-circle-o'
        }
      }
    });
    childrenItems.push({
      path: 'customersfamilies',
      data: {
        menu: {
          title: 'Customers Families',
          icon: 'fa fa-circle-o',
        }
      }
    });
    childrenItems.push({
      path: 'dispositions',
      data: {
        menu: {
          title: 'Dispositions',
          icon: 'fa fa-circle-o',
        }
      }
    });
    childrenItems.push({
      path: 'materialtypes',
      data: {
        menu: {
          title: 'Material Type',
          icon: 'fa fa-circle-o',
        }
      }
    });
    childrenItems.push({
      path: 'packaging',
      data: {
        menu: {
          title: 'Packaging',
          icon: 'fa fa-circle-o',
        }
      }
    });
    childrenItems.push({
      path: 'partsegments',
      data: {
        menu: {
          title: 'Part Segments',
          icon: 'fa fa-circle-o',
        }
      }
    });
    childrenItems.push({
      path: 'pressgroups',
      data: {
        menu: {
          title: 'Press Groups',
          icon: 'fa fa-circle-o',
        }
      }
    });
    childrenItems.push({
      path: 'programs',
      data: {
        menu: {
          title: 'Programs',
          icon: 'fa fa-circle-o',
        }
      }
    });
    childrenItems.push({
      path: 'ratelist',
      data: {
        menu: {
          title: 'Rate List',
          icon: 'fa fa-circle-o',
        }
      }
    });
    childrenItems.push({
      path: 'statuses',
      data: {
        menu: {
          title: 'Statuses',
          icon: 'fa fa-circle-o',
        }
      }
    });
    childrenItems.push({
      path: 'vendors',
      data: {
        menu: {
          title: 'Vendors',
          icon: 'fa fa-circle-o',
        }
      }
    });
    this.Pages_Menu.push({
      path: 'sales',
      data: {
        menu: {
          title: 'Master Data',
          icon: 'fa fa-table'
        }
      },
      children: childrenItems
    });

    this.Pages_Menu.push({
      path: 'sales/equipment',
      data: {
        menu: {
          title: 'Equipment',
          icon: 'fa fa-minus-square',
        }
      }
    });
    this.Pages_Menu.push({
      path: 'sales/pmschedule',
      data: {
        menu: {
          title: 'PM Schedules',
          icon: 'fa fa-calendar',
        }
      }
    });
    this.Pages_Menu.push({
      path: 'sales/orders',
      data: {
        menu: {
          title: 'Orders',
          icon: 'fa fa-first-order',
        }
      }
    });

    childrenItems = [];
    childrenItems.push({
      path: 'roles',
      data: {
        menu: {
          title: 'Roles',
          icon: 'fa fa-user'
        }
      }
    });
    childrenItems.push({
      path: 'eqmusersettings',
      data: {
        menu: {
          title: 'User Settings',
          icon: 'fa fa-cog'
        }
      }
    });
    this.Pages_Menu.push({
      path: 'eqmaintenance/administration',
      data: {
        menu: {
          title: 'Administration',
          icon: 'fa fa-cogs'
        }
      },
      children: childrenItems
    });
  }

  loadToolingManagementMenu() {
    let childrenitem = [];

    childrenitem.push({
      path: 'material',
      data: {
        menu: {
          title: 'Material',
          icon: 'fa fa-lemon-o',
        }
      }
    });
    childrenitem.push({
      path: 'status',
      data: {
        menu: {
          title: 'Status',
          icon: 'fa fa-eercast',
        }
      }
    });
    childrenitem.push({
      path: 'reason',
      data: {
        menu: {
          title: 'Reason Codes',
          icon: 'fa fa-rebel',
        }
      }
    });

    this.Pages_Menu.push({
      path: 'toolingmanagement',
      data: {
        menu: {
          title: 'Master Data',
          icon: 'fa fa-file'
        }
      },
      children: childrenitem
    });

    this.Pages_Menu.push({
      path: 'toolingmanagement/requisition',
      data: {
        menu: {
          title: 'Requisitions',
          icon: 'fa fa-first-order',
        }
      }
    });

    this.Pages_Menu.push({
      path: 'toolingmanagement/updatePrint',
      data: {
        menu: {
          title: 'Update Prints',
          icon: 'fa fa-print',
        }
      }
    });

    /*Settings*/
    childrenitem = [];
    childrenitem.push({
      path: 'role',
      data: {
        menu: {
          title: 'Roles',
          icon: 'fa fa-user',
        }
      }
    });
    childrenitem.push({
      path: 'userSetting',
      data: {
        menu: {
          title: 'User Settings',
          icon: 'fa fa-cog'
        }
      }
    });
    if (childrenitem.length > 0) {
      this.Pages_Menu.push({
        path: 'toolingmanagement',
        data: {
          menu: {
            title: 'Administration',
            icon: 'fa fa-cogs'
          }
        },
        children: childrenitem
      });
    }
  }

  prepareArrayToDeleteNode(itm: any) {
    const index = this.pagesMenu.indexOf(itm);

    if (index !== -1) {
      this.toBeRemove.push(index);
    }
  }

  removeChildrenFromMenu(itm: any, children: any) {

    const arr = [];

    const masterIndex = this.pagesMenu.indexOf(itm);

    const childIndex = itm.children.indexOf(children);

    if (childIndex !== -1 && masterIndex !== -1) {
      const arr1 = [masterIndex, childIndex];

      arr.push(arr1);
      this.pagesMenuForSecondLevel.push(arr); //create 2 level of array menu, so then we can remove it's childrens letter
    }
  }

  notificationSignalR() {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started notifiaction ' + this.eqmSettingsService.getUser().userId);

        this.hubConnection
          .invoke('Subscribe', 'General', 'UnReadNotification', 'UserId', this.user.userId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
      const _responseText = messageJson;
      const result200: any = null;
      const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      if (operation.toLowerCase() === 'update' && this.eqmSettingsService.getUser().userId === data.UserId) {
        this.setUnReadNotificationCounter(data.UnRead);
      }
    });
  }

  chatSignalR() {
    this.hubConnectionMsg =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnectionMsg
      .start()
      .then(() => {
        console.log('Connection started for Message Unread Counter');

        this.hubConnectionMsg
          .invoke('Subscribe', 'General', 'MessageUnreadCount', 'UserId', this.eqmSettingsService.getUser().userId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnectionMsg.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
      const _responseText = messageJson;
      let result200: any = null;
      const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      const resultData200 = this.commonService.toCamel(data);
      result200 = resultData200 ? MessageContextFollowerDto.fromJS(resultData200) : new MessageContextFollowerDto();
      if (operation.toLowerCase() === 'update' && this.eqmSettingsService.getUser().userId === data.UserId) {
        this.setUnreadMessageCounter(result200.unreadCount);
      }
    });
  }

  setUnReadNotificationCounter(unRead: number = 0) {
    const element = document.getElementById('sidemenuNotification');
    if (typeof (element) != 'undefined' && element != null) {
      if (unRead > 0) {
        document.getElementById('sidemenuNotification').innerHTML = unRead + '';
        document.getElementById('sidemenuNotification').style.display = 'block';
      } else {
        document.getElementById('sidemenuNotification').style.display = 'none';
      }
      const updateUserData = this.eqmSettingsService.getUser();
      updateUserData.unreadNotifications = unRead;
      this.eqmSettingsService.setUserCookies(updateUserData);
    }
  }

  setUnreadMessageCounter(unRead: number = 0) {
    const element = document.getElementById('sidemenuMessaging');
    if (typeof (element) != 'undefined' && element != null) {
      if (unRead > 0) {
        document.getElementById('sidemenuMessaging').innerHTML = unRead + '';
        document.getElementById('sidemenuMessaging').style.display = 'block';
      } else {
        document.getElementById('sidemenuMessaging').style.display = 'none';
      }
      const updateUserData = this.eqmSettingsService.getUser();
      updateUserData.unreadMessages = unRead;
      this.eqmSettingsService.setUserCookies(updateUserData);
    }
  }

  menuClick(item: any) {
    this.editDataItem = true;
    this.messagePopUp = true;
  }

  childMenuClick(item: any) {
    this.router.navigate(['app/project/projectslist']);
  }

  public cancelHandler() {
    this.editDataItem = undefined;
    this.messagePopUp = undefined;
  }
}
