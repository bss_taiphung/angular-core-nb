// region IEqmSettings

export interface IEqmSettings {
  userSettingsId: number;
  roleId: number;
  roleName: string;
  currentSiteId: number;
  canToggleSites: boolean;
  canEditOrders: boolean;
  canEditMasterData: boolean;
  canEditSchedules: boolean;
  canWorkOnUnassignedOrders: boolean;
  canAddPartsToOrders: boolean;
  canCreateRelatedOrders: boolean;
  canManageRoles: boolean;
  canAssignRoles: boolean;
  receiveSupervisorNotifications: boolean;
  showOnlyMyAssignments: boolean;
  canAccessEqm: boolean;
  laborHour: any;
  chgPWDNextLogin: boolean;
}

export class EqmSettings implements IEqmSettings {
  userSettingsId: number;
  roleId: number;
  roleName: string;
  currentSiteId: number;
  canToggleSites: boolean;
  canEditOrders: boolean;
  canEditMasterData: boolean;
  canEditSchedules: boolean;
  canWorkOnUnassignedOrders: boolean;
  canAddPartsToOrders: boolean;
  canCreateRelatedOrders: boolean;
  canManageRoles: boolean;
  canAssignRoles: boolean;
  receiveSupervisorNotifications: boolean;
  showOnlyMyAssignments: boolean;
  canAccessEqm: boolean;
  laborHour: any;
  chgPWDNextLogin: boolean;

  static fromJS(data: any): EqmSettings {
    const result = new EqmSettings();
    result.init(data);
    return result;
  }

  constructor(data?: IEqmSettings) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.currentSiteId = data['currentSiteId'];
      this.canAccessEqm = data['canAccessEQM'];

      if (data.userRole) {
        this.roleId = data.userRole['roleId'];
        this.roleName = data.userRole['roleName'];
        this.canToggleSites = data.userRole['canToggleSites'];
        this.canEditOrders = data.userRole['canEditOrders'];
        this.canEditMasterData = data.userRole['canEditMasterData'];
        this.canEditSchedules = data.userRole['canEditSchedules'];
        this.canWorkOnUnassignedOrders = data.userRole['canWorkOnUnassignedOrders'];
        this.canAddPartsToOrders = data.userRole['canAddPartsToOrders'];
        this.canCreateRelatedOrders = data.userRole['canCreateRelatedOrders'];
        this.canManageRoles = data.userRole['canManageRoles'];
        this.canAssignRoles = data.userRole['canAssignRoles'];
        this.receiveSupervisorNotifications = data.userRole['receiveSupervisorNotifications'];
        this.laborHour = data['laborHour'];
        this.chgPWDNextLogin = data['chgPWDNextLogin'];

      }

      if (data.userSettings) {
        this.userSettingsId = data.userSettings['userSettingsId'];
        this.showOnlyMyAssignments = data.userSettings['showOnlyMyAssignments'];
      }
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['userSettingsId'] = this.userSettingsId;
    data['roleId'] = this.roleId;
    data['roleName'] = this.roleName;
    data['currentSiteId'] = this.currentSiteId;
    data['canToggleSites'] = this.canToggleSites;
    data['canEditOrders'] = this.canEditOrders;
    data['canEditMasterData'] = this.canEditMasterData;
    data['canEditSchedules'] = this.canEditSchedules;
    data['canWorkOnUnassignedOrders'] = this.canWorkOnUnassignedOrders;
    data['canAddPartsToOrders'] = this.canAddPartsToOrders;
    data['canCreateRelatedOrders'] = this.canCreateRelatedOrders;
    data['canManageRoles'] = this.canManageRoles;
    data['canAssignRoles'] = this.canAssignRoles;
    data['receiveSupervisorNotifications'] = this.receiveSupervisorNotifications;
    data['showOnlyMyAssignments'] = this.showOnlyMyAssignments;
    data['canAccessEQM'] = this.canAccessEqm;
    return data;
  }
}

// endregion

// region IUser

export interface IUser {
  active: boolean;
  userId: number;
  firstName: string;
  lastName: string;
  displayName: string;
  userPin: number;
  email: string;
  password: string;
  userTypeId: number;
  siteId: number;
  mobilePhone: number;
  pager: number;
  o365User: string;
  proxCardCode: string;
  supervisorUserId: number;
  departmentId: number;
  userRoleId: number;
  receiveEmailNotifications: boolean;
  receiveSmsNotifications: boolean;
  defaultUrl: string;
  unreadNotifications: number;
  unreadMessages: number;
  userTypeName: string;
  laborReporting: boolean;

}

export class User implements IUser {
  active: boolean;
  userId: number;
  firstName: string;
  lastName: string;
  displayName: string;
  userPin: number;
  email: string;
  password: string;
  userTypeId: number;
  siteId: number;
  mobilePhone: number;
  pager: number;
  o365User: string;
  proxCardCode: string;
  supervisorUserId: number;
  departmentId: number;
  userRoleId: number;
  receiveEmailNotifications: boolean;
  receiveSmsNotifications: boolean;
  defaultUrl: string;
  unreadNotifications: number;
  userTypeName: string;
  laborReporting: boolean;
  unreadMessages: number;

  static fromJs(data: any): User {
    const result = new User();
    result.init(data);
    return result;
  }

  constructor(data?: IUser) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {

      this.active = data['active'];
      this.userId = data['userId'];
      this.firstName = data['firstName'];
      this.lastName = data['lastName'];
      this.displayName = data['displayName'];
      this.userPin = data['userPIN'];
      this.email = data['email'];
      this.password = data['password'];
      this.userTypeId = data['userTypeId'];
      this.siteId = data['siteId'];
      this.mobilePhone = data['mobilePhone'];
      this.pager = data['pager'];
      this.o365User = data['o365User'];
      this.proxCardCode = data['proxCardCode'];
      this.supervisorUserId = data['supervisorUserId'];
      this.departmentId = data['departmentId'];
      this.userRoleId = data['userRoleId'];
      this.receiveEmailNotifications = data['receiveEmailNotifications'];
      this.receiveSmsNotifications = data['receiveSMSNotifications'];
      this.defaultUrl = data['defaultURL'];
      this.unreadNotifications = data['unreadNotifications'] != null ? data['unreadNotifications'] : 0;
      this.unreadMessages = data['unreadMessages'] != null ? data['unreadMessages'] : 0;
      this.userTypeName = data['userTypeName'];
      this.laborReporting = data['laborReporting'];

    }
  }

  toJson(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['active'] = this.active;
    data['userId'] = this.userId;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['displayName'] = this.displayName;
    data['userPIN'] = this.userPin;
    data['email'] = this.email;
    data['password'] = this.password;
    data['userTypeId'] = this.userTypeId;
    data['siteId'] = this.siteId;
    data['mobilePhone'] = this.mobilePhone;
    data['pager'] = this.pager;
    data['o365User'] = this.o365User;
    data['proxCardCode'] = this.proxCardCode;
    data['supervisorUserId'] = this.supervisorUserId;
    data['departmentId'] = this.departmentId;
    data['userRoleId'] = this.userRoleId;
    data['receiveEmailNotifications'] = this.receiveEmailNotifications;
    data['receiveSMSNotifications'] = this.receiveSmsNotifications;
    data['defaultURL'] = this.defaultUrl;
    data['unreadNotifications'] = this.unreadNotifications;
    data['userTypeName'] = this.userTypeName;
    data['laborReporting'] = this.laborReporting;
    data['unreadMessages'] = this.unreadMessages;
    return data;
  }
}

// endregion

// region IUserRole

export interface IUserRole {

  userRoleId: number;
  userRoleName: string;
  canAccessUserMaintenance: boolean;
  canViewSysAdmin: boolean;
  canViewProjects: boolean;
  canViewEquipmentMaintenance: boolean;
  canViewProcessLog: boolean;
  canViewToolingManagement: boolean;
  canViewReportingAndAnalytics: boolean;
  canViewDocumentManagement: boolean;
  canViewDispatch: boolean;
  canViewQualityManagement: boolean;
  canViewWIPTracking: boolean;
  canViewSuppliers: boolean;
  canViewProductionSupport: boolean;
  canAccessUserTypes: boolean;
}

export class UserRole implements IUserRole {
  userRoleId: number;
  userRoleName: string;
  canAccessUserMaintenance: boolean;
  canViewSysAdmin: boolean;
  canViewProjects: boolean;
  canViewEquipmentMaintenance: boolean;
  canViewProcessLog: boolean;
  canViewToolingManagement: boolean;
  canViewReportingAndAnalytics: boolean;
  canViewDocumentManagement: boolean;
  canViewDispatch: boolean;
  canViewQualityManagement: boolean;
  canViewWIPTracking: boolean;
  canViewSuppliers: boolean;
  canViewProductionSupport: boolean;
  canAccessUserTypes: boolean;

  static fromJS(data: any): UserRole {
    const result = new UserRole();
    result.init(data);
    return result;
  }

  constructor(data?: IUserRole) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.userRoleId = data['userRoleId'];
      this.userRoleName = data['userRoleName'];
      this.canAccessUserMaintenance = data['canAccessUserMaintenance'];
      this.canViewSysAdmin = data['canViewSysAdmin'];
      this.canViewProjects = data['canViewProjects'];
      this.canViewEquipmentMaintenance = data['canViewEquipmentMaintenance'];
      this.canViewProcessLog = data['canViewProcessLog'];
      this.canViewToolingManagement = data['canViewToolingManagement'];
      this.canViewReportingAndAnalytics = data['canViewReportingAndAnalytics'];
      this.canViewDocumentManagement = data['canViewDocumentManagement'];
      this.canViewDispatch = data['canViewDispatch'];
      this.canViewQualityManagement = data['canViewQualityManagement'];
      this.canViewWIPTracking = data['canViewWIPTracking'];
      this.canViewDocumentManagement = data['canViewDocumentManagement'];
      this.canViewSuppliers = data['canViewSuppliers'];
      this.canViewProductionSupport = data['canViewProductionSupport'];
      this.canAccessUserTypes = data['canAccessUserTypes'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['userRoleId'] = this.userRoleId;
    data['userRoleName'] = this.userRoleName;
    data['canAccessUserMaintenance'] = this.canAccessUserMaintenance;
    data['canViewSysAdmin'] = this.canViewSysAdmin;
    data['canViewProjects'] = this.canViewProjects;
    data['canViewEquipmentMaintenance'] = this.canViewEquipmentMaintenance;
    data['canViewProcessLog'] = this.canViewProcessLog;
    data['canViewToolingManagement'] = this.canViewToolingManagement;
    data['canViewReportingAndAnalytics'] = this.canViewReportingAndAnalytics;
    data['canViewDocumentManagement'] = this.canViewDocumentManagement;
    data['canViewDispatch'] = this.canViewDispatch;
    data['canViewQualityManagement'] = this.canViewQualityManagement;
    data['canViewWIPTracking'] = this.canViewWIPTracking;
    data['canViewDocumentManagement'] = this.canViewDocumentManagement;
    data['canViewSuppliers'] = this.canViewSuppliers;
    data['canViewProductionSupport'] = this.canViewProductionSupport;
    data['canAccessUserTypes'] = this.canAccessUserTypes;
    return data;
  }
}

// endregion

// region IParamUserLogin

export interface IParamUserLogin {
  userPIN: number;
  deviceHostName: string;
  deviceIPAddress: string;
  password: string;
  deviceMACAddress: string;
}

export class ParamUserLogin implements IParamUserLogin {
  userPIN: number;
  deviceHostName: string;
  deviceIPAddress: string;
  password: string;
  deviceMACAddress: string;
}

// endregion
