// region ISelectListDto

export interface ISelectListDto {
  text: string;
  value: string;
  isSelected: boolean;

}

export class SelectListDto implements ISelectListDto {
  text: string;
  value: string;
  isSelected: boolean;
  userAvatar: string;
  extraDetail: any;

  static fromJS(data: any): SelectListDto {
    const result = new SelectListDto();
    result.init(data);
    return result;
  }

  constructor(data?: ISelectListDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {

      this.text = data['text'];
      this.value = data['value'];
      this.isSelected = data['isSelected'];
    }
  }

  toJSON(data?: any) {

    data = typeof data === 'object' ? data : {};
    data['text'] = this.text;
    data['value'] = this.value;
    data['isSelected'] = this.isSelected;
    return data;
  }
}

// endregion

// region IResultDtoOfSelectListDto

export interface IResultDtoOfSelectListDto {
  results: SelectListDto[];
}

export class ResultDtoOfSelectListDto implements IResultDtoOfSelectListDto {
  results: SelectListDto[];

  static fromJS(data: any): ResultDtoOfSelectListDto {
    const result = new ResultDtoOfSelectListDto();
    result.init(data);
    return result;
  }

  constructor(data?: IResultDtoOfSelectListDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {

    if (data && data != null) {
      if (data && data.constructor === Array) {
        this.results = [];
        for (const item of data) {
          this.results.push(SelectListDto.fromJS(item));
        }
      }

    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }

    return data;
  }
}

// endregion

