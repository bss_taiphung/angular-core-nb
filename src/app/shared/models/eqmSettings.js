"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EqmSettings = /** @class */ (function () {
    function EqmSettings(data) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    this[property] = data[property];
            }
        }
    }
    EqmSettings.prototype.init = function (data) {
        if (data) {
            this.currentSiteId = data["currentSiteId"];
            this.canAccessEqm = data["canAccessEQM"];
            if (data.userRole) {
                this.roleId = data.userRole["roleId"];
                this.roleName = data.userRole["roleName"];
                this.canToggleSites = data.userRole["canToggleSites"];
                this.canEditOrders = data.userRole["canEditOrders"];
                this.canEditMasterData = data.userRole["canEditMasterData"];
                this.canEditSchedules = data.userRole["canEditSchedules"];
                this.canWorkOnUnassignedOrders = data.userRole["canWorkOnUnassignedOrders"];
                this.canAddPartsToOrders = data.userRole["canAddPartsToOrders"];
                this.canCreateRelatedOrders = data.userRole["canCreateRelatedOrders"];
                this.canManageRoles = data.userRole["canManageRoles"];
                this.canAssignRoles = data.userRole["canAssignRoles"];
                this.receiveSupervisorNotifications = data.userRole["receiveSupervisorNotifications"];
            }
            if (data.userSettings) {
                this.userSettingsId = data.userSettings["userSettingsId"];
                this.showOnlyMyAssignments = data.userSettings["showOnlyMyAssignments"];
            }
        }
    };
    EqmSettings.fromJS = function (data) {
        var result = new EqmSettings();
        result.init(data);
        return result;
    };
    EqmSettings.prototype.toJSON = function (data) {
        data = typeof data === 'object' ? data : {};
        data["userSettingsId"] = this.userSettingsId;
        data["roleId"] = this.roleId;
        data["roleName"] = this.roleName;
        data["currentSiteId"] = this.currentSiteId;
        data["canToggleSites"] = this.canToggleSites;
        data["canEditOrders"] = this.canEditOrders;
        data["canEditMasterData"] = this.canEditMasterData;
        data["canEditSchedules"] = this.canEditSchedules;
        data["canWorkOnUnassignedOrders"] = this.canWorkOnUnassignedOrders;
        data["canAddPartsToOrders"] = this.canAddPartsToOrders;
        data["canCreateRelatedOrders"] = this.canCreateRelatedOrders;
        data["canManageRoles"] = this.canManageRoles;
        data["canAssignRoles"] = this.canAssignRoles;
        data["receiveSupervisorNotifications"] = this.receiveSupervisorNotifications;
        data["showOnlyMyAssignments"] = this.showOnlyMyAssignments;
        data["canAccessEQM"] = this.canAccessEqm;
        return data;
    };
    return EqmSettings;
}());
exports.EqmSettings = EqmSettings;
var User = /** @class */ (function () {
    function User(data) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    this[property] = data[property];
            }
        }
    }
    User.prototype.init = function (data) {
        if (data) {
            this.active = data["active"];
            this.userId = data["userId"];
            this.firstName = data["firstName"];
            this.lastName = data["lastName"];
            this.displayName = data["displayName"];
            this.userPin = data["userPIN"];
            this.email = data["email"];
            this.password = data["password"];
            this.userTypeId = data["userTypeId"];
            this.siteId = data["siteId"];
            this.mobilePhone = data["mobilePhone"];
            this.pager = data["pager"];
            this.o365User = data["o365User"];
            this.proxCardCode = data["proxCardCode"];
            this.supervisorUserId = data["supervisorUserId"];
            this.departmentId = data["departmentId"];
            this.userRoleId = data["userRoleId"];
            this.receiveEmailNotifications = data["receiveEmailNotifications"];
            this.receiveSmsNotifications = data["receiveSMSNotifications"];
            this.defaultUrl = data["defaultURL"];
            this.unreadNotifications = data["unreadNotifications"];
            this.userTypeName = data["userTypeName"];
        }
    };
    User.fromJs = function (data) {
        var result = new User();
        result.init(data);
        return result;
    };
    User.prototype.toJson = function (data) {
        data = typeof data === 'object' ? data : {};
        data["active"] = this.active;
        data["userId"] = this.userId;
        data["firstName"] = this.firstName;
        data["lastName"] = this.lastName;
        data["displayName"] = this.displayName;
        data["userPIN"] = this.userPin;
        data["email"] = this.email;
        data["password"] = this.password;
        data["userTypeId"] = this.userTypeId;
        data["siteId"] = this.siteId;
        data["mobilePhone"] = this.mobilePhone;
        data["pager"] = this.pager;
        data["o365User"] = this.o365User;
        data["proxCardCode"] = this.proxCardCode;
        data["supervisorUserId"] = this.supervisorUserId;
        data["departmentId"] = this.departmentId;
        data["userRoleId"] = this.userRoleId;
        data["receiveEmailNotifications"] = this.receiveEmailNotifications;
        data["receiveSMSNotifications"] = this.receiveSmsNotifications;
        data["defaultURL"] = this.defaultUrl;
        data["unreadNotifications"] = this.unreadNotifications;
        data["userTypeName"] = this.userTypeName;
        return data;
    };
    return User;
}());
exports.User = User;
var UserRole = /** @class */ (function () {
    function UserRole(data) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    this[property] = data[property];
            }
        }
    }
    UserRole.prototype.init = function (data) {
        if (data) {
            this.userRoleId = data["userRoleId"];
            this.userRoleName = data["userRoleName"];
            this.canAccessUserMaintenance = data["canAccessUserMaintenance"];
            this.canViewSysAdmin = data["canViewSysAdmin"];
            this.canViewProjects = data["canViewProjects"];
            this.canViewEquipmentMaintenance = data["canViewEquipmentMaintenance"];
            this.canViewProcessLog = data["canViewProcessLog"];
            this.canViewToolingManagement = data["canViewToolingManagement"];
            this.canViewReportingAndAnalytics = data["canViewReportingAndAnalytics"];
            this.canViewDocumentManagement = data["canViewDocumentManagement"];
            this.canViewDispatch = data["canViewDispatch"];
            this.canViewQualityManagement = data["canViewQualityManagement"];
            this.canViewWIPTracking = data["canViewWIPTracking"];
            this.canViewDocumentManagement = data["canViewDocumentManagement"];
            this.canViewSuppliers = data["canViewSuppliers"];
            this.canViewProductionSupport = data["canViewProductionSupport"];
            this.canAccessUserTypes = data["canAccessUserTypes"];
        }
    };
    UserRole.fromJS = function (data) {
        var result = new UserRole();
        result.init(data);
        return result;
    };
    UserRole.prototype.toJSON = function (data) {
        data = typeof data === 'object' ? data : {};
        data["userRoleId"] = this.userRoleId;
        data["userRoleName"] = this.userRoleName;
        data["canAccessUserMaintenance"] = this.canAccessUserMaintenance;
        data["canViewSysAdmin"] = this.canViewSysAdmin;
        data["canViewProjects"] = this.canViewProjects;
        data["canViewEquipmentMaintenance"] = this.canViewEquipmentMaintenance;
        data["canViewProcessLog"] = this.canViewProcessLog;
        data["canViewToolingManagement"] = this.canViewToolingManagement;
        data["canViewReportingAndAnalytics"] = this.canViewReportingAndAnalytics;
        data["canViewDocumentManagement"] = this.canViewDocumentManagement;
        data["canViewDispatch"] = this.canViewDispatch;
        data["canViewQualityManagement"] = this.canViewQualityManagement;
        data["canViewWIPTracking"] = this.canViewWIPTracking;
        data["canViewDocumentManagement"] = this.canViewDocumentManagement;
        data["canViewSuppliers"] = this.canViewSuppliers;
        data["canViewProductionSupport"] = this.canViewProductionSupport;
        data["canAccessUserTypes"] = this.canAccessUserTypes;
        return data;
    };
    return UserRole;
}());
exports.UserRole = UserRole;
var ParamUserLogin = /** @class */ (function () {
    function ParamUserLogin() {
    }
    return ParamUserLogin;
}());
exports.ParamUserLogin = ParamUserLogin;
//# sourceMappingURL=eqmSettings.js.map