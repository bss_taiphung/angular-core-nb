// region ITokenDto

export interface ITokenDto {
  tokenType: string;
  userName: string;
  accessToken: string;
  expires: string;
  issued: string;
}

export class TokenDto implements ITokenDto {
  tokenType: string;
  userName: string;
  accessToken: string;
  expires: string;
  issued: string;

  static fromJS(data: any): TokenDto {
    const result = new TokenDto();
    result.init(data);
    return result;
  }

  constructor(data?: TokenDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.tokenType = data['token_type'];
      this.userName = data['userName'];
      this.accessToken = data['access_token'];
      this.expires = data['.expires'];
      this.issued = data['.issued'];
    }
  }
}

// endregion

export class ParamInteger {
  IntegerValue: number;
}
