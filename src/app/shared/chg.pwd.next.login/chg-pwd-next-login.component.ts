import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';

import {ToasterHelperService} from '../services/toasterHelper.service';

import {
  EqmSettings,
  User,
  UserRole
} from '../models/eqmSettings';
import {CommonService} from '../services/common.service';
import {EqmSettingsService} from '../services/eqmsettings.service';
import {UserSettingsService} from '../../eqmaintenance/eqmusersettings/service/eqmusersettings.service';
import {EquipmentService} from '../../eqmaintenance/equipment/service/equipment.service';
import {ChangePwdNextLoginService} from './service/chg-pwd.service';
import {Router} from '@angular/router';


@Component({
  selector: 'chg-pwd-next-login',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [' :host >>> .k-dialog-close { display: none; } '],
  templateUrl: './html/chg-pwd-next-login.component.html'
})
export class ChangePasswordNextLoginComponent {
  @Input() public openChangePwdNextLoginDialog = false;
  eqmSettings: EqmSettings = new EqmSettings();
  user: User = new User();
  userRole: UserRole = new UserRole();
  public editForm: FormGroup = this.formBuilder.group({

    currentPassword: ['', Validators.required],
    newPassword: ['', Validators.required],
    confirmPassword: ['', Validators.required]
  }, {validator: this.checkIfMatchingPasswords('newPassword', 'confirmPassword')});

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private changePwdNextLoginService: ChangePwdNextLoginService,
              private userSettingsService: UserSettingsService,
              private equipmentService: EquipmentService,
              private formBuilder: FormBuilder,
              private router: Router) {
    this.eqmSettings = this.eqmSettingsService.getEqmSettings();
    this.user = this.eqmSettingsService.getUser();
    this.userRole = this.eqmSettingsService.getUserRoles();
    this.commonService.validateLogin();
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();

  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({notEquivalent: true});
      }
      else {
        return passwordConfirmationInput.setErrors(null);
      }
    }
  }

  errormessage: string = '';

  public saveHandler() {

    const data: any = {
      'userId': this.user.userId,
      'currentPassword': this.editForm.value.currentPassword,
      'newPassword': this.editForm.value.newPassword
    }
    this.changePwdNextLoginService.update(data,
      (resp) => {
        this.toasterService.success('', 'Change Password Next Login has done successfully');
        this.eqmSettings.chgPWDNextLogin = false;
        this.eqmSettingsService.setEqmSettings(this.eqmSettings);
        this.errormessage = '';
        this.openChangePwdNextLoginDialog = false;
      },
      (error) => {
        this.errormessage = error.error.exceptionMessage;
      }//this.toasterService.errorMessage(error)
    );

  }

  private closeForm(): void {
    this.openChangePwdNextLoginDialog = false;
    this.cancel.emit();
  }

  public onCancel(value: any) {
    this.openChangePwdNextLoginDialog = false;
    this.cancel.emit();
  }
}

