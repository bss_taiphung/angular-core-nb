import {Injectable} from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

import {CommonService} from '../../../shared/services/common.service';
import {ToasterHelperService} from '../../services/toasterHelper.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class ChangePwdNextLoginService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService, private http: HttpClient,
              private toasterService: ToasterHelperService) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/general/api/User/';
  }


  update(data: any, callbackSuccess, callbackError) {
    let url = this.apiBaseUrl + 'ChangePasswordNextLogin';
    const params = new HttpParams()
      .set('userId', data.userId)
      .set('currentPassword', data.currentPassword)
      .set('newPassword', data.newPassword);
    let options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };
    this.commonService.httpRequest('post', url, options,
      callbackSuccess,
      callbackError);
  }
}
