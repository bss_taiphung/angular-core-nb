import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';

import {FileUploadDto} from './fileUploadDto'

@Component({
  selector: 'file-upload',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './fileUpload.component.html'
})
export class FileUploadComponent {


  @Input() public fileType = [];

  @Output() onChange: EventEmitter<any> = new EventEmitter();

  @Output() onClick: EventEmitter<any> = new EventEmitter();

  getFiles(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        const fileUploadDto: FileUploadDto = new FileUploadDto();
        fileUploadDto.name = file.name;
        fileUploadDto.bytes = reader.result.split(',')[1];
        this.onChange.emit(fileUploadDto);
      };
    }
  }

  onFileClick(event) {
    event.target.value = null;
    this.onClick.emit(event);
  }

}
