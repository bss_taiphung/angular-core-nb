export class FileUploadDto {
  name: string;
  type: string;
  bytes: string;
}
