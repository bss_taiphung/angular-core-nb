import {Injectable} from '@angular/core';
import {
  Headers,
  Http,
  Response
} from '@angular/http';
import {HttpParams} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';
import {ParamInteger,
  TokenDto} from '../models/tokenDto';

import {CommonService} from './common.service'
import {environment} from '../../../environments/environment';

@Injectable()
export class AuthenticationService {

  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private http: Http, private commonService: CommonService) {
  }

  login(username: string, password: string): Observable<TokenDto> {

    const url = environment.bllApiBaseAddress + '/token';

    const body = new HttpParams()
      .set('username', username)
      .set('password', password)
      .set('MacAddress', this.commonService.getMacAddress())
      .set('grant_type', 'password');

    const httpOptions = {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    };

    return this.http.post(url, body.toString(), httpOptions).flatMap((response) => {
      return this.processLogin(response);
    });
  }

  ressetpassword(username: string, successCallback, errorCallback): any {
    const url = environment.bllApiBaseAddress + '/general/api/Logon/CreatePasswordResetRequest';
    const input: ParamInteger = new ParamInteger();
    input.IntegerValue = parseInt(username);
    const httpOptions = {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    };

    return this.http.post(url, input)
      .subscribe(
        res => {
          successCallback(res);
        },
        err => {
          errorCallback(err);
        });
  }

  completePasswordResetRequest(input: any, successCallback, errorCallback): any {
    const url = environment.bllApiBaseAddress + '/general/api/Logon/CompletePasswordResetRequest';
    return this.http.post(url, input)
      .subscribe(
        res => {
          successCallback(res);
        },
        err => {
          errorCallback(err);
        });
  }

  protected processLogin(response: Response): Observable<TokenDto> {
    const status = response.status;

    if (status === 200) {
      const responseText = response.text();
      let result200: any = null;
      const resultData200 = responseText === '' ? null : JSON.parse(responseText, this.jsonParseReviver);
      result200 = resultData200 ? TokenDto.fromJS(resultData200) : new TokenDto();
      return Observable.of(result200);
    }
    else if (status !== 200 && status !== 204) {
      const responseText = response.text();
    }

    return Observable.of<TokenDto>(<any>null);
  }
}
