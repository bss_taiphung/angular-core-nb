import { Injectable, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { EqmSettingsService } from "./eqmsettings.service"
import { find, includes } from 'lodash';
import { CommonService } from "./common.service";
import { EqmSettings } from "../models/eqmSettings"
@Injectable()
export class PermissionService {

  eqmSettings: EqmSettings = new EqmSettings();

  constructor(private eqmSettingsService: EqmSettingsService,
    private router: Router,
    private commonService: CommonService) {
    this.eqmSettings = this.eqmSettingsService.getEqmSettings();
  }

  private unAuthorizedUrl = "app/general/unauthorized";

  checkPermission(relativePath): boolean {
    var access = this.eqmSettingsService.getUserRoles();
    
    if (relativePath.indexOf('sysadmin') !== -1 && !access.canViewSysAdmin) {
      this.router.navigate([this.unAuthorizedUrl]);
      return false;
    }
    //else if (relativePath === '/app/eqmaintenance' && (!access.canViewEquipmentMaintenance || !this.eqmSettings.canAccessEqm)) {
    else if (relativePath === '/app/eqmaintenance' && (!access.canViewEquipmentMaintenance)) {
      this.router.navigate([this.unAuthorizedUrl]);
      return false;
    }

    else if (relativePath === '/app/eqmaintenance/equipment' && (!access.canViewEquipmentMaintenance)) {
      this.router.navigate([this.unAuthorizedUrl]);
      return false;
    }

    else if (relativePath === '/app/eqmaintenance/pmschedule' && (!access.canViewEquipmentMaintenance)) {
      this.router.navigate([this.unAuthorizedUrl]);
      return false;
    }

    else if (relativePath === '/app/eqmaintenance/orders' && (!access.canViewEquipmentMaintenance)) {
      this.router.navigate([this.unAuthorizedUrl]);
      return false;
    }

    else if (relativePath === '/app/eqmaintenance/administration/eqmusersettings') {
      if (!this.eqmSettings) {
        this.router.navigate([this.unAuthorizedUrl]);
        return false;
      }
      else if (!this.eqmSettings.canAssignRoles) {
        this.router.navigate([this.unAuthorizedUrl]);
        return false;
      }
    }

    else if (relativePath === '/app/eqmaintenance/administration/roles') {
      if (!this.eqmSettings) {
        this.router.navigate([this.unAuthorizedUrl]);
        return false;
      }
      else if (!this.eqmSettings.canManageRoles) {
        this.router.navigate([this.unAuthorizedUrl]);
        return false;
      }
    }

    else if (relativePath.includes('/app/project') && (!access.canViewProjects)) {
      this.router.navigate([this.unAuthorizedUrl]);
      return false;
    }

    else if (relativePath.includes('/app/document') && (!access.canViewDocumentManagement)) {
      this.router.navigate([this.unAuthorizedUrl]);
      return false;
    }

    return true;
  }
}

