import { Injectable } from '@angular/core';
import { ToasterService, ToasterConfig, Toast } from 'angular2-toaster';

@Injectable()
export class ToasterHelperService {
  private toasterService: ToasterService;
  public toasterconfig: ToasterConfig

  constructor(toasterService: ToasterService) {
    this.toasterService = toasterService;
    this.toasterconfig =
      new ToasterConfig({
        tapToDismiss: true,
        timeout: 2000,
        positionClass: "toast-bottom-right"
      });
  }

  success(title: string, message: string) {
    this.Notification(AlertType.success, title, message);
  }

  error(title: string, message: string) {
    var toast: Toast = {
      type: 'error',
      showCloseButton: true,
      body: message,
      timeout: 0
    };
    this.toasterService.pop(toast);
  }

  info(title: string, message: string) {
    this.Notification(AlertType.info, title, message);
  }

  warn(title: string, message: string) {
    this.Notification(AlertType.warning, title, message);
  }

  private Notification(type: AlertType, title: string, message: string) {
    this.toasterService.pop(AlertType[type], title, message);
  }

  clear() {
    this.toasterService.clear();
  }
  customErrorMessage(error: any) {
    var toast: Toast = {
      type: 'error',
      body: error,
      showCloseButton: true,
      timeout: 0
    };
    this.toasterService.pop(toast);
  }
  errorMessage(error: any) {
    var toast: Toast = {
      type: 'error',
      body: error.error.exceptionMessage,
      showCloseButton: true,
      timeout: 0
    };
    this.toasterService.pop(toast);
  }
}


export class Alert {
  type: AlertType;
  message: string;
}

export enum AlertType {
  success,
  error,
  info,
  warning
}
