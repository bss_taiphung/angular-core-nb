import {HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import {extend, each} from 'lodash';

export abstract class ApiService {
  /**
   * Handle HTTP error
   */
  protected handleError(error: HttpErrorResponse) {
    return Observable.throw(error.error);
  }

  /**
   * Get options
   * @param {{headers?: any; params?: any}} opts
   * @returns {any}
   */
  protected getOptions(opts?: { headers?: any, params?: any }) {
    let headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };
    if (opts && opts.headers) {
      headers = extend(headers, opts.headers);
    }
    const options: { headers: HttpHeaders, params?: HttpParams } = {
      headers: new HttpHeaders(headers),
    };
    if (opts && opts.params) {
      options.params = new HttpParams(opts.params);
      each(opts.params, (value, property) => {
        if (opts.params.hasOwnProperty(property)) {
          options.params = options.params.append(property, value);
        }
      });
    }
    return options;
  }
}
