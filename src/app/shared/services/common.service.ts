import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';
import {CookieService} from 'ngx-cookie-service';

import {ResultDtoOfSelectListDto} from '../models/selectListDto'
import {environment} from '../../../environments/environment';

const headers = new HttpHeaders({
  'Content-Type': 'application/json',
  'Accept': 'application/json'
})

@Injectable()
export class CommonService {
  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  public macAddressCookieName = 'MacAddress';
  public mss = 'Mss';

  constructor(private cookieService: CookieService,
              private http: HttpClient) {
    this.apiBaseUrl = environment.bllApiBaseAddress; // originUrl;
  }

  getAll(areaType: string, urltype: string): Observable<ResultDtoOfSelectListDto> {
    const url = this.apiBaseUrl + '/' + areaType + '/api/' + urltype + '/GetSelectList';
    const options = {
      headers: headers
    };

    return this.http.request<Response>('get', url, options)
      .flatMap((response) => {
        const result200 = ResultDtoOfSelectListDto.fromJS(response);
        return Observable.of(result200);
      });
  }

  getAllWithId(id: number, areaType: string, urltype: string): Observable<ResultDtoOfSelectListDto> {

    const url = this.apiBaseUrl + '/' + areaType + '/api/' + urltype + '/GetSelectList';

    const params = new HttpParams().set('id', id.toString());

    const options = {
      params: params
    };

    return this.http.request('get', url, options)
      .flatMap((response) => {
        const result200 = ResultDtoOfSelectListDto.fromJS(response);
        return Observable.of(result200);
      });
  }

  getAllWithUserId(id: number, areaType: string, urltype: string): Observable<ResultDtoOfSelectListDto> {
    const url = this.apiBaseUrl + '/' + areaType + '/api/' + urltype + '/GetSelectList';
    const params = new HttpParams().set('userid', id.toString());

    const options = {
      params: params
    };

    return this.http.request('get', url, options).flatMap((response) => {
      const result200 = ResultDtoOfSelectListDto.fromJS(response);
      return Observable.of(result200);
    });
  }

  getAllUserWithWorkspaceId(id: number, areaType: string, urltype: string): Observable<ResultDtoOfSelectListDto> {
    const url = this.apiBaseUrl + '/' + areaType + '/api/' + urltype + '/GetSelectListUserByWorkspce';
    const params = new HttpParams().set('workspaceId', id.toString());

    const options = {
      params: params
    };

    return this.http.request('get', url, options).flatMap((response) => {
      const result200 = ResultDtoOfSelectListDto.fromJS(response);
      return Observable.of(result200);
    });
  }

  initService() {
    const cookieMss = this.cookieService.get('Mss');
  }

  public httpGet(resource: string, params: HttpParams = new HttpParams()) {
    return this.http.get(`${this.apiBaseUrl}/${resource}`,
      {params, headers: headers});
  }

  public httpPost(url: string, input: any, successCallback: any, errorCallback: any) {
    return this.http.post(url, input)
      .subscribe(
        res => {
          successCallback(res);
        },
        err => {
          errorCallback(err);
        });
  };

  public httpRequest(method: string, url: string, options: any, successCallback: any, errorCallback: any) {
    return this.http.request(method, url, options)
      .subscribe(
        res => {
          successCallback(res);
        },
        err => {
          errorCallback(err);
        });
  }

  firstUpper(input: string) {
    return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1) : '';
  }

  toCamel(o) {
    let newO, origKey, newKey, value
    if (o instanceof Array) {
      newO = []
      for (origKey in o) {
        value = o[origKey]
        if (typeof value === 'object') {
          value = this.toCamel(value)
        }
        newO.push(value)
      }
    } else {
      newO = {}
      for (origKey in o) {
        if (o.hasOwnProperty(origKey)) {
          newKey = (origKey.charAt(0).toLowerCase() + origKey.slice(1) || origKey).toString()
          value = o[origKey]
          if (value instanceof Array || (value !== null && value.constructor === Object)) {
            value = this.toCamel(value)
          }
          newO[newKey] = value
        }
      }
    }
    return newO
  }

  getFilter(filter: any, isActive: boolean): string {
    let retFilter = '';

    if (isActive) {
      retFilter = 'Active eq true';
    }

    filter.forEach(itm => {
      let field = itm.field;
      const operator = itm.operator;
      const value = itm.value;

      if (operator === 'gte') {
        return '';
      }

      if (field && operator && value) {
        field = this.capitalizeFirstLetter(field);

        if (value === parseInt(value, 10)) {
          if (value > 0) {

            retFilter += (retFilter.length > 0) ? ' and ' : '';

            // if value is int and positive int
            if (operator === 'eq') {
              retFilter += field + ' eq ' + value + '';
            } else if (operator === 'contains') {
              retFilter += 'contains(' + field + ', ' + value + ')';
            } else if (operator === 'startswith') {
              retFilter += 'startswith(' + field + ', ' + value + ')';
            } else if (operator === 'endswith') {
              retFilter += 'endswith(' + field + ', ' + value + ')';
            } else {
              // eq condition becuase kendo has it on all type of input
              retFilter += field + ' eq ' + value + '';
            }
          } else {
            retFilter = '';
          }
        } else {
          retFilter += (retFilter.length > 0) ? ' and ' : '';

          if (operator === 'eq') {
            retFilter += field + ' eq \'' + value + '\'';
          } else if (operator === 'contains') {
            retFilter += 'contains(' + field + ', \'' + value + '\')';
          } else if (operator === 'startswith') {
            retFilter += 'startswith(' + field + ', \'' + value + '\')';
          } else if (operator === 'endswith') {
            retFilter += 'endswith(' + field + ', \'' + value + '\')';
          } else {
            // condition becuase kendo has it on all type of input
            retFilter += field + ' eq \'' + value + '\'';
          }
        }
      }
    });

    return retFilter;
  }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  validateLogin() {
    // To do, check old code and modify it with new httpclient strucuter
  }

  validateResponseCode() {
    // To do, check old code and modify it with new httpclient strucuter
  }

  s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

  guid() {
    return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + this.s4() + this.s4();
  }

  getMacAddress(): string {
    const mac = this.getCookies(this.macAddressCookieName);
    if (mac === undefined || mac === '') {
      const guid = this.guid().toString();
      this.setCookies(this.macAddressCookieName, guid);
      return guid;
    } else {
      return mac;
    }
  }


  // region Cookies methods
  public setCookies(key: string, value: any) {
    /*if (key === this.macAddressCookieName || key === this.mss) {
      this.cookieService.set(key, value, (360 * 30), '/', this.settingsProvider.config.cookieDomain);
    } else {
      this.cookieService.set(key, value, (360 * 30), '/');
    }*/
    this.cookieService.set(key, value, (360 * 30), '/');
  }

  public getCookies(key: string): string {
    return this.cookieService.get(key);
  }

  public deleteAllCookies() {
    // remove both cookies
    this.cookieService.deleteAll('/', environment.cookieDomain);
    this.cookieService.deleteAll('/');
  }

  // endregion


  // region Thumbnail methods

  public getUserThumbnail(userId: string, height: number = 35, width: number = 35): string {
    return environment.apiBlobUrl
      + '/'
      + environment.azureStorageContainer
      + '/general_useravatar/'
      + userId + '.png?mode=crop&maxwidth=' + width
      + '&maxHeight=' + height
      + '&scale=both&Anchor=middlecenter';
  }

  public getChannelThumbnail(contextId: string, height: number = 35, width: number = 35): string {
    return environment.apiBlobUrl
      + '/'
      + environment.azureStorageContainer
      + '/message_context/'
      + contextId + '.png?mode=crop&maxwidth=' + width
      + '&maxHeight=' + height
      + '&scale=both&Anchor=middlecenter';
  }

  // endregion

  public checkUserPermission(userPermissionModel, action): boolean {

    if (userPermissionModel === 'FULL') {
      return true;
    }
    else if (userPermissionModel === 'LIMITED') {
      if (action === 'complete') {
        return true;
      }
      else {
        return false;
      }
    }
    else if (userPermissionModel === 'RESTRICTED') {
      return false;
    } else {
      return false;
    }
  }

  public verifyDate(todate: any): any {
    if (todate != null && todate !== undefined) {
      const compareDate = new Date(1970, 0, 1);
      if (todate.getDay() === compareDate.getDay() &&
        todate.getMonth() === compareDate.getMonth() &&
        todate.getFullYear() === compareDate.getFullYear()) {
        return null;
      }
      else {
        return todate;
      }
    }
    return null;
  }
}
