import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {HubConnection} from '@aspnet/signalr-client';

import {EqmSettings} from '../../shared/models/eqmSettings'
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {CommonService} from '../../shared/services/common.service';
import {environment} from '../../../environments/environment';

@Injectable()
export class SignalRService {

  private hubConnection: HubConnection;
  public eqmSettings: EqmSettings = new EqmSettings();
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private eqmSettingsService: EqmSettingsService,
              private commonService: CommonService) {
    this.eqmSettings = eqmSettingsService.getEqmSettings();
  }

  public startSignalR(area: string, controller: string) {

    if (this.eqmSettings && this.eqmSettings.currentSiteId) {
      this.hubConnection =
        new HubConnection(environment.signalRServer + 'repositoryHub');
      this.hubConnection
        .start()
        .then(() => {
          console.log('Connection started!');

          this.hubConnection
            .invoke('Subscribe', area, controller, 'SiteId', this.eqmSettings.currentSiteId)
            .catch(err => console.error(err));
        })
        .catch(err => console.log('Error while establishing connection :('));
    }
    else {
      console.log('No SiteId Available.');
    }
  }

  /*public signalRCrud<T>(gridView: GridDataResult, isactive: boolean, dto: T) {
      debugger
      this.hubConnection.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
          const _responseText = messageJson;

          let result200: EquipmentTypeDto = new EquipmentTypeDto();
          let data = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
          let resultData200 = this.commonService.toCamel(data);
          result200 = resultData200 ? EquipmentTypeDto.fromJS(resultData200) : new EquipmentTypeDto();

          var dataExist = this.containsObject(result200, gridView.data);

          if (operation.toLowerCase() === "insert" && !dataExist) {
            gridView.data.unshift(result200);
          }

          if (operation.toLowerCase() === "update") {
            gridView.data.forEach((element: EquipmentTypeDto, index) => {
              if (element.equipmentTypeId === result200.equipmentTypeId) {
                if (isactive) {
                  if (result200.active == isactive) {
                    gridView.data[index] = result200;
                  } else {
                    operation = "delete";
                  }
                } else {
                  gridView.data[index] = result200;
                }
              }
            });
          }

          if (operation.toLowerCase() === "delete" && dataExist) {
            var index = null;
            gridView.data.forEach((element: EquipmentTypeDto, i) => {
              if (element.equipmentTypeId === result200.equipmentTypeId) {
                index = i;
              }
            });
            if (index !== null) {
              gridView.data.splice(index, 1);
            }
          }
        });
  }*/

  public containsObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].equipmentTypeId === obj.equipmentTypeId) {
        return true;
      }
    }

    return false;
  }
}

