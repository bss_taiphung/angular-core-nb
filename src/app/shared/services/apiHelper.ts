import {Injectable} from '@angular/core';
import {
  Headers,
  Http,
  RequestOptions,
  Response
} from '@angular/http';
import {Observable} from 'rxjs/Observable';
// Observable class extensions
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';

@Injectable()
export class APIHelper {
  urlPreFix: string;
  headers: Headers;
  options: RequestOptions;

  constructor(private http: Http) {
    const pathArray = location.href.split('/');
    const protocol = pathArray[0];
    const host = pathArray[2];

    this.urlPreFix = protocol + '//' + host + '/api';
    this.headers = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Methods': 'GET, PUT, POST',
      'MacAddress': '4929ef8421ce4a6cbf36c8bc1bf35642',
      'Access-Control-Allow-Headers': 'X-Requested-With,content-type,**Authorization**',
    });
    this.options = new RequestOptions({headers: this.headers});
  }

  getService(url: string): Observable<any> {
    return this.http
      .get(url, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  getAllService(url: string, param: any): Observable<any> {
    this.options = new RequestOptions({headers: this.headers, params: param});
    return this.http
      .get(url, this.options)
      .map(this.extractPlainData)
      .catch(this.handleError);
  }


  createService(url: string, param: any): Observable<any> {
    const body = JSON.stringify(param);
    return this.http
      .post(this.urlPreFix + url, body, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }


  postCreateUserServiceWithPlainResponse(url: string, param: any): Observable<any> {
    const body = JSON.stringify(param);
    this.headers = new Headers({
      'Access-Control-Allow-Headers': 'X-Requested-With,content-type,**Authorization**',
      'Content-Type': 'application/json'
    });
    return this.http
      .post(this.urlPreFix + url, body, this.options)
      .map(this.extractPlainData)
      .catch(this.handleError);
  }


  deleteServiceWithId(url: string, key: string, val: string): Observable<any> {
    return this.http
      .delete(this.urlPreFix + url + '/?' + key + '=' + val, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }


  GetDataFromLocal(url: string): Observable<any> {
    return this.http
      .get(url, this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    const body = res.json();
    return body || {};
  }

  private extractPlainData(res: Response) {
    const body = res;
    return body || {};
  }

  private handleError(error: any) {
    const errMsg = (error.statusText) ? error.statusText :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    const err = {'errorCode': error.status, 'errorMsg': errMsg};
    return Observable.throw(err);
  }
}
