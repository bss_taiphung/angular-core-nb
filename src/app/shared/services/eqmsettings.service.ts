import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {CookieService} from 'ngx-cookie-service';
import {CommonService} from './common.service';
import {EqmSettings, ParamUserLogin, User, UserRole} from '../models/eqmSettings'
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment';

@Injectable()
export class EqmSettingsService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private router: Router,
              private cookieService: CookieService,
              private http: HttpClient,
              private commonService: CommonService) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/equipmentmaintenance/api/UserDetails';
  }

  public getEqmSettings(): EqmSettings {
    const cookieData = this.commonService.getCookies('Eqm');
    if (cookieData) {
      return EqmSettings.fromJS(JSON.parse(cookieData));
    }
    else {
      this.router.navigate(['auth/login']);
      return null;
    }
  }

  public getUser(): User {
    const cookieData = this.commonService.getCookies('user');
    if (cookieData) {
      return User.fromJs(JSON.parse(cookieData));
    } else {
      return null;
    }
  }

  public getUserRoles(): UserRole {
    const cookieData = this.commonService.getCookies('userRole');
    if (cookieData) {
      return UserRole.fromJS(JSON.parse(cookieData));
    } else {
      return null;
    }
  }

  public setEqmSettings(eqm: any) {
    this.commonService.setCookies('Eqm', JSON.stringify(eqm));
  }

  public setUser(): any {
    this.user(res => {
      const user = User.fromJs(this.commonService.toCamel(res));
      this.setUserCookies(user);
    }, err => {
      this.commonService.deleteAllCookies();
    });
  }

  setUserCookies(user: any) {
    this.commonService.setCookies('user', JSON.stringify(user));
  }

  public setUserRole(userRole: any) {
    this.commonService.setCookies('userRole', JSON.stringify(userRole));
  }

  userSettings(): any {
    const macAddress = this.commonService.getMacAddress();
    const url = this.apiBaseUrl + '/GetUserDetails';
    const params = new HttpParams().set('DeviceMACAddress', macAddress);
    const options = {
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      const eqm = EqmSettings.fromJS(this.commonService.toCamel(response));
      this.setEqmSettings(response);
      return Observable.of(eqm);
    });
  }

  userRoles(): any {
    const macAddress = this.commonService.getMacAddress();
    const url = environment.bllApiBaseAddress +
      '/general/api/UserRole/GetByUser';
    const params = new HttpParams().set('DeviceMACAddress', macAddress);
    const options = {
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      const userrole = UserRole.fromJS(this.commonService.toCamel(response));
      this.setUserRole(response);
      return Observable.of(userrole);
    });
  }

  user(successCallback: any, errorCallback: any): any {

    const macAddress = this.commonService.getMacAddress();
    const url = environment.bllApiBaseAddress + '/general/api/account/GetLoggedInDeviceFromCache';

    const input: ParamUserLogin = new ParamUserLogin();
    input.deviceMACAddress = macAddress;

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  update(showOnlyMyAssignments: boolean, successCallback: any, errorCallback: any): any {
    const input = '';

    this.http.post(this.apiBaseUrl, input)
      .subscribe(
        res => {
          successCallback(res);
        },
        err => {
          console.log('Error occured');
          errorCallback(err);
        });
  }
}
