export class Helper {
  /**
   * Scroll to first error
   */
  public static scrollToFirstError() {
    setTimeout(() => {
      let ele = document.querySelector('.has-error');
      if (!ele) {
        ele = document.querySelector('.ng-invalid:not(form)');
      }
      if (ele) {
        const eleRect = ele.getBoundingClientRect();
        const main = document.getElementById('main');
        if (main) {
          const bodyRect = main.getBoundingClientRect();
          const top = this.findPosTop(ele) - bodyRect.top - 35;
          document.getElementById('main').scrollTo(0, top);
        } else {
          const bodyRect = document.body.getBoundingClientRect();
          const top = eleRect.top - bodyRect.top - 35;
          window.scrollTo(0, top);
        }
      }
    });
  }

  /**
   * Find position top
   */
  public static findPosTop(obj) {
    let ele = obj;
    let top = 0;
    do {
      top += ele.offsetTop;
      ele = ele.offsetParent;
    } while (ele);
    return top;
  }

  /***
   * Round number
   * @param {number} value
   * @param {number} decimals
   * @returns {number}
   */
  public static round(value: number, decimals: number): number {
    return Number(Math.round(parseFloat(value + 'e' + decimals)) + 'e-' + decimals);
  }
}
