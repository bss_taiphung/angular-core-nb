export class UrlHelper {
  /**
   * Url encoded string
   *
   * @param data
   * @returns {string}
   */
  public static toUrlEncodedString(data: any) {
    let body = '';
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        if (body.length) {
          body += '&';
        }
        body += key + '=';
        body += encodeURIComponent(data[key]);
      }
    }
    return body;
  }
}
