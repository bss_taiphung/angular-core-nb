import {
  AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild,
  ChangeDetectionStrategy
} from '@angular/core';

import {CommonService} from '../services/common.service';
import 'rxjs/add/observable/fromEvent';
import {ToasterHelperService} from '../services/toasterHelper.service'

declare var kendo: any;

@Component({
  selector: 'kendo-custom-TextEditor',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './customtexteditor.component.html'
})

export class CustomTextEditorComponent implements AfterViewInit {
  private projectUserList: string[] = [];

  @ViewChild('chatckeditor') ckeditor: any;

  constructor(private elementRef: ElementRef,
              private commonService: CommonService, private toasterService: ToasterHelperService) {
    this.commonService.getAll('General', 'User').subscribe(eventResult => {
      this.projectUserList = [];
      eventResult.results.forEach((user, index) => {
        this.projectUserList.push(user.text);
      });
    });

  }

  ngAfterViewInit() {
    this.ckeditor.instance.on('key', (event) => {
      if (event.data.keyCode === 13) {
        this.sendMessage();
      }
    });
  }

  ckeditorContent = '';
  isCloseHide = false;
  buttonName = 'Send';

  @Input()
  public set commentButtonName(data: any) {
    this.buttonName = data;
  };

  @Input()
  public set inputModel(data: any) {
    if (data && data != undefined) {
      this.ckeditorContent = data;
      this.isCloseHide = true;
    }
    else {
      this.isCloseHide = false;
    }
  }


  @Output() sendMessageToChannel: EventEmitter<any> = new EventEmitter();

  sendMessage(event: any = '') {
    if (this.ckeditorContent !== '') {
      this.isCloseHide = false;
      this.sendMessageToChannel.emit(this.ckeditorContent);
      this.ckeditorContent = '';
      this.ckeditor.instance.setData('');
    } else {
      this.ckeditor.instance.setData('');
      this.toasterService.error('', 'Please enter message to send.');
    }
  }

  closeMessage() {
    this.isCloseHide = false;
    this.sendMessageToChannel.emit('');
    this.ckeditorContent = '';

  }

  items: string[] = ['Noah', 'Liam', 'Mason', 'Jacob'];
}
