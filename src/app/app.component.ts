import {
  AfterViewInit,
  Component,
  HostListener
} from '@angular/core';
import {ToasterConfig} from 'angular2-toaster';

import {Globals} from './shared/globals/globals';
import {ToasterHelperService} from './shared/services/toasterHelper.service';
import {CommonService} from './shared/services/common.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements AfterViewInit {

  title = 'Manufacturing System Suite';
  toasterconfig: ToasterConfig;

  changeNaveMenu: any = this.commonService.getCookies('module');

  constructor(public globals: Globals,
              private toaster: ToasterHelperService,
              private commonService: CommonService) {
    this.toasterconfig = toaster.toasterconfig;
  }

  ngAfterViewInit() {
    document.getElementById('contentWrapper').style.height = (window.innerHeight - 64) + 'px';
  }

  public hideRightMenu() {
    if (window.screen.width < 767) {
      this.globals.toogleMenu = false;
    }

    this.globals.userMenu = false;
    this.globals.userProfileMenu = false;
  }

  changeModules(data) {
    this.changeNaveMenu = data;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (document.getElementById('contentWrapper') != null) {
      document.getElementById('contentWrapper').style.height = (window.innerHeight - 64) + 'px';
    }
  }
}
