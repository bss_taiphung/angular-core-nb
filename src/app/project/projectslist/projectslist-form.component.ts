import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild
} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';
import {
  CreateProjectDto,
  ProjectDto
} from './service/projectslist.model';
import {CommonService} from '../../shared/services/common.service';
import {SelectListDto} from '../../shared/models/selectListDto'
import {ProjectSettingsService} from '../shared/services/projectsettings.service'
import {ProjectListService} from './service/projectslist.service';
import {ProjectTagDTO} from '../projecttaskdetail/service/projecttag.model';
import {TagMasterService} from '../tagmaster/service/tagmaster.service';
import {ToasterHelperService} from 'app/shared/services/toasterHelper.service';
import {User} from 'app/shared/models/eqmSettings';
import {EqmSettingsService} from 'app/shared/services/eqmsettings.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'kendo-grid-project-form',
  styleUrls: ['./css/projectslist.component.css'],
  templateUrl: './html/projectslist-form.component.html'
})
export class ProjectsListFormComponent {
  public active = false;
  public setSelectedTags: any = [];
  public setSelectedTeam: any = [];
  public setSelectedAdmin: any = [];
  user: User = new User();
  public editForm: FormGroup = new FormGroup({
    'projectId': new FormControl(),
    'projectGroupId': new FormControl(''),
    'projectName': new FormControl('', Validators.required),
    'permissionModel': new FormControl('', Validators.required),
    'projectDescription': new FormControl(''),
    'projectStatus': new FormControl('', Validators.required),
    'startDate': new FormControl(null),
    'dueDate': new FormControl(null),
    'actualCompletionDate': new FormControl(null),
    'archivedDate': new FormControl(null),
    'isPublicProject': new FormControl(false),
    'useTaskPoints': new FormControl(false),
    'isArchived': new FormControl(false),
    'active': new FormControl(false),
    'projectTeams': new FormControl(),
    'projectAdminTeams': new FormControl(),
    'tagDisplayList': new FormControl(''),
    'savingsFirstYear': new FormControl(''),
    'savingsSecondYear': new FormControl(''),
    'savingsAnnual': new FormControl('')
  });

  @ViewChild('tags') tags;
  @ViewChild('teamsmembers') teamsmembers;
  @ViewChild('adminmembers') adminmembers;

  public onClosetags(event: any) {
    event.preventDefault();
    //Close the list if the component is no longer focused
    setTimeout(() => {
      if (!this.tags.wrapper.nativeElement.contains(document.activeElement)) {
        this.tags.toggle(false);
      }
    });
  }

  public onCloseteamsmembers(event: any) {
    event.preventDefault();
    //Close the list if the component is no longer focused
    setTimeout(() => {
      if (!this.teamsmembers.wrapper.nativeElement.contains(document.activeElement)) {
        this.teamsmembers.toggle(false);
      }
    });
  }

  public onCloseadminmembers(event: any) {
    event.preventDefault();
    //Close the list if the component is no longer focused
    setTimeout(() => {
      if (!this.adminmembers.wrapper.nativeElement.contains(document.activeElement)) {
        this.adminmembers.toggle(false);
      }
    });
  }

  constructor(private commonService: CommonService,
              private projectSettingsService: ProjectSettingsService,
              private projectListService: ProjectListService,
              private tagMasterService: TagMasterService,
              private toasterService: ToasterHelperService,
              private eqmSettingsService: EqmSettingsService,) {
    this.user = this.eqmSettingsService.getUser();
    this.azureStorageContainer = environment.azureStorageContainer;
  }


  public permissionModelList: any = [
    {text: 'Project Members have FULL access.', value: 'FULL', isSelected: false},
    {text: 'Project Members have LIMITED access.', value: 'LIMITED', isSelected: false},
    {text: 'Project Members have RESTRICTED access.', value: 'RESTRICTED', isSelected: false}];

  public projectStatusList: any = [
    {text: 'No Status', value: 0, selected: true},
    {text: 'Active', value: 1, isSelected: false},
    {text: 'On Hold', value: 2, isSelected: false},
    {text: 'Planned', value: 3, isSelected: false},
    {text: 'Cancelled', value: 4, isSelected: false},
    {text: 'Completed', value: 5, isSelected: false}];

  public projectGroupIdList: SelectListDto[];
  public filterprojectGroupIdList: SelectListDto[];

  public workspaceIdList: SelectListDto[];
  public filterworkspaceIdList: SelectListDto[];
  public workspaceTagMasterList: ProjectTagDTO[];
  public workspaceTagMasterListVM: ProjectTagDTO[];
  public newProjectDto: ProjectDto;

  public workspaceTeamList: SelectListDto[];
  private isArchived: boolean;
  private changePermissionModelText: any;
  azureStorageContainer: string;
  @Input() public isNew = false;
  @Input() public isActiveHide = false;

  @Input()
  public set model(project: ProjectDto) {
    if (project != undefined) {

      this.changePermissionModelTextHandler(project.permissionModel);
      this.editForm.reset(project);
      if (!this.isNew) {

      } else {
        this.editForm.controls['projectId'].setValue(0);
        this.editForm.controls['projectStatus'].setValue(0);
        this.editForm.controls['permissionModel'].setValue('FULL');
        this.changePermissionModelTextHandler('FULL');
      }

      this.setSelectedTags = null;
      this.setSelectedTeam = null;
      this.setSelectedAdmin = null;
      this.active = project !== undefined;
      this.isArchived = project === undefined ? false : project.isArchived;


      this.commonService
        .getAllWithId(this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId, 'Projects', 'ProjectGroup')
        .subscribe(eventResult => {
          if (eventResult.results.length > 0) {
            eventResult.results.unshift(new SelectListDto({text: '', value: '', isSelected: false}));
          }
          this.filterprojectGroupIdList = this.projectGroupIdList = eventResult.results;
        });


      this.tagMasterService
        .getAlltagsByWorkspanceId(this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId).subscribe(
        eventResult => {
          this.workspaceTagMasterList = ProjectTagDTO.fromJSArrayList(eventResult);
          this.workspaceTagMasterListVM = this.workspaceTagMasterList;
        });
      this.commonService
        .getAllUserWithWorkspaceId(this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId,
          'Projects',
          'WorkspaceTeam').subscribe(eventResult => {
        if (eventResult.results != null && eventResult.results.length > 0) {
          eventResult.results.forEach(itm => {
            if (itm.userAvatar == null) {
              itm.userAvatar = this.commonService.getUserThumbnail(itm.value);
            }
          });
        }
        this.workspaceTeamList = eventResult.results;
      });

      if (project.projectId > 0) {
        this.setSelectedTags = project.tagDisplayList;
        project.projectTeams.forEach(itm => {
          itm.userAvatar = this.commonService.getUserThumbnail(itm.value);
        });
        project.projectAdminTeams.forEach(itm => {
          itm.userAvatar = this.commonService.getUserThumbnail(itm.value);
        });
        this.setSelectedTeam = project.projectTeams;
        this.setSelectedAdmin = project.projectAdminTeams;
      }
    }
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<ProjectDto> = new EventEmitter<ProjectDto>();

  public onSave(e): void {
    e.preventDefault();
    this.save.emit(this.editForm.value);
    this.active = false;
  }

  public permissionModelChangeEvent(value) {
    this.changePermissionModelTextHandler(value);
  }

  public changePermissionModelTextHandler(permissionName: any) {

    if (permissionName === 'FULL') {
      this.changePermissionModelText = '<b>Full Access : </b><i style=\'color:red;\'>All Project members will be able to view and edit all tasks in the project.</i>';
    } else if (permissionName === 'LIMITED') {
      this.changePermissionModelText = '<b>LIMITED Access : </b><i style=\'color:red;\'>Project members cannot Add/Edit Tasks, Title, Description, Location, Start/Due Date, Assigned to.</i>';
    } else if (permissionName === 'RESTRICTED') {
      this.changePermissionModelText = '<b>RESTRICTED Access : </b><i style=\'color:red;\'>Project members can only view tasks that they are assignees or followers of. </i>';
    } else {
      this.changePermissionModelText = '';
    }
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();

  }

  private closeForm(): void {
    this.active = false;
    this.cancel.emit();
  }

  private projectGroupIdListHandleFilter(value) {
    this.filterprojectGroupIdList = this.projectGroupIdList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private workspaceIdListHandleFilter(value) {
    this.filterworkspaceIdList = this.workspaceIdList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  public tagHandleFilter(filter: any): void {
    this.workspaceTagMasterList = this.workspaceTagMasterListVM.filter((s) => s['tagName'].toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  public teamMemberHandleFilter(filter: any): void {
    //this.workspaceTeamList = this.workspaceTagMasterListVM.filter((s) => s["text"].toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  public adminMemberHandleFilter(filter: any): void {
    //this.workspaceTeamList = this.workspaceTagMasterListVM.filter((s) => s["text"].toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  saveLoading: boolean = false;

  public saveHandler() {
    this.saveLoading = true;
    debugger;
    const inputprojectdata: CreateProjectDto = this.editForm.value;
    if (inputprojectdata.projectId === 0) {
      inputprojectdata.active = true;
      inputprojectdata.workspaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;
      this.projectListService.create(inputprojectdata,
        response => {
          this.toasterService.success('', 'Project Saved Successfully');
          this.cancel.emit();
          this.active = false;
          this.saveLoading = false;
        },
        error => {
          this.toasterService.errorMessage(error);
          this.saveLoading = false;
        }
      );
    }
    else {
      inputprojectdata.workspaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;
      if (inputprojectdata.isArchived) {
        inputprojectdata.archivedByUserId = this.user.userId;
      }
      this.projectListService.update(inputprojectdata,
        response => {
          this.toasterService.success('', 'Project Updated Successfully');
          this.cancel.emit(inputprojectdata);
          this.active = false;
          this.saveLoading = false;
        },
        error => {
          this.toasterService.errorMessage(error);
          this.saveLoading = false;
        }
      );
    }
  }

  public adminMemberChange(event) {
    let selectedAdmin = event.map(e => e.value);
    for (let userId of selectedAdmin) {
      if (this.setSelectedTeam != null) {
        this.setSelectedTeam = this.setSelectedTeam.filter(item => item.value !== userId);
        this.editForm.controls['projectTeams'].setValue(this.setSelectedTeam);
      }
    }
  }

  public teamMemberChange(event) {
    let selectedUser = event.map(e => e.value);
    for (let userId of selectedUser) {
      if (this.setSelectedAdmin != null) {
        this.setSelectedAdmin = this.setSelectedAdmin.filter(item => item.value !== userId);
        this.editForm.controls['projectAdminTeams'].setValue(this.setSelectedAdmin);
      }
    }
  }

}
