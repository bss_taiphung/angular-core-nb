import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {ProjectTagDTO} from '../../projecttaskdetail/service/projecttag.model';


// region IProjectDto

export interface IProjectDto {
  projectId: number;
  projectGroupId: number;
  projectGroupName: string;
  workspaceId: number;
  projectName: string;
  permissionModel: string;
  projectDescription: string;
  projectStatus: number;
  startDate: Date;
  dueDate: Date;
  actualCompletionDate: Date;
  isPublicProject: boolean;
  useTaskPoints: boolean;
  isArchived: boolean;
  archivedDate: Date;
  archivedByUserId: number;
  numActivePoints: number;
  numCompletedPoints: number;
  numMinutesEstimated: number;
  numMinutesSpent: number;
  percentComplete: number;
  numTasksActive: number;
  numTasksCompleted: number;
  numTasksWithTimeSpent: number;
  numTasksWithPoints: number;
  useIntegrationLogic: boolean;
  savingsFirstYear: number;
  savingsSecondYear: number;
  savingsAnnual: number;
  restrictDelete: boolean;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
  userStarredProjectId: number;
  tagSearchList: string;
  tagDisplayList: any;
  projectAdminTeams: any;
  projectTeams: any;
  workspaceTagMasterIds: any;
}

export class ProjectDto implements IProjectDto {

  projectId: number;
  projectGroupId: number;
  projectGroupName: string;
  workspaceId: number;
  projectName: string;
  permissionModel: string;
  projectDescription: string;
  projectStatus: number;
  startDate: Date;
  dueDate: Date;
  actualCompletionDate: Date;
  isPublicProject: boolean;
  useTaskPoints: boolean;
  isArchived: boolean;
  archivedDate: Date;
  archivedByUserId: number;
  numActivePoints: number;
  numCompletedPoints: number;
  numMinutesEstimated: number;
  numMinutesSpent: number;
  percentComplete: number;
  numTasksActive: number;
  numTasksCompleted: number;
  numTasksWithTimeSpent: number;
  numTasksWithPoints: number;
  useIntegrationLogic: boolean;
  savingsFirstYear: number;
  savingsSecondYear: number;
  savingsAnnual: number;
  restrictDelete: boolean;

  createdByName: string;
  createdDate: string;
  createdDateTime: string;
  modifiedByName: string;
  modifiedDateTime: string;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
  userStarredProjectId: number;
  tagSearchList: string;
  tagDisplayList: any;
  projectAdminTeams: any;
  projectTeams: any;
  workspaceTagMasterIds: any;
  workspaceTeamIds: any;

  static fromJS(data: any): ProjectDto {
    const result = new ProjectDto();
    result.init(data);
    return result;
  }

  constructor(data?: IProjectDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.projectId = data['projectId'];
      this.projectGroupId = data['projectGroupId'];
      this.projectGroupName = data['projectGroupName'];
      this.workspaceId = data['workspaceId'];
      this.projectName = data['projectName'];
      this.permissionModel = data['permissionModel'];
      this.projectDescription = data['projectDescription'];
      this.projectStatus = data['projectStatus'];
      this.startDate = data['startDate'] != null ? new Date(data['startDate']) : null;
      this.dueDate = data['dueDate'] != null ? new Date(data['dueDate']) : null;
      this.actualCompletionDate = data['actualCompletionDate'] != null ? new Date(data['actualCompletionDate']) : null;
      this.isPublicProject = data['isPublicProject'];
      this.useTaskPoints = data['useTaskPoints'];
      this.isArchived = data['isArchived'];
      this.archivedDate = data['archivedDate'] != null ? new Date(data['archivedDate']) : null;
      this.archivedByUserId = data['archivedByUserId'];
      this.numActivePoints = data['numActivePoints'];
      this.numCompletedPoints = data['numCompletedPoints'];
      this.numMinutesEstimated = data['numMinutesEstimated'];
      this.numMinutesSpent = data['numMinutesSpent'];
      this.percentComplete = data['percentComplete'];
      this.numTasksActive = data['numTasksActive'];
      this.numTasksCompleted = data['numTasksCompleted'];
      this.numTasksWithTimeSpent = data['numTasksWithTimeSpent'];
      this.numTasksWithPoints = data['numTasksWithPoints'];
      this.savingsFirstYear = data['savingsFirstYear'];
      this.savingsSecondYear = data['savingsSecondYear'];
      this.savingsAnnual = data['savingsAnnual'];
      this.restrictDelete = data['restrictDelete'];
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.active = data['active'];
      this.userStarredProjectId = data['userStarredProjectId'];
      this.tagSearchList = data['tagSearchList'];
      this.tagDisplayList = ProjectTagDTO.fromJSArrayList(JSON.parse(data['tagDisplayList']));
      this.workspaceTagMasterIds = data['workspaceTagMasterIds'];
      this.projectAdminTeams = ProjectTeamDto.fromJS(data['projectTeams']).filter(function (obj) {
        return obj.isAdmin === 1
      });
      this.projectTeams = ProjectTeamDto.fromJS(data['projectTeams']).filter(function (obj) {
        return obj.isAdmin === 0
      });
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['projectId'] = this.projectId;
    data['projectGroupId'] = this.projectGroupId;
    data['projectGroupName'] = this.projectGroupName;
    data['workspaceId'] = this.workspaceId;
    data['projectName'] = this.projectName;
    data['permissionModel'] = this.permissionModel;
    data['projectDescription'] = this.projectDescription;
    data['projectStatus'] = this.projectStatus;
    data['startDate'] = this.startDate;
    data['dueDate'] = this.dueDate;
    data['actualCompletionDate'] = this.actualCompletionDate;
    data['isPublicProject'] = this.isPublicProject;
    data['useTaskPoints'] = this.useTaskPoints;
    data['isArchived'] = this.isArchived;
    data['archivedDate'] = this.archivedDate;
    data['archivedByUserId'] = this.archivedByUserId;
    data['numActivePoints'] = this.numActivePoints;
    data['numCompletedPoints'] = this.numCompletedPoints;
    data['numMinutesEstimated'] = this.numMinutesEstimated;
    data['numMinutesSpent'] = this.numMinutesSpent;
    data['percentComplete'] = this.percentComplete;
    data['numTasksActive'] = this.numTasksActive;
    data['numTasksCompleted'] = this.numTasksCompleted;
    data['numTasksWithTimeSpent'] = this.numTasksWithTimeSpent;
    data['numTasksWithPoints'] = this.numTasksWithPoints;
    data['savingsFirstYear'] = this.savingsFirstYear;
    data['savingsSecondYear'] = this.savingsSecondYear;
    data['savingsAnnual'] = this.savingsAnnual;
    data['restrictDelete'] = this.restrictDelete;
    data['createdByName'] = this.createdByName;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;
    data['userStarredProjectId'] = this.userStarredProjectId;
    data['tagSearchList'] = this.tagSearchList;
    data['tagDisplayList'] = this.tagDisplayList;
    data['workspaceTagMasterIds'] = this.workspaceTagMasterIds;
    data['workspaceTeamIds'] = this.workspaceTeamIds;
    data['projectAdminTeams'] = this.projectAdminTeams;
    data['projectTeams'] = this.projectTeams;
    return data;
  }
}

// endregion

// region TagDisplayList

export interface ITagDisplayList {
  tagName: any;
  tagColor: any;
  projectTagId: number;
}

export class TagDisplayList implements ITagDisplayList {
  tagName: any;
  tagColor: any;
  projectTagId: number;

  static fromJS(data: any): TagDisplayList[] {
    const results = [];
    if (data) {
      for (const item of data) {
        const obj = new TagDisplayList();
        obj.init(item);
        results.push(obj);
      }
      return results;
    }
  }

  constructor(data?: TagDisplayList) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.tagName = data['TagName'];
      this.tagColor = data['TagColor'];
      this.projectTagId = data['projectTagId'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['projectTagId'] = this.projectTagId;
    data['TagName'] = this.tagName;
    data['TagColor'] = this.tagColor;
    return data;
  }

}

// endregion

// region ICreateProjectDto
export interface ICreateProjectDto {
  projectId: number;
  projectGroupId: number;
  projectGroupName: string;
  workspaceId: number;
  projectName: string;
  permissionModel: string;
  projectDescription: string;
  projectStatus: number;
  startDate: Date;
  dueDate: Date;
  actualCompletionDate: Date;
  isPublicProject: boolean;
  useTaskPoints: boolean;
  isArchived: boolean;
  archivedDate: Date;
  active: boolean;
  workspaceTagMasterIds: any;
  workspaceTeamIds: any;
  archivedByUserId: number;
}

export class CreateProjectDto implements ICreateProjectDto {
  projectId = 0;
  archivedByUserId = 0;
  projectGroupId: number;
  projectGroupName: string;
  workspaceId: number;
  projectName: string;
  permissionModel: string;
  projectDescription: string;
  projectStatus: number;
  startDate: Date;
  dueDate: Date;
  actualCompletionDate: Date;
  isPublicProject: boolean;
  useTaskPoints: boolean;
  isArchived: boolean;
  archivedDate: Date;
  active: boolean;
  workspaceTagMasterIds: any;
  workspaceTeamIds: any;

  static fromJS(data: any): ProjectDto {
    const result = new ProjectDto();
    result.init(data);
    return result;
  }

  constructor(data?: IProjectDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.projectId = 0;
      this.projectGroupId = data['projectGroupId'];
      this.projectGroupName = data['projectGroupName'];
      this.workspaceId = data['workspaceId'];
      this.projectName = data['projectName'];
      this.permissionModel = data['permissionModel'];
      this.projectDescription = data['projectDescription'];
      this.projectStatus = data['projectStatus'];
      this.startDate = data['startDate'] != null ? new Date(data['startDate']) : null;
      this.dueDate = data['dueDate'] != null ? new Date(data['dueDate']) : null;
      this.actualCompletionDate = data['actualCompletionDate'] != null ? new Date(data['actualCompletionDate']) : null;
      this.isPublicProject = data['isPublicProject'];
      this.useTaskPoints = data['useTaskPoints'];
      this.isArchived = data['isArchived'];
      this.archivedDate = data['archivedDate'] != null ? new Date(data['archivedDate']) : null;
      this.active = (data['active'] == null) ? false : data['active'];
      this.workspaceTagMasterIds = data['workspaceTagMasterIds'];
      this.workspaceTeamIds = data['workspaceTeamIds'];
      this.archivedByUserId = data['archivedByUserId'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['projectId'] = 0;
    data['projectId'] = this.projectId;
    data['projectGroupId'] = this.projectGroupId;
    data['projectGroupName'] = this.projectGroupName;
    data['workspaceId'] = this.workspaceId;
    data['projectName'] = this.projectName;
    data['permissionModel'] = this.permissionModel;
    data['projectDescription'] = this.projectDescription;
    data['projectStatus'] = this.projectStatus;
    data['startDate'] = this.startDate;
    data['dueDate'] = this.dueDate;
    data['actualCompletionDate'] = this.actualCompletionDate;
    data['isPublicProject'] = this.isPublicProject;
    data['useTaskPoints'] = this.useTaskPoints;
    data['isArchived'] = this.isArchived;
    data['archivedDate'] = this.archivedDate;
    data['active'] = (this.active == null) ? false : this.active;
    data['workspaceTagMasterIds'] = this.workspaceTagMasterIds;
    data['workspaceTeamIds'] = this.workspaceTeamIds;
    return data;
  }
}

// endregion

// region IPagedResultDtoOfProjectDto

export interface IPagedResultDtoOfProjectDto {
  results: ProjectDto[];
  count: number;
}

export class PagedResultDtoOfProjectDto implements IPagedResultDtoOfProjectDto {
  results: ProjectDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfProjectDto {
    const result = new PagedResultDtoOfProjectDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfProjectDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(ProjectDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion

// region IProjectTaskListDto

export interface IProjectTaskListDto {
  projectTaskListId: number;
  projectId: number;
  projectTaskListName: string;
  projectSortOrder: number;
  numTasksWithTimeSpent: number;
  numMinutesSpent: number;
  numTasksWithPoints: number;
  numTasksCompleted: number;
  numActivePoints: number;
  numCompletedPoints: number;
  numMinutesEstimated: number;
  percentComplete: number;
}

export class ProjectTaskListDto implements IProjectTaskListDto {

  projectTaskListId: number;
  projectId: number;
  projectTaskListName: string;
  projectSortOrder: number;
  numTasksWithTimeSpent: number;
  numMinutesSpent: number;
  numTasksWithPoints: number;
  numTasksCompleted: number;
  numActivePoints: number;
  numCompletedPoints: number;
  numMinutesEstimated: number;
  percentComplete: number;

  static fromJS(data: any): ProjectTaskListDto {
    const result = new ProjectTaskListDto();
    result.init(data);
    return result;
  }

  constructor(data?: IProjectDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.projectId = data['projectId'];
      this.projectTaskListId = data['projectTaskListId'];
      this.projectTaskListName = data['projectTaskListName'];
      this.projectSortOrder = data['projectSortOrder'];
      this.numTasksWithTimeSpent = data['numTasksWithTimeSpent'];
      this.numMinutesSpent = data['numMinutesSpent'];
      this.numTasksWithPoints = data['numTasksWithPoints'];
      this.numTasksCompleted = data['numTasksCompleted'];
      this.numActivePoints = data['numActivePoints'];
      this.numCompletedPoints = data['numCompletedPoints'];
      this.numMinutesEstimated = data['numMinutesEstimated'];
      this.percentComplete = data['percentComplete'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['projectId'] = this.projectId;
    data['projectTaskListId'] = this.projectTaskListId;
    data['projectTaskListName'] = this.projectTaskListName;
    data['projectSortOrder'] = this.projectSortOrder;
    data['numTasksWithTimeSpent'] = this.numTasksWithTimeSpent;
    data['numMinutesSpent'] = this.numMinutesSpent;
    data['numTasksWithPoints'] = this.numTasksWithPoints;
    data['numTasksCompleted'] = this.numTasksCompleted;
    data['numActivePoints'] = this.numActivePoints;
    data['numCompletedPoints'] = this.numCompletedPoints;
    data['numMinutesEstimated'] = this.numMinutesEstimated;
    data['percentComplete'] = this.percentComplete;

    return data;
  }
}

// endregion

// region ICreateProjectTaskListDto

export interface ICreateProjectTaskListDto {
  projectTaskListName: string;
  active: boolean;
}

export class CreateProjectTaskListDto implements ICreateProjectTaskListDto {
  projectTaskListId = 0;
  projectTaskListName: string;
  active = true;

  static fromJS(data: any): ProjectTaskListDto {
    const result = new ProjectTaskListDto();
    result.init(data);
    return result;
  }

  constructor(data?: IProjectTaskListDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.projectTaskListId = 0;
      this.projectTaskListName = data['projectTaskListName'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['projectTaskListId'] = 0;
    data['projectTaskListName'] = this.projectTaskListName;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

//endregion


// region IPagedResultDtoOfProjectTaskListDto

export interface IPagedResultDtoOfProjectTaskListDto {
  results: ProjectTaskListDto[];
  count: number;
}

export class PagedResultDtoOfProjectTaskListDto implements IPagedResultDtoOfProjectTaskListDto {
  results: ProjectTaskListDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfProjectTaskListDto {
    const result = new PagedResultDtoOfProjectTaskListDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfProjectTaskListDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(ProjectTaskListDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion


// region IWorkspaceTeamDto

export interface IWorkspaceTeamDto {
  wsTeamMemberId: number;
  workspaceId: number;
  workspaceName: string;
  userId: number;
  userDisplayName: string;
  isAdmin: number;
  createdByName: string;
  modifiedByName: string;

}

export class WorkspaceTeamDto implements IWorkspaceTeamDto {
  wsTeamMemberId: number;
  workspaceId: number;
  workspaceName: string;
  userId: number;
  userDisplayName: string;
  isAdmin: number;
  createdByName: string;
  modifiedByName: string;

  static fromJS(data: any): WorkspaceTeamDto {
    const result = new WorkspaceTeamDto();

    result.init(data);
    return result;
  }

  constructor(data?: IWorkspaceTeamDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.wsTeamMemberId = data['wsTeamMemberId'];
      this.workspaceId = data['workspaceId'];
      this.workspaceName = data['workspaceName'];
      this.userId = data['userId'];
      this.userDisplayName = data['userDisplayName'];
      this.isAdmin = data['isAdmin'];
      this.createdByName = data['createdByName'];
      this.modifiedByName = data['modifiedByName'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['wsTeamMemberId'] = this.wsTeamMemberId;
    data['workspaceId'] = this.workspaceId;
    data['workspaceName'] = this.workspaceName;
    data['userId'] = this.userId;
    data['userDisplayName'] = this.userDisplayName;
    data['isAdmin'] = this.isAdmin;
    data['createdByName'] = this.createdByName;
    data['modifiedByName'] = this.modifiedByName;
    return data;
  }
}

// endregion


// region IProjectTeamDto
export interface IProjectTeamDto {
  projectTeamId: number;
  projectId: number;
  userId: number;
  userName: string;
  isAdmin: number;
}

export class ProjectTeamDto implements IProjectTeamDto {
  projectTeamId: number;
  projectId: number;
  userId: number;
  userName: string;
  isAdmin: number;
  userAvatar: string;
  text: string;
  value: number;

  static fromJS(data: any): ProjectTeamDto[] {
    const results = [];
    if (data !== null && data !== undefined) {
      data = JSON.parse(data);
      for (const item of data) {
        const obj = new ProjectTeamDto();
        obj.init(item);
        results.push(obj);
      }
    }
    return results;
  }

  constructor(data?: IProjectTeamDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      data = this.toCamel(data);
      this.projectTeamId = data['projectTeamId'];
      this.projectId = data['projectId'];
      this.text = this.userName = data['userName'];
      this.value = this.userId = data['userId'];
      this.isAdmin = data['isAdmin'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['projectTeamId'] = this.projectTeamId;
    data['projectId'] = this.projectId;
    data['text'] = data['userName'] = this.userName;
    data['value'] = data['userId'] = this.userId;
    data['isAdmin'] = this.isAdmin;
    return data;
  }

  toCamel(o) {
    let newO, origKey, newKey, value
    if (o instanceof Array) {
      newO = []
      for (origKey in o) {
        value = o[origKey]
        if (typeof value === 'object') {
          value = this.toCamel(value)
        }
        newO.push(value)
      }
    } else {
      newO = {}
      for (origKey in o) {
        if (o.hasOwnProperty(origKey)) {
          newKey = (origKey.charAt(0).toLowerCase() + origKey.slice(1) || origKey).toString()
          value = o[origKey]
          if (value instanceof Array || (value !== null && value.constructor === Object)) {
            value = this.toCamel(value)
          }
          newO[newKey] = value
        }
      }
    }
    return newO
  }
}

// endregion

