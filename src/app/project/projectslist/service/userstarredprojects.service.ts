import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {UserStarredProjectDto} from './userstarredprojects.model';

import {CommonService} from '../../../shared/services/common.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class UserStarredProjectsService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService,
              private http: HttpClient) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/Projects/api/UserStarredProject/';
  }

  create(input: UserStarredProjectDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'InsertAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }


}

