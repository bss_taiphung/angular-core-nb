import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

// region IUserStarredProjectDto

export interface IUserStarredProjectDto {
  userStarredProjectId: number;
  projectId: number;
}

export class UserStarredProjectDto implements IUserStarredProjectDto {
  userStarredProjectId: number;
  projectId: number;

  static fromJS(data: any): UserStarredProjectDto {
    const result = new UserStarredProjectDto();
    result.init(data);
    return result;
  }

  constructor(data?: IUserStarredProjectDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.userStarredProjectId = data['userStarredProjectId'];
      this.projectId = data['projectId'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['userStarredProjectId'] = this.userStarredProjectId;
    data['projectId'] = this.projectId;

    return data;
  }
}

// endregion

