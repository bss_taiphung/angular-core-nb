import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {
  HttpClient,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {
  CreateProjectDto,
  PagedResultDtoOfProjectDto
} from './projectslist.model';
import {CommonService} from '../../../shared/services/common.service';
import {ResultDtoOfSelectListDto} from '../../../shared/models/selectListDto';
import {environment} from '../../../../environments/environment';

export interface setSelectedTags {
  value: number
}

@Injectable()
export class ProjectListService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService, private http: HttpClient) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/Projects/api/Project/';
  }

  create(input: CreateProjectDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'InsertAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  update(input: CreateProjectDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'UpdateAsync';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  delete(id: any, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'DeleteAsync';

    const params = new HttpParams().set('id', id);

    const options = {
      params: params
    };

    this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
  }

  getProjectTagsByProjectId(projectId: number): Observable<ResultDtoOfSelectListDto> {
    const url = environment.bllApiBaseAddress + '/Projects/api/ProjectTags/GetTagsByProjectId';
    const params = new HttpParams().set('projectId', projectId.toString());

    const options = {
      params: params
    };

    return this.http.request('get', url, options).flatMap((response) => {
      const result200 = ResultDtoOfSelectListDto.fromJS(response);
      return Observable.of(result200);
    });
  }

  getProjectTeamByProjectId(projectId: number): Observable<any> {
    const url = environment.bllApiBaseAddress + '/Projects/api/ProjectTeam/GetTeamByProjectId';
    const params = new HttpParams().set('projectId', projectId.toString());

    const options = {
      params: params
    };

    return this.http.request('get', url, options).flatMap((response) => {
      return Observable.of(response);
    });
  }

  getAll(take: number,
         skip: number,
         sort: any,
         isactive: boolean,
         filter: string,
         workspaceId: number,
         searchByTag: any[]): Observable<PagedResultDtoOfProjectDto> {
    const url = this.apiBaseUrl + 'GetPagedQuery';

    // Order by Clause
    let orderBy = '';
    if (sort !== undefined && sort.length > 0) {
      for (const item of sort) {
        orderBy = this.commonService.firstUpper(item.field) + ' ' + (item.dir == undefined ? 'asc' : item.dir);
      }
    } else {
      orderBy = 'ProjectId desc';
    }
    if (isactive) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += 'Active eq true';
    }

    if (workspaceId > 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += 'WorkspaceId eq ' + workspaceId;
    }
    if (searchByTag.length > 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += `contains(TagDisplayList, ${searchByTag[0].text})`;

      for (let i = 1; i < searchByTag.length; i++) {
        filter += ' or ';
        filter += `contains(TagDisplayList, ${searchByTag[i].text})`;
      }
    }
    const params = new HttpParams()
      .set('$top', take.toString())
      .set('$skip', skip.toString())
      .set('$count', 'true')
      .set('$orderby', orderBy.toString())
      .set('$filter', filter.toString());


    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      const result200 = PagedResultDtoOfProjectDto.fromJS(response);
      return Observable.of(result200);
    });
  }
}
