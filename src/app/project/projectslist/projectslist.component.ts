import {
  Component,
  HostListener,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {Observable} from 'rxjs/Observable';
import {
  SortDescriptor,
  State
} from '@progress/kendo-data-query';
import {
  DataStateChangeEvent,
  GridDataResult,
  PageChangeEvent
} from '@progress/kendo-angular-grid';
import {FormGroup} from '@angular/forms';
import {ProjectListService} from './service/projectslist.service'
import {
  CreateProjectDto,
  IProjectDto,
  ProjectDto
} from './service/projectslist.model';
import {HubConnection} from '@aspnet/signalr-client';
import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {
  EqmSettings,
  User,
  UserRole
} from '../../shared/models/eqmSettings'
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {SelectListDto} from '../../shared/models/selectListDto'
import {UserSettingsDto} from '../../shared/usersettings/usersetting.model';
import {UserSettingsService} from '../../shared/usersettings/usersetting.service';
import {UserStarredProjectDto} from './service/userstarredprojects.model';
import {UserStarredProjectsService} from './service/userstarredprojects.service';
import {ProjectSettingsService} from '../shared/services/projectsettings.service';
import {TagMasterService} from '../tagmaster/service/tagmaster.service';
import {CreateProjectGroupsDto} from '../projectgroups/service/projectgroups.model';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-projectslist',
  templateUrl: './html/projectslist.component.html',
  styleUrls: ['./css/projectslist.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class ProjectListComponent implements OnInit {

  data: IProjectDto[];
  formGroup: FormGroup;
  view: Observable<GridDataResult>;
  private editedRowIndex: number;
  editDataItem: any = undefined;
  searchByTag: any = [];
  editProjectGroupData = new CreateProjectGroupsDto();
  isNew: boolean;
  pageSize = 10;
  skip = 0;
  state: State = {
    skip: 0,
    take: 5,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };
  gridView: GridDataResult;
  stargridView: GridDataResult;
  unStargridView: GridDataResult;
  deleteDataItem: any;
  sort: SortDescriptor[] = [];
  isactive = true;
  isArchivedView = false;
  searchtext = '';
  private hubConnection: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  eqmSettings: EqmSettings = new EqmSettings();
  user: User = new User();
  userRole: UserRole = new UserRole();
  siteList: SelectListDto[];
  mss: any;
  selectedProject: ProjectDto;
  openTaskListViewer = false;
  workspaceTagMasterList: SelectListDto[];
  workspaceTagMasterListVM: SelectListDto[];

  public workspaceTagMasterIds: any = [];
  private workspaceSettings: any;

  public buttonActionData: Array<any> = [];

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private userSettingsService: UserSettingsService,
              private projectListService: ProjectListService,
              private userStarredProjectsService: UserStarredProjectsService,
              private projectSettingsService: ProjectSettingsService,
              private tagMasterService: TagMasterService) {
    this.mss = this.commonService.getCookies('Mss');
    this.eqmSettings = this.eqmSettingsService.getEqmSettings();
    this.user = this.eqmSettingsService.getUser();
    this.userRole = this.eqmSettingsService.getUserRoles();
    this.workspaceSettings = this.projectSettingsService.getWorkSpaceSettings();
  }

  ngOnInit() {

  }

  getAllTagsByCurrentWorkspaceId() {

    if (this.workspaceSettings &&
      (this.workspaceSettings.canCreateWorkspaces ||
        this.workspaceSettings.globalWorkspaceAdmin ||
        this.workspaceSettings.globalWorkspaceViewing)) {
      this.buttonActionData = [];
      this.buttonActionData.push({
        text: 'New Groups',
        icon: 'plus',
        getText: () => {
          return 'NewGroups';
        }
      });
    }
    this.tagMasterService.getAlltagsByWorkspanceId(this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId).subscribe(
      eventResult => {

        this.workspaceTagMasterList = eventResult.map(function (obj) {
          console.log(obj.tagName)
          return {'text': obj.tagName, 'value': obj.workspaceTagMasterId, 'tagColor': obj.tagColor};
        });
        this.workspaceTagMasterListVM = this.workspaceTagMasterList;
      });
  }

  signalRConnection() {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');

        this.hubConnection
          .invoke('Subscribe',
            'Projects',
            'Project',
            'WorkspaceId',
            this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated',
      (groupName: string, operation: string, messageJson: string) => {
        const _responseText = messageJson;
        let result200: any = null;
        const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
        const resultData200 = this.commonService.toCamel(data);
        result200 = resultData200 ? ProjectDto.fromJS(resultData200) : new ProjectDto();
        const dataExist = this.containsObject(result200, this.gridView.data);

        if (operation.toLowerCase() === 'insert' && !dataExist) {
          this.gridView.data.unshift(result200);
        }

        if (operation.toLowerCase() === 'update') {
          this.gridView.data.forEach((element, index) => {
            if (element.projectId === result200.projectId) {
              if (this.isactive) {
                if (result200.active == this.isactive) {
                  this.gridView.data[index] = result200;
                } else {
                  operation = 'delete';
                }
              } else {
                this.gridView.data[index] = result200;
              }
            }
          });
        }

        if (operation.toLowerCase() === 'delete' && dataExist) {
          let index = null;
          this.gridView.data.forEach((element, i) => {
            if (element.projectId === result200.projectId) {
              index = i;
            }
          });
          if (index !== null) {
            this.gridView.data.splice(index, 1);
          }
        }
        this.loadGridData();
      });
  }

  containsObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].projectId === obj.projectId) {
        return true;
      }
    }

    return false;
  }


  userStarredProjectSignalRConnection(projectId) {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');

        this.hubConnection
          .invoke('Subscribe',
            'Projects',
            'UserStarredProject',
            'ProjectId', projectId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated',
      (groupName: string, operation: string, messageJson: string) => {
        const _responseText = messageJson;
        let result200: any = null;
        const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
        const resultData200 = this.commonService.toCamel(data);
        result200 = resultData200 ? UserStarredProjectDto.fromJS(resultData200) : new ProjectDto();
        const dataExist = this.userStarredProjectContainsObject(result200, this.gridView.data);

        if (operation.toLowerCase() === 'insert') {
          this.gridView.data.forEach((element, index) => {
            if (element.projectId === result200.projectId) {
              this.gridView.data[index].userStarredProjectId = result200.userStarredProjectId;
            }
          });
        }

        if (operation.toLowerCase() === 'update') {
          this.gridView.data.forEach((element, index) => {
            if (element.projectId === result200.projectId) {
              this.gridView.data[index].userStarredProjectId = result200.userStarredProjectId;
            }
          });
        }
        if (operation.toLowerCase() === 'delete') {
          this.gridView.data.forEach((element, index) => {
            if (element.projectId === result200.projectId) {
              this.gridView.data[index].userStarredProjectId = null;
            }
          });
        }
      });
  }

  userStarredProjectContainsObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].projectId === obj.projectId) {
        return true;
      }
    }

    return false;
  }

  // Dialog box
  opened = false;
  loading = true;
  gridLoading = true;

  public close(status) {

    const $this = this;
    if (status) {
      this.gridLoading = true;
      this.projectListService.delete($this.deleteDataItem.projectId,
        response => {
          this.toasterService.success('', 'Project Removed Successfully');
          this.gridLoading = false;
        },
        error => {
          this.gridLoading = false;
          this.toasterService.errorMessage(error);
        }
      );
    }
    $this.deleteDataItem = null;
    $this.opened = false;
  }

  public open() {
    this.opened = true;
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.loadGridData();
  }

  loadGridDataByTag(value: any) {
    this.loadGridData();
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
    this.loadGridData();
  }

  private loadGridData(): void {

    this.gridLoading = true;
    const currentWorkspaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;
    this.projectListService
      .getAll(this.pageSize, this.skip, this.sort, this.isactive, this.searchtext, currentWorkspaceId, this.searchByTag).subscribe(
      eventResult => {
        this.gridView = {
          data: eventResult.results,
          total: eventResult.count
        };
        this.loading = false;
        this.gridLoading = false;
      });
  }

  getProjectStatus(dataitem): string {
    let status = '';
    if (dataitem.projectStatus === 0) {
      status = 'No Status';
    }
    else if (dataitem.projectStatus === 1) {
      status = 'Active';
    }
    else if (dataitem.projectStatus === 2) {
      status = 'On Hold';
    }
    else if (dataitem.projectStatus === 3) {
      status = 'Planned';
    }
    else if (dataitem.projectStatus === 4) {
      status = 'Cancelled';
    }
    else if (dataitem.projectStatus === 5) {
      status = 'Completed';
    }
    return status;
  }

  private isStarredGridView = false;

  setGridData() {
    const starredData = [];
    const unStarredData = [];
    this.isStarredGridView = false;
    this.gridView.data.forEach((element, i) => {
      if (this.gridView.data[i].userStarredProjectId != undefined) {
        this.isStarredGridView = true;
        starredData.push(this.gridView.data[i]);
      } else {
        unStarredData.push(this.gridView.data[i]);
      }
    });

    this.stargridView = {
      data: starredData,
      total: starredData.length
    };

    this.unStargridView = {
      data: unStarredData,
      total: unStarredData.length
    };

  }

  public tagHandleFilter(filter: any): void {
    this.workspaceTagMasterList = this.workspaceTagMasterListVM
      .filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  private projectGroupFormFlag = false;

  public addNewProjectGroup() {
    this.editProjectGroupData = new CreateProjectGroupsDto();
    this.projectGroupFormFlag = true;
  }

  public closeForm() {
    this.projectGroupFormFlag = false;
  }

  public addNewProject() {
    this.editDataItem = new CreateProjectDto();
    this.isNew = true;
  }

  public addHandler({sender}) {
    this.closeEditor(sender);
    this.editDataItem = new CreateProjectDto();
    this.isNew = true;
    this.isArchivedView = false;
  }

  public editHandler({dataItem}) {
    this.editDataItem = dataItem;
    this.isNew = false;
    this.isArchivedView = true;
  }


  closeProjectGroupFrom($event) {
    this.projectGroupFormFlag = false;
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public cancelHandler() {
    this.editDataItem = undefined;
    this.formGroup = undefined;
    this.isNew = false;
  }

  public removeHandler({dataItem}) {
    this.deleteDataItem = dataItem;
    this.open();
  }

  public showActive() {
    this.isactive = !this.isactive;
    this.gridLoading = true;
    this.doSearch(this.state, this);
  }

  public onSearchKeyup() {
    this.skip = 0;
    this.pageSize = 10;
    this.gridLoading = true;
    this.loadGridData();

  }


  timeout = null;

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridLoading = true;

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.doSearch(state, this);
    }, 1000);
  }

  doSearch(state: any, that) {
    if (state.filter) {
      const search = that.commonService.getFilter(state.filter.filters, that.isactive);
      const currentWorkspaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;
      that.projectListService.getAll(
        state.take,
        state.skip,
        that.sort,
        this.isactive,
        search,
        currentWorkspaceId,
        this.workspaceTagMasterIds)
        .subscribe(eventResult => {

          that.gridView = {
            data: eventResult.results,
            total: eventResult.count
          };

          that.loading = false;
          that.gridLoading = false;
        });

      this.skip = state.skip;
      this.pageSize = state.take;
    }
    else {
      that.loadGridData();
    }
  }


  public ActionData: Array<any> = [
    {
      text: 'Edit',
      icon: 'edit',
      getText: () => {
        return 'Edit';
      }
    },
    {
      text: 'Delete',
      icon: 'trash',
      getText: () => {
        return 'Delete';
      }
    }];

  public openDetailViewer = false;
  public openTaskDetailViewer = false;

  public onButtonAction(e, dataItem) {
    this.workspaceSettings = this.projectSettingsService.getWorkSpaceSettings();
    if (e === undefined) {
      if (this.workspaceSettings.isAdmin) {
        this.addNewProject();
      } else {
        this.toasterService.error('', 'You have no permission');
      }

    } else if (e.text === 'New Groups') {

      if (this.workspaceSettings.isAdmin) {
        this.addNewProjectGroup();
      } else {
        this.toasterService.error('', 'You have no permission');
      }
    }

  }

  public projectId: ProjectDto;

  public onAction(e, dataItem) {
    this.workspaceSettings = this.projectSettingsService.getWorkSpaceSettings();
    if (e === undefined) {
      this.openTaskListViewer = true;
      this.selectedProject = dataItem;
    } else if (e.text === 'Delete') {
      if (this.workspaceSettings.isAdmin) {
        this.deleteDataItem = dataItem;
        this.open();
      } else {
        this.toasterService.error('', 'You have no permission');
      }
    }
    else if (e.text === 'Edit') {
      if (this.workspaceSettings.isAdmin) {
        this.editDataItem = dataItem;
        this.editDataItem.startDate = this.editDataItem.startDate; //new Date();
        this.editDataItem.dueDate = this.editDataItem.dueDate; //new Date();
        this.editDataItem.actualCompletionDate = this.editDataItem.actualCompletionDate; /// new Date(this.editDataItem.actualCompletionDate);
        this.editDataItem.archivedDate = this.editDataItem.archivedDate; // new Date(this.editDataItem.archivedDate);
        this.isNew = false;
      } else {
        this.toasterService.error('', 'You have no permission');
      }
    }
  }

  public openTaskDetail(e) {
    this.openTaskDetailViewer = true;
  }

  public onOpen(e, datatim) {
    console.log(e, datatim);
    console.log('oen');
  }


  public siteHandleFilter(filter: any): void {
    this.siteList = this.siteList
      .filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  public updateCurrentSiteId() {

    // we have to remove this from here
    this.gridLoading = true;

    const data = new UserSettingsDto();

    data.active = true;
    data.roleId = this.userRole.userRoleId;
    data.userId = this.user.userId;
    data.userSettingsId = this.eqmSettings.userSettingsId;
    data.currentSiteId = this.eqmSettings.currentSiteId;
    // data.showOnlyMyAssignments = this.isShowOnlyMyAssignments;

    this.userSettingsService.update(data,
      response => {
        this.toasterService.success('', 'User Site Updated Successfully');

        // update cookies to take new changes
        this.eqmSettingsService.userSettings().subscribe();

        this.loadGridData();
        this.gridLoading = false;
      },
      error => {
        this.gridLoading = false;
        this.toasterService.errorMessage(error);
      }
    );
  }

  public setUserStarredProject(projectId) {
    const data = new UserStarredProjectDto();
    data.projectId = projectId;
    this.userStarredProjectSignalRConnection(projectId);
    this.gridLoading = true;
    this.userStarredProjectsService.create(data,
      response => {
        this.toasterService.success('', 'Project starred Successfully');
        this.gridLoading = false;
      },
      error => {
        this.toasterService.errorMessage(error);
        this.gridLoading = false;
      }
    );
  }


  public closeTaskListViewer(data) {
    this.openTaskListViewer = false;
    this.selectedProject = undefined;
  }

  public closeTaskDetailViewer(data) {
    console.log(this.openTaskDetailViewer);
    this.openTaskDetailViewer = false;
  }

  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {
      this.openTaskListViewer = false;
      this.cancelHandler();
      this.close(false);
    }
  }

  private selectedWorkspaceChange = false;

  workspaceSelectionChange($event) {
    if ($event !== undefined) {
      this.loadGridData();
      this.selectedWorkspaceChange = true;
      this.signalRConnection();
      this.getAllTagsByCurrentWorkspaceId();
    }
  }
}
