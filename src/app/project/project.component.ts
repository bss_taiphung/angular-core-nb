import {Component} from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';


@Component({
  selector: 'app-project',
  template: '<router-outlet></router-outlet>'
})
export class ProjectComponent {

  constructor() {
  }
}
