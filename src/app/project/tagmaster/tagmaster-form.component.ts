import {
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';


import {SelectListDto} from '../../shared/models/selectListDto';
import {CommonService} from '../../shared/services/common.service';
import {UserRole} from '../../shared/models/eqmSettings';
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {ProjectSettingsService} from '../shared/services/projectsettings.service';

import {TagMasterDto} from './service/tagmaster.model';

@Component({
  selector: 'kendo-grid-tagmaster-form',
  styles: [
    'input[type=text] { width: 100%; }'
  ],
  templateUrl: './html/tagmaster-form.component.html'
})
export class TagMasterFormComponent {
  public open = false;
  userRole: UserRole
  defaultColor = '';
  public editForm: FormGroup = new FormGroup({
    'workspaceTagMasterId': new FormControl(),
    'workspaceId': new FormControl(),
    'tagName': new FormControl('', Validators.required),
    'tagColor': new FormControl(),
    'active': new FormControl(false),
  });

  constructor(private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private toasterService: ToasterHelperService,
              private projectSettingsService: ProjectSettingsService) {

  }

  @Input() public isNew = false;
  @Input() public active = false;
  public workspaceIdList: SelectListDto[];
  public filterworkspaceIdList: SelectListDto[];


  @Input()
  public set model(tagMaster: TagMasterDto) {
    this.editForm.reset(tagMaster);
    this.active = tagMaster !== undefined;
    this.commonService.getAll('Projects', 'Workspace').subscribe(eventResult => {
      this.filterworkspaceIdList = this.workspaceIdList = eventResult.results;
    });
    if (!this.editForm.value.tagColor) {
      this.editForm.setValue({
        workspaceTagMasterId: null,
        workspaceId: null,
        tagName: null,
        active: true,
        tagColor: '#ffffff'
      });
    }
  }

  private workspaceIdListHandleFilter(value) {
    this.filterworkspaceIdList = this.workspaceIdList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<TagMasterDto> = new EventEmitter();

  public onSave(e): void {
    if (this.isNew === false && this.projectSettingsService.getWorkSpaceSettings().isAdmin) {
      e.preventDefault();
      this.save.emit(this.editForm.value);
      this.open = false;
      this.active = false;
    }
    else if (this.isNew === false && !this.projectSettingsService.getWorkSpaceSettings().isAdmin) {
      this.toasterService.errorMessage('You don\'t have edit permission');

    }
    if (this.isNew) {
      e.preventDefault();
      this.save.emit(this.editForm.value);
      this.open = false;
      this.active = false;
    }
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
    this.open = false;
  }

  private closeForm(): void {
    this.open = false;
    this.cancel.emit();
    this.active = false;
  }
}
