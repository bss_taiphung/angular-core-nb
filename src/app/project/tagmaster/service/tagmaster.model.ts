import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';


// region ITagMasterDto

export interface ITagMasterDto {
  tagName: string;
  tagColor: string;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  workspaceTagMasterId: number;
  workspaceId: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
}

export class TagMasterDto implements ITagMasterDto {
  tagName: string;
  tagColor: string;
  createdByName: string;
  createdDate: string;
  createdDateTime: string;
  modifiedByName: string;
  modifiedDateTime: string;
  workspaceTagMasterId: number;
  workspaceId: number;
  createdBy: number;
  modifiedBy: number;
  active: boolean;

  static fromJS(data: any): TagMasterDto {
    const result = new TagMasterDto();
    result.init(data);
    return result;
  }

  constructor(data?: ITagMasterDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.tagName = data['tagName'];
      this.tagColor = data['tagColor'];
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.workspaceTagMasterId = data['workspaceTagMasterId'];
      this.workspaceId = data['workspaceId'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.active = data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['tagName'] = this.tagName;
    data['tagColor'] = this.tagColor;
    data['createdByName'] = this.createdByName;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['workspaceTagMasterId'] = this.workspaceTagMasterId;
    data['workspaceId'] = this.workspaceId;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;

    return data;
  }
}

// endregion

// region ICreateTagMasterDto
export interface ICreateTagMasterDto {
  tagName: string;
  tagColor: string;
  active: boolean;
}

export class CreateTagMasterDto implements ICreateTagMasterDto {
  workspaceTagMasterId = 0;
  tagName: string;
  tagColor: string;
  workspaceId: number;
  active = true;

  static fromJS(data: any): TagMasterDto {
    const result = new TagMasterDto();
    result.init(data);
    return result;
  }

  constructor(data?: ITagMasterDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.workspaceTagMasterId = 0;
      this.tagName = data['tagName'];
      this.tagColor = data['tagColor'];
      this.workspaceId = data['workspaceId'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['workspaceworkspaceTagMasterId'] = 0;
    data['tagName'] = this.tagName;
    data['tagColor'] = this.tagColor;
    data['workspaceId'] = this.workspaceId;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

// endregion

// region IPagedResultDtoOfTagMasterDto

export interface IPagedResultDtoOfTagMasterDto {
  results: TagMasterDto[];
  count: number;
}

export class PagedResultDtoOfTagMasterDto implements IPagedResultDtoOfTagMasterDto {
  results: TagMasterDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfTagMasterDto {
    const result = new PagedResultDtoOfTagMasterDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfTagMasterDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(TagMasterDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion
