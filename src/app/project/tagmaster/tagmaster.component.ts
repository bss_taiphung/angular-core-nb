import {
  Component,
  HostListener,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import {FormGroup} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';
import {Observable} from 'rxjs/Observable';

import {SortDescriptor, State} from '@progress/kendo-data-query';
import {
  DataStateChangeEvent,
  GridDataResult,
  PageChangeEvent
} from '@progress/kendo-angular-grid';
import {HubConnection} from '@aspnet/signalr-client';

import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {EqmSettings, User} from '../../shared/models/eqmSettings'
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {SelectListDto} from '../../shared/models/selectListDto'
import {UserSettingsService} from '../../shared/usersettings/usersetting.service';
import {ProjectSettingsService} from '../shared/services/projectsettings.service'

import {TagMasterService} from './service/tagmaster.service'
import {CreateTagMasterDto, ITagMasterDto, TagMasterDto} from './service/tagmaster.model';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'tagmaster',
  templateUrl: './html/tagmaster.component.html',
  styleUrls: ['./css/tagmaster.component.css'],
  encapsulation: ViewEncapsulation.None,
})

export class TagMasterComponent implements OnInit {

  data: ITagMasterDto[];
  public formGroup: FormGroup;
  public view: Observable<GridDataResult>;
  private editedRowIndex: number;
  public editDataItem: CreateTagMasterDto;
  public isNew: boolean;
  private workspaceSettings: any;
  public active: boolean;
  public pageSize = 10;
  public skip = 0;
  public state: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };
  public gridView: GridDataResult;
  public deleteDataItem: any;
  private sort: SortDescriptor[] = [];
  public isactive: boolean = true;
  public searchtext: string = '';
  private hubConnection: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  eqmSettings: EqmSettings = new EqmSettings();
  user: User = new User();
  public siteList: SelectListDto[];
  public mss: any;

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private userSettingsService: UserSettingsService,
              private tagMasterService: TagMasterService,
              private projectSettingsService: ProjectSettingsService) {
    this.mss = this.commonService.getCookies('Mss');
    this.eqmSettings = this.eqmSettingsService.getEqmSettings();
    this.user = this.eqmSettingsService.getUser();
    this.projectSettingsService.getWorkSpaceSettings().isAdmin
  }

  ngOnInit() {
    this.loadGridData();
    this.signalRConnection();
  }

  signalRConnection() {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    let workspaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');
        this.hubConnection
          .invoke('Subscribe', 'Projects', 'WorkspaceTagMaster', 'workSpaceId', workspaceId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
      const _responseText = messageJson;
      let result200: any = null;
      let data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      let resultData200 = this.commonService.toCamel(data);
      result200 = resultData200 ? TagMasterDto.fromJS(resultData200) : new TagMasterDto();

      var dataExist = this.containsObject(result200, this.gridView.data);

      if (operation.toLowerCase() === 'insert' && !dataExist) {
        this.gridView.data.unshift(result200);
      }

      if (operation.toLowerCase() === 'update') {
        this.gridView.data.forEach((element, index) => {
          if (element.workspaceTagMasterId === result200.workspaceTagMasterId) {
            // 
            if (this.isactive) {
              if (result200.active == this.isactive) {
                this.gridView.data[index] = result200;
              } else {
                operation = 'delete';
              }
            } else {
              this.gridView.data[index] = result200;
            }
          }
        });
      }

      if (operation.toLowerCase() === 'delete' && dataExist) {
        var index = null;
        this.gridView.data.forEach((element, i) => {
          if (element.WorkspaceTagMasterId === result200.workspaceTagMasterId) {
            index = i;
          }
        });
        if (index !== null) {
          this.gridView.data.splice(index, 1);
        }
      }
    });
  }

  containsObject(obj, list) {
    var x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].workspaceTagMasterId === obj.workspaceTagMasterId) {
        return true;
      }
    }

    return false;
  }


  timeout = null;

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridLoading = true;

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.doSearch(state, this);
    }, 1000);
  }

  doSearch(state: any, that) {
    if (state.filter) {
      var search = that.commonService.getFilter(state.filter.filters, that.isactive);
      let workspaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;
      that.tagMasterService.getAll(state.take, state.skip, that.sort, this.isactive, search, workspaceId)
        .subscribe(eventResult => {
          that.gridView = {
            data: eventResult.results,
            total: eventResult.count
          };
          that.loading = false;
          that.gridLoading = false;
        });

      this.skip = state.skip;
      this.pageSize = state.take;
    }
    else {
      that.loadGridData();
    }
  }

  // Dialog box 
  opened: boolean = false;
  loading: boolean = true;
  gridLoading: boolean = true;

  public close(status) {

    var $this = this;
    if (status && this.projectSettingsService.getWorkSpaceSettings().isAdmin) {
      this.gridLoading = true;
      this.tagMasterService.delete($this.deleteDataItem.workspaceTagMasterId,
        response => {
          if (response) {
            this.toasterService.success('', 'TagMaster Removed Successfully');
          }
          else {
            this.toasterService.customErrorMessage('It can\'t be deleted because It is being used under Project');
          }
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
    $this.deleteDataItem = null;
    $this.opened = false;
  }

  public open() {
    this.opened = true;
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.loadGridData();
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
  }

  private loadGridData(): void {
    let workspaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;
    this.tagMasterService.getAll(
      this.pageSize,
      this.skip,
      this.sort,
      this.isactive,
      this.searchtext,
      workspaceId)
      .subscribe(eventResult => {
        this.gridView = {
          data: eventResult.results,
          total: eventResult.count
        };
        this.loading = false;
        this.gridLoading = false;
      });
  }

  public addHandler({sender}) {
    this.closeEditor(sender);
    this.editDataItem = new CreateTagMasterDto();
    this.isNew = true;

  }

  public editHandler({dataItem}) {
    this.editDataItem = dataItem;
    this.isNew = false;
    this.active = true;

  }

  public saveHandler(data) {
    if (this.isNew) {
      data.active = (data.active == null) ? false : data.active;
      const inputTagMasterData: CreateTagMasterDto = data;
      inputTagMasterData.workspaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;
      this.tagMasterService.create(inputTagMasterData,
        response => {
          this.toasterService.success('', 'TagMaster Saved Successfully');
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
    else {
      const inputTagMasterData: TagMasterDto = data;
      inputTagMasterData.workspaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;
      this.tagMasterService.update(inputTagMasterData,
        response => {
          this.toasterService.success('', 'TagMaster Updated Successfully');
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public cancelHandler() {
    this.editDataItem = undefined;
    this.formGroup = undefined;
  }

  public removeHandler({dataItem}) {
    this.deleteDataItem = dataItem;
    this.open();
  }

  public showActive() {
    this.isactive = !this.isactive;
    this.gridLoading = true;
  }

  public onSearchKeyup() {
    this.skip = 0;
    this.pageSize = 10;
    this.gridLoading = true;
    this.loadGridData();
  }

  public ActionData: Array<any> = [{
    text: 'Delete',
    icon: 'trash'
  }];

  public onAction(e, dataItem) {
    if (e === undefined) {
      this.editDataItem = dataItem;
      this.isNew = false;
    } else {
      if (e.text === 'Delete') {
        this.deleteDataItem = dataItem;
        this.open();
      }
    }
  }

  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {
      this.cancelHandler();
      this.close(false);
    }
  }

  workspaceSelectionChange($event) {
    this.loadGridData();
    this.signalRConnection();
  }
}
