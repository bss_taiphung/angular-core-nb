import {RouterModule, Routes} from '@angular/router';
import {ProjectComponent} from './project.component';
import {AuthGuard} from '../shared/guards/auth.guard'
import {WorkspaceComponent} from './workspace/workspace.component';
import {WorkspaceTeamComponent} from './workspace/workspace-team.component';
import {RoleFormComponent} from './role/role.component';
import {UserSettingsComponent} from './usersettings/usersettings.component';
import {ProjectGroupsComponent} from './projectgroups/projectgroups.component';
import {ProjectListComponent} from './projectslist/projectslist.component';
import {TagMasterComponent} from './tagmaster/tagmaster.component';


export const routes: Routes = [
  {
    path: '',
    component: ProjectComponent,
    canActivate: [AuthGuard],
    children: [
      {path: 'workspace', component: WorkspaceComponent, canActivate: [AuthGuard]},
      {path: 'workspaceteam', component: WorkspaceTeamComponent, canActivate: [AuthGuard]},
      {path: 'roles', component: RoleFormComponent, canActivate: [AuthGuard]},
      {path: 'usersettings', component: UserSettingsComponent, canActivate: [AuthGuard]},
      //{ path: 'projectgroups', component: ProjectGroupsComponent, canActivate: [AuthGuard] },
      {path: 'projectslist', component: ProjectListComponent, canActivate: [AuthGuard]},
      //{ path: 'projecttasklist', component: ProjectTaskListComponent, canActivate: [AuthGuard] },
      {path: 'tagmaster', component: TagMasterComponent, canActivate: [AuthGuard]},
      {path: 'projectgroups', component: ProjectGroupsComponent, canActivate: [AuthGuard]}
    ]
  }
];

export const routing = RouterModule.forChild(routes);
