import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {Observable} from 'rxjs/Observable';
import {HubConnection} from '@aspnet/signalr-client';

import {GridDataResult} from '@progress/kendo-angular-grid';
import {
  filter,
  forEach, groupBy,
  includes,
  orderBy as orderForGrid,
  remove,
  replace
} from 'lodash';

import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {ProjectSettingsService} from '../shared/services/projectsettings.service';
import {TagMasterService} from '../tagmaster/service/tagmaster.service';

import {
  EqmSettings,
  User,
  UserRole
} from '../../shared/models/eqmSettings'
import {SelectListDto} from '../../shared/models/selectListDto'
import {UserSettingsService} from '../../shared/usersettings/usersetting.service';

import {
  BulkupdateProjectTaskListDto,
  CreateProjectTaskListDto,
  IProjectTaskListDto,
  ProjectTaskListDto
} from './service/projecttasklist.model';
import {ProjectTaskListService} from './service/projecttasklist.service'
import {ProjectDto} from '../projectslist/service/projectslist.model';
import {ProjectTaskService} from './projecttask/service/projecttask.service'
import {ProjecTaskDTO} from './projecttask/service/projecttask.model';
import {ProjectListService} from '../projectslist/service/projectslist.service';
import {ProjectTagDTO} from '../projecttaskdetail/service/projecttag.model';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'kendo-projects-tasks-list',
  templateUrl: './html/projecttasklist.component.html',
  styleUrls: ['./css/projecttasklist.component.css'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '(document:click)': 'documentOnClick($event)',
  }
})


export class ProjectTaskListComponent implements AfterViewInit {

  @ViewChild('txtAddNewTasklist') private addNewTaskElementRef: ElementRef;

  public myFocusTriggeringEventEmitter = new EventEmitter<boolean>();
  private project: ProjectDto;
  private editProjectDataItem: any;
  private userPermissionModel: string;

  private taskDragArray: any[] = [];
  projectTaskUpdate = false;
  private projectTaskDto: ProjecTaskDTO;
  @ViewChild('txtAddNewTasklist') txtAddNewTasklist: ElementRef;
  @Input() public openTaskListViewer = false;

  @Input()
  public set model(selectedProject: any) {
    if (selectedProject !== undefined) {
      this.project = selectedProject;
      this.userPermissionModel = selectedProject.permissionModel;
      this.loadGridData();
      this.signalRConnection();
      this.signalRConnectionForProjectUpdate();

    }
  }

  @Output() closeTaskListViewer: EventEmitter<any> = new EventEmitter();

  data: IProjectTaskListDto[];
  public view: Observable<GridDataResult>;
  private checkListDataUpdateFlag = false;
  public reoderBulkUpdateFlag = false;
  public isNewTaskList = false;
  isNew = false;
  public gridView: any;
  public isactive = true;
  private hubConnection: HubConnection;
  private hubConnectionForProject: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  eqmSettings: EqmSettings = new EqmSettings();
  user: User = new User();
  userRole: UserRole = new UserRole();
  siteList: SelectListDto[];
  mss: any;
  opened = false;
  loading = true;
  isDragTask = false;
  isTaskInDragMode = false;

  public colorsA: string[] = ['Violet', 'Magenta', 'Purple', 'SlateBlue'];
  isdragChild = false;
  private workspaceSettings: any;

  constructor(private toasterService: ToasterHelperService,
              private projectTaskService: ProjectTaskService,
              private commonService: CommonService,
              private userSettingsService: UserSettingsService,
              private projectTaskListService: ProjectTaskListService,
              private projectListService: ProjectListService,
              private projectSettingsService: ProjectSettingsService,
              private tagMasterService: TagMasterService) {
    this.workspaceSettings = this.projectSettingsService.getWorkSpaceSettings();
  }

  ngAfterViewInit() {
    this.setfocusAddTaskList();
  }

  private scrollTimeout = null;

  signalRConnectionForProjectUpdate() {
    this.hubConnectionForProject =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnectionForProject
      .start()
      .then(() => {
        console.log('Connection started!');

        this.hubConnectionForProject
          .invoke('Subscribe',
            'Projects',
            'Project',
            'WorkspaceId',
            this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnectionForProject.on('clientOnEntityUpdated',
      (groupName: string, operation: string, messageJson: string) => {
        const _responseText = messageJson;
        let result200: any = null;

        const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
        const resultData200 = this.commonService.toCamel(data);
        result200 = resultData200 ? ProjectDto.fromJS(resultData200) : new ProjectDto();
        if (operation.toLowerCase() === 'update') {
          if (this.project.projectId === result200.projectId) {
            this.project = result200;
          }
        }
      });
  }


  signalRConnection() {

    function containsObject(obj, list) {
      let x;
      for (x in list) {
        if (list.hasOwnProperty(x) && list[x].projectTaskListId === obj.projectTaskListId) {
          return true;
        }
      }

      return false;
    }

    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');

        this.hubConnection
          .invoke('Subscribe', 'Projects', 'ProjectTaskList', 'ProjectId', this.project.projectId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
      const _responseText = messageJson;
      let result200: any = null;
      const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      const resultData200 = this.commonService.toCamel(data);
      if (operation.toLowerCase() === 'bulkupdate') {
        result200 = resultData200 ? BulkupdateProjectTaskListDto.fromJS(resultData200).results : new ProjectTaskListDto();
      } else {
        result200 = resultData200 ? ProjectTaskListDto.fromJS(resultData200) : new ProjectTaskListDto();
      }
      const dataExist = containsObject(result200, this.gridView);
      if (operation.toLowerCase() === 'insert' && !dataExist) {
        this.gridView.push(result200);
        this.scrollTimeout = setTimeout(() => {
          this.setfocusAddTaskList();
        }, 500);
      }
      if (operation.toLowerCase() === 'bulkupdate') {
        if (!this.reoderBulkUpdateFlag) {
          forEach(result200,
            (item) => {
              this.gridView.forEach((element, index) => {
                if (element.projectTaskListId === item.projectTaskListId) {
                  this.gridView[index] = item;
                }
              });
            });
        }
      }

      if (operation.toLowerCase() === 'delete' && dataExist) {
        let index = null;
        this.gridView.forEach((element, i) => {
          if (element.projectTaskListId === result200.projectTaskListId) {
            index = i;
          }
        });
        if (index !== null) {
          this.gridView.splice(index, 1);
        }
      }
      if (!this.reoderBulkUpdateFlag) {
        this.gridView = orderForGrid(this.gridView, ['projectSortOrder'], ['asc']);
      } else {
        this.reoderBulkUpdateFlag = false;
      }
    });
  }


  timeout = null;

  public taskDragHandler(data: any) {

    if (!this.taskDragObject(this.taskDragArray, data.dataItem)) {
      this.taskDragArray.push({
        projectId: data.dataItem.projectId,
        projectTaskId: data.dataItem.projectTaskId,
        projectTaskListId: data.dataItem.projectTaskListId,
        projectTaskName: data.dataItem.projectTaskName,
        Action: data.Action
      });
    }

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }
    console.log(this.taskDragArray);
    this.timeout = setTimeout(() => {
      this.saveTask(this.taskDragArray.filter(i => i.Action === 'add'));
    }, 3000);
  }


  public saveTask(dataItem: any) {
    const projectTasksList: any = [];
    dataItem.forEach(item => {
      const projectTasks: ProjecTaskDTO = new ProjecTaskDTO();
      projectTasks.projectTaskId = item.projectTaskId;
      projectTasks.projectTaskListId = item.projectTaskListId;
      projectTasks.projectTaskName = item.projectTaskName;
      projectTasksList.push(projectTasks);
    });
    this.checkListDataUpdateFlag = true;
    this.taskDragArray = [];
    this.projectTaskService.updateTaskListId(projectTasksList,
      response => {
        this.toasterService.success('', 'Project Task Saved Successfully');
      },
      error => {
        this.checkListDataUpdateFlag = false;
        this.toasterService.errorMessage(error);
      }
    );
  }

  public deleteTask(task: any) {
    this.projectTaskService.delete(task.projectTaskId,
      response => {
        this.toasterService.success('', 'Project Task Delete Successfully');
      },
      error => {
        this.toasterService.errorMessage(error);
      }
    );
  }

  taskDragObject(obj, list) {
    if (!(obj.length > 0)) {
      return false;
    }
    for (let i = 0; i < obj.length; i++) {
      if (obj[i].projectId === list.projectId && obj[i].projectTaskId === list.projectTaskId && obj[i].projectTaskListId === list.projectTaskListId) {
        this.taskDragArray.splice(i, 1);
        return true;
      }
    }

  }

  private loadGridData(): void {
    this.projectTaskListService.getAll(this.isactive, this.project.projectId).subscribe(eventResult => {
      this.gridView = orderForGrid(eventResult.results, ['projectSortOrder'], ['asc']);
      if (eventResult.results.length === 0) {
        this.setfocusAddTaskList();
      }
      this.getAllUsersAndTags();
      this.loading = false;
    });
  }

  private projectUserAssignedToList: any[] = [];
  private projectTagList: any[] = [];


  private getAllUsersAndTags() {
    /*Get users and tag*/
    this.commonService.getAllWithId(this.project.projectId, 'Projects', 'ProjectTeam').subscribe(eventResult => {
      if (eventResult.results != null && eventResult.results.length > 0) {
        eventResult.results.forEach(itm => {
          if (itm.userAvatar == null) {
            itm.userAvatar = this.commonService.getUserThumbnail(itm.value);
          }
        });
      }

      this.projectUserAssignedToList = eventResult.results;
    });

    //Get all project tages


    this.tagMasterService.getAlltagsByWorkspanceId(this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId).subscribe(
      eventResult => {

        this.projectTagList = ProjectTagDTO.fromJSArrayList(eventResult);
      });
  }


  private checkUserPermission(action): boolean {

    if (this.userPermissionModel.toUpperCase() === 'FULL') {
      return true;
    }
    else {
      return false;
    }
  }

  public showNewTaskList() {
    // this.addNewTaskElementRef.nativeElement.focus();
    // document.getElementById("txtAddNewTasklist").focus();
    this.isNewTaskList = !this.isNewTaskList;
  }

  documentOnClick(event) {
    if (this.isNewTaskList && event.target.innerText !== 'Add New Tasklist' && event.target.innerText !== '') {
      this.isNewTaskList = !this.isNewTaskList;
    }
  }

  public updateTaskListName(taskListDto) {
    this.isNew = false;
    this.saveHandler(taskListDto);
  }

  inputFocusClass = true;

  public saveTaskList(event, dataItem, value) {
    if (event.keyCode == 13) {
      if (dataItem !== null) {
        dataItem.projectTaskListName = value;
        this.isNew = false;
        this.saveHandler(dataItem);
      } else {
        this.isNew = true;
        this.saveHandler({projectTaskListName: value, projectId: this.project.projectId});
      }
      event.target.value = '';
      // this.isNewTaskList = false;
    }
  }

  public saveHandler(projecttasklistdata) {

    if (this.isNew) {
      if (!this.checkUserPermission('add')) {
        return;
      }
      const inputProjectListTaskData: CreateProjectTaskListDto = projecttasklistdata;
      this.projectTaskListService.create(inputProjectListTaskData,
        response => {
          this.toasterService.success('', 'Project Task List Saved Successfully');
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
    else {
      if (!this.checkUserPermission('edit')) {
        return;
      }
      const inputProjectTask: ProjectTaskListDto = projecttasklistdata;
      this.projectTaskListService.update(inputProjectTask,
        response => {
          this.toasterService.success('', 'Project Task List Updated Successfully');
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  setfocusAddTaskList() {
    this.inputFocusClass = true;
  }

  public isdragChildHandler(status) {
    this.isdragChild = status;
  }

  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {
      this.closeForm();
      //this.close(false);
    }
  }

  private closeForm(): void {
    this.loading = true;
    this.gridView = null;
    this.openTaskListViewer = false;
    this.closeTaskListViewer.emit();
  }


  /*Drage & Drop Functionality for task list*/
  public onTaskListDragEnd(projectTaskList: any, e: any): void {
    if (!this.checkUserPermission('edit')) {
      return;
    }
    if (!this.isdragChild) {
      this.projectTaskListService.reOrderProjectTaksList(projectTaskList,
        response => {
          this.isDragTask = true;
          this.reoderBulkUpdateFlag = true;
          // this.toasterService.success("", 'Updated order');
        },
        error => {
          // this.toasterService.errorMessage(error);
        }
      );

    }
    this.isdragChild = false;
  }

  openTaskListDeleteDialog = false;
  deleteTaskListItem: any;

  public deleteTaskList(taskListDto) {
    if (!this.checkUserPermission('delete')) {
      return;
    }
    this.deleteTaskListItem = taskListDto;
    this.openTaskListDeleteDialog = true;
  }

  public deleteTaskListHandler(status) {
    if (status) {
      if (!this.checkUserPermission('delete')) {
        return;
      }
      this.projectTaskListService.delete(this.deleteTaskListItem.projectTaskListId,
        response => {
          this.toasterService.success('', 'Task List Removed Successfully');
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
    this.deleteTaskListItem = null;
    this.openTaskListDeleteDialog = false;
  }

  checkListUpdateFlag(chekListUpdated) {
    this.checkListDataUpdateFlag = true;
  }

  isOpen = false;

  editProjectSettings() {
    this.editProjectDataItem = this.project;
    this.isOpen = false;
  }

  public projectcancelHandler($event) {

    if ($event !== undefined) {
      this.project = $event;
      this.userPermissionModel = this.project.permissionModel;
    }
    this.editProjectDataItem = undefined;
    this.isOpen = false;
  }
}
