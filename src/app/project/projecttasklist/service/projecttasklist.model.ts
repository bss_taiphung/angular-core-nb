import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

// region IProjectTaskListDto

export interface IProjectTaskListDto {
  projectTaskListId: number;
  projectId: number;
  projectTaskListName: string;
  createdByName: string;
  active: boolean;
  Tasks: any;
  projectSortOrder: boolean;
  numTasksWithTimeSpent: number;
  numMinutesSpent: number;
  numTasksWithPoints: number;
  numTasksActive: number;
  numTasksCompleted: number;
  numActivePoints: number;
  numCompletedPoints: number;
  numMinutesEstimated: number;
  percentComplete: number;
  taskListSortOrder: number;
}

export class ProjectTaskListDto implements IProjectTaskListDto {
  projectTaskListId: number;
  projectId: number;
  projectTaskListName: string;
  createdByName: string;
  active: boolean;
  Tasks: any;
  projectSortOrder: boolean;
  numTasksWithTimeSpent: number;
  numMinutesSpent: number;
  numTasksWithPoints: number;
  numTasksActive: number;
  numTasksCompleted: number;
  numActivePoints: number;
  numCompletedPoints: number;
  numMinutesEstimated: number;
  percentComplete: number;
  taskListSortOrder: number;

  static fromJS(data: any): ProjectTaskListDto {
    const result = new ProjectTaskListDto();
    result.init(data);
    return result;
  }

  /*
  assignedTo: SelectListDto[];
  followers: SelectListDto[];
  tags: ProjectTagDTO[];
  */

  constructor(data?: IProjectTaskListDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.projectTaskListId = data['projectTaskListId'];
      this.projectId = data['projectId'];
      this.projectTaskListName = data['projectTaskListName'];
      this.createdByName = data['createdByName'];
      this.active = data['active'];
      this.Tasks = null;
      this.projectSortOrder = data['projectSortOrder'];
      this.numTasksWithTimeSpent = data['numTasksWithTimeSpent'] != null ? data['numTasksWithTimeSpent'] : 0;
      this.numMinutesSpent = data['numMinutesSpent'] != null ? data['numMinutesSpent'] : 0;
      this.numTasksWithPoints = data['numTasksWithPoints'] != null ? data['numTasksWithPoints'] : 0;
      this.numTasksActive = data['numTasksActive'] != null ? data['numTasksActive'] : 0;
      this.numTasksCompleted = data['numTasksCompleted'] != null ? data['numTasksCompleted'] : 0;
      this.numActivePoints = data['numActivePoints'] != null ? data['numActivePoints'] : 0;
      this.numCompletedPoints = data['numCompletedPoints'] != null ? data['numCompletedPoints'] : 0;
      this.numMinutesEstimated = data['numMinutesEstimated'] != null ? data['numMinutesEstimated'] : 0;
      this.percentComplete = data['percentComplete'] != null ? data['percentComplete'] : 0;
      this.taskListSortOrder = data['taskListSortOrder'];

    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['projectTaskListId'] = this.projectTaskListId;
    data['projectId'] = this.projectId;
    data['projectTaskListName'] = this.projectTaskListName;
    data['active'] = this.active;
    data['createdByName'] = this.createdByName;
    data['projectSortOrder'] = this.projectSortOrder;
    data['numTasksWithTimeSpent'] = this.numTasksWithTimeSpent;
    data['numMinutesSpent'] = this.numMinutesSpent;
    data['numTasksWithPoints'] = this.numTasksWithPoints;
    data['numTasksActive'] = this.numTasksActive;
    data['numTasksCompleted'] = this.numTasksCompleted;
    data['numActivePoints'] = this.numActivePoints;
    data['numCompletedPoints'] = this.numCompletedPoints;
    data['numMinutesEstimated'] = this.numMinutesEstimated;
    data['percentComplete'] = this.percentComplete;
    data['taskListSortOrder'] = this.taskListSortOrder;

    return data;
  }
}

// endregion


// region ICreateProjectTaskListDto

export interface ICreateProjectTaskListDto {
  projectTaskListId: number;
  projectId: number;
  projectTaskListName: string;
  active: boolean;
  projectSortOrder: boolean;
  numTasksWithTimeSpent: number;
  numMinutesSpent: number;
  numTasksWithPoints: number;
  numTasksActive: number;
  numTasksCompleted: number;
  numActivePoints: number;
  numCompletedPoints: number;
  numMinutesEstimated: number;
  percentComplete: number;
}

export class CreateProjectTaskListDto implements ICreateProjectTaskListDto {
  projectTaskListId = 0;
  projectId: number;
  projectTaskListName: string;
  active: boolean;
  projectSortOrder: boolean;
  numTasksWithTimeSpent: number;
  numMinutesSpent: number;
  numTasksWithPoints: number;
  numTasksActive: number;
  numTasksCompleted: number;
  numActivePoints: number;
  numCompletedPoints: number;
  numMinutesEstimated: number;
  percentComplete: number;

  static fromJS(data: any): ProjectTaskListDto {
    const result = new ProjectTaskListDto();
    result.init(data);
    return result;
  }

  constructor(data?: ICreateProjectTaskListDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.projectTaskListId = 0;
      this.projectId = data['projectId'];
      this.projectTaskListName = data['projectTaskListName'];
      this.active = (data['active'] == null) ? false : data['active'];
      this.projectSortOrder = data['projectSortOrder'];
      this.numTasksWithTimeSpent = data['numTasksWithTimeSpent'];
      this.numMinutesSpent = data['numMinutesSpent'];
      this.numTasksWithPoints = data['numTasksWithPoints'];
      this.numTasksActive = data['numTasksActive'];
      this.numTasksCompleted = data['numTasksCompleted'];
      this.numActivePoints = data['numActivePoints'];
      this.numCompletedPoints = data['numCompletedPoints'];
      this.numMinutesEstimated = data['numMinutesEstimated'];
      this.percentComplete = data['percentComplete'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['projectTaskListId'] = 0;
    data['projectId'] = this.projectId;
    data['projectTaskListName'] = this.projectTaskListName;
    data['active'] = (this.active == null) ? false : this.active;
    data['projectSortOrder'] = this.projectSortOrder;
    data['numTasksWithTimeSpent'] = this.numTasksWithTimeSpent;
    data['numMinutesSpent'] = this.numMinutesSpent;
    data['numTasksWithPoints'] = this.numTasksWithPoints;
    data['numTasksActive'] = this.numTasksActive;
    data['numTasksCompleted'] = this.numTasksCompleted;
    data['numActivePoints'] = this.numActivePoints;
    data['numCompletedPoints'] = this.numCompletedPoints;
    data['numMinutesEstimated'] = this.numMinutesEstimated;
    data['percentComplete'] = this.percentComplete;

    return data;
  }
}

// endregion

// region IPagedResultDtoOfProjectTaskListDto

export interface IPagedResultDtoOfProjectTaskListDto {
  results: ProjectTaskListDto[];
  count: number;
}

export class PagedResultDtoOfProjectTaskListDto implements IPagedResultDtoOfProjectTaskListDto {
  results: ProjectTaskListDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfProjectTaskListDto {
    const result = new PagedResultDtoOfProjectTaskListDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfProjectTaskListDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(ProjectTaskListDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

export class BulkupdateProjectTaskListDto implements IPagedResultDtoOfProjectTaskListDto {
  results: ProjectTaskListDto[];
  count: number;

  static fromJS(data: any): BulkupdateProjectTaskListDto {

    const result = new BulkupdateProjectTaskListDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfProjectTaskListDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      this.results = [];
      for (const item of data) {
        this.results.push(ProjectTaskListDto.fromJS(item));
      }

    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }

    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion
