import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {
  HttpClient,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';

import {
  CreateProjectTaskListDto,
  PagedResultDtoOfProjectTaskListDto,
  ProjectTaskListDto
} from './projecttasklist.model';

import {CommonService} from '../../../shared/services/common.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class ProjectTaskListService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService,
              private http: HttpClient) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/Projects/api/ProjectTaskList/';
  }

  create(input: CreateProjectTaskListDto,
         successCallback: any,
         errorCallback: any): any {
    const url = this.apiBaseUrl + 'InsertAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  update(input: ProjectTaskListDto,
         successCallback: any,
         errorCallback: any): any {
    const url = this.apiBaseUrl + 'UpdateAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  delete(id: any, successCallback: any,
         errorCallback: any): any {
    const url = this.apiBaseUrl + 'DeleteAsync';
    const params = new HttpParams().set('id', id);

    const options = {
      params: params
    };

    this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
  }

  getAll(isactive: boolean, projectId: number): Observable<PagedResultDtoOfProjectTaskListDto> {
    const url = this.apiBaseUrl + 'GetPagedQuery';

    // Order by Clause
    let orderBy = '';

    orderBy = 'ProjectSortOrder asc';


    let filter = '';
    if (isactive) {
      filter = 'Active eq true';
    }
    if (projectId !== 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += 'ProjectId eq ' + projectId;
    }

    const params = new HttpParams()
      .set('$count', 'true')
      .set('$orderby', orderBy.toString())
      .set('$filter', filter.toString());


    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      const result200 = PagedResultDtoOfProjectTaskListDto.fromJS(response);
      return Observable.of(result200);
    });
  }

  reOrderProjectTaksList(input: ProjectTaskListDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'ReOrderProjectTaskList';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }
}

