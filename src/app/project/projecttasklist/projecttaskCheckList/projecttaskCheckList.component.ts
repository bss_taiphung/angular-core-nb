import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import {ProjectTaskCheckListService} from './service/projecttaskCheckList.service'
import {HubConnection} from '@aspnet/signalr-client';
import {Router} from '@angular/router';
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';

import {filter, forEach, groupBy, includes, orderBy, remove, replace} from 'lodash';

import {ToasterHelperService} from '../../../shared/services/toasterHelper.service';
import {EqmSettingsService} from '../../../shared/services/eqmsettings.service';
import {CommonService} from '../../../shared/services/common.service';
import {
  CreateProjecTaskCheckListDTO,
  ProjecTaskCheckListDTO
} from './service/projecttaskCheckList.model';
import {ProjecTaskDTO} from './../projecttask/service/projecttask.model';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-projectTaskCheckList',
  templateUrl: './projecttaskCheckList.component.html'
  // styleUrls: ["../css/projecttasklist.component.css"],
})
export class ProjectTaskCheckListComponent implements OnInit {

  private projectTaskDto: ProjecTaskDTO;
  private taskName: string;
  private isAddTaskShow = false;
  private isDragTask = false;
  private isDragAddTask = false;
  private isTaskInDragMode = false;
  private taskDragSourceId = 0;
  private addNewCheckListName: any;
  private showCheckList = false;
  private userPermissionModel: string;
  private taskTagList: any[] = [];
  private assignUserList: any[] = [];
  private taskDueDate: string;
  TotalTaskTime: string;
  private taskCheckList: any = [];
  @Input() public isView = false;
  @Output() openProjectTaskDetails = new EventEmitter<any>();

  @Input()
  public set taskInfo(projecTaskDTO: any) {
    if (projecTaskDTO !== undefined) {
      this.projectTaskDto = projecTaskDTO;
      this.assignUserList = [];
      this.taskTagList = [];
      this.assignUserList = this.projectTaskDto.assignedTo;
      this.taskTagList = this.projectTaskDto.tags;
      this.TotalTaskTime = this.setTotalTaskTime();
      this.projectTaskDto.numChecklistItemsCompleted =
        (this.projectTaskDto.numChecklistItemsCompleted === null) ? 0 : this.projectTaskDto.numChecklistItemsCompleted;
      this.projectTaskDto.numChecklistItems =
        (this.projectTaskDto.numChecklistItems === null) ? 0 : this.projectTaskDto.numChecklistItems;

      this.taskCheckList = this.projectTaskDto.checkLists;
      this.setProjectCheckListData();
    }
  }

  @Input()
  public set userModeInfo(userPermissionModel: any) {
    this.userPermissionModel = userPermissionModel;
  }

  private hubConnection: HubConnection;

  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  public checklistForm: FormGroup = new FormGroup({
    'checklistText': new FormControl('', Validators.required),
    'projectTaskId': new FormControl(),
    'active': new FormControl(false),
    'completed': new FormControl(false),
  });

  constructor(private projectTaskCheckListService: ProjectTaskCheckListService,
              private router: Router,
              private commonService: CommonService,
              private toasterService: ToasterHelperService,
              private eqmSettingsService: EqmSettingsService) {
  }

  ngOnInit() {
    this.signalRConnection();
  }

  signalRConnection() {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');

        this.hubConnection
          .invoke('Subscribe', 'Projects', 'ProjectTaskChecklist', 'ProjectTaskId', this.projectTaskDto.projectTaskId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
      const _responseText = messageJson;
      let result200: any = null;
      const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      const resultData200 = this.commonService.toCamel(data);
      result200 = resultData200 ? ProjecTaskCheckListDTO.fromJS(resultData200) : new ProjecTaskCheckListDTO();
      const dataExist = this.containsObject(result200, this.taskCheckList);

      if (operation.toLowerCase() === 'insert' && !dataExist) {
        this.taskCheckList.push(result200);
      }

      if (operation.toLowerCase() === 'update') {
        this.taskCheckList.forEach((element, index) => {
          if (element.projectTaskChecklistId === result200.projectTaskChecklistId) {
            this.taskCheckList[index] = result200;
          }
        });
      }

      if (operation.toLowerCase() === 'delete' && dataExist) {
        let index = null;
        this.taskCheckList.forEach((element, i) => {
          if (element.projectTaskId === result200.projectTaskId) {
            index = i;
          }
        });
        if (index !== null) {
          this.taskCheckList.splice(index, 1);
        }
      }
      this.setProjectCheckListData();
    });
  }

  containsObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].projectTaskChecklistId === obj.projectTaskChecklistId) {
        return true;
      }
    }

    return false;
  }

  private setTotalTaskTime(actualMinutes = 0): string {
    let returnValue = '';
    if (this.projectTaskDto.actualMinutes > 0) {
      const totalMinutes = this.projectTaskDto.actualMinutes;
      if (totalMinutes > 0) {
        if (totalMinutes > 59) {
          const totalHours = Math.floor(totalMinutes / 60);
          returnValue = totalHours + 'h ' + (totalMinutes - (totalHours * 60)) + 'm';
        } else {
          returnValue = '00h ' + totalMinutes + 'm';
        }
      }
    } else {
      returnValue = '00h 00m';
    }
    return returnValue;
  }

  getTasksCheckList() {
    this.projectTaskCheckListService.getAll(this.projectTaskDto.projectTaskId).subscribe(eventResult => {
      this.taskCheckList = eventResult.results;
      this.setProjectCheckListData();
      if (eventResult.results.length > 0) {
        this.showCheckList = true;
      }
      else {
        this.showCheckList = false;
      }
    });
  }

  private assignCheckListData: any = [];
  private numChecklistItemsCompletedCounter = 0;
  private totalChecklistItemsCounter = 0;

  setProjectCheckListData() {
    let tempdata: any = [];
    this.numChecklistItemsCompletedCounter = (filter(this.taskCheckList, (f) => {
      return f.completed
    })).length;
    this.totalChecklistItemsCounter = this.taskCheckList.length;
    tempdata = (filter(this.taskCheckList, (f) => {
      return !f.completed
    }));
    if (tempdata.length > 3) {
      this.assignCheckListData = [];
      for (let i = 0; i < 3; i++) {
        this.assignCheckListData.push(tempdata[i]);
      }
    } else {
      this.assignCheckListData = tempdata;
    }
  }

  private checkUserPermission(action): boolean {

    if (this.userPermissionModel === 'FULL') {
      return true;
    }
    else if (this.userPermissionModel === 'LIMITED') {
      if (action === 'complete' || action === 'add' || action === 'edit' || action === 'delete') {
        return true;
      }
      else {
        return false;
      }
    }
    else if (this.userPermissionModel === 'RESTRICTED') {
      return false;
    } else {
      return false;
    }
  }

  public saveChecklistHandler(event, value, projectTaskId) {
    if (event.keyCode === 13) {
      if (value !== null) {
        if (this.checklistForm.valid) {
          this.checklistForm.controls['projectTaskId'].setValue(this.projectTaskDto.projectTaskId);
          this.saveHandler(this.checklistForm.value, false);
        }
      }
    }
  }

  public updateChecklistHandler(event, value, checklistData) {
    if (event.keyCode === 13) {
      if (value !== null) {
        const updateCheckListData: CreateProjecTaskCheckListDTO = checklistData;
        updateCheckListData.checklistText = value;
        this.saveHandler(updateCheckListData, true);
      }
    }
  }

  public completedCheckListHandler(event, value, checklistData) {
    if (!this.checkUserPermission('complete')) {
      return false;
    }
    this.setProjectCheckListData();
    const updateCheckListData: CreateProjecTaskCheckListDTO = checklistData;
    updateCheckListData.completed = value;
    updateCheckListData.completedByUserId = this.eqmSettingsService.getUser().userId;
    updateCheckListData.completedDateTime = new Date();
    this.taskCheckList.forEach((element, index) => {
      if (element.projectTaskChecklistId === updateCheckListData.projectTaskChecklistId) {
        this.taskCheckList[index] = updateCheckListData;
      }
    });
    this.setProjectCheckListData();
    this.saveHandler(updateCheckListData, true);
  }

  public saveHandler(checklistData, isUpdate) {
    this.checklistForm.reset();
    if (isUpdate) {
      if (!this.checkUserPermission('edit')) {
        return false;
      }
      this.projectTaskCheckListService.update(checklistData,
        response => {
          this.toasterService.success('', 'Check List Updated Successfully');
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    } else {
      if (!this.checkUserPermission('add')) {
        return false;
      }
      const inputCheckListData: CreateProjecTaskCheckListDTO = checklistData;
      inputCheckListData.completed = false;
      this.projectTaskCheckListService.create(inputCheckListData,
        response => {

          this.toasterService.success('', 'New Check List Saved Successfully');
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  public deleteCheckListHandler(checklistData) {
    this.deleteHandler(checklistData);
  }

  public deleteHandler(checklistData) {
    if (!this.checkUserPermission('delete')) {
      return false;
    }
    this.projectTaskCheckListService.delete(checklistData.projectTaskChecklistId,
      response => {
        this.toasterService.success('', 'Check List Deleted Successfully');
      },
      error => {
        this.toasterService.errorMessage(error);
      }
    );
  }


  openProjectTaskDetailsFrom() {
    this.openProjectTaskDetails.emit(this.projectTaskDto);
  }

  updateUrl(data: any) {
    data.UserAvatar = 'assets/img/no_avatar.png';
  }

  public month = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'November', 'Dec'];

  setDueDateText(dueDate: any): string {
    function calcDate(date1, date2) {
      const diff = Math.floor(date1.getTime() - date2.getTime());
      const day = 1000 * 60 * 60 * 24;

      const days = Math.floor(diff / day);
      const months = Math.floor(days / 31);
      const years = Math.floor(months / 12);

      let message = ' Overdue by ';
      message += days + ' days ';
      message += months !== 0 ? months + ' months ' : '';
      message += years !== 0 ? years + ' years ago' : '';
      return message;
    }

    let returndate = '';
    if (this.projectTaskDto.dueDate !== null) {
      const dueDate = new Date(this.projectTaskDto.dueDate.toString());
      const todayDate = new Date();
      if (dueDate.getDate() === todayDate.getDate()
        && dueDate.getMonth() === todayDate.getMonth()
        && dueDate.getFullYear() === todayDate.getFullYear()) {
        returndate = 'Today, ' + this.month[dueDate.getMonth()] + ' ' + dueDate.getDate().toString();
      }
      else if (dueDate > todayDate) {
        // let dueDate = new Date(this.projectTaskDto.dueDate.toString());
        returndate = this.month[dueDate.getMonth()] + ' ' + dueDate.getDate().toString();
      }
      else {
        returndate = this.month[dueDate.getMonth()] + ' ' + dueDate.getDate().toString(); // calcDate(todayDate, dueDate);
      }
    } else {
      returndate = '';
    }
    return returndate;
  }

  setDueDateClass(dueDate: any): string {
    let returndate = '';
    if (this.projectTaskDto.dueDate !== null) {
      const dueDate = new Date(this.projectTaskDto.dueDate.toString());
      const todayDate = new Date();
      if (dueDate.getDate() === todayDate.getDate()
        && dueDate.getMonth() === todayDate.getMonth()
        && dueDate.getFullYear() === todayDate.getFullYear()) {
        returndate = 'txtColorblue';
      }
      else if (dueDate > todayDate) {
        returndate = 'txtColorblue';
      }
      else {
        returndate = 'txtColorRed';
      }
    } else {
      returndate = 'txtColorblue';
    }
    return returndate;
  }

  setCompletedDate(): string {
    let returndate = '';
    if (this.projectTaskDto.completedDateTime !== null) {
      const dueDate = new Date(this.projectTaskDto.completedDateTime.toString());
      returndate = 'Completed on ' + this.month[dueDate.getMonth()] + ' ' + dueDate.getDate().toString();
    }
    return returndate;
  }
}
