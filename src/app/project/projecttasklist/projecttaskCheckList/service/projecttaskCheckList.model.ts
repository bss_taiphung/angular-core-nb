// region IProjectaskCheckListDTO

export interface IProjectaskCheckListDTO {
  projectTaskChecklistId: number;
  projectTaskId: number;
  checklistText: string;
  completed: boolean;
  completedDateTime: Date;
  completedByUserId: number;
  assignedToUserId: number;
}

export class ProjecTaskCheckListDTO implements IProjectaskCheckListDTO {
  projectTaskChecklistId: number;
  projectTaskId: number;
  checklistText: string;
  completed: boolean;
  completedDateTime: Date;
  completedByUserId: number;
  assignedToUserId: number;

  static fromJS(data: any): ProjecTaskCheckListDTO {
    const result = new ProjecTaskCheckListDTO();
    result.init(data);
    return result;
  }

  static fromJSarrya(data: any): ProjecTaskCheckListDTO[] {
    const results = [];
    if (data !== null && data !== undefined) {
      data = JSON.parse(data);
      for (const item of data) {
        const obj = new ProjecTaskCheckListDTO();
        obj.init(item);
        results.push(obj);
      }
    }
    return results;
  }

  constructor(data?: IProjectaskCheckListDTO) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      data = this.toCamel(data);
      this.projectTaskChecklistId = data['projectTaskChecklistId'];
      this.projectTaskId = data['projectTaskId'];
      this.checklistText = data['checklistText'];
      this.completed = data['completed'];
      this.completedDateTime = data['completedDateTime'];
      this.completedByUserId = data['completedByUserId'];
      this.assignedToUserId = data['assignedToUserId'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['projectTaskChecklistId'] = this.projectTaskChecklistId;
    data['projectTaskId'] = this.projectTaskId;
    data['checklistText'] = this.checklistText;
    data['completed'] = this.completed;
    data['completedDateTime'] = this.completedDateTime;
    data['completedByUserId'] = this.completedByUserId;
    data['assignedToUserId'] = this.assignedToUserId;

    return data;
  }

  toCamel(o) {
    let newO, origKey, newKey, value
    if (o instanceof Array) {
      newO = []
      for (origKey in o) {
        value = o[origKey]
        if (typeof value === 'object') {
          value = this.toCamel(value)
        }
        newO.push(value)
      }
    } else {
      newO = {}
      for (origKey in o) {
        if (o.hasOwnProperty(origKey)) {
          newKey = (origKey.charAt(0).toLowerCase() + origKey.slice(1) || origKey).toString()
          value = o[origKey]
          if (value instanceof Array || (value !== null && value.constructor === Object)) {
            value = this.toCamel(value)
          }
          newO[newKey] = value
        }
      }
    }
    return newO
  }
}

// endregion

// region IPagedResultDtoOfProjecTaskCheckListDTO
export interface IPagedResultDtoOfProjecTaskCheckListDTO {
  results: ProjecTaskCheckListDTO[];
  count: number;
}

export class PagedResultDtoOfProjecTaskCheckListDTO implements IPagedResultDtoOfProjecTaskCheckListDTO {
  results: ProjecTaskCheckListDTO[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfProjecTaskCheckListDTO {
    const result = new PagedResultDtoOfProjecTaskCheckListDTO();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfProjecTaskCheckListDTO) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(ProjecTaskCheckListDTO.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion

// region ICreateProjecTaskCheckListDTO

export interface ICreateProjecTaskCheckListDTO {
  projectTaskChecklistId: number;
  projectTaskId: number;
  checklistText: string;
  completed: boolean;
  completedDateTime: Date;
  completedByUserId: number;
  assignedToUserId: number;
  active: boolean;
}

export class CreateProjecTaskCheckListDTO implements ICreateProjecTaskCheckListDTO {
  projectTaskChecklistId: number;
  projectTaskId: number;
  checklistText: string;
  completed: boolean;
  completedDateTime: Date;
  completedByUserId: number;
  assignedToUserId: number;
  active: boolean;

  static fromJS(data: any): CreateProjecTaskCheckListDTO {
    const result = new CreateProjecTaskCheckListDTO();
    result.init(data);
    return result;
  }

  constructor(data?: ICreateProjecTaskCheckListDTO) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.projectTaskChecklistId = data['projectTaskChecklistId'];
      this.projectTaskId = data['projectTaskId'];
      this.checklistText = data['checklistText'];
      this.completed = data['completed'];
      this.completedDateTime = data['completedDateTime'];
      this.completedByUserId = data['completedByUserId'];
      this.assignedToUserId = data['assignedToUserId'];
      this.active = data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['projectTaskChecklistId'] = this.projectTaskChecklistId;
    data['projectTaskId'] = this.projectTaskId;
    data['checklistText'] = this.checklistText;
    data['completed'] = this.completed;
    data['completedDateTime'] = this.completedDateTime;
    data['completedByUserId'] = this.completedByUserId;
    data['assignedToUserId'] = this.assignedToUserId;
    data['active'] = this.active;
    return data;
  }

  toCamel(o) {
    let newO, origKey, newKey, value;
    if (o instanceof Array) {
      newO = []
      for (origKey in o) {
        value = o[origKey]
        if (typeof value === 'object') {
          value = this.toCamel(value)
        }
        newO.push(value)
      }
    } else {
      newO = {}
      for (origKey in o) {
        if (o.hasOwnProperty(origKey)) {
          newKey = (origKey.charAt(0).toLowerCase() + origKey.slice(1) || origKey).toString()
          value = o[origKey]
          if (value instanceof Array || (value !== null && value.constructor === Object)) {
            value = this.toCamel(value)
          }
          newO[newKey] = value
        }
      }
    }
    return newO
  }
}

// endregion
