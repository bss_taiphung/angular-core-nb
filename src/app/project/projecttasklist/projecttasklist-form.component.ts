import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';
import {ProjectTaskListDto} from './service/projecttasklist.model';
import {CommonService} from '../../shared/services/common.service';
import {SelectListDto} from '../../shared/models/selectListDto'

@Component({
  selector: 'kendo-grid-projecttasklist-form',
  templateUrl: './html/projecttasklist-form.component.html'
})
export class ProjectTaskListFormComponent {
  public active = false;
  public editForm: FormGroup = new FormGroup({
    'projectTaskListId': new FormControl(),
    'projectId': new FormControl('', Validators.required),
    'projectTaskListName': new FormControl('', Validators.required),
    'active': new FormControl(false),
  });

  constructor(private commonService: CommonService) {

  }

  public projectIdList: SelectListDto[];
  public filterprojectIdList: SelectListDto[];

  @Input() public isNew = false;

  @Input()
  public set model(projectTaskList: ProjectTaskListDto) {
    // console.log(failureType);
    this.editForm.reset(projectTaskList);
    this.active = projectTaskList !== undefined;
    if (this.active) {


      this.commonService.getAll('Projects', 'Project').subscribe(eventResult => {
        this.filterprojectIdList = this.projectIdList = eventResult.results;
      });
    }
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<ProjectTaskListDto> = new EventEmitter();

  public onSave(e): void {

    e.preventDefault();
    if (this.active == null) {
      this.active = false;
    }
    this.save.emit(this.editForm.value);
    this.active = false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.active = false;
    this.cancel.emit();
  }

  private projectIdListHandleFilter(value) {
    this.filterprojectIdList = this.projectIdList
      .filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
}
