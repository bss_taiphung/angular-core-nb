import {
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import {Router} from '@angular/router';

import {
  filter,
  forEach,
  groupBy,
  includes,
  orderBy,
  sremove,
  replace
} from 'lodash';

import {ProjectTaskService} from './service/projecttask.service'
import {CommonService} from '../../../shared/services/common.service';
import {ProjecTaskDTO} from './service/projecttask.model';

@Component({
  selector: 'app-projectTaskCompleted',
  templateUrl: './projecttaskcompleted.component.html',
  styles: [` :host >>> .k-dialog-close { display: none; } `],
})
export class ProjectTaskCompletedComponent {


  private projecTaskDTO: ProjecTaskDTO = new ProjecTaskDTO();
  public taskWorkFlowOption: any = [];
  public taskWorkFlowResult = '';
  @Input() public isCompleteTask = false;

  @Input()
  public set projectTask(projectTaskDetails: any) {
    if (projectTaskDetails !== undefined) {
      this.projecTaskDTO = projectTaskDetails;
      if (this.projecTaskDTO.workflowOptions != null && this.projecTaskDTO.active) {
        this.taskWorkFlowOption = this.projecTaskDTO.workflowOptions;
        this.taskWorkFlowResult = this.projecTaskDTO.workflowOptions[0];
        this.isCompleteTask = true;
      } else {
        // this.onApprrovedOrRejected.emit(true);
      }
    }
  }

  @Output()
  onApprrovedOrRejected = new EventEmitter<any>();


  constructor(private projectTaskService: ProjectTaskService,
              private router: Router,
              private commonService: CommonService) {
  }


  public completedTaskHandler(event, value, taskData) {
    this.projecTaskDTO = taskData;
  }

  public completeTaskAction(status) {
    if (status) {
      this.onApprrovedOrRejected.emit({'status': true, 'workFlowResult': this.taskWorkFlowResult});
    }
    else {
      this.onApprrovedOrRejected.emit({'status': false, 'workFlowResult': this.taskWorkFlowResult});
    }
    this.isCompleteTask = false;
  }
}
