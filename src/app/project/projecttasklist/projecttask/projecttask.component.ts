import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  Renderer,
  ViewChild
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import {Router} from '@angular/router';
import {HubConnection} from '@aspnet/signalr-client';

import {
  filter,
  forEach,
  groupBy,
  includes,
  orderBy,
  remove,
  replace
} from 'lodash';

import {ToasterHelperService} from '../../../shared/services/toasterHelper.service';

import {EqmSettingsService} from '../../../shared/services/eqmsettings.service';
import {CommonService} from '../../../shared/services/common.service';
import {BulkupdateProjecTaskDto, ProjecTaskDTO} from './service/projecttask.model';
import {ProjectTaskListDto} from './../service/projecttasklist.model';
import {ProjectTaskListService} from './../service/projecttasklist.service'
import {ProjectDto} from '../../projectslist/service/projectslist.model';
import {TagMasterService} from '../../tagmaster/service/tagmaster.service';
import {ProjectTaskService} from './service/projecttask.service'
import {ProjectSettingsService} from '../../shared/services/projectsettings.service';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-projectTask',
  templateUrl: './projecttask.component.html',
  styleUrls: ['../css/projecttasklist.component.css'],
})
export class ProjectTaskComponent implements AfterViewInit {

  projectTaskListDto: ProjectTaskListDto;
  checkListDataUpdateFlag = false;
  private taskName: string;
  isAddTaskShow = false;
  isDragTask = false;
  isDragAddTask = false;
  isTaskInDragMode = false;
  private taskDragSourceId = 0;
  isTaskDetailOpen = false;
  OpenTaskListDeleteDialog: any = false;
  userPermissionModel: string;
  isDataLoaded = false;
  isShowTaskComplete = false;
  project: ProjectDto;
  isCompleteTask = false;
  newFunctionalities = '';
  projectTaskNameFocus = false;


  public menus = [
    {label: 'View', action: 'callView', active: true},
    {label: 'Delete', action: 'deleteItem', active: true},
    {label: 'Send', action: 'sendItem', active: false}
  ];

  @Input()
  public set selectedProject(projectListDto: any) {
    if (projectListDto !== undefined) {
      this.project = projectListDto;
    }
  }

  projectUserAssignedToListDto: any;

  @Input()
  public set projectUserAssignedToList(userList: any) {
    if (userList !== undefined) {
      this.projectUserAssignedToListDto = this.selectListDto = userList;
    }
  }

  projectTagListDto: any;

  @Input()
  public set projectTagList(tagListDto: any) {
    if (tagListDto !== undefined) {
      this.projectTagListDto = this.selectedprojectTagListDto = tagListDto;
    }
  }

  public tagHandleFilter(filter: any): void {
    this.selectedprojectTagListDto = this.projectTagListDto
      .filter((s) => s.tagName.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  public userHandleFilter(filter: any): void {
    this.selectListDto = this.projectUserAssignedToListDto
      .filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  @Input()
  public set taskListInfo(projectTaskListDto: any) {
    if (projectTaskListDto !== undefined) {
      this.projectTaskListDto = projectTaskListDto;
      this.projectTaskListDto.numTasksWithPoints =
        this.projectTaskListDto.numTasksWithPoints === null ? 0 : this.projectTaskListDto.numTasksWithPoints;
      this.projectTaskListDto.numActivePoints =
        this.projectTaskListDto.numActivePoints === null ? 0 : this.projectTaskListDto.numActivePoints;
      this.getTasks();
      this.signalRConnectionForProjectTask();
      setTimeout(() => {
        this.signalRConnectionForProjectTaskList();
      }, 3000);

    }
  }

  isProjectTaskUpdate = false;

  @Input()
  public set projectTaskUpdate(flag: any) {
    if (flag !== undefined) {
      this.isProjectTaskUpdate = flag;
    }
  }

  @Input()
  public set userModeInfo(userPermissionModel: any) {
    this.userPermissionModel = userPermissionModel;
  }

  @Output()
  deleteTaskList: EventEmitter<any> = new EventEmitter();

  @Output()
  taskDragHandler: EventEmitter<any> = new EventEmitter();

  @Output()
  isdragChild: EventEmitter<any> = new EventEmitter();

  private taskList: any = [];
  private bulkUpdateReorderFlag = false;
  private hubConnectionProjectTask: HubConnection;
  private hubConnectionProjectTaskList: HubConnection;
  projectTaskDto: ProjecTaskDTO;
  private projectTaskCompleteDto: ProjecTaskDTO;

  constructor(private projectTaskService: ProjectTaskService,
              private router: Router,
              private commonService: CommonService,
              private projectTaskListService: ProjectTaskListService,
              private toasterService: ToasterHelperService,
              private eqmSettingsService: EqmSettingsService,
              private tagMasterService: TagMasterService,
              private projectSettingsService: ProjectSettingsService,
              private renderer: Renderer) {
    this.isDataLoaded = false;
  }

  @ViewChild('projectTasknameFocus') projectTasknameFocus: ElementRef;

  ngAfterViewInit() {

  }

  public taskForm: FormGroup = new FormGroup({
    'projectTaskId': new FormControl(),
    'projectTaskListId': new FormControl(''),
    'projectTaskName': new FormControl('', Validators.required),
    'assignedTo': new FormControl(''),
    'tags': new FormControl(''),
    'dueDate': new FormControl(''),
    'active': new FormControl(false),
  });

  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  selectListDto: any;
  selectedprojectTagListDto: any;

  signalRConnectionForProjectTask() {

    function containsObject(obj, list) {
      let x;
      for (x in list) {
        if (list.hasOwnProperty(x) && list[x].projectTaskId === obj.projectTaskId) {
          return true;
        }
      }
      return false;
    }

    this.hubConnectionProjectTask =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnectionProjectTask
      .start()
      .then(() => {
        console.log('Connection started! ' + this.projectTaskListDto.projectTaskListId);

        this.hubConnectionProjectTask
          .invoke('Subscribe', 'Projects', 'ProjectTask', 'ProjectTaskListId', this.projectTaskListDto.projectTaskListId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnectionProjectTask.on('clientOnEntityUpdated',
      (groupName: string, operation: string, messageJson: string) => {
        const _responseText = messageJson;
        let result200: any = null;
        const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
        const resultData200 = this.commonService.toCamel(data);
        if (operation.toLowerCase() === 'bulkupdate') {
          result200 = resultData200 ? BulkupdateProjecTaskDto.fromJS(resultData200).results : new ProjecTaskDTO();
          result200.forEach(item => {
            item.assignedTo.forEach(user => {
              user.userAvatar = this.commonService.getUserThumbnail(user.userId);
            });
          });
        } else {
          result200 = resultData200 ? ProjecTaskDTO.fromJS(resultData200) : new ProjecTaskDTO();
          result200.assignedTo.forEach(user => {
            user.userAvatar = this.commonService.getUserThumbnail(user.userId);
          });
        }

        const dataExist = containsObject(result200, this.taskList);
        if (operation.toLowerCase() === 'insert' && !dataExist) {
          this.taskList.push(result200);
          this.setCompletedDataTask();
        }
        if (operation.toLowerCase() === 'update') {
          this.taskList.forEach((element, index) => {
            if (element.projectTaskId === result200.projectTaskId) {
              this.taskList[index] = result200;
              this.setCompletedDataTask();
            }
          });
        }
        else if (operation.toLowerCase() === 'bulkupdate') {
          // if (!this.bulkUpdateReorderFlag)
          {
            result200.forEach(result => {
              this.taskList.forEach((element, index) => {
                if (element.projectTaskId === result.projectTaskId) {
                  this.taskList[index] = result;
                }
              });
            });
            this.setCompletedDataTask();
          }

        }

        if (operation.toLowerCase() === 'delete' && dataExist) {
          let index = null;
          this.taskList.forEach((element, i) => {
            if (element.projectTaskId === result200.projectTaskId) {
              index = i;
            }
          });
          if (index !== null) {
            this.taskList.splice(index, 1);
            this.setCompletedDataTask();
          }
        }
      });
  }


  signalRConnectionForProjectTaskList() {
    this.hubConnectionProjectTaskList =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnectionProjectTaskList
      .start()
      .then(() => {
        console.log('Connection started!');

        this.hubConnectionProjectTaskList
          .invoke('Subscribe', 'Projects', 'ProjectTaskList', 'ProjectId', this.project.projectId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnectionProjectTaskList.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
      const _responseText = messageJson;
      let result200: any = null;
      const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      const resultData200 = this.commonService.toCamel(data);
      if (operation.toLowerCase() === 'update') {
        result200 = resultData200 ? ProjectTaskListDto.fromJS(resultData200) : new ProjectTaskListDto();
        if (this.projectTaskListDto.projectTaskListId === result200.projectTaskListId) {
          this.projectTaskListDto = result200;
        }
      }
    });

  }

  getHoursMins(timeInMin: number) {
    let result;
    if (timeInMin) {
      const realmin = timeInMin % 60
      const hours = Math.floor(timeInMin / 60);
      result = hours + 'h ' + realmin + 'min';
    } else {
      result = 'not added';
    }
    return result;
  }

  getTasks() {
    this.isDataLoaded = false;
    this.projectTaskService.getAll(this.projectTaskListDto.projectTaskListId).subscribe(eventResult => {
      this.taskList = orderBy(eventResult.results, ['taskListSortOrder'], ['asc']);
      this.taskList.forEach(item => {
        item.assignedTo.forEach(user => {
          user.userAvatar = this.commonService.getUserThumbnail(user.userId);
        });
      });
      this.taskList.forEach(item => {
        item.followers.forEach(user => {
          user.userAvatar = this.commonService.getUserThumbnail(user.userId);
        });
      });
      this.setCompletedDataTask();
    });
  }

  numTaskCompletedCounter = 0;
  activeData: any = [];
  unCompleteTaskData: any = [];

  setCompletedDataTask() {
    this.taskList = orderBy(this.taskList, ['taskListSortOrder'], ['asc']);
    this.numTaskCompletedCounter = (filter(this.taskList, (f) => {
      return f.active
    })).length;
    this.activeData = (filter(this.taskList, (f) => {
      return f.active
    }));
    this.unCompleteTaskData = (filter(this.taskList, (f) => {
      return !f.active
    }));
    this.isDataLoaded = true;
  }

  checkUserPermission(action): boolean {
    return this.commonService.checkUserPermission(this.userPermissionModel, action);
  }

  private checkIsRestrictedUser(projectTask): boolean {
    if (this.userPermissionModel === 'RESTRICTED') {
      return this.checkUserTaskViewPermission(projectTask);
    }
    return true;
  }

  private checkUserTaskViewPermission(projectTask): boolean {

    if (projectTask.assignUserList != null) {
      const assignUserList = JSON.parse(projectTask.assignUserList);
      const currentUserId = this.eqmSettingsService.getUser().userId;

      for (let i = 0; i < assignUserList.length; i++) {
        if (assignUserList[i].UserId == currentUserId) {
          return true;
        }
      }
    }
    return false;
  }


  public AddNewTask() {

    if (!this.checkUserPermission('add')) {
      return;
    }

    if (this.taskForm.valid) {
      this.isAddTaskShow = false;
      this.taskForm.controls['projectTaskListId'].setValue(this.projectTaskListDto.projectTaskListId);
      this.saveHandler(this.taskForm.value, false);
      this.taskForm.reset();
    }


  }

  addNewTaskkeyPress(e) {
    const code = (e.keyCode ? e.keyCode : e.which);
    if (code === 13) { //Enter keycode
      this.AddNewTask();
    }
  }

  projecTaskDTO: ProjecTaskDTO = undefined;
  taskWorkFlowOption: any = [];
  taskWorkFlowResult = '';

  public completedTaskHandler(event, value, taskData) {
    if (!this.checkUserPermission('complete')) {
      return;
    }
    this.projecTaskDTO = taskData;
    if (this.projecTaskDTO.workflowOptions != null && this.projecTaskDTO.active) {
      this.isCompleteTask = true;
    } else {
      this.completeTaskAction({'status': true, 'workFlowResult': null});
    }
  }

  public completeTaskAction(result) {
    if (result.status) {
      if (this.projecTaskDTO.active) {
        this.projecTaskDTO.workflowResult = result.workFlowResult;
        this.projecTaskDTO.completedByUserId = this.eqmSettingsService.getUser().userId;
        this.projecTaskDTO.completedDateTime = new Date();
      } else {
        this.projecTaskDTO.workflowResult = null;
        //this.projecTaskDTO.active = !this.projecTaskDTO.active;
        this.projecTaskDTO.completedByUserId = null;
        this.projecTaskDTO.completedDateTime = null;
      }
      this.taskList.forEach((element, index) => {
        if (element.projectTaskId === this.projecTaskDTO.projectTaskId) {
          this.taskList[index] = this.projecTaskDTO;
          this.setCompletedDataTask();
        }
      });
      this.saveHandler(this.projecTaskDTO, true);
      this.isDataLoaded = true;
    } else {
      this.projecTaskDTO.active = false;
    }
    this.isCompleteTask = false;
    this.projecTaskDTO = undefined;
  }

  public saveHandler(projecttasklistdata, isUpdate) {
    this.isDataLoaded = false;
    const inputProjectTaskData: ProjecTaskDTO = projecttasklistdata;
    if (isUpdate) {
      this.projectTaskService.update(inputProjectTaskData,
        response => {
          this.projecTaskDTO = undefined;
          this.isDataLoaded = true;
          this.toasterService.success('', 'Project Task List Saved Successfully');
        },
        error => {
          this.isDataLoaded = true;
          this.toasterService.errorMessage(error);
        }
      );
    } else {
      inputProjectTaskData.active = false;
      this.projectTaskService.create(inputProjectTaskData,
        response => {
          this.isAddTaskShow = true;
          this.projecTaskDTO = undefined;
          this.isDataLoaded = true;
          this.toasterService.success('', 'Project Task List Saved Successfully');
        },
        error => {
          this.isDataLoaded = true;
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  public focusSettingEventEmitter = new EventEmitter<boolean>();

  public addTaskToggale(projectTaskListId) {
    this.isAddTaskShow = !this.isAddTaskShow;
    this.projectTaskNameFocus = true;
    this.projecTaskDTO = new ProjecTaskDTO();
    this.taskForm.reset();

  }

  public closeTaskForm() {
    this.isAddTaskShow = false;
    this.taskForm.controls['projectTaskName'].setValue('');
    this.taskForm.controls['assignedTo'].setValue('');
    this.taskForm.controls['tags'].setValue('');
    this.newFunctionalities = '';
  }

  originalTaskList: any;

  timeout = null;

  /*Drage & Drop Functionality for tasks*/
  public onTaskDragEnd(taskList: any, event: any): void {
    //  console.log('onTaskDragEnd' + taskList + " List name");
    this.isdragChild.emit(true);
    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      this.updateReordering(taskList);
    }, 4000);
  }

  updateReordering(taskList: any) {
    this.projectTaskService.reOrderAllTasks(taskList,
      response => {
        this.isDragTask = true;
        this.bulkUpdateReorderFlag = true;
      },
      error => {
        this.toasterService.errorMessage(error);
      }
    );
  }

  public onTaskAdd(taskList: any, event: any): void {
    event.dataItem.projectTaskListId = this.projectTaskListDto.projectTaskListId;
    this.taskDragHandler.emit({dataItem: event.dataItem, Action: 'add'});
  }

  public onTaskRemove(ProjectTask: any, event: any): void {
    event.dataItem.projectTaskListId = this.projectTaskListDto.projectTaskListId;
    this.taskDragHandler.emit({dataItem: event.dataItem, Action: 'remove'});
  }

  public onTaskDragOver(ProjectTask: any, event: any): void {
    this.isTaskInDragMode = true;
    this.isTaskInDragMode = false;
  }

  public onTaskDragStart(ProjectTaskList: any, event: any): void {
    if (!this.checkUserPermission('edit')) {
      return;
    }
    this.isTaskInDragMode = true;
    this.taskDragSourceId = ProjectTaskList[event.index].projectTaskId;
  }

  opened = false;
  deleteTaskItem: any;

  public deleteTaskhandler(task: any) {
    this.deleteTaskItem = task;
    this.opened = true;
  }

  public deleteTask(action) {
    this.opened = false;
    if (action) {
      this.isDataLoaded = false;
      this.projectTaskService.delete(this.deleteTaskItem.projectTaskId,
        response => {
          this.toasterService.success('', 'Project Task Deleted Successfully');
          this.deleteTaskItem = undefined;
          this.isDataLoaded = true;

        },
        error => {
          this.isDataLoaded = true;
          this.toasterService.errorMessage(error);
        }
      );
    } else {
      this.opened = false;
      this.deleteTaskItem = undefined;
    }

  }

  public duplicateTaskhandler(task: any) {
    this.opened = false;
    this.projectTaskService.duplicateTask(task.projectTaskId,
      response => {
        this.toasterService.success('', 'Project Task Added Successfully');
        this.deleteTaskItem = undefined;

      },
      error => {
        this.toasterService.errorMessage(error);
      }
    );
  }

  public movetoTophandlerhandler(task: any) {
    const tempList: any = [];
    let sortOrder = 0;
    task.taskListSortOrder = sortOrder++;
    tempList.push(task);
    this.unCompleteTaskData.forEach(item => {
      if (item.projectTaskId !== task.projectTaskId) {
        item.taskListSortOrder = sortOrder++;
        tempList.push(item);
      }
    });
    this.unCompleteTaskData = tempList;
    this.onTaskDragEnd(tempList, null);
  }

  public movetoBottomTaskhandler(task: any) {
    const tempList: any = [];
    let sortOrder = 0;
    this.unCompleteTaskData.forEach(item => {
      if (item.projectTaskId !== task.projectTaskId) {
        item.taskListSortOrder = sortOrder++;
        tempList.push(item);
      }
    });
    task.taskListSortOrder = sortOrder++;
    tempList.push(task);
    this.unCompleteTaskData = tempList;
    this.onTaskDragEnd(tempList, null);
  }

  public toggleClass(id, task) {
    const elements = document.getElementsByClassName('projectMainTasks');
    for (let i = 0; i < elements.length; i++) {
      const selectedTaskId = elements[i].firstElementChild.id;
      if (id.toString() === selectedTaskId.split('_')[1].toString()) {
        elements[i].firstElementChild.classList.add('taskclass');
        // elements[i].firstElementChild.firstElementChild.classList.add('mainTaskup');
        elements[i].firstElementChild.getElementsByTagName('label')[0].classList.add('mainTaskup');
      } else {
        elements[i].firstElementChild.classList.remove('taskclass');
        // elements[i].firstElementChild.firstElementChild.classList.remove("mainTaskup");
        elements[i].firstElementChild.getElementsByTagName('label')[0].classList.remove('mainTaskup');
      }
    }

    this.projectTaskDto = task;
    this.projectTaskDto.projectId = this.projectTaskListDto.projectId;
    this.isTaskDetailOpen = true;

  }

  checkListUpdateFlag(e) {
    console.log(e);
  }

  public onCLoseRaskDetailClose() {
    this.isTaskDetailOpen = false;
    this.projectTaskDto = undefined;
  }


  public deleteTasklisthandler(dataItem) {
    this.deleteTaskList.emit(dataItem);
  }

  public updateTaskListNameHandler(event, value) {
    if (event.keyCode === 13) {
      if (value !== null && value !== '') {

        const projecktask = this.projectTaskListDto;
        projecktask.projectTaskListName = value;
        if (!this.checkUserPermission('edit')) {
          return;
        }
        this.isDataLoaded = false;
        const inputProjectTask: ProjectTaskListDto = projecktask;
        this.projectTaskListService.update(inputProjectTask,
          response => {
            this.isDataLoaded = true;
            this.toasterService.success('', 'Project Task List Updated Successfully');
          },
          error => {
            this.isDataLoaded = true;
            this.toasterService.errorMessage(error);
          }
        );
      } else {

      }
    }
  }


  allCompletedTaskListFlag = false;

  completedTaskList() {
    this.allCompletedTaskListFlag = !this.allCompletedTaskListFlag;
  }

  openProjectTaskDetails(projectTaskDto) {
    this.projectTaskDto = projectTaskDto;
    this.isTaskDetailOpen = true;
  }

  public editTaskCompleteForm: FormGroup = new FormGroup({
    'projectTaskId': new FormControl(),
    'workflowResult': new FormControl('', Validators.required),
  });

  private closeTaskCompleteForm(): void {
    this.isShowTaskComplete = false;
  }

  public onSaveTaskComplete(e): void {
    this.projectTaskCompleteDto.workflowResult = this.editTaskCompleteForm.value.workflowResult;

    const updatetaskData: ProjecTaskDTO = this.projectTaskCompleteDto;
    updatetaskData.active = true;
    updatetaskData.completedByUserId = this.eqmSettingsService.getUser().userId;
    updatetaskData.completedDateTime = new Date();
    this.saveHandler(updatetaskData, true);
  }

  public taskPointList: Array<{ text: string, value?: number }> = [
    {text: 'No estimate', value: null},
    {text: '0', value: 0},
    {text: '1', value: 1},
    {text: '2', value: 2},
    {text: '3', value: 3},
    {text: '4', value: 4},
    {text: '5', value: 5}
  ];

  public addDotsToPoints(value: number) {
    let html = '';
    if (value != null) {
      for (let i = 0; i < value; i++) {
        html += `<span class="dotTask"></span>`;
      }
      for (let i = 0; i < this.taskPointList.length - 2 - value; i++) {
        html += `<span class="dotTask bg-lightgreyTask"></span>`;
      }
    }
    return html;
  }

  whateverEventHandler() {

    return false;
  }

  /*New task functionalities*/
  newTaskAdd(fun: string) {
    this.newFunctionalities = fun;
  }
}
