import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {PagedResultDtoOfProjecTaskDTO, ProjecTaskDTO} from './projecttask.model';
import {CommonService} from '../../../../shared/services/common.service';
import {
  filter,
  find,
  forEach,
  groupBy,
  includes,
  orderBy,
  remove,
  replace
} from 'lodash';
import {environment} from '../../../../../environments/environment';

@Injectable()
export class ProjectTaskService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;


  constructor(private commonService: CommonService, private http: HttpClient) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/Projects/api/ProjectTask/';
  }

  create(input: ProjecTaskDTO, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'InsertAsync';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  update(input: ProjecTaskDTO, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'UpdateAsync';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  updateTaskListId(input: ProjecTaskDTO[], successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'UpdateTaskListIdAsync';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  delete(id: any, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'DeleteAsync';
    const params = new HttpParams().set('id', id);

    const options = {
      params: params
    };

    this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
  }

  duplicateTask(id: any, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'DublicateTaskAsync';
    const params = new HttpParams().set('id', id);

    const options = {
      params: params
    };

    this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
  }

  getAll(ProjectTaskListid): Observable<PagedResultDtoOfProjecTaskDTO> {

    const url = this.apiBaseUrl + 'GetPagedQuery';
    const macAddress = this.commonService.getMacAddress();
    // Order by Clause
    let orderBy = '';

    orderBy = 'TaskListSortOrder asc';

    let filter = '';
    /*if (true) {
     filter = "Active eq true";
    }*/
    if (ProjectTaskListid !== 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += 'ProjectTaskListId eq ' + ProjectTaskListid;
    }

    const params = new HttpParams()
      .set('$count', 'true')
      .set('$orderby', orderBy.toString())
      .set('$filter', filter.toString());


    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'MacAddress': macAddress
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      const result200 = PagedResultDtoOfProjecTaskDTO.fromJS(response);
      return Observable.of(result200);
    });

  }

  reOrderAllTasks(input: ProjecTaskDTO, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'ReorderAllTasks';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }
}


