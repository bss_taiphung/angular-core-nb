import {SelectListDto} from '../../../../shared/models/selectListDto';
import {ProjectTagDTO} from '../../../projecttaskdetail/service/projecttag.model';
import {ProjectTaskAssignedTo} from '../../../projecttaskdetail/service/projectTaskAssignedTo.model';
import {ProjecTaskCheckListDTO} from '../../projecttaskCheckList/service/projecttaskCheckList.model';

// region IProjecTaskDTO

export interface IProjecTaskDTO {
  projectTaskId: number;
  projectId: number;
  projectTaskListId: number;
  projectTaskName: string;
  taskDescription: string;
  startDate: Date;
  dueDate: Date;
  points: number;
  labelColor: string;
  completedDateTime: Date;
  completedByUserId: number;
  active: boolean;
  numFilesLinked: number;
  taskListSortOrder: number;
  estimatedMinutes: number;
  actualMinutes: number;
  numComments: number;
  numChecklistItems: number;
  numChecklistItemsCompleted: number;
  assignedTo: any;
  followers: any;
  tags: any;
  checkLists: any;
  workflowResult: string;
  workflowOptions: string;
  assignUserList: any;
  tagList: any;
  totalTaskTime: any;
  totalTaskMinute: any;
  startDateString: string;
  dueDateString: string;
  updateTaskList: any;
}

export class ProjecTaskDTO implements IProjecTaskDTO {
  projectTaskId: number;
  projectId: number;
  projectTaskListId: number;
  projectTaskName: string;
  taskDescription: string;
  startDate: Date;
  dueDate: Date;
  startDateString: string;
  dueDateString: string;
  points: number;
  labelColor: string;
  completedDateTime: Date;
  completedByUserId: number;
  active: boolean;
  numFilesLinked: number;
  taskListSortOrder: number;
  estimatedMinutes: number;
  actualMinutes: number;
  numComments: number;
  numChecklistItems: number;
  numChecklistItemsCompleted: number;
  assignedTo: any;
  followers: any;
  tags: any;
  workflowResult: string;
  workflowOptions: any;
  totalTaskTime: any;
  totalTaskMinute: any;
  assignUserList: any;
  tagList: any;
  checkLists: any;
  updateTaskList: any;

  static fromJS(data: any): ProjecTaskDTO {
    const result = new ProjecTaskDTO();
    result.init(data);
    return result;
  }

  constructor(data?: IProjecTaskDTO) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.projectTaskId = data['projectTaskId'];
      this.projectId = data['projectId'];
      this.projectTaskListId = data['projectTaskListId'];
      this.projectTaskName = data['projectTaskName'];
      this.taskDescription = data['taskDescription'];
      this.startDateString = data['startDate'];
      this.dueDateString = data['dueDate'];
      this.startDate = data['startDate'] !== null ? new Date(data['startDate']) : null;
      this.dueDate = data['dueDate'] !== null ? new Date(data['dueDate']) : null;
      this.points = data['points'];
      this.labelColor = data['labelColor'];
      this.completedDateTime = data['completedDateTime'];
      this.completedByUserId = data['completedByUserId'];
      this.active = data['active'];
      this.numFilesLinked = data['numFilesLinked'];
      this.taskListSortOrder = data['taskListSortOrder'];
      this.estimatedMinutes = data['estimatedMinutes'] === null ? 0 : data['estimatedMinutes'];
      this.actualMinutes = data['actualMinutes'] === null ? 0 : data['actualMinutes'];
      this.numComments = data['numComments'] === null ? 0 : data['numComments'];
      this.numChecklistItems = data['numChecklistItems'] === null ? 0 : data['numChecklistItems'];
      this.numChecklistItemsCompleted = data['numChecklistItemsCompleted'] === null ? 0 : data['numChecklistItemsCompleted'];

      this.tags = ProjectTagDTO.fromJSArray(data['tags']);
      this.followers = ProjectTaskAssignedTo.fromJS(data['assignedTo']).filter(function (obj) {
        return parseInt(obj.followOnly) === 1
      });
      this.assignedTo = ProjectTaskAssignedTo.fromJS(data['assignedTo']).filter(function (obj) {
        return parseInt(obj.followOnly) === 0
      });
      this.checkLists = ProjecTaskCheckListDTO.fromJSarrya(data['checkLists']);
      this.totalTaskTime = data['totalTaskTime'];
      this.totalTaskMinute = data['totalTaskMinute'] === null ? 0 : data['totalTaskMinute'];
      this.workflowResult = data['workflowResult'];
      this.workflowOptions = data['workflowOptions'] != null ? JSON.parse(data['workflowOptions']) : null;
      this.updateTaskList = data['updateTaskList'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['projectTaskId'] = this.projectTaskId;
    data['projectId'] = this.projectId;
    data['projectTaskListId'] = this.projectTaskListId;
    data['projectTaskName'] = this.projectTaskName;
    data['taskDescription'] = this.taskDescription;
    data['startDate'] = this.startDate;
    data['dueDate'] = this.dueDate;
    data['points'] = this.points;
    data['labelColor'] = this.labelColor;
    data['completedDateTime'] = this.completedDateTime;
    data['completedByUserId'] = this.completedByUserId;
    data['active'] = this.active;
    data['numFilesLinked'] = this.numFilesLinked;
    data['taskListSortOrder'] = this.taskListSortOrder;
    data['estimatedMinutes'] = this.estimatedMinutes;
    data['actualMinutes'] = this.actualMinutes;
    data['numComments'] = this.numComments;
    data['numChecklistItems'] = this.numChecklistItems;
    data['numChecklistItemsCompleted'] = this.numChecklistItemsCompleted;
    data['tags'] = this.tags;
    data['assignedTo'] = this.assignedTo;
    data['checkLists'] = this.checkLists;
    data['followers'] = this.followers;
    data['workflowResult'] = this.workflowResult;
    data['workflowOptions'] = this.workflowOptions;
    data['assignUserList'] = this.assignUserList;
    data['tagList'] = this.tagList;
    data['totalTaskTime'] = this.totalTaskTime;
    data['totalTaskMinute'] = this.totalTaskMinute;
    data['updateTaskList'] = this.updateTaskList;
    return data;
  }
}

// endregion


// region IPagedResultDtoOfProjecTaskDTO
export interface IPagedResultDtoOfProjecTaskDTO {
  results: ProjecTaskDTO[];
  count: number;
}

export class PagedResultDtoOfProjecTaskDTO implements IPagedResultDtoOfProjecTaskDTO {
  results: ProjecTaskDTO[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfProjecTaskDTO {
    const result = new PagedResultDtoOfProjecTaskDTO();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfProjecTaskDTO) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(ProjecTaskDTO.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

export class BulkupdateProjecTaskDto implements IPagedResultDtoOfProjecTaskDTO {
  results: ProjecTaskDTO[];
  count: number;

  static fromJS(data: any): BulkupdateProjecTaskDto {

    const result = new BulkupdateProjecTaskDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfProjecTaskDTO) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      this.results = [];
      for (const item of data) {
        this.results.push(ProjecTaskDTO.fromJS(item));
      }

    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }

    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }

}

// endregion

// region ICreateProjecTaskDTO

export interface ICreateProjecTaskDTO {
  projectTaskId: number;
  projectId: number;
  projectTaskListId: number;
  projectTaskName: string;
  taskDescription: string;
  startDate: Date;
  dueDate: Date;
  points: number;
  labelColor: string;
  completedDateTime: Date;
  completedByUserId: number;
  numFilesLinked: number;
  taskListSortOrder: number;
  estimatedMinutes: number;
  actualMinutes: number;
  numComments: number;
  numChecklistItems: number;
  numChecklistItemsCompleted: number;
  active: boolean;
  assignedTo: SelectListDto[];
  followers: SelectListDto[];
  assignedToMember: SelectListDto[];
  tags: ProjectTagDTO[];
  workflowResult: string;
  workflowOptions: string;
}

export class CreateProjecTaskDTO implements ICreateProjecTaskDTO {
  projectTaskId: number;
  projectId: number;
  projectTaskListId: number;
  projectTaskName: string;
  taskDescription: string;
  startDate: Date;
  dueDate: Date;
  points: number;
  labelColor: string;
  completedDateTime: Date;
  completedByUserId: number;
  numFilesLinked: number;
  taskListSortOrder: number;
  estimatedMinutes: number;
  actualMinutes: number;
  numComments: number;
  numChecklistItems: number;
  numChecklistItemsCompleted: number;
  active: boolean;
  assignedTo: SelectListDto[];
  assignedToMember: SelectListDto[];
  followers: SelectListDto[];
  tags: ProjectTagDTO[];
  workflowResult: string;
  workflowOptions: string;

  static fromJS(data: any): CreateProjecTaskDTO {
    const result = new CreateProjecTaskDTO();
    result.init(data);
    return result;
  }

  constructor(data?: ICreateProjecTaskDTO) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.projectTaskId = data['projectTaskId'];
      this.projectId = data['projectId'];
      this.projectTaskListId = data['projectTaskListId'];
      this.projectTaskName = data['projectTaskName'];
      this.taskDescription = data['taskDescription'];
      this.startDate = data['startDate'];
      this.dueDate = data['dueDate'];
      this.points = data['points'];
      this.labelColor = data['labelColor'];
      this.completedDateTime = data['completedDateTime'];
      this.completedByUserId = data['completedByUserId'];
      this.numFilesLinked = data['numFilesLinked'];
      this.taskListSortOrder = data['taskListSortOrder'];
      this.estimatedMinutes = data['estimatedMinutes'];
      this.actualMinutes = data['actualMinutes'];
      this.numComments = data['numComments'];
      this.numChecklistItems = data['numChecklistItems'];
      this.numChecklistItemsCompleted = data['numChecklistItemsCompleted'];
      this.active = data['active'];

      this.assignedTo = data['assignedTo'];
      this.followers = data['followers'];
      this.tags = data['tags'];
      this.workflowResult = data['workflowResult'];
      this.workflowOptions = data['workflowOptions'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['projectTaskId'] = this.projectTaskId;
    data['projectId'] = this.projectId;

    data['projectTaskListId'] = this.projectTaskListId;
    data['projectTaskName'] = this.projectTaskName;
    data['taskDescription'] = this.taskDescription;
    data['startDate'] = this.startDate;
    data['dueDate'] = this.dueDate;
    data['points'] = this.points;
    data['labelColor'] = this.labelColor;
    data['completedDateTime'] = this.completedDateTime;
    data['completedByUserId'] = this.completedByUserId;
    data['numFilesLinked'] = this.numFilesLinked;
    data['taskListSortOrder'] = this.taskListSortOrder;
    data['estimatedMinutes'] = this.estimatedMinutes;
    data['actualMinutes'] = this.actualMinutes;
    data['numComments'] = this.numComments;
    data['numChecklistItems'] = this.numChecklistItems;
    data['numChecklistItemsCompleted'] = this.numChecklistItemsCompleted;
    data['active'] = this.active;

    data['assignedTo'] = this.assignedTo;
    data['followers'] = this.followers;

    data['workflowResult'] = this.workflowResult;
    data['workflowOptions'] = this.workflowOptions;

    return data;
  }
}

// endregion
