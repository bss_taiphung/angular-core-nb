"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var projecttag_model_1 = require("../../../projecttaskdetail/service/projecttag.model");
var projectTaskAssignedTo_model_1 = require("../../../projecttaskdetail/service/projectTaskAssignedTo.model");
var ProjecTaskDTO = /** @class */ (function () {
    function ProjecTaskDTO(data) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    this[property] = data[property];
            }
        }
    }
    ProjecTaskDTO.prototype.init = function (data) {
        if (data) {
            this.projectTaskId = data["projectTaskId"];
            this.projectId = data["projectId"];
            this.projectTaskListId = data["projectTaskListId"];
            this.projectTaskName = data["projectTaskName"];
            this.taskDescription = data["taskDescription"];
            this.startDateString = data["startDate"];
            this.dueDateString = data["dueDate"];
            this.startDate = data["startDate"] !== null ? new Date(data["startDate"]) : null;
            this.dueDate = data["dueDate"] !== null ? new Date(data["dueDate"]) : null;
            this.points = data["points"];
            this.labelColor = data["labelColor"];
            this.completedDateTime = data["completedDateTime"];
            this.completedByUserId = data["completedByUserId"];
            this.active = data["active"];
            this.numFilesLinked = data["numFilesLinked"];
            this.taskListSortOrder = data["taskListSortOrder"];
            this.estimatedMinutes = data["estimatedMinutes"] === null ? 0 : data["estimatedMinutes"];
            this.actualMinutes = data["actualMinutes"] === null ? 0 : data["actualMinutes"];
            this.numComments = data["numComments"] === null ? 0 : data["numComments"];
            this.numChecklistItems = data["numChecklistItems"] === null ? 0 : data["numChecklistItems"];
            this.numChecklistItemsCompleted = data["numChecklistItemsCompleted"] === null ? 0 : data["numChecklistItemsCompleted"];
            this.tags = projecttag_model_1.ProjectTagDTO.fromJSArray(data["tags"]);
            this.followers = projectTaskAssignedTo_model_1.ProjectTaskAssignedTo.fromJS(data["assignedTo"]).filter(function (obj) { return parseInt(obj.followOnly) === 1; });
            this.assignedTo = projectTaskAssignedTo_model_1.ProjectTaskAssignedTo.fromJS(data["assignedTo"]).filter(function (obj) { return parseInt(obj.followOnly) === 0; });
            this.totalTaskTime = data["totalTaskTime"];
            this.totalTaskMinute = data["totalTaskMinute"] === null ? 0 : data["totalTaskMinute"];
            this.workflowResult = data["workflowResult"];
            this.workflowOptions = data["workflowOptions"];
        }
    };
    ProjecTaskDTO.fromJS = function (data) {
        var result = new ProjecTaskDTO();
        result.init(data);
        return result;
    };
    ProjecTaskDTO.prototype.toJSON = function (data) {
        data = typeof data === 'object' ? data : {};
        data["projectTaskId"] = this.projectTaskId;
        data["projectId"] = this.projectId;
        data["projectTaskListId"] = this.projectTaskListId;
        data["projectTaskName"] = this.projectTaskName;
        data["taskDescription"] = this.taskDescription;
        data["startDate"] = this.startDate;
        data["dueDate"] = this.dueDate;
        data["points"] = this.points;
        data["labelColor"] = this.labelColor;
        data["completedDateTime"] = this.completedDateTime;
        data["completedByUserId"] = this.completedByUserId;
        data["active"] = this.active;
        data["numFilesLinked"] = this.numFilesLinked;
        data["taskListSortOrder"] = this.taskListSortOrder;
        data["estimatedMinutes"] = this.estimatedMinutes;
        data["actualMinutes"] = this.actualMinutes;
        data["numComments"] = this.numComments;
        data["numChecklistItems"] = this.numChecklistItems;
        data["numChecklistItemsCompleted"] = this.numChecklistItemsCompleted;
        data["tags"] = this.tags;
        data["assignedTo"] = this.assignedTo;
        data["followers"] = this.followers;
        data["workflowResult"] = this.workflowResult;
        data["workflowOptions"] = this.workflowOptions;
        data["assignUserList"] = this.assignUserList;
        data["tagList"] = this.tagList;
        data["totalTaskTime"] = this.totalTaskTime;
        data["totalTaskMinute"] = this.totalTaskMinute;
        return data;
    };
    return ProjecTaskDTO;
}());
exports.ProjecTaskDTO = ProjecTaskDTO;
var PagedResultDtoOfProjecTaskDTO = /** @class */ (function () {
    function PagedResultDtoOfProjecTaskDTO(data) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    this[property] = data[property];
            }
        }
    }
    PagedResultDtoOfProjecTaskDTO.prototype.init = function (data) {
        if (data && data != null) {
            if (data["results"] && data["results"].constructor === Array) {
                this.results = [];
                for (var _i = 0, _a = data["results"]; _i < _a.length; _i++) {
                    var item = _a[_i];
                    this.results.push(ProjecTaskDTO.fromJS(item));
                }
            }
            this.count = data["count"];
        }
    };
    PagedResultDtoOfProjecTaskDTO.fromJS = function (data) {
        var result = new PagedResultDtoOfProjecTaskDTO();
        result.init(data);
        return result;
    };
    PagedResultDtoOfProjecTaskDTO.prototype.toJSON = function (data) {
        data = typeof data === 'object' ? data : {};
        if (this.results && this.results.constructor === Array) {
            data["results"] = [];
            for (var _i = 0, _a = this.results; _i < _a.length; _i++) {
                var item = _a[_i];
                data["results"].push(item.toJSON());
            }
        }
        if (this.count) {
            data["count"] = this.count;
        }
        return data;
    };
    return PagedResultDtoOfProjecTaskDTO;
}());
exports.PagedResultDtoOfProjecTaskDTO = PagedResultDtoOfProjecTaskDTO;
var CreateProjecTaskDTO = /** @class */ (function () {
    function CreateProjecTaskDTO(data) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    this[property] = data[property];
            }
        }
    }
    CreateProjecTaskDTO.prototype.init = function (data) {
        if (data) {
            this.projectTaskId = data["projectTaskId"];
            this.projectId = data["projectId"];
            this.projectTaskListId = data["projectTaskListId"];
            this.projectTaskName = data["projectTaskName"];
            this.taskDescription = data["taskDescription"];
            this.startDate = data["startDate"];
            this.dueDate = data["dueDate"];
            this.points = data["points"];
            this.labelColor = data["labelColor"];
            this.completedDateTime = data["completedDateTime"];
            this.completedByUserId = data["completedByUserId"];
            this.numFilesLinked = data["numFilesLinked"];
            this.taskListSortOrder = data["taskListSortOrder"];
            this.estimatedMinutes = data["estimatedMinutes"];
            this.actualMinutes = data["actualMinutes"];
            this.numComments = data["numComments"];
            this.numChecklistItems = data["numChecklistItems"];
            this.numChecklistItemsCompleted = data["numChecklistItemsCompleted"];
            this.active = data["active"];
            this.assignedTo = data["assignedTo"];
            this.followers = data["followers"];
            this.tags = data["tags"];
            this.workflowResult = data["workflowResult"];
            this.workflowOptions = data["workflowOptions"];
        }
    };
    CreateProjecTaskDTO.fromJS = function (data) {
        var result = new CreateProjecTaskDTO();
        result.init(data);
        return result;
    };
    CreateProjecTaskDTO.prototype.toJSON = function (data) {
        data = typeof data === 'object' ? data : {};
        data["projectTaskId"] = this.projectTaskId;
        data["projectId"] = this.projectId;
        data["projectTaskListId"] = this.projectTaskListId;
        data["projectTaskName"] = this.projectTaskName;
        data["taskDescription"] = this.taskDescription;
        data["startDate"] = this.startDate;
        data["dueDate"] = this.dueDate;
        data["points"] = this.points;
        data["labelColor"] = this.labelColor;
        data["completedDateTime"] = this.completedDateTime;
        data["completedByUserId"] = this.completedByUserId;
        data["numFilesLinked"] = this.numFilesLinked;
        data["taskListSortOrder"] = this.taskListSortOrder;
        data["estimatedMinutes"] = this.estimatedMinutes;
        data["actualMinutes"] = this.actualMinutes;
        data["numComments"] = this.numComments;
        data["numChecklistItems"] = this.numChecklistItems;
        data["numChecklistItemsCompleted"] = this.numChecklistItemsCompleted;
        data["active"] = this.active;
        data["assignedTo"] = this.assignedTo;
        data["followers"] = this.followers;
        data["workflowResult"] = this.workflowResult;
        data["workflowOptions"] = this.workflowOptions;
        return data;
    };
    return CreateProjecTaskDTO;
}());
exports.CreateProjecTaskDTO = CreateProjecTaskDTO;
var BulkupdateProjecTaskDto = /** @class */ (function () {
    function BulkupdateProjecTaskDto(data) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    this[property] = data[property];
            }
        }
    }
    BulkupdateProjecTaskDto.prototype.init = function (data) {
        if (data && data != null) {
            this.results = [];
            for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                var item = data_1[_i];
                this.results.push(ProjecTaskDTO.fromJS(item));
            }
        }
    };
    BulkupdateProjecTaskDto.fromJS = function (data) {
        var result = new BulkupdateProjecTaskDto();
        result.init(data);
        return result;
    };
    BulkupdateProjecTaskDto.prototype.toJSON = function (data) {
        data = typeof data === 'object' ? data : {};
        if (this.results && this.results.constructor === Array) {
            data["results"] = [];
            for (var _i = 0, _a = this.results; _i < _a.length; _i++) {
                var item = _a[_i];
                data["results"].push(item.toJSON());
            }
        }
        if (this.count) {
            data["count"] = this.count;
        }
        return data;
    };
    return BulkupdateProjecTaskDto;
}());
exports.BulkupdateProjecTaskDto = BulkupdateProjecTaskDto;
//# sourceMappingURL=projecttask.model.js.map