import {
  CUSTOM_ELEMENTS_SCHEMA,
  NgModule
} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {BsDropdownModule} from 'ngx-bootstrap';

import {ButtonsModule} from '@progress/kendo-angular-buttons';
import {DialogModule} from '@progress/kendo-angular-dialog';
import {GridModule} from '@progress/kendo-angular-grid';
import {InputsModule} from '@progress/kendo-angular-inputs';
import {DateInputsModule} from '@progress/kendo-angular-dateinputs';
import {DropDownsModule} from '@progress/kendo-angular-dropdowns';
import {TabStripModule} from '@progress/kendo-angular-layout';
import {TelerikReportingModule} from '@progress/telerik-angular-report-viewer';
import {SortableModule} from '@progress/kendo-angular-sortable';

import {FocusModule} from 'angular2-focus';

import {QuillModule} from 'ngx-quill';
import {ColorPickerModule} from 'ngx-color-picker';

import {DocumentModule} from '../document/document.module';
/*
* For share
* */
import {SharedModule} from '../shared/index';

/*
* Routes
* */
import {routing} from './project.routes';

/*
* Component
* */

// Project components
import {ProjectComponent} from './project.component';
import {ProjectListComponent} from './projectslist/projectslist.component';
import {ProjectsListFormComponent} from './projectslist/projectslist-form.component';
import {ProjectTaskListComponent} from './projecttasklist/projecttasklist.component';
import {ProjectTaskListFormComponent} from './projecttasklist/projecttasklist-form.component';
import {ProjectTaskCheckListComponent} from './projecttasklist/projecttaskCheckList/projecttaskCheckList.component';
import {ProjectGroupsComponent} from './projectgroups/projectgroups.component';
import {ProjectGroupsFormComponent} from './projectgroups/projectgroups-form.component';
import {ProjectTaskDetailComponent} from './projecttaskdetail/projecttaskdetail.component';
import {ProjectTaskComponent} from './projecttasklist/projecttask/projecttask.component';
import {ProjectTaskCompletedComponent} from './projecttasklist/projecttask/projecttaskcompleted.component';

// Workspace components
import {WorkspaceComponent} from './workspace/workspace.component';
import {WorkspaceDropdownComponent} from './shared/workspacedropdown/workspacedropdown.component';
import {WorkspaceFormComponent} from './workspace/workspace-form.component';
import {WorkspaceTeamComponent} from './workspace/workspace-team.component';

// TagMaster components
import {TagMasterComponent} from './tagmaster/tagmaster.component';
import {TagMasterFormComponent} from './tagmaster/tagmaster-form.component';

// Role components
import {RoleFormComponent} from './role/role.component';

// UserSettings components
import {UserSettingsComponent} from './usersettings/usersettings.component';

/*
* Service
* */

// Project services
import {ProjectListService} from './projectslist/service/projectslist.service';
import {ProjectTaskListService} from './projecttasklist/service/projecttasklist.service';
import {ProjectSettingsService} from './shared/services/projectsettings.service';
import {ProjectTaskCheckListService} from './projecttasklist/projecttaskCheckList/service/projecttaskCheckList.service';
import {ProjectTaskTimeService} from './projecttaskdetail/service/projectTaskTime.service';
import {ProjectTaskService} from './projecttasklist/projecttask/service/projecttask.service';
import {ProjectGroupsService} from './projectgroups/service/projectgroups.service';
import {ProjectTaskDetailService} from './projecttaskdetail/service/projecttaskdetail.service';

// Workspace services
import {WorkspaceService} from './workspace/service/workspace.service';
import {WorkspaceTeamService} from './workspace/service/workspace-team.service';

// Roles services
import {RolesService} from './role/service/role.service';

// UserSettings services
import {UserSettingsService} from './usersettings/service/usersettings.service';
import {UserStarredProjectsService} from './projectslist/service/userstarredprojects.service';

// TagMaster
import {TagMasterService} from './tagmaster/service/tagmaster.service';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    ButtonsModule,
    GridModule,
    InputsModule,
    DialogModule,
    DateInputsModule,
    DropDownsModule,
    CommonModule,
    SharedModule,
    routing,
    TelerikReportingModule,
    SortableModule,
    TabStripModule,
    QuillModule,
    ColorPickerModule,
    DocumentModule,
    BsDropdownModule.forRoot(),
    FocusModule.forRoot()
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  declarations: [
    WorkspaceDropdownComponent,
    ProjectComponent,
    WorkspaceComponent,
    WorkspaceFormComponent,
    WorkspaceTeamComponent,
    ProjectListComponent,
    ProjectTaskCheckListComponent,
    ProjectsListFormComponent,
    ProjectGroupsComponent,
    ProjectTaskListComponent,
    ProjectTaskListFormComponent,
    RoleFormComponent,
    UserSettingsComponent,
    ProjectTaskComponent,
    TagMasterComponent,
    TagMasterFormComponent,
    ProjectTaskDetailComponent,
    ProjectGroupsFormComponent,
    ProjectTaskCompletedComponent,
    // FocusDirective

  ],
  providers: [
    WorkspaceService,
    WorkspaceTeamService,
    ProjectListService,
    ProjectTaskCheckListService,
    ProjectGroupsService,
    ProjectTaskListService,
    RolesService,
    UserSettingsService,
    ProjectTaskService,
    TagMasterService,
    ProjectSettingsService,
    ProjectTaskDetailService,
    UserStarredProjectsService,
    ProjectTaskTimeService
  ]
})
export class ProjectModule {
}
