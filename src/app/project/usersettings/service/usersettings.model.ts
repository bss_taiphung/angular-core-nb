import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';


// region IUsersettingsDto

export interface IUsersettingsDto {
  userSettingsId: number;
  userId: number;
  roleId: number;
  displayName: string;
  currentWorkspaceId: number;
  workspaceName: string;
  roleName: string;
  createdDate: string;
  createdDateTime: string;
  modifiedByName: string;
  modifiedDateTime: string;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
}

export class UsersettingsDto implements IUsersettingsDto {
  userSettingsId: number;
  userId: number;
  roleId: number;
  displayName: string;
  workspaceName: string;
  roleName: string;
  currentWorkspaceId: number;
  createdDate: string;
  createdDateTime: string;
  modifiedByName: string;
  modifiedDateTime: string;
  createdBy: number;
  modifiedBy: number;
  active: boolean;

  static fromJS(data: any): UsersettingsDto {
    const result = new UsersettingsDto();
    result.init(data);
    return result;
  }

  constructor(data?: IUsersettingsDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.userSettingsId = data['userSettingsId'];
      this.userId = data['userId'];
      this.roleId = data['roleId'];
      this.displayName = data['displayName'];
      this.workspaceName = data['workspaceName'];
      this.roleName = data['roleName'];
      this.currentWorkspaceId = data['currentWorkspaceId'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.active = data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['userSettingsId'] = this.userSettingsId;
    data['userId'] = this.userId;
    data['roleId'] = this.roleId;
    data['displayName'] = this.displayName;
    data['workspaceName'] = this.workspaceName;
    data['roleName'] = this.roleName;
    data['currentWorkspaceId'] = this.currentWorkspaceId;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;

    return data;
  }
}

// endregion

// region ICreateUsersettingsDto

export interface ICreateUsersettingsDto {
  userSettingsId: number;
  userId: number;
  roleId: number;
  currentWorkspaceId: number;
  active: boolean;
}

export class CreateUsersettingsDto implements ICreateUsersettingsDto {
  userSettingsId = 0;
  userId: number;
  roleId: number;
  currentWorkspaceId: number;
  active = true;

  static fromJS(data: any): UsersettingsDto {
    const result = new UsersettingsDto();
    result.init(data);
    return result;
  }

  constructor(data?: IUsersettingsDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.userSettingsId = 0;
      this.userId = data['userId'];
      this.roleId = data['roleId'];
      this.currentWorkspaceId = data['currentWorkspaceId'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['userSettingsId'] = 0;
    data['userId'] = this.userId;
    data['roleId'] = this.roleId;
    data['currentWorkspaceId'] = this.currentWorkspaceId;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

// endregion


// region IPagedResultDtoOfUsersettingsDto

export interface IPagedResultDtoOfUsersettingsDto {
  results: UsersettingsDto[];
  count: number;
}

export class PagedResultDtoOfUsersettingsDto implements IPagedResultDtoOfUsersettingsDto {
  results: UsersettingsDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfUsersettingsDto {
    const result = new PagedResultDtoOfUsersettingsDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfUsersettingsDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(UsersettingsDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion
