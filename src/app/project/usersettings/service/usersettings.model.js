"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("rxjs/add/operator/map");
require("rxjs/add/operator/mergeMap");
require("rxjs/add/observable/fromPromise");
require("rxjs/add/observable/of");
require("rxjs/add/observable/throw");
require("rxjs/add/operator/map");
require("rxjs/add/operator/toPromise");
require("rxjs/add/operator/mergeMap");
require("rxjs/add/operator/catch");
var UsersettingsDto = /** @class */ (function () {
    function UsersettingsDto(data) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    this[property] = data[property];
            }
        }
    }
    UsersettingsDto.prototype.init = function (data) {
        if (data) {
            this.userSettingsId = data["userSettingsId"];
            this.userId = data["userId"];
            this.roleId = data["roleId"];
            this.displayName = data["displayName"];
            this.workspaceName = data["workspaceName"];
            this.roleName = data["roleName"];
            this.currentWorkspaceId = data["currentWorkspaceId"];
            this.createdDateTime = data["createdDateTime"];
            this.createdDate = data["createdDate"];
            this.modifiedByName = data["modifiedByName"];
            this.modifiedDateTime = data["modifiedDateTime"];
            this.createdBy = data["createdBy"];
            this.modifiedBy = data["modifiedBy"];
            this.active = data["active"];
        }
    };
    UsersettingsDto.fromJS = function (data) {
        var result = new UsersettingsDto();
        result.init(data);
        return result;
    };
    UsersettingsDto.prototype.toJSON = function (data) {
        data = typeof data === 'object' ? data : {};
        data["userSettingsId"] = this.userSettingsId;
        data["userId"] = this.userId;
        data["roleId"] = this.roleId;
        data["displayName"] = this.displayName;
        data["workspaceName"] = this.workspaceName;
        data["roleName"] = this.roleName;
        data["currentWorkspaceId"] = this.currentWorkspaceId;
        data["createdDateTime"] = this.createdDateTime;
        data["createdDateTime"] = this.createdDateTime;
        data["createdDate"] = this.createdDate;
        data["modifiedByName"] = this.modifiedByName;
        data["modifiedDateTime"] = this.modifiedDateTime;
        data["createdBy"] = this.createdBy;
        data["modifiedBy"] = this.modifiedBy;
        data["active"] = this.active;
        return data;
    };
    return UsersettingsDto;
}());
exports.UsersettingsDto = UsersettingsDto;
var CreateUsersettingsDto = /** @class */ (function () {
    function CreateUsersettingsDto(data) {
        this.userSettingsId = 0;
        this.active = true;
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    this[property] = data[property];
            }
        }
    }
    CreateUsersettingsDto.prototype.init = function (data) {
        if (data) {
            this.userSettingsId = 0;
            this.userId = data["userId"];
            this.roleId = data["roleId"];
            this.currentWorkspaceId = data["currentWorkspaceId"];
            this.active = (data["active"] == null) ? false : data["active"];
        }
    };
    CreateUsersettingsDto.fromJS = function (data) {
        var result = new UsersettingsDto();
        result.init(data);
        return result;
    };
    CreateUsersettingsDto.prototype.toJSON = function (data) {
        data = typeof data === 'object' ? data : {};
        data["userSettingsId"] = 0;
        data["userId"] = this.userId;
        data["roleId"] = this.roleId;
        data["currentWorkspaceId"] = this.currentWorkspaceId;
        data["active"] = (this.active == null) ? false : this.active;
        return data;
    };
    return CreateUsersettingsDto;
}());
exports.CreateUsersettingsDto = CreateUsersettingsDto;
var PagedResultDtoOfUsersettingsDto = /** @class */ (function () {
    function PagedResultDtoOfUsersettingsDto(data) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    this[property] = data[property];
            }
        }
    }
    PagedResultDtoOfUsersettingsDto.prototype.init = function (data) {
        if (data && data != null) {
            if (data["results"] && data["results"].constructor === Array) {
                this.results = [];
                for (var _i = 0, _a = data["results"]; _i < _a.length; _i++) {
                    var item = _a[_i];
                    this.results.push(UsersettingsDto.fromJS(item));
                }
            }
            if (data["count"]) {
                this.count = data["count"];
            }
        }
    };
    PagedResultDtoOfUsersettingsDto.fromJS = function (data) {
        var result = new PagedResultDtoOfUsersettingsDto();
        result.init(data);
        return result;
    };
    PagedResultDtoOfUsersettingsDto.prototype.toJSON = function (data) {
        data = typeof data === 'object' ? data : {};
        if (this.results && this.results.constructor === Array) {
            data["results"] = [];
            for (var _i = 0, _a = this.results; _i < _a.length; _i++) {
                var item = _a[_i];
                data["results"].push(item.toJSON());
            }
        }
        if (this.count) {
            data["count"] = this.count;
        }
        return data;
    };
    return PagedResultDtoOfUsersettingsDto;
}());
exports.PagedResultDtoOfUsersettingsDto = PagedResultDtoOfUsersettingsDto;
//# sourceMappingURL=usersettings.model.js.map