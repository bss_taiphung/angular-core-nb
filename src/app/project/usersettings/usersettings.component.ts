import {
  Component,
  OnInit
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';
import {DomSanitizer} from '@angular/platform-browser';
import {Observable} from 'rxjs/Observable';

import {HubConnection} from '@aspnet/signalr-client';
import {SortDescriptor, State} from '@progress/kendo-data-query';
import {DataStateChangeEvent, GridDataResult, PageChangeEvent} from '@progress/kendo-angular-grid';

import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {SelectListDto} from '../../shared/models/selectListDto';


import {UserSettingsService} from './service/usersettings.service'
import {CreateUsersettingsDto, IUsersettingsDto, UsersettingsDto} from './service/usersettings.model';
import {ProjectSettingsService} from '../shared/services/projectsettings.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'kendo-grid-usersettings',
  templateUrl: './html/usersettings.component.html'
})
export class UserSettingsComponent implements OnInit {
  public active = false;
  public editForm: FormGroup = new FormGroup({
    'userSettingsId': new FormControl(),
    'userId': new FormControl(''),
    'roleId': new FormControl('', Validators.required),
    'currentWorkspaceId': new FormControl(''),
    'active': new FormControl(false),
  });

  private userName: string;
  public currentWorkspaceIdList: SelectListDto[];
  public filtercurrentWorkspaceIdList: SelectListDto[];

  public userIdList: SelectListDto[];
  public filteruserIDList: SelectListDto[];

  public filterroleIdList: SelectListDto[];
  public roleIdList: SelectListDto[];

  public selectedWorkspaceName: any;
  public selectedWorkspaceId: any;


  public showActive() {
    this.isactive = !this.isactive;
    this.gridLoading = true;
    this.signalrRConnection();
    this.loadGridData();
  }


  private closeForm(): void {
    this.addUserSettingsFlag = false;
    this.isDuplicateMember = false;
  }


  data: IUsersettingsDto[];
  public formGroup: FormGroup;
  public view: Observable<GridDataResult>;
  private editedRowIndex: number;
  public addUserSettingsFlag: boolean;
  public editDataItem: CreateUsersettingsDto;
  public pageSize = 10;
  public skip = 0;
  public state: State = {
    skip: 0,
    take: 5,
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };

  constructor(private toasterService: ToasterHelperService,
              private projectSettingsService: ProjectSettingsService,
              private domSanitizer: DomSanitizer, private commonService: CommonService,
              private userSettingsService: UserSettingsService) {

  }

  ngOnInit() {

    this.loadUserData();
    this.commonService.getAll('Projects', 'Role').subscribe(eventResult => {
      this.filterroleIdList = this.roleIdList = eventResult.results;
    });
    this.signalrRConnection();
    this.loadGridData();
  }

  signalrRConnection() {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    const workspaceid = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');
        this.hubConnection
          .invoke('Subscribe', 'Projects', 'UserSettings', 'Active', 'True')
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));

    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');
        this.hubConnection
          .invoke('Subscribe', 'Projects', 'UserSettings', 'Active', 'False')
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated',
      (groupName: string, operation: string, messageJson: string) => {
        const _responseText = messageJson;
        let result200: any = null;

        const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
        const resultData200 = this.commonService.toCamel(data);
        result200 = resultData200 ? UsersettingsDto.fromJS(resultData200) : new UsersettingsDto();
        const dataExist = this.containsObject(result200, this.gridView.data);
        if (operation.toLowerCase() === 'insert' && !dataExist) {
          this.gridView.data.unshift(result200);
        }
        if (operation.toLowerCase() === 'update') {
          this.gridView.data.forEach((element, index) => {
            if (element.userSettingsId === result200.userSettingsId) {
              if (this.isactive) {
                if (result200.active === this.isactive) {
                  this.gridView.data[index] = result200;
                } else {
                  operation = 'delete';
                }
              } else {
                this.gridView.data[index] = result200;
              }
            }
          });
        }

        if (operation.toLowerCase() === 'delete' && dataExist) {
          let index = null;
          this.gridView.data.forEach((element, i) => {
            if (element.userSettingsId === result200.userSettingsId) {
              index = i;
            }
          });
          if (index !== null) {
            this.gridView.data.splice(index, 1);
          }
        }
      });


  }

  containsObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].userSettingsId === obj.userSettingsId) {
        return true;
      }
    }

    return false;
  }


  gridView: GridDataResult;
  deleteDataItem: any;
  sort: SortDescriptor[] = [];
  isactive = true;
  isDuplicateTeamMember: boolean;
  searchtext = '';
  selWorkspaceId: number;
  selWorkspaceName = '';

  private hubConnection: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;


  // Dialog box
  opened = false;
  loading = true;
  gridLoading = true;


  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.loadGridData();
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
    this.loadGridData();
  }

  private loadGridData(): void {
    this.gridLoading = true;
    this.userSettingsService.getAll(this.pageSize,
      this.skip,
      this.sort,
      this.isactive,
      this.searchtext,
      0)
      .subscribe(eventResult => {
        this.gridView = {
          data: eventResult.results,
          total: eventResult.count
        };
        this.loading = false;
        this.gridLoading = false;
      });
  }


  public isNew: boolean;

  public saveUserSettingsData() {
    this.addUserSettingsFlag = false;
    if (this.isNew) {
      const inputData: CreateUsersettingsDto = this.editForm.value;
      inputData.currentWorkspaceId = null; // this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;
      this.userSettingsService.create(inputData,
        response => {
          if (response === false) {
            this.isNew = true;
            this.addUserSettingsFlag = true;
            this.isDuplicateMember = true;
            return false;
          } else {
            this.toasterService.success('', 'User Settings Saved Successfully');
            this.addUserSettingsFlag = false;
            this.isDuplicateMember = false;
          }
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    } else {
      const editInputData = this.editForm.value;
      // editInputData.currentWorkspaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId
      this.userSettingsService.update(editInputData,
        response => {
          if (response) {
            this.toasterService.success('', 'User Settings Updated Successfully');
            this.addUserSettingsFlag = false;
            this.isDuplicateMember = false;
          }
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  public isDuplicateMember = false;

  public addNewUserSettings() {
    this.editDataItem = new CreateUsersettingsDto();
    this.addUserSettingsFlag = true;
    this.editForm.reset();
    this.isNew = true;
  }


  timeout = null;

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridLoading = true;

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.doSearch(state, this);
    }, 1000);
  }

  doSearch(state: any, that) {
    if (state.filter) {
      const search = that.commonService.getFilter(state.filter.filters, that.isactive);
      that.userSettingsService.getAll(state.take, state.skip, that.sort, this.isactive, search, 0)
        .subscribe(eventResult => {
          that.gridView = {
            data: eventResult.results,
            total: eventResult.count
          };
          that.loading = false;
          that.gridLoading = false;
        });

      this.skip = state.skip;
      this.pageSize = state.take;
    }
    else {
      that.loadGridData();
    }
  }

  public closeAddUserSettings() {
    this.editDataItem = undefined;
    this.editForm.reset();
    this.addUserSettingsFlag = false;
    this.isDuplicateMember = false;
  }

  // Delete confrm
  public close(status) {
    this.isDuplicateMember = false;
    const $this = this;
    if (status) {
      this.userSettingsService.delete($this.deleteDataItem.userSettingsId,
        response => {
          if (response) {
            this.toasterService.success('', 'User Settings Removed Successfully');
          }
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
    $this.deleteDataItem = null;
    $this.opened = false;
  }

  // Delete dialog
  public open() {
    this.opened = true;
  }

  public ActionData: Array<any> = [
    {
      text: 'Delete',
      icon: 'trash'
    }];

  public onAction(e, roleData) {
    if (e === undefined) {
      this.editDataItem = roleData;
      this.isNew = false;
      this.editForm.reset(roleData);
      this.userName = roleData.displayName;
      this.addUserSettingsFlag = true;
    } else {
      if (e.text === 'Delete') {
        this.addUserSettingsFlag = false;
        this.deleteDataItem = roleData;
        this.open();
      }
    }
  }

  private userIDListHandleFilter(value) {
    this.filteruserIDList = this.userIdList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private roleIdListHandleFilter(value) {
    this.filterroleIdList = this.roleIdList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  private currentWorkspaceIdListHandleFilter(value) {
    this.filtercurrentWorkspaceIdList = this.currentWorkspaceIdList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  workspaceSelectionChange($event) {
    this.loadUserData();
    this.loadGridData();
  }

  loadUserData() {
    const workspaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;
    this.commonService.getAll('general', 'User').subscribe(eventResult => {
      this.filteruserIDList = this.userIdList = eventResult.results;
    });
  }
}
