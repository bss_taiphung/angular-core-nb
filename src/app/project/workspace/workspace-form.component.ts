import {
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import 'rxjs/add/observable/fromEvent';

import {CommonService} from '../../shared/services/common.service';
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';

import {
  CreateWorkspaceDto,
  WorkspaceDto
} from './service/workspace.model';
import {WorkspaceService} from './service/workspace.service'
import {CreateWorkspaceTeamDto} from './service/workspace-team.model';
import {WorkspaceTeamService} from './service/workspace-team.service';

@Component({
  selector: 'kendo-grid-workspace-form',
  styles: [
    'input[type=text] { width: 100%; }'
  ],
  templateUrl: './html/workspace-form.component.html'
})
export class WorkspaceFormComponent {
  public openWorkspaceForm = false;

  public editForm: FormGroup = new FormGroup({
    'workspaceId': new FormControl(),
    'workspaceName': new FormControl('', Validators.required)
  });

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private domSanitizer: DomSanitizer,
              private workspaceService: WorkspaceService,
              private eqmSettingsService: EqmSettingsService,
              private workspaceTeamService: WorkspaceTeamService,) {
  }

  @Input() public isNew = false;

  @Input()
  public set model(workspace: WorkspaceDto) {

    this.editForm.reset(workspace);
    this.openWorkspaceForm = workspace !== undefined;
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<WorkspaceDto> = new EventEmitter();

  public onSave(e): void {
    e.preventDefault();
    this.save.emit(this.editForm.value);

  }

  public saveHandler(workSpacedata) {
    workSpacedata = this.editForm.value;
    if (this.isNew) {
      workSpacedata.active = (workSpacedata.active == null) ? false : workSpacedata.active;
      const inputWorkSpacedata: CreateWorkspaceDto = workSpacedata;
      inputWorkSpacedata.active = true;
      this.workspaceService.create(inputWorkSpacedata,
        response => {
          let workspadedetails = response;
          let inputData: CreateWorkspaceTeamDto = new CreateWorkspaceTeamDto();
          inputData.userId = this.eqmSettingsService.getUser().userId;
          inputData.workspaceId = response.workspaceId;
          inputData.isAdmin = true;
          inputData.active = true;
          this.workspaceTeamService.create(inputData,
            response => {
              this.toasterService.success('', 'Workspace Saved Successfully');
              this.cancel.emit(workspadedetails);
            },
            error => {
              this.toasterService.errorMessage(error);
              this.cancel.emit();
            }
          );
        },
        error => {
          this.toasterService.errorMessage(error);
          this.cancel.emit();
        }
      );
    }
    else {
      const inputWorkSpacedata: WorkspaceDto = workSpacedata;
      this.workspaceService.update(inputWorkSpacedata,
        response => {
          this.toasterService.success('', 'Workspace Updated Successfully');
          this.cancel.emit(inputWorkSpacedata);
        },
        error => {
          this.toasterService.errorMessage(error);
          this.cancel.emit();
        }
      );
    }
    this.openWorkspaceForm = false;
  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }

  private closeForm(): void {
    this.openWorkspaceForm = false;
    this.cancel.emit();
  }
}
