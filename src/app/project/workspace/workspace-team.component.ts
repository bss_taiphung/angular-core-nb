import {
  Component,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs/Observable';

import {HubConnection} from '@aspnet/signalr-client';

import {SortDescriptor, State} from '@progress/kendo-data-query';
import {DataStateChangeEvent, GridDataResult, PageChangeEvent} from '@progress/kendo-angular-grid';


import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {SelectListDto} from '../../../app/shared/models/selectListDto';
import {ProjectSettingsService} from '../shared/services/projectsettings.service';

import {
  CreateWorkspaceTeamDto,
  IWorkspaceTeamDto,
  WorkspaceTeamDto
} from './service/workspace-team.model';
import {
  CreateWorkspaceDto,
  WorkspaceDto
} from './service/workspace.model';

import {WorkspaceTeamService} from './service/workspace-team.service'
import {WorkspaceService} from './service/workspace.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'workspace-team',
  templateUrl: './html/workspace-team.component.html',
  styleUrls: ['./css/workspace.component.css'],
  encapsulation: ViewEncapsulation.None
})


export class WorkspaceTeamComponent implements OnInit {

  data: IWorkspaceTeamDto[];
  public formGroup: FormGroup;
  public view: Observable<GridDataResult>;
  private editedRowIndex: number;
  public addNewTeam: boolean;
  public editDataItem: CreateWorkspaceTeamDto;
  public isNew: boolean;
  public pageSize = 10;
  public skip = 0;
  public state: State = {
    skip: 0,
    take: 5,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };
  private isAllowAddUpdate = false;
  public workspaceTeamForm: FormGroup = new FormGroup({
    'wsTeamMemberId': new FormControl(),
    'workspaceId': new FormControl(),
    'userId': new FormControl('', Validators.required),
    'isAdmin': new FormControl(false),
    'active': new FormControl(false)
  });

  public gridView: GridDataResult;
  public deleteDataItem: any;
  private sort: SortDescriptor[] = [];
  public isactive = true;
  public isDuplicateTeamMember: boolean;
  public searchtext = '';

  public selWorkspaceName = '';

  public currentWorkSpaceDeails: WorkspaceDto;
  public currentWorkspaceName = '';
  public userList: SelectListDto[];
  public filterUserIdList: SelectListDto[];


  private hubConnection: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  private workspaceSettings: any = [];

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private workspaceService: WorkspaceService,
              private workspaceTeamService: WorkspaceTeamService,
              private projectSettingsService: ProjectSettingsService) {
    this.workspaceSettings = this.projectSettingsService.getWorkSpaceSettings();
    if (this.workspaceSettings &&
      (this.workspaceSettings.canCreateWorkspaces === true ||
        this.workspaceSettings.globalWorkspaceAdmin === true)) {
      this.isAllowAddUpdate = true;
    } else {
      this.isAllowAddUpdate = false;
    }

  }

  ngOnInit() {
    if (this.projectSettingsService.validateProjectWorkspace()) {
      this.commonService.getAll('General', 'User').subscribe(eventResult => {
        this.filterUserIdList = this.userList = eventResult.results;
      });
      this.workspaceSettings = this.projectSettingsService.getWorkSpaceSettings();
      // this.loadGridData();
      // this.signalRConnection();
    }
  }

  signalRConnection() {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    const workspaceid = this.workspaceSettings.currentWorkspaceId;
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');

        this.hubConnection
          .invoke('Subscribe', 'Projects', 'WorkspaceTeam', 'WorkspaceId', workspaceid)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
      const _responseText = messageJson;
      let result200: any = null;
      const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      const resultData200 = this.commonService.toCamel(data);
      result200 = resultData200 ? WorkspaceTeamDto.fromJS(resultData200) : new WorkspaceTeamDto();

      const dataExist = this.containsObject(result200, this.gridView.data);

      if (operation.toLowerCase() === 'insert' && !dataExist) {
        this.gridView.data.unshift(result200);
      }

      if (operation.toLowerCase() === 'update') {
        this.gridView.data.forEach((element, index) => {
          if (element.wsTeamMemberId === result200.wsTeamMemberId) {
            //
            if (this.isactive) {
              if (result200.active == this.isactive) {
                this.gridView.data[index] = result200;
              } else {
                operation = 'delete';
              }
            } else {
              this.gridView.data[index] = result200;
            }
          }
        });
      }

      if (operation.toLowerCase() === 'delete' && dataExist) {
        let index = null;
        this.gridView.data.forEach((element, i) => {
          if (element.wsTeamMemberId === result200.wsTeamMemberId) {
            index = i;
          }
        });
        if (index !== null) {
          this.gridView.data.splice(index, 1);
        }
      }
    });
  }

  containsObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].wsTeamMemberId === obj.wsTeamMemberId) {
        return true;
      }
    }

    return false;
  }

  // Dialog box
  opened = false;
  loading = true;
  gridLoading = true;


  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.loadGridData();
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
    this.loadGridData();
  }

  public showActive() {
    this.isactive = !this.isactive;
    this.gridLoading = true;
    this.loadGridData();
  }

  private loadGridData(): void {
    this.gridLoading = true;
    const currentWorkspaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;
    this.workspaceTeamService.getAllByWorkspaceId(this.pageSize, this.skip, this.sort, this.isactive, this.searchtext, currentWorkspaceId).subscribe(eventResult => {
      this.gridView = {
        data: eventResult.results,
        total: eventResult.count
      };
      this.loading = false;
      this.gridLoading = false;
    });
  }

  public saveWorkspaceTeam() {
    if (this.isNew) {
      const inputData: CreateWorkspaceTeamDto = this.workspaceTeamForm.value;
      inputData.workspaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;

      this.workspaceTeamService.create(inputData,
        response => {
          if (response == false) {
            this.gridLoading = false;
            this.isDuplicateTeamMember = true;
            return false;
          }

          if (response != null && response.wsTeamMemberId > 0) {
            this.toasterService.success('', 'Team Member Saved Successfully');
            this.addNewTeam = false;
          }
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    } else {

      const editInputData = this.workspaceTeamForm.value;
      editInputData.workspaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;

      this.workspaceTeamService.update(editInputData,
        response => {
          if (response) {
            this.toasterService.success('', 'Team Member Updated Successfully');
            this.addNewTeam = false;
          }
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  public userIdHandleFilter(filter: any): void {
    this.filterUserIdList = this.userList.filter((u) => u.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
    this.isDuplicateTeamMember = false;
  }

  timeout = null;

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridLoading = true;

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.doSearch(state, this);
    }, 1000);
  }

  doSearch(state: any, that) {
    if (state.filter) {
      const search = that.commonService.getFilter(state.filter.filters, that.isactive);
      const workspaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;
      that.workspaceTeamService.getAll(state.take, state.skip, that.sort, this.isactive, search, workspaceId)
        .subscribe(eventResult => {
          that.gridView = {
            data: eventResult.results,
            total: eventResult.count
          };
          that.loading = false;
          that.gridLoading = false;
        });

      this.skip = state.skip;
      this.pageSize = state.take;
    }
    else {
      that.loadGridData();
    }
  }

  // Close WorkspaceTeam dialog
  private closeForm(): void {
    this.addNewTeam = false;
    //this.openWorkspaceTeamForm = false;
    //this.closedialogWorkspaceTeam.emit();
  }

  // Open addTeam form
  public addTeam() {
    this.isDuplicateTeamMember = false;
    this.editDataItem = new CreateWorkspaceTeamDto();
    this.addNewTeam = true;
    this.workspaceTeamForm.controls['userId'].setValue('');
    this.workspaceTeamForm.reset();
    this.isNew = true;
  }

  // Close add WorkspaceTeam dialog
  public closeAddTeam() {
    this.editDataItem = undefined;

    this.workspaceTeamForm.reset();
    this.addNewTeam = false;
  }

  // Delete confirm
  public close(status) {
    const $this = this;
    if (status) {
      this.workspaceTeamService.delete($this.deleteDataItem,
        response => {
          if (response) {
            this.toasterService.success('', 'Team Member Removed Successfully');
          } else {
            this.toasterService.error('', 'Workspace have atleast one Admin');
          }
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
    $this.deleteDataItem = null;
    $this.opened = false;
  }

  // Delete dialog
  public open() {
    this.opened = true;
  }

  public ActionData: Array<any> = [
    {
      text: 'Delete',
      icon: 'trash'
    }];

  public onAction(e, workspaceTeam) {
    this.workspaceSettings = this.projectSettingsService.getWorkSpaceSettings();
    if (this.workspaceSettings.isAdmin) {
      if (e === undefined) {
        this.editDataItem = workspaceTeam;
        this.isDuplicateTeamMember = false;
        this.isNew = false;
        this.workspaceTeamForm.reset(workspaceTeam);
        this.addNewTeam = true;

      } else {
        if (e.text === 'Delete') {
          this.addNewTeam = false;
          this.deleteDataItem = workspaceTeam;
          this.open();
        }
      }
    } else {
      this.toasterService.error('', 'You have no permission');
    }

  }

  workspaceSelectionChange($event) {
    if ($event !== undefined) {
      this.currentWorkspaceName = $event.workspaceName;
    }
    this.currentWorkSpaceDeails = $event;
    this.loadGridData();
    this.signalRConnection();
    this.getCurrentWorkspaceDetails();
  }

  public workspacedetails: CreateWorkspaceDto;

  addNewWorkspace() {
    this.workspaceSettings = this.projectSettingsService.getWorkSpaceSettings();
    if (this.workspaceSettings.canCreateWorkspaces === true || this.workspaceSettings.globalWorkspaceAdmin === true) {
      this.isNew = true;
      this.workspacedetails = new CreateWorkspaceDto();
    } else {
      this.toasterService.error('', 'You have no permission');
    }

  }

  editWorkspace() {
    this.isNew = false;
    this.workspacedetails = this.currentWorkSpaceDeails;
  }

  changeCurrentWorkSpace: any;

  public cancelHandler($event = undefined) {
    if ($event !== undefined) {
      if (this.isNew) {
        this.currentWorkSpaceDeails = $event;
        this.changeCurrentWorkSpace = $event;
      }
      this.currentWorkspaceName = $event.workspaceName;
    }
    this.workspacedetails = undefined;
    this.isNew = true;
  }

  getCurrentWorkspaceDetails() {
    const currentWorkspaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;
    this.workspaceService.getById(currentWorkspaceId).subscribe(result => {
      this.currentWorkSpaceDeails = result;
      this.currentWorkspaceName = this.currentWorkSpaceDeails.workspaceName;
    });
  }
}
