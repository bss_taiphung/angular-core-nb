import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';


// region IWorkspaceDto

export interface IWorkspaceDto {
  workspaceId: number;
  workspaceName: string;
  active: boolean;
}

export class WorkspaceDto implements IWorkspaceDto {
  workspaceId: number;
  workspaceName: string;
  createdByName: string;
  createdDate: string;
  createdDateTime: string;
  modifiedByName: string;
  modifiedDateTime: string;
  createdBy: number;
  modifiedBy: number;
  active: boolean;

  static fromJS(data: any): WorkspaceDto {
    const result = new WorkspaceDto();
    result.init(data);
    return result;
  }

  constructor(data?: IWorkspaceDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.workspaceId = data['workspaceId'];
      this.workspaceName = data['workspaceName'];
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.active = data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['workspaceId'] = this.workspaceId;
    data['workspaceName'] = this.workspaceName;
    data['createdByName'] = this.createdByName;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;
    return data;
  }
}

// endregion

// region ICreateWorkspaceDto

export interface ICreateWorkspaceDto {
  workspaceId: number;
  workspaceName: string;
  active: boolean;
}

export class CreateWorkspaceDto implements ICreateWorkspaceDto {
  workspaceId: number;
  workspaceName: string;
  active = true;

  static fromJS(data: any): WorkspaceDto {
    const result = new WorkspaceDto();
    result.init(data);
    return result;
  }

  constructor(data?: IWorkspaceDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.workspaceId = data['workspaceId'];
      this.workspaceName = data['workspaceName'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['workspaceId'] = this.workspaceId;
    data['workspaceName'] = this.workspaceName;
    data['active'] = (this.active == null) ? false : this.active;

    return data;
  }
}

// endregion


// region IPagedResultDtoOfWorkspaceDto

export interface IPagedResultDtoOfWorkspaceDto {
  results: WorkspaceDto[];
  count: number;
}

export class PagedResultDtoOfWorkspaceDto implements IPagedResultDtoOfWorkspaceDto {
  results: WorkspaceDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfWorkspaceDto {
    const result = new PagedResultDtoOfWorkspaceDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfWorkspaceDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(WorkspaceDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion
