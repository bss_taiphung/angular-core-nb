import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {
  HttpClient,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

import {CommonService} from '../../../shared/services/common.service';

import {
  CreateWorkspaceTeamDto, PagedResultDtoOfWorkspaceTeamDto,
  WorkspaceTeamDto
} from './workspace-team.model';
import {environment} from '../../../../environments/environment';

@Injectable()
export class WorkspaceTeamService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService, private http: HttpClient) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/projects/api/WorkspaceTeam/';
  }

  create(input: CreateWorkspaceTeamDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'InsertAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  update(input: WorkspaceTeamDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'UpdateAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  delete(input: WorkspaceTeamDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'DeleteTeamMember';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  /*delete(id: any, successCallback: any, errorCallback: any): any {
   let url = this.apiBaseUrl + "DeleteTeamMember";
   let params = new HttpParams().set('id', id);

   let options = {
     params: params
   };
   this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
  }*/

  getAll(take: number,
         skip: number,
         sort: any,
         isactive: boolean,
         filter: string,
         workspaceId: number): Observable<PagedResultDtoOfWorkspaceTeamDto> {
    const url = this.apiBaseUrl + 'GetPagedQuery';

    let orderBy = '';
    if (sort != undefined && sort.length > 0) {
      for (const item of sort) {
        orderBy = this.commonService.firstUpper(item.field) + ' ' + (item.dir == undefined ? 'asc' : item.dir);
      }
    } else {
      orderBy = 'wsTeamMemberId desc';
    }

    if (isactive) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += ' Active eq true';
    }

    if (workspaceId > 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += ' WorkspaceId eq ' + workspaceId + '';
    }

    const params = new HttpParams()
      .set('$top', take.toString())
      .set('$skip', skip.toString())
      .set('$count', 'true')
      .set('$orderby', orderBy.toString())
      .set('$filter', filter.toString());


    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      const result200 = PagedResultDtoOfWorkspaceTeamDto.fromJS(response);
      return Observable.of(result200);
    });
  }


  getAllByWorkspaceId(take: number,
                      skip: number,
                      sort: any,
                      isactive: boolean,
                      searchtext: string,
                      workspaceId: number = 0,
                      filter: string = ''): Observable<PagedResultDtoOfWorkspaceTeamDto> {
    const url = this.apiBaseUrl + 'GetPagedQuery';

    // Order by Clause
    let orderBy = '';
    if (sort !== undefined && sort.length > 0) {
      for (const item of sort) {
        orderBy = this.commonService.firstUpper(item.field) + ' ' + (item.dir == undefined ? 'asc' : item.dir);
      }
    } else {
      orderBy = 'wsTeamMemberId desc';
    }

    if (isactive) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += 'Active eq true';
    }

    if (workspaceId > 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += 'WorkspaceId eq ' + workspaceId;
    }


    const params = new HttpParams()
      .set('$top', take.toString())
      .set('$skip', skip.toString())
      .set('$count', 'true')
      .set('$orderby', orderBy.toString())
      .set('$filter', filter.toString());

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      const result200 = PagedResultDtoOfWorkspaceTeamDto.fromJS(response);
      return Observable.of(result200);
    });
  }
}

