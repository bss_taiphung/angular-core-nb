import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

// region IWorkspaceTeamDto

export interface IWorkspaceTeamDto {
  wsTeamMemberId: number;
  workspaceId: number;
  userId: number;
  isAdmin: boolean;
  active: boolean;
}

export class WorkspaceTeamDto implements IWorkspaceTeamDto {
  wsTeamMemberId: number;
  workspaceId: number;
  userId: number;
  userDisplayName: string;
  isAdmin: boolean;
  createdDate: string;
  createdDateTime: string;
  modifiedByName: string;
  modifiedDateTime: string;
  createdBy: number;
  modifiedBy: number;
  active: boolean;

  static fromJS(data: any): WorkspaceTeamDto {
    const result = new WorkspaceTeamDto();
    result.init(data);
    return result;
  }

  constructor(data?: IWorkspaceTeamDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.wsTeamMemberId = data['wsTeamMemberId'];
      this.workspaceId = data['workspaceId'];
      this.userId = data['userId'];
      this.userDisplayName = data['userDisplayName'];
      this.isAdmin = data['isAdmin'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.active = data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['wsTeamMemberId'] = this.wsTeamMemberId;
    data['workspaceId'] = this.workspaceId;
    data['userId'] = this.userId;
    data['userDisplayName'] = this.userDisplayName;
    data['isAdmin'] = this.isAdmin;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;

    return data;
  }
}

// endregion


// region IWorkspaceTeamDto

export interface ICreateWorkspaceTeamDto {
  wsTeamMemberId: number;
  workspaceId: number;
  userId: number;
  isAdmin: boolean;
  active: boolean;
}

export class CreateWorkspaceTeamDto implements ICreateWorkspaceTeamDto {
  wsTeamMemberId: number;
  workspaceId: number;
  userId: number;
  userDisplayName: string;
  isAdmin: boolean;
  active = true;

  static fromJS(data: any): WorkspaceTeamDto {
    const result = new WorkspaceTeamDto();
    result.init(data);
    return result;
  }

  constructor(data?: IWorkspaceTeamDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.wsTeamMemberId = data['wsTeamMemberId'];
      this.workspaceId = data['workspaceId'];
      this.userId = data['userId'];
      this.userDisplayName = data['userDisplayName'];
      this.isAdmin = (data['isAdmin'] == null) ? false : data['isAdmin'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['wsTeamMemberId'] = this.wsTeamMemberId;
    data['workspaceId'] = this.workspaceId;
    data['userId'] = this.userId;
    data['userDisplayName'] = this.userDisplayName;
    data['isAdmin'] = (this.isAdmin == null) ? false : this.isAdmin;
    data['active'] = (this.active == null) ? false : this.active;

    return data;
  }
}

// endregion


// region IPagedResultDtoOfWorkspaceTeamDto
export interface IPagedResultDtoOfWorkspaceTeamDto {
  results: WorkspaceTeamDto[];
  count: number;
}

export class PagedResultDtoOfWorkspaceTeamDto implements IPagedResultDtoOfWorkspaceTeamDto {
  results: WorkspaceTeamDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfWorkspaceTeamDto {
    const result = new PagedResultDtoOfWorkspaceTeamDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfWorkspaceTeamDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(WorkspaceTeamDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion

// region IResultDtoOfWorkspaceTeamDto
export interface IResultDtoOfWorkspaceTeamDto {
  results: WorkspaceTeamDto[];
  count: number;
}

export class ResultDtoOfWorkspaceTeamDto implements IResultDtoOfWorkspaceTeamDto {
  results: WorkspaceTeamDto[];
  count: number;

  static fromJS(data: any): ResultDtoOfWorkspaceTeamDto {
    const result = new ResultDtoOfWorkspaceTeamDto();
    result.init(data);
    return result;
  }

  constructor(data?: IResultDtoOfWorkspaceTeamDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {

    if (data && data != null) {
      if (data && data.constructor === Array) {
        this.results = [];
        for (const item of data) {
          this.results.push(WorkspaceTeamDto.fromJS(item));
        }
      }

    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data = [];
      for (const item of this.results) {
        data.push(item.toJSON());
      }
    }

    return data;
  }
}

// endregion
