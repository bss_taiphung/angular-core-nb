import {
  Component,
  HostListener,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Observable} from 'rxjs/Observable';

import {HubConnection} from '@aspnet/signalr-client';
import {DataStateChangeEvent, GridDataResult, PageChangeEvent} from '@progress/kendo-angular-grid';
import {SortDescriptor, State} from '@progress/kendo-data-query';

import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {CommonService} from '../../shared/services/common.service';
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {ProjectSettingsService} from '../shared/services/projectsettings.service';

import {
  CreateWorkspaceDto,
  IWorkspaceDto,
  WorkspaceDto
} from './service/workspace.model';
import {CreateWorkspaceTeamDto} from './service/workspace-team.model';
import {WorkspaceTeamService} from './service/workspace-team.service';
import {WorkspaceService} from './service/workspace.service'
import {environment} from '../../../environments/environment';

@Component({
  selector: 'workspace',
  templateUrl: './html/workspace.component.html',
  styleUrls: ['./css/workspace.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class WorkspaceComponent implements OnInit {

  data: IWorkspaceDto[];
  public formGroup: FormGroup;
  public view: Observable<GridDataResult>;
  private editedRowIndex: number;
  public editDataItem: CreateWorkspaceDto;

  public isNew: boolean;
  public openWorkspaceTeamForm: boolean;
  public openProjectGroupForm: boolean;
  public openRoleDialog: boolean;
  public openUserSettingsDialog: boolean;
  public pageSize = 10;
  public skip = 0;
  public state: State = {
    skip: 0,
    take: 5,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };
  public gridView: GridDataResult;
  public deleteDataItem: any;
  private sort: SortDescriptor[] = [];
  public isactive: boolean = true;
  public searchtext: string = '';
  private hubConnection: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  private isAllowAddUpdate: boolean = false;

  private userId: any;
  private workspaceSettings: any;

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private workspaceService: WorkspaceService,
              private projectSettingsService: ProjectSettingsService,
              private workspaceTeamService: WorkspaceTeamService,
              private eqmSettingsService: EqmSettingsService) {
    this.userId = this.eqmSettingsService.getUser().userId;

  }


  ngOnInit() {
    this.workspaceSettings = this.projectSettingsService.getWorkSpaceSettings();
    if (this.workspaceSettings &&
      (this.workspaceSettings.canCreateWorkspaces === true ||
        this.workspaceSettings.globalWorkspaceAdmin === true)) {
      this.isAllowAddUpdate = true;
    } else {
      this.isAllowAddUpdate = false;
    }
    this.loadGridData();
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection.start().then(() => {
      console.log('Connection started for workspace!');

      this.hubConnection
        .invoke('Subscribe', 'Projects', 'Workspace', 'CreatedBy', this.workspaceSettings.userId)
        .catch(err => console.error(err));
    })
      .catch(err => console.log('Error while establishing connection :('));
    this.hubConnection.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
      const _responseText = messageJson;
      let result200: any = null;
      let data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      let resultData200 = this.commonService.toCamel(data);
      result200 = resultData200 ? WorkspaceDto.fromJS(resultData200) : new WorkspaceDto();

      var dataExist = this.containsObject(result200, this.gridView.data);

      if (operation.toLowerCase() === 'insert' && !dataExist) {
        this.gridView.data.unshift(result200);
      }

      if (operation.toLowerCase() === 'update') {
        this.gridView.data.forEach((element, index) => {
          if (element.workspaceId === result200.workspaceId) {
            //
            if (this.isactive) {
              if (result200.active == this.isactive) {
                this.gridView.data[index] = result200;
              } else {
                operation = 'delete';
              }
            } else {
              this.gridView.data[index] = result200;
            }
          }
        });
      }

      if (operation.toLowerCase() === 'delete' && dataExist) {
        var index = null;
        this.gridView.data.forEach((element, i) => {
          if (element.workspaceId === result200.workspaceId) {
            index = i;
          }
        });
        if (index !== null) {
          this.gridView.data.splice(index, 1);
        }
      }
    });
  }

  containsObject(obj, list) {
    var x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].workspaceId === obj.workspaceId) {
        return true;
      }
    }

    return false;
  }

  // Dialog box
  opened: boolean = false;
  loading: boolean = true;
  gridLoading: boolean = true;

  public close(status) {
    var $this = this;

    if (status) {
      this.workspaceService.delete(this.deleteDataItem.workspaceId,
        response => {
          if (response)
            this.toasterService.success('', 'Workspace Removed Successfully');
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
    $this.deleteDataItem = null;
    $this.opened = false;
  }

  public open() {
    this.opened = true;
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.loadGridData();
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
    this.loadGridData();
  }

  private loadGridData(): void {
    this.gridLoading = true;
    let userid = this.eqmSettingsService.getUser().userId;
    this.workspaceService.getAll(this.pageSize, this.skip, this.sort, this.isactive, this.searchtext, userid).subscribe(eventResult => {
      this.gridView = {
        data: eventResult.results,
        total: eventResult.count
      };
      this.loading = false;
      this.gridLoading = false;
    });
  }

  public addHandler({sender}) {
    this.workspaceSettings = this.projectSettingsService.getWorkSpaceSettings();
    if (this.workspaceSettings &&
      (this.workspaceSettings.canCreateWorkspaces === true || this.workspaceSettings.globalWorkspaceAdmin === true)) {
      this.closeEditor(sender);
      this.editDataItem = new CreateWorkspaceDto();
      this.isNew = true;
    } else {
      this.toasterService.error('', 'You do not have permission to add new workspace.');
    }
  }

  public editHandler({dataItem}) {
    this.editDataItem = dataItem;
    this.isNew = false;
  }

  public saveHandler(workSpacedata) {
    if (this.isNew) {
      workSpacedata.active = (workSpacedata.active == null) ? false : workSpacedata.active;
      const inputWorkSpacedata: CreateWorkspaceDto = workSpacedata;
      inputWorkSpacedata.active = true;
      this.workspaceService.create(inputWorkSpacedata,
        response => {
          var inputData: CreateWorkspaceTeamDto = new CreateWorkspaceTeamDto();
          inputData.userId = this.eqmSettingsService.getUser().userId;
          inputData.workspaceId = response.workspaceId;
          inputData.isAdmin = true;
          inputData.active = true;
          this.workspaceTeamService.create(inputData,
            response => {
              this.toasterService.success('', 'Workspace Saved Successfully');
            },
            error => {
              this.toasterService.errorMessage(error);
            }
          );
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
    else {
      const inputWorkSpacedata: WorkspaceDto = workSpacedata;
      this.workspaceService.update(inputWorkSpacedata,
        response => {
          this.toasterService.success('', 'Workspace Updated Successfully');
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public cancelHandler($event = undefined) {
    if ($event !== undefined) {
      this.loadGridData();
    }
    this.editDataItem = undefined;
    this.formGroup = undefined;
  }

  public removeHandler({dataItem}) {
    this.deleteDataItem = dataItem;
    this.open();
  }

  public showActive() {
    this.isactive = !this.isactive;
    this.loadGridData();
  }

  public onSearchKeyup() {
    this.skip = 0;
    this.pageSize = 10;
    this.loadGridData();

  }


  public ActionData: Array<any> = [
    {
      text: 'Delete',
      icon: 'trash'
    }];

  public onAction(e, dataItem) {
    this.workspaceSettings = this.projectSettingsService.getWorkSpaceSettings();
    if (this.workspaceSettings &&
      (this.workspaceSettings.canCreateWorkspaces === false ||
        this.workspaceSettings.globalWorkspaceAdmin === false ||
        this.workspaceSettings.globalWorkspaceViewing)) {
      this.toasterService.error('', 'You have no permission.');
    }
    else {
      if (e === undefined) {
        this.editDataItem = dataItem;
        this.isNew = false;
      } else {
        if (e.text === 'Delete') {
          this.deleteDataItem = dataItem;
          this.open();
        }
      }
    }

  }

  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {
      this.closeAllDialog();
    }
  }


  timeout = null;

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridLoading = true;

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.doSearch(state, this);
    }, 1000);
  }

  doSearch(state: any, that) {
    if (state.filter) {
      var search = that.commonService.getFilter(state.filter.filters, that.isactive);
      that.workspaceService.getAllWtihFilter(state.take, state.skip, that.sort, this.isactive, search)
        .subscribe(eventResult => {
          that.gridView = {
            data: eventResult.results,
            total: eventResult.count
          };
          that.loading = false;
          that.gridLoading = false;
        });

      this.skip = state.skip;
      this.pageSize = state.take;
    }
    else {
      that.loadGridData();
    }
  }

  public closedialogWorkspaceTeam() {
    this.openWorkspaceTeamForm = false;
  }

  public closeAllDialog() {
    this.cancelHandler();
    this.close(false);
    this.closedialogWorkspaceTeam();
  }
}
