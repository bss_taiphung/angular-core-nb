import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {Router} from '@angular/router';
import {FormGroup} from '@angular/forms';

import {SelectListDto} from '../../../shared/models/selectListDto'
import {ToasterHelperService} from '../../../shared/services/toasterHelper.service';
import {CommonService} from '../../../shared/services/common.service';
import {EqmSettingsService} from '../../../shared/services/eqmsettings.service';

import {UsersettingsDto} from '../../usersettings/service/usersettings.model';
import {ProjectSettingsService} from '../services/projectsettings.service';
import {CreateWorkspaceDto} from 'app/project/workspace/service/workspace.model';

@Component({
  selector: 'grid-workspace-dropdown',
  templateUrl: './html/workspacedropdown.component.html',
  encapsulation: ViewEncapsulation.None,
})

export class WorkspaceDropdownComponent implements OnInit, AfterViewInit {

  public formGroup: FormGroup;

  public workspaceSettings: any;
  public selectedWorkspaceId: any;

  public workspaceList: SelectListDto[];
  public workspaceListVm: SelectListDto[];

  public workspaceSaving = false;
  public isNew = false;
  public mss: any;
  public helpLink: string;
  public page: string;
  isWorkspanceNotSeletected = false;
  isCreatWorkspanceAuth = false;
  public workspacedetails: CreateWorkspaceDto;

  @Input()
  public set pageName(data: any) {
    if (data) {
      this.page = data;
    }
  }

  @Input()
  public set selectCurrentWorkSpace(workspacedetails: any) {
    if (workspacedetails) {
      this.selectedWorkspaceId = workspacedetails.workspaceId;
      this.setUserSetting(workspacedetails.workspaceId);
    }
  }

  @Output() workspaceSelectionChange: EventEmitter<any> = new EventEmitter();

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private projectSettingsService: ProjectSettingsService,
              private eqmSettingsService: EqmSettingsService,
              private router: Router) {
    this.workspaceSettings = projectSettingsService.getWorkSpaceSettings();
  }

  ngOnInit() {
    if (this.workspaceSettings === undefined) {
      return;
    }
    this.setUserSetting();
  }

  setUserSetting(workSpaceID: number = 0) {
    let userId = 0;
    this.workspaceSettings = this.projectSettingsService.getWorkSpaceSettings();
    if (!this.workspaceSettings.globalWorkspaceAdmin && !this.workspaceSettings.globalWorkspaceViewing) {
      userId = this.eqmSettingsService.getUser().userId;
    }

    this.commonService.getAllWithUserId(userId, 'Projects', (this.workspaceSettings.globalWorkspaceAdmin && this.workspaceSettings.globalWorkspaceViewing ? 'Workspace' : 'WorkspaceTeam')).subscribe(eventResult => {
      this.workspaceListVm = eventResult.results;

      this.workspaceList = this.workspaceListVm = eventResult.results;
      if (this.workspaceList.length === 0) {
        if (this.workspaceSettings.canCreateWorkspaces === true) {
          this.isNew = true;
          this.workspacedetails = new CreateWorkspaceDto();
        } else {
          this.router.navigate(['app/general/noaccess']);
        }
      } else {
        this.selectedWorkspaceId = this.workspaceSettings.currentWorkspaceId;
        if (this.workspaceSettings.currentWorkspaceId == null) {
          this.isWorkspanceNotSeletected = true;
        }
      }
      if (workSpaceID > 0) {
        this.selectedWorkspaceId = workSpaceID;
        this.updateCurrentWorkspaceId();
      }

    });
  }

  ngAfterViewInit() {
    this.workspaceSettings = this.projectSettingsService.getWorkSpaceSettings();
    if (this.workspaceSettings.currentWorkspaceId !== null) {
      this.workspaceSelectionChange.emit(this.workspaceSettings.currentWorkspaceId);
    }
  }

  public workspaceHandleFilter(filter: any): void {
    this.workspaceList = this.workspaceListVm.filter((s) => s.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  public updateCurrentWorkspaceId() {
    let data = new UsersettingsDto();
    if (this.selectedWorkspaceId != undefined) {
      this.workspaceSaving = true;
      data.active = true;
      data.userSettingsId = this.workspaceSettings.userSettingsId;
      data.userId = this.workspaceSettings.userId;
      data.currentWorkspaceId = this.selectedWorkspaceId;
      data.roleId = this.workspaceSettings.roleId;
      this.projectSettingsService.update(data,
        response => {
          this.toasterService.success('', 'workspace Updated Successfull');
          this.projectSettingsService.workSpaceUserSettings().subscribe(eventResult => {
            this.workspaceSaving = false;
            this.workspaceSelectionChange.emit(eventResult);
          });
        },
        error => {
          this.toasterService.errorMessage(error);
          this.workspaceSaving = false;
        }
      );
    }
  }

  public onWorkspaceSelectionChange(changedWorkspaceId: any): void {
    if (changedWorkspaceId) {
      if (this.selectedWorkspaceId !== changedWorkspaceId.value) {
        this.isWorkspanceNotSeletected = false;
        this.selectedWorkspaceId = changedWorkspaceId.value;
        this.updateCurrentWorkspaceId();
      } else {
        this.workspaceSelectionChange.emit(this.selectedWorkspaceId);
      }
    }
  }

  closeSelectWorkspaceDd() {
    this.isWorkspanceNotSeletected = false;
  }

  closeAuthForm() {
    this.isCreatWorkspanceAuth = false;
  }

  public cancelHandler($event = undefined) {

    if ($event !== undefined) {
      this.setUserSetting();
      this.workspacedetails = undefined;
      this.isNew = false;
    }
    else {
      this.isNew = true;
      this.workspacedetails = new CreateWorkspaceDto();
    }
  }

}
