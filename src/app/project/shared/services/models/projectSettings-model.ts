// region IProjectSettingsDto

export interface IProjectSettingsDto {
  userSettingsId: number;
  userId: number;
  displayName: string;
  currentWorkspaceId: number;
  workspaceName: string;
  roleId: number;
  roleName: string;
  canCreateWorkspaces: boolean;
  globalWorkspaceViewing: boolean;
  globalWorkspaceAdmin: boolean;
  isAdmin: boolean;
}

export class ProjectSettingsDto implements IProjectSettingsDto {
  userSettingsId: number;
  userId: number;
  displayName: string;
  currentWorkspaceId: number;
  workspaceName: string;
  roleId: number;
  roleName: string;
  canCreateWorkspaces: boolean;
  globalWorkspaceViewing: boolean;
  globalWorkspaceAdmin: boolean;
  isAdmin: boolean;

  static fromJS(data: any): ProjectSettingsDto {
    let result = new ProjectSettingsDto();
    result.init(data);
    return result;
  }

  constructor(data?: IProjectSettingsDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.userSettingsId = data['userSettingsId'];
      this.userId = data['userId'];
      this.displayName = data['displayName'];
      this.currentWorkspaceId = data['currentWorkspaceId'];
      this.workspaceName = data['workspaceName'];
      this.roleId = data['roleId'];
      this.roleName = data['roleName'];
      this.canCreateWorkspaces = data['canCreateWorkspaces'];
      this.globalWorkspaceViewing = data['globalWorkspaceViewing'];
      this.globalWorkspaceAdmin = data['globalWorkspaceAdmin'];
      this.isAdmin = data['isAdmin'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['userSettingsId'] = this.userSettingsId;
    data['userId'] = this.userId;
    data['displayName'] = this.displayName;
    data['currentWorkspaceId'] = this.currentWorkspaceId;
    data['workspaceName'] = this.workspaceName;
    data['roleId'] = this.roleId;
    data['roleName'] = this.roleName;
    data['canCreateWorkspaces'] = this.canCreateWorkspaces;
    data['globalWorkspaceViewing'] = this.globalWorkspaceViewing;
    data['globalWorkspaceAdmin'] = this.globalWorkspaceAdmin;
    data['isAdmin'] = this.isAdmin;
    return data;
  }
}

// endregion

