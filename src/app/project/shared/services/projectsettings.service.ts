import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {
  HttpClient,
  HttpParams
} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {CookieService} from 'ngx-cookie-service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

import {EqmSettingsService} from '../../../shared/services/eqmsettings.service';
import {CommonService} from '../../../shared/services/common.service';
import {ProjectSettingsDto} from './models/projectSettings-model'
import {UsersettingsDto} from '../../usersettings/service/usersettings.model'
import {environment} from '../../../../environments/environment';

@Injectable()
export class ProjectSettingsService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private cookieService: CookieService,
              private router: Router,
              private http: HttpClient,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/Projects/api/UserSettings';
  }

  validateProjectWorkspace() {
    const workspaceSetting = this.getWorkSpaceSettings();
    if (Object.keys(workspaceSetting).length === 0) {
      this.router.navigate(['app/general/unauthorized']);
    } else {
      return true;
    }
  }

  update(input: UsersettingsDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + '/UpdateCurrentWorkSpaceId';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  public setWorkspaceSettings(proj: any) {
    this.commonService.setCookies('ProjectRole', JSON.stringify(proj));
  }

  public getWorkSpaceSettings(): ProjectSettingsDto {
    const cookieData = this.commonService.getCookies('ProjectRole');
    if (cookieData) {
      return ProjectSettingsDto.fromJS(JSON.parse(cookieData));
    } else {
      return null;
    }
  }

  workSpaceUserSettings(): Observable<ProjectSettingsDto> {
    const userId = this.eqmSettingsService.getUser().userId;
    const url = this.apiBaseUrl + '/GetUserSettings';
    const params = new HttpParams().set('userId', userId.toString());
    const options = {
      params: params
    };

    return this.http.request<Response>('get', url, options)
      .flatMap((response) => {
        const project = ProjectSettingsDto.fromJS(this.commonService.toCamel(response));
        this.setWorkspaceSettings(response);
        return Observable.of(project);
      });
  }
}

