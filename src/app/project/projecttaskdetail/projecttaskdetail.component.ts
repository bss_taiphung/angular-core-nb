import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {HubConnection} from '@aspnet/signalr-client';
import {filter, forEach, groupBy, includes, remove, replace} from 'lodash';

import {ProjectTaskDetailService} from './service/projecttaskdetail.service';
import {ProjecTaskDTO} from '../projecttasklist/projecttask/service/projecttask.model'
import {ProjectTagDTO} from './service/projecttag.model';
import {ProjectSettingsService} from '../shared/services/projectsettings.service';
import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';

import {CreateProjectTaskTimeDto, ProjectTaskTimeDto} from './service/projectTaskTime.model';
import {ProjectTaskTimeService} from './service/projectTaskTime.service';
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {TagMasterService} from '../tagmaster/service/tagmaster.service';
import {ProjectDto} from '../projectslist/service/projectslist.model';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'kendo-projects-task-detail',
  templateUrl: './html/projecttaskdetail.component.html',
  styleUrls: ['./css/projecttaskdetail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ProjectTaskDetailComponent {
  azureStorageContainer: string;
  public projecTaskDTO = new ProjecTaskDTO();
  @Input()
  public openDetailViewer = false;
  @Output()
  closeDetailViewer: EventEmitter<any> = new EventEmitter();

  public selectAssignedToList: any = [];
  public selectedprojectTagListDto: ProjectTagDTO[] = [];
  public selectedprojectTagListDtoVM: ProjectTagDTO[] = [];

  public mask: string = '00-00';
  public estimatedMinutes: string;
  public editor;
  public editorContent = `<h3>I am Example content</h3>`;
  public editorOptions = {
    placeholder: 'insert content...'
  };
  private tagList: any = [];
  private projecTaskDtoAssignedTo: any = [];
  private setPointsValue: any;
  private userPermissionModel: any = '';

  @Input()
  public set userModeInfo(userPermissionModel: any) {
    this.userPermissionModel = userPermissionModel;
  }

  private projecTaskDTOSelectedAssignedTo: any = [];
  private projecTaskDTOSelectedTags: any = [];

  public projectTaskTimeListDto: ProjectTaskTimeDto[] = [];
  public projecTaskTimeList: any = [];
  public isShowTaskTime: boolean = false;
  public isShowTaskTimeDetail: boolean = false;
  private chatData: any = [{value: 55}];
  private project: ProjectDto;
  isCompleteTask: boolean = false;
  private completeTaskModelMessage: any = '';

  @Input()
  public set selectedProject(projectListDto: any) {
    if (projectListDto !== undefined) {
      this.project = projectListDto;
    }
  }

  followersList: any;
  assignedToList: any;
  tagsList: any;
  public taskPointList: Array<{ text: string, value?: number }> = [
    {text: 'No estimate', value: null},
    {text: '0', value: 0},
    {text: '1', value: 1},
    {text: '2', value: 2},
    {text: '3', value: 3},
    {text: '4', value: 4},
    {text: '5', value: 5}
  ];

  @Input()
  public set model(temProjecTaskDTO: ProjecTaskDTO) {
    if (temProjecTaskDTO !== undefined) {
      this.projecTaskDTO = temProjecTaskDTO;

      if (this.projecTaskDTO.completedByUserId == null) {
        this.completeTaskModelMessage = 'Are you sure you want to sure complete this task ?';
      } else {
        this.completeTaskModelMessage = 'Are you sure you want to sure uncomplete this task ?';
      }

      if (this.projecTaskDTO.startDate !== null && this.projecTaskDTO.startDate !== undefined)
        this.projecTaskDTO.startDate = new Date(this.projecTaskDTO.startDate.toString());
      if (this.projecTaskDTO.dueDate !== null && this.projecTaskDTO.startDate !== undefined)
        this.projecTaskDTO.dueDate = new Date(this.projecTaskDTO.dueDate.toString());

      this.followersList = [];
      this.assignedToList = [];
      this.tagList = [];
      this.followersList = this.projecTaskDTO.followers.map(function (obj) {
        return {'text': obj.text, value: parseInt(obj.value), 'userAvatar': obj.value};
      });
      this.followersList.forEach(itm => {
        itm.userAvatar = this.commonService.getUserThumbnail(itm.value);
      });

      this.assignedToList = this.projecTaskDTO.assignedTo.map(function (obj) {
        return {'text': obj.text, value: parseInt(obj.value), 'userAvatar': obj.value};
      });

      this.assignedToList.forEach(itm => {
        itm.userAvatar = this.commonService.getUserThumbnail(itm.value);
      });
      this.tagList = this.projecTaskDTO.tags.map(function (obj) {
        return {
          'tagColor': obj.tagColor,
          'tagName': obj.tagName,
          'workspaceTagMasterId': parseInt(obj.workspaceTagMasterId)
        };
      });
      this.getTaskTimeList();
      this.signalRConnectionForTaskUpdate();
      setTimeout(() => {
          this.signalRConnectionForTaskTime();
        },
        3000);
    }
  }


  projectUserAssignedToListDto: any;

  @Input()
  public set projectUserAssignedToList(userList: any) {
    if (userList !== undefined) {

      this.projectUserAssignedToListDto = this.selectAssignedToList = userList;
    }
  }

  projectTagListDto: any;

  @Input()
  public set projectTagList(tagListDto: any) {
    if (tagListDto !== undefined) {
      this.projectTagListDto = this.selectedprojectTagListDto = tagListDto;

    }
  }

  minutesSpent: any;

  constructor(private commonService: CommonService,
              private projectTaskDetailService: ProjectTaskDetailService,
              private projectSettingsService: ProjectSettingsService,
              private projectTaskTimeService: ProjectTaskTimeService,
              private toasterHelperService: ToasterHelperService,
              private eqmSettingsService: EqmSettingsService,
              private tagMasterService: TagMasterService) {
    this.minutesSpent = new Date(0, 0, 0, 0, 0, 0);
    this.azureStorageContainer = environment.azureStorageContainer;

  }

  private hubConnectionTaskDetails: HubConnection;
  private hubConnectionTaskTimes: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  signalRConnectionForTaskUpdate() {
    this.hubConnectionTaskDetails =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnectionTaskDetails
      .start()
      .then(() => {
        console.log('Connection started! ');

        this.hubConnectionTaskDetails
          .invoke('Subscribe',
            'Projects',
            'ProjectTask',
            'ProjectTaskListId',
            this.projecTaskDTO.projectTaskListId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnectionTaskDetails.on('clientOnEntityUpdated',
      (groupName: string, operation: string, messageJson: string) => {
        const _responseText = messageJson;
        let result200: any = null;
        let data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
        let resultData200 = this.commonService.toCamel(data);
        result200 = resultData200 ? ProjecTaskDTO.fromJS(resultData200) : new ProjecTaskDTO();
        if (operation.toLowerCase() === 'update') {
          if (this.projecTaskDTO.projectTaskId === result200.projectTaskId) {
            this.projecTaskDTO = result200;

          }
        }
      });
  }

  signalRConnectionForTaskTime() {
    this.hubConnectionTaskTimes =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnectionTaskTimes
      .start()
      .then(() => {
        console.log('Connection started! ');

        this.hubConnectionTaskTimes
          .invoke('Subscribe',
            'Projects',
            'ProjectTaskTime',
            'ProjectTaskId',
            this.projecTaskDTO.projectTaskId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnectionTaskTimes.on('clientOnEntityUpdated',
      (groupName: string, operation: string, messageJson: string) => {
        const _responseText = messageJson;
        let result200: any = null;
        let data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
        let resultData200 = this.commonService.toCamel(data);
        result200 = resultData200 ? ProjectTaskTimeDto.fromJS(resultData200) : new ProjectTaskTimeDto();
        var dataExist = this.containsObject(result200, this.projecTaskTimeList);
        if (operation.toLowerCase() === 'insert' && !dataExist) {
          this.projecTaskTimeList.push(result200);
        }
        if (operation.toLowerCase() === 'delete' && dataExist) {
          var index = null;
          this.projecTaskTimeList.forEach((element, i) => {
            if (element.projectTaskTimeId === result200.projectTaskTimeId) {
              index = i;
            }
          });
          if (index !== null) {
            this.projecTaskTimeList.splice(index, 1);
          }
        }
      });
  }

  containsObject(obj, list) {
    var x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].projectTaskTimeId === obj.projectTaskTimeId) {
        return true;
      }
    }
    return false;
  }

  public tagHandleFilter(filter: any): void {
    this.selectedprojectTagListDto =
      this.projectTagListDto.filter((s) => s.tagName.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

  addDotsToPoints(value: number) {
    let html = '';
    if (value != null) {
      for (var i = 0; i < value; i++) {
        html += '<span class="dot"></span>';
      }
      for (var i = 0; i < this.taskPointList.length - 2 - value; i++) {
        html += '<span class="dot bg-lightgrey"></span>';
      }
    }

    return html;
  }

  private checkUserPermission(action): boolean {
    return this.commonService.checkUserPermission(this.userPermissionModel, action);
  }

  isCompleted: boolean = false;
  projecTaskDetails: ProjecTaskDTO = undefined;

  public completedTaskHandler(event, value, taskData) {
    if (!this.checkUserPermission('complete'))
      return;
    if (this.projecTaskDTO.workflowOptions != null && taskData.active) {
      this.projecTaskDetails = taskData;
      this.isCompleteTask = true;
    } else {
      this.completeTaskAction({'status': true, 'workFlowResult': null});
    }
  }

  public completeTaskAction(result) {
    if (result.status) {
      if (this.projecTaskDTO.active) {
        this.projecTaskDTO.workflowResult = result.workFlowResult;
        this.projecTaskDTO.completedByUserId = this.eqmSettingsService.getUser().userId;
        this.projecTaskDTO.completedDateTime = new Date();
      } else {
        this.projecTaskDTO.workflowResult = null;
        //this.projecTaskDTO.active = !this.projecTaskDTO.active;
        this.projecTaskDTO.completedByUserId = null;
        this.projecTaskDTO.completedDateTime = null;
      }
      this.updateProjectDto(this.projecTaskDTO, '');
    } else {
      this.projecTaskDTO.active = false;
    }
    this.isCompleteTask = false;
    this.projecTaskDetails = undefined;
  }

  private closeForm(): void {
    this.openDetailViewer = false;
    this.closeDetailViewer.emit();
  }

  private pad2(num): string {
    num = num === undefined ? 0 : num;
    if (num.length === 1) {
      return 0 + '' + num;
    }
    return num;
  }


  private getTaskTimeList() {
    this.projectTaskTimeService.getAll(this.projecTaskDTO.projectTaskId).subscribe(
      eventResult => {
        this.projecTaskTimeList = eventResult.results;
      });
  }

  getTotalTaskTime(minute) {

    let result = '00:00';
    if (minute !== 0) {
      if (minute > 0) {
        if (minute > 59) {

          let totalHours = Math.floor(minute / 60);
          let extraminute = (minute - (totalHours * 60));

          result = ((totalHours > 9) ? totalHours : ('0' + totalHours)) +
            ':' +
            ((extraminute > 9) ? extraminute : ('0' + extraminute));
        } else {
          result = '00:' + ((minute > 9) ? minute : ('0' + minute));
        }
      }

    }
    //else {
    //    return "00:00";
    //}
    return result;
  }

  private setTotalTaskTime(actualMinutes: any): string {
    let returnValue: string = '';
    if (this.projecTaskDTO.actualMinutes > 0) {
      let totalMinutes = this.projecTaskDTO.actualMinutes;
      if (totalMinutes > 0) {
        if (totalMinutes > 59) {
          let totalHours = Math.floor(totalMinutes / 60);
          returnValue = 'Total: ' + totalHours + 'h ' + (totalMinutes - (totalHours * 60)) + 'm';
        } else {
          returnValue = 'Total: ' + '00h ' + totalMinutes + 'm';
        }
      }
    } else {
      returnValue = 'Total: ' + '00h 00m';
    }
    return returnValue;
  }

  private onTabSelect(e): void {
    console.log(e);
  }

  selectColor(color: any) {
    this.projecTaskDTO.labelColor = color;
  }

  onSaveTaskname(e) {
    if (e.keyCode === 13) {
      this.onSaveOrderDetails(e);
    }
  }

  onSaveOrderDetails(e) {
    this.updateProjectDto(this.projecTaskDTO, '');
  }


  /*Autoupdate methods*/

  updateProjectDto(projecTaskDto: ProjecTaskDTO, message: string) {
    let saveProjecTaskDto: ProjecTaskDTO;
    saveProjecTaskDto = this.projecTaskDTO;
    //saveProjecTaskDto.dueDate = this.commonService.verifyDate(this.projecTaskDTO.dueDate);
    //saveProjecTaskDto.startDate = this.commonService.verifyDate(this.projecTaskDTO.startDate);
    saveProjecTaskDto.assignedTo = this.assignedToList;
    saveProjecTaskDto.followers = this.followersList;
    saveProjecTaskDto.tags = this.tagList;
    this.projectTaskDetailService.saveProjectTask(saveProjecTaskDto, response => {
        if (response === true) {
          this.toasterHelperService.success('Updated', message + ' Updated sucessfully.');
        } else {
          this.toasterHelperService.error('', message + ' notUpdated');
        }
      },
      error => {
        this.toasterHelperService.error('Server error', message + ' notUpdated');
      }
    );
  }

  /**/
  onAssignedTovalueChange(value: any) {


  }

  public saveManualTimeSpent() {
    let manualdate = this.minutesSpent;
    let totlaMinute = ((manualdate.getHours() * 60) + manualdate.getMinutes());
    if (totlaMinute > 0) {
      this.isShowTaskTime = false;
      this.saveTaskTimeHandler(totlaMinute);
    }
  }

  public showTaskTime(value) {
    this.isShowTaskTime = !this.isShowTaskTime;
    this.isShowTaskTimeDetail = false;
  }

  public showTaskTimeDetail(value) {
    this.isShowTaskTimeDetail = !this.isShowTaskTimeDetail;
    this.isShowTaskTime = false;
  }

  public deleteTaskTime(id: any) {

    this.projectTaskTimeService.delete(id,
      response => {
        this.toasterHelperService.success('', 'Project Task Time deleted successfully.');
      },
      error => {
        this.toasterHelperService.errorMessage(error);
      }
    );
  }

  public saveTaskTimeHandler(totaltime) {

    if (totaltime > 0) {

      const inputProjectTaskTimeData: CreateProjectTaskTimeDto = new CreateProjectTaskTimeDto();
      inputProjectTaskTimeData.projectTaskId = this.projecTaskDTO.projectTaskId;
      inputProjectTaskTimeData.notes = '';
      inputProjectTaskTimeData.projectTaskTimeId = 0;
      //inputProjectTaskTimeData.started = new Date();
      inputProjectTaskTimeData.minutesSpent = totaltime;

      this.projectTaskTimeService.create(inputProjectTaskTimeData,
        response => {
          this.minutesSpent = new Date(0, 0, 0, 0, 0, 0);
          this.toasterHelperService.success('', 'Project Task Time save successfully.');
        },
        error => {
          this.toasterHelperService.errorMessage(error);
        }
      );
    } else {

      const inputProjectTaskTimeData: ProjectTaskTimeDto = new CreateProjectTaskTimeDto();
      inputProjectTaskTimeData.projectTaskId = this.projecTaskDTO.projectTaskId;
      inputProjectTaskTimeData.notes = '';
      //inputProjectTaskTimeData.projectTaskTimeId = projectTaskTimeDto.projectTaskTimeId;
      //inputProjectTaskTimeData.started = new Date();
      //inputProjectTaskTimeData.ended = new Date();

      this.projectTaskTimeService.update(inputProjectTaskTimeData,
        response => {
          this.toasterHelperService.success('', 'Project Task Time update successfully.');
        },
        error => {
          this.toasterHelperService.errorMessage(error);
        }
      );
    }
  }

  public assignToChange(event) {
    let assignToUser = event.map(e => e.value);
    for (let userId of assignToUser) {
      this.followersList = this.followersList.filter(item => item.value !== userId);
    }
  }

  public followersChange(event) {
    let selectedUser = event.map(e => e.value);
    for (let userId of selectedUser) {
      this.assignedToList = this.assignedToList.filter(item => item.value !== userId);
    }
  }

}
