import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {
  HttpClient,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';

import {ResultDtoOfSelectListDto} from '../../../../app/shared/models/selectListDto'
import {ProjecTaskDTO} from '../../projecttasklist/projecttask/service/projecttask.model';
import {CommonService} from '../../../shared/services/common.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class ProjectTaskDetailService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService, private http: HttpClient) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/Projects/api/';
  }

  getAssignedByProjectTaskId(projectTaskId): any {

    const url = this.apiBaseUrl + 'ProjectTaskAssignedTo/GetAssignUserByProjectTaskId';
    const params = new HttpParams().set('id', projectTaskId.toString());

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options);
  }

  getTagsByProjectTaskId(projectTaskId): Observable<any> {

    const url = this.apiBaseUrl + 'ProjectTaskTags/GetTagsByProjectTaskId';

    const params = new HttpParams().set('id', projectTaskId.toString());

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };


    return this.http.request<Response>('get', url, options).flatMap((response) => {
      console.log(response);
      const result200 = response;
      return Observable.of(result200);
    });

  }

  getAssignedByProjectId(projectTaskId): Observable<ResultDtoOfSelectListDto> {
    const url = this.apiBaseUrl + 'ProjectTeam/GetSelectList';

    const params = new HttpParams().set('id', projectTaskId.toString());

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };


    return this.http.request<Response>('get', url, options).flatMap((response) => {

      const result200 = ResultDtoOfSelectListDto.fromJS(response);
      return Observable.of(result200);
    });

  }

  /*Save task details*/
  saveProjectTask(input: ProjecTaskDTO, successCallback: any, errorCallback: any): any {

    const url = this.apiBaseUrl + 'ProjectTask/' + 'UpdateAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  saveAssignTo(input: any, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'ProjectTaskAssignedTo/' + 'InsertAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  deleteAssignTo(id: number, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'ProjectTaskAssignedTo/' + 'DeleteAsync';

    const params = new HttpParams().set('id', id.toString());
    const options = {
      params: params
    };

    this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
  }
}

