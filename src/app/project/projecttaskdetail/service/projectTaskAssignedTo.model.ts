// region IProjectTaskAssignedTo

export interface IProjectTaskAssignedTo {
  taskAssignedToId: number;
  projectTaskId: number;
  followOnly: string;
  userId: number;
  userName: string;
  text: string;
  value: number;
}

export class ProjectTaskAssignedTo implements IProjectTaskAssignedTo {
  taskAssignedToId: number;
  projectTaskId: number;
  followOnly: string;
  userId: number;
  userName: string;
  userAvatar: string;
  text: string;
  value: number;

  static fromJS(data: any): ProjectTaskAssignedTo[] {
    const results = [];
    if (data !== null && data !== undefined) {
      data = JSON.parse(data);
      for (const item of data) {
        const obj = new ProjectTaskAssignedTo();
        obj.init(item);
        results.push(obj);
      }
    }
    return results;
  }

  constructor(data?: IProjectTaskAssignedTo) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      data = this.toCamel(data);
      this.taskAssignedToId = data['taskAssignedToId'];
      this.projectTaskId = data['projectTaskId'];
      this.followOnly = data['followOnly'];
      this.userId = data['userId'];
      this.userName = data['userName'];
      this.userAvatar = data['userAvatar'];
      this.text = data['userName'];
      this.value = data['userId'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['taskAssignedToId'] = this.taskAssignedToId;
    data['projectTaskId'] = this.projectTaskId;
    data['followOnly'] = this.followOnly;
    data['userId'] = this.userId;
    data['userName'] = this.userName;
    data['userAvatar'] = this.userAvatar;
    data['value'] = this.value;
    data['text'] = this.text;
    return data;
  }

  toCamel(o) {
    let newO, origKey, newKey, value
    if (o instanceof Array) {
      newO = []
      for (origKey in o) {
        value = o[origKey]
        if (typeof value === 'object') {
          value = this.toCamel(value)
        }
        newO.push(value)
      }
    } else {
      newO = {}
      for (origKey in o) {
        if (o.hasOwnProperty(origKey)) {
          newKey = (origKey.charAt(0).toLowerCase() + origKey.slice(1) || origKey).toString()
          value = o[origKey]
          if (value instanceof Array || (value !== null && value.constructor === Object)) {
            value = this.toCamel(value)
          }
          newO[newKey] = value
        }
      }
    }
    return newO
  }
}

// endregion
