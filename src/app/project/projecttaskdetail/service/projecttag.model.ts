// region IProjectTagDTO

export interface IProjectTagDTO {
  projectTagId: number;
  projectId: number;
  tagName: string;
  tagColor: string;
  workspaceTagMasterId: number;
}

export class ProjectTagDTO implements IProjectTagDTO {
  projectTagId: number;
  projectId: number;
  tagName: string;
  tagColor: string;
  workspaceTagMasterId: number;

  static fromJS(data: any): ProjectTagDTO {
    const result = new ProjectTagDTO();
    result.init(data);
    return result;
  }

  static fromJSArrayList(data: any): ProjectTagDTO[] {
    const results = [];
    if (data !== null && data !== undefined) {
      for (const item of data) {
        const obj = new ProjectTagDTO();
        obj.init(item);
        results.push(obj);
      }
    }
    return results;
  }

  static fromJSArray(data: any): ProjectTagDTO[] {
    const results = [];
    if (data !== null && data !== undefined) {
      data = JSON.parse(data);
      for (const item of data) {
        const obj = new ProjectTagDTO();
        obj.init(item);
        results.push(obj);
      }
    }
    return results;
  }

  constructor(data?: IProjectTagDTO) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {

      data = this.toCamel(data);
      this.projectTagId = data['projectTagId'];
      this.projectId = data['projectId'];
      this.tagName = data['tagName'];
      this.tagColor = data['tagColor'];
      this.workspaceTagMasterId = data['workspaceTagMasterId'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['projectTagId'] = this.projectTagId;
    data['projectId'] = this.projectId;
    data['tagName'] = this.tagName;
    data['tagColor'] = this.tagColor;
    data['workspaceTagMasterId'] = this.workspaceTagMasterId;
    return data;
  }

  toCamel(o) {
    let newO, origKey, newKey, value
    if (o instanceof Array) {
      newO = []
      for (origKey in o) {
        value = o[origKey]
        if (typeof value === 'object') {
          value = this.toCamel(value)
        }
        newO.push(value)
      }
    } else {
      newO = {}
      for (origKey in o) {
        if (o.hasOwnProperty(origKey)) {
          newKey = (origKey.charAt(0).toLowerCase() + origKey.slice(1) || origKey).toString()
          value = o[origKey]
          if (value instanceof Array || (value !== null && value.constructor === Object)) {
            value = this.toCamel(value)
          }
          newO[newKey] = value
        }
      }
    }
    return newO
  }
}

// endregion


// region IPagedResultDtoOfProjecTagDTO
export interface IPagedResultDtoOfProjecTagDTO {
  results: ProjectTagDTO[];
  count: number;
}

export class PagedResultDtoOfProjecTagDTO implements IPagedResultDtoOfProjecTagDTO {
  results: ProjectTagDTO[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfProjecTagDTO {
    const result = new PagedResultDtoOfProjecTagDTO();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfProjecTagDTO) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(ProjectTagDTO.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion
