import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {SelectListDto} from '../../../shared/models/selectListDto'

// region IProjectTaskDetailDto
export interface IProjectTaskDetailDto {
  projectTaskId: number;
  projectTaskListId: number;
  projectId: number;
  projectTaskName: string;
  startDate: Date;
  dueDate: Date;
  points: number;
  labelColor: string;
  taskDescription: string;
  active: boolean;
  taskListSortOrder: number;
  estimatedMinutes: number;
  actualMinutes: number;
  assignedTo: SelectListDto[];
  followers: SelectListDto[];
}

export class ProjectTaskDetailDto implements IProjectTaskDetailDto {
  projectTaskId: number;
  projectTaskListId: number;
  projectId: number;
  projectTaskName: string;
  startDate: Date;
  dueDate: Date;
  points: number;
  labelColor: string;
  taskDescription: string;
  active: boolean;
  taskListSortOrder: number;
  estimatedMinutes: number;
  actualMinutes: number;
  assignedTo: SelectListDto[];
  followers: SelectListDto[];


  static fromJS(data: any): ProjectTaskDetailDto {
    const result = new ProjectTaskDetailDto();
    result.init(data);
    return result;
  }

  constructor(data?: IProjectTaskDetailDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.projectTaskId = data['projectTaskId'];
      this.projectTaskListId = data['projectTaskListId'];
      this.projectId = data['projectId'];
      this.projectTaskName = data['projectTaskName'];
      this.startDate = data['startDate'];
      this.dueDate = data['dueDate'];
      this.points = data['points'];
      this.labelColor = data['labelColor'];
      this.taskDescription = data['taskDescription'];
      this.active = data['active'];
      this.taskListSortOrder = data['taskListSortOrder'];
      this.estimatedMinutes = data['estimatedMinutes'];
      this.actualMinutes = data['actualMinutes'];
      this.assignedTo = data['assignedTo'];
      this.followers = data['followers'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['projectTaskId'] = this.projectTaskId;
    data['projectTaskListId'] = this.projectTaskListId;
    data['projectId'] = this.projectId;
    data['projectTaskName'] = this.projectTaskName;
    data['startDate'] = this.startDate;
    data['dueDate'] = this.dueDate;
    data['points'] = this.points;
    data['labelColor'] = this.labelColor;
    data['taskDescription'] = this.taskDescription;
    data['active'] = this.active;
    data['taskListSortOrder'] = this.taskListSortOrder;
    data['estimatedMinutes'] = this.estimatedMinutes;
    data['actualMinutes'] = this.actualMinutes;
    data['assignedTo'] = this.assignedTo;
    data['followers'] = this.followers;
    return data;
  }
}

// endregion
