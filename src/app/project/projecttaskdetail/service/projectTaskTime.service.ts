import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {
  PagedResultDtoOfProjectTaskTimeDto,
  ProjectTaskTimeDto
} from '../../projecttaskdetail/service/projectTaskTime.model'
import {CommonService} from '../../../shared/services/common.service';
import {
  filter,
  find,
  forEach,
  groupBy,
  includes,
  orderBy,
  remove,
  replace
} from 'lodash';
import {environment} from '../../../../environments/environment';

@Injectable()
export class ProjectTaskTimeService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;


  constructor(private commonService: CommonService, private http: HttpClient) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/Projects/api/ProjectTaskTime/';
  }

  create(input: ProjectTaskTimeDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'InsertAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  update(input: ProjectTaskTimeDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'UpdateAsync';

    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  delete(id: any, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'DeleteAsync';
    const params = new HttpParams().set('id', id);

    const options = {
      params: params
    };

    this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
  }

  getAll(ProjectTaskId): any {

    const url = this.apiBaseUrl + 'GetPagedQuery';
    const macAddress = this.commonService.getMacAddress();
    // Order by Clause
    const orderBy = '';

    let filter = '';
    // if (true) {
    //   filter = "Active eq true";
    // }
    if (ProjectTaskId !== 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += 'ProjectTaskId eq ' + ProjectTaskId;
    }

    const params = new HttpParams()
      .set('$count', 'true')
      .set('$orderby', orderBy.toString())
      .set('$filter', filter.toString());


    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'MacAddress': macAddress
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      const result200 = PagedResultDtoOfProjectTaskTimeDto.fromJS(response);
      return Observable.of(result200);
    });
  }
}


