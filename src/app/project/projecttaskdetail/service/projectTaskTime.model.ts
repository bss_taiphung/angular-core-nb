// region IProjectTaskTimeDto
export interface IProjectTaskTimeDto {
  projectTaskTimeId: number;
  projectTaskId: number;
  minutesSpent: number;
  started: Date;
  notes: string;
  ended: Date;
  active: boolean;
  createdDateTime: Date;
  createdByName: string;
}

export class ProjectTaskTimeDto implements IProjectTaskTimeDto {
  projectTaskTimeId: number;
  projectTaskId: number;
  minutesSpent: number;
  started: Date;
  notes: string;
  ended: Date;
  active: boolean;
  createdDateTime: Date;
  createdByName: string;

  static fromJS(data: any): ProjectTaskTimeDto {
    let result = new ProjectTaskTimeDto();
    result.init(data);
    return result;
  }

  constructor(data?: IProjectTaskTimeDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.projectTaskTimeId = data['projectTaskTimeId'];
      this.projectTaskId = data['projectTaskId'];
      this.minutesSpent = data['minutesSpent'];
      this.started = data['started'];
      this.notes = data['notes'];
      this.ended = data['ended'];
      this.createdDateTime = data['createdDateTime'];
      this.createdByName = data['createdByName'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['projectTaskTimeId'] = this.projectTaskTimeId;
    data['projectTaskId'] = this.projectTaskId;
    data['minutesSpent'] = this.minutesSpent;
    data['started'] = this.started;
    data['notes'] = this.notes;
    data['ended'] = this.ended;
    return data;
  }
}

// endregion


// region ICreateProjectTaskTimeDto

export interface ICreateProjectTaskTimeDto {
  projectTaskTimeId: number;
  projectTaskId: number;
  minutesSpent: number;
  started: Date;
  notes: string;
  ended: Date;
  active: boolean;
  createdDateTime: Date;
  createdByName: string;
}

export class CreateProjectTaskTimeDto implements ICreateProjectTaskTimeDto {
  projectTaskTimeId: number;
  projectTaskId: number;
  minutesSpent: number;
  started: Date;
  notes: string;
  ended: Date;
  active: boolean;
  duretion: string;
  createdDateTime: Date;
  createdByName: string;

  static fromJS(data: any): ProjectTaskTimeDto {
    let result = new ProjectTaskTimeDto();
    result.init(data);
    return result;
  }

  constructor(data?: ICreateProjectTaskTimeDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.projectTaskTimeId = data['projectTaskTimeId'];
      this.projectTaskId = data['projectTaskId'];
      this.minutesSpent = data['minutesSpent'];
      this.started = data['started'];
      this.notes = data['notes'];
      this.ended = data['ended'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['projectTaskTimeId'] = this.projectTaskTimeId;
    data['projectTaskId'] = this.projectTaskId;
    data['minutesSpent'] = this.minutesSpent;
    data['started'] = this.started;
    data['notes'] = this.notes;
    data['ended'] = this.ended;

    return data;
  }
}

// endregion


// region IPagedResultDtoOfProjectTaskTimeDto
export interface IPagedResultDtoOfProjectTaskTimeDto {
  results: ProjectTaskTimeDto[];
  count: number;
}

export class PagedResultDtoOfProjectTaskTimeDto implements IPagedResultDtoOfProjectTaskTimeDto {
  results: ProjectTaskTimeDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfProjectTaskTimeDto {
    let result = new PagedResultDtoOfProjectTaskTimeDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfProjectTaskTimeDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (let item of data['results'])
          this.results.push(ProjectTaskTimeDto.fromJS(item));
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (let item of this.results)
        data['results'].push(item.toJSON());
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion
