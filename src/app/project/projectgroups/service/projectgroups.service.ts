import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {
  HttpClient,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';
import {
  CreateProjectGroupsDto,
  PagedResultDtoOfProjectGroupsDto,
  ProjectGroupsDto
} from './projectgroups.model';
import {ProjectSettingsService} from '../../shared/services/projectsettings.service';
import {CommonService} from '../../../shared/services/common.service';
import {EqmSettingsService} from '../../../shared/services/eqmsettings.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class ProjectGroupsService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private eqmSettingsService: EqmSettingsService,
              private projectSettingsService: ProjectSettingsService,
              private commonService: CommonService,
              private http: HttpClient) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/Projects/api/ProjectGroup/';
  }

  create(input: CreateProjectGroupsDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'InsertAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  update(input: ProjectGroupsDto, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'UpdateAsync';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  delete(id: any, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'DeleteAsync';
    const params = new HttpParams().set('id', id);

    const options = {
      params: params
    };

    this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
  }

  getAll(take: number,
         skip: number,
         sort: any,
         isactive: boolean,
         filter: string,
         workspaceId: number): Observable<PagedResultDtoOfProjectGroupsDto> {

    const url = this.apiBaseUrl + 'GetPagedQuery';

    let orderBy = '';
    if (sort !== undefined && sort.length > 0) {
      for (const item of sort) {
        orderBy = this.commonService.firstUpper(item.field) + ' ' + (item.dir === undefined ? 'asc' : item.dir);
      }
    } else {
      orderBy = 'ProjectGroupId desc';
    }

    if (isactive) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += 'Active eq true';
    }

    if (workspaceId > 0) {
      filter += (filter.length > 0) ? ' and ' : '';
      filter += ' WorkspaceId eq ' + workspaceId + '';
    }

    /*let userid = this.eqmSettingsService.getUser().userId;
    if (!this.projectSettingsService.getWorkSpaceSettings().globalWorkspaceAdmin && !this.projectSettingsService.getWorkSpaceSettings().globalWorkspaceViewing) {
     filter += (filter.length > 0) ? ' and ' : '';
     filter += "UserId eq " + userid + "";
    }*/
    const params = new HttpParams()
      .set('$top', take.toString())
      .set('$skip', skip.toString())
      .set('$count', 'true')
      .set('$orderby', orderBy.toString())
      .set('$filter', filter.toString());


    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }),
      params: params
    };

    return this.http.request<Response>('get', url, options).flatMap((response) => {
      const result200 = PagedResultDtoOfProjectGroupsDto.fromJS(response);
      return Observable.of(result200);
    });
  }
}

