import {Injectable, Inject} from '@angular/core';
import {Http, Response, Headers, HttpModule} from '@angular/http';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';

export interface IProjectGroupsDto {
  projectGroupId: number;
  workspaceId: number;
  projectGroupName: string;
  numProjects: number;
  active: boolean;
}

export class ProjectGroupsDto implements IProjectGroupsDto {
  projectGroupId: number;
  workspaceId: number;
  projectGroupName: string;
  numProjects: number;
  createdByName: string;
  modifiedByName: string;
  active: boolean;

  constructor(data?: IProjectGroupsDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.projectGroupId = data['projectGroupId'];
      this.workspaceId = data['workspaceId'];
      this.projectGroupName = data['projectGroupName'];
      this.numProjects = data['numProjects'];
      this.createdByName = data['createdByName'];
      this.modifiedByName = data['modifiedByName'];
      this.active = data['active'];

    }
  }

  static fromJS(data: any): ProjectGroupsDto {
    let result = new ProjectGroupsDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['projectGroupId'] = this.projectGroupId;
    data['workspaceId'] = this.workspaceId;
    data['projectGroupName'] = this.projectGroupName;
    data['numProjects'] = this.numProjects;
    data['createdByName'] = this.createdByName;
    data['modifiedByName'] = this.modifiedByName;
    data['active'] = this.active;


    return data;
  }
}

export interface ICreateProjectGroupsDto {
  projectGroupId: number;
  workspaceId: number;
  projectGroupName: string;
  numProjects: number;
  active: boolean;
}

export class CreateProjectGroupsDto implements ICreateProjectGroupsDto {
  projectGroupId: number = 0;
  workspaceId: number;
  projectGroupName: string;
  numProjects: number;

  active: boolean;

  constructor(data?: IProjectGroupsDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.projectGroupId = 0;
      this.workspaceId = data['workspaceId'];
      this.projectGroupName = data['projectGroupName'];
      this.active = (data['active'] == null) ? false : data['active'];
    }
  }

  static fromJS(data: any): ProjectGroupsDto {
    let result = new ProjectGroupsDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['projectGroupId'] = 0;
    data['workspaceId'] = this.workspaceId;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

export interface IPagedResultDtoOfProjectGroupsDto {
  results: ProjectGroupsDto[];
  count: number;
}

export class PagedResultDtoOfProjectGroupsDto implements IPagedResultDtoOfProjectGroupsDto {
  results: ProjectGroupsDto[];
  count: number;

  constructor(data?: IPagedResultDtoOfProjectGroupsDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (let item of data['results'])
          this.results.push(ProjectGroupsDto.fromJS(item));
      }
      this.count = data['count'];
    }
  }

  static fromJS(data: any): PagedResultDtoOfProjectGroupsDto {
    let result = new PagedResultDtoOfProjectGroupsDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (let item of this.results)
        data['results'].push(item.toJSON());
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}


