import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import {ProjectGroupsService} from './service/projectgroups.service'
import {
  CreateProjectGroupsDto,
  IProjectGroupsDto,
  ProjectGroupsDto
} from './service/projectgroups.model';
import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {UserSettingsService} from '../../shared/usersettings/usersetting.service';
import {ProjectSettingsService} from '../shared/services/projectsettings.service';

@Component({
  selector: 'kendo-grid-form-projectGroups',
  templateUrl: './html/projectgroups-form.component.html',
  styleUrls: ['./css/projectgroups.component.css'],
  encapsulation: ViewEncapsulation.None
})


export class ProjectGroupsFormComponent {
  public workspaceIdList: any;
  public addNewProjectGroupForm: boolean = false;


  @Input() public isNew = false;

  @Input()
  public set model(projectGroup: ProjectGroupsDto) {
    if (projectGroup !== undefined) {
      if (projectGroup.projectGroupId > 0) {
        this.editForm.reset(projectGroup);
        this.addNewProjectGroupForm = false;
      } else {
        this.addNewProjectGroupForm = true;
      }
    }
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();


  data: IProjectGroupsDto[];
  public formGroup: FormGroup;
  private editedRowIndex: number;
  public editDataItem: CreateProjectGroupsDto;

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private userSettingsService: UserSettingsService,
              private projectgroupsService: ProjectGroupsService,
              private projectSettingsService: ProjectSettingsService) {
  }

  public addHandler({sender}) {
    this.editForm.reset();
    this.editDataItem = new CreateProjectGroupsDto();
    this.addNewProjectGroupForm = true;
  }


  private closeProjectGroupForm(): void {
    this.editForm.reset();
    this.editDataItem = new CreateProjectGroupsDto();
    this.addNewProjectGroupForm = false;
    this.cancel.emit();
  }

  public saveProjectGroupData() {
    const inputProjectGroupsData: ProjectGroupsDto = this.editForm.value;
    if (this.addNewProjectGroupForm) {
      inputProjectGroupsData.projectGroupId = 0;
      inputProjectGroupsData.active = true;
    }
    if (inputProjectGroupsData.projectGroupId == 0 || inputProjectGroupsData.projectGroupId === undefined) {
      // failuredata.active = (failuredata.active == null) ? false : failuredata.active;
      const inputProjectGroupsData: CreateProjectGroupsDto = this.editForm.value;
      inputProjectGroupsData.workspaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;
      this.projectgroupsService.create(inputProjectGroupsData,
        response => {
          this.toasterService.success('', 'Project Group Saved Successfully');
          this.cancel.emit();
        },
        error => {
          this.toasterService.errorMessage(error);
          this.cancel.emit();
        }
      );
    }
    else {

      inputProjectGroupsData.workspaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;
      this.projectgroupsService.update(inputProjectGroupsData,
        response => {
          this.toasterService.success('', 'Project Group Updated Successfully');
          this.cancel.emit();
        },
        error => {
          this.toasterService.errorMessage(error);
          this.cancel.emit();
        }
      );
    }
  }

  public editForm: FormGroup = new FormGroup({
    'projectGroupId': new FormControl(),
    'projectGroupName': new FormControl('', Validators.required),
    'active': new FormControl(false),
  });
}
