import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {Observable} from 'rxjs/Observable';
import {
  SortDescriptor,
  State
} from '@progress/kendo-data-query';
import {
  DataStateChangeEvent,
  GridDataResult,
  PageChangeEvent
} from '@progress/kendo-angular-grid';
import {FormGroup} from '@angular/forms';
import {ProjectGroupsService} from './service/projectgroups.service'
import {
  CreateProjectGroupsDto,
  IProjectGroupsDto,
  ProjectGroupsDto
} from './service/projectgroups.model';

import {HubConnection} from '@aspnet/signalr-client';
import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';

import {
  EqmSettings,
  User,
  UserRole
} from '../../shared/models/eqmSettings'
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {SelectListDto} from '../../shared/models/selectListDto'
import {UserSettingsService} from '../../shared/usersettings/usersetting.service';
import {ProjectSettingsService} from '../shared/services/projectsettings.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'kendo-grid-projectGroups',
  templateUrl: './html/projectgroups.component.html',
  styleUrls: ['./css/projectgroups.component.css'],
  encapsulation: ViewEncapsulation.None
})


export class ProjectGroupsComponent implements OnInit {


  public workspaceIdList: any;
  public addNewProjectGroupForm = false;

  @Output() closeProjectgroupFrom: EventEmitter<any> = new EventEmitter();

  @Input()
  public set addNewProjectGroup(loadData) {
    if (loadData) {
      this.editDataItem = new CreateProjectGroupsDto();
      this.isNew = true;
      this.addNewProjectGroupForm = true;
    }
  }

  @Input()
  public set selectedWorkspaceChange(loaddata: any) {
    this.loadGridData();
    this.signalRConnection();
  }


  data: IProjectGroupsDto[];
  public formGroup: FormGroup;
  public view: Observable<GridDataResult>;
  private editedRowIndex: number;
  public editDataItem: CreateProjectGroupsDto;
  public isNew: boolean;
  public pageSize = 10;
  public skip = 0;
  public state: State = {
    skip: 0,
    take: 5,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [{field: '', operator: '', value: ''}]
    }
  };
  public gridView: GridDataResult;
  public deleteDataItem: any;
  private sort: SortDescriptor[] = [];
  public isactive = true;
  public searchtext = '';
  private hubConnection: HubConnection;
  public logMessage = '';
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  eqmSettings: EqmSettings = new EqmSettings();
  user: User = new User();
  userRole: UserRole = new UserRole();
  public siteList: SelectListDto[];
  public mss: any;
  private workspaceSettings: any;

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private userSettingsService: UserSettingsService,
              private projectgroupsService: ProjectGroupsService,
              private projectSettingsService: ProjectSettingsService) {
    this.mss = this.commonService.getCookies('Mss');
    this.eqmSettings = this.eqmSettingsService.getEqmSettings();
    this.user = this.eqmSettingsService.getUser();
    this.userRole = this.eqmSettingsService.getUserRoles();
    this.workspaceSettings = this.projectSettingsService.getWorkSpaceSettings();
  }

  ngOnInit() {
    if (this.projectSettingsService.validateProjectWorkspace()) {
      this.loadGridData();
    }
  }

  signalRConnection() {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    const workSpaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started for Project Group!');

        this.hubConnection
          .invoke('Subscribe', 'Projects', 'ProjectGroup', 'WorkspaceId', workSpaceId)
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
      const _responseText = messageJson;
      let result200: any = null;
      const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      const resultData200 = this.commonService.toCamel(data);
      result200 = resultData200 ? ProjectGroupsDto.fromJS(resultData200) : new ProjectGroupsDto();
      const dataExist = this.containsObject(result200, this.gridView.data);

      if (operation.toLowerCase() === 'insert' && !dataExist) {
        this.gridView.data.unshift(result200);
      }

      if (operation.toLowerCase() === 'update') {
        this.gridView.data.forEach((element, index) => {
          if (element.projectGroupId === result200.projectGroupId) {
            if (this.isactive) {
              if (result200.active == this.isactive) {
                this.gridView.data[index] = result200;
              } else {
                operation = 'delete';
              }
            } else {
              this.gridView.data[index] = result200;
            }
          }
        });
      }

      if (operation.toLowerCase() === 'delete' && dataExist) {
        let index = null;
        this.gridView.data.forEach((element, i) => {
          if (element.projectGroupId === result200.projectGroupId) {
            index = i;
          }
        });
        if (index !== null) {
          this.gridView.data.splice(index, 1);
        }
      }
    });
  }

  containsObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].projectGroupId === obj.projectGroupId) {
        return true;
      }
    }

    return false;
  }

  // Dialog box
  opened = false;
  loading = true;
  gridLoading = true;

  public close(status) {
    const $this = this;
    if (status) {
      this.projectgroupsService.delete($this.deleteDataItem.projectGroupId,
        response => {
          this.toasterService.success('', 'Project Group Removed Successfully');
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
    $this.deleteDataItem = null;
    $this.opened = false;
    this.addNewProjectGroupForm = false;
  }

  public open() {
    this.opened = true;
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.loadGridData();
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
    this.loadGridData();
  }

  private loadGridData(): void {
    this.gridLoading = true;
    const currentWorkspaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;
    this.projectgroupsService.getAll(
      this.pageSize,
      this.skip,
      this.sort,
      this.isactive,
      this.searchtext,
      currentWorkspaceId)
      .subscribe(eventResult => {
        this.gridView = {
          data: eventResult.results,
          total: eventResult.count
        };
        this.loading = false;
        this.gridLoading = false;
      });
  }

  public addHandler({sender}) {
    this.closeEditor(sender);
    this.editDataItem = new CreateProjectGroupsDto();
    this.isNew = true;
    this.addNewProjectGroupForm = true;
  }

  public editHandler({dataItem}) {
    this.editDataItem = dataItem;
    this.isNew = false;
    this.addNewProjectGroupForm = false;
  }

  private closeProjectGroupForm(): void {
    this.editDataItem = new CreateProjectGroupsDto();
    this.addNewProjectGroupForm = false;
    this.closeProjectgroupFrom.emit();
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public cancelHandler() {
    this.editDataItem = undefined;
    this.formGroup = undefined;
    this.closeProjectgroupFrom.emit();
  }

  public removeHandler({dataItem}) {
    this.deleteDataItem = dataItem;
    this.open();
  }

  private closeForm() {
    this.editedRowIndex = undefined;
    this.editDataItem = undefined;
    this.formGroup = undefined;
    this.addNewProjectGroupForm = false;
  }

  public showActive() {
    this.isactive = !this.isactive;
    this.gridLoading = true;
    this.loadGridData();
  }

  public onSearchKeyup() {
    this.skip = 0;
    this.pageSize = 10;
    this.gridLoading = true;
    this.loadGridData();
  }

  public ActionData: Array<any> = [{
    text: 'Delete',
    icon: 'trash'
  }];

  public onAction(e, dataItem) {
    this.workspaceSettings = this.projectSettingsService.getWorkSpaceSettings();
    if (this.workspaceSettings.isAdmin) {
      if (e === undefined) {
        this.editDataItem = dataItem;
        this.isNew = false;

        this.addNewProjectGroupForm = true;

      } else {
        this.deleteDataItem = dataItem;
        this.open();
      }
    }
    else {
      this.toasterService.error('', 'You have no permission.');
    }
  }

  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {
      this.cancelHandler();
      this.close(false);
      this.closeForm();
    }
  }


  workspaceSelectionChange($event) {
    this.loadGridData();
    this.signalRConnection();
  }


  timeout = null;

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridLoading = true;

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.doSearch(state, this);
    }, 1000);
  }

  doSearch(state: any, that) {
    if (state.filter) {
      const search = that.commonService.getFilter(state.filter.filters, that.isactive);
      const workspaceId = this.projectSettingsService.getWorkSpaceSettings().currentWorkspaceId;
      that.projectgroupsService.getAll(state.take, state.skip, that.sort, this.isactive, search, workspaceId)
        .subscribe(eventResult => {
          that.gridView = {
            data: eventResult.results,
            total: eventResult.count
          };
          that.loading = false;
          that.gridLoading = false;
        });

      this.skip = state.skip;
      this.pageSize = state.take;
    }
    else {
      that.loadGridData();
    }
  }

}
