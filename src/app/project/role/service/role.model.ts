import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

// region IRolesDto
export interface IRolesDto {
  roleId: number;
  roleName: string;
  canCreateWorkspaces: boolean;
  globalWorkspaceViewing: boolean;
  globalWorkspaceAdmin: boolean;
  modifiedDateTime: string;
  modifiedBy: string;
  createdBy: string;
  createdDateTime: string;
  active: boolean;
}

export class RolesDto implements IRolesDto {
  roleId: number;
  roleName: string;
  canCreateWorkspaces: boolean;
  globalWorkspaceViewing: boolean;
  globalWorkspaceAdmin: boolean;
  modifiedDateTime: string;
  modifiedBy: string;
  createdBy: string;
  createdDateTime: string;
  active: boolean;

  static fromJS(data: any): RolesDto {
    const result = new RolesDto();
    result.init(data);
    return result;
  }

  constructor(data?: IRolesDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.roleId = data['roleId'];
      this.roleName = data['roleName'];
      this.canCreateWorkspaces = data['canCreateWorkspaces'];
      this.globalWorkspaceViewing = data['globalWorkspaceViewing'];
      this.globalWorkspaceAdmin = data['globalWorkspaceAdmin'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.modifiedBy = data['modifiedBy'];
      this.createdBy = data['createdBy'];
      this.createdDateTime = data['createdDateTime'];
      this.active = data['active'];

    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['roleId'] = this.roleId;
    data['roleName'] = this.roleName;
    data['canCreateWorkspaces'] = this.canCreateWorkspaces;
    data['globalWorkspaceViewing'] = this.globalWorkspaceViewing;
    data['globalWorkspaceAdmin'] = this.globalWorkspaceAdmin;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['modifiedBy'] = this.modifiedBy;
    data['createdBy'] = this.createdBy;
    data['createdDateTime'] = this.createdDateTime;
    data['active'] = this.active;
    return data;
  }
}

// endregion

// region ICreateRolesDto

export interface ICreateRolesDto {
  roleId: number;
  roleName: string;
  canCreateWorkspaces: boolean;
  globalWorkspaceViewing: boolean;
  globalWorkspaceAdmin: boolean;
  modifiedDateTime: string;
  modifiedBy: string;
  createdBy: string;
  createdDateTime: string;
  active: boolean;
}

export class CreateRolesDto implements ICreateRolesDto {
  roleId = 0;
  roleName: string;
  canCreateWorkspaces: boolean;
  globalWorkspaceViewing: boolean;
  globalWorkspaceAdmin: boolean;
  modifiedDateTime: string;
  modifiedBy: string;
  createdBy: string;
  createdDateTime: string;
  active: boolean;

  static fromJS(data: any): RolesDto {
    const result = new RolesDto();
    result.init(data);
    return result;
  }

  constructor(data?: IRolesDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.roleId = data['roleId'];
      this.roleName = data['roleName'];
      this.canCreateWorkspaces = data['canCreateWorkspaces'];
      this.globalWorkspaceViewing = data['globalWorkspaceViewing'];
      this.globalWorkspaceAdmin = data['globalWorkspaceAdmin'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.modifiedBy = data['modifiedBy'];
      this.createdBy = data['createdBy'];
      this.createdDateTime = data['createdDateTime'];
      this.active = (data['active'] == null) ? false : data['active'];

    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['roleId'] = this.roleId;
    data['roleName'] = this.roleName;
    data['canCreateWorkspaces'] = this.canCreateWorkspaces;
    data['globalWorkspaceViewing'] = this.globalWorkspaceViewing;
    data['globalWorkspaceAdmin'] = this.globalWorkspaceAdmin;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['modifiedBy'] = this.modifiedBy;
    data['createdBy'] = this.createdBy;
    data['createdDateTime'] = this.createdDateTime;
    data['active'] = (this.active == null) ? false : this.active;
    return data;
  }
}

// endregion

// region IPagedResultDtoOfRolesDto

export interface IPagedResultDtoOfRolesDto {
  results: RolesDto[];
  count: number;
}

export class PagedResultDtoOfRolesDto implements IPagedResultDtoOfRolesDto {
  results: RolesDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfRolesDto {
    const result = new PagedResultDtoOfRolesDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfRolesDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data['results'] && data['results'].constructor === Array) {
        this.results = [];
        for (const item of data['results']) {
          this.results.push(RolesDto.fromJS(item));
        }
      }
      this.count = data['count'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data['results'] = [];
      for (const item of this.results) {
        data['results'].push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

// endregion
