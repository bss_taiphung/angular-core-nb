import {
  AfterViewInit,
  Component
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import {DomSanitizer} from '@angular/platform-browser';
import 'rxjs/add/observable/fromEvent';
import {Observable} from 'rxjs/Observable';

import {HubConnection} from '@aspnet/signalr-client';
import {DataStateChangeEvent, GridDataResult, PageChangeEvent} from '@progress/kendo-angular-grid';
import {SortDescriptor, State} from '@progress/kendo-data-query';

import {SelectListDto} from './../../shared/models/selectListDto';
import {CommonService} from './../../shared/services/common.service';
import {ToasterHelperService} from './../../shared/services/toasterHelper.service';
import {CreateRolesDto, IRolesDto, RolesDto} from './service/role.model';
import {RolesService} from './service/role.service'
import {environment} from '../../../environments/environment';

@Component({
  selector: 'kendo-grid-role-form',
  templateUrl: './html/role.component.html'
})
export class RoleFormComponent implements AfterViewInit {

  data: IRolesDto[];
  public formGroup: FormGroup;
  public view: Observable<GridDataResult>;
  private editedRowIndex: number;
  public addRoleFlag: boolean;
  public editDataItem: CreateRolesDto;
  public isNew: boolean;
  public pageSize = 10;
  public skip = 0;
  public state: State = {
    skip: 0,
    take: 5,
  };

  constructor(private toasterService: ToasterHelperService,
              private domSanitizer: DomSanitizer,
              private commonService: CommonService,
              private roleService: RolesService) {
  }

  ngAfterViewInit() {
    this.loadGridData();
  }

  signalRConnection() {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');

        this.hubConnection
          .invoke('Subscribe', 'Projects', 'Role', '', '')
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated', (groupName: string, operation: string, messageJson: string) => {
      const _responseText = messageJson;
      let result200: any = null;
      const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
      const resultData200 = this.commonService.toCamel(data);
      result200 = resultData200 ? RolesDto.fromJS(resultData200) : new RolesDto();
      const dataExist = this.containsObject(result200, this.gridView);

      if (operation.toLowerCase() === 'insert' && !dataExist) {
        // this.gridView.push(result200);
      }

      if (operation.toLowerCase() === 'update') {
        /*this.gridView.forEach((element, index) => {
         if (element.projectTaskListId === result200.projectTaskListId) {
           //
           if (this.isactive) {
             if (result200.active == this.isactive) {
               this.gridView[index] = result200;
             } else {
               operation = "delete";
             }
           } else {
             this.gridView[index] = result200;
           }
         }
        });*/
      }

      if (operation.toLowerCase() === 'delete' && dataExist) {
        /*var index = null;
        this.gridView.forEach((element, i) => {
         if (element.projectTaskListId === result200.projectTaskListId) {
           index = i;
         }
        });
        if (index !== null) {
         this.gridView.splice(index, 1);
        }*/
      }
    });
  }

  containsObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].roleId === obj.roleId) {
        return true;
      }
    }

    return false;
  }

  public gridView: GridDataResult;
  public deleteDataItem: any;
  private sort: SortDescriptor[] = [];
  public isactive = true;
  public isDuplicateTeamMember: boolean;
  public searchtext = '';
  public selWorkspaceId: number;
  public selWorkspaceName = '';

  public userList: SelectListDto[];
  public filterUserIdList: SelectListDto[];

  private hubConnection: HubConnection;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;


  // Dialog box
  opened = false;
  loading = true;
  gridLoading = true;


  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.loadGridData();
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
    this.loadGridData();
  }

  private loadGridData(): void {
    this.gridLoading = true;
    this.roleService.getAll(this.pageSize, this.skip, this.sort, this.isactive, this.searchtext).subscribe(eventResult => {
      this.gridView = {
        data: eventResult.results,
        total: eventResult.count
      };
      this.loading = false;
      this.gridLoading = false;
    });
  }

  public showActive() {
    this.isactive = !this.isactive;
    this.gridLoading = true;
    this.gridView = null;
    this.loadGridData();
  }

  public saveRoleData() {
    this.addRoleFlag = false;
    if (this.isNew) {
      const inputData = this.roleForm.value;
      inputData.canCreateWorkspaces = inputData.canCreateWorkspaces === null ? false : true;
      inputData.globalWorkspaceAdmin = inputData.globalWorkspaceAdmin === null ? false : true;
      inputData.globalWorkspaceViewing = inputData.globalWorkspaceViewing === null ? false : true;
      this.roleService.create(inputData,
        response => {
          this.toasterService.success('', 'Role Saved Successfully');
          this.loadGridData(); // Remove after signalR code
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    } else {
      const editInputData = this.roleForm.value;
      this.roleService.update(editInputData,
        response => {
          if (response) {
            this.toasterService.success('', 'Role Updated Successfully');
            this.loadGridData(); // Remove after signalR code
          }
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
  }

  public userIdHandleFilter(filter: any): void {
    this.filterUserIdList = this.userList.filter((u) => u.text.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
    this.isDuplicateTeamMember = false;
  }


  public addNewRole() {
    this.editDataItem = new CreateRolesDto();
    this.addRoleFlag = true;
    this.roleForm.reset();
    this.isNew = true;
  }


  public closeAddRoleForm() {
    this.editDataItem = undefined;
    this.roleForm.reset();
    this.addRoleFlag = false;
  }

  timeout = null;

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridLoading = true;

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.doSearch(state, this);
    }, 1000);
  }

  doSearch(state: any, that) {
    if (state.filter) {
      const search = that.commonService.getFilter(state.filter.filters, that.isactive);
      that.roleService.getAll(state.take, state.skip, this.isactive, that.sort, search)
        .subscribe(eventResult => {
          that.gridView = {
            data: eventResult.results,
            total: eventResult.count
          };
          that.loading = false;
          that.gridLoading = false;
        });

      this.skip = state.skip;
      this.pageSize = state.take;
    }
    else {
      that.loadGridData();
    }
  }

  // Delete confirm
  public close(status) {
    const $this = this;
    if (status) {
      this.gridLoading = true;
      this.roleService.delete($this.deleteDataItem.roleId,
        response => {
          if (response) {
            this.toasterService.success('', 'Role Removed Successfully');
            this.loadGridData(); // Remove after signalR code
          }
        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );
    }
    $this.deleteDataItem = null;
    $this.opened = false;
  }

  // Delete dialog
  public open() {
    this.opened = true;
  }

  public ActionData: Array<any> = [
    {
      text: 'Edit',
      icon: 'pencil'
    },
    {
      text: 'Delete',
      icon: 'trash'
    }];

  public onAction(e, roleData) {
    if (e === undefined) {
      this.editDataItem = roleData;
      this.isNew = false;
      this.roleForm.reset(roleData);
      this.addRoleFlag = true;
    } else {
      if (e.text === 'Delete') {
        this.addRoleFlag = false;
        this.deleteDataItem = roleData;
        this.open();
      }
    }
  }

  public roleForm: FormGroup = new FormGroup({
    'roleId': new FormControl(),
    'roleName': new FormControl('', Validators.required),
    'canCreateWorkspaces': new FormControl(false),
    'globalWorkspaceViewing': new FormControl(false),
    'globalWorkspaceAdmin': new FormControl(false),
    'active': new FormControl(false),
  });


  private closeForm(): void {

    this.editDataItem = undefined;
    this.roleForm.reset();
    this.addRoleFlag = false;
  }

  public saveRole(e): void {
    e.preventDefault();

  }

  public onCancel(e): void {
    e.preventDefault();
    this.closeForm();
  }
}
