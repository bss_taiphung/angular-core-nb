import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ButtonsModule} from '@progress/kendo-angular-buttons';
import {DialogModule} from '@progress/kendo-angular-dialog';
import {GridModule} from '@progress/kendo-angular-grid';
import {DateInputsModule} from '@progress/kendo-angular-dateinputs';
import {DropDownsModule} from '@progress/kendo-angular-dropdowns';
import {TelerikReportingModule} from '@progress/telerik-angular-report-viewer';

import {GeneralModule} from '../general/general.module';

import {routing} from './sales.routes';
import {SharedModule} from '../shared/index';
import {SalesComponent} from './sales.component';
// Customers Component
import {CustomersComponent} from './customers/customers.component';
import {CustomersFormComponent} from './customers/customers-form.component';
import {CustomersService} from './customers/service/customers.service';
// Customers Families Component
import {CustomersFamiliesComponent} from './customersfamilies/customersfamilies.component';
import {CustomersFamiliesFormComponent} from './customersfamilies/customersfamilies-form.component';
import {CustomersFamiliesService} from './customersfamilies/service/customersfamilies.service';
// Dispositions Component
import {DispositionsComponent} from './dispositions/dispositions.component';
import {DispositionsFormComponent} from './dispositions/dispositions-form.component';
import {DispositionsService} from './dispositions/service/dispositions.service';
// Material types Component
import {MaterialTypesComponent} from './materialtypes/materialtypes.component';
import {MaterialTypesFormComponent} from './materialtypes/materialtypes-form.component';
import {MaterialTypesService} from './materialtypes/service/materialtypes.service';
// Packaging Component
import {PackagingComponent} from './packaging/packaging.component';
import {PackagingFormComponent} from './packaging/packaging-form.component';
import {PackagingService} from './packaging/service/packaging.service';
// Part Segments Component
import {PartSegmentsComponent} from './partsegments/partsegments.component';
import {PartSegmentsFormComponent} from './partsegments/partsegments-form.component';
import {PartSegmentsService} from './partsegments/service/partsegments.service';
// Pressgroups Component
import {PressGroupsComponent} from './pressgroups/pressgroups.component';
import {PressGroupsFormComponent} from './pressgroups/pressgroups-form.component';
import {PressGroupsService} from './pressgroups/service/pressgroups.service';
// Programs Component
import {ProgramsComponent} from './programs/programs.component';
import {ProgramsFormComponent} from './programs/programs-form.component';
import {ProgramsService} from './programs/service/programs.service';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    ButtonsModule,
    GridModule,
    DialogModule,
    DateInputsModule,
    DropDownsModule,
    CommonModule,
    SharedModule,
    routing,
    TelerikReportingModule,
    GeneralModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  declarations: [
    SalesComponent,
    CustomersComponent,
    CustomersFormComponent,
    CustomersFamiliesComponent,
    CustomersFamiliesFormComponent,
    DispositionsComponent,
    DispositionsFormComponent,
    MaterialTypesComponent,
    MaterialTypesFormComponent,
    PackagingComponent,
    PackagingFormComponent,
    PartSegmentsComponent,
    PartSegmentsFormComponent,
    PressGroupsComponent,
    PressGroupsFormComponent,
    ProgramsComponent,
    ProgramsFormComponent,
  ],
  providers: [
    CustomersService,
    CustomersFamiliesService,
    DispositionsService,
    MaterialTypesService,
    PackagingService,
    PartSegmentsService,
    PressGroupsService,
    ProgramsService
  ]

})
export class SalesModule {
}
