import {Component, ViewEncapsulation} from '@angular/core';
import 'rxjs/add/observable/fromEvent';

import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {UserSettingsService} from '../../shared/usersettings/usersetting.service';

@Component({
  selector: 'app-customers',
  templateUrl: './html/customers.component.html',
  styleUrls: ['./css/customers.component.css'],
  encapsulation: ViewEncapsulation.None,
})

export class CustomersComponent {
  opened: boolean;
  loading: boolean;

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private userSettingsService: UserSettingsService) {

  }

  ngOnInit() {
  }

  close(e) {
  }

  showActive() {
  }
}
