import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import 'rxjs/add/observable/fromEvent';

import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {EqmSettingsService} from '../../shared/services/eqmsettings.service';
import {UserSettingsService} from '../../shared/usersettings/usersetting.service';

@Component({
  selector: 'app-pressgroups',
  templateUrl: './html/pressgroups.component.html',
  styleUrls: ['./css/pressgroups.component.css'],
  encapsulation: ViewEncapsulation.None,
})

export class PressGroupsComponent implements OnInit {
  opened: boolean;
  loading: boolean;

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private eqmSettingsService: EqmSettingsService,
              private userSettingsService: UserSettingsService) {

  }

  ngOnInit() {
  }

  close(e) {
  }

  showActive() {
  }

}
