import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../shared/guards/auth.guard'


import {SalesComponent} from './sales.component';
import {CustomersComponent} from './customers/customers.component';
import {CustomersFamiliesComponent} from './customersfamilies/customersfamilies.component';
import {DispositionsComponent} from './dispositions/dispositions.component';
import {MaterialTypesComponent} from './materialtypes/materialtypes.component';
import {PackagingComponent} from './packaging/packaging.component';
import {PartSegmentsComponent} from './partsegments/partsegments.component';
import {PressGroupsComponent} from './pressgroups/pressgroups.component';
import {ProgramsComponent} from './programs/programs.component';

export const routes: Routes = [
  {
    path: '', component: SalesComponent,
    canActivate: [AuthGuard],
    children: [
      {path: 'customers', component: CustomersComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'customersfamilies', component: CustomersFamiliesComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'dispositions', component: DispositionsComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'materialtypes', component: MaterialTypesComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'packaging', component: PackagingComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'partsegments', component: PartSegmentsComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'pressgroups', component: PressGroupsComponent, data: {preload: false}, canActivate: [AuthGuard]},
      {path: 'programs', component: ProgramsComponent, data: {preload: false}, canActivate: [AuthGuard]},

      //{ path: "administration/roles", component: RolesComponent, data: { preload: false }, canActivate: [AuthGuard] },
      //{ path: "administration/eqmusersettings", component: EQMusersettingsComponent, data: { preload: false }, canActivate: [AuthGuard] }
    ]
  },
];

export const routing = RouterModule.forChild(routes);
