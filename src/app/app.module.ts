import * as Rollbar from 'rollbar';
import {BrowserModule} from '@angular/platform-browser';
import {
  Injectable,
  Injector,
  InjectionToken,
  NgModule,
  ErrorHandler
} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {TabStripModule} from '@progress/kendo-angular-layout';

// Routes
import {routes} from './app.routes';
import {AppComponent} from './app.component';
import {EqmaintenanceModule} from './eqmaintenance/eqmaintenance.module';
import {GeneralModule} from './general/general.module';
import {SysAdminModule} from './sys-admin/sys-admin.module';
import {SharedModule} from './shared/index';
import {ProjectModule} from './project/project.module';
import {DocumentModule} from './document/document.module';
import {ToolingManagementModule} from './toolingmanagement/toolingmanagement.module';
import {SalesModule} from './sales/sales.module';
//import { AuthGuard } from "./shared/guards/auth.guard"

// Import the ButtonsModule...
import {ToasterModule} from 'angular2-toaster';

const rollbarConfig = {
  accessToken: '2a9717d7937e4ff2bb2dc448b962c3de',
  captureUncaught: true,
  captureUnhandledRejections: true,
};

@Injectable()
export class RollbarErrorHandler implements ErrorHandler {
  constructor(private injector: Injector) {
  }

  handleError(err: any): void {

    const rollbar = this.injector.get(RollbarService);
    rollbar.error(err.originalError || err);

    console.error(err);
  }
}

export function rollbarFactory() {
  return new Rollbar(rollbarConfig);
}

export const RollbarService = new InjectionToken<Rollbar>('rollbar');

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    ToasterModule.forChild(),
    /*
    * App modules
    * */
    CommonModule,
    EqmaintenanceModule,
    GeneralModule,
    SharedModule,
    SysAdminModule,
    TabStripModule,
    ProjectModule,
    DocumentModule,
    ToolingManagementModule,
    SalesModule
  ],
  providers: [
    {provide: ErrorHandler, useClass: RollbarErrorHandler},
    {provide: RollbarService, useFactory: rollbarFactory}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
