// region ILinkedDocumentDto
export interface ILinkedDocumentDto {
  siteContentTypeId: string;
  libraryContentTypeId: string;
  documentTypeName: string;
  group: string;
  libraryName: string;
  documentTypeColumns: Array<any>;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  createdBy: number;
  modifiedBy: number;
  active: boolean;
}

/*
* Edit by loiptx
* */
export class LinkedDocumentDto implements ILinkedDocumentDto {
  siteContentTypeId: string;
  libraryContentTypeId: string;
  documentTypeName: string;
  group: string;
  libraryName: string;
  documentTypeColumns: Array<any>;
  createdByName: string;
  createdDateTime: string;
  createdDate: string;
  modifiedByName: string;
  modifiedDateTime: string;
  createdBy: number;
  modifiedBy: number;
  active: boolean;

  static fromJS(data: any): LinkedDocumentDto {
    const result = new LinkedDocumentDto();
    result.init(data);
    return result;
  }

  constructor(data?: ILinkedDocumentDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.siteContentTypeId = data['siteContentTypeId'];
      this.libraryContentTypeId = data['libraryContentTypeId'];
      this.documentTypeName = data['documentTypeName'];
      this.group = data['group'];
      this.libraryName = data['libraryName'];
      this.documentTypeColumns = data['documentTypeColumns'];
      this.createdByName = data['createdByName'];
      this.createdDateTime = data['createdDateTime'];
      this.createdDate = data['createdDate'];
      this.modifiedByName = data['modifiedByName'];
      this.modifiedDateTime = data['modifiedDateTime'];
      this.createdBy = data['createdBy'];
      this.modifiedBy = data['modifiedBy'];
      this.active = data['active'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};

    data['siteContentTypeId'] = this.siteContentTypeId;
    data['libraryContentTypeId'] = this.libraryContentTypeId;
    data['documentTypeName'] = this.documentTypeName;
    data['group'] = this.group;
    data['libraryName'] = this.libraryName;
    data['documentTypeColumns'] = this.documentTypeColumns;
    data['createdByName'] = this.createdByName;
    data['createdDateTime'] = this.createdDateTime;
    data['createdDate'] = this.createdDate;
    data['modifiedByName'] = this.modifiedByName;
    data['modifiedDateTime'] = this.modifiedDateTime;
    data['createdBy'] = this.createdBy;
    data['modifiedBy'] = this.modifiedBy;
    data['active'] = this.active;

    return data;
  }
}

// endregion

/*
* Edit by loiptx
* */
export class PagedResultDtoOfLinkedDocumentDto implements IPagedResultDtoOfLinkedDocumentDto {
  results: LinkedDocumentDto[];
  count: number;

  static fromJS(data: any): PagedResultDtoOfLinkedDocumentDto {
    const result = new PagedResultDtoOfLinkedDocumentDto();
    result.init(data);
    return result;
  }

  constructor(data?: IPagedResultDtoOfLinkedDocumentDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data && data != null) {
      if (data && data.constructor === Array) {
        this.results = [];
        for (const item of data) {
          this.results.push(LinkedDocumentDto.fromJS(item));
        }
      }
      if (this.count) {
        data['count'] = this.count;
      }
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.results && this.results.constructor === Array) {
      data = [];
      for (const item of this.results) {
        data.push(item.toJSON());
      }
    }
    if (this.count) {
      data['count'] = this.count;
    }
    return data;
  }
}

export interface IPagedResultDtoOfLinkedDocumentDto {
  results: LinkedDocumentDto[];
  count: number;
}

export interface ISerachDocumentDTO {
  documentTypeId: string;
  metadata: any;
  resultLimit: number;
}

export class SerachDocumentDTO implements ISerachDocumentDTO {
  documentTypeId: string;
  metadata: any;
  resultLimit: number;

  static fromJS(data: any): SerachDocumentDTO {
    const result = new SerachDocumentDTO();
    result.init(data);
    return result;
  }

  constructor(data?: ISerachDocumentDTO) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.documentTypeId = data['documentTypeId'];
      this.metadata = data['metadata'];
      this.resultLimit = data['resultLimit'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['documentTypeId'] = this.documentTypeId;
    data['metadata'] = this.metadata;
    data['resultLimit'] = this.resultLimit;
    return data;
  }
}

export class ParamString {
  stringValue: string;
}

export interface IParamUploadDocumentDTO {
  filename: string;
  siteContentTypeId: string;
  metaData: any;
  file: any;
}

export class ParamUploadDocumentDTO implements IParamUploadDocumentDTO {
  filename: string;
  siteContentTypeId: string;
  metaData: any;
  file: any;
  documentFile: string;

  static fromJS(data: any): ParamUploadDocumentDTO {
    const result = new ParamUploadDocumentDTO();
    result.init(data);
    return result;
  }

  constructor(data?: IParamUploadDocumentDTO) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.filename = data['filename'];
      this.siteContentTypeId = data['siteContentTypeId'];
      this.metaData = data['metaData'];
      this.file = data['file'];
      this.documentFile = data['documentFile'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['filename'] = this.filename;
    data['siteContentTypeId'] = this.siteContentTypeId;
    data['metaData'] = this.metaData;
    data['file'] = this.file;
    data['documentFile'] = this.documentFile;
    return data;
  }
}


/*Delete Document*/
export interface IParamDeleteDocumentDTO {
  documentPath: string;

}

export class ParamDeleteDocumentDTO implements IParamDeleteDocumentDTO {
  documentPath: string;

  static fromJS(data: any): ParamDeleteDocumentDTO {
    const result = new ParamDeleteDocumentDTO();
    result.init(data);
    return result;
  }

  constructor(data?: IParamDeleteDocumentDTO) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.documentPath = data['documentPath'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['documentPath'] = this.documentPath;
    return data;
  }
}


/**
 Document publish
 */
export interface IParamDocumentPublishing {
  documentId: number;
  libraryName: string;
  setToStatus: DocumentPublishingStatus;
  docPath: string;

}

export enum DocumentPublishingStatus {
  Pending = 2,
  Approved = 0,
  Rejected = 1,
  Unpublished = 3
}

export class ParamDocumentPublishing implements IParamDocumentPublishing {
  documentId: number;
  libraryName: string;
  setToStatus: DocumentPublishingStatus;
  docPath: string;

  static fromJS(data: any): ParamDeleteDocumentDTO {
    const result = new ParamDeleteDocumentDTO();
    result.init(data);
    return result;
  }

  constructor(data?: IParamDeleteDocumentDTO) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.documentId = data['documentId'];
      this.libraryName = data['libraryName'];
      this.setToStatus = data['setToStatus'];

      this.docPath = data['docPath'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['documentId'] = this.documentId;
    data['libraryName'] = this.libraryName;
    data['setToStatus'] = this.setToStatus;
    data['docPath'] = this.docPath;
    return data;
  }
}

export interface IParamUpdateDocument {
  documentPath: string;
  siteContentTypeId: string;
  metaData: any;
}

export class ParamUpdateDocument implements IParamUpdateDocument {
  documentPath: string;
  siteContentTypeId: string;
  metaData: any;

  static fromJS(data: any): ParamUpdateDocument {
    const result = new ParamUpdateDocument();
    result.init(data);
    return result;
  }

  constructor(data?: IParamDeleteDocumentDTO) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.documentPath = data['documentPath'];
      this.siteContentTypeId = data['siteContentTypeId'];
      this.metaData = data['metaData'];
    }
  }


  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['documentPath'] = this.documentPath;
    data['siteContentTypeId'] = this.siteContentTypeId;
    data['metaData'] = this.metaData;
    return data;
  }
}


export interface ITerm {
  id: string;
  termSetName: string;
  name: string;
  description: string;
  localCustomProperties: any;
  modifiedDate: Date;
}

export class Term implements ITerm {
  id: string;
  termSetName: string;
  name: string;
  description: string;
  localCustomProperties: any;
  modifiedDate: Date;

  static fromJS(data: any): Term {
    const result = new Term();
    result.init(data);
    return result;
  }

  constructor(data?: ITerm) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.id = data['id'];
      this.termSetName = data['termSetName'];
      this.name = data['name'];
      this.description = data['description'];
      this.localCustomProperties = data['localCustomProperties'];
      this.modifiedDate = data['modifiedDate'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['termSetName'] = this.termSetName;
    data['name'] = this.name;
    data['description'] = this.description;
    data['localCustomProperties'] = this.localCustomProperties;
    data['modifiedDate'] = this.modifiedDate;
    return data;
  }
}

/*Delete tag param*/
export interface IParamAppTag {
  documentPath: string;
  relatedApplication: RegisteredApplication;
  relatedObjectType: RegisteredApplicationObjectType;
  relatedObjectId: string;
}

export enum RegisteredApplication {
  Documents = 0,
  ProcessLog = 1,
  QualityManagementSystem = 2,
  EquipmentMaintenance = 3,
  ToolingManagementSystem = 4,
  Projects = 5,
  SalesQuoting = 6
}

export enum RegisteredApplicationObjectType {
  Project = 0,
  Task = 1,
  HoldTag = 2,
  Concern = 3,
  Quote = 4,
  RFQ = 5,
  Part = 6,
  Customer = 7,
  CustomerPart = 8,
  LogEntry = 9,
  WIPID = 10,
  MaintenanceOrder = 11,
  Process = 12,
  WorkOrder = 13,
  ProductionStandard = 14,
  Area = 15,
  Department = 16,
  Team = 17,
  Site = 18,
  User = 19,
  None = 20
}

export class ParamAppTag implements IParamAppTag {
  documentPath: string;
  relatedApplication: RegisteredApplication;
  relatedObjectType: RegisteredApplicationObjectType;
  relatedObjectId: string;

  static fromJS(data: any): ParamAppTag {
    const result = new ParamAppTag();
    result.init(data);
    return result;
  }

  constructor(data?: IParamAppTag) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.documentPath = data['documentPath'];
      this.relatedApplication = data['relatedApplication'];
      this.relatedObjectType = data['relatedObjectType'];
      this.relatedObjectId = data['relatedObjectId'];
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data['documentPath'] = this.documentPath;
    data['relatedApplication'] = this.relatedApplication;
    data['relatedObjectType'] = this.relatedObjectType;
    data['relatedObjectId'] = this.relatedObjectId;
    return data;
  }
}
