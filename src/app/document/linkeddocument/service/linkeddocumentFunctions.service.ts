import {Injectable, Inject} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import {
  ParamUploadDocumentDTO,
  ParamDeleteDocumentDTO,
  ParamDocumentPublishing,
  ParamUpdateDocument,
  ParamAppTag
} from './linkeddocument.model';
import {CommonService} from '../../../shared/services/common.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class LinkedDocumentFunctionsService {

  private apiBaseUrl;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService, private http: HttpClient) {
    this.apiBaseUrl = environment.bllApiBaseAddress + '/documents/api/documentfunctions/';
  }

  fileUpload(internalname: ParamUploadDocumentDTO, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'uploaddocument';
    this.commonService.httpPost(url, internalname, successCallback, errorCallback);
  }

  fileDelete(internalname: ParamDeleteDocumentDTO, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'DeleteDocument';
    this.commonService.httpPost(url, internalname, successCallback, errorCallback);
  }

  documentPublishing(internalName: ParamDocumentPublishing, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'documentpublishing';
    this.commonService.httpPost(url, internalName, successCallback, errorCallback);
  }

  documentUpdate(internalname: ParamUpdateDocument, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'updateDocument';
    this.commonService.httpPost(url, internalname, successCallback, errorCallback);
  }

  /*App Tag*/
  linkDocumentToAppTag(internalname: ParamAppTag, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'linkDocumentToAppTag';
    this.commonService.httpPost(url, internalname, successCallback, errorCallback);
  }

  unLinkAppTag(internalname: ParamAppTag, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'unlinkdocumenttoapptag';
    this.commonService.httpPost(url, internalname, successCallback, errorCallback);
  }
}

