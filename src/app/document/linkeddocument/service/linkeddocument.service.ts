import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

import {
  PagedResultDtoOfLinkedDocumentDto,
  SerachDocumentDTO,
  ParamString
} from './linkeddocument.model';
import {CommonService} from '../../../shared/services/common.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class LinkedDocumentService {

  private apiBaseUrl;
  private baseResource;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;

  constructor(private commonService: CommonService,
              private http: HttpClient) {
    this.baseResource = '/documents/api/default/';
    this.apiBaseUrl = environment.bllApiBaseAddress + this.baseResource;
  }

  /*create(input: CreateManufacturerDto, successCallback: any, errorCallback: any): any {
   let url = this.apiBaseUrl + "InsertAsync";

   this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  update(input: ManufacturerDto, successCallback: any, errorCallback: any): any {
   let url = this.apiBaseUrl + "UpdateAsync";

   this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  delete(id: any, successCallback: any, errorCallback: any): any {
   let url = this.apiBaseUrl + "DeleteAsync";

   let params = new HttpParams().set('id', id);

   let options = {
     params: params
   };

   this.commonService.httpRequest('post', url, options, successCallback, errorCallback);
  }*/

  getAllDocumentType(): Observable<PagedResultDtoOfLinkedDocumentDto> {
    const resource = this.baseResource + 'getlistdocumenttypes';

    return this.commonService.httpGet(resource)
      .flatMap((response) => {
        const result200 = PagedResultDtoOfLinkedDocumentDto.fromJS(response);
        return Observable.of(result200);
      })
  }

  getTerms(internalname: any, successCallback: any, errorCallback: any): any {

    const url = this.apiBaseUrl + 'getterms';

    this.commonService.httpPost(url, internalname, successCallback, errorCallback);
  }

  getAll(input: SerachDocumentDTO) {

    const url = this.apiBaseUrl + 'search';
    this.commonService.httpRequest('post',
      url,
      input,
      success => {
      },
      errorCallback => {
      });
  }

  getT(input: SerachDocumentDTO, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'search';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  getDocumentLockedByUser(library: string, itemId: number): any {
    const resource = this.baseResource + 'getdocumentlockedbyuser';
    const params = new HttpParams()
      .set('Library', library)
      .set('ItemId', itemId.toString());
    return this.commonService.httpGet(resource, params);
  }

  getDocumentMetadata(input: ParamString, successCallback: any, errorCallback: any): any {
    const url = this.apiBaseUrl + 'getdocumentmetadata';
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  /*App tag*/
  getDocumentAppTags(docPath: string, successCallback: any, errorCallback: any) {
    const url = this.apiBaseUrl + 'getDocumentAppTags';
    const input: ParamString = new ParamString();
    input.stringValue = docPath;
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }

  getDocumentsWithAppTag(tagName: string, successCallback: any, errorCallback: any) {
    const url = this.apiBaseUrl + 'GetDocumentsWithAppTag';
    const input: ParamString = new ParamString();
    input.stringValue = tagName;
    this.commonService.httpPost(url, input, successCallback, errorCallback);
  }
}

