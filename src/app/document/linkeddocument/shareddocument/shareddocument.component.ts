import {
  Component,
  HostListener,
  Input, OnInit,
  ViewEncapsulation
} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {Observable} from 'rxjs/Observable';
import {
  process,
  SortDescriptor,
  State
} from '@progress/kendo-data-query';
import {
  DataStateChangeEvent,
  GridDataResult,
  PageChangeEvent
} from '@progress/kendo-angular-grid';
import {HubConnection} from '@aspnet/signalr-client';

import {LinkedDocumentService} from '../service/linkeddocument.service';
import {LinkedDocumentFunctionsService} from '../service/linkeddocumentFunctions.service'
import {
  ILinkedDocumentDto,
  ParamAppTag,
  ParamUploadDocumentDTO,
  RegisteredApplication,
  RegisteredApplicationObjectType,
  SerachDocumentDTO
} from '../service/linkeddocument.model';

import {CommonService} from '../../../shared/services/common.service';
import {ToasterHelperService} from '../../../shared/services/toasterHelper.service';
import {UserSettingsService} from '../../../shared/usersettings/usersetting.service';
import {SharedDocumentDto} from './service/shareddocument.model';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'sharedDocument',
  templateUrl: './shareddocument.component.html',
  styleUrls: ['../css/linkeddocument.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class SharedDocumentComponent implements OnInit {
  @Input() public moduleType: string;
  @Input() public objectType: string;
  @Input() public objectId: string;


  active = false;
  paramUploadDocumentDTO: ParamUploadDocumentDTO = new ParamUploadDocumentDTO();

  data: ILinkedDocumentDto;
  public view: Observable<GridDataResult>;
  public editDataItem: any;
  public isNew: boolean;
  public pageSize = 10;
  public skip = 0;
  private editedRowIndex: number;

  allDocumentData: any = [];
  public gridView: GridDataResult;
  public deleteDataItem: any;
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  public azureStorageContainer: string;

  private sort: SortDescriptor[] = [];
  private hubConnection: HubConnection;
  private activeDataItem: any;

  private linkedDocType: any;
  private searchByList: any = [];
  private filtersearchByList: any = [];

  isPublishConfirmation: Boolean = false;
  columns: string[] = [];

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private userSettingsService: UserSettingsService,
              private linkedDocumentService: LinkedDocumentService,
              private linkedDocumentFunctionsService: LinkedDocumentFunctionsService,
              private toasterHelperService: ToasterHelperService) {
    this.azureStorageContainer = environment.azureStorageContainer;
  }

  ngOnInit() {
    this.signalRConnection();
    this.loadGridData();
  }

  signalRConnection() {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');

        this.hubConnection
          .invoke('Subscribe',
            'Documents',
            'DocumentMetadata', '', '')
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated',
      (groupName: string, operation: string, messageJson: string) => {
        const _responseText = messageJson;
        let result200: any = null;
        const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
        const resultData200 = this.commonService.toCamel(data);
        result200 = resultData200 ? SerachDocumentDTO.fromJS(resultData200) : new SerachDocumentDTO();
        const dataExist = this.containsObject(result200, this.gridView.data);

        if (operation.toLowerCase() === 'insert' && !dataExist) {
          this.gridView.data.unshift(result200);
        }

        if (operation.toLowerCase() === 'update') {
          this.gridView.data.forEach((element, index) => {
            if (element.documentTypeId === result200.documentTypeId) {
              //
            }
          });
        }

        if (operation.toLowerCase() === 'delete' && dataExist) {
          let index = null;
          this.gridView.data.forEach((element, i) => {
            if (element.documentTypeId === result200.documentTypeId) {
              index = i;
            }
          });
          if (index !== null) {
            this.gridView.data.splice(index, 1);
          }
        }
        this.loadGridData();
      });
  }

  containsObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].documentTypeId === obj.documentTypeId) {
        return true;
      }
    }

    return false;
  }


  // Dialog box
  isDeleteOpenConfirmation = false;
  loading = false;
  gridLoading = true;
  isEditMode = false;


  private sharedDocumentDto: SharedDocumentDto = new SharedDocumentDto();

  unlinkComfirmation(status) {
    const $this = this;
    if (status) {
      const paramAppTag: ParamAppTag = new ParamAppTag();
      paramAppTag.documentPath = this.sharedDocumentDto.fileRef;
      paramAppTag.relatedApplication = RegisteredApplication[this.moduleType];
      paramAppTag.relatedObjectType = RegisteredApplicationObjectType[this.objectType];
      paramAppTag.relatedObjectId = this.objectId;
      this.gridLoading = true;
      this.linkedDocumentFunctionsService.unLinkAppTag(paramAppTag,
        sucess => {
          if (paramAppTag) {
            this.toasterHelperService.success('Sucess', 'Tag Unlink sucessfully');
            this.gridView.data = this.gridView.data
              .filter(g => g.fileRef !== this.sharedDocumentDto.fileRef);
          } else {
            this.toasterHelperService.error('Unlink tag error', paramAppTag.toString());
          }
          this.sharedDocumentDto = undefined;
          this.gridLoading = false;
        },
        error => {
          this.toasterHelperService.error('Error', 'Tag not Unlinked.');
          this.sharedDocumentDto = undefined;
          this.gridLoading = false;
        });
    } else {
      this.sharedDocumentDto = undefined;
    }
    $this.isDeleteOpenConfirmation = false;

  }

  public openDeleteConformation() {
    this.isDeleteOpenConfirmation = true;
  }

  public actionData(dataItem): Array<any> {
    const item: Array<any> = [];

    item.push({text: 'Unlink', icon: 'trash'});
    return item;
  }

  public onAction(e, dataItem) {
    if (e === undefined || e === null) {
      const link = document.createElement('a');
      link.target = '_blank';
      link.href = dataItem.path.toString();
      link.setAttribute('visibility', 'hidden');
      link.click();
    }
    else {
      if (e.text === 'Unlink') {
        this.sharedDocumentDto = dataItem;
        this.openDeleteConformation();
      }
    }
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.loadGridData();
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
  }

  onPublishComfirmation(isConfirm: boolean, isCancel: boolean = false) {

  }

  private loadGridData(): void {
    this.gridLoading = true;
    const input: string = RegisteredApplication[this.moduleType] + '-' + RegisteredApplicationObjectType[this.objectType] + '-' + this.objectId;
    this.linkedDocumentService.getDocumentsWithAppTag(input,
      res => {
        this.allDocumentData = SharedDocumentDto.initList(res);
        this.gridView = {
          data: this.allDocumentData,
          total: res.length
        };
        this.gridLoading = false;
      },
      err => {
      });
  }

  public addHandler({sender}) {

  }

  public editHandler({dataItem}) {
    this.editDataItem = dataItem;
    this.isNew = false;
  }

  public saveHandler(failuredata) {
    this.gridLoading = true;
    if (this.isNew) {
      failuredata.active = (failuredata.active == null) ? false : failuredata.active;

    } else {

    }
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    // this.formGroup = undefined;
  }

  public cancelHandler() {
    this.editDataItem = undefined;
    // this.formGroup = undefined;
  }

  public removeHandler({dataItem}) {
    this.deleteDataItem = dataItem;
    this.openDeleteConformation();
  }


  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {
      this.cancelHandler();
      this.unlinkComfirmation(false);
    }
  }


  public siteSelectionChange(): void {
    this.gridLoading = true;
    this.loadGridData();
  }


  public internaltype: any;
  public internalText: any;

  /*For alert*/
  private isAlertOpen = false;
  private alertText: string;

  onAlertClose() {
    this.alertText = '';
    this.isAlertOpen = false;
  }

  /**
   Document Upload
   */
  public isOpenDocument = false;

  public onCancelLinkedDocumentForm() {
    this.loadGridData();
    this.isOpenDocument = false;
  }

  public onDocumentOpen() {
    this.isOpenDocument = true;
  }

  updateUrl(data) {
    data.userAvatar = 'assets/img/no_avatar.png';
  }


  /**
   Local data filter
   */

  public state: State = {
    skip: 0,
    take: 10,
  };

  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.gridView = process(this.allDocumentData, this.state);
  }
}
