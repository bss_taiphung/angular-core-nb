// region ISharedDocumentDto
export interface ISharedDocumentDto {
  appTags: string;
  documentType: string;
  editor: string;
  fileName: string;
  fileRef: string;
  fileSize: number;
  modified: Date;
  path: string;
}

export class SharedDocumentDto implements ISharedDocumentDto {
  appTags: string;
  documentType: string;
  editor: string;
  fileName: string;
  fileRef: string;
  fileSize: number;
  modified: Date;
  path: string;

  static fromJS(data: any): SharedDocumentDto {
    const result = new SharedDocumentDto();
    result.init(data);
    return result;
  }

  static initList(data?: any) {
    const results: SharedDocumentDto[] = [];
    if (data && data != null) {
      if (data && data.constructor === Array) {
        for (const item of data) {
          results.push(SharedDocumentDto.fromJS(item));
        }
      }
    }
    return results;
  }

  constructor(data?: ISharedDocumentDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.appTags = data['appTags'];
      this.documentType = data['documentType'];
      this.editor = data['editor'];
      this.fileName = data['fileName'];
      this.fileRef = data['fileRef'];
      this.fileSize = data['fileSize'];
      this.modified = data['modified'] !== null ? new Date(data['modified']) : null;
      this.path = data['path'];
    }
  }
}

// endregion
