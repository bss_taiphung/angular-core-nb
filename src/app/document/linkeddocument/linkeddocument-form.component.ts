import {
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {
  FormControl,
  FormGroup
} from '@angular/forms';

import {LinkedDocumentService} from './service/linkeddocument.service';
import {LinkedDocumentFunctionsService} from './service/linkeddocumentFunctions.service';
import {
  ParamAppTag,
  ParamString,
  ParamUpdateDocument,
  ParamUploadDocumentDTO,
  RegisteredApplication,
  RegisteredApplicationObjectType
} from './service/linkeddocument.model';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';

import {CommonService} from '../../shared/services/common.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'kendo-linkedDocument-form',
  styleUrls: ['./css/linkeddocument.component.css'],
  templateUrl: './html/linkeddocument-form.component.html'
})


export class LinkedDocumentFormComponent {

  @Input() public moduleType: string;
  @Input() public objectType: string;
  @Input() public objectId: string;


  dialogWidth = '100%';
  public listItems: Array<string> = [];
  private documentTypeList: any = [];
  private filterdocumentTypeList: any = [];
  private metaTitle = '';
  private metaSubject = '';
  private metaType = '';
  private filesBytes: any;
  isShowMetaData = false;
  isEditMode = false;
  isDataLoaded = false;
  documentForm: FormGroup = new FormGroup({
    'file': new FormControl(''),
    'filename': new FormControl(''),
    'siteContentTypeId': new FormControl(''),
  });

  uploadDocumentDto: ParamUploadDocumentDTO;
  private documentFile = '';

  constructor(private linkedDocumentService: LinkedDocumentService,
              private linkedDocumentFunctionsService: LinkedDocumentFunctionsService,
              private commonService: CommonService,
              private toasterHelperService: ToasterHelperService) {


  }

  @Input() public isDocumentOpen = false;

  @Input()
  public set model(uploadDocumentDto: ParamUploadDocumentDTO) {

    if (Object.keys(uploadDocumentDto).length > 0) {
      this.isDataLoaded = false;
      this.uploadDocumentDto = uploadDocumentDto;
      this.documentFile = uploadDocumentDto.documentFile;
      const paramString: ParamString = new ParamString();
      paramString.stringValue = this.documentFile;
      this.linkedDocumentService.getDocumentMetadata(paramString,
        response => {
          this.metaTitle = response.title;
          this.metaSubject = response.subject;
          this.metaType = response.part;
          this.documentTypeList.forEach((docType) => {
            if (this.uploadDocumentDto.siteContentTypeId.toString().includes(docType.value.toString())) {
              this.documentForm.controls['siteContentTypeId'].setValue(docType.value);
              this.onDocumentTypeChange(docType);
            }
          });
          this.isDataLoaded = true;
        },
        error => {

        });
      this.isEditMode = true;
      this.isShowMetaData = true;
    } else {

      this.isDataLoaded = true;
    }
    this.diologClass = true;
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();

  private linkedDocType: any;

  ngOnInit() {
    this.linkedDocumentService.getAllDocumentType()
      .subscribe(eventResult => {
        this.linkedDocType = eventResult.results;
        eventResult.results
          .forEach((element) => {
            this.documentTypeList.push({text: element.documentTypeName, value: element.siteContentTypeId});
          });
        this.filterdocumentTypeList = this.documentTypeList;

      });
  }

  private documentTypeListHandleFilter(value) {
    this.filterdocumentTypeList =
      this.documentTypeList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  diologClass = true;

  public onTabSelect(e) {
    if (e.index === 0) {
      this.diologClass = true;
    }
    else {
      this.diologClass = false;
    }
  }

  private searchByList: any = [];
  private filtersearchByList: any = [];
  private taxonomyFieldTypeDropdown = false;
  private taxonomyFieldTypeList: any = [];

  onDocumentTypeChange(docType: any) {
    this.isShowMetaData = true;
    let taxonomyColumn: any;
    const linkedDocType = this.linkedDocType
      .filter(ldt => ldt.siteContentTypeId === docType.value)[0];
    taxonomyColumn = linkedDocType.documentTypeColumns
      .filter(dtc => dtc.columnType === 'TaxonomyFieldType')[0];

    if (taxonomyColumn !== undefined && taxonomyColumn !== null && taxonomyColumn.columnType === 'TaxonomyFieldType') {
      this.taxonomyFieldTypeDropdown = true;
      const paramString: ParamString = new ParamString();
      paramString.stringValue = taxonomyColumn.internalName;
      this.linkedDocumentService.getTerms(paramString,
        response => {
          this.taxonomyFieldTypeList.length = 0;
          response.forEach((element, index) => {
            this.taxonomyFieldTypeList.push({text: element.name, value: element.id});
          });
        },
        error => {

        }
      );
    } else {
      this.taxonomyFieldTypeDropdown = false;
    }
  }

  private file: any = [];

  getFiles(event) {
    this.documentForm.controls['filename'].setValue(event.name);
    this.file = event.bytes;
    this.isDataLoaded = true;
  }

  onClickFileUpload(event) {
    this.isDataLoaded = false;
  }

  onDocumentUpload(): void {

    if (this.documentForm.valid) {
      this.isDataLoaded = false;
      if (this.isEditMode) {
        const paramUpdateDocument: ParamUpdateDocument = new ParamUpdateDocument();
        paramUpdateDocument.documentPath = this.documentFile;
        paramUpdateDocument.siteContentTypeId = this.documentForm.controls['siteContentTypeId'].value.toString();
        const metaData: Object = {Title: this.metaTitle, Subject: this.metaSubject, Part: this.metaType};
        paramUpdateDocument.metaData = metaData;
        this.linkedDocumentFunctionsService.documentUpdate(paramUpdateDocument,
          response => {
            if (response.includes('Password is null')) {
              this.toasterHelperService.error('Error', response.toString());
            } else {
              if (this.moduleType !== undefined && this.moduleType !== '') {
                this.onLinkToApp(response.toString());
              } else {
                this.toasterHelperService.success('Update', 'Document updated sucessfully.');
                this.closeForm();
              }

            }
          },
          error => {
            this.toasterHelperService.errorMessage(error);
          });
      } else {
        const selectedDocType: ParamUploadDocumentDTO = new ParamUploadDocumentDTO();
        selectedDocType.siteContentTypeId = this.documentForm.controls['siteContentTypeId'].value.toString();
        const metaData: Object = {Title: this.metaTitle, Subject: this.metaSubject, Part: this.metaType};
        selectedDocType.metaData = metaData;
        selectedDocType.filename = this.documentForm.controls['filename'].value.toString();
        selectedDocType.documentFile = this.file;
        this.linkedDocumentFunctionsService.fileUpload(selectedDocType,
          response => {
            if (response.includes('Password is null')) {
              this.toasterHelperService.error('Error', response.toString());
            } else {
              if (this.moduleType !== undefined && this.moduleType !== '') {
                this.onLinkToApp(response.toString());
              } else {
                this.toasterHelperService.success('Update', 'Document updated sucessfully.');
                this.closeForm();
              }
            }

          },
          error => {
            this.toasterHelperService.errorMessage(error);
          });
      }
    }
  }

  onLinkToApp(relativeUrl) {
    const paramAppTag: ParamAppTag = new ParamAppTag();
    paramAppTag.documentPath = relativeUrl;
    paramAppTag.relatedApplication = RegisteredApplication[this.moduleType];
    paramAppTag.relatedObjectType = RegisteredApplicationObjectType[this.objectType];
    paramAppTag.relatedObjectId = this.objectId;
    this.linkedDocumentFunctionsService.linkDocumentToAppTag(paramAppTag,
      sucess => {
        if (paramAppTag) {
          this.toasterHelperService.success('Sucess', 'Document updated and Tag created sucessfully');
          this.closeForm();
        } else {
          this.toasterHelperService.error('Link tag error', paramAppTag.toString());
        }
      },
      error => {
        this.toasterHelperService.errorMessage(error);
      });
  }

  public showButton(state: any): boolean {
    return false;
  }

  uploadSaveUrl = environment.bllApiBaseAddress + '/documents/api/documentfunctions/UploadDoc'; // should represent an actual API endpoint
  uploadRemoveUrl = 'removeUrl';

  private closeForm(): void {
    this.isEditMode = false;
    this.isDataLoaded = true;
    this.taxonomyFieldTypeDropdown = false;
    this.isDocumentOpen = false;
    this.documentForm.controls['file'].setValue('');
    this.documentForm.controls['filename'].setValue('');
    this.documentForm.controls['siteContentTypeId'].setValue('');
    this.metaTitle = '';
    this.metaSubject = '';
    this.metaType = '';
    this.isShowMetaData = false;
    this.cancel.emit();
  }
}
