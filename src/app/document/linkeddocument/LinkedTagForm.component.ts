import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import {FormControl, FormGroup} from '@angular/forms';

import {GridDataResult} from '@progress/kendo-angular-grid';

import {ParamAppTag, RegisteredApplication, RegisteredApplicationObjectType} from './service/linkeddocument.model';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {LinkedDocumentService} from './service/linkeddocument.service';
import {LinkedDocumentFunctionsService} from './service/linkeddocumentFunctions.service';
import {CommonService} from '../../shared/services/common.service';

@Component({
  selector: 'kendo-linkedDocument-tag-form',
  styles: [
    'input[type=text] { width: 100%; }', '.k-dropdown  {width: 100% !important;}'
  ],
  templateUrl: './html/LinkedTagForm.component.html'
})


export class LinkedTagFormComponent implements OnInit {

  public tagForm: FormGroup = new FormGroup({
    'file': new FormControl(''),
    'filename': new FormControl(''),
    'siteContentTypeId': new FormControl(''),
  });

  private documentFile = '';
  private isDataLoaded = true;
  private columns: any;
  private gridView: GridDataResult;

  private application: any;
  private objectType: any;

  private addObjectType: any;
  private addApplication: any;
  private addObjectId: any;
  private docPath: string;
  private actionData: any = [];
  @Input() public isTagOpen = false;

  constructor(private linkedDocumentService: LinkedDocumentService,
              private linkedDocumentFunctionsService: LinkedDocumentFunctionsService,
              private commonService: CommonService,
              private toasterHelperService: ToasterHelperService) {
  }

  @Input()

  public set model(documentPath: string) {
    if (documentPath !== undefined) {
      this.isDataLoaded = false;
      this.docPath = documentPath;
      this.linkedDocumentService.getDocumentAppTags(documentPath, res => {
        this.gridView = {
          data: res,
          total: res.length
        };


        this.isDataLoaded = true;
      }, error => {
      });
    }
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.objectType = Object.keys(RegisteredApplicationObjectType)
      .map(key => ({value: key, text: RegisteredApplicationObjectType[key]}));
    this.application = Object.keys(RegisteredApplication)
      .map(key => ({value: key, text: RegisteredApplication[key]}));
  }


  private closeForm(): void {
    this.isTagOpen = false;
    this.cancel.emit();
  }

  onLinkToApp() {
    const paramAppTag: ParamAppTag = new ParamAppTag();
    paramAppTag.documentPath = this.docPath;
    paramAppTag.relatedApplication = this.addApplication;
    paramAppTag.relatedObjectType = this.addObjectType;

    paramAppTag.relatedObjectId = this.addObjectId;
    this.linkedDocumentFunctionsService.linkDocumentToAppTag(paramAppTag, sucess => {
      if (paramAppTag) {
        this.toasterHelperService.success('Sucess', 'Tag attached sucessfully');
      } else {
        this.toasterHelperService.error('Link tag error', paramAppTag.toString());
      }
    }, error => {
      this.toasterHelperService.error('Error', 'Tag not attached.');
    });


  }

  onAction(event, data) {
    const paramAppTag: ParamAppTag = new ParamAppTag();
    paramAppTag.documentPath = this.docPath;
    paramAppTag.relatedApplication = data.localCustomProperties.relatedApplication;
    paramAppTag.relatedObjectType = data.localCustomProperties.relatedObjectType;
    paramAppTag.relatedObjectId = data.localCustomProperties.relatedObjectId;
    this.linkedDocumentFunctionsService.unLinkAppTag(paramAppTag, sucess => {
      if (paramAppTag) {
        this.toasterHelperService.success('Sucess', 'Tag Unlink sucessfully');
      } else {
        this.toasterHelperService.error('Unlink tag error', paramAppTag.toString());
      }
    }, error => {
      this.toasterHelperService.error('Error', 'Tag not Unlinked.');
    });
  }
}
