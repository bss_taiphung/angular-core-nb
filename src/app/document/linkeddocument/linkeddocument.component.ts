import {
  Component,
  ViewEncapsulation,
  HostListener,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';

import 'rxjs/add/observable/fromEvent';
import {Observable} from 'rxjs/Observable';
import {
  process,
  State
} from '@progress/kendo-data-query';
import {
  PageChangeEvent,
  GridDataResult,
  DataStateChangeEvent
} from '@progress/kendo-angular-grid';
import {HubConnection} from '@aspnet/signalr-client';
import {SortDescriptor} from '@progress/kendo-data-query';

import {LinkedDocumentService} from './service/linkeddocument.service';
import {LinkedDocumentFunctionsService} from './service/linkeddocumentFunctions.service'

import {
  ILinkedDocumentDto,
  SerachDocumentDTO,
  ParamString,
  ParamDeleteDocumentDTO,
  ParamUploadDocumentDTO,
  ParamDocumentPublishing,
  DocumentPublishingStatus,
  RegisteredApplication,
  RegisteredApplicationObjectType
} from './service/linkeddocument.model';
import {CommonService} from '../../shared/services/common.service';
import {ToasterHelperService} from '../../shared/services/toasterHelper.service';
import {SelectListDto} from '../../shared/models/selectListDto'
import {UserSettingsService} from '../../shared/usersettings/usersetting.service';
import {ParamAppTag} from './service/linkeddocument.model';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'linkeddocument',
  templateUrl: './html/linkeddocument.component.html',
  styleUrls: ['./css/linkeddocument.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class LinkedDocumentComponent {

  public active = false;
  public searchDocumentForm: FormGroup = new FormGroup({
    'documentType': new FormControl('', Validators.required),
    'searchBy': new FormControl('', Validators.required),
    'choice': new FormControl(''),
    'taxonomyFieldType': new FormControl(''),
  });


  @Input() public isViewOnly = false;
  @Input() public moduleType: string;
  @Input() public objectType: string;
  @Input() public objectId: string;

  @Output() closeform: EventEmitter<any> = new EventEmitter();
  public documentTypeList: any = [];
  public taxonomyFieldTypeList: any = [];
  public filterdocumentTypeList: any = [];
  data: ILinkedDocumentDto;
  public formGroup: FormGroup;
  public view: Observable<GridDataResult>;
  public editDataItem: any;
  public isNew: boolean;
  public pageSize = 10;
  public skip = 0;
  public gridView: GridDataResult;
  public deleteDataItem: any;
  public isactive = true;
  public searchtext = '';
  public siteList: SelectListDto[];
  public mss: any;
  public azureStorageContainer: string;
  // Dialog box
  public isDeleteOpenConfirmation = false;
  public internaltype: any;
  public internalText: any;
  /**
   Document Upload
   */
  public isOpenDocument = false;
  /**
   Local data filter
   */
  public state: State = {
    skip: 0,
    take: 10,
  };
  protected jsonParseReviver: (key: string, value: any) => any = undefined;
  paramUploadDocumentDTO: ParamUploadDocumentDTO = new ParamUploadDocumentDTO();
  private choiceTextBox = false;
  private taxonomyFieldTypeDropdown = false;
  private documentTypeId: any = '';
  private documentsSiteCollection: any;
  private documentsTypeAndColumnGroupName: any;
  private editedRowIndex: number;
  isFormTagOpen = false;
  /*Tag */
  tagModel: string;
  sort: SortDescriptor[] = [];
  private hubConnection: HubConnection;
  seachCriteriaList: any[] = [];
  private allDocumentData: any[] = [];
  private linkedDocType: any;
  private searchByList: any = [];
  private filtersearchByList: any = [];
  loading = true;
  gridLoading = true;
  isEditMode = false;
  private paramDeleteDocumentDto: ParamDeleteDocumentDTO = new ParamDeleteDocumentDTO();
  private activeDataItem: any;
  isPerformingOperation = false;
  /**
   publish
   */

  isPublishConfirmation: Boolean = false;
  columns: string[] = [];
  /*For alert*/
  isAlertOpen = false;
  alertText: string;

  constructor(private toasterService: ToasterHelperService,
              private commonService: CommonService,
              private userSettingsService: UserSettingsService,
              private linkedDocumentService: LinkedDocumentService,
              private linkedDocumentFunctionsService: LinkedDocumentFunctionsService,
              private toasterHelperService: ToasterHelperService) {
    this.mss = this.commonService.getCookies('Mss');
    this.documentsTypeAndColumnGroupName = environment.documentsTypeAndColumnGroupName;
    this.documentsSiteCollection = environment.documentsSiteCollection;
    this.azureStorageContainer = environment.azureStorageContainer;
  }

  ngOnInit() {
    this.documentTypeList.push({text: 'All', value: 'All'});
    this.linkedDocumentService.getAllDocumentType()
      .subscribe(eventResult => {
        this.linkedDocType = eventResult.results;
        this.filterdocumentTypeList.length = 0;
        eventResult.results.forEach((element, index) => {
          this.documentTypeList.push({text: element.documentTypeName, value: element.siteContentTypeId});
        });
        this.filterdocumentTypeList = this.documentTypeList;
        this.searchDocumentForm.controls['documentType'].setValue('All');
        this.documentTypeId = 'All';
        this.searchByDropDownList();
      });
    this.signalRConnection();

    if (!this.isViewOnly) {
      this.loadGridData();
    }
    else {
      this.loading = false;
      this.gridLoading = false;
    }
  }

  signalRConnection() {
    this.hubConnection =
      new HubConnection(environment.signalRServer + 'repositoryHub');
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started!');

        this.hubConnection
          .invoke('Subscribe',
            'Documents',
            'DocumentMetadata', 'Active', 'True')
          .catch(err => console.error(err));
      })
      .catch(err => console.log('Error while establishing connection :('));


    this.hubConnection.on('clientOnEntityUpdated',
      (groupName: string, operation: string, messageJson: string) => {
        const _responseText = messageJson;
        const data = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
        const result200 = this.commonService.toCamel(data);
        const dataExist = this.containsObject(result200, this.gridView.data);

        if (result200.editor !== null && result200.editor !== '') {
          const item: Array<any> = [];
          item.push({
            userAvatar: '',
            userName: result200.editor,
          });
          result200.editor = item;
        } else {
          result200.editor = null;
        }

        if (operation.toLowerCase() === 'insert' && !dataExist) {
          this.gridView.data.unshift(result200);
        }

        if (operation.toLowerCase() === 'update') {
          this.gridView.data.forEach((element, index) => {
            if (element.gUID === result200.gUID) {
              //
              if (this.isactive) {
                if (result200.active === this.isactive) {
                  this.gridView.data[index] = result200;
                } else {
                  operation = 'delete';
                }
              } else {
                this.gridView.data[index] = result200;
              }
            }
          });
        }

        if (operation.toLowerCase() === 'delete' && dataExist) {
          let index = null;
          this.gridView.data.forEach((element, i) => {
            if (element.gUID === result200.gUID) {
              index = i;
            }
          });
          if (index !== null) {
            this.gridView.data.splice(index, 1);
          }
        }
      });
  }

  containsObject(obj, list) {
    let x;
    for (x in list) {
      if (list.hasOwnProperty(x) && list[x].gUID === obj.gUID) {
        return true;
      }
    }

    return false;
  }

  searchByDropDownList() {
    this.searchDocumentForm.controls['searchBy'].setValue('');
    this.searchByList = [];
    const dt = this.filterdocumentTypeList.filter((s) => s.text !== '');
    this.filterdocumentTypeList = dt;

    if (this.documentTypeId.value === 'All') {
      this.linkedDocType.forEach((element, index) => {
        this.documentTypeList.push({text: '', value: ''});
        element.documentTypeColumns.forEach((e1) => {
          if (e1.internalName === 'Title' ||
            e1.internalName === 'Subject' ||
            e1.group === this.documentsTypeAndColumnGroupName) {
            let isNotElementInList = true;
            this.searchByList.forEach((serchListElement) => {
              if (serchListElement.value === e1.internalName) {
                isNotElementInList = false;
              }
            });
            if (isNotElementInList) {
              this.searchByList.push({text: e1.displayName, value: e1.internalName});
            }
          }
        });
      });
    } else {
      this.linkedDocType.forEach((element, index) => {
        element.documentTypeColumns.forEach((e1) => {
          let isNotElementInList = true;
          this.searchByList.forEach((serchListElement) => {
            if (serchListElement.value === e1.internalName) {
              isNotElementInList = false;
            }
          });
          if (isNotElementInList) {
            this.searchByList.push({text: e1.displayName, value: e1.internalName});
          }

        });
      });
    }

  }

  deleteComfirmation(status) {
    const $this = this;
    if (status) {
      this.linkedDocumentFunctionsService.fileDelete(this.paramDeleteDocumentDto, response => {
          if (response.toString().toLowerCase() === 'true') {
            if (response) {
              this.toasterHelperService.success('Sucess', 'Document deleted!');
            }
          } else {
            this.toasterHelperService.success('Error', response.toString());
          }
        },
        error => {
          this.toasterHelperService.error('Error', 'system error.');
        });
    }
    this.paramDeleteDocumentDto = undefined;
    $this.isDeleteOpenConfirmation = false;

  }

  public openDeleteConformation() {
    this.isDeleteOpenConfirmation = true;
  }

  public actionData(dataItem): Array<any> {
    const item: Array<any> = [];
    if (!this.isViewOnly) {
      item.push({text: 'Edit', icon: 'edit-tools'});
      item.push({text: 'Delete', icon: 'trash'});
      if (dataItem.status !== 'Approved') {
        item.push({text: 'Publish', icon: 'connector'});
      }
      item.push({text: 'App Tags', icon: 'link-vertical'});
    }
    return item;
  }

  public onAction(e, dataItem) {
    this.isPerformingOperation = true;
    this.linkedDocumentService.getDocumentLockedByUser(dataItem.libraryName, dataItem.id)
      .subscribe(eventResult => {
          this.isPerformingOperation = false;
          if (eventResult === '') {

            if (e === undefined || e === null) {
              if (!this.isViewOnly) {
                const link = document.createElement('a');
                link.target = '_blank';
                link.href = dataItem.openURL.toString();
                link.setAttribute('visibility', 'hidden');
                link.click();
              } else {
                this.onLinkToApp(dataItem.relativeURL.toString());
              }
            }
            else {
              if (e.text === 'Delete') {

                this.isDeleteOpenConfirmation = true;
                if (dataItem.relativeURL) {
                  this.paramDeleteDocumentDto.documentPath = dataItem.relativeURL;

                }
              }
              else if (e.text === 'Publish') {
                if (dataItem.status === 'Approved') {
                  this.toasterService.error('Approved', 'Document is aready approved.');
                } else {
                  this.activeDataItem = dataItem;
                  this.isPublishConfirmation = true;
                }

              }
              else if (e.text === 'Edit') {
                const paramUploadDoc: ParamUploadDocumentDTO = new ParamUploadDocumentDTO();
                paramUploadDoc.documentFile = dataItem.relativeURL.toString();
                paramUploadDoc.siteContentTypeId = dataItem.contentTypeId.toString();
                paramUploadDoc.filename = dataItem.fileName.toString();
                this.paramUploadDocumentDTO = paramUploadDoc;
                this.isOpenDocument = true;
              }
              else if (e.text === 'App Tags') {
                if (this.isViewOnly) {
                  this.onLinkToApp(dataItem.relativeURL.toString());
                } else {
                  this.tagModel = dataItem.relativeURL.toString();
                  this.isFormTagOpen = true;
                }

              }

            }

          } else {
            this.toasterHelperService.success('Error', 'Document is locked!');
          }
        },
        error => {
          this.toasterHelperService.error('Error', 'system error.');
          this.isPerformingOperation = false;
        });

  }

  onLinkToApp(relativeUrl) {

    const paramAppTag: ParamAppTag = new ParamAppTag();
    paramAppTag.documentPath = relativeUrl;
    paramAppTag.relatedApplication = RegisteredApplication[this.moduleType];
    paramAppTag.relatedObjectType = RegisteredApplicationObjectType[this.objectType];
    paramAppTag.relatedObjectId = this.objectId;
    this.isPerformingOperation = true;
    this.linkedDocumentFunctionsService.linkDocumentToAppTag(paramAppTag, sucess => {
      if (paramAppTag) {
        this.toasterHelperService.success('Sucess', 'Tag attached sucessfully');
        this.isPerformingOperation = false;
        this.closeform.emit();
      } else {
        this.toasterHelperService.error('Link tag error', paramAppTag.toString());
        this.isPerformingOperation = false;
      }
    }, error => {
      this.toasterHelperService.error('Error', 'Tag not attached.');
    });


  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.gridLoading = true;
    this.loadGridData();
  }

  public addHandler({sender}) {

  }

  public editHandler({dataItem}) {
    this.editDataItem = dataItem;
    this.isNew = false;
  }

  public saveHandler(failuredata) {
    this.gridLoading = true;
    if (this.isNew) {
      failuredata.active = (failuredata.active == null) ? false : failuredata.active;
      /*const inputFailureData: CreateManufacturerDto = failuredata;
      this.linkedDocumentService.create(inputFailureData,
       response => {
         this.toasterService.success("", 'Manufacturer Saved Successfully');
         this.gridLoading = false;
       },
       error => {
         this.toasterService.errorMessage(error);
         this.gridLoading = false;
       }
      );*/
    } else {
      /*const inputFailureData: ManufacturerDto = failuredata;
      this.linkedDocumentService.update(inputFailureData,
       response => {
         this.toasterService.success("", 'Manufacturer Updated Successfully');
         this.gridLoading = false;
       },
       error => {
         this.toasterService.errorMessage(error);
         this.gridLoading = false;
       }
      );*/
    }
  }

  public cancelHandler() {
    this.editDataItem = undefined;
    this.formGroup = undefined;
  }

  public removeHandler({dataItem}) {
    this.deleteDataItem = dataItem;
    this.openDeleteConformation();
  }

  public showActive() {
    this.isactive = !this.isactive;
    this.gridLoading = true;
  }

  public onSearchKeyup() {
    this.skip = 0;
    this.pageSize = 10;
    this.gridLoading = true;
    this.loadGridData();

  }

  @HostListener('keydown', ['$event'])
  public keydown(event: any): void {
    if (event.keyCode === 27) {
      this.cancelHandler();
      this.deleteComfirmation(false);
    }
  }

  public siteSelectionChange(): void {
    this.gridLoading = true;
    this.loadGridData();
  }

  public onDocumentTypeChange(changedDocumentType: any): void {
    this.documentTypeId = changedDocumentType.value;
    this.searchByDropDownList();
    this.seachCriteriaList = [];
    this.taxonomyFieldTypeDropdown = false;
    this.choiceTextBox = false;
  }

  public onSearchTypeChange(changedSearcheType: any): void {
    let columnType;
    this.linkedDocType.forEach((element, index) => {
      element.documentTypeColumns.forEach((e1, index) => {
        if (e1.internalName === changedSearcheType.value) {
          columnType = e1.columnType;
          this.internaltype = e1.internalName;
          this.internalText = e1.displayName;
        }
      });
    });
    if (columnType.includes('TaxonomyFieldType')) {
      this.taxonomyFieldTypeDropdown = true;
      this.choiceTextBox = false;
      const paramString: ParamString = new ParamString();
      paramString.stringValue = this.internaltype;
      this.linkedDocumentService.getTerms(paramString,
        response => {
          this.taxonomyFieldTypeList.length = 0;
          response.forEach((element, index) => {
            this.taxonomyFieldTypeList.push({text: element.name, value: element.id});
          });
        },
        error => {
          this.toasterService.errorMessage(error);
          this.gridLoading = false;
        }
      );
    } else {
      this.choiceTextBox = true;
      this.taxonomyFieldTypeDropdown = false;
    }
  }

  searchDocuments() {
    const input: SerachDocumentDTO = new SerachDocumentDTO();
    input.documentTypeId = this.documentTypeId.value === 'All' ? '' : this.documentTypeId.value;
    input.resultLimit = 500;
    const v = this.seachCriteriaList;
    const objectMetadata: Object = new Object();
    objectMetadata['Path'] = this.documentsSiteCollection;
    this.seachCriteriaList.forEach((e1, index) => {
      objectMetadata[e1.searchKey] = e1.searchId;
    });

    input.metadata = objectMetadata;
    this.gridLoading = true;
    this.linkedDocumentService.getT(input,
      res => {
        if (res != null && res.length > 0) {
          this.columns = Object.keys(res[0]).filter(
            col => col !== 'id'
              && col !== 'relativeURL'
              && col !== 'fullURL'
              && col !== 'openURL'
              && col !== 'contentTypeId' && col !== 'libraryName');
          res.forEach(itm => {
            if (itm.editor != null && itm.editor !== '') {
              const item: Array<any> = [];
              item.push({
                userAvatar: this.commonService.getUserThumbnail(JSON.parse(itm.editor).Key),
                userName: JSON.parse(itm.editor).Value,
              });
              itm.editor = item;
            } else {
              itm.editor = null;
            }
          });
        }
        this.allDocumentData = res;
        this.state = {
          /*To reset Filter after new seraching*/
          skip: 0,
          take: 10,
        };
        this.gridView = process(this.allDocumentData, this.state);
        this.loading = false;
        this.gridLoading = false;


      },
      err => {
      });
  }

  public addSearchDocuments() {

    if (this.searchDocumentForm.valid) {
      if (this.seachCriteriaList.filter(scl => scl.searchKey === this.internaltype).length > 0) {
        this.isAlertOpen = true;
        this.alertText = this.internaltype + ' is already in seach createria.';
      } else {
        if (this.choiceTextBox) {
          this.seachCriteriaList.push({
            searchText: this.internalText + ':' + this.searchDocumentForm.controls['choice'].value,
            searchId: this.searchDocumentForm.controls['choice'].value,
            searchKey: this.internaltype
          });
          this.searchDocumentForm.controls['choice'].setValue('');
        } else {
          let selectedText: string;
          this.taxonomyFieldTypeList.forEach((e1, index) => {
            if (e1.value === this.searchDocumentForm.controls['taxonomyFieldType'].value) {
              selectedText = e1.text;
            }
          });
          this.seachCriteriaList.push({
            searchText: this.internalText + ':' + selectedText,
            searchId: selectedText,
            searchKey: this.internaltype
          });
          this.searchDocumentForm.controls['taxonomyFieldType'].setValue('');
        }
      }
    }
  }

  onAlertClose() {
    this.alertText = '';
    this.isAlertOpen = false;
  }

  public onCancelLinkedDocumentForm() {
    // this.loadGridData();
    this.isOpenDocument = false;
  }

  public onDocumentOpen() {
    this.isOpenDocument = true;
  }

  onCancelLinkedTagForm() {
    this.tagModel = undefined;
    this.isFormTagOpen = false;
  }

  updateUrl(data) {
    data.userAvatar = 'assets/img/no_avatar.png';
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.gridView = process(this.allDocumentData, this.state);
  }

  protected pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.gridLoading = true;
  }

  onPublishComfirmation(isConfirm: boolean, isCancel: boolean = false) {
    if (!isCancel) {
      const paramDocumentPublishing: ParamDocumentPublishing = new ParamDocumentPublishing();
      paramDocumentPublishing.documentId = this.activeDataItem.id.toString();
      paramDocumentPublishing.libraryName = this.activeDataItem.libraryName.toString();

      if (isConfirm) {
        paramDocumentPublishing.setToStatus = DocumentPublishingStatus.Approved;
      } else {

        paramDocumentPublishing.setToStatus = DocumentPublishingStatus.Rejected;
      }
      paramDocumentPublishing.docPath = this.activeDataItem.relativeURL.toString();
      this.linkedDocumentFunctionsService.documentPublishing(paramDocumentPublishing, response => {
          if (response.toString().toLowerCase() === 'true') {
            if (isConfirm) {
              this.toasterService.success('Approved', 'Document approved  Successfully');
            } else {
              this.toasterService.success('Rejected', 'Document rejected  Successfully');
            }
          } else {
            this.toasterService.error('Publish error', response.toString());
          }


        },
        error => {
          this.toasterService.errorMessage(error);
        }
      );

    }
    this.isPublishConfirmation = false;
  }

  private loadGridData(): void {
    this.gridLoading = true;
    const input: SerachDocumentDTO = new SerachDocumentDTO();
    input.documentTypeId = this.documentTypeId.value;
    input.resultLimit = 500;
    input.metadata = JSON.parse('{"Path":  "' + this.documentsSiteCollection + '" }');
    this.linkedDocumentService.getT(input,
      res => {
        res.forEach(itm => {
          if (itm.editor !== null && itm.editor !== '') {
            const item: Array<any> = [];
            item.push({
              userAvatar: this.commonService.getUserThumbnail(JSON.parse(itm.editor).Key),
              userName: JSON.parse(itm.editor).Value,
            });
            itm.editor = item;
          } else {
            itm.editor = null;
          }
        });
        if (res.length > 0) {
          this.columns = Object.keys(res[0]).filter(
            col => col !== 'id'
              && col !== 'relativeURL'
              && col !== 'fullURL'
              && col !== 'openURL'
              && col !== 'contentTypeId'
              && col !== 'libraryName');
        }
        this.allDocumentData = res;
        this.gridView = {
          data: res,
          total: res.length
        };

        this.loading = false;
        this.gridLoading = false;

      },
      err => {
      });
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  private documentTypeListHandleFilter(value) {
    this.filterdocumentTypeList =
      this.documentTypeList.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
}
