import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {TabStripModule} from '@progress/kendo-angular-layout';
import {ButtonsModule} from '@progress/kendo-angular-buttons';
import {DialogModule} from '@progress/kendo-angular-dialog';
import {GridModule} from '@progress/kendo-angular-grid';
import {DateInputsModule} from '@progress/kendo-angular-dateinputs';
import {DropDownsModule} from '@progress/kendo-angular-dropdowns';
import {TelerikReportingModule} from '@progress/telerik-angular-report-viewer';

import {GeneralModule} from '../general/general.module';
import {SharedModule} from '../shared/index';


import {routing} from './document.routes';
import {DocumentModuleComponent} from './document.component';
import {LinkedDocumentComponent} from './linkeddocument/linkeddocument.component';
import {LinkedComponent} from './linkeddocument/linked.component';
import {SharedDocumentComponent} from './linkeddocument/shareddocument/shareddocument.component';
import {LinkedDocumentService} from './linkeddocument/service/linkeddocument.service';
import {LinkedDocumentFunctionsService} from './linkeddocument/service/linkeddocumentFunctions.service';

import {LinkedDocumentFormComponent} from './linkeddocument/linkeddocument-form.component';
import {LinkedTagFormComponent} from './linkeddocument/LinkedTagForm.component';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    ButtonsModule,
    GridModule,
    DialogModule,
    DateInputsModule,
    DropDownsModule,
    CommonModule,
    routing,
    TelerikReportingModule,
    GeneralModule,
    SharedModule,
    TabStripModule
  ],
  exports: [
    SharedDocumentComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  declarations: [
    DocumentModuleComponent,
    LinkedDocumentComponent,
    LinkedDocumentFormComponent,
    LinkedTagFormComponent,
    SharedDocumentComponent,
    LinkedComponent
  ],
  providers: [
    LinkedDocumentService,
    LinkedDocumentFunctionsService
  ]

})
export class DocumentModule {
}
