import {
  RouterModule,
  Routes
} from '@angular/router';
import {DocumentModuleComponent} from './document.component';
import {LinkedComponent} from './linkeddocument/linked.component';

import {AuthGuard} from '../shared/guards/auth.guard'

export const routes: Routes = [
  {
    path: '', component: DocumentModuleComponent,
    canActivate: [AuthGuard],
    children: [
      {path: 'linkdocument', component: LinkedComponent, data: {preload: false}, canActivate: [AuthGuard]}
    ]
  },
];

export const routing = RouterModule.forChild(routes);
