import {Routes} from '@angular/router';
import {AuthGuard} from './shared/guards/auth.guard'
import {AppComponent} from './app.component'

export const routes: Routes = [
  {
    path: 'app',
    component: AppComponent,
    children: [
      {
        path: 'eqmaintenance',
        loadChildren: './eqmaintenance/eqmaintenance.module#EqmaintenanceModule',
        data: {preload: false},
        canActivate: [AuthGuard]
      },
      {
        path: 'sysadmin',
        loadChildren: './sys-admin/sys-admin.module#SysAdminModule',
        data: {preload: false},
        canActivate: [AuthGuard]
      },
      {
        path: 'general',
        loadChildren: './general/general.module#GeneralModule',
        data: {preload: false}
      },
      {
        path: 'project',
        loadChildren: './project/project.module#ProjectModule',
        canActivate: [AuthGuard],
        data: {preload: false}
      },
      {
        path: 'document',
        loadChildren: './document/document.module#DocumentModule',
        canActivate: [AuthGuard],
        data: {preload: false}
      }
      ,
      {
        path: 'toolingmanagement',
        loadChildren: './toolingmanagement/toolingmanagement.module#ToolingManagementModule',
        canActivate: [AuthGuard],
        data: {preload: false}
      },
      {
        path: 'sales',
        loadChildren: './sales/sales.module#SalesModule',
        data: { preload: false },
        canActivate: [AuthGuard]
      }
    ],
  }
];
