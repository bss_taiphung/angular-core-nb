"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.routes = [
    { path: '', redirectTo: 'auth', pathMatch: 'full' },
    //{ path: '**', redirectTo: 'auth', pathMatch: 'full' },
    {
        path: 'auth',
        loadChildren: './auth/auth.module#AuthModule',
        data: { preload: true },
    },
    {
        path: 'app',
        loadChildren: './app/app.module#AppModule',
        data: { preload: false },
    }
];
//# sourceMappingURL=root.routes.js.map