Server Requirements
===================


Installing platform
===================
Step 1:
-------
#### Clone
```bash
$ git clone https://domain/project.git
```

Step 2:
-------
#### Installation
```bash
$ npm i && npm i @aspnet/signalr-client@1.0.0-alpha2-final
```

#### Localhost
Run serve
```bash
$ npm start
```
Or run watch
```bash
$ npm run watch
```
Or run build
```bash
$ npm run build
```

#### Development
```bash
$ npm run build:dev
```

#### Production
```bash
$ npm run build:prod
```
